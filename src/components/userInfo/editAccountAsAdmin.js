import React, { Component } from "react";
import PropTypes from "prop-types";
import UserContext from "../../contexts/user.jsx";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';
import "../../assets/formik-form-style.css";
import Loader from "../Loader/Loader";
import lodashGet from 'lodash/get';
import lodashFind from 'lodash/find';
import lodashMap from 'lodash/map';

// import "./form.css";
import { updateUser, updateUserPassword, getUserById, getCountries, getStates, getCities, getUserChannelFee, addChannelFees } from "../../services/userService";
import { getAllChannels } from "../../services/listingsService";
// import Cookies from "universal-cookie";
import { Row, Col, FormInput } from "shards-react";
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Switch from 'antd/es/switch';

import { withTranslation, Translation } from "react-i18next";
import Select from 'react-select'
// const cookies = new Cookies();

// icon style
import styled from 'styled-components'
import { Add } from '@styled-icons/material-rounded/Add';
import { Minus } from '@styled-icons/heroicons-solid/Minus';
import { IosArrowLeft } from '@styled-icons/fluentui-system-filled/IosArrowLeft';
import { userRolesLower, listingConst, channels } from "../../constants/constants";
import { Modal } from 'react-bootstrap';
const Leftarrow = styled(IosArrowLeft)`
  color: #2C91EE;
  width:18px;
  height:18px;
`;
const AddIcon = styled(Add)`
color: #2C91EE;
width:30px;
height:30px;
`
const DefaultAddIcon = styled(Add)`
color: #bdc2c7;
width:30px;
height:30px;
`

const MinusIcon = styled(Minus)`
color: #BEBEBE;
width:30px;
height:30px;
`

export class EditAccountAsAdmin extends Component {
  static contextType = UserContext;
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      channelManage: [],
      channelManagmentInputList: [],
      countryList: "",
      stateList: [],
      cityList: [],
      selectedCountry: "",
      selectedCountryDropDown: "",
      selectedState: "",
      selectedStateDropDown: "",
      selectedCity: "",
      selectedCityDropDown: "",
      user: null,
      pass: "",
      passConfirm: "",
      ebayMarkupValue: '',
      ebayMarkupType: false,
      phunnelMarkupValue: '',
      phunnelMarkupType: false,
      amazonMarkupValue: '',
      amazonMarkupType: false,
      stockxMarkupValue: '',
      stockxMarkupType: false,
      ebayMarkupActive: false,
      amazonMarkupActive: false,
      phunnelMarkupActive: true,
      stockxMarkupActive: false,
      modalResponse: '',
      userInfoModal: false,
      userPageTitle: '',
      userConfirmModal: false,
      formSubmit: ''
    };
    this.handlePassChange = this.handlePassChange.bind(this);
    this.handlePassConfirmChange = this.handlePassConfirmChange.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleStateChange = this.handleStateChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.dropdownFilterOption = this.dropdownFilterOption.bind(this);
    this.onLoadCountryDropDown = this.onLoadCountryDropDown.bind(this);
    this.onLoadStateDropDown = this.onLoadStateDropDown.bind(this);
    this.onLoadCityDropDown = this.onLoadCityDropDown.bind(this);
    this.onLoadGetChannels = this.onLoadGetChannels.bind(this);
    this.channelManagmentOnchange = this.channelManagmentOnchange.bind(this);
    this.handlChannelFeeSubmit = this.handlChannelFeeSubmit.bind(this);
    this.handlFormSubmit = this.handlFormSubmit.bind(this);
  }

  async onLoadCountryDropDown() {

    let countries = await getCountries();
    if (countries) {
      const countriesList = [];
      countries.length && countries.map(items => {
        if (this.state.user.country === items.id) {
          this.setState({
            selectedCountryDropDown: { label: items.name, value: items.id }
          });
        }
        countriesList.push({ label: items.name, value: items.id },)
      }
      )
      await this.setState({
        countryList: countriesList
      });
    }
    if (this.state.user.country) {
      await this.setState({
        selectedCountry: this.state.user.country
      });
    } else {
      if (countries) {
        await this.setState({
          selectedCountry: countries[0].id
        });
      }
    }

  }

  async onLoadStateDropDown() {

    let states = await getStates(this.state.user.country);
    if (states) {
      const stateList = [];
      states.length && states.map(items => {

        if (this.state.user.state === items.id) {
          this.setState({ selectedStateDropDown: { label: items.name, value: items.id }, selectedState: items.id });
        }

        stateList.push({ label: items.name, value: items.id })
      });
      await this.setState({
        stateList: stateList
      });
    }

  }

  async onLoadCityDropDown() {

    let cities = await getCities(this.state.user.country, this.state.user.state);
    if (cities) {
      const cityList = [];
      cities.length && cities.map(items => {

        if (this.state.user.city === items.id) {
          this.setState({ selectedCityDropDown: { label: items.name, value: items.id }, selectedCity: items.id });
        }

        cityList.push({ label: items.name, value: items.id })
      });
      await this.setState({
        cityList: cityList
      });
    }
  }

  async componentDidMount() {
    let user = await (getUserById(this.props.match.params.id));
    if (user) {
      await this.setState({ user });
      this.props.history.push({ pageTitle: 'User Profile', pageSubTitle: 'subtitle', redirectPath: 'all-marketplace-users' });

      await this.onLoadCountryDropDown();
      await this.onLoadStateDropDown();
      await this.onLoadCityDropDown();
      await this.onLoadGetChannels();
      this.setState({loading:false});
    }
    else{
      window.open("/home", "_self");
    }




  }
  async submitUserChange(user) {
    let userUpdate = await updateUser(user);
    return userUpdate;
  }

  async changePass(password) {

    let emailid = this.state.user.email
    let userPassUpdate = await updateUserPassword(emailid, password);
    if (userPassUpdate.email) {
      this.setState({ modalResponse: this.props.t("changePassword.successbyAdmin"), pass: '', passConfirm: '', userInfoModal: true, userPageTitle: this.props.t("changePassword.title") })
    }
    else {
      this.setState({ modalResponse: this.props.t("changePassword.errorbyAdmin"), pass: '', passConfirm: '', userInfoModal: true, userPageTitle: this.props.t("changePassword.title") });
    }
  }
  handlePassChange(event) {
    this.setState({ pass: event.target.value });
  }
  handlePassConfirmChange(event) {
    this.setState({ passConfirm: event.target.value });
  }

  async handleCountryChange(country) {
    if (country) {
      this.setState({
        selectedCountryDropDown: country,
        selectedCountry: country.value,
        selectedState: "",
        selectedStateDropDown: null,
        stateList: [],
        cityList: [],
        selectedCity: "",
        selectedCityDropDown: null
      });

      let states = await getStates(country.value && country.value.length ? country.value : '');
      if (states) {
        const stateList = [];
        states.length && states.map(items => {
          stateList.push({ label: items.name, value: items.id })
        });
        await this.setState({
          stateList: stateList
        });
      }

    } else {
      this.setState({
        selectedCountryDropDown: country,
        selectedCountry: "",
        stateList: [],
        selectedState: "",
        selectedStateDropDown: null,
        cityList: [],
        selectedCity: "",
        selectedCityDropDown: null
      });
    }
  }


  async handleStateChange(state) {
    if (state) {
      this.setState({
        selectedStateDropDown: state,
        selectedState: state.value
      });


      let cities = await getCities(this.state.selectedCountry, state.value);
      if (cities) {
        const cityList = [];
        cities.length && cities.map(items => {
          cityList.push({ label: items.name, value: items.id })
        });
        await this.setState({
          cityList: cityList
        });
      }


    } else {

      await this.setState({
        selectedStateDropDown: state,
        selectedState: "",
        cityList: [],
        selectedCity: "",
        selectedCityDropDown: null
      });
    }


  }

  async handleCityChange(city) {
    if (city) {
      this.setState({
        selectedCity: city.value,
        selectedCityDropDown: city
      });
    } else {
      this.setState({
        selectedCityDropDown: city,
        selectedCity: ""
      });
    }
  }

  dropdownFilterOption = ({ label, value, data }, string) => {
    if (this.state.value1 === data) {
      return false;
    } else if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };
  /**
   * This function handles updating all associated user listings when there
   * is new global markup information.
   * TODO: Uncomment the necessary imports at the beginning of the file.
   * @param {*} event
   * @param {*} userID
   */
  handleMarkupSubmit(event, sellerID) {
    const {
      ebayMarkupValue,
      ebayMarkupType,
      phunnelMarkupValue,
      phunnelMarkupType,
      amazonMarkupValue,
      amazonMarkupType,
      stockxMarkupValue,
      stockxMarkupType,
      ebayMarkupActive,
      amazonMarkupActive,
      phunnelMarkupActive,
      stockxMarkupActive
    } = this.state


  }

  async inputUserAccount(e) { //User Account fields add/update    
    let userValues = this.state.user;
    if (userValues && e) {
      userValues[e.target.name] = e.target.value;
    }
    await this.setState({ user: userValues })
  }

  // Channel Start
  async onLoadGetChannels() {
    const getChannels = await getAllChannels();
    const getChannelFee = await getUserChannelFee(this.state.user.id);

      getChannels && getChannels.length > 0
        && getChannels.map((channelitem) => {
          var data = {
            "channel_id": channelitem.id,
            "channel_name": channelitem.channel_name,
            "markup_amount": "0"
          }
         this.setState({ channelManagmentInputList: [...this.state.channelManagmentInputList, data] })
        });
        let updatedChannelManagmentInputList = lodashMap(this.state.channelManagmentInputList,ch=>{
          ch.markup_amount= lodashGet(lodashFind(lodashGet(getChannelFee,'channelFeesJSON',[]),cf=>cf.channel_id == ch.channel_id),'markup_amount')||0;
          return ch;
        });
        this.setState({ channelManagmentInputList: updatedChannelManagmentInputList });
  }

  async handlFormSubmit() {
    const { channelManagmentInputList } = this.state;
    if (channelManagmentInputList.length > 0 ) {
     await addChannelFees(this.state.user.id, this.context.user.id, channelManagmentInputList);
    await this.CloseConfirmModal();
    }
  }
  async CloseConfirmModal() {
    await this.setState({ userConfirmModal: false, modalResponse: '', formSubmit: '' })
  }

  async channelManagmentOnchange(e, statearraynumber, userRole) {
    var statearrayvalue = e.target.value;
    let maxLength = listingConst.maxLength_percent;
    if (statearrayvalue.length <= maxLength && e.nativeEvent.data != 'e') {//avoid length of the value more than specified 
      let updatedChanelInputList = this.state.channelManagmentInputList;
      if(userRole == userRolesLower.super_super_admin || userRole == userRolesLower.super_admin || userRole == userRolesLower.admin ){
        updatedChanelInputList[statearraynumber].markup_amount = statearrayvalue;
      }else{
        updatedChanelInputList[statearraynumber].markup_amount = updatedChanelInputList[statearraynumber].markup_amount;
      }
      await this.setState({
        channelManagmentInputList: updatedChanelInputList
      });
    }
  }

    async handlChannelFeeSubmit() {
      this.setState({ modalResponse: this.props.t("userInfo.channelFee.confirmMessage"), userPageTitle: this.props.t("userInfo.channelFee.title"), userConfirmModal: true, formSubmit: this.props.t("userInfo.formSubmit.channelFee") });
    }

  render() {
    let submitUserChange = this.submitUserChange;

    let country_name = '';

    this.state.countryList.length > 0
      && this.state.countryList.map((item, i) => {
        if (this.state.selectedCountry == item.id) {

          // <option
          //   key={i}
          //   value={item.id}
          // >{(item.name)}</option>
          country_name = item.name

        }
      })

    return !this.state.loading ? (
      <UserContext.Consumer>
        {context => (
          <div className="container-userprofile pl-db-50">
            {/* <div style={{ display: "flex", alignItems: "center", width: "100%" }}>
              <button className="back-btn btn"><Leftarrow /></button>
              <PageTitle
                title={"User Profile"}
                className="text-sm-left mb-3"
              />
            </div> */}
            <div className="small-container-userprofile user-card-section">
              <div className="user-account-card-userprofile">
                {this.state.user && (

                  <Formik

                    initialValues={{
                      firstName: this.state.user.firstName,
                      lastName: this.state.user.lastName,
                      id: this.state.user.id,
                      phone: this.state.user.phone && this.state.user.phone != 0 ? this.state.user.phone : '',
                      email: this.state.user.email,
                      about: this.state.user.about,
                      streetAddress: this.state.user.streetAddress,
                      streetAddress2: this.state.user.streetAddress2,
                      state: this.state.selectedState,
                      country: this.state.selectedCountry,
                      city: this.state.selectedCity,
                      postalCode: this.state.user.postalCode && this.state.user.postalCode != 0 ? this.state.user.postalCode : '',
                      companyName: this.state.user.companyName,

                    }}

                    enableReinitialize={true}

                    validationSchema={Yup.object().shape({

                      firstName: Yup.string()
                        .required('First Name is required')
                        .min(3, 'First Name should be a minimum of 3 characters'),

                      lastName: Yup.string()
                        .required('Last Name is required'),

                      // companyName: Yup.string().nullable()
                      //   .required('Company Name is required'),

                      phone: Yup.string()
                        .required("Phone number is required")
                        .matches(/^[0-9]+$/, "Phone number must be only digits")
                        .min(10, 'Phone number should be a minimum of 10 digits')
                        .max(15, 'Phone number should be a maximum of 15 digits'),


                      streetAddress: Yup.string()
                        .required('Street Address is required'),

                      state: Yup.string()
                        .required('State is required'),

                      city: Yup.string()
                        .required('City is required'),

                      postalCode: Yup.string()
                        .required("Zip Code is required")
                        .matches(/^[a-zA-Z0-9]+$/, "ZipCode must be only alphanumerics")
                        .min(3, 'ZipCode should be a minimum of 3 alphanumerics')
                        .max(8, 'ZipCode should be a maximum of 8 alphanumerics'),

                    })}


                    onSubmit={async fields => {

                      const { id } = this.state.user
                      var country_data = await { country: this.state.selectedCountry }
                      const userdata = await { ...fields, ...country_data };
                      submitUserChange(userdata).then(user => {
                        this.setState({ modalResponse: this.props.t("userInfo.submitSuccess2"), userPageTitle: this.props.t("userProfile.title"), userInfoModal: true });
                      });

                    }}

                    render={({ errors, status, touched }) => (


                      <Card
                        style={{
                          borderRadius: "10px"
                        }}
                        small
                        className=""
                        bordered={false}
                      >
                        <h6 className="purchase-order-form-headings secondary-font-family primary-color">
                          {this.props.t("userInfo.title")}
                        </h6>


                        <Form>

                          <Row>
                            <Col>

                              <Row>
                                <Col md="3">
                                  <label
                                    htmlFor="firstName"
                                    className="form-label-userprofile"
                                  >
                                    {this.props.t("userInfo.firstName")}
                                  </label>

                                  <div className="form-group">
                                    <Field name="firstName" className={'formik-input-user-form form-control' + (errors.firstName && touched.firstName ? ' is-invalid' : '')} placeholder="First Name" onChange={(e) => this.inputUserAccount(e)} value={this.state.user.firstName} />
                                    <ErrorMessage name="firstName" component="div" className="invalid-feedback" />
                                  </div>

                                </Col>


                                <Col md="3">
                                  <label
                                    htmlFor="lastName"
                                    className="form-label-userprofile"
                                  >
                                    {this.props.t("userInfo.lastName")}
                                  </label>

                                  <div className="form-group">
                                    <Field name="lastName" className={'formik-input-user-form form-control' + (errors.lastName && touched.lastName ? ' is-invalid' : '')} placeholder="Last Name" onChange={(e) => this.inputUserAccount(e)} value={this.state.user.lastName} />
                                    <ErrorMessage name="lastName" component="div" className="invalid-feedback" />
                                  </div>
                                </Col>
                                <Col md="3">
                                  <label
                                    htmlFor="email"
                                    className="form-label-userprofile"
                                  >
                                    {this.props.t("userInfo.email")}
                                  </label>

                                  <div className="form-group">
                                    <Field name="email" readonly="readonly" className={'formik-input-user-form form-control' + (errors.email && touched.email ? ' is-invalid' : '')} placeholder="Email Address" />
                                    <ErrorMessage name="email" component="div" className="invalid-feedback" />
                                  </div>

                                </Col>


                                <Col md="3">
                                  <label
                                    htmlFor="phone"
                                    className="form-label-userprofile"
                                  >
                                    {this.props.t("userInfo.phone")}
                                  </label>

                                  <div className="form-group">
                                    <Field name="phone" className={'formik-input-user-form form-control' + (errors.phone && touched.phone ? ' is-invalid' : '')} placeholder="Phone Number" onChange={(e) => this.inputUserAccount(e)} value={this.state.user.phone} />
                                    <ErrorMessage name="phone" component="div" className="invalid-feedback" />
                                  </div>
                                </Col>
                                <Col md="6">
                                  <label
                                    htmlFor="companyName"
                                    className="form-label-userprofile"
                                  >
                                    Address
                                  </label>

                                  <div className="form-group">
                                    <Field name="streetAddress" className={'formik-input-user-form form-control' + (errors.streetAddress && touched.streetAddress ? ' is-invalid' : '')} placeholder="Street Address1" onChange={(e) => this.inputUserAccount(e)} value={this.state.user.streetAddress} />
                                    <ErrorMessage name="streetAddress" component="div" className="invalid-feedback" />
                                  </div>
                                  <label
                                    htmlFor="companyName"
                                    className="form-label-userprofile"
                                  >
                                    Address 2
                                  </label>

                                  <div className="form-group">
                                    <Field name="streetAddress2" className={'formik-input-user-form form-control' + (errors.streetAddress2 && touched.streetAddress2 ? ' is-invalid' : '')} placeholder="Street Address2" onChange={(e) => this.inputUserAccount(e)} value={this.state.user.streetAddress2} />
                                    <ErrorMessage name="streetAddress2" component="div" className="invalid-feedback" />
                                  </div>

                                </Col>
                                <Col md="6">
                                  <Row>
                                    <Col md="6">
                                      <label
                                        htmlFor="country"
                                        className="form-label-userprofile"
                                      >
                                        {this.props.t("userInfo.address.country")}
                                      </label>

                                      <Select
                                        {...this.props}
                                        placeholder={this.props.t("userInfo.address.country")}
                                        style={{ width: "100%" }}
                                        name={this.props.t("userInfo.address.country")}
                                        filterOption={this.dropdownFilterOption}
                                        value={this.state.selectedCountryDropDown ? this.state.selectedCountryDropDown : ""}
                                        options={this.state.countryList}
                                        onChange={e => this.handleCountryChange(e)}
                                        className={(errors.country && touched.country ? ' is-invalid' : '')}
                                        allowClear
                                        showSearch
                                        isClearable
                                      />
                                    </Col>

                                    <Col md="6">
                                      <label
                                        htmlFor="city"
                                        className="form-label-userprofile"
                                      >
                                        {this.props.t("userInfo.address.city")}
                                      </label>

                                      <div className="form-group">
                                        <Select
                                          {...this.props}
                                          placeholder={this.props.t("userInfo.address.city")}
                                          style={{ width: "100%" }}
                                          name="city"
                                          filterOption={this.dropdownFilterOption}
                                          value={this.state.selectedCityDropDown ? this.state.selectedCityDropDown : ""}
                                          options={this.state.cityList}
                                          onChange={e => this.handleCityChange(e)}
                                          className={(errors.city && touched.city ? ' is-invalid' : '')}
                                          allowClear
                                          showSearch
                                          isClearable
                                        />
                                        <ErrorMessage name="city" component="div" className="invalid-feedback" />
                                      </div>

                                    </Col>


                                    <Col md="6">
                                      <label
                                        htmlFor="state"
                                        className="form-label-userprofile"
                                      >
                                        {this.props.t("userInfo.address.state")}
                                      </label>

                                      <div className="form-group">
                                        <Select
                                          {...this.props}
                                          placeholder={this.props.t("userInfo.address.state")}
                                          style={{ width: "100%" }}
                                          name="state"
                                          filterOption={this.dropdownFilterOption}
                                          value={this.state.selectedStateDropDown ? this.state.selectedStateDropDown : ""}
                                          options={this.state.stateList}
                                          onChange={e => this.handleStateChange(e)}
                                          className={(errors.state && touched.state ? ' is-invalid' : '')}
                                          allowClear
                                          showSearch
                                          isClearable
                                        />
                                        <ErrorMessage name="state" component="div" className="invalid-feedback" />
                                      </div>
                                    </Col>

                                    <Col md="6">
                                      <label
                                        htmlFor="postalCode"
                                        className="form-label-userprofile"
                                      >
                                        {this.props.t("userInfo.address.zip")}
                                      </label>

                                      <div className="form-group">
                                        <Field name="postalCode" className={'formik-input-user-form form-control' + (errors.postalCode && touched.postalCode ? ' is-invalid' : '')} placeholder="Zip Code" onChange={(e) => this.inputUserAccount(e)} value={this.state.user.postalCode} />
                                        <ErrorMessage name="postalCode" component="div" className="invalid-feedback" />
                                      </div>
                                    </Col>




                                  </Row>
                                </Col>
                                <Col md="6">
                                  <label
                                    htmlFor="companyName"
                                    className="form-label-userprofile"
                                  >
                                    {this.props.t("userInfo.companyName")}
                                  </label>

                                  <div className="form-group">
                                      <Field name="companyName" className={'formik-input-user-form form-control' + (errors.companyName && touched.companyName ? ' is-invalid' : '')} placeholder="Company Name" readonly="readonly" />
                                    <ErrorMessage name="companyName" component="div" className="invalid-feedback" />
                                  </div>

                                </Col>


                                <Col md="6" >
                                  <div className="form-group text-center" style={{ marginTop: '30px' }}>
                                    <button type="submit" className="user-info-update-btn stylefont-weight-medium primary-bg-color" >{this.props.t("userInfo.submit")}</button>
                                  </div>
                                </Col>
                              </Row>


                            </Col></Row>
                        </Form>

                      </Card>
                    )}
                  />
                )}
              </div>
              {/* second card starts */}


              <div className="change-password-card-userprofile">

                {/* Change password start */}
                <Formik
                  initialValues={{
                    password: this.state.pass,
                    passwordConfirm: this.state.passConfirm,

                  }}
                  enableReinitialize={true}
                  validationSchema={Yup.object().shape({
                    password: Yup.string()
                      .required('Password is required')
                      // .min(6, 'Password is too short - should be 6 chars minimum.')
                      .matches(
                        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
                        "Must Contain 8 Characters, at least 1 Uppercase, 1 Lowercase, 1 Number and 1 Special Character"
                      ),
                    passwordConfirm: Yup.string()
                      .required('Confirm password is required')
                      .oneOf([Yup.ref('password'), null], 'Passwords must match')
                  })}

                  onSubmit={async fields => {
                    await this.changePass(fields.password);
                  }}
                  render={({ errors, status, touched }) => (
                    <Card
                      style={{
                        borderRadius: "10px"
                      }}
                      small
                      className="mb-4"
                      bordered={false}
                    >
                      <h6 className="purchase-order-form-headings secondary-font-family primary-color">
                        {this.props.t("userInfo.password.change")}
                      </h6>
                      <Form>

                        <Row className="align-items-end">
                          <Col md="4">
                            <label
                              htmlFor="password"
                              className="form-label-userprofile"
                            >
                              {this.props.t("userInfo.password.new")}
                            </label>
                            <div className="form-group">
                              <Field name="password" type="password" className={'formik-input-user-form form-control' + (errors.password && touched.password ? ' is-invalid' : '')} placeholder="New Password" onChange={(e) => this.handlePassChange(e)} value={this.state.pass} />
                              <ErrorMessage name="password" component="div" className="invalid-feedback" />
                            </div>
                          </Col>
                          <Col md="4">
                            <label
                              htmlFor="passwordConfirm"
                              className="form-label-userprofile"
                            >
                              {this.props.t("userInfo.password.confirm")}
                            </label>

                            <div className="form-group">
                              <Field name="passwordConfirm" type="password" className={'formik-input-user-form form-control' + (errors.passwordConfirm && touched.passwordConfirm ? ' is-invalid' : '')} placeholder="Confirm Password" onChange={(e) => this.handlePassConfirmChange(e)} value={this.state.passConfirm} />
                              <ErrorMessage name="passwordConfirm" component="div" className="invalid-feedback" />
                            </div>
                          </Col>
                          <Col md="4">
                            <label style={{ visibility: "hidden" }}
                              htmlFor="passwordCurrent"
                              className="form-label-userprofile">
                            </label>
                            <div className="form-group">
                              <button type="submit" className="user-change-psw-btn primary-bg-color stylefont-weight-bold" >{this.props.t("userInfo.password.submit")}</button>
                            </div>
                          </Col>
                        </Row>
                      </Form>
                    </Card>
                  )}
                />
                {/* Change password end */}

              </div>
            </div>
            {/* second card ends */}
            {/* 3rd card starts */}



            <div className="chennel-grid--section">

              {/* Channel Fees Start*/}

            <div className="channel-grid-column">
                <div className="change-password-card-userprofile">
                  <Card
                    style={{
                      borderRadius: "10px"
                    }}
                    small
                    className=""
                    bordered={false}
                  >
                    <h6 className="purchase-order-form-headings secondary-font-family primary-color">
                      {this.props.t("userInfo.channelFee.title")}
                    </h6>

                    <Form>
                      <Row>
                       

                        {this.state.channelManagmentInputList ? this.state.channelManagmentInputList.map((channel, channelCount) => { 
                          return (
                            <Col md="12" className="form-group">
                            <div className="channel-markup input-row">
                            <div className="channel-inner-sec">
                              <div className="channel-grid2-sec">
                                <div className="chennel-title">
                                  <h3 className="channel-fee-left-title">{channel.channel_name}</h3>
                                </div>
                                <div className="channel-input-field">
                                    <div className="channel-fee-change-txt">
                                      <input 
                                      className={`form-control ${channel.channel_name == channels.Phunnel ?'channel-fee-no-edit-field':''}`} 
                                      type="number"
                                      min="0"
                                      max={this.state.channelManage[channelCount] ? this.state.channelManage[channelCount] : 100}
                                      key={'channel_value' + channelCount}
                                      placeholder={'%'}
                                      value={channel.markup_amount}
                                      disabled={channel.channel_name == channels.Phunnel ? true : false}
                                      onChange={(value) => this.channelManagmentOnchange(value, channelCount, context.user.userRole)}
                                      />
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          </Col>
                            );
                          }
                          ):
                          <Col md="12" className="form-group">
                            <div className="channel-markup input-row">
                            <div className="channel-inner-sec">
                              <div className="channel-grid3-sec">
                                <h4>{this.props.t("userInfo.channelNotFound")}</h4>
                              </div>
                            </div>
                          </div>
                          </Col>
                          }
                        {this.state.channelManagmentInputList?
                        <div className="text-center w-100 mt-4">
                          <Button
                            style={{ margin: "5px" }}
                            type="primary"
                            disabled={context.user.userRole == userRolesLower.super_super_admin || context.user.userRole == userRolesLower.super_admin || context.user.userRole ==  userRolesLower.admin ?false:true}
                            className="channel-submit-btn primary-bg-color stylefont-weight-bold"
                            onClick={e => this.handlChannelFeeSubmit() }
                          >
                            {this.props.t(
                              "userInfo.channelFee.submit"
                            )}
                          </Button>
                      </div>:''}
                      </Row>
                    </Form>
                  </Card>
                </div>

              </div>

            {/* Channel Fees End*/}

              <div className="channel-grid-column">
                <div className="change-password-card-userprofile">
                  <Card
                    style={{
                      borderRadius: "10px"
                    }}
                    small
                    className=""
                    bordered={false}
                  >
                    <h6 className="purchase-order-form-headings secondary-font-family primary-color">
                      {this.props.t("userInfo.channelMarkups.title")}
                    </h6>

                    <Form>
                      <Row>
                        <Col md="12" className="form-group">
                          <div className="channel-markup input-row">
                            <div className="channel-inner-sec">
                              <div className="row align-items-center">
                                <div className="col-md-3">
                                  <label className="form-label-userprofile">
                                    {this.props.t("userInfo.channelMarkups.ebay")}
                                  </label>
                                </div>
                                <div className="col-md-4 p-0">
                                  <div className="d-flex">
                                    <div className="left-switch-item">
                                      <div className="button-switch">
                                        <Switch
                                          checkedChildren="ON"
                                          unCheckedChildren="OFF"
                                          defaultChecked
                                          className="filter-btn-hide--seat"
                                          checked={this.state.ebayMarkupActive}
                                          onChange={value => this.setState({ ebayMarkupActive: value })}
                                        />
                                      </div>
                                    </div>
                                    <div className="right-switch-item">
                                      <div className="button-switch">
                                        <Switch className="price-switch-btn"
                                          checkedChildren="$"
                                          unCheckedChildren="%"
                                          defaultChecked
                                          checked={this.state.ebayMarkupType}
                                          onChange={value => this.setState({ ebayMarkupType: value })}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-md-5">
                                  <div className="plus-minus-btn">
                                    <div className="price-change-txt">
                                      <FormInput
                                        type="number"
                                        min="0"
                                        placeholder="$"
                                        className="form-control"
                                        value={this.state.ebayMarkupValue}
                                        onChange={event => this.setState({ ebayMarkupValue: Math.abs(event.target.value) })}
                                      />
                                    </div>
                                    <button className="price-add-btn primary-border-color"><AddIcon /></button>
                                    <button className="price-mini-btn"><MinusIcon /></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                        <Col md="12" className="form-group">
                          <div className="channel-markup input-row">
                            <div className="channel-inner-sec">
                              <div className="row align-items-center">
                                <div className="col-md-3">
                                  <label className="form-label-userprofile">
                                    {this.props.t("userInfo.channelMarkups.phunnel")}
                                  </label>
                                </div>
                                <div className="col-md-4 p-0">
                                  <div className="d-flex">
                                    <div className="left-switch-item">
                                      <div className="button-switch">
                                        <Switch
                                          className="filter-btn-hide--seat"
                                          checkedChildren="ON"
                                          unCheckedChildren="OFF"
                                          defaultChecked
                                          checked={this.state.phunnelMarkupActive}
                                          onChange={value => this.setState({ phunnelMarkupActive: value })}
                                        />
                                      </div>
                                    </div>
                                    <div className="right-switch-item">                                 <div className="button-switch">
                                      <Switch
                                        className="price-switch-btn"
                                        checkedChildren="$"
                                        unCheckedChildren="%"
                                        checked={this.state.phunnelMarkupType}
                                        onChange={value => this.setState({ phunnelMarkupType: value })}
                                      />
                                    </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-md-5">
                                  <div className="plus-minus-btn">
                                    <div className="price-change-txt">
                                      <FormInput
                                        type="number"
                                        placeholder="%"
                                        className="form-control"
                                        value={this.state.phunnelMarkupValue}
                                        onChange={event => this.setState({ phunnelMarkupValue: event.target.value })}
                                      />
                                    </div>
                                    <button className="price-add-btn inactive primary-border-color"><AddIcon /></button>
                                    <button className="price-mini-btn"><MinusIcon /></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                        <Col md="12" className="form-group">
                          <div className="channel-markup input-row">
                            <div className="channel-inner-sec">
                              <div className="row align-items-center">
                                <div className="col-md-3">
                                  <label
                                    htmlFor="fePassword"
                                    className="form-label-userprofile"
                                  >
                                    {this.props.t("userInfo.channelMarkups.amazon")}
                                  </label>
                                </div>
                                <div className="col-md-4 p-0">
                                  <div className="d-flex">
                                    <div className="left-switch-item">
                                      <div className="button-switch">
                                        <Switch className="filter-btn-hide--seat"
                                          checkedChildren="ON"
                                          unCheckedChildren="OFF"
                                          defaultChecked
                                          checked={this.state.amazonMarkupActive}
                                          onChange={value => this.setState({ amazonMarkupActive: value })}
                                        />
                                      </div>
                                    </div>
                                    <div className="right-switch-item">
                                      <div className="button-switch">
                                        <Switch className="price-switch-btn"
                                          checkedChildren="$"
                                          unCheckedChildren="%"
                                          defaultChecked
                                          checked={this.state.amazonMarkupType}
                                          onChange={value => this.setState({ amazonMarkupType: value })}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-md-5">
                                  <div className="plus-minus-btn">
                                    <div className="price-change-txt">
                                      <FormInput
                                        type="number"
                                        placeholder="$"
                                        className="form-control"
                                        value={this.state.amazonMarkupValue}
                                        onChange={event => this.setState({ amazonMarkupValue: Math.abs(event.target.value) })}
                                      />
                                    </div>
                                    <button className="price-add-btn primary-border-color"><AddIcon /></button>
                                    <button className="price-mini-btn"><MinusIcon /></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                        <Col md="12" className="form-group">
                          <div className="channel-markup input-row">
                            <div className="channel-inner-sec">
                              <div className="row align-items-center">
                                <div className="col-md-3">
                                  <label
                                    htmlFor="fePassword"
                                    className="form-label-userprofile"
                                  >
                                    {this.props.t("userInfo.channelMarkups.stockx")}
                                  </label>
                                </div>
                                <div className="col-md-4 p-0">
                                  <div className="d-flex">
                                    <div className="left-switch-item">
                                      <div className="button-switch">
                                        <Switch className="filter-btn-hide--seat"
                                          checkedChildren="ON"
                                          unCheckedChildren="OFF"
                                        />
                                      </div>
                                    </div>
                                    <div className="right-switch-item">
                                      <div className="button-switch">
                                        <Switch className="price-switch-btn"
                                          checkedChildren="ON"
                                          unCheckedChildren="OFF"
                                          defaultChecked
                                          checked={this.state.stockxMarkupActive}
                                          onChange={value => this.setState({ stockxMarkupActive: value })}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-md-5">
                                  <div className="plus-minus-btn">
                                    <div className="price-change-txt">
                                      <FormInput
                                        type="number"
                                        placeholder="%"
                                        className="form-control"
                                        value={this.state.stockxMarkupValue}
                                        onChange={event => this.setState({ stockxMarkupValue: Math.abs(event.target.value) })}
                                      />
                                    </div>
                                    <button className="price-add-btn inactive primary-border-color"><AddIcon /></button>
                                    <button className="price-mini-btn"><MinusIcon /></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                        <div className="text-center w-100 mt-4">
                          <Button
                            style={{ margin: "5px" }}
                            type="primary"
                            className="channel-submit-btn primary-bg-color stylefont-weight-bold"
                            onClick={event =>
                              this.handleMarkupSubmit(event, this.state.user.id)
                            }
                          >
                            {this.props.t("userInfo.channelMarkups.submit")}
                          </Button>
                        </div>
                      </Row>
                    </Form>
                  </Card>
                </div>
              </div>
              {/* 3rd card ends */}
              {/* 4th card starts */}
              <div className="channel-grid-column">
                {(this.state.user && this.state.user.userRole !== userRolesLower.admin && this.state.user.userRole !== userRolesLower.buyer) && (
                  <div className="change-password-card-userprofile">
                    <Card
                      style={{
                        borderRadius: "10px",
                        border: "0",
                        backgroundColor: "transparent"
                      }}
                      small
                      className=""
                      bordered={false}
                    >
                      <h6 className="purchase-order-form-headings secondary-font-family primary-color">
                        {this.props.t("userInfo.accountSync.title")}
                      </h6>

                      <Row>
                        <Col>
                          <Form>
                            <div className="channel-markup-row">
                              <label className="account-sync-label">
                                {this.props.t("userInfo.accountSync.payments.title")}
                              </label>
                              <div className="account-sync-button-row">
                                <Button
                                  style={{ margin: "5px" }}
                                  type="primary"
                                  className="custom-sync-btn stylefont-weight-bold secondary-bg-color"
                                // onClick={() => this.changePass(context.user.email)}
                                >
                                  {this.props.t(
                                    "userInfo.accountSync.payments.bankAccount"
                                  )}
                                </Button>
                                <Button
                                  style={{ margin: "5px" }}
                                  type="primary"
                                  className="custom-sync-btn stylefont-weight-bold secondary-bg-color"
                                // onClick={() => this.changePass(context.user.email)}
                                >
                                  {this.props.t(
                                    "userInfo.accountSync.payments.paypal"
                                  )}
                                </Button>
                              </div>
                            </div>
                            <div className="channel-markup-row">
                              <label className="account-sync-label">
                                {this.props.t("userInfo.accountSync.ebay.title")}
                              </label>
                              <div className="account-sync-button-row">
                                <Button
                                  style={{ margin: "5px" }}
                                  type="primary"
                                  className="custom-sync-btn stylefont-weight-bold secondary-bg-color"
                                // onClick={() => this.changePass(context.user.email)}
                                >
                                  {this.props.t(
                                    "userInfo.accountSync.ebay.userAccount"
                                  )}
                                </Button>
                                <Button
                                  style={{ margin: "5px" }}
                                  type="primary"
                                  className="custom-sync-btn stylefont-weight-bold secondary-bg-color"
                                // onClick={() => this.changePass(context.user.email)}
                                >
                                  {this.props.t(
                                    "userInfo.accountSync.ebay.developerAccount"
                                  )}
                                </Button>
                              </div>
                            </div>
                            <div className="channel-markup-row">
                              <label className="account-sync-label">
                                {this.props.t("userInfo.accountSync.Amazon.title")}
                              </label>
                              <div className="account-sync-button-row">
                                <Button
                                  style={{ margin: "5px" }}
                                  type="primary"
                                  className="custom-sync-btn stylefont-weight-bold secondary-bg-color"
                                // onClick={() => this.changePass(context.user.email)}
                                >
                                  {this.props.t(
                                    "userInfo.accountSync.Amazon.syncAmazon"
                                  )}
                                </Button>
                              </div>
                            </div>
                            <div className="channel-markup-row">
                              <label className="account-sync-label">
                                {this.props.t("userInfo.accountSync.Stockx.title")}
                              </label>
                              <div className="account-sync-button-row">
                                <Button
                                  style={{ margin: "5px" }}
                                  type="primary"
                                  className="custom-sync-btn stylefont-weight-bold secondary-bg-color"
                                // onClick={() => this.changePass(context.user.email)}
                                >
                                  {this.props.t(
                                    "userInfo.accountSync.Stockx.syncStockx"
                                  )}
                                </Button>
                              </div>
                            </div>
                          </Form>
                        </Col>
                      </Row>
                    </Card>
                  </div>
                )}
              </div>
            </div>
            {/* 4th card ends */}
            <Modal show={this.state.userInfoModal} onHide={() => this.setState({ userInfoModal: false, modalResponse: '' })} backdrop="static">
              <Modal.Header closeButton>
                <Modal.Title>{this.state.userPageTitle}</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <h4 style={{ textAlign: 'center' }} >{this.state.modalResponse}</h4>
              </Modal.Body>
              <Modal.Footer className="modal-btn-align">
                <Button className="btn btn-success info-btn-listing" onClick={(e) => this.setState({ userInfoModal: false, modalResponse: '' })} >OK</Button>
              </Modal.Footer>
            </Modal>

            {/* Confirm Modal */}
            <Modal show={this.state.userConfirmModal} onHide={() => this.CloseConfirmModal()} backdrop="static">
              <Modal.Header closeButton>
                <Modal.Title>{this.state.userPageTitle}</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <h4 style={{ textAlign: 'center' }} >{this.state.modalResponse}</h4>
              </Modal.Body>
              <Modal.Footer className="modal-btn-align">
              <Button className="btn btn-secondary info-btn-listing" onClick={(e) => this.CloseConfirmModal()} >Cancel</Button>
              <Button className="btn btn-success info-btn-listing" onClick={(e) => this.handlFormSubmit()} >Submit</Button>
              </Modal.Footer>
            </Modal>
            {/* Confirm Modal */}
            
          </div>
        )}
      </UserContext.Consumer>
    ) : (
      <Loader />
    );
  }
}

EditAccountAsAdmin.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

// Note: The normal "this.props.t()" won't work here.
EditAccountAsAdmin.defaultProps = {
  title: (
    <Translation>
      {(t, { i18n }) => {
        t("userInfo.details");
      }}
    </Translation>
  )
};

export default withTranslation()(EditAccountAsAdmin);
