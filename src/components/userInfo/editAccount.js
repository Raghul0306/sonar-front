import React, { Component } from "react";
import PropTypes from "prop-types";
import UserContext from "../../contexts/user.jsx";
import "../../assets/formik-form-style.css";
import { Formik, Field, Form, ErrorMessage } from "formik";
import loadable from "@loadable/component";
import Loader from "../Loader/Loader";
import * as Yup from 'yup';
import { Link } from "react-router-dom";
// import "./form.css";
import {
  updateUser,
  updateUserPassword,
  checkUserPassword,
  getCountries,
  getStates,
  getCities,
  getUserChannelFee,
  addChannelFees
} from "../../services/userService";
import Cookies from "universal-cookie";
import { Row, Col } from "shards-react";
// eslint-disable-next-line

import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Switch from 'antd/es/switch';
import Modal from 'antd/es/modal';
import Input from 'antd/es/input';

import CloseRoundedIcon from "@material-ui/icons/CloseRounded";
import { withTranslation, Translation } from "react-i18next";
import Select from 'react-select'
import {
  getAllChannels,
  updateAllUserListingsEbayMarkup,
  updateAllUserListingsEbayMarkupType,
  updateAllUserListingsPhunnelMarkup,
  updateAllUserListingsPhunnelMarkupType,
  updateAllUserListingsAmazonMarkup,
  updateAllUserListingsAmazonMarkupType,
  updateAllUserListingsStockxMarkup,
  updateAllUserListingsStockxMarkupType,
  updateAllUserListingsAmazonMarkupActive,
  updateAllUserListingsEbayMarkupActive,
  updateAllUserListingsPhunnelMarkupActive,
  updateAllUserListingsStockxMarkupActive
} from "../../services/listingsService";

import ConnectBankButton from "../purchases/ConnectBankButton";
import { userRolesLower, listingConst, channels } from "../../constants/constants";
import *  as bootModal from 'react-bootstrap';

// icon style
import styled from 'styled-components'
import { Add } from '@styled-icons/material-rounded/Add';
import { Minus } from '@styled-icons/heroicons-solid/Minus';
import { IosArrowLeft } from '@styled-icons/fluentui-system-filled/IosArrowLeft';
import UpLoadIcon from "../../assets/file-upload.png";
import lodashGet from 'lodash/get';
import lodashFind from 'lodash/find';
import lodashMap from 'lodash/map';

const BootstrapModal = bootModal.Modal;

const Leftarrow = styled(IosArrowLeft)`
  color: #2C91EE;
  width:18px;
  height:18px;
`;
const AddIcon = styled(Add)`
color: #2C91EE;
width:30px;
height:30px;
`
const MinusIcon = styled(Minus)`
color: #BEBEBE;
width:30px;
height:30px;
`
const cookies = new Cookies();
const PageTitle = loadable(() => import("../common/PageTitle"));
export class EditAccount extends Component {
  static contextType = UserContext;
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      channelManage: [],
      channelManagmentInputList: [],
      countryList: "",
      stateList: [],
      cityList: [],
      selectedCountry: "",
      selectedCountryDropDown: "",
      selectedState: "",
      selectedStateDropDown: "",
      selectedCity: "",
      selectedCityDropDown: "",
      pass: "",
      passConfirm: "",
      ebayMarkupValue: "",
      ebayMarkupType: false,
      phunnelMarkupValue: "",
      phunnelMarkupType: false,
      amazonMarkupValue: "",
      amazonMarkupType: false,
      stockxMarkupValue: "",
      stockxMarkupType: false,
      ebayMarkupActive: false,
      amazonMarkupActive: false,
      phunnelMarkupActive: true,
      stockxMarkupActive: false,
      showEbayDevModal: false,
      clientIdInput: "",
      clientSecretInput: "",
      redirectURIInput: "",
      ebaySettingDuration: "",
      ebaySettingPaymentpolicy: "",
      ebaySettingShippingpolicy: "",
      ebaySettingReturnpolicy: "",
      ebaySettingDimensions: { a: "", b: "", c: "" },
      ebaySettingPackagetype: "",
      ebaySettingIrregularpackage: false,
      ebaySettingWeight: "",
      ebaySettingtheme: "",
      ebayPaymentPolicyModal: false,
      ebayShippingPolicyModal: false,
      ebayReturnPolicyModal: false,
      modalResponse: '',
      userInfoModal: false,
      userPageTitle: '',
      contextDetails: {},
      userDetails: {},
      passwordConfirmationModal: false,
      passwordDetails: {
        password: "",
        passwordCurrent: "",
        user: ""
      },
      userContextDetails: {},
      userConfirmModal: false,
      formSubmit: '',
      userInfoConfirmationModal: false,
      userAccountInfo:'',
      userContext:''
    };
    this.handlecurrentPasswordChange = this.handlecurrentPasswordChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handlePasswordConfirmChange = this.handlePasswordConfirmChange.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleStateChange = this.handleStateChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.dropdownFilterOption = this.dropdownFilterOption.bind(this);
    this.onLoadCountryDropDown = this.onLoadCountryDropDown.bind(this);
    this.onLoadStateDropDown = this.onLoadStateDropDown.bind(this);
    this.onLoadCityDropDown = this.onLoadCityDropDown.bind(this);
    this.changeContextData = this.changeContextData.bind(this);
    this.onLoadGetChannels = this.onLoadGetChannels.bind(this);
    this.channelManagmentOnchange = this.channelManagmentOnchange.bind(this);
    this.handlChannelFeeSubmit = this.handlChannelFeeSubmit.bind(this);
    this.handlFormSubmit = this.handlFormSubmit.bind(this);
    this.avoidOnchange = this.avoidOnchange.bind(this);
  }
  async componentDidMount() {
    await this.onLoadGetChannels();
    await this.onLoadCountryDropDown();
    await this.onLoadStateDropDown();
    await this.onLoadCityDropDown();

    await this.setState({ userContextDetails: this.context.user });


    this.setHistoryPageTitle();
    this.setState({ loading: false });
  }
  setHistoryPageTitle = () => {
    if (!this.props.history.location.pageTitle) {
      this.props.history.push({ pageTitle: 'User Profile', pageSubTitle: 'subtitle', redirectPath: 'home' });
    }
  }
  async componentDidUpdate() {
    this.setHistoryPageTitle();
  }
  async onLoadGetChannels() {
    const getChannels = await getAllChannels();
    const getChannelFee = await getUserChannelFee(this.context.user.id);

    getChannels && getChannels.length > 0
      && getChannels.map((channelitem) => {
        var data = {
          "channel_id": channelitem.id,
          "channel_name": channelitem.channel_name,
          "markup_amount": "0"
        }
        this.setState({ channelManagmentInputList: [...this.state.channelManagmentInputList, data] })
      });
    let updatedChannelManagmentInputList = lodashMap(this.state.channelManagmentInputList, ch => {
      ch.markup_amount = lodashGet(lodashFind(lodashGet(getChannelFee, 'channelFeesJSON', []), cf => cf.channel_id == ch.channel_id), 'markup_amount') || 0;
      return ch;
    });
    this.setState({ channelManagmentInputList: updatedChannelManagmentInputList });
  }


  async handlFormSubmit() {

    const { channelManagmentInputList } = this.state;
    if (channelManagmentInputList.length > 0) {
      await addChannelFees(this.context.user.id, this.context.user.id, channelManagmentInputList);
      await this.CloseConfirmModal();
    }

  }

  async avoidOnchange(e) {
    if (e.nativeEvent.data == 'e' || e.key == 'e') {
      e.preventDefault();
    }
  }
  async channelManagmentOnchange(e, statearraynumber, userRole) {
    var statearrayvalue = e.target.value;
    let maxLength = listingConst.maxLength_percent;
    if (statearrayvalue.length <= maxLength && (e.nativeEvent.data != 'e' || e.key != 'e')) {//avoid length of the value more than specified 
      let updatedChanelInputList = this.state.channelManagmentInputList;
      if(userRole == userRolesLower.super_super_admin || userRole == userRolesLower.super_admin || userRole == userRolesLower.admin ){
        updatedChanelInputList[statearraynumber].markup_amount = statearrayvalue;
      }else{
        updatedChanelInputList[statearraynumber].markup_amount = updatedChanelInputList[statearraynumber].markup_amount;
      }
      await this.setState({
        channelManagmentInputList: updatedChanelInputList
      });

    }
  }

  async onLoadCountryDropDown() {
    let countries = await getCountries();
    if (countries) {
      const countriesList = [];
      countries.length && countries.map(items => {
        if (this.context.user.country === items.id) {
          this.setState({
            selectedCountryDropDown: { label: items.name, value: items.id }
          });
        }
        countriesList.push({ label: items.name, value: items.id },)
      }
      )
      await this.setState({
        countryList: countriesList
      });
    }
    if (this.context.user.country) {
      await this.setState({
        selectedCountry: this.context.user.country
      });
    } else {
      if (countries) {
        await this.setState({
          selectedCountry: countries[0].id
        });
      }
    }
  }

  async onLoadStateDropDown() {

    let states = (this.context.user && this.context.user.country) ? await getStates(this.context.user.country) : '';
    if (states) {
      const stateList = [];
      states.length && states.map(items => {
        if (this.context.user.state === items.id) {
          this.setState({ selectedStateDropDown: { label: items.name, value: items.id }, selectedState: items.id });
        }
        stateList.push({ label: items.name, value: items.id })
      });
      await this.setState({
        stateList: stateList
      });
    }

  }

  async onLoadCityDropDown() {
    let cities = '';
    if (this.context.user && this.context.user.country && this.context.user.state) {
      cities = await getCities(this.context.user.country, this.context.user.state);
    }
    if (cities) {
      const cityList = [];
      cities.length && cities.map(items => {

        if (this.context.user.city === items.id) {
          this.setState({ selectedCityDropDown: { label: items.name, value: items.id }, selectedCity: items.id });
        }
        cityList.push({ label: items.name, value: items.id })
      });
      await this.setState({
        cityList: cityList
      });
    }
  }


  async submitUserChange(user, context) {
    let userUpdate = await updateUser(user);
    const userdata = { ...context.user, ...user }
    context.setUser(userdata);
    cookies.set("userObject", userdata, { path: "/" });
    this.setState({userInfoConfirmationModal: false})
    return userUpdate;
  }
  async CloseConfirmModal() {
    await this.setState({ userConfirmModal: false, modalResponse: '', formSubmit: '' })
  }
  async changeContextData() {
    let context = this.state.contextDetails;
    let user = this.state.userDetails;
    if (context && user) {
      const userdata = { ...context.user, ...user }
      context.setUser(userdata);
      cookies.set("userObject", userdata, { path: "/" });
    }
    await this.setState({ userInfoModal: false, modalResponse: '' })

  }


  setShowEbayDevModal(visible) {
    this.setState({ showEbayDevModal: visible });
  }
  setClientIdInput(newValue) {
    this.setState({ clientIdInput: newValue });
  }
  setClientSecretInput(newValue) {
    this.setState({ clientSecretInput: newValue });
  }
  setRedirectURIInput(newValue) {
    this.setState({ redirectURIInput: newValue });
  }
  async handleEbayDeveloperSubmit(user) {

    // write the recorded ebay developer values to the DB
    user.ebayClientId = this.state.clientIdInput;
    user.ebayClientSecret = this.state.clientSecretInput;
    user.ebayRedirectUri = this.state.redirectURIInput;
    // remove unsavory values before DB upload
    delete user.fullName;
    await updateUser(user);
    cookies.set("userObject", user, { path: "/" });
    this.setShowEbayDevModal(false);
  }

  async changePasswordConfirm(password, passwordCurrent, user) {
    let passwordDetails = this.state.passwordDetails;
    passwordDetails['password'] = password;
    passwordDetails['passwordCurrent'] = passwordCurrent;
    passwordDetails['user'] = user;
    await this.setState({ passwordConfirmationModal: true, passwordDetails: passwordDetails });

  }

  async changePass() {
    let passwordDetails = this.state.passwordDetails;

    let checkPassword = await checkUserPassword(passwordDetails.user, passwordDetails.passwordCurrent);
    if (checkPassword && checkPassword.id) {
      let userPassUpdate = await updateUserPassword(passwordDetails.user, passwordDetails.password);

      if (userPassUpdate.email) {
        this.setState({ modalResponse: this.props.t("changePassword.successbyAdmin"), currentpass: '', pass: '', passConfirm: '', userInfoModal: true, userPageTitle: this.props.t("changePassword.title"), passwordDetails: {}, passwordConfirmationModal: false })
      }
      else {
        this.setState({ modalResponse: this.props.t("changePassword.errorbyAdmin"), currentpass: '', pass: '', passConfirm: '', userInfoModal: true, userPageTitle: this.props.t("changePassword.title"), passwordDetails: {}, passwordConfirmationModal: false });
      }
    }
    else {
      this.setState({ modalResponse: this.props.t("changePassword.errorMismatch"), userInfoModal: true, userPageTitle: this.props.t("changePassword.title"), passwordConfirmationModal: false });
    }
  }
  handlecurrentPasswordChange(event) {
    this.setState({ currentpass: event.target.value });
  }

  async handleCountryChange(country) {
    if (country) {
      this.setState({
        selectedCountryDropDown: country,
        selectedCountry: country.value,
        selectedState: "",
        selectedStateDropDown: [],
        stateList: [], cityList: [],
        selectedCity: "",
        selectedCityDropDown: []
      });

      let states = await getStates(country.value && country.value.length ? country.value : '');
      if (states) {
        const stateList = [];
        states.length && states.map(items => {
          stateList.push({ label: items.name, value: items.id })
        });
        await this.setState({
          stateList: stateList
        });
      }

    } else {
      this.setState({
        selectedCountryDropDown: country,
        selectedCountry: "",

        selectedState: "",
        selectedStateDropDown: null,
        stateList: [], cityList: [],
        selectedCity: "",
        selectedCityDropDown: null
      });
    }
  }

  async handleStateChange(state) {
    if (state) {
      this.setState({
        selectedStateDropDown: state,
        selectedState: state.value,
        selectedCity: "",
        selectedCityDropDown: null
      });
      let cities = await getCities(this.state.selectedCountry, state.value);
      if (cities) {
        const cityList = [];
        cities.length && cities.map(items => {
          cityList.push({ label: items.name, value: items.id })
        });
        await this.setState({
          cityList: cityList
        });
      }
    } else {
      this.setState({
        selectedStateDropDown: [],
        selectedState: "",

        selectedCity: "",
        selectedCityDropDown: null
      });
    }
  }
  async handleCityChange(city) {
    if (city) {
      this.setState({
        selectedCity: city.value,
        selectedCityDropDown: city
      });
    } else {
      this.setState({
        selectedCityDropDown: [],
        selectedCity: ""
      });
    }
  }
  dropdownFilterOption = ({ label, value, data }, string) => {
    if (this.state.value1 === data) {
      return false;
    } else if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };
  handlePasswordChange(event) {
    this.setState({ pass: event.target.value });
  }
  handlePasswordConfirmChange(event) {
    this.setState({ passConfirm: event.target.value });
  }
  /**
   * This function handles updating all associated user listings when there
   * is new global markup information.
   * TODO: Uncomment the necessary imports at the beginning of the file.
   * @param {*} event
   * @param {*} userID
   */
  async handleMarkupSubmit(event, userID) {
    const {
      ebayMarkupValue,
      ebayMarkupType,
      phunnelMarkupValue,
      phunnelMarkupType,
      amazonMarkupValue,
      amazonMarkupType,
      stockxMarkupValue,
      stockxMarkupType,
      ebayMarkupActive,
      amazonMarkupActive,
      phunnelMarkupActive,
      stockxMarkupActive
    } = this.state;

    if (ebayMarkupValue !== "") {
      updateAllUserListingsEbayMarkup(userID, ebayMarkupValue);
    }

    if (phunnelMarkupValue !== "") {

      updateAllUserListingsPhunnelMarkup(userID, phunnelMarkupValue);
    }

    if (amazonMarkupValue !== "") {
      updateAllUserListingsAmazonMarkup(userID, amazonMarkupValue);
    }

    if (stockxMarkupValue !== "") {
      updateAllUserListingsStockxMarkup(userID, stockxMarkupValue);
    }

    if (ebayMarkupType) {
      updateAllUserListingsEbayMarkupType(userID, "$");
    } else {
      updateAllUserListingsEbayMarkupType(userID, "%");
    }

    if (phunnelMarkupType) {
      updateAllUserListingsPhunnelMarkupType(userID, "$");
    } else {
      updateAllUserListingsPhunnelMarkupType(userID, "%");
    }

    if (amazonMarkupType) {
      updateAllUserListingsAmazonMarkupType(userID, "$");
    } else {
      updateAllUserListingsAmazonMarkupType(userID, "%");
    }

    if (stockxMarkupType) {
      updateAllUserListingsStockxMarkupType(userID, "$");
    } else {
      updateAllUserListingsStockxMarkupType(userID, "%");
    }

    updateAllUserListingsAmazonMarkupActive(userID, amazonMarkupActive);

    updateAllUserListingsPhunnelMarkupActive(userID, phunnelMarkupActive);

    updateAllUserListingsEbayMarkupActive(userID, ebayMarkupActive);
    // update the user with the new ebay markup active setting

    updateAllUserListingsStockxMarkupActive(userID, stockxMarkupActive);

    this.setState({ modalResponse: this.props.t("userInfo.updatedMarkup"), userPageTitle: this.props.t("channelMarkups.title"), userInfoModal: true });
  }

  async inputUserAccount(e) { //User Account fields add/update    
    let userValues = this.state.userContextDetails;
    if (userValues && e) {
      userValues[e.target.name] = e.target.value;
    }
    await this.setState({ userContextDetails: userValues })
  }
  async handlChannelFeeSubmit() {
    this.setState({ modalResponse: this.props.t("userInfo.channelFee.confirmMessage"), userPageTitle: this.props.t("userInfo.channelFee.modalTitle"), userConfirmModal: true, formSubmit: this.props.t("userInfo.formSubmit.channelFee") });
  }

  render() {


    let submitUserChange = this.submitUserChange;
    return !this.state.loading ? (
      <UserContext.Consumer>
        {context => (


          <div className="container-userprofile pl-db-50">
            <div style={{ display: "flex", alignItems: "center", width: "100%" }}>
              <Link to="/listings">
                {/* <button className="back-btn btn"><Leftarrow /></button> */}
              </Link>
              {/* <PageTitle
                title={"User Profile"}
                className="text-sm-left mb-3"
              /> */}
            </div>
            {/* <h1 className="card-heading-userprofile">User Profile</h1> */}
            <div className="small-container-userprofile user-card-section">
              <div className="user-account-card-userprofile">
                {/* Update user start */}
                <Formik
                  initialValues={{
                    firstName: this.state.userContextDetails.firstName,
                    lastName: this.state.userContextDetails.lastName,
                    companyName: this.state.userContextDetails.companyName,
                    id: context.user.id,
                    phone: this.state.userContextDetails.phone && this.state.userContextDetails.phone != 0 ? this.state.userContextDetails.phone : '',
                    email: context.user.email,
                    streetAddress: this.state.userContextDetails.streetAddress,
                    streetAddress2: this.state.userContextDetails.streetAddress2,
                    state: this.state.selectedState,
                    city: this.state.selectedCity,
                    country: this.state.selectedCountry,
                    postalCode: this.state.userContextDetails.postalCode && this.state.userContextDetails.postalCode != 0 ? this.state.userContextDetails.postalCode : '',
                  }}

                  enableReinitialize={true}

                  validationSchema={Yup.object().shape({

                    firstName: Yup.string()
                      .required('First Name is required')
                      .min(3, 'First Name should be a minimum of 3 characters'),

                    lastName: Yup.string()
                      .required('Last Name is required'),

                    companyName: Yup.string().nullable()
                      .required('Company Name is required'),

                    phone: Yup.string()
                      .required("Phone number is required")
                      .matches(/^[0-9]+$/, "Phone number must be only digits")
                      .min(10, 'Phone number should be a minimum of 10 digits')
                      .max(15, 'Phone number should be a maximum of 15 digits'),


                    streetAddress: Yup.string().nullable()
                      .required('Street Address is required'),

                    country: Yup.string()
                      .required('Country is required'),

                    state: Yup.string()
                      .required('State is required'),

                    city: Yup.string()
                      .required('City is required'),

                    postalCode: Yup.string()
                      .required("Zip Code is required")
                      .matches(/^[a-zA-Z0-9]+$/, "ZipCode must be only alphanumerics")
                      .min(3, 'ZipCode should be a minimum of 3 alphanumerics')
                      .max(8, 'ZipCode should be a maximum of 8 alphanumerics'),

                  })}


                  onSubmit={async fields => {
                    await this.setState({ userInfoConfirmationModal: true, userAccountInfo: fields, userContext: context });
                    // submitUserChange(fields, context).then(async (user) => {
                    //   await this.setState({ modalResponse: this.props.t("userInfo.submitSuccess2"), userPageTitle: this.props.t("userProfile.title"), userInfoModal: true, contextDetails: context, userDetails: user });
                    // });
                  }}

                  render={({ errors, status, touched, values }) => (

                    <Card
                      style={{
                        borderRadius: "10px"
                      }}
                      small
                      className=""
                      bordered={false}
                    >
                      <h6 className="purchase-order-form-headings secondary-font-family primary-color">
                        {this.props.t("userInfo.title")}
                      </h6>


                      <Form>
                        <Row>
                          <Col md="3">
                            <label
                              htmlFor="firstName"
                              className="form-label-userprofile"
                            >
                              {this.props.t("userInfo.firstName")}
                            </label>
                            <div className="form-group">
                              <Field name="firstName" className={'formik-input-user-form form-control' + (errors.firstName && touched.firstName ? ' is-invalid' : '')} placeholder="First Name" autoComplete="off" onChange={(e) => this.inputUserAccount(e)} value={values.firstName} />
                              <ErrorMessage name="firstName" component="div" className="invalid-feedback" />
                            </div>
                          </Col>


                          <Col md="3">
                            <label
                              htmlFor="lastName"
                              className="form-label-userprofile"
                            >
                              {this.props.t("userInfo.lastName")}
                            </label>

                            <div className="form-group">
                              <Field name="lastName" className={'formik-input-user-form form-control' + (errors.lastName && touched.lastName ? ' is-invalid' : '')} placeholder="Last Name" autoComplete="off" onChange={(e) => this.inputUserAccount(e)} value={values.lastName} />
                              <ErrorMessage name="lastName" component="div" className="invalid-feedback" />
                            </div>
                          </Col>
                          <Col md="3">
                            <label
                              htmlFor="email"
                              className="form-label-userprofile"
                            >
                              {this.props.t("userInfo.email")}
                            </label>
                            <div className="form-group">
                              <Field name="email" readonly="readOnly" className={'formik-input-user-form form-control' + (errors.email && touched.email ? ' is-invalid' : '')} placeholder="Email Address" autoComplete="off" />
                              <ErrorMessage name="email" component="div" className="invalid-feedback" />
                            </div>

                          </Col>


                          <Col md="3">
                            <label
                              htmlFor="phone"
                              className="form-label-userprofile"
                            >
                              {this.props.t("userInfo.phone")}
                            </label>
                            <div className="form-group">
                              <Field name="phone" className={'formik-input-user-form form-control' + (errors.phone && touched.phone ? ' is-invalid' : '')} placeholder="Phone Number" autoComplete="off" onChange={(e) => this.inputUserAccount(e)} value={values.phone} />
                              <ErrorMessage name="phone" component="div" className="invalid-feedback" />
                            </div>
                          </Col>
                        </Row>


                        <Row>
                          <Col md="6">
                            <label
                              htmlFor="companyName"
                              className="form-label-userprofile"
                            >
                              Address
                            </label>
                            <div className="form-group">
                              <Field name="streetAddress" className={'formik-input-user-form form-control' + (errors.streetAddress && touched.streetAddress ? ' is-invalid' : '')} placeholder="Street Address1" autoComplete="off" onChange={(e) => this.inputUserAccount(e)} value={values.streetAddress} />
                              <ErrorMessage name="streetAddress" component="div" className="invalid-feedback" />
                            </div>
                            <label
                              htmlFor="companyName"
                              className="form-label-userprofile"
                            >
                              Address 2
                            </label>
                            <div className="form-group">
                              <Field name="streetAddress2" className={'formik-input-user-form form-control' + (errors.streetAddress2 && touched.streetAddress2 ? ' is-invalid' : '')} placeholder="Street Address2" autoComplete="off" onChange={(e) => this.inputUserAccount(e)} value={values.streetAddress2} />
                              <ErrorMessage name="streetAddress2" component="div" className="invalid-feedback" />
                            </div>
                          </Col>


                          <Col md="6">
                            <Row>

                              <Col md="6">
                                <label
                                  htmlFor="country"
                                  className="form-label-userprofile"
                                >
                                  {this.props.t("userInfo.address.country")}
                                </label>
                                <div className="form-group">
                                  <Select
                                    {...this.props}
                                    placeholder={this.props.t("userInfo.address.country")}
                                    style={{ width: "100%" }}
                                    name="country" autoComplete="off"
                                    filterOption={this.dropdownFilterOption}
                                    value={this.state.selectedCountryDropDown ? this.state.selectedCountryDropDown : ""}
                                    options={this.state.countryList}
                                    onChange={e => this.handleCountryChange(e)}
                                    className={(errors.country && touched.country ? ' is-invalid' : '')}
                                    allowClear
                                    showSearch
                                    isClearable
                                  />
                                  <ErrorMessage name="country" component="div" className="invalid-feedback" />
                                </div>
                              </Col>

                              <Col md="6">
                                <label
                                  htmlFor="city"
                                  className="form-label-userprofile"
                                >
                                  {this.props.t("userInfo.address.city")}
                                </label>
                                <div className="form-group">
                                  <Select
                                    {...this.props}
                                    placeholder={this.props.t("userInfo.address.city")}
                                    style={{ width: "100%" }}
                                    name="city" autoComplete="off"
                                    filterOption={this.dropdownFilterOption}
                                    value={this.state.selectedCityDropDown ? this.state.selectedCityDropDown : ""}
                                    options={this.state.cityList}
                                    onChange={e => this.handleCityChange(e)}
                                    className={(errors.city && touched.city ? ' is-invalid' : '')}
                                    allowClear
                                    showSearch
                                    isClearable
                                  />
                                  <ErrorMessage name="city" component="div" className="invalid-feedback" />
                                </div>
                              </Col>


                              <Col md="6">
                                <label
                                  htmlFor="state"
                                  className="form-label-userprofile"
                                >
                                  {this.props.t("userInfo.address.state")}
                                </label>
                                <div className="form-group">
                                  <Select
                                    {...this.props}
                                    placeholder={this.props.t("userInfo.address.state")}
                                    style={{ width: "100%" }}
                                    name="state" autoComplete="off"
                                    filterOption={this.dropdownFilterOption}
                                    value={this.state.selectedStateDropDown ? this.state.selectedStateDropDown : ""}
                                    options={this.state.stateList}
                                    onChange={e => this.handleStateChange(e)}
                                    className={(errors.state && touched.state ? ' is-invalid' : '')}
                                    allowClear
                                    showSearch
                                    isClearable
                                  />
                                  <ErrorMessage name="state" component="div" className="invalid-feedback" />
                                </div>
                              </Col>
                              <Col md="6">
                                <label
                                  htmlFor="postalCode"
                                  className="form-label-userprofile"
                                >
                                  {this.props.t("userInfo.address.zip")}
                                </label>
                                <div className="form-group">
                                  <Field name="postalCode" className={'formik-input-user-form form-control' + (errors.postalCode && touched.postalCode ? ' is-invalid' : '')} placeholder="Zip Code" autoComplete="off" onChange={(e) => this.inputUserAccount(e)} value={values.postalCode} />
                                  <ErrorMessage name="postalCode" component="div" className="invalid-feedback" />
                                </div>
                              </Col>



                            </Row>
                          </Col>
                        </Row>
                        <Row>
                          <Col md="6">
                            <label
                              htmlFor="companyName"
                              className="form-label-userprofile"
                            >
                              {this.props.t("userInfo.companyName")}
                            </label>

                            <div className="form-group">
                              <Field name="companyName" className={'formik-input-user-form form-control' + (errors.companyName && touched.companyName ? ' is-invalid' : '')} placeholder="Company Name" readonly="readOnly" />
                              <ErrorMessage name="companyName" component="div" className="invalid-feedback" />
                            </div>

                          </Col>
                          <Col md="6">
                            <div className="form-group text-center" style={{ marginTop: "30px" }}>
                              <button type="submit" className="user-info-update-btn stylefont-weight-medium primary-bg-color" >{this.props.t("userInfo.submit")}</button>
                            </div>
                          </Col>
                        </Row>



                      </Form>

                    </Card>
                  )}
                />
                {/* Update user end */}



              </div>
              {/* second card starts */}
              <div className="change-password-card-userprofile">


                {/* Change password start */}
                <Formik
                  initialValues={{
                    password: this.state.pass,
                    passwordConfirm: this.state.passConfirm,
                    passwordCurrent: this.state.currentpass,
                  }}
                  enableReinitialize={true}
                  validationSchema={Yup.object().shape({

                    passwordCurrent: Yup.string()
                      .required('Current Password is required'),

                    password: Yup.string()
                      .required('Password is required')
                      .matches(
                        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
                        "Must Contain 8 Characters, at least 1 Uppercase, 1 Lowercase, 1 Number and 1 Special Character"
                      ),

                    passwordConfirm: Yup.string()
                      .required('Confirm password is required')
                      .oneOf([Yup.ref('password'), null], 'Passwords must match')


                  })}

                  onSubmit={async fields => {
                    await this.changePasswordConfirm(fields.password, fields.passwordCurrent, context.user.email);
                  }}

                  render={({ errors, status, touched }) => (
                    <Card
                      style={{
                        borderRadius: "10px"
                      }}
                      small
                      className=""
                      bordered={false}
                    >
                      <h6 className="purchase-order-form-headings secondary-font-family primary-color">
                        {this.props.t("userInfo.password.change")}
                      </h6>
                      <Form>

                        <Row>
                          <Col md="3">
                            <label
                              htmlFor="passwordCurrent"
                              className="form-label-userprofile">
                              {this.props.t("userInfo.password.current")}
                            </label>
                            <div className="form-group">
                              <Field name="passwordCurrent" type="password" className={'formik-input-user-form form-control' + (errors.passwordCurrent && touched.passwordCurrent ? ' is-invalid' : '')} placeholder="Current Password" onChange={(e) => this.handlecurrentPasswordChange(e)} value={this.state.currentpass} />
                              <ErrorMessage name="passwordCurrent" component="div" className="invalid-feedback" />
                            </div>
                          </Col>
                          <Col md="3">
                            <label
                              htmlFor="password"
                              className="form-label-userprofile"
                            >
                              {this.props.t("userInfo.password.new")}
                            </label>
                            <div className="form-group">
                              <Field name="password" type="password" className={'formik-input-user-form form-control' + (errors.password && touched.password ? ' is-invalid' : '')} placeholder="New Password" onChange={(e) => this.handlePasswordChange(e)} value={this.state.pass} />
                              <ErrorMessage name="password" component="div" className="invalid-feedback" />
                            </div>
                          </Col>
                          <Col md="3">
                            <label
                              htmlFor="passwordConfirm"
                              className="form-label-userprofile"
                            >
                              {this.props.t("userInfo.password.confirm")}
                            </label>

                            <div className="form-group">
                              <Field name="passwordConfirm" type="password" className={'formik-input-user-form form-control' + (errors.passwordConfirm && touched.passwordConfirm ? ' is-invalid' : '')} placeholder="Confirm New Password" value={this.state.passConfirm} onChange={(e) => this.handlePasswordConfirmChange(e)} />
                              <ErrorMessage name="passwordConfirm" component="div" className="invalid-feedback" />
                            </div>
                          </Col>
                          <Col md="3">
                            <div className="form-group" style={{ marginTop: "30px" }}>
                              <button type="submit" className="user-change-psw-btn primary-bg-color stylefont-weight-bold" >{this.props.t("userInfo.password.submit")}</button>
                            </div>
                          </Col>
                        </Row>
                      </Form>
                    </Card>
                  )}
                />
                {/* Change password end */}
              </div>
            </div>
            {/* second card ends */}
            {/* second card starts */}
            <div className="change-password-card-userprofile market-place-user-section">


              {/* Change password start */}
              <Formik
                initialValues={{ file: null }}
                onSubmit={(values) => {
                  alert(
                    JSON.stringify(
                      {
                        fileName: values.file.name,
                        type: values.file.type,
                        size: `${values.file.size} bytes`
                      },
                      null,
                      2
                    )
                  );
                }}

                render={({ values, handleSubmit, setFieldValue }) => {
                  return (
                    <Card
                      style={{
                        borderRadius: "10px"
                      }}
                      small
                      className=""
                      bordered={false}
                    >
                      <form onSubmit={handleSubmit}>
                        <div className="file-upload-section">
                          <div className="left-content-item">
                            <h4>Upload Images</h4>
                          </div>
                          <div className="form-group form-group-relative">
                            <div className="upload-input">
                              <input id="file" name="file" type="file" onChange={(event) => {
                                setFieldValue("file", event.currentTarget.files[0]);
                              }} className="form-control" />
                              <div className="upload-btn-section">
                                <button className="profile-upload-image"><img src={UpLoadIcon} alt="upload icon" /> Upload Images</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </Card>
                  );
                }} />
              {/* Change password end */}
            </div>

            {/* Ebay  start */}
            <div className="chennel-grid--section">

              {/* Channel Fees Start*/}

              <div className="channel-grid-column">
                <div className="change-password-card-userprofile">
                  <Card
                    style={{
                      borderRadius: "10px"
                    }}
                    small
                    className=""
                    bordered={false}
                  >
                    <h6 className="purchase-order-form-headings secondary-font-family primary-color">
                      {this.props.t("userInfo.channelFee.title")}
                    </h6>

                    <Form>
                      <Row>

                        {this.state.channelManagmentInputList ? this.state.channelManagmentInputList.map((channel, channelCount) => {
                          return (
                            <Col md="12" className="form-group">
                              <div className="channel-markup input-row">
                                <div className="channel-inner-sec">
                                  <div className="channel-grid2-sec">
                                    <div className="chennel-title">
                                      <h3 className="channel-fee-left-title">{channel.channel_name}</h3>
                                    </div>
                                    <div className="channel-input-field">
                                        <div className="channel-fee-change-txt">
                                          <input 
                                          className={`form-control ${channel.channel_name == channels.Phunnel ?'channel-fee-no-edit-field':''}`} 
                                            type="number"
                                            min="0"
                                            max={this.state.channelManage[channelCount] ? this.state.channelManage[channelCount] : 100}
                                            key={'channel_value' + channelCount}
                                            placeholder={'%'}
                                            value={channel.markup_amount}
                                            disabled={channel.channel_name == channels.Phunnel ? true : false}
                                            onKeyPress={(e) => this.avoidOnchange(e)}
                                            onChange={(value) => this.channelManagmentOnchange(value, channelCount, context.user.userRole)}
                                          />
                                        </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </Col>
                            );
                          }
                          ):
                          <Col md="12" className="form-group">
                            <div className="channel-markup input-row">
                            <div className="channel-inner-sec">
                              <div className="channel-grid3-sec">
                                <h4>{this.props.t("userInfo.channelNotFound")}</h4>
                              </div>
                            </div>
                          </div>
                          </Col>
                          }
                        {this.state.channelManagmentInputList?
                        <div className="text-center w-100 mt-4">
                          <Button
                            style={{ margin: "5px" }}
                            type="primary"
                            disabled={context.user.userRole == userRolesLower.super_super_admin || context.user.userRole == userRolesLower.super_admin || context.user.userRole ==  userRolesLower.admin ?false:true}
                            className="channel-submit-btn primary-bg-color stylefont-weight-bold"
                            onClick={e => this.handlChannelFeeSubmit()}
                          >
                            {this.props.t(
                              "userInfo.channelFee.submit"
                            )}
                          </Button>
                      </div>:''}
                      </Row>
                    </Form>
                  </Card>
                </div>

              </div>

              {/* Channel Fees End*/}


              <div className="channel-grid-column">
                <div className="change-password-card-userprofile">
                  <Card
                    style={{
                      borderRadius: "10px"
                    }}
                    small
                    className=""
                    bordered={false}
                  >
                    <h6 className="purchase-order-form-headings secondary-font-family primary-color">
                      {this.props.t("userInfo.channelMarkups.title")}
                    </h6>

                    <Form>
                      <Row>
                        <Col md="12" className="form-group">
                          <div className="channel-markup input-row">
                            <div className="channel-inner-sec">
                              <div className="channel-grid3-sec">
                                <div className="chennel-title">
                                  <h3 className="channel-left-title">Stubhub</h3>
                                </div>
                                <div className="channel-switch">
                                  <div className="d-flex">
                                    <div className="left-switch-item">
                                      <div className="button-switch">
                                        <Switch className="filter-btn-hide--seat"
                                          checkedChildren="ON"
                                          unCheckedChildren="OFF"
                                          defaultChecked
                                        />
                                      </div>
                                    </div>
                                    <div className="right-switch-item">
                                      <div className="button-switch">
                                        <Switch className="price-switch-btn"
                                          checkedChildren="%"
                                          unCheckedChildren="$"
                                          defaultChecked
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="channel-input-field">
                                  <div className="plus-minus-btn">
                                    <div className="price-change-txt">
                                      <input className="form-control" type="number" onKeyPress={(e) => this.avoidOnchange(e)}/>
                                    </div>
                                    <button className="price-add-btn primary-border-color" type="button"><AddIcon /></button>
                                    <button className="price-mini-btn" type="button"><MinusIcon /></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                        <Col md="12" className="form-group">
                          <div className="channel-markup input-row">
                            <div className="channel-inner-sec">
                              <div className="channel-grid3-sec">
                                <div className="chennel-title">
                                  <h3 className="channel-left-title">Phunnel</h3>
                                </div>
                                <div className="channel-switch">
                                  <div className="d-flex">
                                    <div className="left-switch-item">
                                      <div className="button-switch">
                                        <Switch className="filter-btn-hide--seat"
                                          checkedChildren="ON"
                                          unCheckedChildren="OFF"
                                        />
                                      </div>
                                    </div>
                                    <div className="right-switch-item" style={{ visibility: "hidden" }}>
                                      <div className="button-switch">
                                        <Switch className="price-switch-btn"
                                          checkedChildren="%"
                                          unCheckedChildren="$"
                                          defaultChecked
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="channel-input-field">
                                  <div className="plus-minus-btn">
                                    <div className="price-change-txt">
                                      <input className="form-control" type="number" onKeyPress={(e) => this.avoidOnchange(e)}/>
                                    </div>
                                    <button className="price-add-btn inactive primary-border-color" type="button"><AddIcon /></button>
                                    <button className="price-mini-btn" type="button"><MinusIcon /></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                        <Col md="12" className="form-group">
                          <div className="channel-markup input-row">
                            <div className="channel-inner-sec">
                              <div className="channel-grid3-sec">
                                <div className="chennel-title">
                                  <h3 className="channel-left-title">Viagogo</h3>
                                </div>
                                <div className="channel-switch">
                                  <div className="d-flex">
                                    <div className="left-switch-item">
                                      <div className="button-switch">
                                        <Switch className="filter-btn-hide--seat"
                                          checkedChildren="ON"
                                          unCheckedChildren="OFF"
                                          defaultChecked
                                        />
                                      </div>
                                    </div>
                                    <div className="right-switch-item">
                                      <div className="button-switch">
                                        <Switch className="price-switch-btn"
                                          checkedChildren="%"
                                          unCheckedChildren="$"
                                          defaultChecked
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="channel-input-field">
                                  <div className="plus-minus-btn">
                                    <div className="price-change-txt">
                                      <input className="form-control" type="number" onKeyPress={(e) => this.avoidOnchange(e)}/>
                                    </div>
                                    <button className="price-add-btn primary-border-color" type="button"><AddIcon /></button>
                                    <button className="price-mini-btn" type="button"><MinusIcon /></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                        <Col md="12" className="form-group">
                          <div className="channel-markup input-row">
                            <div className="channel-inner-sec">
                              <div className="channel-grid3-sec">
                                <div className="chennel-title">
                                  <h3 className="channel-left-title">Seatgeek</h3>
                                </div>
                                <div className="channel-switch">
                                  <div className="d-flex">
                                    <div className="left-switch-item">
                                      <div className="button-switch">
                                        <Switch className="filter-btn-hide--seat"
                                          checkedChildren="ON"
                                          unCheckedChildren="OFF"
                                        />
                                      </div>
                                    </div>
                                    <div className="right-switch-item">
                                      <div className="button-switch">
                                        <Switch className="price-switch-btn"
                                          checkedChildren="%"
                                          unCheckedChildren="$"
                                          defaultChecked
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="channel-input-field">
                                  <div className="plus-minus-btn">
                                    <div className="price-change-txt">
                                      <input className="form-control" type="number" onKeyPress={(e) => this.avoidOnchange(e)}/>
                                    </div>
                                    <button className="price-add-btn primary-border-color inactive" type="button"><AddIcon /></button>
                                    <button className="price-mini-btn" type="button"><MinusIcon /></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                        <div className="text-center w-100 mt-4">
                          <Button
                            style={{ margin: "5px" }}
                            type="primary"
                            className="channel-submit-btn primary-bg-color stylefont-weight-bold"
                            onClick={event =>
                              this.handleMarkupSubmit(
                                event,
                                context.user.id
                              )
                            }
                          >
                            {this.props.t(
                              "userInfo.channelMarkups.submit"
                            )}
                          </Button>
                        </div>
                      </Row>
                    </Form>
                  </Card>
                </div>

              </div>

              <div className="channel-grid-column">
                {
                  /**
                      * Page : user-profile
                      * Function For : To hide the account sysnc for beoker user
                      * Ticket No : TIC-346
                  */
                }
                {(context.user.userRole !== userRolesLower.admin && context.user.userRole !== userRolesLower.buyer && context.user.userRole !== userRolesLower.broker_user && context.user.userRole !== userRolesLower.marketplace_user) && (
                  <div className="user-account-card-userprofile">
                    <Card
                      style={{
                        borderRadius: "10px",
                        border: "0",
                        backgroundColor: "transparent"
                      }}
                      small
                      className=""
                      bordered={false}
                    >
                      <h6 className="purchase-order-form-headings secondary-font-family primary-color">
                        {this.props.t("userInfo.accountSync.title")}
                      </h6>

                      <Row>
                        <Col>
                          <Form>
                            <div className="channel-markup-row">
                              <label className="account-sync-label">
                                {this.props.t("userInfo.accountSync.payments.title")}
                              </label>
                              <div className="account-sync-button-row">
                                <div style={{ margin: "5px" }}>
                                  <ConnectBankButton />
                                </div>
                                <Button
                                  style={{ margin: "5px" }}
                                  type="primary"
                                  className="custom-sync-btn stylefont-weight-bold secondary-bg-color"
                                // onClick={() => this.changePass(context.user.email)}
                                >
                                  {this.props.t(
                                    "userInfo.accountSync.payments.paypal"
                                  )}
                                </Button>
                              </div>
                            </div>
                            {context.user.userRole !== null && (
                              /* if the user has a refresh token and access token, don't show this, instead show an ebay settings URL*/
                              //(context.user.ebayAccessToken === null && context.user.ebayRefreshToken === null) ? (
                              <div className="channel-markup-row">
                                <label className="account-sync-label">
                                  {this.props.t("userInfo.accountSync.ebay.title")}
                                </label>
                                <div className="account-sync-button-row">
                                  <Button
                                    style={{ margin: "5px" }}
                                    type="primary"
                                    className="custom-sync-btn stylefont-weight-bold secondary-bg-color"
                                    onClick={() => this.setShowEbayDevModal(true)}
                                  >
                                    {this.props.t(
                                      "userInfo.accountSync.ebay.developerAccount"
                                    )}
                                  </Button>
                                  <Button
                                    style={{ margin: "5px" }}
                                    type="primary"
                                    className="custom-sync-btn stylefont-weight-bold secondary-bg-color"
                                  // onClick={async () => {
                                  //   window.open(
                                  //     await getEbayAuthorizationURL(
                                  //       context.user.ebayClientId,
                                  //       context.user.ebayClientSecret,
                                  //       context.user.ebayRedirectUri,
                                  //       process.env.REACT_APP_EBAY_ENVIRONMENT
                                  //     ),
                                  //     "_blank",
                                  //     "noopener, noreferrer"
                                  //   );
                                  // }}
                                  >
                                    {this.props.t(
                                      "userInfo.accountSync.ebay.userAccount"
                                    )}
                                  </Button>
                                </div>
                              </div>
                              //):(
                              //  <p>Ebay Account all synced!</p>
                              //))}
                            )}
                            <div className="channel-markup-row">
                              <label className="account-sync-label">
                                {this.props.t("userInfo.accountSync.Amazon.title")}
                              </label>
                              <div className="account-sync-button-row">
                                <Button
                                  style={{ margin: "5px" }}
                                  type="primary"
                                  className="custom-sync-btn stylefont-weight-bold secondary-bg-color"
                                // onClick={() => this.changePass(context.user.email)}
                                >
                                  {this.props.t(
                                    "userInfo.accountSync.Amazon.syncAmazon"
                                  )}
                                </Button>
                              </div>
                            </div>
                            <div className="channel-markup-row">
                              <label className="account-sync-label">
                                {this.props.t("userInfo.accountSync.Stockx.title")}
                              </label>
                              <div className="account-sync-button-row">
                                <Button
                                  style={{ margin: "5px" }}
                                  type="primary"
                                  className="custom-sync-btn stylefont-weight-bold secondary-bg-color"
                                // onClick={() => this.changePass(context.user.email)}
                                >
                                  {this.props.t(
                                    "userInfo.accountSync.Stockx.syncStockx"
                                  )}
                                </Button>
                              </div>
                            </div>
                          </Form>
                          <Modal
                            title="Ebay Developer Account Sync"
                            visible={this.state.showEbayDevModal}
                            closable={false}
                            onCancel={() => this.setShowEbayDevModal(false)}
                            footer={false}
                            width="40%"
                          >
                            <div className="modalWithHead-floating-cancel-button">
                              <Button
                                shape="circle"
                                size="large"
                                onClick={() => this.setShowEbayDevModal(false)}
                              >
                                <CloseRoundedIcon
                                  style={{
                                    fontSize: "23px"
                                  }}
                                />
                              </Button>
                            </div>
                            <div className="popup-body">
                              <Input
                                placeholder="Enter Client ID"
                                value={this.state.clientIdInput}
                                onChange={e =>
                                  this.setClientIdInput(e.target.value)
                                }
                              />
                              <Input
                                placeholder="Enter Client Secret"
                                value={this.state.clientSecretInput}
                                onChange={e =>
                                  this.setClientSecretInput(e.target.value)
                                }
                              />
                              <Input
                                placeholder="Enter Redirect URL"
                                value={this.state.redirectURIInput}
                                onChange={e =>
                                  this.setRedirectURIInput(e.target.value)
                                }
                              />
                              <Button
                                type="primary"
                                style={{ marginTop: "10px" }}
                                onClick={async () =>
                                  await this.handleEbayDeveloperSubmit(context.user)
                                }
                              >
                                Submit
                              </Button>
                            </div>
                          </Modal>
                        </Col>
                      </Row>
                    </Card>
                  </div>

                )}
              </div>
            </div>
            {/* second card ends */}
            <BootstrapModal show={this.state.userInfoModal} onHide={(e) => this.setState({ userInfoModal: false })} className="edit-account-modal-popup">
              <BootstrapModal.Header closeButton >
                <BootstrapModal.Title>{this.state.userPageTitle}</BootstrapModal.Title>
              </BootstrapModal.Header>
              <BootstrapModal.Body>
                <h4 style={{ textAlign: 'center' }} >{this.state.modalResponse}</h4>
              </BootstrapModal.Body>
              <BootstrapModal.Footer className="modal-btn-align">
                <Button className="btn btn-success info-btn-listing" onClick={(e) => this.setState({ userInfoModal: false })} >OK</Button>

              </BootstrapModal.Footer>
            </BootstrapModal>

            <BootstrapModal show={this.state.passwordConfirmationModal} onHide={() => this.setState({ passwordConfirmationModal: false })}  className="edit-account-modal-popup">
              <BootstrapModal.Header closeButton >
                <BootstrapModal.Title>Change Password</BootstrapModal.Title>
              </BootstrapModal.Header>
              <BootstrapModal.Body>
                <h4 style={{ textAlign: 'center' }} className="confirmation-channel-text">Do you want to update your account password?</h4>
              </BootstrapModal.Body>
              <BootstrapModal.Footer className="modal-btn-align">
                <Button className="btn btn-secondary" onClick={(e) => this.setState({ passwordConfirmationModal: false })} >Cancel</Button>
                <Button className="btn btn-success info-btn-listing" onClick={(e) => this.changePass()} >Submit</Button>
              </BootstrapModal.Footer>
            </BootstrapModal>
            
            {/* User Info changes confirmation modal  */}
            <BootstrapModal show={this.state.userInfoConfirmationModal} onHide={() => this.setState({ userInfoConfirmationModal: false })} className="edit-account-modal-popup">
              <BootstrapModal.Header closeButton >
                <BootstrapModal.Title>Update Account Info</BootstrapModal.Title>
              </BootstrapModal.Header>
              <BootstrapModal.Body>
                <h4 style={{ textAlign: 'center' }} className="confirmation-channel-text">Do you want to submit these changes <br></br>to your account?</h4>
              </BootstrapModal.Body>
              <BootstrapModal.Footer className="modal-btn-align">
                <Button className="btn btn-secondary" onClick={(e) => this.setState({ userInfoConfirmationModal: false })} >Cancel</Button>
                <Button className="btn btn-success info-btn-listing" onClick={ (e) => this.submitUserChange(this.state.userAccountInfo, this.state.userContext) } >Submit</Button>
              </BootstrapModal.Footer>
            </BootstrapModal>

            {/* Confirm Modal */}
            <BootstrapModal show={this.state.userConfirmModal} onHide={() => this.CloseConfirmModal()}  className="edit-account-modal-popup">
              <BootstrapModal.Header closeButton >
                <BootstrapModal.Title>{this.state.userPageTitle}</BootstrapModal.Title>
              </BootstrapModal.Header>
              <BootstrapModal.Body>
                <h4 style={{ textAlign: 'center' }} className="confirmation-channel-text">{this.state.modalResponse}</h4>
              </BootstrapModal.Body>
              <BootstrapModal.Footer className="modal-btn-align">
                <Button className="btn btn-secondary info-btn-listing" onClick={(e) => this.CloseConfirmModal()} >Cancel</Button>
                <Button className="btn btn-success info-btn-listing" onClick={(e) => this.handlFormSubmit()} >Submit</Button>
              </BootstrapModal.Footer>
            </BootstrapModal>
            {/* Confirm Modal */}
          </div>
        )}
      </UserContext.Consumer>
    ) : (
      <Loader />
    );
  }
}

EditAccount.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

// Note: The normal "this.props.t()" won't work here.
EditAccount.defaultProps = {
  title: (
    <Translation>
      {(t, { i18n }) => {
        t("userInfo.details");
      }}
    </Translation>
  )
};

export default withTranslation()(EditAccount);
