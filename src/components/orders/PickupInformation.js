import React, { useState, useEffect, useContext } from "react";
import {
  Col
} from "shards-react";

import { useTranslation } from "react-i18next";

import UserContext from "../../contexts/user.jsx";
import Card from "antd/es/card";
// style icon
import styled from 'styled-components'
import { Download } from '@styled-icons/bootstrap/Download'
import { Edit } from '@styled-icons/feather/Edit'
import { Attachment } from '@styled-icons/entypo/Attachment'
import { ticketTypes } from "../../constants/constants";

const EditIcon = styled(Edit)`
  color: #ffffff;
  width:20px;
  height:20px;
`
const DownloadIcon = styled(Download)`
  color: #ffffff;
  width:20px;
  height:20px;
`
const AttachmentIcon = styled(Attachment)`
  color: #2C91EE;
  width:20px;
  height:20px;
`


const PickupInformation = (props) => {

  const { user } = useContext(UserContext);

  const { t } = useTranslation();

  const ticketType = props.ticketType;

  useEffect(() => {
  }, []);

  return (
    <>
      <div className="small-card-body">
      {ticketTypes.localPickup.id == ticketType.ticket_type_id && (
        <Card
          className="order-detail-channel-card"
          title="Pickup Information"
        >
          <div className="information-grid-section">
            <div className="pickup-contact-details">
              <div className="pickup-status-grid">
                <div className="pickup-label">
                  <h6>Contact Name:</h6>
                </div>
                <div className="pickup-field-item">
                  <h6>Tim Miller</h6>
                </div>
                <div className="pickup-label">
                  <h6>Pickup Address: </h6>
                </div>
                <div className="pickup-field-item">
                  <h6>342 North Ave, Suite #861, San Francisco, CA, USA, 90475</h6>
                </div>
                <div className="pickup-label">
                  <h6>Email Address:</h6>
                </div>
                <div className="pickup-field-item">
                  <h6>Tim@yadara.com</h6>
                </div>
                <div className="pickup-label">
                  <h6>Phone Number:</h6>
                </div>
                <div className="pickup-field-item">
                  <h6>(579) 670-6754</h6>
                </div>
              </div>
            </div>
            <div className="pickup-contact-details">
              <div className="pickup-label">
                <h6>Pickup Notes:</h6>
              </div>
              <div className="pickup-field-item">
                <h6>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Gravida cum sociis natoque penatibus. Tellus id interdum velit laoreet id donec.</h6>
              </div>
            </div>
          </div>
        </Card>
      )}
        {/* Shopping information */}
      {ticketTypes.hardStock.id == ticketType.ticket_type_id && (  
        <Card
          className="order-detail-channel-card"
          title="Shipping Information"
        >
          <div className="information-grid-section">
            <div className="pickup-contact-details">
              <div className="pickup-status-grid">
                <div className="pickup-label">
                  <h6>Contact Name:</h6>
                </div>
                <div className="pickup-field-item">
                  <h6>Tim Miller</h6>
                </div>
                <div className="pickup-label">
                  <h6>Pickup Address: </h6>
                </div>
                <div className="pickup-field-item">
                  <h6>342 North Ave, Suite #861, San Francisco, CA, USA, 90475</h6>
                </div>
                <div className="pickup-label">
                  <h6>Email Address:</h6>
                </div>
                <div className="pickup-field-item">
                  <h6>Tim@yadara.com</h6>
                </div>
                <div className="pickup-label">
                  <h6>Phone Number:</h6>
                </div>
                <div className="pickup-field-item">
                  <h6>(579) 670-6754</h6>
                </div>
              </div>
            </div>
            <div className="pickup-contact-details">
              <div className="pickup-label">
                <h6>Pickup Notes:</h6>
              </div>
              <div className="pickup-field-item">
                <h6>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Gravida cum sociis natoque penatibus. Tellus id interdum velit laoreet id donec.</h6>
              </div>
            </div>
          </div>
        </Card>
      )}
      </div>
    </>
  );
};

export default PickupInformation;
