import React, { useContext } from "react";
import { useTranslation } from "react-i18next";

import UserContext from "../../contexts/user.jsx";
import { Modal} from 'react-bootstrap';  


import Card from "antd/es/card";


const toolTipStyle = {
  fontSize: "18px",
  marginLeft: "5px",
  paddingBottom: "3px",
  color: "rgba(89,89,89, 0.4)"
};

const OrderActionCard = () => {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
// substitute modal
const [substitute, setIsOpen] = React.useState(false);

  const showModal = () => {
    setIsOpen(true);
  };

  const hideModal = () => {
    setIsOpen(false);
  };

  // order cancel modal
  const [orderCancel, orderCancelOpen] = React.useState(false);

  const showOrderModal = () => {
    orderCancelOpen(true);
  };

  const hideOrderModal = () => {
    orderCancelOpen(false);
  };
  
  // Postevent modal
  const [postEvent, postEventOpen] = React.useState(false);

  const showPostEventModal = () => {
    postEventOpen(true);
  };

  const hidePostEventModal = () => {
    postEventOpen(false);
  };
  // eventcancel
  const [eventCancel, eventCancelOpen] = React.useState(false);

  const showEventCancelModal = () => {
    eventCancelOpen(true);
  };

  const hideEventCancelModal = () => {
    eventCancelOpen(false);
  };


  return (
    <>
      <Card
        className="order-detail-channel-card"
        title="Order Actions"
      >
        <div className="graph-text-body">
          <button className="btn substitute-ticket-btn stylefont-weight-bold" style={{width:"100%"}} onClick={showModal}>Substitute Tickets</button>
          <button className="btn order-cancel-btn stylefont-weight-bold" style={{width:"100%"}} onClick={showOrderModal}>Order Cancellation</button>
            <button className="btn post-event-btn stylefont-weight-bold" style={{width:"100%"}} onClick={showPostEventModal} >Postponed Event</button>
            <button className="btn cancel-event-btn stylefont-weight-bold" style={{width:"100%"}} onClick={showPostEventModal}>Cancelled Event</button>
          </div>
      </Card>

      {/* modal popup */}
      <Modal show={substitute} onHide={hideModal} className="substitute-modal" aria-labelledby="contained-modal-title-vcenter" centered backdrop="static">
        <Modal.Header closeButton>
          <h5 className="substitute-txt">Are your substitute tickets within 5 rows of the current tickets?</h5>
        </Modal.Header>
        <Modal.Footer>
          <button className="btn btn-primary substitute-yes-btn" onClick={hideModal}>Yes</button>
          <button className="btn btn-primary substitute-no--btn">No</button>
        </Modal.Footer>
      </Modal>

      {/* modal popup */}
      <Modal show={orderCancel} onHide={hideOrderModal} className="order-cancel-modal" aria-labelledby="contained-modal-title-vcenter" centered backdrop="static">
        <Modal.Header closeButton>
          <h5 className="substitute-txt">Cancel Order</h5>
        </Modal.Header>
        <Modal.Body>
          <h4>Cancelling this order will result in a penalty.</h4>
        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-primary order-_cancel-btn" onClick={hideOrderModal}>Back</button>
          <button className="btn btn-primary submit-cancel-btn">Submit Cancellation</button>
        </Modal.Footer>
      </Modal>

      {/* modal popup */}
      <Modal show={postEvent} onHide={hidePostEventModal} className="order-cancel-modal" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
          <h5 className="substitute-txt">Postponed Event</h5>
        </Modal.Header>
        <Modal.Body>
          <h4>Submit a status update for this event.</h4>
        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-primary order-_cancel-btn" onClick={hidePostEventModal}>Back</button>
          <button className="btn btn-primary submit-post-event-btn">Submit Postponement</button>
        </Modal.Footer>
      </Modal>

      {/* modal popup */}
      <Modal show={eventCancel} onHide={hideEventCancelModal} className="order-cancel-modal" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
          <h5 className="substitute-txt">Cancelled Event</h5>
        </Modal.Header>
        <Modal.Body>
          <h4>Submit a status update for this event.</h4>
        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-primary order-_cancel-btn" onClick={hideEventCancelModal}>Back</button>
          <button className="btn btn-primary submit-cancel-event-btn">Submit Cancelled Event</button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default OrderActionCard;
