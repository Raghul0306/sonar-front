import React, { useState, useContext } from "react";

import { useTranslation } from "react-i18next";

import UserContext from "../../contexts/user.jsx";
import Card from "antd/es/card";
// import * as moment from "moment";
import * as moment from "moment-timezone";


const EventsListCard = (props) => {
  const [show, setShow] = useState(true);
  const { user } = useContext(UserContext);

  const { t } = useTranslation();
  const eventDetails = props.eventDetails;
  const listingDetails = props.listingDetails;

  let eventId = props.eventId ? props.eventId : "-";
  let eventname = eventDetails && eventDetails.eventname ? eventDetails.eventname : "-";
  let eventdate = eventDetails && eventDetails.eventdate ? eventDetails.eventdate : "-";
  let eventstatus = eventDetails && eventDetails.event_status ? eventDetails.event_status : "-";


  let venue = eventDetails && eventDetails.venue ? eventDetails.venue : '';
  let venuecity = venue && venue.venuecity ? venue.venuecity : '';
  let venuename = venue && venue.venuename ? venue.venuename : '';
  let venuestate = venue && venue.venuestate ? venue.venuestate : '';
  let venuecountry = venue && venue.venuecountry ? venue.venuecountry : '';
  let timezone = venue && venue.timezone ? venue.timezone : '';

  let section = listingDetails && listingDetails.section ? listingDetails.section : "-";
  let row = listingDetails && listingDetails.row ? listingDetails.row : "-";
  let seats = listingDetails && listingDetails.seats ? listingDetails.seats : "-";
  let price = listingDetails && listingDetails.price ? "$ " + listingDetails.price : "-";
  let margin = listingDetails && listingDetails.margin ? listingDetails.margin : "-";
  let soldcount = listingDetails && listingDetails.soldcount ? listingDetails.soldcount : "-";
  let listing_tags = listingDetails && listingDetails.listing_tags ? listingDetails.listing_tags : [];



  return (

    <div className="small-card-body">
      <Card
        className="order-detail-channel-card event-status-card"
        title="Event Status"
        extra={<a  className="event-status-btn">{eventstatus}</a>}
      >
        <div className="event-card-body">
          <div className="event-detail-section">
            <div className="d-flex">
              <h3 className="order-detail-event-title stylefont-weight-bold">{eventname}</h3>
              <h5 className="order-detail-event-id stylefont-weight-medium">{eventId}</h5>
            </div>
            <h4 className="event-time-date stylefont-weight-bold primary-color">
              {eventdate ?
                (timezone ? moment.utc(eventdate).tz(timezone).format("dddd MMM D, YYYY @ h:mm A z") : moment.utc(eventdate).format("dddd MMM D, YYYY @ h:mm A z")) : ''
              }
            </h4>
            <h4 className="event-location stylefont-weight-bold secondary-color">{venuename}, {venuecity}, {venuestate}, {venuecountry}</h4>
          </div>
          <div className="seat-row-order-details">
            <div className="seat-section-details">
              <table className="table">
                <tr>
                  <td>Sec: <span className="blue-green-txt">{section}</span></td>
                  <td>Row: <span className="blue-green-txt">{row}</span></td>
                  <td>Seats: <span className="blue-green-txt"> {seats} </span></td>
                </tr>
                <tr>
                  <td>Price: <span className="cyan-green-txt">{price}</span></td>
                  <td>Margin: <span className="cyan-green-txt">{margin}</span></td>
                  <td>Quantity: <span className="cyan-green-txt">{soldcount}</span></td>
                </tr>
                <tr>
                  <td colSpan="2">
                    <div className="d-flex">Tags:
                      <div className="tags-list">
                        {listing_tags ? listing_tags.map((t_name) => {
                          return (<div className="list-tags-item"><span>{t_name.tag_name}</span></div>)
                        }) : {}}
                      </div>
                    </div>
                  </td>
                  <td>Listing ID: <span className="listing-id-txt">{
                    props.listingGroupId
                  }</span></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};

export default EventsListCard;
