import React, { useContext } from "react";
import PropTypes from "prop-types";
import {
  InputGroup,
  DatePicker,
  InputGroupAddon,
  InputGroupText,
  FormCheckbox,
  FormRadio
} from "shards-react";
import "../../assets/checkbox.css";
import { useTranslation } from "react-i18next";
import Input from 'antd/es/input';
import { Tooltip } from "@material-ui/core";
import CalendarTodayIcon from "@material-ui/icons/CalendarToday";
import UserContext from "../../contexts/user.jsx";
import { userRoles } from "../../constants/constants";
const OrderFilter = ({
  checkboxes,
  radioButtons,
  rangeDatePicker,
  location
}) => {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const CustomInputDate = ({ value, onClick }) => (
    <Input
      value={value}
      onClick={onClick}
      style={{ paddingLeft: "10px" }}
      suffix={
        <CalendarTodayIcon
          style={{
            color:
              user.userRole === userRoles.marketplace_admin ? "#2e333d" : "#5f8ff0",
            fontSize: "18px"
          }}
        />
      }
    />
  );
  return (
    <div
      style={{ marginTop: "10px" }}
      className="d-flex  align-items-center mb-3"
    >
      {radioButtons &&
        radioButtons.map(radioButton =>
          radioButton.type === "mycards" ? (
            <Tooltip title={t("form.purchaseOrderTooltip.orderFilterActive")}>
              <FormRadio
                key={radioButton.name}
                checked={radioButton.value}
                onChange={e => {
                  radioButton.setValue(oldValue => !oldValue);
                }}
                inline
              >
                {t(radioButton.name)}
              </FormRadio>
            </Tooltip>
          ) : (
            <FormRadio
              key={radioButton.name}
              cssClass="e-primary"
              checked={radioButton.value}
              onChange={e => {
                radioButton.setValue(oldValue => !oldValue);
              }}
              inline
            >
              {t(radioButton.name)}
            </FormRadio>
          )
        )}
      {checkboxes &&
        checkboxes.map(checkbox => (
          <FormCheckbox
            key={checkbox.name}
            checked={checkbox.value}
            onChange={e => {
              checkbox.setValue(oldValue => !oldValue);
            }}
            inline
          >
            {t(checkbox.name)}
          </FormCheckbox>
        ))}
      {rangeDatePicker && (
        <InputGroup className="d-flex date-range w-auto">
          <InputGroupAddon type="append">
            <InputGroupText
              style={{
                fontWeight: "500",
                backgroundColor: "transparent",
                border: "none"
              }}
            >
              {t("listing.dateAdded", { location: location })}
            </InputGroupText>
          </InputGroupAddon>
          <DatePicker
            size="sm"
            selected={rangeDatePicker.startdateValue}
            onChange={props => {
              rangeDatePicker.setStartdateValue(props);
            }}
            placeholderText={t("form.startDate")}
            dropdownMode="select"
            className="text-center"
            customInput={<CustomInputDate />}
          />

          <DatePicker
            size="sm"
            selected={rangeDatePicker.enddateValue}
            onChange={props => {
              rangeDatePicker.setEnddateValue(props);
            }}
            placeholderText={t("form.endDate")}
            dropdownMode="select"
            className="text-center margin-left"
            customInput={<CustomInputDate />}
          />
        </InputGroup>
      )}
    </div>
  );
};

OrderFilter.prototype = {
  checkboxes: PropTypes.arrayOf(Object),
  rangeDatePicker: PropTypes.object
};

export default OrderFilter;
