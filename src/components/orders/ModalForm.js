import React, { useState } from "react";
import Modal from 'antd/es/modal';
import Form from 'antd/es/form';
import Input from 'antd/es/input';
import Select from 'antd/es/select';
import Button from 'antd/es/button';
// style icon
import styled from 'styled-components'
import { Attachment } from '@styled-icons/entypo/Attachment'
const AttachmentIcon = styled(Attachment)`
  color: #2C91EE;
  width:25px;
  height:25px;
`
const { Option } = Select;

const ModalFormComponent = ({ visible, onCancel, onCreate, form, attachment, viewTicket, uploadDoc, seatStart }) => {

    const [selectedAttachment, setselectedAttachment ] = useState(null);

    const handleOnChange = (value, event) => {
        if( value != ""){
           let newValue = attachment.filter(ar => ar.id === value );
           if(newValue.length){
            if(newValue[0].attachment_url === "nil" || newValue[0].attachment_url === null || newValue[0].attachment_url === ""){
                setselectedAttachment(null)
            }
            else { setselectedAttachment(newValue[0])  }
           }
        }else{
          setselectedAttachment(null)
        }
    }

    const handleFileUpload = ( event ) => {
        if(event.target.files)
            uploadDoc(event.target.files[0])  
    }
    
  const { getFieldDecorator } = form;
  return (
    <Modal
      visible={visible}
      title="Edit Tickets"
      okText="Update Ticket"
      onCancel={onCancel}
      onOk={onCreate}
      maskClosable = {false}
      cancelButtonProps={{ style: { display: 'none' } }}
      className="download-modal-popup"
      maskClosable={false}
    >
      <Form layout="vertical">
        <div className="form-group edit-seat-select">
        
        <Form.Item label="Select Ticket">
          {getFieldDecorator("attachment_id", {
              initialValue: "",
            rules: [
              {
                required: true,
                message: "Please select seat!"
              }
            ]
          })(
            <Select onSelect={(value, event) => handleOnChange(value, event)} >                
                <Option value="">Select seat</Option>
                
                {attachment.map((t, index) => {
                    var seatStartNo = parseInt(seatStart) + parseInt(index);
                    return <Option key={index} value={t.id} > Seat #{seatStartNo}</Option>                                       
                }) }
                 
            </Select>       
          )}
        </Form.Item>         
        </div>
        <div className="form-group edit-seat-select">
        <div class="ant-col ant-form-item-label">
          <label for="" class="ant-form-item-required" title="Attached Ticket">
          Attached Ticket</label>
        </div>
        <div className="document-grid-sec-jtk">
            <div className="document--item">
              <div className="document-info">
                {selectedAttachment ? (
                  <div class="attach-btn">
                  <AttachmentIcon/> {selectedAttachment.attachment_url.substring(selectedAttachment.attachment_url.lastIndexOf('/')+1)} </div>
                ):(
                  <div class="attach-btn">
                  <AttachmentIcon/> New York Yankees Ticket.pdf</div>
                ) }
              
              </div>
            </div>
            {selectedAttachment ? (
            <div className="view-section">
              <Button className="invoice-view--btn" onClick={() => viewTicket(selectedAttachment.id, selectedAttachment.attachment_url.replace(/^.*\./, ''))} >View</Button>
            </div>
             ) : (
              <div className="view-section">
                <Button className="invoice-view--btn" disabled={true} >View</Button>
              </div>
             ) }
            <div className="replace-sec_gr">
              <div className="replace--item-kjk">
              <Form.Item label="Attach file">
                {getFieldDecorator("file", {
                  rules: [
                    {
                      required: true,
                      message: "Please upload attachment!"
                    }
                  ]
                })(<Input type="file" onChange={ (e)=> handleFileUpload(e) } />)}
              </Form.Item>
              </div>
               <Button className="invoice-replace--btn replace-order-btn">Replace</Button>
            </div>
          </div>
          
        </div>
        
      </Form>
    </Modal>
  );
};

const ModalForm = Form.create({ name: "edit_ticket_form" })(ModalFormComponent);

export default ModalForm;
