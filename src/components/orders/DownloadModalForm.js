import React, { useState, useRef } from "react";
import Modal from 'antd/es/modal';
import Form from 'antd/es/form';
import MultiSelect from "react-multi-select-component";


const ModalFormComponent = ({ visible, onCancel, onCreate, attachment, selectedTicket }) => {
  
  const [selected, setSelected] = useState([]);

  selectedTicket(selected);
  

  return (
    <Modal
      visible={visible}
      title="Download Tickets"
      okText="Download Tickets"
      onCancel={onCancel}
      cancelButtonProps={{ style: { display: 'none' } }}
      onOk={onCreate}
      maskClosable = {false}
      className="download-modal-popup"
    >
      <Form layout="vertical">
        
        <Form.Item label="Select Tickets">
        
            <MultiSelect
              options={attachment}
              name={"seats"}
              className="form-control capitalize"
              selectAllLabel='Select All'
              labelledBy="Select Seats"
              onChange={setSelected}
              value={selected}
              maskClosable={false}
            />
        
        </Form.Item>       
        
      </Form>
    </Modal>
  );
};

const ModalForm = Form.create({ name: "download_ticket_form" })(ModalFormComponent);

export default ModalForm;
