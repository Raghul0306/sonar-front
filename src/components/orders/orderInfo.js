import React, { useState, useEffect, useContext } from "react";
import { useTranslation } from "react-i18next";
import CloseRoundedIcon from "@material-ui/icons/CloseRounded";
import { getDeliveryStatusByLabelId } from "../../services/shippingService";
import { getListingById } from "../../services/listingsService";
import isEmpty from "lodash/isEmpty";
import isArray from "lodash/isArray";
import "react-responsive-carousel/lib/styles/carousel.min.css";

import Button from 'antd/es/button';
import Divider from 'antd/es/divider';
import Modal from 'antd/es/modal';
import { Carousel } from "react-responsive-carousel";
import { returnListingImages } from "../../helpers/getListingImage";
import { SupportModalForm } from "../insightsDash/SupportModalContent";
import UserContext from "../../contexts/user.jsx";

export const OrderInfo = props => {
  const { t } = useTranslation();
  const { order } = props;
  const { user } = useContext(UserContext);
  // this is where the order is passed in and will
  // have all of the properties you see in the grid (and more)
  // eslint-disable-next-line
  const [deliveryState, setDeliveryState] = useState();
  const [trackingNumber, setTrackingNumber] = useState("-");
  const [listing, setListing] = useState(null);
  const [cardImages, setCardImages] = useState(false);
  const [supportModalVisible, setSupportModalVisible] = useState(false);

  useEffect(() => {
    const componentMounted = async () => {
      if (props.order.shippingLabelId) {
        const deliveryInformation = await getDeliveryStatusByLabelId(
          props.order.shippingLabelId
        );
        setTrackingNumber(deliveryInformation.tracking_number);
        setDeliveryState(deliveryInformation.status_description);
      }
      const listingById = await getListingById(props.order.listingId);
      setListing(listingById);
    };
    const fetchImages = async () => {
      if (order && order.listingId) {
        let images = await returnListingImages(order.listingId);
        if (images.length > 0) {
          setCardImages(isArray(images) ? images : JSON.parse(images));
        }
      }
    };
    fetchImages();
    componentMounted();
  }, [order]);

  const handleSupportOk = e => {
    setSupportModalVisible(false);
  };

  const handleSupportCancel = e => {
    setSupportModalVisible(false);
  };

  return (
    <>
      <div className="modalWithHead-floating-cancel-button">
        <Button shape="circle" size="large" onClick={props.handleCancel}>
          <CloseRoundedIcon
            style={{
              fontSize: "23px"
            }}
          />
        </Button>
      </div>
      {order && (
        <div className="popup-body">
          {/* --- */}
          <div className="orderDetails-popup-card-body">
            <div>
              <label className="orderDetails-label">
                {t("orders.channel")}
              </label>
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              >
                {/* {info goes here} */}-
              </p>
            </div>
            <div>
              <label className="orderDetails-label">{t("orders.shipBy")}</label>
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              >
                {/* {info goes here} */}-
              </p>
            </div>
            <div>
              <label className="orderDetails-label">
                {t("orders.shippingStatus")}
              </label>
              {deliveryState ? (
                <div className="orderDetails-delivered">{deliveryState}</div>
              ) : (
                <div className="orderDetails-inprocess">
                  {t("orders.inprocess")}
                </div>
              )}
            </div>
          </div>
          {/* ------- */}
          <Divider />
          {/* ------- */}

          <div
            className="orderDetails-popup-card-body"
            style={{
              marginTop: "15px"
            }}
          >
            <div>
              <label className="orderDetails-label">
                {t("orders.shippingTrackingNumber")}
              </label>
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              >
                {/* {info goes here} */}
                {trackingNumber}
              </p>
              <div
                className="orderDetails-label"
                style={{ margin: "20px 0 10px 0" }}
              >
                {t("orders.listingId")}:{" "}
                {order.listingByListingId ? order.listingByListingId.id : "-"}
              </div>
              <label className="orderDetails-label">
                {t("orders.cardDetails")}
              </label>
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              >
                {/* {info goes here} */}
                <div>
                  {listing && listing.cardByCardId.setBySet.brandByBrandId.name}{" "}
                  {listing && listing.cardByCardId.setBySet.year}{" "}
                  {listing && listing.cardByCardId.setBySet.name}
                </div>
                <div>
                  {order.listingByListingId
                    ? order.listingByListingId.name
                    : "-"}{" "}
                  {listing && listing.cardByCardId.cardNumber}
                </div>
                <div>
                  {listing && listing.grade} {listing && listing.gradeType}{" "}
                  {listing && listing.gradeAttribute}
                </div>
              </p>
              <div
                className="orderDetails-label"
                style={{ margin: "20px 0 10px 0" }}
              >
                {t("orders.price")}:{" "}
                <label className="cardinfo-form-label">
                  {order.listingByListingId
                    ? `$${order.listingByListingId.price}`
                    : "-"}
                </label>
              </div>
            </div>
            <div>
              <div>
                <label className="orderDetails-label">Tickets</label>
                {/* Carousel has it's width handled in overrides.css */}
                {/* {cardImages.length > 0 && (
                  <Carousel
                    showArrows={true}
                    // onChange={onChange}
                    // onClickItem={onClickItem}
                    // onClickThumb={onClickThumb}
                  >
                    {cardImages.map(images => (
                      <div>
                        <img style={{ width: "300px" }} alt="" src={images} />
                      </div>
                    ))}
                  </Carousel>
                )}
                {(!cardImages || cardImages.length < 1) && (
                  <img
                    src="https://via.placeholder.com/300x150?text=No+image+to+show"
                    alt=""
                  />
                )} */}
                {!isEmpty(cardImages) ? (
                  <div>
                    {/* Carousel has it's width handled in overrides.css */}
                    <Carousel
                      showArrows={true}
                      // onChange={onChange}
                      // onClickItem={onClickItem}
                      // onClickThumb={onClickThumb}
                    >
                      {cardImages &&
                        cardImages.map(image => (
                          <div>
                            <img alt="" src={image} />
                          </div>
                        ))}
                    </Carousel>
                  </div>
                ) : (
                  <div style={{ textAlign: "center", marginTop: "5%" }}>
                    <img
                      alt="order-img"
                      src="https://via.placeholder.com/300x350"
                    />
                  </div>
                )}
              </div>
            </div>
          </div>

          {/* ---- */}
          {/* <Divider /> */}
          {/* --- */}
          {/* <div className="orderDetails-popup-card-body">
            <div>
              <label className="orderDetails-label">
                {t("orders.purchaseId")}
              </label>
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label" */}
          {/* > */}
          {/* {info goes here} */}
          {/* {order.listingByListingId
                  ? order.listingByListingId.purchaseOrderId
                  : "-"}
              </p>
            </div>
            <div>
              <label className="orderDetails-label">
                {t("orders.purchasePrice")}
              </label>
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              > */}
          {/* {info goes here} */}
          {/* {order.listingByListingId
                  ? order.listingByListingId.price
                  : "-"}
              </p>
            </div>
          </div> */}
          <Divider />
          {/* ---- */}
          <label style={{ fontSize: "20px" }} className="orderDetails-label">
            {t("orders.buyerDetails.title")}
          </label>
          <div className="orderDetails-popup-card-body">
            <div>
              {/* <label className="orderDetails-label">
                {t("orders.buyerDetails.name")}
              </label> */}
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              >
                {/* {info goes here} */}
                {order.buyerByBuyerId
                  ? `${order.buyerByBuyerId.firstName} ${order.buyerByBuyerId.lastName}`
                  : ""}
              </p>
            </div>
            <div>
              {/* <label className="orderDetails-label">
                {t("orders.buyerDetails.email")}
              </label> */}
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              >
                {/* {info goes here} */}
                {order.buyerByBuyerId ? `${order.buyerByBuyerId.email} ` : "-"}
              </p>
            </div>
          </div>

          {/* ---- */}
          <div className="orderDetails-popup-card-body">
            <div>
              {/* <label className="orderDetails-label">
                {t("orders.buyerDetails.address1")}
              </label> */}
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              >
                {/* {info goes here} */}
                {order.buyerByBuyerId
                  ? `${order.buyerByBuyerId.streetAddress} `
                  : ""}
              </p>
            </div>
            <div>
              {/* <label className="orderDetails-label">
                {t("orders.buyerDetails.phone")}
              </label> */}
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              >
                {/* {info goes here} */}
                {order.buyerByBuyerId && order.buyerByBuyerId.phone
                  ? `${order.buyerByBuyerId.phone} `
                  : ""}
              </p>
            </div>
          </div>
          {/* ---- */}
          <div className="orderDetails-popup-card-body">
            <div>
              {/* <label className="orderDetails-label">
                {t("orders.buyerDetails.address2")}
              </label> */}
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              >
                {/* {info goes here} */}
                {order.buyerByBuyerId && order.buyerByBuyerId.streetAddress2
                  ? `${order.buyerByBuyerId.streetAddress2} `
                  : ""}
              </p>
            </div>
          </div>
          {/* ----- */}
          <div className="orderDetails-popup-card-body">
            <div>
              {/* <label className="orderDetails-label">
                {t("orders.buyerDetails.city")}
              </label> */}
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              >
                {/* {info goes here} */}
                {order.buyerByBuyerId ? `${order.buyerByBuyerId.city} ` : ""},
                {order.buyerByBuyerId ? `${order.buyerByBuyerId.state} ` : ""}
              </p>
            </div>
            <div>
              {/* <label className="orderDetails-label">
                {t("orders.buyerDetails.state")}
              </label>
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              > */}
              {/* {info goes here} */}
              {/* {order.buyerByBuyerId ? `${order.buyerByBuyerId.state} ` : "-"} */}
              {/* </p> */}
            </div>
          </div>
          {/* ----- */}
          <div className="orderDetails-popup-card-body">
            <div>
              {/* <label className="orderDetails-label">
                {t("orders.buyerDetails.country")}
              </label> */}
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              >
                {/* {info goes here} */}
                {order.buyerByBuyerId
                  ? `${order.buyerByBuyerId.postalCode} `
                  : ""}{" "}
                {order.buyerByBuyerId ? `${order.buyerByBuyerId.country} ` : ""}
              </p>
            </div>
            <div>
              {/* <label className="orderDetails-label">
                {t("orders.buyerDetails.zip")}
              </label>
              <p
                style={{ padding: 0, margin: 0 }}
                className="cardinfo-form-label"
              > */}
              {/* {info goes here} */}
              {/* {order.buyerByBuyerId
                  ? `${order.buyerByBuyerId.postalCode} `
                  : "-"}
              </p> */}
            </div>
          </div>
          <Modal
            title={t("support.contact")}
            visible={supportModalVisible}
            onOk={handleSupportOk}
            onCancel={handleSupportCancel}
            closable={false}
            footer={false}
            maskClosable={false}
          >
            <SupportModalForm
              handleCancel={handleSupportCancel}
              userEmail={user.email}
              orderId={props.order.id}
            />
          </Modal>
          {/* buttons will go here: */}
          <div style={{ marginTop: "25px", display: "flex" }}>
            <Button
              type="primary"
              onClick={async () => {
                await props.printShippingLabel(props.order);
              }}
            >
              {t("orders.shippingLabel")}
            </Button>
            <Button style={{ marginLeft: "10px" }} type="primary">
              {t("orders.trackShipping")}
            </Button>
            <Button
              style={{ marginLeft: "10px" }}
              type="primary"
              onClick={() => setSupportModalVisible(true)}
            >
              {t("support.contact")}
            </Button>
          </div>
        </div>
      )}
    </>
  );
};
