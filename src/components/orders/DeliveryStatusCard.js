import React, { useState, useEffect, useContext } from "react";

import { useTranslation } from "react-i18next";

import UserContext from "../../contexts/user.jsx";
import Card from "antd/es/card";


const DeliveryStatusCard = (props) => {
  const [show, setShow] = useState(true);
  const { user } = useContext(UserContext);
  const deliveryStatus = props.deliveryStatus;
  const buyerDetails = deliveryStatus.broker_order.buyerUser;  

  let buyerusername = buyerDetails && buyerDetails.buyerusername ? buyerDetails.buyerusername : "-";
  let buyeremail = buyerDetails && buyerDetails.buyeremail ? buyerDetails.buyeremail : "-";
  let buyerphonenumber = buyerDetails && buyerDetails.buyerphonenumber ? buyerDetails.buyerphonenumber : "-";
  var ticketTypeName = deliveryStatus.ticket_type ? deliveryStatus.ticket_type.ticket_type_name : "";
  if(ticketTypeName==null){
    ticketTypeName = "";
  }
  const deliveryDate = deliveryStatus && deliveryStatus.deliverydate ? deliveryStatus.deliverydate : "-";

  const listing_group = deliveryStatus && deliveryStatus.listing_group ? deliveryStatus.listing_group : {};
  const inhand = listing_group.inhand ? listing_group.inhand : "-";
  const ticketing_system = listing_group && listing_group.ticketing_system ? listing_group.ticketing_system : {};
  const delivery_status = deliveryStatus && deliveryStatus.delivery_status ? deliveryStatus.delivery_status : {};
  const deliverystatusname = deliveryStatus && delivery_status.deliverystatusname ? delivery_status.deliverystatusname : "-";
  const ticketingsystemname = ticketing_system && ticketing_system.ticketingsystemname ? ticketing_system.ticketingsystemname : '-';
  const ticketing_system_account = listing_group && listing_group.ticketing_system_account ? listing_group.ticketingsystemname : '-';
  const ticketing_system_account_name = ticketing_system_account && ticketing_system_account.ticketing_system_account ? ticketing_system_account.ticketing_system_account : '-';

  const { t } = useTranslation();



  useEffect(() => {
  }, []);

  return (
    <>

      <div className="small-card-body">
        <Card
          className="order-detail-channel-card"
          title={ticketTypeName+" Delivery Status"}
          extra={<a  className="event-status-btn">{deliverystatusname}</a>}
        >

          <div className="top-performer-list">
            <div className="delivery-status-grid">
              <div className="Left-delivery-grid stylefont-weight-medium"><h6>In-Hand Date :</h6></div>
              <div className="right-delivery-grid stylefont-weight-medium"><h6>{inhand}</h6></div>             
              <div className="Left-delivery-grid stylefont-weight-medium"><h6>Delivery Date :</h6> </div>
              <div className="right-delivery-grid stylefont-weight-medium"><h6>{deliveryDate}</h6></div>
              <div className="Left-delivery-grid stylefont-weight-medium"><h6>Ticket System :</h6> </div>
              <div className="right-delivery-grid stylefont-weight-medium"><h6>{ticketingsystemname}</h6> </div>
              <div className="Left-delivery-grid stylefont-weight-medium"><h6>Ticket System Account :</h6> </div>
              <div className="right-delivery-grid stylefont-weight-medium"><h6>{ticketing_system_account_name}</h6> </div>
            </div>
          </div>
        </Card>
      </div>

      <div className="small-card-body">
        <Card
          className="order-detail-channel-card"
          title="Buyer Information"
        >

          <div className="top-performer-list">
            <div className="delivery-status-grid">
              <div className="Left-delivery-grid"><h6>Buyer Name</h6></div>
              <div className="right-delivery-grid"><h6>{buyerusername}</h6></div>
              <div className="Left-delivery-grid"><h6>Buyer Email </h6></div>
              <div className="right-delivery-grid"><h6>{buyeremail} </h6></div>
              <div className="Left-delivery-grid"><h6>Buyer Phone</h6></div>
              <div className="right-delivery-grid"><h6>{buyerphonenumber}</h6></div>
            </div>
          </div>

        </Card>
      </div>
    </>
  );
};

export default DeliveryStatusCard;
