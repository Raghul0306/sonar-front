import React, { useState, useEffect, useContext, useCallback } from "react";
import { useTranslation } from "react-i18next";

import UserContext from "../../contexts/user.jsx";
import Card from "antd/es/card";
// style icon
import styled from 'styled-components'
import { Download } from '@styled-icons/bootstrap/Download'
import { Edit } from '@styled-icons/feather/Edit'
import { Attachment } from '@styled-icons/entypo/Attachment'
import { downloadAttachmentZipFile, viewTicket, editAttachmentTicket } from "../../services/ordersService";
import { ticketTypes, fileTypes } from "../../constants/constants";
import ModalForm from "./ModalForm";
import DownloadModalForm from "./DownloadModalForm";

const EditIcon = styled(Edit)`
  color: #ffffff;
  width:20px;
  height:20px;
`
const DownloadIcon = styled(Download)`
  color: #ffffff;
  width:20px;
  height:20px;
`
const AttachmentIcon = styled(Attachment)`
  color: #2C91EE;
  width:20px;
  height:20px;
`


const SellerDetailsCard = (props) => {

  const { user } = useContext(UserContext);

  const { t } = useTranslation();

  const [visible, setVisible] = useState(false);
  const [downloadModalVisible, setDownloadModalVisible] = useState(false);
  const [formRef, setFormRef] = useState(null);
  const [uploadDoc, setuploadDoc ] = useState(null);
  const [selectedTicketForDownload, setSelectedTicketForDownload ] = useState(null);
  

  const handleCreate = () => {
     formRef.validateFields(async (err, values) => {
      if (err) {
        return;
      }
      
      if(uploadDoc){
        let timestamp = new Date().getTime();
        let attachmentfile_data = uploadDoc;
        var attachmentId = values.attachment_id;        
        let attachmentfilename = attachmentfile_data ? (timestamp + "_" + attachmentfile_data.name) : '';

        var data = {
          "attachment_file": attachmentfilename,
          "attachment_file_data": attachmentfile_data,
          'attachmentId': attachmentId,
          "attachment_file_type": '',
          'attachment_file_name': ''
        };

        var listingData = {
          'eventId':props.eventId,
          'listingGroupId':props.listingGroupId,
          'attachmentId': attachmentId,
        }
        await editAttachmentTicket(data, listingData).then((res)=>{
          props.pageReload(true) }); 
      }
    
      formRef.resetFields();
      setVisible(false);
    });
  };

  const cancelUpdate = () => {
    formRef.validateFields((err, values) => {
      formRef.resetFields();
      setVisible(false);
    });
  }

  const downloadseats = () => {
    if( selectedTicketForDownload && selectedTicketForDownload.length > 0){

      var attachmentData = {
        eventId:props.eventId,
        listingGroupId:props.listingGroupId,
        selectedAttachment:selectedTicketForDownload
      }

       downloadAttachmentZipFile(attachmentData).then((res) => {
        if(res && res.data){
            const data = res.data; 
            
            const url = window.URL.createObjectURL(
              new Blob([data], {
                type: res.headers["Content-Type"]
              })
            );
    
            const link = document.createElement("a");
            link.href = url;
            link.setAttribute("download", new Date().getTime()+'tickets.zip');
            document.body.appendChild(link);
            link.click();
            link.parentNode.removeChild(link);
            setDownloadModalVisible(false);
            props.pageReload(true)
        }  
      });
     // setSelectedTicketForDownload([])
      
    }
  }

  const saveFormRef = useCallback(node => {
    if (node !== null) {
      setFormRef(node);
    }
  }, []);

  const attatchmentDetails = props.attatchmentDetails ? props.attatchmentDetails : [] ;
  const listing_attachments = attatchmentDetails.listing_attachments ? attatchmentDetails.listing_attachments : [] ;
  const ticketType = props.ticketType;
  const seatStart = props.seatStart;

  const options = [];
  listing_attachments && listing_attachments.length > 0
    && listing_attachments.map((item, i) => {
      var seatStartNo = parseInt(seatStart) + parseInt(i);
      if(item.attachment_url === "nil" || item.attachment_url === null || item.attachment_url === ""){
        var combined = { label: "Seat #"+ seatStartNo, value: item.id, key: item.id.toString(), disabled: true }
      }else{
        var combined = { label: "Seat #"+ seatStartNo, value: item.id, key: item.id.toString(), disabled: false }
      }
      options.push(combined);
  })

  
 

const uploadAttachment = fileData => {
  if(fileData) setuploadDoc(fileData)
}

const selectedTicket = data => {
  if(data) setSelectedTicketForDownload(data)
}

const viewTickets = async (attachmentId, ext) => {

  await viewTicket(attachmentId, props.eventId, props.listingGroupId).then((res) => {

    if(res && res.data)
    {
      let pdfWindow = window.open("");
      pdfWindow.document.write(
          "<iframe width='100%' height='100%' src='data:"+fileTypes[ext]+";base64, " +
          encodeURI(res.data) + "'></iframe>"
      )
    }

  });

};

  useEffect(() => {
  }, []);

  return  (
    <div>
      
    
   { ticketTypes.eTransfer.id !== ticketType.ticket_type_id && listing_attachments.length>0 ? (      
  <div className="small-card-body">
    <Card
          className="order-detail-channel-card"
      >

          <div className="top-performer-list">
            <div className="Attache-ticket-header">
                 <h4>Attached Tickets: </h4>
                 <div className="ml-auto header-ticket-btns">
                   {/* <button className="download-ticket-btn" onClick={zipFileDownload} ><DownloadIcon/> Download Tickets</button> */}
                   <button className="download-ticket-btn" onClick={() => setDownloadModalVisible(true)} ><DownloadIcon/> Download Tickets</button>
                   <button className="edit-ticket-btn" onClick={() => setVisible(true)} ><EditIcon/> Edit Tickets</button>
                 </div>
              </div>           
              <div className="grid-tick-section">  

              {listing_attachments ? listing_attachments.map((t, index) => {
                var seatStartNo = parseInt(seatStart) + parseInt(index);
                return  t.attachment_url === "nil" || t.attachment_url === null || t.attachment_url === "" ? (
                      <div key={index} className="seat-count-item">
                        <h5 className="seat-num-item">Seat #{seatStartNo}</h5>
                        <button className="view-down-load-btn" disabled={true} ><AttachmentIcon/> View </button>
                      </div> 
                    ) : (
                      <div key={index} className="seat-count-item">
                        <h5 className="seat-num-item">Seat #{seatStartNo}</h5>
                        <button className="view-down-load-btn" onClick={() => viewTickets(t.id, t.attachment_url.replace(/^.*\./, ''))} ><AttachmentIcon/> View </button>
                      </div>
                    )
              }) : {} }

                
              </div>
          </div>
        
      </Card>

      <ModalForm
        ref={saveFormRef}
        visible={visible}
        onCancel={() => cancelUpdate()}
        onCreate={() => handleCreate()}
        attachment={listing_attachments}
        viewTicket={viewTickets}
        uploadDoc={uploadAttachment}
        seatStart={seatStart}
      />

      <DownloadModalForm
        visible={downloadModalVisible}
        onCancel={() => setDownloadModalVisible(false)}
        onCreate={() => downloadseats()}
        attachment={options}
        selectedTicket={selectedTicket}
      />
      
    </div> ): (
    <div />
  )}
  </div>
  )
};

export default SellerDetailsCard;
