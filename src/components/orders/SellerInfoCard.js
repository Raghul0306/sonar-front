import React, { useState, useEffect, useContext } from "react";
import {
  Col
} from "shards-react";

import { useTranslation } from "react-i18next";
import UserContext from "../../contexts/user.jsx";
import Card from "antd/es/card";


const SellerInfoCard = (props) => {
  const [show, setShow] = useState(true);
  const { user } = useContext(UserContext);

  const { t } = useTranslation();
 
  const SellerDetails = props.SellerDetails;

  useEffect(() => {
  }, []);
 
  return (
        <div className="small-card-body">
            <Card
                className="order-detail-channel-card" 
                title="Seller Information" 
                >
                <div className="graph-text-body seller-grid-view">
                    <div className="list-details-view">
                        <div className="left-side-details">
                        <h5 className="stylefont-weight-medium">Seller Name: </h5>
                        </div>
                        <div className="right-side-details">
                        <h5>{SellerDetails.sellerusername}</h5>
                        </div>
                    </div>    
                    <div className="list-details-view">
                        <div className="left-side-details">
                        <h5 className="stylefont-weight-medium">Seller Email: </h5>
                        </div>
                        <div className="right-side-details">
                        <h5>{SellerDetails.selleremail}</h5>
                        </div>
                    </div>    
                    <div className="list-details-view">
                        <div className="left-side-details">
                        <h5 className="stylefont-weight-medium">Seller Delivery %: </h5>
                        </div>
                        <div className="right-side-details">
                        <h5>-</h5>
                        </div>
                    </div>    
                    <div className="list-details-view">
                        <div className="left-side-details">
                        <h5 className="stylefont-weight-medium">Seller Phone: </h5>
                        </div>
                        <div className="right-side-details">
                        <h5>{SellerDetails.sellerphonenumber}</h5>
                        </div>
                    </div>    
                    <div className="list-details-view">
                        <div className="left-side-details">
                        <h5 className="stylefont-weight-medium">Seller Company: </h5>
                        </div>
                        <div className="right-side-details">
                        <h5>{SellerDetails.sellercompanyname}</h5>
                        </div>
                    </div>    
                    <div className="list-details-view">
                        <div className="left-side-details">
                        <h5 className="stylefont-weight-medium">Seller Profile: </h5>
                        </div>
                        <div className="right-side-details">
                        <button className="btn view-profile-btn">view profile </button>
                        </div>
                    </div>    
                </div>    
            </Card>
        </div>
  );
};

export default SellerInfoCard;
