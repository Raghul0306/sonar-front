import React, { useState, useEffect, useContext } from "react";
import UserContext from "../../contexts/user.jsx";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import { autoSizeAllColumns } from "../datagrid/DataGridHelpers";
import Loader from "../Loader/Loader";

// import loadable from "@loadable/component";
// const TicketSubstituteGrid = loadable(() =>
//   import("../components/datagrid/TicketSubstituteGrid")
// );

// style icon
import styled from 'styled-components'
import { Download } from '@styled-icons/bootstrap/Download'
import { Edit } from '@styled-icons/feather/Edit'
import { Attachment } from '@styled-icons/entypo/Attachment'


const EditIcon = styled(Edit)`
  color: #ffffff;
  width:20px;
  height:20px;
`
const DownloadIcon = styled(Download)`
  color: #ffffff;
  width:20px;
  height:20px;
`
const AttachmentIcon = styled(Attachment)`
  color: #2C91EE;
  width:20px;
  height:20px;
`


const TicketSubstitute = () => {

  const { user } = useContext(UserContext);

  const [columnData, setColumn] = useState([]);
  const [rowSelection] = useState("single");

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
  }, []);

  useEffect(() => {
    const columnDefs = [{
      headerName: "ID",
      field: "ID"
    },
    {
      headerName: "Sec",
      field: "section"
    },
    {
      headerName: "Row",
      field: "row"
    },
    {
      headerName: "Seat",
      field: "seat"
    },
    {
      headerName: "Price",
      field: "price"
    },
    {
      headerName: "Update",
      field: "update"
    },
    {
      headerName: "Face",
      field: "face"
    },
    {
      headerName: "Cost",
      field: "cost"
    },
    {
      headerName: "Margin",
      field: "margin"
    },
    {
      headerName: "Available/Sold",
      field: "available"
    },
    {
      headerName: "Delivery Type",
      field: "deliveryType"
    },
    {
      headerName: "Listing Settings",
      field: "listing"
    },
    {
      headerName: "Tags",
      field: "tags"
    }

    ]
    setColumn(columnDefs);

  }, [user]);

  const rowDataSample = [
    { ID: "93850", section: "101", row: "2", seat: "1-3", price: "$60", update: "$50", face: "$0", cost: "$4", margin: "3.50%", available: "3/0", deliveryType: "eTicket", listing: "icons", tags: "pricing tags" },
    { ID: "93850", section: "101", row: "2", seat: "1-3", price: "$60", update: "$50", face: "$0", cost: "$4", margin: "3.50%", available: "3/0", deliveryType: "eTicket", listing: "icons", tags: "pricing tags" },
    { ID: "93850", section: "101", row: "2", seat: "1-3", price: "$60", update: "$50", face: "$0", cost: "$4", margin: "3.50%", available: "3/0", deliveryType: "eTicket", listing: "icons", tags: "pricing tags" },
    { ID: "93850", section: "101", row: "2", seat: "1-3", price: "$60", update: "$50", face: "$0", cost: "$4", margin: "3.50%", available: "3/0", deliveryType: "eTicket", listing: "icons", tags: "pricing tags" },
    { ID: "93850", section: "101", row: "2", seat: "1-3", price: "$60", update: "$50", face: "$0", cost: "$4", margin: "3.50%", available: "3/0", deliveryType: "eTicket", listing: "icons", tags: "pricing tags" },
    { ID: "93850", section: "101", row: "2", seat: "1-3", price: "$60", update: "$50", face: "$0", cost: "$4", margin: "3.50%", available: "3/0", deliveryType: "eTicket", listing: "icons", tags: "pricing tags" },
    { ID: "93850", section: "101", row: "2", seat: "1-3", price: "$60", update: "$50", face: "$0", cost: "$4", margin: "3.50%", available: "3/0", deliveryType: "eTicket", listing: "icons", tags: "pricing tags" },
  ]


  useEffect(() => {
    let data = '';
    // setRowData(data);
    setLoading(true);

  }, []);
  return loading ? (
    <div
      style={{
        height: "calc(100vh - 18rem)",
        width: "100%",
        backgroundColor: "inherit"
      }}
      className="ag-theme-balham common-ag-grid-table custom-table-header"
    >
      <AgGridReact
        defaultColDef={{ flex: 1 }}
        masterDetail={true}
        detailCellRendererParams={{
          detailGridOptions: {
            columnDefs: [
              { field: 'callId' },
              { field: 'direction' },
              {
                field: 'number',
                minWidth: 150,
              },
              {
                field: 'duration',
                valueFormatter: "x.toLocaleString() + 's'",
              },
              {
                field: 'switchCode',
                minWidth: 150,
              },
            ],
            defaultColDef: { flex: 1 },
          },
          getDetailRowData: function (params) {
            params.successCallback([{
              "name": "susan",
              "callId": 555,
              "duration": 72,
              "switchCode": "SW3",
              "direction": "Out",
              "number": "(00) 88542069"
            }, {
              "name": "susan",
              "callId": 556,
              "duration": 61,
              "switchCode": "SW3",
              "direction": "In",
              "number": "(01) 7432576"
            }]);
          }
        }}
        columnDefs={columnData}
        rowData={rowDataSample}
        rowSelection={rowSelection}
        onFirstDataRendered={autoSizeAllColumns}
        rowHeight="60"
        headerHeight="65"
      ></AgGridReact>
    </div >
  ) : (
    <Loader />
  );
};

export default TicketSubstitute;
