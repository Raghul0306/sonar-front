import React, { useContext } from "react";
import { useTranslation } from "react-i18next";
import UserContext from "../../contexts/user.jsx";
import defaultLogo from "../../assets/logo-placeholder.png";

import Card from "antd/es/card";

const ChannelCards = (props) => {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const channels = props;
 
  let order_detail = props && props.orderDetails ? props.orderDetails : {};
  let broker_order_data = order_detail.broker_order ? order_detail.broker_order : {};
  let channel_data = broker_order_data.channel ? broker_order_data.channel : {};
  const channel_name = channel_data && channel_data.channelname ? channel_data.channelname : '-';
  
  const order_date = order_detail && order_detail.orderdate ? order_detail.orderdate : '-';
  const order_state = order_detail.order_status && order_detail.order_status.orderstatusname ? order_detail.order_status.orderstatusname : '-';
  const order_id = order_detail && order_detail.marketplaceorderid ? order_detail.marketplaceorderid : '-';

  return (

    <Card
      className="order-detail-channel-card"
    >
      <div className="pie-chart-card--body" style={{ textAlign: "center" }}>
      {(props.logoImageData==null || props.logoImageData==undefined || props.logoImageData=="" ) && props.logoImageData!==false &&
            <img
              src={defaultLogo}
              style={{ width: 100 }}
              alt={"Yadara"}
          />
        }
        {props && props.logoImageData && 
          <img
            src={`data:image/jpeg;base64,${props.logoImageData}`}
            style={{ width: 100, paddingBottom:"15px" }}
            alt={"Yadara"}
          /> 
        }
        { props.logoImageData===false &&
            <img
            src={"https://i.ibb.co/xzZmGn9/mytickets-logo.png"}
            style={{ width: 100 }}
            alt={"Yadara"}
          />
        }
      </div>
      <div className="graph-text-body">
        <div className="list-details-view">
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Channel: </h5>
          </div>
          <div className="right-side-details">
            <h5 className="stylefont-weight-medium">{channel_name} </h5>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Order ID: </h5>
          </div>
          <div className="right-side-details">
            <h5 className="stylefont-weight-medium">{order_id}</h5>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Order Date:</h5>
          </div>
          <div className="right-side-details">
            <h5 className="stylefont-weight-medium">{order_date}</h5>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">PO ID: </h5>
          </div>
          <div className="right-side-details">
            <h5 className="stylefont-weight-medium">5064821</h5>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Payout Date:</h5>
          </div>
          <div className="right-side-details">
            <h5 className="stylefont-weight-medium">06/04/2021</h5>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Payout Status: </h5>
          </div>
          <div className="right-side-details">
            <button className="btn delivery-detail-btn">Delivery </button>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Order Status:</h5>
          </div>
          <div className="right-side-details">
            <button className="btn delivery-detail-btn" title={order_state} >{order_state} </button>
          </div>
        </div>
      </div>
    </Card>
  );
};

export default ChannelCards;
