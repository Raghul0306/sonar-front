import React, { useState, useEffect, useContext } from "react";
import {
  Col
} from "shards-react";

import { useTranslation } from "react-i18next";
import UserContext from "../../contexts/user.jsx";
import Card from 'antd/es/card';


const DeliveryStatusCard = (props) => {
  const [show, setShow] = useState(true);
  const { user } = useContext(UserContext);

  const { t } = useTranslation();
  const deliveryDetails = props.deliveryDetails;
  const brokerOrderDetails = deliveryDetails.broker_order_detail ? deliveryDetails.broker_order_detail : [];
  var ticketTypeName = brokerOrderDetails.ticket_type && brokerOrderDetails.ticket_type[0] ? brokerOrderDetails.ticket_type[0].name : "";
  if(ticketTypeName==null){
    ticketTypeName = "";
  }
  const listingGroupDetails = brokerOrderDetails.listing_groups ? brokerOrderDetails.listing_groups[0] : [];
  
  const inHand = listingGroupDetails && listingGroupDetails.in_hand ? listingGroupDetails.in_hand : "-";
  const deliveryAt = brokerOrderDetails && brokerOrderDetails.delivery_at ? brokerOrderDetails.delivery_at : "-";
  const ticketingSystemDetails = listingGroupDetails && listingGroupDetails.ticketing_systems ? listingGroupDetails.ticketing_systems : [];
  
  const ticketingSystemName = ticketingSystemDetails[0] && ticketingSystemDetails[0].ticketing_system_name ? ticketingSystemDetails[0].ticketing_system_name : '-';
  const ticketingSystemAcc = ticketingSystemDetails[0] && ticketingSystemDetails[0].ticketing_system_accounts ? ticketingSystemDetails[0].ticketing_system_accounts.account_name : '-';
  const deliveryStatusDetails = brokerOrderDetails && brokerOrderDetails.delivery_statuses ? brokerOrderDetails.delivery_statuses[0] : {};
  const deliveryStatusName = deliveryStatusDetails && deliveryStatusDetails.name ? deliveryStatusDetails.name : "-";

  useEffect(() => {
  }, []);

  return (
    <div>

      <div className="small-card-body">
        <Card
          className="order-detail-channel-card"
          title={ticketTypeName+" Delivery Status"}
          extra={<a  className="event-status-btn" style={{ textTransform: 'capitalize' }}>{deliveryStatusName}</a>}
        >

          <div className="top-performer-list">
            <div className="delivery-status-grid">
              <div className="Left-delivery-grid stylefont-weight-medium"><h6>In-Hand Date :</h6></div>
              <div className="right-delivery-grid stylefont-weight-bold"><h6>{inHand}</h6></div>
              <div className="Left-delivery-grid stylefont-weight-medium"><h6>Delivery Date :</h6> </div>
              <div className="right-delivery-grid stylefont-weight-bold"><h6>{deliveryAt}</h6></div>
              <div className="Left-delivery-grid stylefont-weight-medium"><h6>Ticket System :</h6> </div>
              <div className="right-delivery-grid stylefont-weight-bold"><h6>{ticketingSystemName}</h6> </div>
              <div className="Left-delivery-grid stylefont-weight-medium"><h6>Ticket System Account :</h6> </div>
              <div className="right-delivery-grid stylefont-weight-bold"><h6>{ticketingSystemAcc}</h6> </div>
            </div>
          </div>
        </Card>
      </div>
    </div>
  );
};

export default DeliveryStatusCard;
