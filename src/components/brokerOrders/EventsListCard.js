import React, { useState, useEffect, useContext } from "react";

import { useTranslation } from "react-i18next";

import UserContext from "../../contexts/user.jsx";
import Card from "antd/es/card";

// import * as moment from "moment";
import * as moment from "moment-timezone";

const EventsListCard = (props) => {
  const [show, setShow] = useState(true);
  const { user } = useContext(UserContext);
  const { t } = useTranslation();

  let brokerOrderDetails = props.eventDetails && props.eventDetails.broker_order_detail ? props.eventDetails.broker_order_detail : {};


  let listingGroupDetails = brokerOrderDetails && brokerOrderDetails.listing_groups && brokerOrderDetails.listing_groups ? brokerOrderDetails.listing_groups[0] : {};
  let eventDetails = brokerOrderDetails && brokerOrderDetails.events && brokerOrderDetails.events ? brokerOrderDetails.events[0] : {};
  let venues = brokerOrderDetails && brokerOrderDetails.events && brokerOrderDetails.events[0].venues ? brokerOrderDetails.events[0].venues[0] : {};
  let listing_groups_details = { section: '', row: '', seats: '', price: '', margin: '', quantity: '', listing_id: '', venue: '' };
  if (listingGroupDetails) {
    listing_groups_details.section = listingGroupDetails.section;
    listing_groups_details.row = listingGroupDetails.row;
    listing_groups_details.seats = listingGroupDetails.seat_start ? listingGroupDetails.seat_start + ' - ' + listingGroupDetails.seat_end : '-';
    listing_groups_details.price = listingGroupDetails.price ? '$ ' + parseFloat(listingGroupDetails.price).toFixed(2) : 0;
    listing_groups_details.margin = listingGroupDetails.margin ? listingGroupDetails.margin : 0;
    listing_groups_details.listing_id = listingGroupDetails.id;
    listing_groups_details.quantity = listingGroupDetails.quantity;
    listing_groups_details.venue = (venues.name ? venues.name + ', ' : '') + (venues.city ? venues.city + ', ' : '') + (venues.state ? venues.state + ', ' : '') + venues.country;
    listing_groups_details.timezone = venues.timezone;
  }



  return (

    <div className="small-card-body">
      <Card
        className="order-detail-channel-card event-status-card"
        title="Event Status"
        extra={<a className="event-status-btn">{eventDetails.event_status_name ? eventDetails.event_status_name : ''}</a>}
      >
        <div className="event-card-body">
          <div className="event-detail-section">
            <div className="d-flex">
              <h3 className="order-detail-event-title stylefont-weight-bold">{eventDetails.name ? eventDetails.name : ''}</h3>
              <h5 className="order-detail-event-id stylefont-weight-medium">{eventDetails.id ? eventDetails.id : ''}</h5>
            </div>

            <h4 className="event-time-date stylefont-weight-bold primary-color">
              {/* {eventDetails.date ? moment.utc(eventDetails.date).local().format("dddd MMM D, YYYY @ h:mm A ") : ''}{moment && moment.tz.guess() && moment.tz.guess().length && moment.tz(moment.tz.guess()).zoneAbbr().length ? moment.tz(moment.tz.guess()).zoneAbbr() : ''} */}

              {eventDetails.date ?
                (listing_groups_details.timezone ? moment.utc(eventDetails.date).tz(listing_groups_details.timezone).format("dddd MMM D, YYYY @ h:mm A z") : moment.utc(eventDetails.date).format("dddd MMM D, YYYY @ h:mm A z")) : ''
              }




            </h4>

            <h4 className="event-location stylefont-weight-bold secondary-color">{listing_groups_details.venue}</h4>
          </div>
          <div className="seat-row-order-details">
            <div className="seat-section-details">
              <table className="table">
                <tr>
                  <td className="stylefont-weight-medium">Sec: <span className="blue-green-txt stylefont-weight-medium">{listing_groups_details.section}</span></td>
                  <td className="stylefont-weight-medium">Row: <span className="blue-green-txt stylefont-weight-medium">{listing_groups_details.row}</span></td>
                  <td className="stylefont-weight-medium">Seats: <span className="blue-green-txt stylefont-weight-medium">{listing_groups_details.seats}</span></td>
                </tr>
                <tr>
                  <td className="stylefont-weight-medium">Price: <span className="cyan-green-txt stylefont-weight-bold">{listing_groups_details.price}</span></td>
                  <td className="stylefont-weight-medium">Margin: <span className="cyan-green-txt stylefont-weight-bold">{listing_groups_details.margin ? parseFloat(listing_groups_details.margin).toFixed(2) + ' %' : '0'}</span></td>
                  <td className="stylefont-weight-medium">Quantity: <span className="cyan-green-txt stylefont-weight-bold">{listing_groups_details.quantity}</span></td>
                </tr>
                <tr>
                  <td colSpan="2">
                    <div className="d-flex"><span className="stylefont-weight-medium">Tags:</span>
                      <div className="tags-list">
                        {listingGroupDetails.listing_tags && listingGroupDetails.listing_tags.length > 0 && listingGroupDetails.listing_tags.map((tags_name) => (
                          tags_name.tags && tags_name.tags.name ?
                            <div className="list-tags-item  stylefont-weight-semibold"><span>
                              {tags_name.tags.name}
                            </span></div> : ''
                        )
                        )
                        }
                      </div>
                    </div>
                  </td>
                  <td className="stylefont-weight-medium">Listing Group ID: <span className="listing-id-txt stylefont-weight-semibold">{listing_groups_details.listing_id}</span></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};

export default EventsListCard;
