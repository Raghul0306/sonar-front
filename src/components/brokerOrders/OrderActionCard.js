import React, { useContext } from "react";
import { useTranslation } from "react-i18next";

import UserContext from "../../contexts/user.jsx";


import Card from "antd/es/card";


const toolTipStyle = {
  fontSize: "18px",
  marginLeft: "5px",
  paddingBottom: "3px",
  color: "rgba(89,89,89, 0.4)"
};

const OrderActionCard = () => {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  
  
  return (
      
      <Card 
        className="order-detail-channel-card order-action-section"
        title="Order Actions" 
      >
        <div className="graph-text-body">
          <button className="btn substitute-ticket-btn stylefont-weight-bold" style={{width:"100%"}} >Substitute Tickets</button>
          <button className="btn order-cancel-btn stylefont-weight-bold" style={{width:"100%"}} >Order Cancellation</button>
          <button className="btn post-event-btn stylefont-weight-bold" style={{width:"100%"}} >Postponed Event</button>
          <button className="btn cancel-event-btn stylefont-weight-bold" style={{width:"100%"}} >Cancelled Event</button>
        </div>
      </Card>
  );
};

export default OrderActionCard;
