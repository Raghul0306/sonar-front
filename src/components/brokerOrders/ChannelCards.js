import React, { useEffect, useContext } from "react";
import { useTranslation } from "react-i18next";
import { order_status } from '../../constants/constants'
import UserContext from "../../contexts/user.jsx";
import defaultLogo from "../../assets/logo-placeholder.png"

import { useHistory } from "react-router-dom";
import Card from "antd/es/card";

const ChannelCards = (props) => {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const channels = props;
  let brokerOrderDetails = props.orderDetails && props.orderDetails.broker_order_detail ? props.orderDetails.broker_order_detail : {};
  let channel_data = brokerOrderDetails && brokerOrderDetails.channels ? brokerOrderDetails.channels[0] : {};
  const channel_name = channel_data && channel_data.channel_name ? channel_data.channel_name : '-';
  const order_id = brokerOrderDetails && brokerOrderDetails.id ? brokerOrderDetails.id : '-';
  const order_date = brokerOrderDetails && brokerOrderDetails.order_date;
  const order_state = brokerOrderDetails && brokerOrderDetails.order_status_id ? order_status[brokerOrderDetails.order_status_id] : order_status[0];
  const listing_group_id = brokerOrderDetails && brokerOrderDetails.listing_group_id ? brokerOrderDetails.listing_group_id : '-';

  const history = useHistory();
  useEffect(() => {    
    let userActions = {
      ticketType: '',
      retransfer: '',
      order_id: order_id,
      userContext: user,
      invoice:true // need to change the value
    };
    history.push({ pageTitle: "Order Details", pageSubTitle: "title", redirectPath: "broker-orders", orderDetails: userActions });
  }, [brokerOrderDetails]);
  return (

    <Card
      className="order-detail-channel-card"
    >
      <div className="pie-chart-card--body" style={{ textAlign: "center",marginBottom:"1.5rem" }}>
        {(props.logoImageData==null || props.logoImageData==undefined || props.logoImageData=="" ) && props.logoImageData!==false &&
              <img
                src={defaultLogo}
                style={{ width: 100 }}
                alt={"Yadara"}
            />
        }
        {props && props.logoImageData && 
          <img
            src={`data:image/jpeg;base64,${props.logoImageData}`}
            style={{ width: 100, paddingBottom:"15px" }}
            alt={"Yadara"}
          /> 
        }
        { props.logoImageData===false &&
            <img
            src={"https://i.ibb.co/xzZmGn9/mytickets-logo.png"}
            style={{ width: 100 }}
            alt={"Yadara"}
          />
        }
      </div>
      <div className="graph-text-body">
        <div className="list-details-view">
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Channel: </h5>
          </div>
          <div className="right-side-details">
            <h5 className="stylefont-weight-bold">{channel_name} </h5>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Order ID: </h5>
          </div>
          <div className="right-side-details">
            <h5 className="stylefont-weight-bold">{order_id}</h5>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Order Date:</h5>
          </div>
          <div className="right-side-details">
            <h5 className="stylefont-weight-bold">{order_date}</h5>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">PO ID: </h5>
          </div>
          <div className="right-side-details">
            <h5 className="stylefont-weight-bold">5064821</h5>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Listing ID:</h5>
          </div>
          <div className="right-side-details">
            <h5 className="stylefont-weight-bold">{listing_group_id}</h5>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Payout Date:</h5>
          </div>
          <div className="right-side-details">
            <h5 className="stylefont-weight-bold">06-04-2021</h5>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Payout Status: </h5>
          </div>
          <div className="right-side-details">
            <button className="btn delivery-detail-btn stylefont-weight-bold">Delivery </button>
          </div>
          <div className="left-side-details">
            <h5 className="stylefont-weight-medium">Order Status:</h5>
          </div>
          <div className="right-side-details">
            <button className="btn delivery-detail-btn stylefont-weight-bold" title={order_state} >{order_state} </button>
          </div>
        </div>
      </div>
    </Card>
  );
};

export default ChannelCards;
