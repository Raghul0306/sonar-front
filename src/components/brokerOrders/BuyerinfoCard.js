import React, { useState, useEffect, useContext } from "react";

import { useTranslation } from "react-i18next";
import UserContext from "../../contexts/user.jsx";
import Card from "antd/es/card";

const BuyerinfoCard = (props) => {
  const [show, setShow] = useState(true);
  const { user } = useContext(UserContext);

  const { t } = useTranslation();

  let buyerDetails = { buyer_name: '', buyer_email: '', buyer_phone: ''};

  let buyer_data =  props.buyerDetails && props.buyerDetails.broker_order_detail ? props.buyerDetails.broker_order_detail  : null;

  if(buyer_data){
    buyerDetails.buyer_name = buyer_data.buyerUsers ? buyer_data.buyerUsers[0].first_name: '';
    buyerDetails.buyer_email = buyer_data.buyerUsers ? buyer_data.buyerUsers[0].email: '';
    buyerDetails.buyer_phone = buyer_data.buyerUsers ? buyer_data.buyerUsers[0].phone_number: '';
  }

  useEffect(() => {
  }, []);

  return (
        <div className="small-card-body">
            <Card
                className="order-detail-channel-card" 
                title="Buyer Information" 
                >
                
                <div className="top-performer-list">    
                    <div className="delivery-status-grid">
                        <div className="Left-delivery-grid stylefont-weight-medium"><h6>Buyer Name :</h6></div>
                        <div className="right-delivery-grid stylefont-weight-bold"><h6>{buyerDetails.buyer_name}</h6></div>
                        <div className="Left-delivery-grid stylefont-weight-medium"><h6>Buyer Email :</h6></div>
                        <div className="right-delivery-grid stylefont-weight-bold"><h6>{buyerDetails.buyer_email}</h6></div>
                        <div className="Left-delivery-grid stylefont-weight-medium"><h6>Buyer Phone :</h6></div>
                        <div className="right-delivery-grid stylefont-weight-bold"><h6>{buyerDetails.buyer_phone}</h6></div>
                    </div>
                </div> 
                
            </Card>
        </div>
  );
};

export default BuyerinfoCard;
