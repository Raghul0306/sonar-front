import React from "react";
import Form from 'antd/es/form';
import UserContext from "../contexts/user.jsx";
import { getToken, getUser, getCount } from "../services/checkAuthService";
import Cookies from "universal-cookie";

import { withTranslation } from "react-i18next";
import canSeeConsoleMessages from "../helpers/canSeeConsoleMessages";
import LogoImage from "../assets/mytickets_logo-new.png";
import { sendMagicLink, changePassword, checkValidResetToken } from "../services/userService";
import history from '../helpers/history';
import styled from 'styled-components'
import { User } from '@styled-icons/heroicons-outline/User'
import { Lock } from '@styled-icons/boxicons-regular/Lock'
import { Envelope } from '@styled-icons/bootstrap/Envelope'
import { fieldTypes } from "../constants/constants";
import * as moment from "moment-timezone";
import { Redirect } from "react-router-dom";

const UserIcon = styled(User)`
  color: #4a88c5;
  width:30px;
  height:30px;
`
const LockIcon = styled(Lock)`
  color: #4a88c5;
  width:30px;
  height:30px;
`
const EnvelopeIcon = styled(Envelope)`
  color: #4a88c5;
  width:30px;
  height:30px;
`
const cookies = new Cookies();

class NormalLoginForm extends React.Component {
  state = {
    formError: false,
    signIn: true,
    resetPassword: false,
    changePassword: false,
    mailSentSuccess: false,
    fieldType: fieldTypes.password,
    resetPasswordSuccess: false,
    newPasswordFieldType: fieldTypes.password,
    confirmPasswordFieldType: fieldTypes.password,
    confirmPasswordNotmatch: false
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
           getToken({ email: values.username, password: values.password }).then(
             tokenResponse => {
            if (tokenResponse && tokenResponse.id) {
              cookies.set("accessToken", tokenResponse.token, { path: "/" });
              getUser(tokenResponse.id).then(response => {
                return response.id ? this.setUserAndCookie(response) : cookies.set("accessToken", '', { path: "/", maxAge: 0 })
              }
              );
            } else {
              //do nothing for now. Bad login
              this.props.form.resetFields();
              this.setState({ formError: true });
            }
          }
        );
      }
    });
  };

  resetPassword = async (e) => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {

      if(!err && values.password != values.confirmpassword){
        await this.setState({confirmPasswordNotmatch:true})
      }else{
        await this.setState({confirmPasswordNotmatch:false})
      }

         if (!err && this.state.confirmPasswordNotmatch == false) {
            values.token = window.location.href.split("/").pop();
            const status = await changePassword(values);
              this.setState({ changePassword: false, signIn: false, resetPassword: false, resetPasswordSuccess: true });
         }
    });
  }

  toggleLoginPage = async (e) => {
    this.setState({ signIn: true, resetPasswordSuccess: false })
    history.push('/');
  }

  sendResetPasswordLink = async e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        // call the API function to send magic link if we have an email.
        var response = await sendMagicLink(values.email);
        // let user know if we have an email on file that matches, they should get one.
        if (response.reset_status) {
          this.setState({ changePassword: false, signIn: false, resetPassword: false, mailSentSuccess: true });
        }
      }
    });
  };

  redircectToHome = async e => {
    await this.setState({resetPasswordSuccess:false});
    history.push('/');
    window.open('/','_self');
  };

  setUserAndCookie(user) {
    // TODO: Add expiration here
    this.setState({ formError: false });
    cookies.set("userObject", user, { path: "/" });
    this.context.setUser(user);
    getCount(user).then(response => null);
    history.push('/');

  }

  async componentDidMount() {
    const currentPath = window.location.pathname;
    let value = this.context;
    if (currentPath.includes('change-password')) {
      const token = window.location.href.split("/").pop();
      if (token.length == 64) {
        let checkToken = await checkValidResetToken(token);//Verifing the token - valid or not
        if (checkToken && checkToken.id) {
          this.setState({ changePassword: true, signIn: false, resetPassword: false });
        }
        else {
          this.setState({ changePassword: false, signIn: true, resetPassword: false });
        }
      }
    }


    return canSeeConsoleMessages(value.user)
      ? console.log(value, "this one")
      : null;
    /* perform a side-effect at mount using the value of MyContext */
  }
  changeType(type) {

    if (type == fieldTypes.password) {
      this.setState({ fieldType: fieldTypes.text });
    }
    else {
      this.setState({ fieldType: fieldTypes.password });
    }

  }
  newPasswordChangeType(type) {
    if (type == fieldTypes.password) {
      this.setState({ newPasswordFieldType: fieldTypes.text });
    }
    else {
      this.setState({ newPasswordFieldType: fieldTypes.password });
    }

  }
  confirmPasswordChangeType(type) {
    if (type == fieldTypes.password) {
      this.setState({ confirmPasswordFieldType: fieldTypes.text });
    }
    else {
      this.setState({ confirmPasswordFieldType: fieldTypes.password });
    }

  }
  render() {
    const { getFieldDecorator } = this.props.form;
    if(this.context.user) return <Redirect to="/home" />;
    return (
      <>
        <div
          style={{
            display: "flex",
            flexDirection: 'column',
            alignItems: "center",
            justifyContent: "center",
            height: "100vh"
          }}
        >
          <div className="login-page-copyright">
            {this.state.signIn && (
              <div className="mb-4 logo-overlay card">
                <div className="card-header border-bottom logo-new-img">
                  <h6 className="m-0">
                    <img
                      src={LogoImage}
                      alt={this.props.t("title")}
                    />
                  </h6>
                </div>
                <div className="card-body">
                  {this.state.formError && <div>{this.props.t("login.error")}</div>}
                  <div className="login-new-section">
                        <Form onSubmit={this.handleSubmit} className="login-form">
                              <div className="psw-show-field form-group">
                                {getFieldDecorator("username", {
                                  rules: [
                                    {
                                      required: true,
                                      message: this.props.t("login.username.required")
                                    }
                                  ]
                                })(
                                  <input className="form-control"
                                      
                                    placeholder={this.props.t(
                                      "login.username.placeholder"
                                    )}
                                  />
                                )}
                                <span className="input-icon"><UserIcon /></span>
                              </div>
                            {getFieldDecorator("password", {
                              rules: [
                                {
                                  required: true,
                                  message: this.props.t("login.password.required")
                                }
                              ]
                            })(
                              <div className="psw-show-field form-group">
                                <input className="form-control"
                                  type={this.state.fieldType}
                                  placeholder={this.props.t(
                                    "login.password.placeholder"
                                  )}
                                />
                                <div onClick={(e) => this.changeType(this.state.fieldType)} className="psw-show-btn">{this.state.fieldType == fieldTypes.password ? 'Show' : 'Hide'}</div>
                              
                                <span className="input-icon"><LockIcon /></span>
                                </div>
                            )}
                            <div className="login-btn-footer">
                              <button
                                type="primary"
                                htmlType="submit"
                                className="login-form-button"
                                style={{ width: "100%" }}
                              >
                                {this.props.t("login.title")}
                              </button>
                            </div>
                          {/* <div
                            onClick={() =>
                              this.setState({ signIn: false, resetPassword: true })
                            }
                            style={{
                              width: "100%",
                              textAlign: "right",
                              cursor: "pointer"
                            }}
                            sm={4}
                          >
                            {this.props.t('register.title')}
                          </div> */}
                          <div className="forgot-textstyle"
                            onClick={() =>
                              this.setState({ signIn: false, resetPassword: true })
                            }
                            style={{
                              position: "relative",
                              textAlign: "center",
                              width: "100%",
                              cursor: "pointer"
                            }}
                          >
                            {this.props.t("userInfo.password.reset")}
                          </div>
                        </Form>
                  </div>
                </div>
              </div>

            )}

            {this.state.changePassword && (
              <div className="login-new-section changes-psw-card-section">
                <div className="mb-4 logo-overlay card">
                  <div className="card-header border-bottom logo-new-img">
                    <h6 className="m-0">
                      <img
                        src={LogoImage}
                        alt={this.props.t("title")}
                      />
                    </h6>
                  </div>
                  <div className="card-body">
                    {this.state.formError && <div>{this.props.t("changePassword.error")}</div>}
                    <div className="login-new-section mb-4">
                      <Form onSubmit={this.resetPassword} className="login-form reset-form">
                        {getFieldDecorator("password", {
                          rules: [
                            {
                              required: true,
                              message: this.props.t("changePassword.password.required")
                            },
                            {
                              pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
                              message: 'Must Contain 8 Characters, at least 1 Uppercase, 1 Lowercase, 1 Number and 1 Special Character',
                          }
                          ]
                        })(
                          <div className="psw-show-field form-group">
                          <label className="password-mail-reset">New Password</label>
                              <input className="form-control"
                                // prefix={
                                //   <LockIcon />
                                // }
                                name="password"
                                type={this.state.newPasswordFieldType == fieldTypes.password ? fieldTypes.password : fieldTypes.text}
                                name={fieldTypes.password}
                                placeholder={this.props.t(
                                  "changePassword.password.placeholder"
                                )}
                                hasFeedback
                              />
                          <div onClick={(e) => this.newPasswordChangeType(this.state.newPasswordFieldType)} className="psw-show-btn">{this.state.newPasswordFieldType == fieldTypes.password ? 'Show' : 'Hide'}</div>
                          </div>
                        )}
                        {getFieldDecorator("confirmpassword", {
                          rules: [
                            {
                              required: true,
                              message: this.props.t("changePassword.confirmpassword.required")
                            }
                          ]
                        })(
                          <div className="psw-show-field form-group">
                          <label className="password-mail-reset">Confirm New Password</label>
                          <input className="form-control"
                            // prefix={
                            //   <LockIcon />
                            // }
                            name="confirmpassword"
                            type={this.state.confirmPasswordFieldType == fieldTypes.password ? fieldTypes.password : fieldTypes.text}
                            placeholder={this.props.t(
                              "changePassword.confirmpassword.placeholder"
                            )}
                            dependencies={['password']}
                            hasFeedback
                            onChange={(e) => this.setState({confirmPasswordNotmatch:false})}
                          />
                          <div onClick={(e) => this.confirmPasswordChangeType(this.state.confirmPasswordFieldType)} className="psw-show-btn">{this.state.confirmPasswordFieldType == fieldTypes.password ? 'Show' : 'Hide'}</div>
                          </div>
                        )}
                        {this.state.confirmPasswordNotmatch ? <span className="change-password-validation"> The confirm password do not match! </span> :''}
                        <div className="login-btn-footer">
                          <button
                            type="primary"
                            htmlType="submit"
                            className="login-form-button"
                            style={{ width: "100%" }}
                          >
                            {this.props.t("register.description.resetPassword")}
                          </button>
                        </div>
                    </Form>
              </div>
            </div>
            </div>
          </div>
        )}

        {this.state.resetPasswordSuccess && (
          <div
            className="success-mail-section reset-psw-section"
            style={{ width: "100%" }}>
              <div className="mb-4 card logo-overlay">
                <div className="border-bottom logo-new-img card-header">
                  <h6 className="m-0">
                    <img
                      src={LogoImage}
                      alt={this.props.t("title")}
                    />
                  </h6>
                </div>
                <div className="card-body">
                  <div class="check-mail-section">
                    <h4>Success!</h4>
                    <p>Your password has been successfully reset. Continue to sign into your account..</p>
                  </div>
                </div>
                <div className="card-footer">
                      <button
                        type="primary"
                        htmlType="button"
                        className="login-form-button"
                        style={{ width: "100%" }}
                        onClick={this.redircectToHome}
                      >
                        Sign In
                      </button>
                </div>
              </div>
          </div>
        )}

        {this.state.resetPassword && (
          <Form
            onSubmit={this.sendResetPasswordLink}
            className="login-form reset-psw-section"
            style={{ width: "100%" }}
          >
            <div className="changes-psw-card-section">
              <div className="mb-4 logo-overlay card">
                <div className="border-bottom logo-new-img card-header">
                  <h6 className="m-0">
                    <img
                      src={LogoImage}
                      alt={this.props.t("title")}
                    />
                  </h6>
                </div>
                <div className="card-body">
                  <h4 className="reset-txt-Qes">{this.props.t("register.description.resetHeading")}</h4>
                    {getFieldDecorator("email", {
                      rules: [
                        {
                          type: "email",
                          required: true,
                          message: this.props.t("register.email.required")
                        }
                      ]
                    })(
                      <div className="psw-show-field">
                        <input className="form-control"
                          type="email"
                          placeholder={this.props.t("register.email.placeholder")}
                        />
                      <span className="input-icon"><EnvelopeIcon /></span>
                      </div>
                    )}
                </div>
                <div className="card-footer">
                  <button
                    type="primary"
                    htmlType="submit"
                    className="login-form-button"
                    style={{ width: "100%" }}
                  >
                    Reset Password
                  </button>
                      <div className="forgot-textstyle"
                        onClick={() =>
                          this.setState({ signIn: true, resetPassword: false, mailSentSuccess:false })
                        }
                        style={{
                          width: "100%",
                          textAlign: "center",
                          cursor: "pointer"
                        }}
                      >
                        {this.props.t("register.back")}
                      </div>
                </div>
              </div>
            </div>
          </Form>
        )}


        {this.state.mailSentSuccess && (
          <div
            onSubmit={this.sendResetPasswordLink}
            className="success-mail-section reset-psw-section"
            style={{ width: "100%" }}>
              <div className="mb-4 logo-overlay card">
                <div className="border-bottom logo-new-img card-header">
                  <h6 className="m-0">
                    <img
                      src={LogoImage}
                      alt={this.props.t("title")}
                    />
                  </h6>
                </div>
                <div className="card-body">
                  <div class="check-mail-section">
                    <h4>Please check your email</h4>
                    <p>Instructions on how to reset your password have been sent to your email address.</p>
                  </div>
                </div>
                <div className="card-footer">
                      <div className="forgot-textstyle"
                        onClick={() =>
                          this.setState({ signIn: true, mailSentSuccess: false })
                        }
                        style={{
                          width: "100%",
                          textAlign: "center",
                          cursor: "pointer",
                          marginTop:"15px"
                        }}
                        sm={4}
                      >
                        {this.props.t("register.back")}
                      </div>
                </div>
              </div>
          </div>
        )}
          </div>
          


          <footer class="main-footer d-flex p-2 px-3 bg-white border-top" style={{ marginTop: 'auto', width: '100%' }}>
            <div class="container-fluid">
              <div class="row">
                 <span class="copyright ml-auto my-auto mr-2">Copyright &copy; {moment.utc(new Date).format("YYYY")} Phunnel Inc</span>
              </div>
            </div>
          </footer>
        </div>

      </>
    );
  }
}
NormalLoginForm.contextType = UserContext;
export default Form.create({ name: "normal_login" })(
  withTranslation()(NormalLoginForm)
);
