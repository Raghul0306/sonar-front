import React, { Component } from "react";
import loadable from "@loadable/component";
import UserContext from "../../contexts/user.jsx";
import * as moment from "moment";
import CloseRoundedIcon from "@material-ui/icons/CloseRounded";
import Button from 'antd/es/button';
import Loader from "../Loader/Loader";

const AuctionEventSubmission = loadable(() =>
  import("./auctionEventSubmissionForm")
);

export class AddAuctionEvent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: moment(new Date()).format("YYYY-MM-DD"),
      endDate: moment(new Date())
        .add(1, "days")
        .format("YYYY-MM-DD"),
      loading: true
    };
  }

  componentDidMount = () => {
    this.setState({ loading: false });
  };
  render() {
    return !this.state.loading ? (
      <div>
        <div className="modal-floating-cancel-button">
          <Button shape="circle" size="large" onClick={this.props.handleCancel}>
            <CloseRoundedIcon
              style={{
                fontSize: "23px"
              }}
            />
          </Button>
        </div>
        <UserContext.Consumer>
          {context => <AuctionEventSubmission user={context.user.id} />}
        </UserContext.Consumer>
      </div>
    ) : (
      <Loader />
    );
  }
}

export default AddAuctionEvent;
