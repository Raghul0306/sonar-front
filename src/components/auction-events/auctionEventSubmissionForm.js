// Render Prop
import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { writeAuctionEventToDB } from "../../services/auctionEventService";
import Button from 'antd/es/button';
import DatePicker from 'antd/es/date-picker';
import TimePicker from 'antd/es/time-picker';
import * as moment from "moment";
import { useTranslation } from "react-i18next";
import { getPerformersByName } from "../../services/listingsService";
import {
  getAllCategories,
  getBrandsByCategory,
  getSetsByBrandAndCategory
} from "../../services/cardService";
import AutoComplete from 'antd/es/auto-complete';
import Select from 'antd/es/select';
import Input from 'antd/es/input';
import PaletteColors from "../common/PaletteColors";

const { Option } = Select;

const AuctionEventSubmission = props => {
  const { t } = useTranslation();

  const defaultStartDate = moment(new Date());
  const defaultEndDate = moment(new Date()).add(1, "week");
  // declare state, originally used for datepickers
  // note: this is a functional component, so we are using React's useState hook
  const [startDate, setStartDate] = useState(0);
  const [endDate, setEndDate] = useState(0);
  const [startTime, setStartTime] = useState(0);
  const [endTime, setEndTime] = useState(0);
  const [options, setOptions] = useState([]);
  const [performer, setPerformer] = useState([]);
  const [performerList, setPerformerList] = useState([]);
  const [categories, setCategories] = useState([]);
  const [brands, setBrands] = useState([]);
  const [sets, setSets] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(false);
  const [selectedBrand, setSelectedBrand] = useState(false);
  const [selectedSet, setSelectedSet] = useState(false);

  const onSearchInputChange = async searchString => {
    if (searchString.length >= 3) {
      let performersNames = [];
      let list = await getPerformersByName(searchString);
      list.forEach(performer => performersNames.push(performer.name));
      setOptions(searchString.length < 4 ? [] : performersNames);
      setPerformerList(list);
    }
  };

  const handleCategorySelection = async category => {
    setSelectedCategory(category);
    setBrands(await getBrandsByCategory(category));
  };

  const handleBrandSelection = async brand => {
    await setSelectedBrand(brand);
    setSets(await getSetsByBrandAndCategory(brand, selectedCategory));
  };

  const handleSetSelection = async set => {
    setSelectedSet(set);
  };

  const onSelect = async data => {
    let filteredList = performerList.filter(
      performer => performer.name === data
    );
    await setPerformer(filteredList[0]);
  };

  const startDateChanged = function(momentDate, dateString) {
    momentDate = moment(momentDate).format("MM/DD/YYYY");
    console.info(`start date updated to ${momentDate}`);
    setStartDate(momentDate);
  };
  const endDateChanged = function(momentDate, dateString) {
    momentDate = moment(momentDate).format("MM/DD/YYYY");
    console.info(`end date updated to ${momentDate}`);
    setEndDate(momentDate);
  };
  const startTimeChanged = function(momentDate, dateString) {
    momentDate = moment(momentDate).format("HH:mm:ss");
    console.info(`start time updated to ${momentDate}`);
    setStartTime(momentDate);
  };
  const endTimeChanged = function(momentDate, dateString) {
    momentDate = moment(momentDate).format("HH:mm:ss");
    console.info(`end time updated to ${momentDate}`);
    setEndTime(momentDate);
  };

  const { register, handleSubmit } = useForm();
  const onSubmit = async data => {
    let auctionEvent = {};
    auctionEvent.name = data.name.toString();
    auctionEvent.category = parseInt(selectedCategory);
    // also set the date state
    // (if it wasn't for date state, i'd never get paid DATE STATE)
    auctionEvent.startDate = moment(startDate + " " + startTime).format(
      "YYYY-MM-DD HH:mm:ss"
    );
    auctionEvent.endDate = moment(endDate + " " + endTime).format(
      "YYYY-MM-DD HH:mm:ss"
    );
    auctionEvent.minGrade = data.grade.toString();
    // TODO: Powered by Search field we don't have yet, Optional
    if (performer) {
      auctionEvent.performer = parseInt(performer.id);
    }
    if (selectedSet) {
      auctionEvent.set = parseInt(selectedSet);
    }
    if (selectedBrand) {
      auctionEvent.brand = parseInt(selectedBrand);
    }
    let thing = await writeAuctionEventToDB(auctionEvent);
    if (thing) {
      alert(t("auction.addSuccess"));
      document.location.reload();
    }
  };

  useEffect(() => {
    async function fetchData() {
      setCategories(await getAllCategories());
    }

    fetchData();
  }, []);

  return (
    <div className="popup-body">
      <h4 className="purchase-order-form-headings secondary-font-family primary-color">
        {t("auction.description")}
      </h4>
      {/* "handleSubmit" will validate your inputs before invoking "onSubmit" */}
      <form onSubmit={handleSubmit(onSubmit)}>
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "repeat(auto-fit, minmax(250px, 1fr)"
          }}
        >
          <div
            style={{ borderRight: "1px solid #efefef", marginRight: "10px" }}
          >
            <label
              htmlFor="name"
              style={{ display: "block" }}
              className="form-label-userprofile"
            >
              {t("auction.name")}
            </label>
            {/* <input
              type="text"
              placeholder={t("auction.name")}
              name="name"
              style={{ width: "calc(90% - 80px)", marginBottom: "16px" }}
              ref={register({ required: true, maxLength: 80 })}
            /> */}
            <Input
              type="text"
              placeholder={t("auction.name")}
              name="name"
              style={{
                width: "90%",
                marginBottom: "16px",
                border: "1px solid #efefef"
              }}
              ref={register({ required: true, maxLength: 80 })}
            />
            <label
              htmlFor="grade"
              style={{ display: "block" }}
              className="form-label-userprofile"
            >
              {t("auction.minGrade")}
            </label>
            {/* <input
              type="text"
              placeholder={t("auction.minGrade")}
              name="grade"
              style={{ width: "calc(90% - 80px)", marginBottom: "16px" }}
              ref={register({ required: true, maxLength: 12 })}
            /> */}
            <Input
              type="text"
              placeholder={t("auction.minGrade")}
              name="grade"
              style={{
                width: "90%",
                marginBottom: "16px",
                border: "1px solid #efefef"
              }}
              ref={register({ required: true, maxLength: 12 })}
            />

            <label
              htmlFor="grade"
              style={{ display: "block" }}
              className="form-label-userprofile"
            >
              {t("form.startDate")}
            </label>
            <div
              style={{
                display: "grid",
                gridTemplateColumns: "repeat(auto-fit, minmax(50px, 1fr))",
                gridGap: "10px",
                marginBottom: "16px",
                width: "90%"
              }}
            >
              <DatePicker
                name="startDate"
                defaultValue={defaultStartDate}
                format={"MM/DD/YYYY"}
                onChange={startDateChanged}
              />
              <TimePicker
                name="startTime"
                defaultValue={moment("08:00", "HH:mm")}
                format={"HH:mm"}
                style={{ width: "100%" }}
                onChange={startTimeChanged}
              />
            </div>
            <label
              htmlFor="endDate"
              style={{ display: "block" }}
              className="form-label-userprofile"
            >
              {t("form.endDate")}
            </label>
            <div
              style={{
                display: "grid",
                gridTemplateColumns: "repeat(auto-fit, minmax(50px, 1fr))",
                gridGap: "10px",
                marginBottom: "16px",
                width: "90%"
              }}
            >
              <DatePicker
                name="endDate"
                defaultValue={defaultEndDate}
                format={"MM/DD/YYYY"}
                onChange={endDateChanged}
              />
              <TimePicker
                name="endTime"
                defaultValue={moment("17:00", "HH:mm")}
                format={"HH:mm"}
                style={{ width: "100%" }}
                onChange={endTimeChanged}
              />
            </div>
          </div>
          <div>
            <label
              htmlFor="category"
              style={{ display: "block" }}
              className="form-label-userprofile"
            >
              {t("auction.category")}
            </label>
            <Select
              showSearch={true}
              style={{ width: "90%", marginBottom: 8 }}
              onSelect={text => handleCategorySelection(text)}
              placeholder={t("card.category")}
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.props.children
                  .toString()
                  .toLowerCase()
                  .indexOf(input.toLowerCase()) >= 0
              }
            >
              {categories &&
                categories.map(category => (
                  <Option key={category.id}>{category.name}</Option>
                ))}
            </Select>
            <label
              htmlFor="brand"
              style={{ display: "block" }}
              className="form-label-userprofile"
            >
              {t("auction.brand")}
              <span style={{ color: "#bababa", fontSize: "10px" }}>
                {" "}
                ({t("auction.notRequired")})
              </span>
            </label>
            <div>
              <Select
                showSearch={true}
                style={{ width: "90%", marginBottom: 8 }}
                onSelect={text => handleBrandSelection(text)}
                placeholder={t("card.brand")}
                disabled={!selectedCategory}
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children
                    .toString()
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
              >
                {brands &&
                  brands.map(brand => (
                    <Option key={brand.id}>{brand.name}</Option>
                  ))}
              </Select>
            </div>
            <label
              htmlFor="set"
              style={{ display: "block" }}
              className="form-label-userprofile"
            >
              {t("auction.set")}{" "}
              <span style={{ color: "#bababa", fontSize: "10px" }}>
                {" "}
                ({t("auction.notRequired")})
              </span>
            </label>
            <div>
              <Select
                showSearch={true}
                style={{ width: "90%", marginBottom: 8 }}
                onSelect={text => handleSetSelection(text)}
                placeholder={t("card.set")}
                disabled={!selectedBrand}
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children
                    .toString()
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
              >
                {sets &&
                  sets.map(set => (
                    <Option key={set.id}>
                      {set.year} {set.name}
                    </Option>
                  ))}
              </Select>
            </div>
            <label
              htmlFor="performer"
              style={{ display: "block" }}
              className="form-label-userprofile"
            >
              {t("auction.performer")}{" "}
              <span style={{ color: "#bababa", fontSize: "10px" }}>
                {" "}
                ({t("auction.notRequired")})
              </span>
            </label>
            <AutoComplete
              style={{ width: "90%", marginBottom: 8 }}
              dataSource={options}
              onSearch={text => onSearchInputChange(text)}
              onSelect={text => onSelect(text)}
              placeholder={t("listing.searchDescription")}
            />
          </div>
        </div>
        <Button
          style={{
            marginTop: 10,
            backgroundColor: PaletteColors.primary,
            color: "#fff"
          }}
          //disabled={!props.auctionEvent.id}
          disabled={false}
          type="submit"
          onClick={handleSubmit(onSubmit)}
        >
          {/* <i className="material-icons">cloud_upload</i>&nbsp; */}
          {t("auction.add")}
        </Button>
      </form>
    </div>
  );
};

export default AuctionEventSubmission;
