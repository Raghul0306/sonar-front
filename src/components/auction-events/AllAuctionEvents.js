import React, { Component } from "react";
import loadable from "@loadable/component";
import UserContext from "../../contexts/user.jsx";
import Modal from 'antd/es/modal';
import Button from 'antd/es/button';
import { withTranslation } from "react-i18next";
import Loader from "../Loader/Loader";
const AddAuctionEvent = loadable(() => import("./addAuctionEvent"));
const AuctionEventsGrid = loadable(() =>
  import("../datagrid/auctionEventsGrid")
);

export class MyListings extends Component {
  state = { visible: false };
  showAuctionEventsModal = () => {
    this.setState({
      visible: true,
      loading: false
    });
  };

  componentDidMount = () => {
    this.setState({ loading: false });
  };

  handleOk = e => {
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };
  render() {
    return !this.state.loading ? (
      <div>
        <UserContext.Consumer>
          {context => (
            <div>
              <div style={{}}>
                {/* <AddListing/> */}
                <Button type="primary" onClick={this.showAuctionEventsModal}>
                  {this.props.t("auction.add")}
                </Button>
                <Modal
                  title={this.props.t("auction.add")}
                  visible={this.state.visible}
                  onOk={this.handleOk}
                  footer={null}
                  onCancel={this.handleCancel}
                  width="50%"
                  closable={false}                  
                  maskClosable={false}
                >
                  <AddAuctionEvent handleCancel={this.handleCancel} />
                </Modal>
              </div>
              <AuctionEventsGrid />
            </div>
          )}
        </UserContext.Consumer>
      </div>
    ) : (
      <Loader />
    );
  }
}
export default withTranslation()(MyListings);
