import React, { useContext, Component } from "react";
import loadable from "@loadable/component";
import moment from 'moment-timezone'
import UserContext from "../../contexts/user.jsx";
import { Row, Col, FormInput } from "shards-react";
import Switch from 'antd/es/switch';
import Modal from 'antd/es/modal';
import Select from 'react-select'
import Button from 'antd/es/button';
import Loader from "../Loader/Loader";
import "../../assets/formik-form-style.css";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';
import ClipLoader from "react-spinners/ClipLoader";
import { Confirm, Info } from "../../components/modal/common-modal";
import {
  getAddListingsFilter, getAllTicketTypes, allGenreDetails, getUserFilterListings, getAllEventsArrayList, eventSearches, getAllTagList, getAllTicketingSystemList, getAllDisclosureList, getAllAttributeList, getAllSeatTypesList, allTicketingSystemAccounts, getAllChannels, listingattachment, Createattribute, Createdisclosure, Createticketingsystem, getAllSplits, fetchListById, fetchChannelValues, updateListing, fetchAttachmentsById, getFileFromAWS, Createtag, selectedTicketingSystemAccounts, createTicketingSystemAccount
} from "../../services/listingsService.js";
import { getCountries } from "../../services/userService";
import "../../assets/custom-style.css";
import { listingConst, preselectedSeat, monthNamesShort, daysFull, fileTypes, seatTypes, responseStatus, listingGroupInfo, channelManagementConst } from "../../constants/constants";
import {  withRouter } from 'react-router-dom';
//Design Starts

import styled from 'styled-components'
import { FilterCircle } from '@styled-icons/bootstrap/FilterCircle'
import { Search } from '@styled-icons/bootstrap/Search'
import { Speakerphone } from '@styled-icons/heroicons-outline/Speakerphone'
import { Calendar } from '@styled-icons/boxicons-regular/Calendar'
import { Notepad } from '@styled-icons/boxicons-regular/Notepad'
import { DollarCircle } from '@styled-icons/boxicons-regular/DollarCircle'
import { PauseCircle } from '@styled-icons/boxicons-regular/PauseCircle'
import { AddCircle } from '@styled-icons/fluentui-system-regular/AddCircle'
import { Directions } from '@styled-icons/boxicons-regular/Directions'
import { Edit } from '@styled-icons/boxicons-solid/Edit'
import { FileUpload } from '@styled-icons/material-outlined/FileUpload';
import { Add } from '@styled-icons/material-rounded/Add';
import { Minus } from '@styled-icons/heroicons-solid/Minus';
import { Attachment } from '@styled-icons/entypo/Attachment';

//React date picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
//React date picker
// Multi select for day of week
import MultiSelect from "react-multi-select-component";

const { Option } = Select;

const Filter = styled(FilterCircle)`
  color: #00c8c8;
  width:40px;
  height:40px;
`
const SearchIcon = styled(Search)`
  color: #9f9f9f;
  width:20px;
  height:20px;
`
const SpeakerIcon = styled(Speakerphone)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const CalendarIcon = styled(Calendar)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const NotePadIcon = styled(Notepad)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const DollarCircleIcon = styled(DollarCircle)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const PauseCircleIcon = styled(PauseCircle)`
color: #9f9f9f;
width:30px;
height:30px;
`
const AddCircleIcon = styled(AddCircle)`
color: #9f9f9f;
width:30px;
height:30px;
`
const DirectionsIcon = styled(Directions)`
color: #9f9f9f;
width:30px;
height:30px;
`
const EditIcon = styled(Edit)`
color: #9f9f9f;
width:30px;
height:30px;
`
const UploadIcon = styled(FileUpload)`
color: #9f9f9f;
width:30px;
height:30px;
`
const AddIcon = styled(Add)`
color: #2C91EE;
width:30px;
height:30px;
`
const MinusIcon = styled(Minus)`
color: #BEBEBE;
width:30px;
height:30px;
`
const AttachmentIcon = styled(Attachment)`
color: #ffffff;
width:23px;
height:25px;
opacity: 0.7;
`

//Design Ends

export class MyViewListingForms extends Component {
  static contextType = UserContext;
  state = {
    loading: true,
    event_id: '',
    rows: [],
    suggestions: [],
    searchText: '',
    inputList: [],
    channelManagmentInputList: [],
    eventDetails: [],
    selectedEventDate: '',
    attachmentInputList: [],
    attributesInputList: [],
    disclosuresInputList: []
  };

  constructor(props) {
    super(props)
    this.listSearchChange = this.listSearchChange.bind(this);
    this.selectLisitng = this.selectLisitng.bind(this);
    this.toggleAdvanced = this.toggleAdvanced.bind(this);
    this.dateRangeDisplay = this.dateRangeDisplay.bind(this);
    this.advanceFilter = this.advanceFilter.bind(this);
    this.daySelected = this.daySelected.bind(this);
    this.handleRemoveClick = this.handleRemoveClick.bind(this);

    this.appendedFormSectionOnchange = this.appendedFormSectionOnchange.bind(this);
    this.appendedFormRowOnchange = this.appendedFormRowOnchange.bind(this);
    this.appendedFormSeatStartOnchange = this.appendedFormSeatStartOnchange.bind(this);
    this.appendedFormSeatEndOnchange = this.appendedFormSeatEndOnchange.bind(this);
    this.appendedFormPriceOnchange = this.appendedFormPriceOnchange.bind(this);
    this.appendedFormFaceValueOnchange = this.appendedFormFaceValueOnchange.bind(this);
    this.appendedFormCostOnchange = this.appendedFormCostOnchange.bind(this);
    this.appendedFormUnitCostOnchange = this.appendedFormUnitCostOnchange.bind(this);
    this.appendedFormDisclosureOnchange = this.appendedFormDisclosureOnchange.bind(this);
    this.appendedFormAttributeOnchange = this.appendedFormAttributeOnchange.bind(this);
    this.appendedFormPOIDOnchange = this.appendedFormPOIDOnchange.bind(this);
    this.appendedFormTagsOnchange = this.appendedFormTagsOnchange.bind(this);
    this.appendedFormInternalNotesOnchange = this.appendedFormInternalNotesOnchange.bind(this);
    this.appendedFormExternalNotesOnchange = this.appendedFormExternalNotesOnchange.bind(this);
    this.channelManagmentInputarray = this.channelManagmentInputarray.bind(this);
    this.channelManagmentIncressOnchange = this.channelManagmentIncressOnchange.bind(this);
    this.channelManagmentDecressOnchange = this.channelManagmentDecressOnchange.bind(this);
    this.channelManagmentOnchange = this.channelManagmentOnchange.bind(this);
    this.channelManagmentToggleOnchange = this.channelManagmentToggleOnchange.bind(this);
    this.appendedFormAttachmentOnchange = this.appendedFormAttachmentOnchange.bind(this);
    this.appendedFormAttachmentTextOnchange = this.appendedFormAttachmentTextOnchange.bind(this);
    this.AddDisclosureOnclick = this.AddDisclosureOnclick.bind(this);
    this.AddAttributeOnclick = this.AddAttributeOnclick.bind(this);
    this.AddTagOnclick = this.AddTagOnclick.bind(this);
    this.singleSelect = this.singleSelect.bind(this);
    this.unitCostCalculator = this.unitCostCalculator.bind(this);
    this.avoidOnchange = this.avoidOnchange.bind(this);
    this.formUpdate = this.formUpdate.bind(this);
    this.hideSeatNumber = this.hideSeatNumber.bind(this);

    this.AddTicketingSystemOnclick = this.AddTicketingSystemOnclick.bind(this);
    this.AppendModelSubmitOnclick = this.AppendModelSubmitOnclick.bind(this);
    this.AppendModelCancelOnclick = this.AppendModelCancelOnclick.bind(this);
    this.downloadAttachment = this.downloadAttachment.bind(this);
    this.handlesubmitButtonModal = this.handlesubmitButtonModal.bind(this);
    this.handleInfoSubmitButtonModal = this.handleInfoSubmitButtonModal.bind(this);
    this.AddTicketingSystemAccountOnclick = this.AddTicketingSystemAccountOnclick.bind(this);
    this.dropdownFilterOption = this.dropdownFilterOption.bind(this);
    this.toggleSwitch = this.toggleSwitch.bind(this);
    this.removeAttachment = this.removeAttachment.bind(this);

    this.state = {
      listingsData: [],
      filterListingsData: [],
      searchText: '',
      displayText: '',
      startDate: null,
      endDate: null,
      inhand_date: '',
      seat_type_value: preselectedSeat.value,
      advancefilter: false,
      quantities: [],
      avoidMultiEmptyCall: false,
      selectedDays: [],
      quantity_array: [],
      quantity: '0',
      quantity_ga: "",
      singleDate: '',
      searchData: {
        searches: "",
        tags: "",
        section: "",
        row: "",
        quantity: '',
        from_date: "",
        to_date: "",
        day_of_week: "",
        in_hand: "",
        inventory_attached: "",
        ticket_type: "",
        category: ""
      },
      inputList: [{
        section: "",
        row: "",
        seat_start: "",
        seat_end: "",
        price: "0",
        face_value: "0",
        cost: "10",
        unit_cost: "0",
        disclosure: "",
        attribute: "",
        po_id: "",
        tags: "",
        internal_notes: "",
        external_notes: "",
        attachments: [],
        userId: ""
      }],
      eventDetails: [],
      channelManagmentInputList: [],
      attachmentInputList: [],
      inputListValidation: [],
      isAppendModelPopup: {
        'isModalVisible': false,
        'title': '',
        'type': ''
      },
      singleSelect: {
        ticket_type_id: [],
        seat_type_id: [preselectedSeat],
        ticketing_system: [],
        ticketing_system_accounts: [],
        hide_seat_numbers: [],
        splitId: []

      },
      singleinputList: {
        seat_start: [],
        seat_end: [],
      },
      channelManage: [],
      setLoader: false, //loader
      listDataDetails: [],
      listAttachments: [],
      downloadAttachmentState: '',
      attachType: '',
      inhand_disable: false,
      myDBPrice: 0,

      InfoSubmitButtonModal: false,
      infocloseButtonModal: false,
      closeButtonModal: false,
      submitButtonModal: false,
      datafields: "",
      infomodalmsg: "",
      Is_show_localpickup: false,
      disableTicketingAccount: false,
      Is_show_sectiondiv: true,
      updatePurchaseOrder: false
    }
  }

  async componentDidMount() {
    this.setMyPageLoader(true)
    let current_path = window.location.pathname.split("/");
    this.props.history.push({ pageTitle: 'Listing Details', pageSubTitle: 'subtitle', redirectPath: "listings" });

    const userId = this.context.user.id;
    let quantityArray = []
    for (var quantityCount = 0; quantityCount < 100; quantityCount++) {  // Dynamic quantity 1-100
      quantityArray[quantityCount] = quantityCount + 1;
    }

    //Row Array Start
    let rowArray = [];
    for (var rowCount = 0; rowCount < 100; rowCount++) {  // Dynamic quantity 1-100
      rowArray[rowCount] = rowCount + 1;
    }
    //Row Array End

    let userFilterListingsData;
    let ticketTypeDetails;
    let getAllEventsArrayLists;
    let genreDetails;
    let getTags;
    let all_getTags;
    let getTicketingSystems;
    let getDisclosures;
    let getAttributes;
    let getTicketingSystemAccounts;
    let getChannelMarkups;
    let getAllSeatTypes;
    let getCountryList;
    let getSplitsList;
    let all_getDisclosures;
    let all_getAttributes;
    //Event details
    //     var event_details =  await getUserFilterListings({ searches: this.props.listId });

    // if (event_details && event_details.length > 0) {
    //   this.setState({ eventDetails: event_details, selectedEventDate: event_details[0].eventdate, loading: false })
    // } else this.setState({ loading: false });
    ticketTypeDetails = await getAllTicketTypes();
    genreDetails = await allGenreDetails();
    userFilterListingsData = await getAddListingsFilter();
    getAllEventsArrayLists = await getAllEventsArrayList();
    getTags = await getAllTagList(userId);
    all_getTags = await getAllTagList();
    getTicketingSystems = await getAllTicketingSystemList();
    getDisclosures = await getAllDisclosureList(userId);
    all_getDisclosures = await getAllDisclosureList();
    getAttributes = await getAllAttributeList(userId);
    all_getAttributes = await getAllAttributeList();
    getTicketingSystemAccounts = await selectedTicketingSystemAccounts(parseInt(this.props.listId));
    getChannelMarkups = await getAllChannels();
    getChannelMarkups.sort(function (channel, list) { return channel.channel_name == listingConst.yadara ? -1 : list.channel_name == listingConst.yadara ? 1 : 0; });
    getAllSeatTypes = await getAllSeatTypesList();
    getCountryList = await getCountries();
    getSplitsList = await getAllSplits();

    await this.setState({
      ticketTypeDetails: ticketTypeDetails,
      genreDetails: genreDetails,
      filterListingsData: userFilterListingsData,
      quantities: quantityArray,
      getTags: getTags,
      getAllSeatTypes: getAllSeatTypes,
      getTicketingSystems: getTicketingSystems,
      getDisclosures: getDisclosures,
      all_getDisclosures: all_getDisclosures,
      all_getAttributes: all_getAttributes,
      all_getTags: all_getTags,
      getAttributes: getAttributes,
      getTicketingSystemAccounts: getTicketingSystemAccounts,
      getChannelMarkups: getChannelMarkups,
      rows: rowArray,
      getCountryList: getCountryList,
      getSplitsList: getSplitsList,
      userId: this.context.user.id
    });

    //Loading list details start

    const selectedDiscs = [];
    const userselectedDiscs = [];
    const userselectedAttr = [];
    const userselectedTags = [];
    const selectedAttr = [];
    const selectedTags = [];
    let listData = await fetchListById(parseInt(this.props.listId));
    let channelData = await fetchChannelValues(parseInt(this.props.listId));
    listData && listData.disclosureids && getDisclosures && getDisclosures.length > 0
      && getDisclosures.map((item, i) => {
        if (listData.disclosureids.includes(parseInt(item.id))) {
          var combined = { label: item.disclosure_name, value: item.id }
          selectedDiscs.push(combined);
        }
      })
    listData && listData.attributeids && this.state.getAttributes && this.state.getAttributes.length > 0
      && this.state.getAttributes.map((item, i) => {
        if (listData.attributeids.includes(parseInt(item.id))) {
          var combined = { label: item.attribute_name, value: item.id }
          selectedAttr.push(combined);
        }
      })
    listData && listData.tagids && this.state.getTags && this.state.getTags.length > 0
      && this.state.getTags.map((item, i) => {
        if (listData.tagids.includes(parseInt(item.id))) {
          var combined = { label: item.tag_name, value: item.id }
          selectedTags.push(combined);
        }
      })

    /*** based on user id -start*/
    listData && listData.disclosureids && this.state.all_getDisclosures && this.state.all_getDisclosures.length > 0
      && this.state.all_getDisclosures.map((item, i) => {
        if (listData.disclosureids.includes(parseInt(item.id))) {
          var combined = { label: item.disclosure_name, value: item.id }
          // selectedDiscs.push(combined);
          userselectedDiscs.push(combined);
        }
      })


    listData && listData.attributeids && this.state.all_getAttributes && this.state.all_getAttributes.length > 0
      && this.state.all_getAttributes.map((item, i) => {
        if (listData.attributeids.includes(parseInt(item.id))) {
          var combined = { label: item.attribute_name, value: item.id }
          // selectedAttr.push(combined);
          userselectedAttr.push(combined);
        }
      })

    listData && listData.tagids && this.state.all_getTags && this.state.all_getTags.length > 0
      && this.state.all_getTags.map((item, i) => {
        if (listData.tagids.includes(parseInt(item.id))) {
          var combined = { label: item.tag_name, value: item.id }
          // selectedTags.push(combined);
          userselectedTags.push(combined);
        }
      })
    /*** based on user id -end*/


    if (listData) {

      if (listData.in_hand) {
        var inHand = listData.in_hand ? new Date(listData.in_hand[0]) : '';
        let month = inHand.getMonth() + 1
        if (month < 10) {
          month = '0' + (inHand.getMonth() + 1);
        }
        const inHandDate = inHand.getFullYear() + '-' + month + '-' + inHand.getDate();
        // listData.in_hand = inHandDate;
      }
      const diffTime = Math.abs(new Date(listData.dateDiff) - new Date());
      const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
      listData.diffDays = diffDays;
    }
    channelData && channelData.length && channelData.map((c_item) => {
      getChannelMarkups && getChannelMarkups.length && getChannelMarkups.map((g_item, i) => {
        if (g_item.id == c_item.channel_id) {
          getChannelMarkups[i].markup_amount = c_item.markup_amount ? c_item.markup_amount : channelManagementConst.zero;
          getChannelMarkups[i].markup_active = c_item.markup_active;
          getChannelMarkups[i].channel_markup_type_id = c_item.channel_markup_type_id ? c_item.channel_markup_type_id : channelManagementConst.one;
          getChannelMarkups[i].rowId = c_item.id;
          if (c_item.markup_active == 0) { getChannelMarkups[i].switch = false; }
          else { getChannelMarkups[i].switch = true; }
        }
      })
    });
    //ticket type id
    let inputListState;
    let seatSelection;
    let listAttachments;
    let fetchedData = this.state.singleSelect;
    if (listData) {
      let fetchedData = this.state.singleSelect;
      this.state.ticketTypeDetails && this.state.ticketTypeDetails.length > 0 && this.state.ticketTypeDetails.map(async (ticketType) => {
        if (ticketType.id == listData.ticket_type_id) {
          fetchedData.ticket_type_id.push({ label: ticketType.name, value: ticketType.id })
        }
      });
      this.state.getSplitsList && this.state.getSplitsList.length && this.state.getSplitsList.map(async (splits) => {
        if (splits.id == listData.splits_id) {
          fetchedData.splitId.push({ label: splits.name, value: splits.id })
        }
      });
      this.state.getTicketingSystems && this.state.getTicketingSystems.length && this.state.getTicketingSystems.map((ticSystem) => {
        if (ticSystem.id == listData.ticket_system_id) {
          fetchedData.ticketing_system.push({ label: ticSystem.ticketing_system_name, value: ticSystem.id })
        }
      })
      this.state.getTicketingSystemAccounts && this.state.getTicketingSystemAccounts.length && this.state.getTicketingSystemAccounts.map((sysAccounts) => {
        if (sysAccounts.id == listData.ticket_system_account) {
          fetchedData.ticketing_system_accounts.push({ label: sysAccounts.email, value: sysAccounts.id })
        }
      })

      if (listData.seat_type_id == seatTypes.generalAdmission.id) {
        listData.seat_end = "";
        listData.seat_start = "";
        listData.section = "";
        listData.row = "";
        this.setState({ Is_show_sectiondiv: false, Is_show_localpickup: false, quantity_ga: listData.quantity });
      } else {
        this.setState({ Is_show_sectiondiv: true });
      }

      this.state.getAllSeatTypes && this.state.getAllSeatTypes.length > 0 && this.state.getAllSeatTypes.map(
        (seatTypes) => {
          if (seatTypes.id == listData.seat_type_id) {
            fetchedData.seat_type_id = [];
            fetchedData.seat_type_id.push({ label: seatTypes.name, value: seatTypes.id })
          }
        }
      );

      this.state.getTicketingSystemAccounts && this.state.getTicketingSystemAccounts.length > 0 && this.state.getTicketingSystemAccounts.map(
        (ticketingSystemAccount) => {
          if (ticketingSystemAccount.id == listData.ticket_system_account) {
            fetchedData.ticketing_system_accounts = [];
            fetchedData.ticketing_system_accounts.push({ label: ticketingSystemAccount.email, value: ticketingSystemAccount.id })
          }
        }
      );


      seatSelection = this.state.singleinputList;
      inputListState = this.state.inputList;
      inputListState.seat_end = listData.seat_end;
      inputListState.seat_start = listData.seat_start;
      seatSelection.seat_start.push({ 'label': listData.seat_start, 'value': listData.seat_start });
      seatSelection.seat_end.push({ 'label': listData.seat_end, 'value': listData.seat_end });

      listAttachments = await fetchAttachmentsById(parseInt(this.props.listId));

      let disable_inhand = false
      if (listData.sold.length == 1 && listData.sold[0] == true) {
        disable_inhand = true;
      }
      await this.setState({
        singleDate: listData.in_hand ? new Date(listData.in_hand) : '', listDataDetails: listData,
        eventId: listData.event_id, listAttachments: listAttachments, inhand_disable: disable_inhand,
        myDBPrice: listData.price
      })
    }
    await this.setState({
      userselectedDiscs: userselectedDiscs, userselectedAttr: userselectedAttr, userselectedTags: userselectedTags,
      selectedDiscolsures: JSON.stringify(userselectedDiscs), selectedAttributes: JSON.stringify(userselectedAttr), selectedTags: JSON.stringify(userselectedTags), channelManagmentInputList: getChannelMarkups, singleSelect: fetchedData, channelData,
      singleinputList: seatSelection, inputList: inputListState
    })

    if (this.state.singleSelect.ticket_type_id && this.state.singleSelect.ticket_type_id[0] && this.state.singleSelect.ticket_type_id[0].value === listingConst.localpickup_id) {
      await this.setState({ Is_show_localpickup: true })
    }

    //Loading list details end

    this.dynamicAttachmentLoading() // Loading the dynamic file upload region based on seat count
    this.setMyPageLoader(false)
    // this.channelManagmentInputarray();
  }

  async AddTicketingSystemAccountOnclick() {

    var modelpopup = {
      'isModalVisible': true,
      'title': 'Add Ticketing System Account',
      'type': 'ticketingSystemAccount'
    }
    this.setState({ isAppendModelPopup: modelpopup })

  }

  handlesubmitButtonModal = async (modalpopupstatus) => {
    if (modalpopupstatus) {
      var listdata = this.state.datafields;
      this.setMyPageLoader(true);

      var editListingresponse = await updateListing(listdata, this.state.getChannelMarkups);
      if (!editListingresponse.isError) {

        const attachment_response = await listingattachment(listdata.seatInformationsJSON, editListingresponse.attachmentData);

        if (attachment_response == true) {
          this.setMyPageLoader(false);
          await this.setState({ infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: editListingresponse.msg });
          return;

        }
        else {  // file attachment message
          this.setMyPageLoader(false);
          await this.setState({ infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: listingConst.add_attachment_error });
          return;
        }
      }
      else {
        this.setMyPageLoader(false)
        await this.setState({ infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: listingConst.add_attachment_error });
        return;
      }
    } else {
      await this.setState({ closeButtonModal: false });

    }
  }

  handleInfoSubmitButtonModal = async (modalpopupstatus) => {
    await this.setState({
      InfoSubmitButtonModal: true,
      infocloseButtonModal: false,
    });

    if (this.state.updatePurchaseOrder && this.props.listId) {
      this.props.history.push({
        pathname: '/purchase/create-order/' + this.props.listId.toString(),
        search: this.props.listId.toString()
      })
    } else {
      this.props.history.push("/listings");
    }
  }
  /**
   * Dynamic input file field generation based on seat start and end  (count)
   */
  async dynamicAttachmentLoading() {
    var updatedInputList = this.state.inputList;
    this.state.inputList && this.state.inputList.length > 0 && this.state.inputList.map(async (seats, i) => {
      const updated_seat_end = this.state.listDataDetails.seat_end
      const updated_seat_start = this.state.listDataDetails.seat_start
      const items = [];
      if (this.state.singleSelect.ticket_type_id && this.state.singleSelect.ticket_type_id[0] && this.state.singleSelect.ticket_type_id[0].value == 1) {
        if (this.state.singleSelect.seat_type_id && this.state.singleSelect.seat_type_id[0] && this.state.singleSelect.seat_type_id[0].value !== seatTypes.generalAdmission.id) {

          var inc = 0;
          if (parseInt(updated_seat_start) != 0 && parseInt(updated_seat_end) != 0) {
            for (var updateSeatCount = Number(updated_seat_start); updateSeatCount <= Number(updated_seat_end); updateSeatCount++) {
              let fileTypeName = '';
              let fileName = '';
              if (this.state.listAttachments[inc] && this.state.listAttachments[inc].attachment_url != 'nil' && this.state.listAttachments[inc].attachment_url != '') {
                var splitFiletype = this.state.listAttachments[inc].attachment_url.split(".");
                fileTypeName = splitFiletype[splitFiletype.length - 1];
                var splitFileName = this.state.listAttachments[inc].attachment_url.split("/");
                fileName = splitFileName[splitFileName.length - 1];
              }
              var data = {
                'attachment_seatno': updateSeatCount,
                'attachment_text': this.state.listAttachments[inc] ? this.state.listAttachments[inc].attachment_name : '',
                'attachment_file': '',
                'attachment_file_type': fileTypeName,
                'attachment_file_name': fileName,
                'attachment_file_data': '',
                'attachmentId': this.state.listAttachments[inc] ? this.state.listAttachments[inc].id : ''
              }
              items.push(data); inc++;
            }
          }
        } else {
          var inc = 0;
          for (var quantityGACount = 1; quantityGACount <= this.state.quantity_ga; quantityGACount++) {
            let fileTypeName = '';
            let fileName = '';
            if (this.state.listAttachments[inc] && this.state.listAttachments[inc].attachment_url != 'nil' && this.state.listAttachments[inc].attachment_url != '') {
              var splitFiletype = this.state.listAttachments[inc].attachment_url.split(".");
              fileTypeName = splitFiletype[splitFiletype.length - 1];
              var splitFileName = this.state.listAttachments[inc].attachment_url.split("/");
              fileName = splitFileName[splitFileName.length - 1];
            }
            var data = {
              'attachment_seatno': quantityGACount,
              'attachment_text': this.state.listAttachments[inc] ? this.state.listAttachments[inc].attachment_name : '',
              'attachment_file': '',
              'attachment_file_type': fileTypeName,
              'attachment_file_name': fileName,
              'attachment_file_data': '',
              'attachmentId': this.state.listAttachments[inc] ? this.state.listAttachments[inc].id : ''
            }
            items.push(data); inc++;
          }
        }
      }

      if (updatedInputList != '' || updatedInputList != undefined) {
        updatedInputList[i].attachments = items;
        await this.setState({
          inputList: updatedInputList
        });
      }

    })
  }

  //Add Discloures Popup
  async AddDisclosureOnclick() {

    var modelpopup = {
      'isModalVisible': true,
      'title': 'Add Disclosures',
      'type': 'disclosure'
    }
    this.setState({ isAppendModelPopup: modelpopup })

  }

  //open Attribute Popup
  async AddAttributeOnclick() {
    var modelpopup = {
      'isModalVisible': true,
      'title': 'Add Attributes',
      'type': 'attribute'
    }
    this.setState({ isAppendModelPopup: modelpopup })
  }

  //open Tags Popup
  async AddTagOnclick() {
    var modelpopup = {
      'isModalVisible': true,
      'title': 'Add Tags',
      'type': 'tag'
    }
    this.setState({ isAppendModelPopup: modelpopup })
  }
  /**
  * Page : Add Listing
  * Function For : Model Add Ticketing system Model popup
  * Ticket No : TIC-113
  */
  //Add Ticketing System Popup
  async AddTicketingSystemOnclick() {

    var modelpopup = {
      'isModalVisible': true,
      'title': 'Add ticketing system',
      'type': 'ticketing_system'
    }
    this.setState({ isAppendModelPopup: modelpopup })
  }


  /**
 * Page : Add Listing
 * Function For : Close Model Append Model popup
 * Ticket No : TIC-113
 */
  //Cancel Append Model Popup
  async AppendModelSubmitOnclick() {

    let updatedAppendModelPopup = this.state.isAppendModelPopup;
    updatedAppendModelPopup.isModalVisible = false;
    await this.setState({
      isAppendModelPopup: updatedAppendModelPopup
    });

  }

  /**
* Page : Add Listing
* Function For : Cancel Model Append Model popup
* Ticket No : TIC-113
*/
  //Cancel Append Model Popup
  async AppendModelCancelOnclick() {
    let updatedAppendModelPopup = this.state.isAppendModelPopup;
    let getTicketingSystemAccounts = [];

    updatedAppendModelPopup.isModalVisible = false;
    await this.setState({
      isAppendModelPopup: updatedAppendModelPopup
    });
    let getTicketingSystems = await getAllTicketingSystemList();
    let getDisclosures = await getAllDisclosureList(this.context.user.id);
    let getAttributes = await getAllAttributeList(this.context.user.id);
    let getTags = await getAllTagList(this.context.user.id);
    if (this.state.singleSelect.ticketing_system && this.state.singleSelect.ticketing_system[0] && this.state.singleSelect.ticketing_system[0].value) {
      getTicketingSystemAccounts = await allTicketingSystemAccounts(this.state.singleSelect.ticketing_system[0].value, this.context.user.id);
    }
    await this.setState({
      getTicketingSystems: getTicketingSystems,
      getDisclosures: getDisclosures,
      getAttributes: getAttributes,
      getTags: getTags,
      getTicketingSystemAccounts: getTicketingSystemAccounts
    });
  }

  /**
   * Download the attachment based on single click
   */
  async downloadAttachment(e) {
    let eventId = this.state.listDataDetails.event_id;
    let listId = this.props.listId;
    let filetype = e.target.getAttribute("data-filetype") ? fileTypes[e.target.getAttribute("data-filetype")] : 'image/jpeg';
    let filename = e.target.getAttribute("data-filename");
    let downloadFile = await getFileFromAWS(eventId, listId, filename);

    if (downloadFile) {
      await this.setState({ downloadAttachmentState: downloadFile, attachType: filetype })
      if (this.state.downloadAttachmentState) {
        this.downloadBtn.click();//trigger/click the hidden anchor tag contains the values of file in base64 string
      }
    }
    else {
      alert('No Attachment found')
    }


  }
  /**
   * Page : Add Listing
   * Function For : Unit Cost Calculation (Cost / Seat Quantity)
   * Ticket No : TIC-113
   */
  async unitCostCalculator() {

    var seat_start = this.state.listDataDetails.seat_start;
    var seat_end = this.state.listDataDetails.seat_end;
    var cost = this.state.listDataDetails.group_cost;
    var unit_cost = '';
    if (this.state.singleSelect.seat_type_id && this.state.singleSelect.seat_type_id[0] && this.state.singleSelect.seat_type_id[0].value === seatTypes.generalAdmission.id) {
      var unit_cost = Number(cost) / this.state.quantity_ga;
      unit_cost = parseFloat(unit_cost).toFixed(2);
      let updatedInputList = this.state.listDataDetails;
      updatedInputList.unit_cost = unit_cost;
      await this.setState({
        listDataDetails: updatedInputList
      });

    } else {
      if (Number(seat_start) <= Number(seat_end)) {
        if (cost != '') {
          var seat_count = Number(seat_end) - Number(seat_start);
          var new_seat_count = seat_count + 1;
          var unit_cost = Number(cost) / new_seat_count;
          unit_cost = parseFloat(unit_cost).toFixed(2);
          let updatedInputList = this.state.listDataDetails;
          updatedInputList.unit_cost = unit_cost;
          await this.setState({
            listDataDetails: updatedInputList
          });
        }
      }
    }

  }

  async channelManagmentInputarray() {
    {
      this.state.getChannelMarkups && this.state.getChannelMarkups.length > 0
        && this.state.getChannelMarkups.map((channelitem, i) => {
          var data = {
            "channel_id": channelitem.id,
            "channel_markup_name": channelitem.channel_name,
            "channel_markup_type_id": "1",
            "markup_amount": "0",
            "markup_active": "1"
          }
          this.setState({ channelManagmentInputList: [...this.state.channelManagmentInputList, data] })
        })
    }
  }

  //channelManagmentIncressOnchange
  async channelManagmentIncressOnchange(e, statearraynumber) {
    let updatedChanelInputList = this.state.channelManagmentInputList;
    updatedChanelInputList[statearraynumber].markup_amount = parseInt(updatedChanelInputList[statearraynumber].markup_amount) + 1;
    await this.setState({
      channelManagmentInputList: updatedChanelInputList
    });
  }

  //channelManagmentDecressOnchange
  async channelManagmentDecressOnchange(e, statearraynumber) {
    let updatedChanelInputList = this.state.channelManagmentInputList;
    if (updatedChanelInputList[statearraynumber].markup_amount && updatedChanelInputList[statearraynumber].markup_amount > 0) {
      updatedChanelInputList[statearraynumber].markup_amount = parseInt(updatedChanelInputList[statearraynumber].markup_amount) - 1;
      await this.setState({
        channelManagmentInputList: updatedChanelInputList
      });
    }
  }


  //channelManagmentOnchange
  async channelManagmentOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let maxLength = listingConst.maxLength_percent;
    if (this.state.channelManagmentInputList[statearraynumber].channel_markup_type_id == listingConst.channel_markup_type_two) {
      maxLength = listingConst.maxLength_amount;
    }
    if (statearrayvalue.length <= maxLength && e.nativeEvent.data != 'e') {//avoid length of the value more than specified 
      let updatedChanelInputList = this.state.channelManagmentInputList;
      updatedChanelInputList[statearraynumber].markup_amount = statearrayvalue;
      await this.setState({
        channelManagmentInputList: updatedChanelInputList
      });
    }

  }

  async channelManagmentToggleOnchange(value, statearraynumber) {
    let channelManageState = this.state.channelManage;
    var statearrayvalue = value;
    if (statearrayvalue) {
      var channel_markup_type_id_value = listingConst.channel_markup_type_one;
      channelManageState[statearraynumber] = listingConst.hundred  // for percent
    } else {
      var channel_markup_type_id_value = listingConst.channel_markup_type_two;
      channelManageState[statearraynumber] = listingConst.max_nine; // for maxlength 5 and max value 99999
    }
    let updatedChanelInputList = this.state.channelManagmentInputList;
    updatedChanelInputList[statearraynumber].channel_markup_type_id = channel_markup_type_id_value;
    await this.setState({
      channelManagmentInputList: updatedChanelInputList,
      channelManage: channelManageState
    });

  }

  toggleAdvanced() {
    this.setState({ advancefilter: !this.state.advancefilter });
  }

  async handleRemoveClick(indexValue) {
    //Input Field Remove
    this.state.inputList.splice(indexValue, 1);
    let singleinputListState = this.state.singleinputList;
    delete singleinputListState[indexValue];
    var singleItems = singleinputListState.filter(function (item) {
      return item !== undefined;
    });
    this.setState({ inputList: this.state.inputList, singleinputList: singleItems })
    //Quantity Remove
    this.state.quantity_array.splice(indexValue, 1);
    this.setState({ quantity_array: this.state.quantity_array })
    const quantity_array = this.state.quantity_array;
    const quantity_sum_result = quantity_array.reduce((total, currentValue) => total = total + currentValue.quantity, 0);
    let updatedquantity = this.state.quantity;
    updatedquantity = quantity_sum_result;
    await this.setState({
      quantity: updatedquantity
    });

  }

  //Section
  async appendedFormSectionOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].section = statearrayvalue;
    await this.setState({
      inputList: updatedInputList
    });

    //Validation array update
    var inputListValidationArray = this.state.inputListValidation;
    if (inputListValidationArray && statearrayvalue != "") {
      var index = inputListValidationArray.findIndex(function (o) {
        return o.id === statearraynumber && o.name === listingGroupInfo.section;
      })
      if (index !== -1) inputListValidationArray.splice(index, 1);
    }
  }

  //Row
  async appendedFormRowOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].row = statearrayvalue;
    await this.setState({
      inputList: updatedInputList
    });

    //Validation array update
    var inputListValidationArray = this.state.inputListValidation;
    if (inputListValidationArray && statearrayvalue != "") {
      var index = inputListValidationArray.findIndex(function (o) {
        return o.id === statearraynumber && o.name === listingGroupInfo.row;
      })
      if (index !== -1) inputListValidationArray.splice(index, 1);
    }
  }

  async hideSeatNumber(value) {
    await this.setState({
      listDataDetails: {
        ...this.state.listDataDetails,
        hide_seat_numbers: value
      }
    })
  }

  filterOptions = (options, filter) => {
    if (!filter) {
      return options;
    }
    const re = new RegExp(filter, "i");
    return options.filter(({ label }) => label && label.match(re));
  };

  //Seat Start
  async formUpdate(e, keyword) {
    let dbValue = this.state.myDBPrice;
    let updateField = this.state.listDataDetails;
    let callCalculator = false;

    if ((keyword == listingConst.seat_start)) {
      updateField[keyword] = e.target.value;

      updateField['quantity'] = 0;
      if (updateField.seat_start > 0 && updateField.seat_end > 0) {
        updateField['quantity'] = ((updateField.seat_end - updateField.seat_start) + 1) > 0 ? (updateField.seat_end - updateField.seat_start) + 1 : 0;
      }
      callCalculator = true;
    }
    else if (keyword == listingConst.seat_end) {
      updateField[keyword] = e.target.value;

      // await this.setState({ listDataDetails: updateEnd })
      updateField['quantity'] = 0;
      if (updateField.seat_end > 0 && updateField.seat_start > 0) {
        updateField['quantity'] = ((updateField.seat_end - updateField.seat_start) + 1) > 0 ? (updateField.seat_end - updateField.seat_start) + 1 : 0;

      }
      callCalculator = true;
    }
    else if (keyword == listingConst.group_cost) {
      if (e.target.value.length <= listingConst.maxLength_amount) {
        updateField[keyword] = e.target.value;
      }
      callCalculator = true;
    }
    else if (keyword == listingConst.face_value) {
      if (e.target.value.length <= listingConst.maxLength_amount) {
        updateField[keyword] = e.target.value;
      }
    }
    else if (keyword == listingConst.price) {
      if (dbValue != e.target.value) // for checking price is changed or not
      {
        updateField['isPriceChanged'] = true;
      }
      else {
        updateField['isPriceChanged'] = false;
      }

      if (e.target.value.length <= listingConst.maxLength_amount) {
        updateField[keyword] = e.target.value;
      }
    }
    else {
      updateField[keyword] = e.target.value;
    }
    await this.setState({ listDataDetails: updateField });
    if (callCalculator) {
      await this.unitCostCalculator();
    }
    this.dynamicAttachmentLoading();
  }


  //Seat Start
  async appendedFormSeatStartOnchange(e) {
    let updateStart = this.state.listDataDetails;
    updateStart['seat_start'] = e.target.value;
    await this.setState({ listDataDetails: updateStart })
  }

  //Seat End
  async appendedFormSeatEndOnchange(e) {

    let updateEnd = this.state.listDataDetails;
    updateEnd['seat_end'] = e.target.value;
    await this.setState({ listDataDetails: updateEnd })

    //Quantity End
  }

  //Price
  async appendedFormPriceOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    if (statearrayvalue.length <= listingConst.maxLength_amount && e.nativeEvent.data != 'e') {//max length 5 digits
      updatedInputList[statearraynumber].price = statearrayvalue;
      await this.setState({
        inputList: updatedInputList
      });
    }
  }

  //Face Value
  async appendedFormFaceValueOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    if (statearrayvalue.length <= listingConst.maxLength_amount && e.nativeEvent.data != 'e') {//max length 5 digits
      updatedInputList[statearraynumber].face_value = statearrayvalue;
      await this.setState({
        inputList: updatedInputList
      });
    }
  }

  //Cost
  async appendedFormCostOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    if (statearrayvalue.length <= listingConst.maxLength_amount && e.nativeEvent.data != 'e') {//max length 5 digits
      updatedInputList[statearraynumber].cost = statearrayvalue;
      await this.setState({
        inputList: updatedInputList
      });
      await this.unitCostCalculator(statearraynumber);
    }

  }

  //Unit Cost
  async appendedFormUnitCostOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].unit_cost = statearrayvalue;
    await this.setState({
      inputList: updatedInputList
    });
  }

  //Disclosure
  async appendedFormDisclosureOnchange(e) {
    var statearrayvalue = e;
    let updatedInputList = JSON.stringify(statearrayvalue);
    await this.setState({
      selectedDiscolsures: updatedInputList
    });
  }

  //Attribute
  async appendedFormAttributeOnchange(e) {
    var statearrayvalue = e;
    let updatedInputList = JSON.stringify(statearrayvalue);
    await this.setState({
      selectedAttributes: updatedInputList
    });
  }

  //PO ID
  async appendedFormPOIDOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].po_id = statearrayvalue;
    await this.setState({
      inputList: updatedInputList
    });
  }

  //Tags
  async appendedFormTagsOnchange(e) {
    var statearrayvalue = e;
    let updatedInputList = JSON.stringify(statearrayvalue);
    await this.setState({
      selectedTags: [updatedInputList]
    });
  }

  //Intrnal Notes
  async appendedFormInternalNotesOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].internal_notes = statearrayvalue;
    await this.setState({
      inputList: updatedInputList
    });
  }

  //External Notes
  async appendedFormExternalNotesOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].external_notes = statearrayvalue;
    await this.setState({
      inputList: updatedInputList
    });
  }

  //Attachment
  async appendedFormAttachmentOnchange(e, statearraynumber) {

    let timestamp = new Date().getTime();
    let attachmentfile_data = e.target.files[0];
    var attachmentId = e.target.getAttribute("data-attid") ? e.target.getAttribute("data-attid") : ''

    let attachmentfile = e.target.value;
    let attachmentfilename = attachmentfile_data ? (timestamp + "_" + attachmentfile_data.name) : '';
    let attachmentseatno = e.target.id;
    var temp_attachments = this.state.inputList[statearraynumber].attachments;
    var inc = 0;
    temp_attachments && temp_attachments.length > 0
      && temp_attachments.map((attachitem, i) => {
        if (attachitem.attachment_seatno === Number(attachmentseatno)) {
          var data = {
            "attachment_file": attachmentfilename,
            "attachment_file_data": attachmentfile_data,
            "attachment_seatno": attachitem.attachment_seatno,
            "attachment_text": attachitem.attachment_text,
            'attachmentId': attachmentId,// this.state.listAttachments[inc].id,
            "attachment_file_type": '',
            'attachment_file_name': '',
          }; inc++;
          let updatedAttachmentInputList = this.state.inputList;
          updatedAttachmentInputList[statearraynumber].attachments[i] = data;
          this.setState({
            inputList: updatedAttachmentInputList
          });
        }
      })
  }

  async appendedFormAttachmentTextOnchange(e, statearraynumber) {

    let attachmentname = e.target.value;
    let attachmentseatno = e.target.id;
    var attachmentId = e.target.getAttribute("data-attid") ? e.target.getAttribute("data-attid") : ''
    var temp_attachments = this.state.inputList[statearraynumber].attachments;
    var inc = 0;
    temp_attachments && temp_attachments.length > 0
      && temp_attachments.map((attachitem, i) => {
        if (attachitem.attachment_seatno === Number(attachmentseatno)) {
          var data = {
            "attachment_file": attachitem.attachment_file,
            "attachment_file_data": attachitem.attachment_file_data,
            "attachment_seatno": attachitem.attachment_seatno,
            "attachment_text": attachmentname,
            'attachmentId': attachmentId,
            "attachment_file_type": '',
            'attachment_file_name': '',
          }; inc++;
          let updatedAttachmentInputList = this.state.inputList;
          updatedAttachmentInputList[statearraynumber].attachments[i] = data;
          this.setState({
            inputList: updatedAttachmentInputList
          });
        }
      })
  }

  /**
   * Removing newly attached file, It will not remove the file(s) in AWS
   */
  async removeAttachment(event, index) {
    var attachments = this.state.inputList[index].attachments;

    attachments && attachments.length > 0
      && attachments.map(async (item, i) => {
        if (item.attachment_seatno === Number(event.target.dataset.id)) {
          var data = {
            "attachment_file": '',
            "attachment_file_data": '',
            "attachment_seatno": item.attachment_seatno,
            "attachment_text": item.attachment_text
          }
          let updatedAttachmentInputList = this.state.inputList;
          updatedAttachmentInputList[index].attachments[i] = data;
          await this.setState({
            inputList: updatedAttachmentInputList
          });
        }
      })
  }

  // Dynamic Form Function end

  /**
   * 
   * @param {*} e 
   * Date filter 'startingDate' is only required, to 'endingDate' is optional and removal 
   */
  async dateRangeDisplay(e) {//Date picker  
    var startingDate = '';
    if (e) {
      var starting = new Date(e);
      let month = starting.getMonth() + 1;
      let date = starting.getDate();
      if (month < 10) {
        month = '0' + month;
      }
      if (date < 10) {
        date = '0' + date;
      }
      startingDate = starting.getFullYear() + '-' + month + '-' + date;
    }

    let listState = this.state.listDataDetails;
    listState.in_hand = startingDate ? startingDate : '';
    this.setState({ inhand_date: startingDate, singleDate: e, listDataDetails: listState });

  }

  toggleSwitch(e, statearraynumber) {
    let updatedChannelInputList = this.state.channelManagmentInputList;
    updatedChannelInputList[statearraynumber].switch = e;
    if (e === false) {
      updatedChannelInputList[statearraynumber].markup_amount = channelManagementConst.zero;
      updatedChannelInputList[statearraynumber].markup_active = channelManagementConst.zero;
    } else {
      let channelData = this.state.channelData;
      for (var channelDataCount = 0; channelDataCount < channelData.length; channelDataCount++) {
        for (var channelInputCount = 0; channelInputCount < updatedChannelInputList.length; channelInputCount++) {
          if (updatedChannelInputList[channelInputCount].id == channelData[channelDataCount].channel_id) {
            updatedChannelInputList[channelInputCount].markup_amount = channelData[channelDataCount].markup_amount;
          }
        }
      }
    }
    this.setState({
      channelManagmentInputList: updatedChannelInputList
    });
  }

  /**
   * 
   * @param {*} e 
   * Selecting days in multi option
   */
  async daySelected(e) {
    var day_select = [];
    e && e.map(days => (
      day_select.push(days.value)
    ))
    let filteredData;
    let stateHolder = this.state.searchData;
    stateHolder['day_of_week'] = day_select;
    this.setState({ selectedDays: e, searchData: stateHolder });
    filteredData = await getAddListingsFilter(this.state.searchData);
    this.setState({ filterListingsData: filteredData });
  }
  /**
   * Method name : listSearchChange
   * Searching the event id, name, etc by user search
   */
  async listSearchChange(e) {   // For autofill search suggestion

    let filteredData = {};
    let suggestions = [];
    const value = e.target.value;
    let stateHolder = this.state.searchData;
    stateHolder.searches = value;
    this.setState({ locationText: '', searchText: value, displayText: value, searchData: stateHolder });
    if (value.length > 3) {
      suggestions = await eventSearches({ searches: value });
    }
    else {
      suggestions = [];
      if (value.length == 0) {
        filteredData = await getAddListingsFilter(stateHolder);
        this.setState({ filterListingsData: filteredData });
      }
    }
    this.setState(() => ({
      suggestions,
    }));
  }

  /**
   * 
   * @param {*} keyword 
   * // Refresh the listing based on search filter suggestion onclick
   */
  async selectLisitng(keyword = false) {
    await this.setState({ suggestions: null, displayText: keyword.toString(), });
    let allSearchData = this.state.searchData;
    allSearchData.searches = keyword.toString();
    await this.setState({ searches: allSearchData })
    let searchedData = await getAddListingsFilter(this.state.searchData);
    this.setState({ filterListingsData: searchedData });
  }


  /**
   * filtering keyword column in data table
   * @param {*} e 
   * @param {*} keyword 
   * all params are required
   */

  async advanceFilter(e, keyword) {
    let filteredData;
    let stateHolder = this.state.searchData;
    if (keyword == listingGroupInfo.quantity) {
      stateHolder[keyword] = parseInt(e.target.value)
    }
    else { stateHolder[keyword] = e.target.value; }
    this.setState({ searchData: stateHolder });
    filteredData = await getAddListingsFilter(this.state.searchData);
    this.setState({ filterListingsData: filteredData });
  }

  /**
  * 
  * @param {*} keyword 
  * // Cancel the listing form
  */


  /**
   * Modified the design for single selects similar to multiselect - functionality
   * @param {*} e  input field value
   * @param {*} keyword which input field value is going to be modified / added
   */
  async singleSelect(e, keyword, listKeyword) {
    let stateHolder = this.state.singleSelect;
    let val = this.state.singleSelect[keyword];
    let selectedValue;
    let newValue;
    let listDataState;

    if (e) { selectedValue = [e]; } else if (this.state.singleSelect[keyword]) { selectedValue = this.state.singleSelect[keyword]; } else { selectedValue = []; }
    if (selectedValue) {
      newValue = selectedValue.filter(ar => !val.find(rm => (rm.label === ar.label && ar.value === rm.value)))
      stateHolder[keyword] = newValue;
      listDataState = this.state.listDataDetails;
      listDataState[listKeyword] = newValue && newValue.length && newValue[0] ? newValue[0].value : '';
    }

    if (keyword == listingConst.seat_type_id) {
      let seatTypeMark = newValue.length && newValue[0] ? newValue[0].value : 0;
      this.setState({ seat_type_value: seatTypeMark, singleSelect: stateHolder, listDataDetails: listDataState });
    }
    else {
      this.setState({ singleSelect: stateHolder, listDataDetails: listDataState });
    }
    if (keyword == listingConst.ticket_type_id) {
      if (newValue && newValue[0] && newValue[0].value === listingConst.localpickup_id) {
        this.setState({ Is_show_localpickup: true });
      } else {
        this.setState({ Is_show_localpickup: false });
      }
    }

    if (keyword == listingConst.seat_type_id) {
      let seatRowStatus = true;
      if (newValue && newValue.length && newValue[0]) {
        if (newValue[0].value === seatTypes.generalAdmission.id) {
          seatRowStatus = false;
        }
      }
      this.setState({ Is_show_sectiondiv: seatRowStatus });
      this.unitCostCalculator();
      this.dynamicAttachmentLoading();
    }

    if (keyword == listingConst.ticket_type_id) {
      if (newValue && newValue[0] && newValue[0].label !== listingConst.etickets) { // Remove all attachments if ticket type is not "eTickets"
        for (var inputCount = 0; inputCount < this.state.inputList.length; inputCount++) {
          let updatedInputList = this.state.inputList;
          updatedInputList[inputCount].attachments = [];
          await this.setState({ inputList: updatedInputList });
        }
      }
      else {// Re adding all attachments if ticket type is "eTickets" based on seat start,end
        this.dynamicAttachmentLoading()
      }
    }

    if (keyword == listingConst.ticketingSystem) {
      if (newValue && newValue[0]) {
        let getTicketingSystemAccounts = await allTicketingSystemAccounts(newValue[0].value, this.context.user.id);
        this.setState({ disableTicketingAccount: false, getTicketingSystemAccounts });
      }
      else {
        this.setState({ disableTicketingAccount: true });
      }

      this.setState(prevState => ({
        singleSelect: {
          ...prevState.singleSelect,
          ticketing_system_accounts: []
        }
      }))

    }

  }

  /**
   * To avoid user change the readonly/disable attributes forcefully
   */
  async avoidOnchange(e) {
    e.preventDefault();
    return false;
  }

  /**
   * Changing the Channel Management type
   * @param {*} e 
   * @param {*} i 
   */
  async channelSlide(e, i) {
    var statearrayvalue = e;
    var channel_markup_type_id_value = "2";
    if (!statearrayvalue) {
      channel_markup_type_id_value = "1";
    }
    let updatedChanelInputList = this.state.getChannelMarkups;
    updatedChanelInputList[i].channel_markup_type_id = channel_markup_type_id_value;
    await this.setState({
      getChannelMarkups: updatedChanelInputList
    });
  }
  /**
   * Loader Function
   */
  async setMyPageLoader(bool) {
    if (bool) {
      this.setState({ setLoader: true });
      document.body.classList.add('loader-background');
    }
    else {
      this.setState({ setLoader: false });
      document.body.classList.remove('loader-background');
    }
  }

  async setQuantity(e) {
    await this.setState({ quantity_ga: e.target.value });
    this.unitCostCalculator();
    this.dynamicAttachmentLoading();
  }

  filterOptions = (options, filter) => {
    if (!filter) {
      return options;
    }
    const re = new RegExp(filter, "i");
    return options.filter(({ label }) => label && label.match(re));
  };

  dropdownFilterOption = ({ label, value, data }, string) => {
    if (this.state.value1 === data) {
      return false;
    } else if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };
  async formValidationExternal() {
    if (this.state.inputList) {

      await this.setState({ inputListValidation: "", quantityInputValidation: "" });

      this.state.inputList && this.state.inputList.length > 0
        && this.state.inputList.map(async (seatitem, i) => {

          if (this.state.singleSelect && this.state.singleSelect.seat_type_id && this.state.singleSelect.seat_type_id.length) {

            if (this.state.singleSelect.seat_type_id[0].value !== seatTypes.generalAdmission.id) {

              if (seatitem.section == "") {
                var validationdata = {
                  id: i,
                  name: 'section',
                  message: 'Section is required'
                }
                await this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })

              }

              if (seatitem.row == "") {
                var validationdata = {
                  id: i,
                  name: 'row',
                  message: 'Row is required'
                }
                await this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })
              }

              if (seatitem.seat_start == "") {
                var validationdata = {
                  id: i,
                  name: 'seat_start',
                  message: 'Seat start is required'
                }
                await this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })
              }

              if (seatitem.seat_end == "") {
                var validationdata = {
                  id: i,
                  name: 'seat_end',
                  message: 'Seat end is required'
                }
                await this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })
              }

            } else {
              if (this.state.quantity_ga == "") {

                var validationdata = {
                  name: 'quantity_ga',
                  message: 'Quantity is required'
                }
                this.setState({ quantityInputValidation: [validationdata] })
              } else if (this.state.quantity_ga <= "0") {

                var validationdata = {
                  name: 'quantity_ga',
                  message: 'Enter valid number'
                }
                this.setState({ quantityInputValidation: [validationdata] })
              } else if (this.state.quantity_ga > listingConst.max_quantity) {
                var validationdata = {
                  name: 'quantity_ga',
                  message: 'Maximum quantity is ' + listingConst.max_quantity
                }
                this.setState({ quantityInputValidation: [validationdata] })
              } else {
                this.setState({ quantityInputValidation: "" })
              }

            }
          }


        })

    }

  }

  render() {
    const { suggestions } = this.state
    const days = daysFull;
    const monthNames = monthNamesShort;

    //Disclosure
    const disclosureValues = [];
    this.state.getDisclosures && this.state.getDisclosures.length > 0
      && this.state.getDisclosures.map((item, itemCount) => {
        disclosureValues.push({ value: item.id, label: item.disclosure_name });
      })

    //Attributes
    const attributeValues = [];
    this.state.getAttributes && this.state.getAttributes.length > 0
      && this.state.getAttributes.map((item, itemCount) => {
        attributeValues.push({ value: item.id, label: item.attribute_name });
      })

    //Tag
    const tagValues = [];
    this.state.getTags && this.state.getTags.length > 0
      && this.state.getTags.map((item, itemCount) => {
        tagValues.push({ value: item.id, label: item.tag_name });
      })


    /** Show other user selected -start **/
    if (this.state.userselectedDiscs && this.state.userselectedDiscs.length > 0) {
      let newDiscValue = this.state.userselectedDiscs.filter(ar => !disclosureValues.find(rm => (rm.label === ar.label && ar.value === rm.value)));
      newDiscValue && newDiscValue.length && newDiscValue.map((item, i) => {
        disclosureValues.push(item);
      });
    }

    if (this.state.userselectedAttr && this.state.userselectedAttr.length > 0) {
      let newAttrValue = this.state.userselectedAttr.filter(ar => !attributeValues.find(rm => (rm.label === ar.label && ar.value === rm.value)));
      newAttrValue && newAttrValue.length && newAttrValue.map((item, i) => {
        attributeValues.push(item);
      });
    }

    if (this.state.userselectedTags && this.state.userselectedTags.length > 0) {
      let newTagValue = this.state.userselectedTags.filter(ar => !tagValues.find(rm => (rm.label === ar.label && ar.value === rm.value)));

      newTagValue && newTagValue.length && newTagValue.map((item, i) => {
        tagValues.push(item);
      });
    }

    /** Show other user selected  -end**/


    //Ticket Type
    const ticketTypeDetails = [];
    this.state.ticketTypeDetails && this.state.ticketTypeDetails.length > 0
      && this.state.ticketTypeDetails.map((item, itemCount) => {
        ticketTypeDetails.push({ value: item.id, label: item.name });
      })

    //Ticket Sysyem Account
    const ticketSystemAccounts = [];
    this.state.getTicketingSystemAccounts && this.state.getTicketingSystemAccounts.length > 0
      && this.state.getTicketingSystemAccounts.map((item, itemCount) => {
        ticketSystemAccounts.push({ value: item.id, label: item.email });
      })

    //Seat type
    const seatType = [];
    this.state.getAllSeatTypes && this.state.getAllSeatTypes.length > 0
      && this.state.getAllSeatTypes.map((item, itemCount) => {
        seatType.push({ value: item.id, label: item.name });
      })

    //Split Details
    const splitDetails = [];
    this.state.getSplitsList && this.state.getSplitsList.length && this.state.getSplitsList.map(splits =>
      splitDetails.push({ value: splits.id, label: splits.name },)
    );
    const seatTypeSelected = preselectedSeat ? preselectedSeat : [];

    const ticketSystem = [];
    this.state.getTicketingSystems && this.state.getTicketingSystems.length && this.state.getTicketingSystems.map(ticSystem => ticketSystem.push({ value: ticSystem.id, label: ticSystem.ticketing_system_name },)
    );
    const seatStarts = [];
    for (var quantityCount = 0; quantityCount < 100; quantityCount++) {  // Dynamic quantity 1-100
      seatStarts.push({ label: quantityCount + 1, value: quantityCount + 1 })
    }
    const event_venue_details = this.state.listDataDetails.venue_name ? this.state.listDataDetails.venue_name + ', ' + this.state.listDataDetails.city + ', ' + this.state.listDataDetails.state + ', ' + this.state.listDataDetails.country : '';


    var startDate = this.state.listDataDetails.days_old ? moment.utc(this.state.listDataDetails.days_old).local().format('YYYY-MM-DD HH:MM') : ''
    var endDate = moment(moment().format('YYYY-MM-DD HH:MM'));
    let days_old = '-';
    if (startDate) {
      days_old = endDate.diff(startDate, 'days') + ' days';
    }




    return !this.state.loading ? (
      <div>
        <UserContext.Consumer>
          {context => (
            <div>
              <div className="loader-bg">
                <ClipLoader color={'#4a88c5'} loading={this.state.setLoader} size={40} />
              </div>
              <div className="myTicket-item">
                <div className="search-item">

                  {/* Add Listing Form Start */}
                  <div className="add-listing-fliter-bottom">
                    <Formik
                      initialValues={{
                        seller_id: (this.state.userId && this.state.userId.length) ? this.state.userId : "",
                        buyer_id: (this.state.singleSelect.ticketing_system_accounts.length > 0 && this.state.singleSelect.ticketing_system_accounts[0]) ? this.state.singleSelect.ticketing_system_accounts[0].value : [],
                        eventId: this.state.listDataDetails.event_id,
                        ticket_type_id: this.state.listDataDetails.ticket_type_id ? this.state.listDataDetails.ticket_type_id : '',
                        inhand_date: this.state.listDataDetails.in_hand ? new Date(this.state.listDataDetails.in_hand) : '',
                        splitId: this.state.listDataDetails.splits_id ? this.state.listDataDetails.splits_id : '',
                        seat_type_id: this.state.listDataDetails.seat_type_id ? this.state.listDataDetails.seat_type_id : '',
                        quantity: this.state.listDataDetails.quantity,
                        ticketing_system: this.state.listDataDetails.ticket_system_id ? this.state.listDataDetails.ticket_system_id : '',
                        ticketing_system_accounts: this.state.listDataDetails.ticket_system_account ? this.state.listDataDetails.ticket_system_account : '',
                        hide_seat_number: this.state.listDataDetails.hide_seat_numbers,
                        row: this.state.listDataDetails.row,
                        sold: this.state.listDataDetails.sold ? this.state.listDataDetails.sold[0] : 'no',
                        group_cost: this.state.listDataDetails.group_cost,
                        unit_cost: this.state.listDataDetails.unit_cost,
                        price: this.state.listDataDetails.price,
                        face_value: this.state.listDataDetails.face_value,
                        seat_start: this.state.listDataDetails.seat_start,
                        seat_end: this.state.listDataDetails.seat_end,
                        section: this.state.listDataDetails.section,
                        po_id: this.state.listDataDetails.PO_ID,
                        disclosureids: this.state.selectedDiscolsures,
                        attrbtids: this.state.selectedAttributes,
                        selectedTags: this.state.selectedTags,
                        internal_notes: this.state.listDataDetails.internal_notes,
                        external_notes: this.state.listDataDetails.external_notes,
                        quantity_ga: this.state.quantity_ga ? this.state.quantity_ga : "",
                      }}

                      enableReinitialize={true}
                      onReset={this.cancelAddListingOnclick}

                      validationSchema={Yup.object().shape({

                        ticket_type_id: Yup.string()
                          .required('Ticket type is required'),

                        inhand_date: Yup.string()
                          .required('Inhand date is required'),

                        seat_type_id: Yup.string()
                          .required('Seat type value is required'),

                        splitId: Yup.string()
                          .required('Split value is required'),

                        ticketing_system: Yup.string()
                          .required('Ticketing system is required'),

                        ticketing_system_accounts: Yup.string()
                          .required('Account is required'),
                        quantity_ga: Yup.number().when('seat_type_id', {
                          is: value => value && value == seatTypes.generalAdmission.id,
                          then: Yup.number()
                            .typeError('you must specify a number')
                            .min(1, 'Min value 0.')
                            .required('Quantity is required'),
                        }),
                        price: Yup.string()
                          .min(1, 'Minimum Price is one')
                          .required('Price is required'),
                        hide_seat_number: Yup.string()
                          .required('Hide seat number is required'),
                        seat_start: Yup.number().when('seat_type_id', {
                          is: value => value && value != seatTypes.generalAdmission.id,
                          then: Yup.number()
                            .min(1, 'Minimum One seat is required')
                            .max(Yup.ref('seat_end'), 'Seat start must be lesser than seat end')
                            .required('Seat start is required'),
                        }),
                        seat_end: Yup.number().when('seat_type_id', {
                          is: value => value && value != seatTypes.generalAdmission.id,
                          then: Yup.number()
                            .required('Seat end is required')
                            .min(Yup.ref('seat_start'), 'Seat end must be greater than seat start')
                            .max((parseInt(this.state.listDataDetails.seat_start) + parseInt(listingConst.max_quantity - 1)), 'Seat quantity should not exceed than total of ' + listingConst.max_quantity),
                        }),
                        group_cost: Yup.number().
                          required('Cost is required'),
                        section: Yup.string().when('seat_type_id', {
                          is: value => value && value != seatTypes.generalAdmission.id,
                          then: Yup.string().
                            required('Section is required'),
                        }),
                        row: Yup.string().when("seat_type_id", {
                          is: value => value && value != seatTypes.generalAdmission.id,
                          then: Yup.string().
                            required('Row is required'),
                        }),

                      })}

                      onSubmit={async (fields) => {

                        var data = {
                          "seatInformationsJSON": this.state.inputList,
                          "channelmarkupsJSON": this.state.channelManagmentInputList,
                        }
                        //await this.formValidationExternal();
                        if (this.state.inputListValidation == "") {
                          const listdata = await { ...fields, ...data };
                          listdata.listingGroupId = Number(this.props.listId);
                          listdata.isPriceChanged = this.state.listDataDetails.isPriceChanged;
                          listdata.seatInformationsJSON[0].eventId = this.state.listDataDetails.event_id;
                          await this.setState({
                            closeButtonModal: true,
                            datafields: listdata
                          });
                        }
                      }}

                      render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                        <Form>
                          <div className="">
                            <div className="add-listing-card-section">
                              <div className="card">

                                {this.state.listDataDetails.event_id ?
                                  <div className="card-header">
                                    <div className="addlisting-title-item">
                                      <div className="listing-title-divinner">
                                        <div className="left-title">

                                          <h2 className="listing-card-title secondary-font-family">
                                            {this.state.listDataDetails.event_name ? this.state.listDataDetails.event_name[0] : ''}
                                          </h2>

                                        </div>
                                        <div className="right-side-id ml-auto">
                                          <p>
                                            {this.state.listDataDetails.event_id ? 'Event ID : ' + this.state.listDataDetails.event_id : ''}
                                            {
                                              // this.state.listDataDetails.days_old 
                                              ' | Days Old : ' + days_old
                                            }
                                          </p>
                                        </div>
                                      </div>
                                      <div className="d-flex">
                                        <h3 className="card-location-item">
                                          {event_venue_details}
                                        </h3>
                                        <h3 className="card-date-item">
                                          {
                                            (this.state.listDataDetails.eventdate && this.state.listDataDetails.eventdate[0]) ?
                                              ((this.state.listDataDetails.timezone && this.state.listDataDetails.timezone[0]) ?
                                                moment.utc(this.state.listDataDetails.eventdate[0]).tz(this.state.listDataDetails.timezone[0]).format('dddd MMM Do YYYY @ h:mm A z') : moment.utc(this.state.listDataDetails.eventdate[0]).format('dddd MMM Do YYYY @ h:mm A z')) : ''
                                          }
                                        </h3>
                                      </div>
                                    </div>
                                  </div> : ''}


                                <div className="card-body">
                                  <div className="form-data-grid-4">
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Ticket Type <span>*</span></label>
                                        <Select
                                          {...this.props}
                                          placeholder={"Ticket Type"}
                                          style={{ width: "100%" }}
                                          name="ticket_type_id"
                                          filterOption={this.dropdownFilterOption}
                                          value={this.state.singleSelect.ticket_type_id ? this.state.singleSelect.ticket_type_id : []}
                                          options={ticketTypeDetails}
                                          isClearable
                                          onChange={(e) => this.singleSelect(e, 'ticket_type_id', 'ticket_type_id')}
                                          className={(errors.ticket_type_id && touched.ticket_type_id ? ' is-invalid' : '')}
                                          allowClear
                                          showSearch
                                        />
                                        <ErrorMessage name="ticket_type_id" component="div" className="invalid-feedback" />
                                      </div>
                                    </div>
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Inhand Date <span>*</span></label>

                                        <div className="date--PickQr">
                                          <CalendarIcon />
                                          <DatePicker
                                            name="inhand_date"
                                            selectsRange={false}
                                            className={'formik-input-user-form form-control singleDate' + (errors.inhand_date && touched.inhand_date ? ' is-invalid' : '')}
                                            selected={this.state.singleDate}
                                            onChange={(e) => this.dateRangeDisplay(e)}
                                            onKeyDown={(e) => this.avoidOnchange(e)}
                                            readOnly={this.state.inhand_disable}
                                            maxDate={this.state.listDataDetails.eventdate && this.state.listDataDetails.eventdate[0] ? new Date(this.state.listDataDetails.eventdate) : ''}
                                            highlightDates={new Date()}
                                            isClearable={this.state.singleDate && !values.sold ? true : false} disabledKeyboardNavigation
                                            dateFormat="dd/MM/yyyy"
                                            placeholderText=" DD/MM/YYYY"
                                          /><ErrorMessage name="inhand_date" component="div" className="invalid-feedback" style={{ display: 'block' }} />

                                        </div>

                                      </div>
                                    </div>
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Splits <span>*</span></label>
                                        <Select
                                          {...this.props}
                                          placeholder={"Splits"}
                                          style={{ width: "100%" }}
                                          name="splitId"
                                          filterOption={this.dropdownFilterOption}
                                          value={this.state.singleSelect.splitId ? this.state.singleSelect.splitId : []}
                                          options={splitDetails}
                                          isClearable
                                          onChange={(e) => this.singleSelect(e, 'splitId', 'splits_id')}
                                          className={(errors.splitId && touched.splitId ? ' is-invalid' : '')}
                                          allowClear
                                          showSearch
                                        />
                                        <ErrorMessage name="splitId" component="div" className="invalid-feedback" />
                                      </div>
                                    </div>
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Seat Type <span>*</span></label>
                                        <Select
                                          {...this.props}
                                          placeholder={"Seat Type"}
                                          style={{ width: "100%" }}
                                          name="seat_type_id"
                                          filterOption={this.dropdownFilterOption}
                                          value={this.state.singleSelect.seat_type_id ? this.state.singleSelect.seat_type_id : [seatType[0]]}
                                          options={seatType}
                                          isClearable
                                          onChange={(e) => this.singleSelect(e, 'seat_type_id', 'seat_type_id')}
                                          className={(errors.seat_type_id && touched.seat_type_id ? ' is-invalid' : '')}
                                          allowClear
                                          showSearch
                                        />
                                        <ErrorMessage name="seat_type_id" component="div" className="invalid-feedback" />
                                      </div>
                                    </div>
                                    {this.state.Is_show_sectiondiv ?
                                      <div className="di-form-items">
                                        <div className="form-group add-listing-form-group">
                                          <label>Quantity <span>*</span></label>
                                          <input
                                            className={'formik-input-user-form form-control' + (errors.quantity && touched.quantity ? ' is-invalid' : '')}
                                            type="text"
                                            placeholder="Quantity"
                                            value={values.quantity}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            readOnly />
                                          <ErrorMessage name="quantity" component="div" className="invalid-feedback" />
                                        </div>
                                      </div>
                                      :
                                      <div className="di-form-items">
                                        <div className="form-group add-listing-form-group">
                                          <label>Quantity <span>*</span></label>
                                          <input
                                            className={'formik-input-user-form form-control' + (errors.quantity_ga && touched.quantity_ga ? ' is-invalid' : '')}
                                            type="number"
                                            id="quantity_ga"
                                            name="quantity_ga"
                                            placeholder="Quantity"
                                            value={this.state.quantity_ga}
                                            onChange={(e) => this.setQuantity(e)}
                                            onBlur={handleBlur}
                                            onKeyDown={e => (e.keyCode === 69 || e.keyCode === 190 || e.keyCode === 110) && e.preventDefault()}
                                            disabled={this.state.Is_show_sectiondiv} />



                                          <ErrorMessage name="quantity_ga" component="div" className="invalid-feedback" />
                                        </div>
                                      </div>
                                    }
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Ticketing System <span>*</span>
                                        </label>
                                        <Select
                                          {...this.props}
                                          placeholder={"Ticketing System"}
                                          style={{ width: "100%" }}
                                          name="ticketing_system"
                                          filterOption={this.dropdownFilterOption}
                                          value={this.state.singleSelect.ticketing_system ? this.state.singleSelect.ticketing_system : [ticketSystem[0]]}
                                          options={ticketSystem}
                                          isClearable
                                          onChange={(e) => this.singleSelect(e, 'ticketing_system', 'ticket_system_id')}
                                          className={(errors.ticketing_system && touched.ticketing_system ? ' is-invalid' : '')}
                                          allowClear
                                          showSearch
                                        />
                                        <ErrorMessage name="ticketing_system" component="div" className="invalid-feedback" />
                                      </div>
                                    </div>
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Account <span>*</span> <i className="fa fa-plus-circle add-disclosure-icon" title="Add Ticketing System Account" onClick={this.AddTicketingSystemAccountOnclick} disabled={this.state.disableTicketingAccount} aria-hidden="true"></i> </label>
                                        <Select
                                          {...this.props}
                                          placeholder={"Ticketing System Account"}
                                          style={{ width: "100%" }}
                                          name="ticketing_system_accounts"
                                          filterOption={this.dropdownFilterOption}
                                          value={this.state.singleSelect.ticketing_system_accounts}
                                          options={ticketSystemAccounts}
                                          isClearable
                                          onChange={(e) => this.singleSelect(e, 'ticketing_system_accounts', 'ticket_system_account')}
                                          className={(errors.ticketing_system_accounts && touched.ticketing_system_accounts ? ' is-invalid' : '')}
                                          allowClear
                                          showSearch
                                        />
                                        <ErrorMessage name="ticketing_system_accounts" component="div" className="invalid-feedback" />
                                      </div>
                                    </div>
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Hide Seat Number <span>*</span></label>

                                        <Switch
                                          className="filter-btn-hide--seat"
                                          name="hide_seat_number"
                                          checkedChildren="ON"
                                          unCheckedChildren="OFF"
                                          onChange={(e) => this.hideSeatNumber(e)}
                                          checked={this.state.listDataDetails.hide_seat_numbers}
                                        />
                                        <ErrorMessage name="hide_seat_number" component="div" className="invalid-feedback" />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>


                              {/* append field start */}
                              {this.state.inputList && this.state.inputList.map((x, i) => {
                                return (
                                  <>
                                    <div className="card">
                                      <div className="card-header add-group-listing-card-header">
                                        <h2 className="add-group-list-title">Group #{i + 1}</h2>
                                      </div>
                                      <div className="card-body">
                                        <div className="add-listing-form-items">
                                          <div className="form-data-grid-4">
                                            {this.state.Is_show_sectiondiv &&
                                              <>
                                                <div className="di-form-items">
                                                  <div className="form-group add-listing-form-group">
                                                    <label>Section <span>*</span></label>
                                                    <input
                                                      type="text"
                                                      name="section"
                                                      placeholder="Section"
                                                      className={'formik-input-user-form form-control' + (errors.section && touched.section ? ' is-invalid' : '')}
                                                      value={values.section}
                                                      onChange={(e) => this.formUpdate(e, 'section')}
                                                      onBlur={handleBlur}
                                                    />
                                                    {/* Validation Section Start */}
                                                    {this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                      if (validationitems.id == i && validationitems.name == listingGroupInfo.section) {
                                                        return (
                                                          <div className="add-listing-validation-error"> {validationitems.message} </div>
                                                        )
                                                      }
                                                    })}
                                                    {/* Validation Section End */}

                                                    <ErrorMessage name="section" component="div" className="invalid-feedback" />
                                                  </div>
                                                </div>
                                                <div className="di-form-items">
                                                  <div className="form-group add-listing-form-group">
                                                    <label>Row <span>*</span></label>
                                                    <input
                                                      type="text"
                                                      name="row"
                                                      placeholder="Row"
                                                      className={'formik-input-user-form form-control' + (errors.row && touched.row ? ' is-invalid' : '')}
                                                      value={values.row}
                                                      onChange={(e) => this.formUpdate(e, 'row')}
                                                      onBlur={handleBlur}
                                                    />

                                                    {/* Validation Section Start */}
                                                    {this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                      if (validationitems.id == i && validationitems.name == listingGroupInfo.row) {
                                                        return (
                                                          <div className="add-listing-validation-error"> {validationitems.message} </div>
                                                        )
                                                      }
                                                    })}
                                                    {/* Validation Section End */}

                                                    <ErrorMessage name="row" component="div" className="invalid-feedback" />
                                                  </div>
                                                </div>
                                                <div className="di-form-items">
                                                  <div className="form-group add-listing-form-group">
                                                    <label>Seat Start <span>*</span></label>

                                                    <input
                                                      type="number"
                                                      name="seat_start"
                                                      className={'formik-input-user-form form-control' + (errors.seat_start && touched.seat_start ? ' is-invalid' : '')}
                                                      value={values.seat_start}
                                                      onChange={(e) => this.formUpdate(e, 'seat_start')}
                                                      onKeyDown={e => (e.keyCode === 69 || e.keyCode === 190 || e.keyCode === 110) && e.preventDefault()}
                                                      placeholder="From" />

                                                    {/* Validation Section Start */}
                                                    {this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                      if (validationitems.id == i && validationitems.name == listingGroupInfo.seatStart) {
                                                        return (
                                                          <div className="add-listing-validation-error"> {validationitems.message} </div>
                                                        )
                                                      }
                                                    })}
                                                    {/* Validation Section End */}
                                                    <ErrorMessage name="seat_start" component="div" className="invalid-feedback" />
                                                  </div>
                                                </div>
                                                <div className="di-form-items">
                                                  <div className="form-group add-listing-form-group">
                                                    <label>Seat End <span>*</span></label>

                                                    <input
                                                      type="number"
                                                      name="seat_end"
                                                      className={'formik-input-user-form form-control' + (errors.seat_end && touched.seat_end ? ' is-invalid' : '')}
                                                      value={values.seat_end}
                                                      onChange={(e) => this.formUpdate(e, 'seat_end')}
                                                      onKeyDown={e => (e.keyCode === 69 || e.keyCode === 190 || e.keyCode === 110) && e.preventDefault()}
                                                      min={values.seat_start}
                                                      placeholder="To" />

                                                    {/* Validation Section Start */}
                                                    {this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                      if (validationitems.id == i && validationitems.name == listingGroupInfo.seatEnd) {
                                                        return (
                                                          <div className="add-listing-validation-error"> {validationitems.message} </div>
                                                        )
                                                      }
                                                    })}
                                                    <ErrorMessage name="seat_end" component="div" className="invalid-feedback" />
                                                    {/* Validation Section End */}
                                                  </div>

                                                </div>
                                              </>
                                            }
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group">
                                                <label>Price <span>*</span></label>
                                                <input
                                                  type="number"
                                                  name="price"
                                                  placeholder="$"
                                                  step="any"
                                                  className={'formik-input-user-form form-control' + (errors.price && touched.price ? ' is-invalid' : '')}
                                                  value={values.price}
                                                  onKeyDown={e => (e.keyCode === 69 || e.keyCode === 190) && e.preventDefault()}
                                                  onChange={(e) => this.formUpdate(e, 'price')}
                                                  onBlur={handleBlur}
                                                  min={0}
                                                  disabled={values.sold ? "disabled" : ""}
                                                />
                                                <ErrorMessage name="price" component="div" className="invalid-feedback" />



                                              </div>
                                            </div>
                                            <div className="di-form-items">

                                              <div className="form-group add-listing-form-group">
                                                <label>Face Value</label>
                                                <input
                                                  type="number"
                                                  name="face_value"
                                                  placeholder="Face Value"
                                                  className={'formik-input-user-form form-control' + (errors.face_value && touched.face_value ? ' is-invalid' : '')}
                                                  value={values.face_value}
                                                  onKeyDown={e => (e.keyCode === 69 || e.keyCode === 190) && e.preventDefault()}
                                                  onChange={(e) => this.formUpdate(e, 'face_value')}
                                                  onBlur={handleBlur}
                                                  min="0"
                                                />
                                                <ErrorMessage name="face_value" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group">
                                                <label>Cost <span>*</span></label>
                                                <input
                                                  type="number"
                                                  name="group_cost"
                                                  placeholder="$"
                                                  className={'formik-input-user-form form-control' + (errors.group_cost && touched.group_cost ? ' is-invalid' : '')}
                                                  value={values.group_cost}
                                                  onChange={(e) => this.formUpdate(e, 'group_cost')}
                                                  onKeyDown={e => (e.keyCode === 69 || e.keyCode === 190) && e.preventDefault()}
                                                  onBlur={handleBlur}
                                                  step="any"
                                                  min="1"
                                                />
                                                <ErrorMessage name="cost" component="div" className="invalid-feedback" />

                                                {/* Validation Section Start */}
                                                {this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                  if (validationitems.id == i && validationitems.name == listingGroupInfo.cost) {
                                                    return (
                                                      <div className="add-listing-validation-error"> {validationitems.message} </div>
                                                    )
                                                  }
                                                })}
                                                {/* Validation Section End */}
                                              </div>
                                            </div>
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group">
                                                <label>Unit Cost</label>
                                                <input
                                                  type="number"
                                                  name="unit_cost"
                                                  placeholder="$"
                                                  className={'formik-input-user-form form-control' + (errors.unit_cost && touched.unit_cost ? ' is-invalid' : '')}
                                                  value={values.unit_cost}
                                                  onChange={(e) => this.formUpdate(e, 'unit_cost')}
                                                  onBlur={handleBlur}
                                                  min="0"
                                                  readOnly
                                                />
                                                <ErrorMessage name="unit_cost" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group multi-select-item">
                                                <label>
                                                  Disclosures
                                                  {/* <i className="fa fa-plus-circle add-disclosure-icon" title="Add Disclosures" onClick={this.AddDisclosureOnclick} aria-hidden="true"></i> */}
                                                </label>

                                                <Select
                                                  {...this.props}
                                                  placeholder={"Disclosures"}
                                                  style={{ width: "100%" }}
                                                  name="disclosures"
                                                  filterOption={this.dropdownFilterOption}
                                                  value={this.state.selectedDiscolsures ? JSON.parse(this.state.selectedDiscolsures) : []}
                                                  options={disclosureValues}
                                                  isClearable={(e) => this.setState({ selectedDiscolsures: "" })}
                                                  onChange={(e) => this.appendedFormDisclosureOnchange(e)}
                                                  className={(errors.paymentType && touched.paymentType ? ' is-invalid' : '')}
                                                  allowClear
                                                  showSearch
                                                  isMulti
                                                />
                                                <ErrorMessage name="disclosure" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group">
                                                <label>
                                                  Attributes
                                                  <i className="fa fa-plus-circle add-disclosure-icon" title="Add Attributes" onClick={this.AddAttributeOnclick} aria-hidden="true"></i>
                                                </label>

                                                <Select
                                                  {...this.props}
                                                  placeholder={"Attributes"}
                                                  style={{ width: "100%" }}
                                                  name="attributes"
                                                  filterOption={this.dropdownFilterOption}
                                                  value={this.state.selectedAttributes ? JSON.parse(this.state.selectedAttributes) : []}
                                                  options={attributeValues}
                                                  isClearable={(e) => this.setState({ selectedAttributes: "" })}
                                                  onChange={(e) => this.appendedFormAttributeOnchange(e)}
                                                  className={(errors.attributes && touched.attributes ? ' is-invalid' : '')}
                                                  allowClear
                                                  showSearch
                                                  isMulti
                                                />                                              <ErrorMessage name={"attributes"} component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group">
                                                <label>PO ID</label>
                                                <input
                                                  type="text"
                                                  name="po_id"
                                                  placeholder="PO ID"
                                                  className={'formik-input-user-form form-control' + (errors.po_id && touched.po_id ? ' is-invalid' : '')}
                                                  value={values.po_id}
                                                  onChange={(e) => this.formUpdate(e, 'PO_ID')}
                                                  onBlur={handleBlur}
                                                  maxLength={20}
                                                />
                                                <ErrorMessage name="po_id" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group multi-select-item">
                                                <label>
                                                  Tags
                                                  <i className="fa fa-plus-circle add-disclosure-icon" title="Add Tags" onClick={this.AddTagOnclick} aria-hidden="true"></i>
                                                </label>

                                                <Select
                                                  {...this.props}
                                                  placeholder={"Tags"}
                                                  style={{ width: "100%" }}
                                                  name="tags"
                                                  filterOption={this.dropdownFilterOption}
                                                  value={this.state.selectedTags ? JSON.parse(this.state.selectedTags) : []}
                                                  options={tagValues}
                                                  onChange={(e) => this.appendedFormTagsOnchange(e)}
                                                  className={(errors.tags && touched.tags ? ' is-invalid' : '')}
                                                  allowClear
                                                  showSearch
                                                  isMulti
                                                >
                                                  <Option value="" selected>Select</Option>
                                                </Select>
                                                <ErrorMessage name="tags" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                          </div>
                                          <div className="form-data-grid-2">
                                            <div className="div-two-form--item">
                                              <div className="form-group add-listing-form-group">
                                                <label>Internal Notes</label>
                                                <textarea
                                                  name="internal_notes"
                                                  className={'formik-input-user-form form-control add-listing-text-area' + (errors.internal_notes && touched.internal_notes ? ' is-invalid' : '')}
                                                  value={values.internal_notes}
                                                  onChange={(e) => this.formUpdate(e, 'internal_notes')}
                                                  onBlur={handleBlur}
                                                  placeholder="Note here"
                                                ></textarea>
                                                <ErrorMessage name="internal_notes" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                            <div className="div-two-form--item">
                                              <div className="form-group add-listing-form-group">
                                                <label>External Notes</label>
                                                <textarea
                                                  name="external_notes"
                                                  className={'formik-input-user-form form-control add-listing-text-area' + (errors.external_notes && touched.external_notes ? ' is-invalid' : '')}
                                                  value={values.external_notes}
                                                  onChange={(e) => this.formUpdate(e, 'external_notes')}
                                                  onBlur={handleBlur}
                                                  placeholder="Note here"
                                                ></textarea>
                                                <ErrorMessage name="external_notes" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                          </div>
                                          {i ?
                                            <div>
                                              <button type="button" className="btn btn-danger group-remove-btn" onClick={() => this.handleRemoveClick(i)} >Cancel</button>
                                            </div>
                                            :
                                            ''
                                          }


                                        </div>
                                      </div>
                                    </div>
                                  </>
                                );
                              })}


                              {/* append field end */}


                            </div>
                          </div>



                          <div className="row m-0 justifyContent-item">


                            {/* Attachment Section Attachment Start */}




                            {/* my attachment start */}
                            {this.state.inputList && this.state.inputList.map((x, i) => {

                              //Attachment Loop
                              var attachments = x.attachments;
                              const items = []; var attachmentId = 0; var attachmentType = ''; var attachmentName = '';

                              if (attachments && attachments.length) {
                                for (var attachmentCount = 0; attachmentCount < attachments.length; attachmentCount++) {
                                  attachmentId = attachments[attachmentCount].attachmentId;
                                  attachmentType = attachments[attachmentCount].attachment_file_type;
                                  attachmentName = attachments[attachmentCount].attachment_file_name;


                                  items.push(
                                    <Row>
                                      <Col md="12" className="form-group">
                                        {this.state.Is_show_sectiondiv ?
                                          <label className="form-label-userprofile">
                                            Seat {attachments[attachmentCount].attachment_seatno}
                                          </label> : <label className="form-label-userprofile">
                                            Ticket {attachments[attachmentCount].attachment_seatno}
                                          </label>}
                                        <div className="upload-item channel-markup-row input-row">
                                          <div className="ticket-count">
                                            <FormInput
                                              type="text"
                                              placeholder="Ticket Number"
                                              data-attid={attachmentId}
                                              id={attachments[attachmentCount].attachment_seatno}
                                              value={attachments[attachmentCount].attachment_text}
                                              onChange={(e) => this.appendedFormAttachmentTextOnchange(e, i)}

                                            />
                                          </div>
                                          {attachments[attachmentCount].attachment_file_data && attachments[attachmentCount].attachment_file_data.name ?
                                            <>
                                              <div className="attach-btn">
                                                <AttachmentIcon /> {attachments[attachmentCount].attachment_file_data.name}
                                              </div>
                                              <div className="file-replace-section">
                                                <div className='file file--upload'>
                                                  <FormInput
                                                    type="file"
                                                    name="attachment"
                                                    id={attachments[attachmentCount].attachment_seatno}
                                                    onChange={(e) => this.appendedFormAttachmentOnchange(e, i)}
                                                  />
                                                </div>
                                                <span className="replace-order-btn">Replace</span>
                                              </div>
                                              {attachments[attachmentCount] && attachments[attachmentCount].attachment_seatno &&
                                                <div className="delete-section">
                                                  <Button className="invoice-delete--btn" data-id={attachments[attachmentCount].attachment_seatno} onClick={(e) => this.removeAttachment(e, i)} >Delete</Button>
                                                </div>
                                              }
                                            </> :
                                          <div className="ticket-attached-btn">
                                            <div className='file file--upload d-flex'>
                                              <div className="input-item-down-sec">
                                                  <label htmlFor='input-file'>
                                                    <AttachmentIcon />
                                                  </label>

                                                  <FormInput
                                                    type="file"
                                                    data-attid={attachmentId}
                                                    name="attachment"
                                                    id={attachments[attachmentCount].attachment_seatno}
                                                    onChange={(e) => this.appendedFormAttachmentOnchange(e, i)}
                                                  />

                                                </div>
                                                <span className="attach-item">Attach</span>
                                              </div>
                                            </div>
                                          }
                                          <div className="downLoad-item pl-3">
                                            <label>
                                              {attachmentType && attachmentName ?
                                                <i
                                                  className="fa fa-download"
                                                  id={attachmentId}
                                                  data-filetype={attachmentType}
                                                  data-filename={attachmentName}
                                                  title="Download"
                                                  style={{
                                                    fontSize: "25px",
                                                    color: "#3bcfd0"
                                                  }} aria-hidden="true" onClick={(e) => this.downloadAttachment(e)} >
                                                </i> : ''}
                                            </label>
                                          </div>
                                        </div>
                                      </Col>
                                    </Row>
                                  );
                                }
                              }



                              {/* my attachment end */ }

                              return (
                                items && items.length ?


                                  <div className="card bottom-switch-card-section" >
                                    <div>
                                      <div className="card-body">
                                        <div className="channel-management-sec mt-0">

                                          <div className="channel-title">
                                            <h4>Attached Tickets</h4>
                                          </div>
                                          <div className="add-listing-group-name"><h4>Group #{i + 1}</h4></div>
                                          <Row className="m-0">
                                            <Form>
                                              {items}
                                              <a style={{ display: "block" }} href={`data:${this.state.attachType};base64,${this.state.downloadAttachmentState}`} download ref={node => (this.downloadBtn = node)}></a>
                                            </Form>
                                          </Row>
                                        </div>
                                      </div>
                                    </div>

                                  </div>

                                  : ''
                              );
                            })}


                            {/* Attachment Section Attachment End */}
                            {this.state.Is_show_localpickup ?
                              <div className="card bottom-switch-card-section">
                                <div className="card-body">
                                  <div className="channel-management-sec mt-0">
                                    <div className="channel-title">
                                      <h4>Pickup Information</h4>
                                    </div>
                                    <div className="channel-inner-sec">
                                      <Form >
                                        <h3 className="channel-left-title">Contact Info:</h3>
                                        <div className="pickup-information-sec">
                                          <div className="form-group">
                                            <input className="form-control" type="text" placeholder="Name" />
                                          </div>
                                          <div className="form-group">
                                            <input className="form-control" type="email" placeholder="Email Address" />
                                          </div>
                                          <div className="form-group">
                                            <input className="form-control" type="text" placeholder="Phone Number" />
                                          </div>
                                        </div>
                                        <h3 className="channel-left-title">Pickup Address:</h3>
                                        <div className="pickup-information-sec">
                                          <div className="form-group">
                                            <input className="form-control" type="text" placeholder="Street Address 1*" />
                                          </div>
                                          <div className="form-group">
                                            <input className="form-control" type="text" placeholder="Street Address 2 " />
                                          </div>
                                          <div className="form-group">
                                            <div className="d-flex">
                                              <div className="city--item">
                                                <input className="form-control" type="text" placeholder="City*" />
                                              </div>
                                              <div className="state--item">
                                                <input className="form-control" type="text" placeholder="State/Country*" />
                                              </div>
                                              <div className="zipcode--item">
                                                <input className="form-control" type="text" placeholder="Zip Code*" />
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </Form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              : ''}
                            <div className="card bottom-switch-card-section">
                              <div className="card-body">
                                <div className="channel-management-sec">
                                  <div className="channel-title">
                                    <h4>Channel Management</h4>
                                  </div>


                                  {this.state.channelManagmentInputList && this.state.channelManagmentInputList.map((x, i) => {
                                    return (
                                      <div className="channel-inner-sec">
                                        <div className="row align-items-center" key={x.channel_name}>
                                          <div className="col-md-4">
                                            <h3 className="channel-left-title">{x.channel_name}</h3>
                                          </div>
                                          <div className="col-md-4 p-0">
                                            <div className="d-flex">
                                              <div className="left-switch-item">
                                                <div className="button-switch">
                                                  <Switch className="filter-btn-hide--seat"
                                                    checkedChildren="ON"
                                                    unCheckedChildren="OFF"
                                                    checked={x.switch}
                                                    onChange={(e) => this.toggleSwitch(e, i)}
                                                  />
                                                </div>
                                              </div>
                                              {x.switch &&
                                                <div className="right-switch-item">
                                                  <div className="button-switch">
                                                    <Switch className="price-switch-btn"
                                                      checkedChildren="%"
                                                      unCheckedChildren="$"
                                                      defaultChecked
                                                      key={'channel_' + i}
                                                      onChange={(e) => this.channelManagmentToggleOnchange(e, i)}
                                                    />
                                                  </div>
                                                </div>}
                                            </div>
                                          </div>
                                          <div className="col-md-4">
                                            <div className="plus-minus-btn">
                                              <div className="price-change-txt">
                                                <input className="form-control"
                                                  type="number"
                                                  min="0"
                                                  max={this.state.channelManage[i] ? this.state.channelManage[i] : 100}
                                                  key={'channel_value' + i}
                                                  placeholder={x.channel_markup_type_id && x.channel_markup_type_id == 1 ? '%' : '$'}
                                                  value={x.markup_amount}
                                                  disabled={!x.switch}
                                                  onChange={(value) => this.channelManagmentOnchange(value, i)}
                                                />
                                              </div>
                                              <button className="price-add-btn primary-border-color" disabled={!x.switch} type="button" onClick={(value) => this.channelManagmentIncressOnchange(value, i)}><AddIcon /></button>
                                              <button className="price-mini-btn" disabled={!x.switch} type="button" onClick={(value) => this.channelManagmentDecressOnchange(value, i)}><MinusIcon /></button>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                    );
                                  }
                                  )}

                                </div>
                              </div>
                            </div>
                          </div>

                          {/* Channel Management Section End */}

                          <div className="list-creat--btn-section">


                            {
                              this.state.inhand_disable ? <button type="button" className="btn creating--list-btn" onClick={(e) => this.props.history.push("/listings")} >Back</button> :
                                <><button type="submit" className="btn creating--list-btn">Update Listing</button>
                                  <button type="submit" className="btn creat-purchase--list-btn" onClick={(e) => this.setState({ updatePurchaseOrder: true })} >Update Listing and Purchase Order</button>
                                  <button type="button" className="btn cancel--list-btn" onClick={(e) => this.props.history.push("/listings")} >Cancel</button>
                                </>
                            }



                          </div>
                          {/* <div className="row">
                            <div className="col-sm-2">
                              <button type="submit"className="btn btn-success rounded-pill" >Create listing</button>
                            </div>
                            <div className="col-sm-2">
                              <button type="reset" className="btn btn-secondary  rounded-pill">Cancel</button>
                            </div>
                          </div><br />
                          <div className="row">
                            <div className="col-sm-4">
                              <button type="button" className="btn btn-primary btn-block rounded-pill">Create listing and Create PO</button>
                            </div>
                          </div> */}
                        </Form>
                      )}
                    />
                  </div>
                  {/* Add Listing Form End */}


                  {    /**
                 * Page : Add Listing
                 * Function For : Insert Lookup Data for Ticketing System, Disclosures,  Disclosures Model popup
                 * Ticket No : TIC-113, 124
                 */
                  }

                  {/* Lookup data Model Popup Start*/}
                  <Modal
                    title={this.state.isAppendModelPopup.title}
                    visible={this.state.isAppendModelPopup.isModalVisible}
                    onCancel={this.AppendModelCancelOnclick}
                    footer={[]}
                    maskClosable={false}>


                    {(() => {
                      if (this.state.isAppendModelPopup.type == listingGroupInfo.attribute) {
                        return (
                          <div key={'attribute'} >
                            {/* Attribute Form Start */}
                            <Formik
                              initialValues={{
                                attribute_name: ''
                              }}

                              validationSchema={Yup.object().shape({
                                attribute_name: Yup.string()
                                  .required('Attribute name is required')
                              })}

                              onSubmit={async (values, { resetForm }) => {
                                resetForm({ attribute_name: '' })
                                if (values.attribute_name) {
                                  var data = [{ "attribute_name": values.attribute_name }];
                                  var response = await Createattribute(data, this.state.userId);
                                }
                                alert(response.message)
                                if (response.status == responseStatus.ok) {
                                  this.AppendModelCancelOnclick();
                                }

                              }}


                              render={({ errors, status, touched, values, handleChange, handleBlur, handleSubmit }) => (
                                <Form>

                                  <Row>
                                    <Col md="12" className="form-group">
                                      <label htmlFor="attribute_name">Attribute Name</label>
                                      <Field
                                        name="attribute_name"
                                        className={'formik-input-modal-form form-control' + (errors.attribute_name && touched.attribute_name ? ' is-invalid' : '')}
                                        placeholder="Attribute"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                      />
                                      <ErrorMessage name="attribute_name" component="div" className="invalid-feedback" />
                                    </Col>

                                    <Col md="12" className="form-group">
                                      <button type="submit" className="btn btn-primary rounded-pill">Submit</button>
                                    </Col>
                                  </Row>

                                </Form>
                              )}
                            />
                            {/* Attribute Form Start */}
                          </div>
                        )
                      } else if (this.state.isAppendModelPopup.type == listingGroupInfo.disclosure) {
                        return (
                          <div key={'disclosure'}>

                            {/* Disclosure Form Start */}
                            <Formik
                              initialValues={{
                                disclosure_name: ''
                              }}

                              validationSchema={Yup.object().shape({
                                disclosure_name: Yup.string()
                                  .required('Disclosure name is required')
                              })}

                              onSubmit={async (values, { resetForm }) => {
                                resetForm({ disclosure_name: '' })
                                if (values.disclosure_name) {
                                  var data = [{ "disclosure_name": values.disclosure_name }];
                                  var response = await Createdisclosure(data, this.state.userId);
                                }
                                alert(response.message)
                                if (response.status == responseStatus.ok) {
                                  this.AppendModelCancelOnclick();
                                }

                              }}

                              render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                                <Form>

                                  <Row>
                                    <Col md="12" className="form-group">
                                      <label htmlFor="disclosure_name">Disclosure Name</label>
                                      <Field
                                        name="disclosure_name"
                                        className={'formik-input-modal-form form-control' + (errors.disclosure_name && touched.disclosure_name ? ' is-invalid' : '')}
                                        placeholder="Disclosure"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                      />
                                      <ErrorMessage name="disclosure_name" component="div" className="invalid-feedback" />
                                    </Col>

                                    <Col md="12" className="form-group">
                                      <button type="submit" className="btn btn-primary rounded-pill">Submit</button>
                                    </Col>
                                  </Row>

                                </Form>
                              )}
                            />
                            {/* Disclosure Form Start */}

                          </div>
                        )
                      } else if (this.state.isAppendModelPopup.type == listingGroupInfo.ticketingSystem) {
                        return (
                          <div key={'ticketing_system'}>

                            {/* Ticketing system Form Start */}
                            <Formik
                              initialValues={{
                                ticketing_system_name: '',
                                country_id: ''
                              }}

                              validationSchema={Yup.object().shape({
                                ticketing_system_name: Yup.string()
                                  .required('Ticketing system name is required'),
                                country_id: Yup.string()
                                  .required('Country is required')
                              })}

                              onSubmit={async (values, { resetForm }) => {
                                resetForm({ ticketing_system_name: '', country_id: '' })
                                if (values.ticketing_system_name) {
                                  var data = [{
                                    "ticketing_system_name": values.ticketing_system_name,
                                    "country_id": values.country_id
                                  }];
                                  var response = await Createticketingsystem(data);
                                }
                                alert(response.message)
                                if (response.status == responseStatus.ok) {
                                  this.AppendModelCancelOnclick();
                                }

                              }}

                              render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                                <Form>
                                  <Row>
                                    <Col md="12" className="form-group">
                                      <label htmlFor="ticketing_system_name">Ticketing System  Name</label>
                                      <Field
                                        name="ticketing_system_name"
                                        className={'formik-input-modal-form form-control' + (errors.ticketing_system_name && touched.ticketing_system_name ? ' is-invalid' : '')}
                                        placeholder="Ticketing System"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                      />
                                      <ErrorMessage name="ticketing_system_name" component="div" className="invalid-feedback" />
                                    </Col>

                                    <Col md="12" className="form-group">
                                      <label htmlFor="ticketing_system_name">Country</label>

                                      <select
                                        name="country_id"
                                        className={'formik-input-modal-form form-control' + (errors.country_id && touched.country_id ? ' is-invalid' : '')}
                                        value={values.country_id}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                      >
                                        <option value="">Select Country</option>
                                        {this.state.getCountryList && this.state.getCountryList.length > 0
                                          && this.state.getCountryList.map((item, i) => {
                                            return (
                                              <option key={i} value={item.id}>{item.name}</option>
                                            )
                                          })}
                                      </select>

                                      <ErrorMessage name="country_id" component="div" className="invalid-feedback" />
                                    </Col>

                                    <Col md="12" className="form-group">
                                      <button type="submit" className="btn btn-primary rounded-pill">Submit</button>
                                    </Col>
                                  </Row>
                                </Form>
                              )}
                            />
                            {/* Ticketing system Form Start */}

                          </div>
                        )
                      } else if (this.state.isAppendModelPopup.type == listingGroupInfo.tag) {
                        return (
                          <div key={'tag'}>

                            {/* Ticketing system Form Start */}
                            <Formik
                              initialValues={{
                                tag_name: ''
                              }}

                              validationSchema={Yup.object().shape({
                                tag_name: Yup.string()
                                  .required('Tag name is required')
                              })}

                              onSubmit={async (values, { resetForm }) => {
                                resetForm({ tag_name: '' })
                                if (values.tag_name) {
                                  var data = [{
                                    "tag_name": values.tag_name
                                  }];
                                  var response = await Createtag(data, this.state.userId);
                                }
                                alert(response.message)
                                if (response.status == responseStatus.ok) {
                                  this.AppendModelCancelOnclick();
                                }

                              }}

                              render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                                <Form>
                                  <Row>
                                    <Col md="12" className="form-group">
                                      <label htmlFor="tag_name">Tag Name</label>
                                      <Field
                                        name="tag_name"
                                        className={'formik-input-modal-form form-control' + (errors.tag_name && touched.tag_name ? ' is-invalid' : '')}
                                        placeholder="Enter tag name"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                      />
                                      <ErrorMessage name="tag_name" component="div" className="invalid-feedback" />
                                    </Col>

                                    <Col md="12" className="form-group">
                                      <button type="submit" className="btn btn-primary rounded-pill">Submit</button>
                                    </Col>
                                  </Row>
                                </Form>
                              )}
                            />
                            {/* Ticketing system Form Start */}

                          </div>
                        )
                      } else if (this.state.isAppendModelPopup.type == listingGroupInfo.ticketingSystemAccount) {
                        return (
                          <div key={'ticketingSystemAccount'}>

                            {/* Disclosure Form Start */}
                            <Formik
                              initialValues={{
                                email: ''
                              }}

                              validationSchema={Yup.object().shape({
                                email: Yup.string()
                                  .required('User Email is required')
                              })}

                              onSubmit={async (values, { resetForm }) => {
                                var response = [];
                                if (values.email && this.state.singleSelect.ticketing_system && this.state.singleSelect.ticketing_system[0]) {
                                  var data = { "email": values.email, "ticket_system_id": this.state.singleSelect.ticketing_system[0].value };
                                  response = await createTicketingSystemAccount(data, this.state.userId);
                                }

                                if (response.status == responseStatus.ok) {
                                  resetForm({ email: '' })
                                  this.AppendModelCancelOnclick();
                                }
                                if (response.status == responseStatus.error) {
                                  this.setState({ userEmailError: response.message });
                                } else {
                                  this.setState({ userEmailError: null });
                                }

                              }}

                              render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                                <Form>

                                  <Row>
                                    <Col md="12" className="form-group text-center">
                                      <label htmlFor="email">Email</label>
                                      <Field
                                        name="email"
                                        className={'formik-input-modal-form form-control' + (errors.email && touched.email ? ' is-invalid' : '')}
                                        placeholder="User Email"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                      />
                                      {this.state.userEmailError && <div style={{ color: "red" }}> {this.state.userEmailError} </div>}
                                      <ErrorMessage name="email" component="div" className="invalid-feedback" />
                                    </Col>

                                    <Col md="12" className="form-group">
                                      <button type="submit" className="btn btn-primary rounded-pill">Submit</button>
                                    </Col>
                                  </Row>

                                </Form>
                              )}
                            />
                            {/* Ticketing system Form Start */}

                          </div>
                        )
                      }

                    })()}

                  </Modal>

                  {/* Lookup data Model Popup End*/}

                  {/* Common Modal popup start */}
                  {/* Comfirm Modal */}

                  <Confirm
                    header={"Confirm"}
                    body={"Are you sure to submit"}
                    show={this.state.closeButtonModal}
                    submit={() => this.handlesubmitButtonModal(true)}
                    cancel={() => this.handlesubmitButtonModal(false)}
                    closeButton={() => this.setState({ closeButtonModal: false })}
                  />

                  {/* Info Modal */}
                  <Info
                    header={"Info"}
                    body={this.state.infomodalmsg}
                    show={this.state.infocloseButtonModal}
                    submit={() => this.handleInfoSubmitButtonModal(true)}
                    closeButton={() => this.handleInfoSubmitButtonModal(true)}
                  />
                  {/* Common Modal popup end */}


                </div>
              </div>
            </div>
          )}
        </UserContext.Consumer>
      </div >
    ) : (
      <Loader />
    );
  }
}

export default withRouter(MyViewListingForms)
