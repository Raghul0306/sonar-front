import React, { useContext, Component } from "react";
import moment from 'moment-timezone'
import UserContext from "../../contexts/user.jsx";
import Button from 'antd/es/button';
import Loader from "../Loader/Loader";
import { Link } from "react-router-dom";
import { withRouter } from 'react-router-dom'
import {
  getAddListingsFilter, getAllTicketTypes, allGenreDetails, eventSearches, getEventCount, getAllTagList
} from "../../services/listingsService.js";
import ClipLoader from "react-spinners/ClipLoader";
import "../../assets/custom-style.css";
import { listingConst, monthNamesShort, daysFull, weekDays } from "../../constants/constants";
import Select from 'react-select'
//Design Starts
import styled from 'styled-components'
import { FilterCircle } from '@styled-icons/bootstrap/FilterCircle'
import { FilterCircleFill } from '@styled-icons/bootstrap/FilterCircleFill'
import { Search } from '@styled-icons/bootstrap/Search'
import { Speakerphone } from '@styled-icons/heroicons-outline/Speakerphone'
import { Calendar } from '@styled-icons/boxicons-regular/Calendar'
import { Notepad } from '@styled-icons/boxicons-regular/Notepad'
import { DollarCircle } from '@styled-icons/boxicons-regular/DollarCircle'
import { PauseCircle } from '@styled-icons/boxicons-regular/PauseCircle'
import { AddCircle } from '@styled-icons/fluentui-system-regular/AddCircle'
import { Directions } from '@styled-icons/boxicons-regular/Directions'
import { Edit } from '@styled-icons/boxicons-solid/Edit'
import { FileUpload } from '@styled-icons/material-outlined/FileUpload';
import { Close } from '@styled-icons/ionicons-outline/Close';

import lodashGet from 'lodash/get';
import lodashFilter from 'lodash/filter';
import lodashIncludes from 'lodash/includes';
// icon img
import iconInhand from "../../assets/listing-icon/MyTickets-icon_inhand-date.png";
import prevArrow from "../../assets/prev-arrow.png";
import nextArrow from "../../assets/next-arrow.png";
//React date picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
//React date picker
// // Multi select for day of week
// import MultiSelect from "react-multi-select-component";
// import Pagination from './Pagination';


const Filter = styled(FilterCircle)`
  color: #4a88c5;
  width:40px;
  height:40px;
`
const FilterFill = styled(FilterCircleFill)`
  color: #4a88c5;
  width:40px;
  height:40px;
`
const SearchIcon = styled(Search)`
  color: #9f9f9f;
  width:20px;
  height:20px;
`
const SpeakerIcon = styled(Speakerphone)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const CalendarIcon = styled(Calendar)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const NotePadIcon = styled(Notepad)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const DollarCircleIcon = styled(DollarCircle)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const PauseCircleIcon = styled(PauseCircle)`
color: #9f9f9f;
width:30px;
height:30px;
`
const AddCircleIcon = styled(AddCircle)`
color: #9f9f9f;
width:30px;
height:30px;
`
const DirectionsIcon = styled(Directions)`
color: #9f9f9f;
width:30px;
height:30px;
`
const EditIcon = styled(Edit)`
color: #9f9f9f;
width:30px;
height:30px;
`
const UploadIcon = styled(FileUpload)`
color: #9f9f9f;
width:30px;
height:30px;
`
const CloseIcon = styled(Close)`
color: #556573;
width:30px;
height:30px;
`

//Design Ends

export class MyListings extends Component {
  static contextType = UserContext;
  state = {
    loading: true,
    event_id: '',
    rows: [],
    suggestions: [],
    searchText: ''
  };

  constructor(props) {
    super(props)
    this.listSearchChange = this.listSearchChange.bind(this);
    this.selectListing = this.selectListing.bind(this);
    this.toggleAdvanced = this.toggleAdvanced.bind(this);
    this.dateRangeDisplay = this.dateRangeDisplay.bind(this);
    this.advanceFilter = this.advanceFilter.bind(this);
    this.daySelected = this.daySelected.bind(this);
    this.handleEventChange = this.handleEventChange.bind(this);
    this.createListingGroup = this.createListingGroup.bind(this);
    this.advanceFilterSubmit = this.advanceFilterSubmit.bind(this);
    this.advanceFilterReset = this.advanceFilterReset.bind(this);
    this.dropdownFilterOption = this.dropdownFilterOption.bind(this);
    this.paginationProcess = this.paginationProcess.bind(this);

    this.state = {
      listingsData: [],
      filterListingsData: [],
      searchText: '',
      displayText: '',
      startDate: null,
      endDate: null,
      singleDate: null,
      advancefilter: false,
      //quantities: [],
      avoidMultiEmptyCall: false,
      selectedDays: [],
      searchData: {
        searches: "",
        tags: "",
        section: "",
        row: "",
        quantity: '',
        from_date: "",
        to_date: "",
        day_of_week: "",
        in_hand: "",
        inventory_attached: "",
        ticket_type: "",
        category: "",
        currentPage: 1
      },
      multiSelect: {
        ticketType: [],//single selection of multiselect
        genre: [],
        inv_attach: [],
        row: [],
        //quantity: []
      },
      from_dateOnly: '',
      to_dateOnly: '',
      setLoader: false,  //loader
      eventIds: [],
      currentListingData: [],
      currentPage: 1,
      totalPages: null,
      pageLimit: 10,
      searchAdvance: false,
      advanceFilterSubmit: false,
      paginationStates: {
        currentPage: 1,
        totalRows: 0,
        totalPages: 0,

      },
      showPagination: false,
      nextPageInActive: false,
      previousPageInActive: false,
      rowCount: 0,
      enableFilter: false
    }
  }

  async componentDidMount() {
    const userId = this.context.user.id;
    this.props.history.push({ pageTitle: 'Add Listing', pageSubTitle: 'subtitle', redirectPath: "listings" });
    let userFilterListingsData;
    let ticketTypeDetails;
    let genreDetails;
    this.setState({ loading: false });
    ticketTypeDetails = await getAllTicketTypes();
    genreDetails = await allGenreDetails();
    let getTags = await getAllTagList(userId);
    this.setMyPageLoader(true);
    //userFilterListingsData = await getAddListingsFilter();
    this.setMyPageLoader(false);
    await this.setState({
      ticketTypeDetails: ticketTypeDetails,
      genreDetails: genreDetails,
      getTags: getTags,
      //filterListingsData: userFilterListingsData,
      //quantities: quantityArray
    });
  }
  async componentDidUpdate() {
    this.setHistoryPageTitle();
  }
  setHistoryPageTitle = async () => {
    if (!this.props.history.location.pageTitle) {
      this.props.history.push({ pageTitle: 'Add Listing', pageSubTitle: 'subtitle', redirectPath: "listings" });
    }
  }

  //To Show or Hide Loader during API calls
  async setMyPageLoader(bool) {
    if (bool) {
      this.setState({ setLoader: true });
      document.body.classList.add('loader-background');
    }
    else {
      this.setState({ setLoader: false });
      document.body.classList.remove('loader-background');
    }
  }

  toggleAdvanced() {
    this.setState({ advancefilter: !this.state.advancefilter });
  }

  dropdownFilterOption = ({ label, value, data }, string) => {
    if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };

  /**
   * 
   * @param {*} e 
   * Date filter 'startingDate' is only required, to 'endingDate' is optional and removal 
   */
  async dateRangeDisplay(e) {//Date picker
    await this.setState({ startDate: e[0], endDate: e[1] });
    var startingDate = '';
    var startingDateOnly = '';
    var endingDate = '';
    var endingDateOnly = '';

    if (e[0]) {
      var starting = new Date(this.state.startDate);
      let date = starting.getDate() < 9 ? ('0' + starting.getDate()) : starting.getDate();
      let month = starting.getMonth() < 9 ? '0' + (starting.getMonth() + 1) : starting.getMonth() + 1;
      startingDateOnly = starting.getFullYear() + '-' + month + '-' + date
      startingDate = startingDateOnly + ' 00:00:00';
    }
    if (e[1]) {
      var ending = new Date(this.state.endDate);
      let date = ending.getDate() < 9 ? ('0' + ending.getDate()) : ending.getDate();
      let month = ending.getMonth() < 9 ? ('0' + (ending.getMonth() + 1)) : ending.getMonth() + 1;
      endingDateOnly = ending.getFullYear() + '-' + month + '-' + date;
      endingDate = endingDateOnly + ' 23:59:59';
    }
    var dateFilter = {};
    if (startingDate) {
      dateFilter.from_date = startingDate;
    }
    if (endingDate) {
      dateFilter.to_date = endingDate;
    }
    let assignData = this.state.searchData;
    assignData.from_date = startingDate;
    assignData.to_date = endingDate;
    await this.setState({ searchData: assignData, from_dateOnly: startingDateOnly, to_dateOnly: endingDateOnly, singleDate: '' });
    await this.paginationProcess();
  }

  /**
   * Pagination function
   */
  async paginationProcess() {
    let assignData = this.state.searchData;
    let userId = this.context.user.id;
    assignData.currentPage = 1;
    assignData.pageLimit = this.state.pageLimit;
    this.setMyPageLoader(true);
    var eventCountDetail = await getEventCount(userId, assignData);
    var eventCount = 0;
    let totalPages = 0;
    let paginationData = this.state.paginationStates;
    if (eventCountDetail && eventCountDetail.countInfo) {
      eventCount = parseInt(eventCountDetail.countInfo.count, eventCount);
      totalPages = Math.ceil(eventCountDetail.countInfo.count / this.state.pageLimit);
      paginationData.currentPage = 1;
    }
    paginationData.totalPages = totalPages;
    let showPagination = true;
    let searchedData = await getAddListingsFilter(assignData);
    let rowCount = 0;
    if (searchedData) {
      rowCount = searchedData.length;
    }
    let nextPageInActive = false;
    if (paginationData.totalPages < 2 || rowCount < this.state.pageLimit) {
      nextPageInActive = true;
      showPagination = false;
    }
    this.setMyPageLoader(false);
    this.setState({ searchData: assignData, filterListingsData: searchedData, eventCount, paginationStates: paginationData, showPagination: showPagination, rowCount: rowCount, nextPageInActive: nextPageInActive, previousPageInActive: true });
  }
  async previousPage(toFirst) {
    let userId = this.context.user.id;
    let assignData = this.state.searchData;

    let paginationData = this.state.paginationStates;
    assignData.pageLimit = this.state.pageLimit;
    let totalPages = 0;
    paginationData.currentPage = parseInt(paginationData.currentPage) - 1;
    let pageNumber = paginationData.currentPage;
    if (toFirst || pageNumber < 2) {
      pageNumber = 1;
    }
    var eventCountDetail = await getEventCount(userId, assignData);
    assignData.currentPage = pageNumber;

    if (eventCountDetail && eventCountDetail.countInfo && eventCountDetail.countInfo.count) {
      totalPages = Math.ceil(eventCountDetail.countInfo.count / this.state.pageLimit);
      paginationData.currentPage = pageNumber;
    }

    paginationData.totalPages = totalPages
    let searchedData = await getAddListingsFilter(assignData);
    let nextPageInActive = true;
    if (paginationData.currentPage < paginationData.totalPages) {
      nextPageInActive = false;
    }
    let previousPageInActive = false;
    if (paginationData.currentPage < 2) {
      previousPageInActive = true;
    }
    let showPagination = true;
    if (paginationData.totalPages == 1) {
      showPagination = false;
    }
    let rowCount = 0;
    if (searchedData) {
      rowCount = searchedData.length
    }
    await this.setState({ paginationStates: paginationData, filterListingsData: searchedData, previousPageInActive: previousPageInActive, nextPageInActive: nextPageInActive, showPagination: showPagination, rowCount: rowCount })
  }

  async nextPage(toLast) {
    let userId = this.context.user.id;
    let assignData = this.state.searchData;

    let paginationData = this.state.paginationStates;
    assignData.pageLimit = this.state.pageLimit;
    let totalPages = 0;
    assignData.currentPage = parseInt(paginationData.currentPage) + 1;
    let pageNumber = assignData.currentPage;

    var eventCountDetail = await getEventCount(userId, assignData);
    if (eventCountDetail && eventCountDetail.countInfo && eventCountDetail.countInfo.count) {
      totalPages = Math.ceil(eventCountDetail.countInfo.count / this.state.pageLimit);
      if (toLast) {
        pageNumber = totalPages;
      }
      paginationData.currentPage = pageNumber;
    }
    paginationData.totalPages = totalPages;

    let searchedData = await getAddListingsFilter(assignData);

    let previousPageInActive = false;
    if (paginationData.currentPage < 2) {
      previousPageInActive = true;
    }
    let nextPageInActive = true;

    if (paginationData.currentPage < paginationData.totalPages) {
      nextPageInActive = false;
    }
    let showPagination = true;
    if (paginationData.totalPages == 1) {
      showPagination = false;
    }
    let rowCount = 0;
    if (searchedData) {
      rowCount = searchedData.length
    }
    await this.setState({ paginationStates: paginationData, filterListingsData: searchedData, nextPageInActive: nextPageInActive, previousPageInActive: previousPageInActive, showPagination: showPagination, rowCount: rowCount })
  }
  /**
   * 
   * @param {*} e 
   * Selecting days in multi option
   */
  async daySelected(e) {
    var day_select = this.state.selectedDays;

    const index = day_select.indexOf(e.target.value);
    if (e.target.value == 'all') {
      if (e.target.checked == true) {
        day_select = daysFull.map(days => days.toLowerCase());
      }
      else if (e.target.checked == false) {
        day_select = [];
      }
    }
    else if (day_select.includes(e.target.value)) {
      day_select.splice(index, 1);
    }
    else {
      day_select.push(e.target.value);
    }
    await this.setState({ selectedDays: day_select });

    let filteredData;
    let stateHolder = this.state.searchData;
    stateHolder['day_of_week'] = day_select;
    if (this.state.searchAdvance == false) {
      stateHolder['searches'] = '';
    }
    await this.setState({ searchData: stateHolder, enableFilter: true });
  }

  /**
   * Reset the values of advanced search filter
   */
  async advanceFilterReset() {


    var searchData = {
      searches: "",
      tags: "",
      section: "",
      row: "",
      quantity: '',
      from_date: this.state.searchData.from_date,
      to_date: this.state.searchData.to_date,
      day_of_week: "",
      in_hand: "",
      inventory_attached: "",
      ticket_type: "",
      category: ""
    }

    var multiSelect = {
      ticketType: [],//single selection of multiselect
      genre: [],
      inv_attach: [],
      row: [],
    };

    var changeState = { searchData: searchData, selectedDays: [], advanceFilterSubmit: false, multiSelect: multiSelect, enableFilter: false };

    await this.setState(changeState);
    await this.paginationProcess();


  }

  /**
  * Submit the values of advanced search filter
  */
  async advanceFilterSubmit() {
    let stateHolder = this.state.searchData;
    stateHolder.currentPage = this.state.currentPage;
    stateHolder.pageLimit = this.state.pageLimit;
    await this.paginationProcess();
    // var filteredData = await getAddListingsFilter(stateHolder);
    // // this.setMyPageLoader(false);
    // /** To filter the event day based on local time **Start** */
    // var day_select = this.state.searchData.day_of_week;
    // if (day_select.length > 0) {
    //   let days_array = [];
    //   var filteredDataNew = [];
    //   day_select.map((day) => {
    //     filteredData.map((row) => {
    //       let days = moment.utc(row.eventdate).local().format('dddd').toLowerCase();
    //       if (day == days) {
    //         filteredDataNew.push(row);
    //       }
    //     })
    //     days_array.push(day.value)
    //   });
    //   filteredData = filteredDataNew;
    // }
    /* *Start* */
    // this.setState({ filterListingsData: filteredData, searchData: stateHolder, advanceFilterSubmit: true, advancefilter: !this.state.advancefilter });
    this.setState({ advanceFilterSubmit: true, advancefilter: !this.state.advancefilter });


  }


  /**
   * Method name : listSearchChange
   * Searching the event id, name, etc by user search
   */
  async listSearchChange(e) {   // For autofill search suggestion

    let filteredData = {};
    let suggestions = [];
    const value = e && e.target ? e.target.value : '';
    let stateHolder = this.state.searchData;
    stateHolder.searches = value;
    this.setState({ locationText: '', searchText: value, displayText: value, searchData: stateHolder });
    if (parseInt(value)) {
      this.setMyPageLoader(true);
      suggestions = await eventSearches({ searches: value });
      this.setMyPageLoader(false);
    }
    else if (value.length > 3) {
      this.setMyPageLoader(true);
      suggestions = await eventSearches({ searches: value });
      this.setMyPageLoader(false);
    }
    else {
      suggestions = [];
      if (value.length == 0) {
        this.setMyPageLoader(true);
        stateHolder.currentPage = this.state.currentPage;
        stateHolder.pageLimit = this.state.pageLimit;
        filteredData = await getAddListingsFilter(stateHolder);
        this.setMyPageLoader(false);
        this.setState({ filterListingsData: filteredData });
      }
    }
    this.setState(() => ({
      suggestions,
    }));
  }

  /**
   * 
   * @param {*} keyword 
   * // Refresh the listing based on search filter suggestion onclick
   */
  async selectListing(keyword = false) {
    await this.setState({ suggestions: null, displayText: keyword.toString(), });
    let allSearchData = this.state.searchData;
    allSearchData.searches = keyword.toString();
    await this.setState({ searches: allSearchData })
    this.setMyPageLoader(true);
    let stateHolder = this.state.searchData;
    // stateHolder.currentPage = this.state.currentPage;
    // stateHolder.pageLimit = this.state.pageLimit;
    // let searchedData = await getAddListingsFilter(stateHolder);
    // this.setMyPageLoader(false);
    // this.setState({ filterListingsData: searchedData });
    await this.setState({ searchData: stateHolder });
    await this.paginationProcess();
  }


  /**
   * filtering keyword column in data table
   * @param {*} e 
   * @param {*} keyword 
   * all params are required
   */

  async advanceFilter(e, keyword, state) {
    if (e) { e = [e]; } else { e = []; };
    let filteredData;
    let stateHolder = this.state.searchData;
    let multiHolder = this.state.multiSelect;
    if (state && keyword != listingConst.in_hand) {
      let newValue = e.filter(ar => !this.state.multiSelect[state].find(rm => (rm.label === ar.label && ar.value === rm.value)))
      multiHolder[state] = newValue;
      if (state == listingConst.quantity) {
        stateHolder[keyword] = newValue[0] ? parseInt(newValue[0].value) : '';
      }
      else { stateHolder[keyword] = newValue[0] ? newValue[0].value.toString() : ''; }
    }
    else if (listingConst.tags == keyword) {
      let tag_values = [];
      if (e && e.length) {
        tag_values = e[0].map(element => Number(element.value));
      }
      stateHolder[listingConst.tags] = tag_values;
    }
    else if (keyword == listingConst.in_hand) {
      var starting = new Date(e);
      let date = starting.getDate() < 9 ? ('0' + starting.getDate()) : starting.getDate();
      let month = starting.getMonth() < 9 ? '0' + (starting.getMonth() + 1) : starting.getMonth() + 1;
      stateHolder[keyword] = e != null ? starting.getFullYear() + '-' + month + '-' + date : '';
      this.setState({ singleDate: e });
    }
    else { stateHolder[keyword] = e.target.value; }


    if (this.state.searchAdvance == false) {
      stateHolder['searches'] = '';
    }
    this.setState({ searchData: stateHolder, multiSelect: multiHolder, enableFilter: true });

    // this.setMyPageLoader(true);
    // filteredData = await getAddListingsFilter(stateHolder);
    // /** To filter the event day based on local time **Start** */
    // if (this.state.searchData.day_of_week && this.state.searchData.day_of_week.length > 0) {
    //   let week_days = this.state.searchData.day_of_week;
    //   let days_array = [];
    //   var filteredDataNew = [];
    //   week_days.map((day) => {
    //     filteredData.map((row) => {
    //       let days = moment.utc(row.eventdate).local().format('dddd').toLowerCase();
    //       if (day == days) {
    //         filteredDataNew.push(row);
    //       }
    //     })
    //     days_array.push(day.value)
    //   });
    //   filteredData = filteredDataNew;
    // }
    // /* *Start* */
    // this.setMyPageLoader(false);
    // this.setState({ searchData: stateHolder, multiSelect: multiHolder, filterListingsData: filteredData });
  }
  /**
   * To avoid user change the readonly/disable attributes forcefully
   */
  async avoidOnchange(e) {
    e.preventDefault();
    return false;
  }
  /**
   * 
   * returns the data in ul list dropdown below the search field
   */
  renderSuggestions = () => {
    const { suggestions } = this.state;

    if (suggestions.length === 0) {
      return null;
    }

    return (
      <ul>
        {suggestions.map(events => <li onClick={() => this.selectListing(events)} >{events}</li>)}
      </ul>
    )
  }

  //Single Event Card Link to Add Listing Page
  goToAddListing = (eventId) => {
    this.props.history.push('add-listing/' + eventId); return;
  }

  //Handle Change Event for Events List
  async handleEventChange(e) {
    let eventId = e.target.value;
    if (e.target.checked && eventId > 0) {
      await this.setState({ eventIds: [...this.state.eventIds, eventId] });
    }
    else {
      var array = [...this.state.eventIds];
      var index = array.indexOf(eventId);
      if (index !== -1) {
        array.splice(index, 1);
        await this.setState({ eventIds: array });
      }
    }
  }

  //Multiple Event Cards Link to Add Listing Page
  createListingGroup() {
    // window.open('add-listing/' + this.state.eventIds, '_self');
    this.props.history.push('add-listing/' + this.state.eventIds);
  }

  onPageChanged = async (data) => {
    const { totalPages, pageLimit } = data;

    var { currentPage } = data;
    if (currentPage == 0 || currentPage == 1) {
      currentPage = 1;
    }
    let userId = this.context.user.id;
    let assignData = this.state.searchData;
    assignData.currentPage = currentPage;
    assignData.pageLimit = this.state.pageLimit;
    let searchedData = await getAddListingsFilter(assignData);
    var eventCountDetail = await getEventCount(userId, assignData);
    var eventCount = 0;
    if (eventCountDetail && eventCountDetail.countInfo) {
      eventCount = parseInt(eventCountDetail.countInfo.count, eventCount)
    }
    await this.setState({ filterListingsData: searchedData, currentPage, pageLimit, totalPages, eventCount });
  }

  render() {
    const { suggestions } = this.state

    const week_days = weekDays;
    const ticketTypeDetails = [];
    this.state.ticketTypeDetails && this.state.ticketTypeDetails.map(tickets =>
      ticketTypeDetails.push({ label: tickets.name, value: tickets.id.toString() },)
    )
    const genres = [];
    this.state.genreDetails && this.state.genreDetails.map(genre =>
      genres.push({ label: genre.name, value: genre.id.toString() },)
    )
    const days = daysFull;
    const monthNames = monthNamesShort;

    const tagValues = [];
    this.state.getTags && this.state.getTags.length > 0
      && this.state.getTags.map((item, i) => {
        var combined = { label: item.tag_name, value: item.id }
        tagValues.push(combined);
      })

    return !this.state.loading ? (



      <div>
        <UserContext.Consumer>
          {context => (
            <div>
              <div className="loader-bg">
                <ClipLoader color={'#4a88c5'} loading={this.state.setLoader} size={40} />
              </div>
              <div className="myTicket-item">
                <div className="search-item">
                  <div className="search-form">
                    <form onSubmit={e => { e.preventDefault(); }}>
                      <div className="input-group form-group" style={{ position: 'relative' }}>
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <span className="ant-input-suffix">
                              <SearchIcon />
                            </span>
                          </span>
                        </div>
                        <input type="text" className="form-control" onChange={this.listSearchChange} placeholder="Search by Event ID, Performer, Venue and City" value={this.state.displayText} />
                        {suggestions && suggestions.length > 0 &&
                          < div className="TypeAheadDropDown">
                            {suggestions && this.state.locationText == '' &&
                              this.renderSuggestions()
                            }
                          </div>
                        }
                        {this.state.searchText &&
                          <button className="search-close-icon" onClick={(e) => this.listSearchChange()} >X</button>
                        }

                      </div>
                    </form>
                  </div>
                  <div className="date-range">
                    <div className="date-filter-itemQr">
                      <div className="form-group-rgtr">
                        {/* <p>Select Date Range</p> */}
                        <form>
                          <div className="date-filter-top  d-flex">
                            <div className="d-flex">
                              <div className="date--PickQr">
                                <span className="custom-calendar-img">
                                  <img src={iconInhand} />
                                </span>
                                {/* <CalendarIcon /> */}
                                <div   >
                                  <DatePicker
                                    selectsRange={true}
                                    startDate={this.state.startDate}
                                    endDate={this.state.endDate}
                                    onChange={this.dateRangeDisplay}
                                    onChangeRaw={(e) => this.avoidOnchange(e)}
                                    isClearable={true}
                                    dateFormat="dd/MM/yyyy"
                                    placeholderText=" DD/MM/YYYY - DD/MM/YYYY"
                                  />
                                </div>
                              </div>
                              <div className={this.state.advancefilter ? "filter-icon fill-color" : "filter-icon"} onClick={this.toggleAdvanced}>
                                <span className="line-filter-icon"><Filter /></span>
                                <span className="fill-filter-icon"><FilterFill /></span>
                              </div>
                            </div>
                            <div className="form-group ml-auto">
                              <Link to="/events/find">
                                <button type="button" className="btn btn-primary stylefont-weight-medium">I Can't Find My Event</button>
                              </Link>
                              &emsp;
                              <button type="button" className="btn btn-primary create_list-btn " disabled={!this.state.eventIds.length} onClick={() => this.createListingGroup()}>Create Listing</button>
                            </div>
                            {/* {this.state.filterListingsData && this.state.filterListingsData.length > 0 && (
                              <div className="align-items-center">


                                <Pagination totalRecords={this.state.eventCount} eventCount={this.state.filterListingsData.length} pageLimit={this.state.pageLimit} pageNeighbours={0} onPageChanged={this.onPageChanged.bind(this)} firstEventId={this.state.filterListingsData[0].event_id} lastEventId={this.state.filterListingsData[this.state.filterListingsData.length - 1].event_id} pageName={} />
                              </div>
                            )} */}
                          </div>
                          {

                            this.state.showPagination &&
                            <div classNmae="pagination-section">
                              <div className="pagination-box">
                                <div className="d-flex align-items-center" style={{ height: '100%', gap: '20px', justifyContent: 'space-between' }}>


                                  <div key={'index1'} className="pagination-item">
                                    <button className="btn prev-btn stylefont-weight-medium" type="button" onClick={(e) =>{ this.previousPage(true); this.setState({ previousPageInActive: true })}} disabled={this.state.previousPageInActive}> First</button>
                                  </div>
                                  <div key={'index'} >
                                    <button className="btn prev-btn-icon stylefont-weight-medium" onClick={(e) => {this.previousPage(false); this.setState({ previousPageInActive: true })}} disabled={this.state.previousPageInActive} type="button">
                                      <img src={prevArrow} alt="prev-arrow" />
                                    </button>
                                  </div>
                                  <div className="pagination-number-item">
                                    {((this.state.paginationStates.currentPage - 1) * this.state.pageLimit) + 1}-
                                    {((this.state.paginationStates.currentPage - 1) * this.state.pageLimit) + this.state.rowCount}
                                  </div>
                                  <div key={'index2'} >
                                    <button type="button" className="btn next-btn-icon stylefont-weight-medium" disabled={this.state.nextPageInActive} onClick={(e) => {this.nextPage(false); this.setState({ nextPageInActive: true })}} ><img src={nextArrow} alt="next-arrow" /></button>
                                  </div>
                                  <div key={'index3'} className="pagination-item">
                                    <button type="button" className="btn next-btn stylefont-weight-medium" disabled={this.state.nextPageInActive} onClick={(e) => {this.nextPage(true); this.setState({ nextPageInActive: true })}} >Last</button>
                                  </div>
                                </div>
                              </div>
                            </div>

                          }

                          <div className={this.state.advancefilter ? "find--filter fliter-bottom listing-filter-item active" : "fliter-bottom listing-filter-item"} style={{ 'transition': '2s' }}>

                            <div className="close-filter">
                              <Button className="filter-close-btn" onClick={this.toggleAdvanced}><CloseIcon /></Button>
                            </div><div className="form-group">
                              <label>Day of Week</label>
                              {/* 
                                <MultiSelect
                                  options={week_days}
                                  className="form-control" selectAllLabel='All Days'
                                  value={this.state.selectedDays}
                                  onChange={this.daySelected}
                                  labelledBy="Select Days"
                                /> */}
                              <div className="find-listing form-day-week">
                                <div className="day-checkbox-sec">
                                  <div class="form-check">
                                    <input type="checkbox" class="form-check-input" onChange={(e) => this.daySelected(e)} value="all" checked={this.state.selectedDays && this.state.selectedDays.length == 7 ? true : false} />
                                    <label class="form-check-label">Select All</label>
                                  </div>
                                  {
                                    weekDays.map((weekDay, index) => (
                                      <div class="form-check">
                                        <input type="checkbox" class="form-check-input" onChange={(e) => this.daySelected(e)} value={weekDay.value} checked={(this.state.selectedDays && this.state.selectedDays.length == 7 || this.state.selectedDays.includes(weekDay.value)) ? true : false} />
                                        <label class="form-check-label">{weekDay.label}</label>
                                      </div>
                                    ))
                                  }
                                </div>
                              </div>

                            </div>
                            {/* <div className="form-group ">
                                <label>Ticket Type</label>

                                <MultiSelect
                                  options={ticketTypeDetails}
                                  className="form-control"
                                  value={this.state.multiSelect.ticketType}
                                  onChange={(e) => this.advanceFilter(e, listingConst.ticket_type, listingConst.ticketType)}
                                  labelledBy="Select Ticket"
                                  hasSelectAll={false}
                                  disableSearch={true}
                                />
                              </div> */}


                            <div className="row m-0">
                              <div className="form-group adduser-filter-sec singleselect--item">
                                <label>Genre</label>

                                <Select
                                  {...this.props}
                                  placeholder={"Select Genre"}
                                  filterOption={this.dropdownFilterOption}
                                  value={this.state.multiSelect.genre}
                                  options={genres}
                                  onChange={(e) => this.advanceFilter(e, listingConst.category, listingConst.genre)}
                                  allowClear
                                  showSearch
                                  isClearable
                                />
                              </div>
                              <div className="form-group adduser-filter-sec multiselect-dropdown">
                                <label>Tags</label>
                                <Select
                                  {...this.props}
                                  placeholder={"Tags"}
                                  style={{ width: "100%" }}
                                  name="tags"
                                  filterOption={this.dropdownFilterOption}
                                  value={lodashFilter(tagValues, tag => lodashIncludes(lodashGet(this.state, `searchData.${listingConst.tags}`, []), Number(tag.value)))}
                                  options={tagValues}
                                  onChange={(e) => this.advanceFilter(e, listingConst.tags)}
                                  // className={(errors.tags && touched.tags ? ' is-invalid' : '')}
                                  allowClear
                                  showSearch
                                  isClearable
                                  isMulti
                                />
                              </div>
                            </div>
                            <div className="submit-reset-btn-item">
                              <button type="button" className={`btn filter-submit-btn primary-bg-color ${this.state.enableFilter && 'active'}`} onClick={this.advanceFilterSubmit} disabled={this.state.enableFilter ? false : true} >Submit</button>
                              <a type="button" onClick={this.advanceFilterReset} className="btn filter-reset-btn active">Reset</a>
                            </div>
                          </div>

                        </form>

                      </div>

                    </div>
                  </div>


                  <div className="ticket-viewitem">
                    {
                      this.state.filterListingsData && this.state.filterListingsData.length > 0 ? this.state.filterListingsData.map((row) => (
                        <div className={this.state.eventIds.includes(row.event_id.toString()) ? "card card-active" : "card"}>
                          <div className="card-body">
                            <div>
                              <div className="ava-ticket-detail view-ticket--listing">
                                <div className="check_ticket">
                                  <div className="left-radio-btn">
                                    <form>
                                      <div className="form-group checkbox-item">
                                        <input className="styled-checkbox" onChange={(e) => this.handleEventChange(e)} id={'checkbox_' + row.event_id} type="checkbox" value={row.event_id} checked={this.state.eventIds.includes(row.event_id.toString())} />
                                        {/* <Link to={'/add-listing/' + row.event_id}> 
                                        </Link> */}
                                        <label htmlFor={'checkbox_' + row.event_id}></label>
                                      </div>
                                    </form>
                                  </div>
                                  <Link to={'/add-listing/' + row.event_id}>
                                    <div className="ticket-details-item" >
                                      <div className="ticket-header">
                                        <div className="d-flex">
                                          <div className="left-header secondary-font-family"><h1>{row.eventname}</h1></div><span className="event-id-number stylefont-weight-medium">Event ID: {row.event_id}</span>
                                        </div>
                                      </div>
                                      <div className="ava--detail">
                                        <div className="view-list-txt--item">
                                          <h1 className="date--time primary-color stylefont-weight-medium">
                                            {row.timezone && row.timezone[0] ? moment.utc(row.eventdate).tz(row.timezone[0]).format('dddd - MMM Do YYYY, hh:mm A z') + ' ' : moment.utc(row.eventdate).format('dddd - MMM Do YYYY, hh:mm A z') + ' '}
                                          </h1>
                                          <h2 className="loaction-text stylefont-weight-bold secondary-color">{row.venuename + ((row.venuename != "") ? ', ' : '') + row.city + ', ' + row.state + ', ' + row.country}</h2>
                                        </div>
                                      </div>
                                    </div>
                                  </Link>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      )
                      ) : <div className="card">
                        <div className="card-body">
                          <div>

                            <div className="ava-ticket-detail">
                              <div className="check_ticket">
                                <h4 className="no-date-text stylefont-weight-medium">No Data Found.</h4>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    }
                    {/* {this.state.filterListingsData && this.state.filterListingsData.length > 0 && (
                      <div className="d-flex flex-row align-items-center">
                          <Pagination totalRecords={this.state.eventCount} eventCount={this.state.filterListingsData.length} pageLimit={this.state.pageLimit} pageNeighbours={0} onPageChanged={this.onPageChanged.bind(this)} firstEventId={this.state.filterListingsData[0].event_id} lastEventId={this.state.filterListingsData[this.state.filterListingsData.length - 1].event_id} />
                        </div>
                      </div>
                    )} */}
                    <div className="bottom-pagination-item">
                      {
                        this.state.showPagination &&
                        <div classNmae="pagination-section">
                          <div className="pagination-box">
                            <div className="d-flex align-items-center" style={{ height: '100%', gap: '20px', justifyContent: 'space-between' }}>


                              <div key={'index1'} className="pagination-item">
                                <button className="btn prev-btn stylefont-weight-medium" type="button" onClick={(e) => { this.previousPage(true); this.setState({ previousPageInActive: true }) }} disabled={this.state.previousPageInActive}> First</button>
                              </div>
                              <div key={'index'} >
                                <button className="btn prev-btn-icon stylefont-weight-medium" onClick={(e) => { this.previousPage(false); this.setState({ previousPageInActive: true }) }} disabled={this.state.previousPageInActive} type="button">
                                  <img src={prevArrow} alt="prev-arrow" />
                                </button>
                              </div>
                              <div className="pagination-number-item">
                                {((this.state.paginationStates.currentPage - 1) * this.state.pageLimit) + 1}-
                                {((this.state.paginationStates.currentPage - 1) * this.state.pageLimit) + this.state.rowCount}
                              </div>
                              <div key={'index2'} >
                                <button type="button" className="btn next-btn-icon stylefont-weight-medium" disabled={this.state.nextPageInActive} onClick={(e) => { this.nextPage(false); this.setState({ nextPageInActive: true }) }} ><img src={nextArrow} alt="next-arrow" /></button>
                              </div>
                              <div key={'index3'} className="pagination-item">
                                <button type="button" className="btn next-btn stylefont-weight-medium" disabled={this.state.nextPageInActive} onClick={(e) => { this.nextPage(true); this.setState({ nextPageInActive: true }) }} >Last</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </UserContext.Consumer>
      </div >
    ) : (
      <Loader />
    );
  }
}

export default withRouter(MyListings);
