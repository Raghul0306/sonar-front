import React, { Component } from "react";
import { Link } from "react-router-dom";
// import moment from 'moment';
import * as moment from "moment-timezone";

import UserContext from "../../contexts/user.jsx";
import Card from 'antd/es/card';
import Switch from 'antd/es/switch';
import Tooltip from 'antd/es/tooltip';
import { Modal, Button } from 'react-bootstrap';
import { withTranslation } from "react-i18next";
import Loader from "../Loader/Loader";
import ClipLoader from "react-spinners/ClipLoader";
import { listingConst, monthNamesShort, daysFull, weekDays, shareTypes, levelNames, filterListingData, sortListingData, allListingStatus, seatTypeNames } from "../../constants/constants";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';
import { Row, Col, FormInput } from "shards-react";
import prevArrow from "../../assets/prev-arrow.png";
import nextArrow from "../../assets/next-arrow.png";
import { Alert } from "../../components/modal/common-modal";
import {
  getUserFilterListings, getAllTicketTypes, allGenreDetails, getAllEventsArrayList, eventSearches, updateListingPrice, updateListing, fetchListById, fetchChannelValues,
  getAllTagList, getAllTicketingSystemList, getAllDisclosureList, getAllAttributeList,
  allTicketingSystemAccounts, getAllChannels, updateShareListings, updateInternalNotesListings, updateDisclosuresInListings, updateSplitsInListings, getAllSplits, removeListing, updateinhandDateInListings, getInHandDateListing, getListingData, updateEventLevelListingPrice, getListingIconId, updateListingGroupStatusById, filterAllDeliveredListingsBySeller, updateListingGroupMarkAsSold, updateMultiListLevelListingPrice, getEventCount, checkInhandDateIsEqual, updateViagogoStatus, updateFlagStatus, createIssueFlag, getAllIssueTypes, getListingGroupDataBasedOnListingGroupId, createViagogoBrokerOrder, getAllDeliveryStatusDetails, getAllSeatTypesList
} from "../../services/listingsService.js";
import Select from 'react-select'
import { getAllUsers, getAllUsersByRole } from "../../services/usersService.js";
//Design Starts

import styled from 'styled-components'
import { FilterCircle } from '@styled-icons/bootstrap/FilterCircle'
import { FilterCircleFill } from '@styled-icons/bootstrap/FilterCircleFill'
import { Search } from '@styled-icons/bootstrap/Search'
import { Megaphone } from '@styled-icons/ionicons-outline/Megaphone'
import { Calendar } from '@styled-icons/ionicons-outline/Calendar'
import { ClipboardTextLtr } from '@styled-icons/fluentui-system-regular/ClipboardTextLtr'
import { DollarCircle } from '@styled-icons/boxicons-regular/DollarCircle'
import { PauseCircle } from '@styled-icons/boxicons-regular/PauseCircle'
import { AddCircle } from '@styled-icons/fluentui-system-regular/AddCircle'
import { CallSplit } from '@styled-icons/material-outlined/CallSplit'
import { Edit } from '@styled-icons/feather/Edit'
import { Share } from '@styled-icons/ionicons-outline/Share'
import { ArrowIosDownwardOutline } from '@styled-icons/evaicons-outline/ArrowIosDownwardOutline';
import { UpArrow } from '@styled-icons/boxicons-solid/UpArrow';
import { DownArrow } from '@styled-icons/boxicons-regular/DownArrow';
import { Add } from '@styled-icons/material-rounded/Add';
import { Minus } from '@styled-icons/heroicons-solid/Minus';
import { Close } from '@styled-icons/ionicons-outline/Close';
import { Tick } from '@styled-icons/typicons/Tick';
//React date picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
//React date picker
// Multi select for day of week
import MultiSelect from "react-multi-select-component";
import lodashGet from 'lodash/get';
import lodashFilter from 'lodash/filter';
import lodashIncludes from 'lodash/includes';

// icon images import
import iconDisclosures from "../../assets/listing-icon/MyTickets-icon_disclosures.png";
import iconInhand from "../../assets/listing-icon/MyTickets-icon_inhand-date.png";
import iconSplits from "../../assets/listing-icon/MyTickets-icon_splits.png";
import iconPurchaseOrder from "../../assets/listing-icon/MyTickets-icon_purchase-orders.png";
import iconPrice from "../../assets/listing-icon/MyTickets-icon_price.png";
import iconUnShare from "../../assets/listing-icon/MyTickets-icon-unshare.png";
import iconShare from "../../assets/listing-icon/MyTickets-icon_share.png";
import iconAddListing from "../../assets/listing-icon/MyTicketsicon_add-listing_.png";
import iconNotes from "../../assets/listing-icon/MyTickets-icon_notes.png";
import iconCalendarImg from "../../assets/myTicketsIconCalendar.png";
import iconFlagOn from "../../assets/listing-icon/mytickets_icon_flag_on.png";
import iconFlagOff from "../../assets/listing-icon/mytickets_icon_flag_off.png";
import iconViagogoActive from "../../assets/listing-icon/MyTickets-icon_viagogo.png";
import iconViagogoHold from "../../assets/listing-icon/MyTicket_icon_viagogo_hold.png";


const Filter = styled(FilterCircle)`
  color: #4a88c5;
  width:40px;
  height:40px;
`
const FilterFill = styled(FilterCircleFill)`
  color: #4a88c5;
  width:40px;
  height:40px;
`
const SearchIcon = styled(Search)`
  color: #001F1D;
  width:20px;
  height:20px;
`
const SpeakerIcon = styled(Megaphone)`
  color: #3a3a3b;
  width:42px;
  height:42px;
`
const CalendarIcon = styled(Calendar)`
  color: #3a3a3b;
  width:42px;
  height:42px;
`
const CalendarIconInput = styled(Calendar)`
  color: #3a3a3b;
  width:30px;
  height:30px;
`
const NotePadIcon = styled(ClipboardTextLtr)`
  color: #3a3a3b;
  width:42px;
  height:42px;
`
const DollarCircleIcon = styled(DollarCircle)`
  color: #3a3a3b;
  width:42px;
  height:42px;
`
const PauseCircleIcon = styled(PauseCircle)`
color: #3a3a3b;
width:42px;
height:42px;
`
const AddCircleIcon = styled(AddCircle)`
color: #3a3a3b;
width:42px;
height:42px;
`
const DirectionsIcon = styled(CallSplit)`
color: #3a3a3b;
width:42px;
height:42px;
`
const EditIcon = styled(Edit)`
color: #3a3a3b;
width:42px;
height:42px;
`
const UploadIcon = styled(Share)`
color: #3a3a3b;
width:42px;
height:42px;
`
const Downarrow = styled(ArrowIosDownwardOutline)`
color: #3a3a3b;
width:42px;
height:42px;
`
const FilterSort = styled(FilterCircle)`
color: #fff;
width:30px;
height:30px;
`
const UpArrowFill = styled(UpArrow)`
color: #2C91EE;
width:12px;
height:12px;
`

const UpArrowBorder = styled(DownArrow)`
color: #556573;
width:12px;
height:12px;
`
const AddIcon = styled(Add)`
color: #2C91EE;
width:30px;
height:30px;
`
const MinusIcon = styled(Minus)`
color: #BEBEBE;
width:30px;
height:30px;
`
const CloseIcon = styled(Close)`
color: #556573;
width:30px;
height:30px;
`
const TickIcon = styled(Tick)`
color: #ffffff;
width:30px;
height:30px;
`
// tooltip
const disclosuresTip = <span>Disclosures</span>;
const inHandDateTip = <span>In-Hand Date</span>;
const splitsTip = <span>Splits</span>;
const purchaseOrdersTip = <span>Purchase Orders</span>;
const priceEventTip = <span>Price Event</span>;
const shareTip = <span>Share</span>;
const unShareTip = <span>Unshare</span>;
const addListingTip = <span>Add Listing</span>;

//Design Ends

export class MyListings extends Component {
  static contextType = UserContext;
  state = {
    loading: true,
    event_id: '',
    suggestions: [],
  };

  constructor(props) {
    super(props)
    this.listSearchChange = this.listSearchChange.bind(this);
    this.selectLisitng = this.selectLisitng.bind(this);
    this.toggleAdvanced = this.toggleAdvanced.bind(this);
    this.dateRangeDisplay = this.dateRangeDisplay.bind(this);
    this.advanceFilter = this.advanceFilter.bind(this);
    this.daySelected = this.daySelected.bind(this);

    this.handleContextMenu = this.handleContextMenu.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.handleAlertSubmitButtonModal = this.handleAlertSubmitButtonModal.bind(this);
    this.contextMenuEvent = this.contextMenuEvent.bind(this);
    this.viewListing = this.viewListing.bind(this);
    this.convertToInput = this.convertToInput.bind(this);
    this.convertToLabel = this.convertToLabel.bind(this);
    this.updateColumn = this.updateColumn.bind(this);
    this.handleAddClick = this.handleAddClick.bind(this);
    this.appendedFormInternalNotesOnchange = this.appendedFormInternalNotesOnchange.bind(this);
    this.appendedFormExternalNotesOnchange = this.appendedFormExternalNotesOnchange.bind(this);
    this.discSelected = this.discSelected.bind(this);
    this.attrSelected = this.attrSelected.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
    this.viagogoChangeHandler = this.viagogoChangeHandler.bind(this);

    this.channelManagmentOnchange = this.channelManagmentOnchange.bind(this);
    this.channelManagmentToggleOnchange = this.channelManagmentToggleOnchange.bind(this);
    this.channelSlide = this.channelSlide.bind(this);
    // this.appendedFormAttachmentOnchange = this.appendedFormAttachmentOnchange.bind(this);
    this.priceUpdateModal = this.priceUpdateModal.bind(this);
    this.shareListing = this.shareListing.bind(this);
    this.shareModal = this.shareModal.bind(this);
    this.internalNotesListing = this.internalNotesListing.bind(this);
    this.internalNotesModal = this.internalNotesModal.bind(this);
    this.handleInternalNotesChange = this.handleInternalNotesChange.bind(this);
    this.handleExternalNotesChange = this.handleExternalNotesChange.bind(this);
    this.disclosuresListing = this.disclosuresListing.bind(this);
    this.splitsListing = this.splitsListing.bind(this);
    this.handleEnterEventForModals = this.handleEnterEventForModals.bind(this);
    this.disclosuresModal = this.disclosuresModal.bind(this);
    this.splitsModal = this.splitsModal.bind(this);
    this.changeDisclosures = this.changeDisclosures.bind(this);
    this.changeSplits = this.changeSplits.bind(this);
    this.avoidOnchange = this.avoidOnchange.bind(this);
    this.eventLevelPrice = this.eventLevelPrice.bind(this);
    this.handleEventLevelPriceUpdate = this.handleEventLevelPriceUpdate.bind(this);
    this.updateEventLevelPrice = this.updateEventLevelPrice.bind(this);
    this.handleEventChange = this.handleEventChange.bind(this);
    this.createListingGroup = this.createListingGroup.bind(this);
    this.markAsSold = this.markAsSold.bind(this);
    this.updateMarkAsSold = this.updateMarkAsSold.bind(this);
    this.sortModal = this.sortModal.bind(this);
    this.advanceFilterSubmit = this.advanceFilterSubmit.bind(this);
    this.advanceFilterReset = this.advanceFilterReset.bind(this);
    this.multiPriceModal = this.multiPriceModal.bind(this);
    this.channelManagmentIncressOnchange = this.channelManagmentIncressOnchange.bind(this);
    this.channelManagmentDecressOnchange = this.channelManagmentDecressOnchange.bind(this);
    this.updatePriceToggleOnchange = this.updatePriceToggleOnchange.bind(this);
    this.dropdownFilterOption = this.dropdownFilterOption.bind(this);
    this.handleSortModal = this.handleSortModal.bind(this);
    this.handleCheckedElement = this.handleCheckedElement.bind(this);
    this.filterAndSortListingElement = this.filterAndSortListingElement.bind(this);
    this.setSortingElement = this.setSortingElement.bind(this);
    this.selectAllListingData = this.selectAllListingData.bind(this);
    this.changeStateDataAfterSorting = this.changeStateDataAfterSorting.bind(this);
    this.resetSorting = this.resetSorting.bind(this);
    this.avoidEvalue = this.avoidEvalue.bind(this);
    this.showTooltip = this.showTooltip.bind(this);
    this.paginationProcess = this.paginationProcess.bind(this);
    this.state = {
      listingsData: [],
      filterListingsData: [],
      displayText: '',
      startDate: null,
      endDate: null,
      singleDate: null,
      advancefilter: false,
      quantities: [],
      changeEventStatusStatus: false,

      showViagogoModal: false,
      viagogoListingGroupData: [],
      viagogoInHandDate: null,  
      selectedViagogoDisclosures: [], 
      selectedViagogoTags: [], 
      viagogoFormFieldsData: [], 

      commonInfoModalMessage: '', 
      showCommonInfoModal: '', 
      alertCloseButtonModal: false, 

      currentEventId: "",
      avoidMultiEmptyCall: false,
      selectedDays: [],
      innerRef: React.createRef(),
      searchData: {
        focusButton: React.createRef(),
        searches: "",
        tags: "",
        section: "",
        row: "",
        quantity: '',
        from_date: "",
        to_date: "",
        day_of_week: "",
        in_hand: "",
        inventory_attached: "",
        ticket_type: "",
        category: "",
        hold: '',
        isSearchEnable: false,
        tagIds: [],
        completedEvents: false
      },

      multiSelect: {
        ticketType: [],//single selection of multiselect
        genre: [],
        inv_attach: [],
        row: [],
        quantity: []
      },
      from_dateOnly: '',
      to_dateOnly: '',
      setLoader: false, //loader
      listIdRC: 0,
      listEventIdRC: 0,
      listInActive: true,
      addUserVisible: false,
      dynamicUpdate: [],
      inputList: [{
        event_id: "",
        in_hand: "",
        tagids: [],
        attributeids: [],
        disclosureids: [],
        channel_ids: [],
        ticket_type_id: "",
        seat_type_id: "",
        splits: "",
        ticket_system_id: "",
        hide_seat_numbers: "",
        ticket_system_account: "",
        quantity: "",
        row: "",
        seat_start: "",
        seat_end: "",
        price: "",
        face_value: "",
        group_cost: "",
        unit_cost: "",
        internal_notes: "",
        external_notes: "",
        section: "",
        userId: ""
      }],
      listDataDetails: [],
      selectedDiscolsures: [],
      shareConfirmationModal: false,
      lastConfirmation: false,
      priceShowConfModal: false,
      inhandShowConfModal: false,
      levelName: '',
      modalListingGroupId: 0,
      modalEventId: 0,
      modalEventDate: '',
      shareType: '',
      myModalTitle: '',
      myModalCancelBtn: '',
      myModalSubmitBtn: '',
      internalNotes: '',
      externalNotes: '',
      disclosuresList: [],
      splitsList: [],
      selectedDisclosures: [],
      selectedSplits: 0,
      updateInhandDate: null,
      commonAlertModal: false,
      alertTitle: '',
      removeListingModal: false,
      searchAdvance: false,
      validation: {
        split: false,
        price: false,
        disclosure: false,
        updatePrice: false,
        inHandDate: false
      },
      listLevelIcon: {
        in_hand: "",
        internal_notes: "",
        splits_id: "0",

      },
      listingGroupIds: [],
      listingGroupIdsObject: { event_id: 0, listing_id: [] },
      eventLevelPriceUpdate: false,
      eventIds: [],
      listingSold: false,
      listingStatusViagogoHold: false,
      listingTicketTypeViagogoHold: '', 
      listingSeatTypeViagogoHold: '', 
      currentPage: null,
      totalPages: null,
      pageLimit: 25,
      sortModal: false,
      advanceFilterSubmit: false,
      eventLevelPrice: '',
      eventLevelUpdatePrice: '',
      priceUpdateType: '%',
      priceUpdateConfirmationModal: false,
      sharingConfirmationModal: false,
      priceByEvent: 'Change all prices in the event to ',
      priceByList: 'Change the price on all selected listings by ',
      changeByEvent: true,
      changeByList: false,
      listRowStatus: [], //for active/inactive
      listRowDisplay: [],//for active/inactive display boolean
      listRowActiveInactive: [{ type: false, displayOption: true }],
      filterEventId: "",
      filterListingElementRadio: '',
      filterListingElement: [
        { value: filterListingData.urgent, isChecked: false },
        { value: filterListingData.sold, isChecked: false },
        { value: filterListingData.available, isChecked: false },
        { value: filterListingData.inactive, isChecked: false },
       // { value: filterListingData.flag, isChecked: false }
      ],
      selectAllListing: false,
      selectFlaggedListing: false,
      selectViagogoListing: false,
      sortListingType: {
        sortListingDataBySectionInAscending: false,
        sortListingDataBySectionInDescending: false,
        sortListingDataByRowInAscending: false,
        sortListingDataByRowInDescending: false,
        sortListingDataByPriceInAscending: false,
        sortListingDataByPriceInDescending: false
      },
      eventCount: 0,
      toolTipId: 0,
      enableFilterSubmit: false,
      disclosureValues: [],
      disclosuresLabel: [],
      deliveryStatusList: [],
      paginationStates: {
        currentPage: 1,
        totalRows: 0,
        totalPages: 0,

      },
      showPagination: false,
      nextPageInActive: false,
      previousPageInActive: false,
      rowCount: 0,
      searchText: '',
      poid_link: 0,
      assignedTo: {},
      viagogoSelectedStatus: [],
      selectedListingRows: [],
      selectedListingEventId: 0,
      showViagogoHold: false,
      isFlagged: false,
      isMultipleFlag: false,
      createIssueModal: false,
      showIssueFlagConfirm: false,
      issueModalValues: {
        assign: [],
        issueType: [],
        issueTag: [],
        issueNotes: ''
      },
      flagIssueResponse: { title: '', msg: '' },
      flagIconStatus: '',
      issueValues: {
        assign: '',
        issueTag: '',
        issueType: '',
        issueNotes: '',
        assigner: ''
      },
      soldFlagHide: true,
      priceChangeListingGroupIds: [],
      showPriceMenu: false,
      eventLevelFilter: [],
      selectedArrow: ''

    }

  }

  /**
* Page : Listings
* Function For :  Change listing group status
* Ticket No : TIC-250
*/

  async changeListingGroupStatus(list_id) {//list id
    this.setState({ changeEventStatusStatus: true, currentEventId: list_id, });
  }

  async changeListingGroupDbStatus() {
    const userId = this.context.user.id;
    var status = await updateListingGroupStatusById(this.state.listingGroupIds.length ? this.state.listingGroupIds : this.state.currentEventId);
    if (status.status_data) {
      this.setMyPageLoader(true);
      var filteredData = await getUserFilterListings(userId, this.state.searchData);
      this.setState({ filterListingsData: filteredData, changeEventStatusStatus: false, currentEventId: "", listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] }, listRowDisplay: [], listRowStatus: [] });
      this.setMyPageLoader(false);
    }
  }

  async componentDidMount() {
    this.setMyPageLoader(true)
    const userId = this.context.user.id;
    let quantityArray = []
    for (var quantityCount = 0; quantityCount < 100; quantityCount++) {  // Dynamic quantity 1-100
      quantityArray[quantityCount] = { 'label': quantityCount + 1, 'value': quantityCount + 1 };
    }
    let userFilterListingsData;
    let ticketTypeDetails;
    let genreDetails;
    let getTags;
    let getTicketingSystems;
    let getDisclosures;
    let getAttributes;
    let getTicketingSystemAccounts;
    let getSplits;
    let eventCountDetail;
    let deliveryStatusListDetails;
    this.setState({ loading: false });
    ticketTypeDetails = await getAllTicketTypes();
    genreDetails = await allGenreDetails();
    // userFilterListingsData = await getAddListingsFilter();
    let getAllEventsArrayLists = await getAllEventsArrayList();
    getTags = await getAllTagList(userId);
    getTicketingSystems = await getAllTicketingSystemList();
    getDisclosures = await getAllDisclosureList(userId);
    deliveryStatusListDetails = await getAllDeliveryStatusDetails();
    let all_getDisclosures = await getAllDisclosureList();
    getAttributes = await getAllAttributeList(userId);
    getTicketingSystemAccounts = await allTicketingSystemAccounts(userId);
    getSplits = await getAllSplits();
    eventCountDetail = await getEventCount(userId);

    let issueTypes = await getAllIssueTypes();



    let allUsers = await getAllUsers();
    var eventCount = 0;
    if (eventCountDetail && eventCountDetail.countInfo) {
      eventCount = parseInt(eventCountDetail.countInfo.count)
    }
    let getAllUsersByRoles = await getAllUsersByRole(this.context.user.id);
    const allAssigneeList = [];
    getAllUsersByRoles && getAllUsersByRoles.length > 0
      && getAllUsersByRoles.map((users) => {
        var combined = { label: users.first_name+' '+users.last_name, value: users.id }
        allAssigneeList.push(combined);
      })

    // this.setMyPageLoader(true);
    let assignData = this.state.searchData;
    assignData.currentPage = 1;
    assignData.pageLimit = this.state.pageLimit;
    // userFilterListingsData = await getUserFilterListings(userId, assignData);
    let disclosuresDtls = getDisclosures;
    if (disclosuresDtls && disclosuresDtls.length > 0) {
      var disclosuresArr = [];
      for (var disclosureCount = 0; disclosureCount < disclosuresDtls.length; disclosureCount++) {
        var obj = { label: disclosuresDtls[disclosureCount].disclosure_name, value: disclosuresDtls[disclosureCount].id };
        disclosuresArr.push(obj);
      }
      await this.setState({ disclosuresList: disclosuresArr });
    }
    await this.paginationProcess();
    await this.setState({
      ticketTypeDetails: ticketTypeDetails,
      genreDetails: genreDetails,
      //filterListingsData: userFilterListingsData,
      quantities: quantityArray,
      getTags: getTags,
      getTicketingSystems: getTicketingSystems,
      getDisclosures: getDisclosures,
      all_getDisclosures: all_getDisclosures,
      getAttributes: getAttributes,
      getTicketingSystemAccounts: getTicketingSystemAccounts,
      splitsList: getSplits,
      eventCount: eventCount,
      allUserDetails: allUsers,
      allAssigneeDetails: allAssigneeList,
      issueTypes: issueTypes,
      deliveryStatusList: deliveryStatusListDetails
    });
    this.setMyPageLoader(false);

  }

  async updateModelStatus() {
    let self = this.state;
    setTimeout(function () {
      self.innerRef.current.focus();
    }, 1000);
  }


  //Drop down search filter
  dropdownFilterOption = ({ label, value, data }, string) => {
    if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };

  //channelManagmentOnchange
  async channelManagmentOnchange(e, typeNumber) {
    var statearrayvalue = e.target.value;
    let updatedChanelInputList = this.state.getChannels;
    updatedChanelInputList[typeNumber].markup_amount = statearrayvalue;
    await this.setState({
      getChannels: updatedChanelInputList
    });
  }

  async channelSlide(e, i) {
    var statearrayvalue = e;
    if (!statearrayvalue) {
      var channel_markup_type_id_value = "1";
    } else {
      var channel_markup_type_id_value = "2";
    }
    let updatedChanelInputList = this.state.getChannels;
    updatedChanelInputList[i].channel_markup_type_id = channel_markup_type_id_value;
    await this.setState({
      getChannels: updatedChanelInputList
    });
  }

  async channelManagmentToggleOnchange(value, statearraynumber) {
    var statearrayvalue = value;
    if (statearrayvalue) {
      var channel_markup_type_id_value = "1";
    } else {
      var channel_markup_type_id_value = "2";
    }
    let updatedChanelInputList = this.state.channelManagmentInputList;
    updatedChanelInputList[statearraynumber].channel_markup_type_id = channel_markup_type_id_value;
    await this.setState({
      channelManagmentInputList: updatedChanelInputList
    });
  }

  componentWillUnmount() {
    document.removeEventListener('contextmenu', this.handleContextMenu);
    document.removeEventListener('click', this.handleClick);
    document.removeEventListener('scroll', this.handleScroll);
  }

  //To Show or Hide Loader during API calls
  async setMyPageLoader(bool) {
    if (bool) {
      this.setState({ setLoader: true });
      document.body.classList.add('loader-background');
    }
    else {
      this.setState({ setLoader: false });
      document.body.classList.remove('loader-background');
    }
  }
  async contextMenuEvent(e, res) {
    if (e.button == 2) {
      document.addEventListener('contextmenu', this.handleContextMenu);
      document.addEventListener('click', this.handleClick);
      document.addEventListener('scroll', this.handleScroll);
      let sold_disable = false, poid_link = 0;
      if (this.state.listingGroupIds && (this.state.listingGroupIds.length == 0 || this.state.listingGroupIds.includes(res.listingGroupId))) {
        await this.setState({ isFlagged: res.is_flagged });
      }
      if (res.sold.length == 1 && res.sold[0] == listingConst.yesValue) {
        sold_disable = true;
      }
      if (res.purchaseid && res.purchaseid[0] != '' && res.purchaseid[0] != null) {
        poid_link = res.purchaseid[0];
      }
      var listInActive = false;
      var listingIsInViagogoHold = false;
      var seatTypeName = "";
      if (res.listing_status && res.listing_status.status === allListingStatus.inactive) {
        listInActive = true;
      }
      if (res.listing_status && res.listing_status.status === allListingStatus.viagogo_hold) {
        listingIsInViagogoHold = true;

        if (res.seat_type_id > 0) {
          let getAllSeatTypes = await getAllSeatTypesList();
          if (getAllSeatTypes && getAllSeatTypes.length > 0) {
            for (const result of getAllSeatTypes) {
              if (result.id == res.seat_type_id) {
                seatTypeName = result.name;
              }
            }
          }
        }
      }

      await this.setState({ listIdRC: res.listingGroupId, listEventIdRC: res.eventId, listInActive: listInActive, listingSold: sold_disable, poid_link: poid_link, listingStatusViagogoHold: listingIsInViagogoHold, listingTicketTypeViagogoHold: res.tickettypename[0], listingSeatTypeViagogoHold: seatTypeName });

      return false;
    }
  }

  handleAlertSubmitButtonModal = async (modalpopupstatus) => {
    await this.setState({
      alertCloseButtonModal: true,
      showCommonInfoModal: false
    });
  }

  async handleListingGroupChange(e, event_Id, list_Id, list_status, listGroupStatus, isSold, flagStatus) {
    let listingGroupId = e.target.value;
    let listRowStatus = this.state.listRowStatus;
    let listRowDisplayStatus = this.state.listRowDisplay;
    let selectedListingRowsStatus = this.state.selectedListingRows;
    let listRowActiveInactive = this.state.listRowActiveInactive;
    let showPriceMenu = false;
    if (this.state.selectedListingEventId != 0 && this.state.selectedListingEventId != event_Id) {
      selectedListingRowsStatus = [];
    }

    if (e.target.checked) {
      listRowStatus.push({ row_id: list_Id, state: list_status[0] });
      listRowDisplayStatus.push(list_status[0]);
      selectedListingRowsStatus.push({ row_id: list_Id, state: listGroupStatus, flag: flagStatus, sold: isSold.length == 1 && isSold[0] == listingConst.yesValue ? true : false })
    }
    else {
      for (var listStatusCount = 0; listStatusCount < listRowStatus.length; listStatusCount++) {
        if (listRowStatus[listStatusCount].row_id == list_Id) {
          listRowStatus.splice(listStatusCount, 1);
          listRowDisplayStatus.splice(listStatusCount, 1);
          selectedListingRowsStatus.splice(listStatusCount, 1)
        }
      }
    }

    var isMultipleFlag = false;
    if (selectedListingRowsStatus && selectedListingRowsStatus.length) {
      selectedListingRowsStatus.map(flagStatus => {        
        if (flagStatus.flag == true) { isMultipleFlag = true; }
      })
    }
    this.setState({ isMultipleFlag: isMultipleFlag })

    let viagogoState = [], showViagogoHold = false, soldFlagState = [], soldFlagHide = true;
    if (selectedListingRowsStatus && selectedListingRowsStatus.length > 1) {
      for (const selectedRowState of selectedListingRowsStatus) {
        viagogoState.push(selectedRowState.state);
        soldFlagState.push(selectedRowState.sold)
      }

      if (viagogoState && viagogoState.length > 1) {
        const unique = (value, index, self) => {
          return self.indexOf(value) === index
        }

        const uniqueState = viagogoState.filter(unique);
        showViagogoHold = uniqueState && uniqueState.length == 1 && uniqueState[0] == allListingStatus.viagogo_hold ? true : false;
      }

      if (soldFlagState && soldFlagState.length > 1) {
        const isSold = (value, index, self) => {
          return self.indexOf(value) === index
        }
        const uniqueSoldState = soldFlagState.filter(isSold);
        soldFlagHide = uniqueSoldState && uniqueSoldState.length == 1 && uniqueSoldState[0] == true ? false : true;
      }

    }
    else if (selectedListingRowsStatus.length && selectedListingRowsStatus.length == 1) {
      if (selectedListingRowsStatus[0].sold == true) {
        showPriceMenu = true; //disable price context menu for single sold row selected
      }
    }
    if (soldFlagState.includes(true)) {
      showPriceMenu = true;
    }


    if (listRowDisplayStatus && listRowDisplayStatus.length == 1) {
      listRowActiveInactive = [{ type: listRowDisplayStatus[0], displayOption: true }]
    }
    else if (listRowDisplayStatus && listRowDisplayStatus.length > 1) {
      let stateCount = [];
      for (var listRowCount = 0; listRowCount < listRowDisplayStatus.length; listRowCount++) {
        if (!stateCount.includes(listRowDisplayStatus[listRowCount])) {
          stateCount.push(listRowDisplayStatus[listRowCount])
        }
      }
      if (stateCount && stateCount.length > 1) {
        listRowActiveInactive = [{ type: false, displayOption: false }]
      }
      else {
        listRowActiveInactive = [{ type: listRowDisplayStatus[0], displayOption: true }]
      }
    }


    let checkedRows = this.state.listingGroupIdsObject;
    let uniqueViagogoStatus = [];
    if (checkedRows.event_id == event_Id) {
      if (e.target.checked) {
        if (e.nativeEvent.shiftKey) {
          if (listRowStatus.length) {
            let eventDetails = this.state.filterListingsData.filter(e => e.event_id && e.event_id == event_Id)
            if (eventDetails && eventDetails.length) {
              var listingGroupDetails = eventDetails[0].listingDtls;

              let selectedListingGroup = listRowStatus.filter(listRow => listingGroupDetails.find(listingGroup => (listingGroup.listingGroupId == listRow.row_id)));

              var startFrom = listingGroupDetails.findIndex(function (list) {
                return list.listingGroupId == selectedListingGroup[selectedListingGroup.length - 2].row_id
              });

              var endList = listingGroupDetails.findIndex(function (list) {
                return list.listingGroupId == list_Id
              });

              var listingDetails = listingGroupDetails.slice(startFrom, endList + 1)

              if (listingDetails && listingDetails.length) {
                listingDetails.map(listing => {
                  checkedRows.listing_id.push(listing.listingGroupId)
                })
              } else {
                listingDetails = listingGroupDetails.slice(endList, startFrom + 1)

                if (listingDetails && listingDetails.length) {
                  listingDetails.map(listing => {
                    checkedRows.listing_id.push(listing.listingGroupId)
                  })
                }
              }

            }
          }
        } else {
          checkedRows.listing_id.push(list_Id)
        }
      }
      else {
        var index = checkedRows.listing_id.indexOf(list_Id);
        if (index !== -1) {
          checkedRows.listing_id.splice(index, 1);
        }
      }
    }
    else {
      checkedRows.listing_id = [];
      checkedRows.event_id = event_Id;
      checkedRows.listing_id.push(list_Id);
    }

    await this.setState({ listingGroupIdsObject: checkedRows, listingGroupIds: checkedRows.listing_id, listRowStatus: listRowStatus, listRowDisplay: listRowDisplayStatus, listRowActiveInactive: listRowActiveInactive, selectedListingRows: selectedListingRowsStatus, selectedListingEventId: event_Id, showViagogoHold: showViagogoHold, soldFlagHide: soldFlagHide, showPriceMenu: showPriceMenu });
  }
  /**
  * 
  * @param {*} e 
  * Selecting days in multi option for attributes
  */
  async attrSelected(e) {
    this.setState({ selectedAttributes: e });
  }
  /**
  * 
  * @param {*} e 
  * Selecting days in multi option for disclosures
  */
  async discSelected(e) {
    await this.setState({ selectedDiscolsures: e });
  }
  /**
   * 
   * @param {*} e 
   * Selecting days in multi option for tags
   */
  async tagSelected(e) {
    this.setState({ selectedTags: e });
  }
  async changeHandler(e, field) {
    let ticketing_state = this.state.listDataDetails;
    ticketing_state[field] = e.target.value;
    await this.setState({ listDataDetails: ticketing_state })
  }
  async viagogoChangeHandler(e, field) {
    let dataField = this.state.viagogoListingGroupData;
    if (field == 'delivery_status_id') {
      dataField[field] = e;
    }
    else {
      dataField[field] = e.target.value;
    }

    if (field == listingConst.seat_start || field == listingConst.seat_end)//Quantity Calculation
    {
      dataField['quantity'] = 0;
      if (dataField.seat_start > 0 && dataField.seat_end > 0) {
        dataField['quantity'] = ((dataField.seat_end - dataField.seat_start) + 1) > 0 ? (dataField.seat_end - dataField.seat_start) + 1 : 0;
      }
    }
    else if (field == listingConst.in_hand)//In Hand Date Format
    {
      var starting = new Date(dataField[field]);
      let date = starting.getDate() < 9 ? ('0' + starting.getDate()) : starting.getDate();
      let month = starting.getMonth() < 9 ? '0' + (starting.getMonth() + 1) : starting.getMonth() + 1;
      dataField[field] = e != null ? starting.getFullYear() + '-' + month + '-' + date : '';
    }
    await this.setState({ viagogoListingGroupData: dataField })
  }
  /**
   * Load the event list data in pop-up
   * @param {*} state 
   */
  async viewListing(state) {
    if (state) {
      const selectedDiscs = [];
      const selectedAttr = [];
      const selectedTags = [];
      let listData = await fetchListById(this.state.listIdRC);
      let channelData = await fetchChannelValues(this.state.listIdRC);
      let getChannels = await getAllChannels();
      getChannels.sort(function (x, y) { return x.channel_name == listingConst.yadara ? -1 : y.channel_name == listingConst.yadara ? 1 : 0; });
      listData && listData.disclosureids && this.state.getDisclosures && this.state.getDisclosures.length > 0
        && this.state.getDisclosures.map((item, i) => {
          if (listData.disclosureids.includes(parseInt(item.id))) {
            var combined = { label: item.disclosure_name, value: item.id }
            selectedDiscs.push(combined);
          }
        })

      listData && listData.attributeids && this.state.getAttributes && this.state.getAttributes.length > 0
        && this.state.getAttributes.map((item, i) => {
          if (listData.attributeids.includes(parseInt(item.id))) {
            var combined = { label: item.attribute_name, value: item.id }
            selectedAttr.push(combined);
          }
        })
      listData && listData.tagids && this.state.getTags && this.state.getTags.length > 0
        && this.state.getTags.map((item, i) => {
          if (listData.tagids.includes(parseInt(item.id))) {
            var combined = { label: item.tag_name, value: item.id }
            selectedTags.push(combined);
          }
        })

      if (listData) {
        var inHand = new Date(listData.in_hand);
        let month = inHand.getMonth() + 1
        if (month < 10) {
          month = '0' + (inHand.getMonth() + 1);
        }
        const inHandDate = inHand.getFullYear() + '-' + month + '-' + inHand.getDate();
        listData.in_hand = inHandDate;
        const diffTime = Math.abs(new Date(listData.dateDiff) - new Date());
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        listData.diffDays = diffDays;
      }

      channelData && channelData.length && channelData.map((c_item) => {
        getChannels && getChannels.length && getChannels.map((g_item, i) => {
          if (g_item.id == c_item.channel_id) {
            getChannels[i].markup_amount = c_item.markup_amount;
            getChannels[i].channel_markup_type_id = c_item.channel_markup_type_id;
            getChannels[i].rowId = c_item.id;
          }
        })
      })
      await this.setState({
        selectedDiscolsures: selectedDiscs, selectedAttributes: selectedAttr, addUserVisible: state, listDataDetails: listData, selectedTags: selectedTags, getChannels: getChannels
      })
    }
    else {
      this.setState({ addUserVisible: state });
    }
  }
  toggleAdvanced() {
    this.setState({ advancefilter: !this.state.advancefilter });
  }

  async removeListing(e) {
    const userId = this.context.user.id;
    this.setState({ removeListingModal: false })
    // let channelData = await removeListing(this.state.listIdRC);
    let channelData = await removeListing(this.state.listingGroupIdsObject.listing_id);

    if (!channelData.isError) {
      this.commonAlertModal(true, channelData.msg);
      var filteredData = await getUserFilterListings(userId, this.state.searchData);
      var eventCountDetail = await getEventCount(userId, this.state.searchData);
      var eventCount = 0;
      if (eventCountDetail && eventCountDetail.countInfo) {
        eventCount = parseInt(eventCountDetail.countInfo.count)
      }
      this.setState({ filterListingsData: filteredData, eventCount, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } });
      // this.viewListing(false)
    }

  }
  commonAlertModal(bool, msg) {
    this.setState({ commonAlertModal: bool, alertTitle: msg });
  }

  /**
   * 
   * @param {*} e 
   * Date filter 'startingDate' is only required, to 'endingDate' is optional and removal 
   */
  async dateRangeDisplay(e) {//Date picker
    await this.setState({ startDate: e[0], endDate: e[1] });
    var startingDate = '';
    var startingDateOnly = '';
    var endingDate = '';
    var endingDateOnly = '';
    if (e[0]) {
      var starting = new Date(this.state.startDate);
      let date = starting.getDate() < 9 ? ('0' + starting.getDate()) : starting.getDate();
      let month = starting.getMonth() < 9 ? '0' + (starting.getMonth() + 1) : starting.getMonth() + 1;
      startingDateOnly = starting.getFullYear() + '-' + month + '-' + date
      startingDate = startingDateOnly + ' 00:00:00';
    }
    if (e[1]) {
      var ending = new Date(this.state.endDate);
      let date = ending.getDate() < 9 ? ('0' + ending.getDate()) : ending.getDate();
      let month = ending.getMonth() < 9 ? ('0' + (ending.getMonth() + 1)) : ending.getMonth() + 1;
      endingDateOnly = ending.getFullYear() + '-' + month + '-' + date;
      endingDate = endingDateOnly + ' 23:59:59';
    }
    var dateFilter = {};
    if (startingDate) {
      dateFilter.from_date = startingDate;
    }
    if (endingDate) {
      dateFilter.to_date = endingDate;
    }
    let assignData = this.state.searchData;
    assignData.from_date = startingDate;
    assignData.to_date = endingDate;
    assignData.in_hand = '';
    const userId = this.context.user.id;
    this.setMyPageLoader(true);
    if (this.state.searchAdvance == false) {
      assignData['searches'] = '';
    }
    // let searchedData = await getUserFilterListings(userId, assignData);
    // var eventCountDetail = await getEventCount(userId, assignData);
    // var eventCount = 0;
    // if (eventCountDetail && eventCountDetail.countInfo) {
    //   eventCount = parseInt(eventCountDetail.countInfo.count)
    // }
    this.setMyPageLoader(false);
    await this.setState({ searchData: assignData, from_dateOnly: startingDateOnly, to_dateOnly: endingDateOnly, singleDate: '' });
    //, filterListingsData: searchedData, eventCount
    await this.paginationProcess();

  }

  /**
   * Pagination function
   */
  async paginationProcess() {
    let assignData = this.state.searchData;
    let userId = this.context.user.id;
    assignData.currentPage = 1;
    assignData.pageLimit = this.state.pageLimit;
    this.setMyPageLoader(true);
    var eventCountDetail = await getEventCount(userId, assignData);
    var eventCount = 0;
    let totalPages = 0;
    let paginationData = this.state.paginationStates;
    if (eventCountDetail && eventCountDetail.countInfo) {
      eventCount = parseInt(eventCountDetail.countInfo.count, eventCount);
      totalPages = Math.ceil(eventCountDetail.countInfo.count / this.state.pageLimit);
      paginationData.currentPage = 1;
    }

    paginationData.totalPages = totalPages
    let showPagination = true;
    let searchedData = await getUserFilterListings(userId, assignData);
    let rowCount = 0;
    if (searchedData) {
      rowCount = searchedData.length
    }
    let nextPageInActive = false;
    if (paginationData.totalPages <= 1 || rowCount < this.state.pageLimit) {
      nextPageInActive = true;
      showPagination = false;
    }
    this.setMyPageLoader(false);
    await this.setState({ searchData: assignData, filterListingsData: searchedData, eventCount, paginationStates: paginationData, showPagination: showPagination, rowCount: rowCount, nextPageInActive: nextPageInActive, previousPageInActive: true, eventLevelFilter: [], selectedArrow: '' });
  }
  async previousPage(toFirst) {
    let userId = this.context.user.id;
    let assignData = this.state.searchData;
    let paginationData = this.state.paginationStates;
    assignData.pageLimit = this.state.pageLimit;
    let totalPages = 0;

    var eventCountDetail = await getEventCount(userId, assignData);
    paginationData.currentPage = parseInt(paginationData.currentPage) - 1;
    let pageNumber = paginationData.currentPage;
    if (toFirst || paginationData.currentPage < 2) {
      pageNumber = 1;
    }
    assignData.currentPage = pageNumber;
    if (eventCountDetail && eventCountDetail.countInfo && eventCountDetail.countInfo.count) {
      totalPages = Math.ceil(eventCountDetail.countInfo.count / this.state.pageLimit);
      paginationData.currentPage = pageNumber;
    }
    paginationData.totalPages = totalPages
    let searchedData = await getUserFilterListings(userId, assignData);
    let nextPageInActive = true;
    if (paginationData.currentPage < paginationData.totalPages) {
      nextPageInActive = false;
    }
    let previousPageInActive = false;
    if (paginationData.currentPage < 2) {
      previousPageInActive = true;
    }
    let showPagination = true;
    if (paginationData.totalPages == 1) {
      showPagination = false;
    }
    let rowCount = 0;
    if (searchedData) {
      rowCount = searchedData.length
    }
    await this.setState({ paginationStates: paginationData, filterListingsData: searchedData, previousPageInActive: previousPageInActive, nextPageInActive: nextPageInActive, showPagination: showPagination, rowCount: rowCount })
  }

  async nextPage(toLast) {
    let userId = this.context.user.id;
    let assignData = this.state.searchData;

    let paginationData = this.state.paginationStates;
    assignData.pageLimit = this.state.pageLimit;
    let totalPages = 0;
    assignData.currentPage = parseInt(paginationData.currentPage) + 1;
    let pageNumber = assignData.currentPage;
    var eventCountDetail = await getEventCount(userId, assignData);
    if (eventCountDetail && eventCountDetail.countInfo && eventCountDetail.countInfo.count) {
      totalPages = Math.ceil(eventCountDetail.countInfo.count / this.state.pageLimit);
      if (toLast) {
        pageNumber = totalPages;
      }
      paginationData.currentPage = pageNumber;
    }
    paginationData.totalPages = totalPages;
    let searchedData = await getUserFilterListings(userId, assignData);

    let previousPageInActive = false;
    if (paginationData.currentPage < 2) {
      previousPageInActive = true;
    }
    let nextPageInActive = true;

    if (paginationData.currentPage < paginationData.totalPages) {
      nextPageInActive = false;
    }

    let showPagination = true;
    if (paginationData.totalPages < 2) {
      showPagination = false;
    }
    let rowCount = 0;
    if (searchedData) {
      rowCount = searchedData.length
    }
    await this.setState({ paginationStates: paginationData, filterListingsData: searchedData, nextPageInActive: nextPageInActive, previousPageInActive: previousPageInActive, showPagination: showPagination, rowCount: rowCount })
  }
  /**
   * 
   * @param {*} e 
   * Selecting days in multi option
   */
  async daySelected(e) {
    var day_select = this.state.selectedDays;
    const index = day_select.indexOf(e.target.value);
    if (e.target.value == 'all') {
      if (e.target.checked == true) {
        day_select = daysFull.map(days => days.toLowerCase());
      }
      else if (e.target.checked == false) {
        day_select = [];
      }
    }
    else if (day_select.includes(e.target.value)) {
      day_select.splice(index, 1);
    }
    else {
      day_select.push(e.target.value);
    }
    await this.setState({ selectedDays: day_select });
    let filteredData;
    let stateHolder = this.state.searchData;
    stateHolder['day_of_week'] = day_select;
    stateHolder.isSearchEnable = true;
    // this.setMyPageLoader(true);
    if (this.state.searchAdvance == false) {
      stateHolder['searches'] = '';
    }

    await this.setState({ searchData: stateHolder });

  }

  /**
   * Reset the values of advanced search filter
   */
  async advanceFilterReset() {
    var searchData = {
      searches: "",
      tags: "",
      section: "",
      row: "",
      quantity: '',
      from_date: this.state.searchData.from_date,
      to_date: this.state.searchData.to_date,
      day_of_week: "",
      in_hand: "",
      inventory_attached: "",
      ticket_type: "",
      category: "",
      hold: '',
      isSearchEnable: false
    }
    var multiSelect = {
      ticketType: [],//single selection of multiselect
      genre: [],
      inv_attach: [],
      row: [],
      quantity: []
    };
    var changeState = { searchData: searchData, selectedDays: [], singleDate: null, selectedTags: [], multiSelect: multiSelect, advanceFilterSubmit: false };
    const userId = this.context.user.id;
    /*if (this.state.advanceFilterSubmit) {
      var filteredData = await getUserFilterListings(userId);
      var eventCountDetail = await getEventCount(userId);
      var eventCount = 0;
      if (eventCountDetail && eventCountDetail.countInfo) {
        eventCount = parseInt(eventCountDetail.countInfo.count)
      }
      changeState.filterListingsData = filteredData;
      changeState.eventCount = eventCount;
    }*/
    await this.setState(changeState);
    await this.paginationProcess();



  }

  /**
   * Submit the values of advanced search filter
   */
  async advanceFilterSubmit() {
    const userId = this.context.user.id;
    let stateHolder = this.state.searchData;
    await this.paginationProcess();
    // var filteredData = await getUserFilterListings(userId, stateHolder);
    // var eventCountDetail = await getEventCount(userId, stateHolder);
    // var eventCount = 0;
    // if (eventCountDetail && eventCountDetail.countInfo) {
    //   eventCount = parseInt(eventCountDetail.countInfo.count)
    // }
    // this.setMyPageLoader(false);
    // /** To filter the event day based on local time **Start** */
    // var day_select = this.state.searchData.day_of_week;
    // if (day_select.length > 0) {
    //   let days_array = [];
    //   var filteredDataNew = [];
    //   day_select.map((day) => {
    //     filteredData.map((row) => {
    //       let days = moment.utc(row.eventdate).local().format('dddd').toLowerCase();
    //       if (day == days) {
    //         filteredDataNew.push(row);
    //       }
    //     })
    //     days_array.push(day.value)
    //   });
    //   filteredData = filteredDataNew;
    // }
    // /* *Start* */
    //  this.setState({ filterListingsData: filteredData, searchData: stateHolder, advanceFilterSubmit: true, advancefilter: false, eventCount });
    await this.setState({ advanceFilterSubmit: true, advancefilter: false });


  }
  /**
   * Method name : listSearchChange
   * Searching the event id, name, etc by user search
   */
  async listSearchChange(e) {   // For autofill search suggestion
    const userId = this.context.user.id;
    let filteredData = {};
    let suggestions = [];
    const value = e && e.target ? e.target.value : '';
    let stateHolder = this.state.searchData;
    stateHolder.searches = value;
    this.setState({ locationText: '', searchText: value, displayText: value, searchData: stateHolder });
    if (parseInt(value) || value.length > 3) {
      this.setMyPageLoader(true);
      suggestions = await eventSearches({ searches: value });
      this.setMyPageLoader(false);
    }
    else {
      suggestions = [];
      if (value.length == 0) {
        if (this.state.searchAdvance == false) {
          stateHolder['searches'] = '';
        }
        await this.setState({ searchData: stateHolder });
        this.paginationProcess()
      }
    }
    this.setState(() => ({
      suggestions, searchAdvance: false
    }));
  }

  /**
   * 
   * @param {*} keyword 
   * // Refresh the listing based on search filter suggestion onclick
   */
  async selectLisitng(keyword = false) {
    const userId = this.context.user.id;
    let allSearchData = this.state.searchData;
    allSearchData.searches = keyword.toString();
    this.setMyPageLoader(true);
    let searchedData = await getUserFilterListings(userId, this.state.searchData);
    var eventCountDetail = await getEventCount(userId, this.state.searchData);
    var eventCount = 0;
    if (eventCountDetail && eventCountDetail.countInfo) {
      eventCount = parseInt(eventCountDetail.countInfo.count)
    }
    await this.paginationProcess();
    this.setMyPageLoader(false);

    await this.setState({ filterListingsData: searchedData, searches: allSearchData, suggestions: null, displayText: keyword.toString(), searchAdvance: true, eventCount });
  }


  /**
   * filtering keyword column in data table
   * @param {*} e 
   * @param {*} keyword 
   * all params are required
   */

  async advanceFilter(e, keyword, state) {
    let eventCall = e;
    if (e) { e = [e]; } else { e = []; };
    let filteredData;
    let stateHolder = this.state.searchData;
    let multiHolder = this.state.multiSelect;
    if (state && keyword != listingConst.in_hand && keyword != listingConst.quantity && keyword != listingConst.inventory_attached && keyword != listingConst.ticket_type) {
      let newValue = e.filter(ar => !this.state.multiSelect[state].find(rm => (rm.label === ar.label && ar.value === rm.value)))
      multiHolder[state] = newValue;
      if (state == listingConst.quantity) {
        stateHolder[keyword] = newValue[0] ? parseInt(newValue[0].value) : '';
      }
      else {
        stateHolder[keyword] = newValue[0] && newValue[0].value ? newValue[0].value.toString() : '';
      }
    }
    else if (keyword == listingConst.in_hand) {
      var starting = new Date(eventCall);
      let date = starting.getDate() < 9 ? ('0' + starting.getDate()) : starting.getDate();
      let month = starting.getMonth() < 9 ? '0' + (starting.getMonth() + 1) : starting.getMonth() + 1;
      stateHolder[keyword] = e != null ? starting.getFullYear() + '-' + month + '-' + date : '';
      this.setState({ singleDate: eventCall });
    }
    else if ([listingConst.ticket_type, listingConst.inventory_attached, listingConst.hold].includes(keyword)) {
      stateHolder[keyword] = (stateHolder[keyword] == eventCall.target.value ? '' : eventCall.target.value);
    } else if (listingConst.tagIds == keyword) {
      if (e.length) stateHolder[listingConst.tagIds] = e[0].map(element => Number(element.value));
      else stateHolder[listingConst.tagIds] = [];
    }
    else if (keyword == listingConst.completedEvents) {
      stateHolder['completedEvents'] = !stateHolder.completedEvents;
    }
    else {
      stateHolder[keyword] = (keyword == listingConst.quantity) ? parseInt(eventCall.target.value) : eventCall.target.value;
    }

    // this.setMyPageLoader(true);
    if (this.state.searchAdvance == false) {
      stateHolder['searches'] = '';
    }
    stateHolder.isSearchEnable = true;

    this.setState({ searchData: stateHolder, multiSelect: multiHolder });
    this.setMyPageLoader(false);
  }
  /**
   * To avoid user change the readonly/disable attributes forcefully
   */
  async avoidOnchange(e) {
    e.preventDefault();
    return false;
  }
  async avoidEvalue(e, target, listingGroupId, column, eventId) {
    if (e.key == 'e') {
      e.preventDefault();
    }
    if (e.key === 'Enter') {
      if (this.state.dynamicUpdate.listValue) {
        this.setState({ priceUpdateConfirmationModal: true });
      }
      else {
        await this.setState({ priceModal: target, listingGroupId: listingGroupId, column: column, modalEventId: eventId });
        await setTimeout(
          async () => {
            if (this.state.priceUpdateConfirmationModal == false) {
              this.priceUpdateModal(false);
            }
          }, 100);
      }
    }
  }
  /**
   * 
   * returns the data in ul list dropdown below the search field
   */
  renderSuggestions = () => {
    const { suggestions } = this.state;

    if (suggestions.length === 0) {
      return null;
    }

    return (
      <ul>
        {suggestions.map((events, index) => <li key={index} onClick={() => this.selectLisitng(events)} >{events}</li>)}
      </ul>
    )
  }

  handleContextMenu = (event) => {


    let element = event.target.parentElement;


    if (element.classList.contains('rightClick-Menu') === false) {
      document.removeEventListener('contextmenu', this.handleContextMenu);
      this.setState({ visible: false, });
      return false;
    }
    event.preventDefault();

    this.setState({ visible: true });
    const clickX = event.clientX;
    const clickY = event.clientY;
    const screenW = window.innerWidth;
    const screenH = window.innerHeight;
    const rootW = this.root.offsetWidth;
    const rootH = this.root.offsetHeight;

    const right = (screenW - clickX) > rootW;
    const left = !right;
    const top = (screenH - clickY) > rootH;
    const bottom = !top;

    if (right) {
      this.root.style.left = `${clickX + 5}px`;
    }

    if (left) {
      this.root.style.left = `${clickX - rootW - 5}px`;
    }

    if (top) {
      this.root.style.top = `${clickY + 5}px`;
    }

    if (bottom) {
      this.root.style.top = `${clickY - rootH - 5}px`;
    }
  };

  handleClick = (event) => {
    const { visible } = this.state;
    const wasOutside = !(event.target.contains === this.root);

    if (wasOutside && visible) this.setState({ visible: false, });
    document.removeEventListener('contextmenu', this.handleContextMenu);
  };

  handleScroll = () => {
    const { visible } = this.state;

    if (visible) this.setState({ visible: false, });
    document.removeEventListener('contextmenu', this.handleContextMenu);
  };
  /**
   *  Double click event listing row value change 
   *  Label to input field
   * @param {*} target  - targeting which column value need to be changed
   * @param {*} listId  - Listing table id
   * @param {*} column  - column name
   * @param {*} value   - initially old value, after change new values will updated
   */
  convertToInput(target, listingGroupId, column, value, eventId) {
    let checkPrice = this.state.dynamicUpdate;
    checkPrice['oldValue_' + listingGroupId] = value;
    checkPrice[target + column] = true;
    checkPrice[target + '_' + column] = this.state.dynamicUpdate[target + '_' + column] ? this.state.dynamicUpdate[target + '_' + column] : value;
    checkPrice.listOldValue = value;
    checkPrice[target + '_id'] = listingGroupId;
    this.setState({ dynamicUpdate: checkPrice, listingGroupId, modalEventId: eventId, toolTipId: 0 });
  }
  /**
   * Double click event listing row value change 
   * Input field to label
   * @param {*} target - targeting which column value need to be changed
   */
  async convertToLabel(target, listingGroupId, column, eventId) {
    await this.setState({ priceModal: target, listingGroupId: listingGroupId, column: column, modalEventId: eventId });
    await setTimeout(
      async () => {
        if (this.state.priceUpdateConfirmationModal == false) {
          this.priceUpdateModal(false);
        }
      }, 100);
  }

  /**
   * Double click event listing row value change 
   * @param {*} e - new input field value
   * @param {*} target - targeting which column value need to be changed
   * @param {*} column - column name
   */
  updateColumn(e, target, column) {
    let checkPrice = this.state.dynamicUpdate;
    if (e.target.value.length <= listingConst.maxLength_amount) {//limit the price length
      checkPrice[target + '_' + column] = parseFloat(e.target.value);
      checkPrice.listValue = parseFloat(e.target.value);
      this.setState({ dynamicUpdate: checkPrice })
    }

  }

  async handleKeyPress(e) {
    await this.setState({ priceUpdateConfirmationModal: true });
  }

  //Internal Notes
  async appendedFormInternalNotesOnchange(e, statearraynumber) {

    let updatedInputList = this.state.listDataDetails
    updatedInputList.internal_notes = e.target.value;
    await this.setState({
      listDataDetails: updatedInputList
    });
  }

  //External Notes
  async appendedFormExternalNotesOnchange(e, statearraynumber) {

    let updatedInputList = this.state.listDataDetails
    updatedInputList.external_notes = e.target.value;
    await this.setState({
      listDataDetails: updatedInputList
    });
  }

  // Dynamic Form Function start
  async handleAddClick() {

    var items = {
      section: "new test",
      row: "",
      seat_start: "",
      seat_end: "",
      price: "",
      face_value: "",
      group_cost: "",
      unit_cost: "",
      disclosure: "",
      attribute: "",
      po_id: "",
      zone_seating: "",
      tags: "",
      internal_notes: "",
      external_notes: ""
    };
    this.setState({ inputList: [...this.state.inputList, items] }) //another array
  }
  async multiPriceModal(list_id, event_id) {
    let checkedListingRows = this.state.listingGroupIdsObject;
    let priceStatus = {
      eventLevelPriceConfirmationModal: true, myModalTitle: 'Update Price', myModalCancelBtn: 'Cancel', myModalSubmitBtn: 'Update', eventLevelPriceUpdate: false, changeByList: true, changeByEvent: false, priceUpdateType: '%'
    }
    if (checkedListingRows && checkedListingRows.listing_id && checkedListingRows.listing_id.length > 0) {
      // priceStatus.listings_id = checkedListingRows
    }
    else {
      checkedListingRows.listing_id = [];
      checkedListingRows.listing_id.push(list_id);
      checkedListingRows.event_id = event_id;
    }

    priceStatus.listingGroupIdsObject = checkedListingRows; priceStatus.eventLevelUpdatePrice = ''; priceStatus.eventLevelPrice = '';
    await this.setState(priceStatus);
    // await this.setState({ listingGroupIdsObject: checkedListingRows, eventLevelUpdatePrice: '', eventLevelPrice: '' })
  }
  async priceUpdateModal(bool) {
    let userId = this.context.user.id;
    const { listingGroupId, column, eventId } = this.state;
    let modalEventId = this.state.modalEventId;
    let checkPrice = this.state.dynamicUpdate;
    let value = this.state.dynamicUpdate['dynamicUpdate_' + listingGroupId + '_price'];
    this.setState({ priceUpdateConfirmationModal: false });
    if (bool == true) {
      this.setMyPageLoader(true)
      let updatedPrice = await updateListingPrice(listingGroupId, value, this.state.dynamicUpdate['oldValue_' + listingGroupId], modalEventId, userId);
      this.setMyPageLoader(false)
    }
    else {
      checkPrice['dynamicUpdate_' + listingGroupId + '_price'] = this.state.dynamicUpdate['oldValue_' + listingGroupId];
    }
    checkPrice[this.state.priceModal] = false;
    let userFilterListingsData = await getUserFilterListings(userId, this.state.searchData);
    var eventCountDetail = await getEventCount(userId, this.state.searchData);
    var eventCount = 0;
    if (eventCountDetail && eventCountDetail.countInfo) {
      eventCount = parseInt(eventCountDetail.countInfo.count)
    }
    await this.setState({ priceUpdateConfirmationModal: false, dynamicUpdate: [], modalEventId: 0, filterListingsData: userFilterListingsData, eventCount });

  }

  //Share & Pause Share On Click Event
  async shareListing(listingGroupId, shareType, eventId, level, counts) {
    let title = '',
      cancelBtn = '',
      submitBtn = '',
      shareModalBody = '';

    if (level == levelNames.listing) {
      if (shareType == shareTypes.share) {
        title = 'Your Listing has been shared with the market';
        cancelBtn = 'Cancel';
        submitBtn = 'OK';
      }
      else if (shareType == shareTypes.pauseShare) {
        title = 'Your Listing has been unshared from the market';
        cancelBtn = 'Cancel';
        submitBtn = 'OK';
      }
    } else {
      if (shareType == shareTypes.share) {
        title = 'Share Listings';
        cancelBtn = 'Cancel';
        submitBtn = 'Share';
        shareModalBody = 'Share all listings from the event.'
      }
      else if (shareType == shareTypes.pauseShare) {
        title = 'Unshare Listings';
        cancelBtn = 'Cancel';
        submitBtn = 'Unshare';
        shareModalBody = 'Unshare all listings from the event.'
      }
    }
    if (counts) {
      this.setState({
        shareConfirmationModal: true,
        levelName: level,
        modalListingGroupId: listingGroupId,
        shareType: shareType,
        modalEventId: eventId,
        myModalTitle: title,
        shareModalBody: shareModalBody,
        myModalCancelBtn: cancelBtn,
        myModalSubmitBtn: submitBtn
      });
    }
    else {
      this.commonAlertModal(true, 'Share cannot be updated.');
    }
  }
  async shareConfirm(bool) {

    this.setState({ sharingConfirmationModal: bool, shareConfirmationModal: false });

  }

  //Share & Pause Share Confirmation Modal
  async shareModal(bool) {
    if (bool) {
      const userId = this.context.user.id;
      let inActive = false;
      if (this.state.shareType == shareTypes.pauseShare) {
        inActive = true;
      }
      this.setMyPageLoader(true);
      let resData = await updateShareListings(this.state.modalListingGroupId, this.state.shareType, this.state.levelName, this.state.modalEventId, inActive);
      if (!resData.isError) {
        let userFilterListingsData = await getUserFilterListings(userId, this.state.searchData);
        var eventCountDetail = await getEventCount(userId, this.state.searchData);
        var eventCount = 0;
        if (eventCountDetail && eventCountDetail.countInfo) {
          eventCount = parseInt(eventCountDetail.countInfo.count)
        }
        this.setState({ filterListingsData: userFilterListingsData, shareConfirmationModal: false, sharingConfirmationModal: false, eventCount });
        this.setMyPageLoader(false);
      }

    }
    this.setState({ shareConfirmationModal: false, modalListingGroupId: 0, shareType: '', levelName: '', modalEventId: 0, sharingConfirmationModal: false });
  }

  async internalNotesListing(listingGroupId, level, eventId) {
    let icon_Values = await getListingIconId(listingGroupId);
    this.setState({
      internalNotesConfirmationModal: true,
      levelName: level,
      modalListingGroupId: listingGroupId,
      modalEventId: eventId,
      myModalTitle: 'Internal Notes Updated',
      myModalCancelBtn: 'Cancel',
      myModalSubmitBtn: 'Save',
      internalNotes: icon_Values.internal_notes
    });
  }

  async handleInternalNotesChange(e) {
    this.setState({ internalNotes: e.target.value });
  }

  async internalNotesModal(bool) {
    if (bool) {
      let resData = await updateInternalNotesListings(this.state.modalListingGroupId, this.state.internalNotes, this.state.levelName, this.state.modalEventId, []);
      if (!resData.isError) {
        this.commonAlertModal(true, resData.msg);
      }
    }
    this.setState({ internalNotesConfirmationModal: false, modalListingGroupId: 0, internalNotes: '' });
  }

  async disclosuresListing(listingGroupId, level, eventId, counts) {
    let disclosuresLists = [];
    let disclosuresDtls = this.state.getDisclosures;//await getAllDisclosureList(userId);
    if (disclosuresDtls && disclosuresDtls.length > 0) {
      var disclosuresArr = [];
      for (var disclosureCount = 0; disclosureCount < disclosuresDtls.length; disclosureCount++) {
        var obj = { label: disclosuresDtls[disclosureCount].disclosure_name, value: disclosuresDtls[disclosureCount].id };
        disclosuresArr.push(obj);
      }
      disclosuresLists = disclosuresArr;
    }

    if (counts) {
      let disclosureDetails = await getListingData(listingGroupId, level, eventId);

      if (disclosureDetails.listingData) {
        let externalNotes = disclosureDetails.listingData.external_notes;
        let updatedDisclosures = disclosureDetails.listingData.disclosures;
        let filteredArray = []
        updatedDisclosures && updatedDisclosures.length && updatedDisclosures.map(id => {
          let disclosureValues = this.state.disclosuresList.filter(x => x.value == id);
          if (disclosureValues.length) { filteredArray.push(disclosureValues[0]) }
        })
        let validation = this.state.validation;
        validation.disclosure = false;
        /*** based on user id -start*/
        let userselectedDiscs = []
        updatedDisclosures && updatedDisclosures.length && this.state.all_getDisclosures && this.state.all_getDisclosures.length > 0
          && this.state.all_getDisclosures.map((item, i) => {
            if (updatedDisclosures.includes(parseInt(item.id))) {
              var combined = { label: item.disclosure_name, value: item.id }
              userselectedDiscs.push(combined);
            }
            filteredArray = userselectedDiscs;
          })
        let newDisclosureOptions = filteredArray.filter(ar => !disclosuresLists.find(rm => (rm.label === ar.label && rm.value === ar.value)));
        if (newDisclosureOptions && newDisclosureOptions.length > 0) {
          for (var disclosuresCount = 0; disclosuresCount < newDisclosureOptions.length; disclosuresCount++) {
            disclosuresLists.push(newDisclosureOptions[disclosuresCount]);
          }
        }
        if (level == levelNames.event && disclosureDetails.listingData.disclosureOption && disclosureDetails.listingData.disclosureOption.length) {
          var disclosureOption = disclosureDetails.listingData.disclosureOption;
          for (var list of disclosureOption) {
            list.value = list.value.toString();
          }
          filteredArray = disclosureOption;
        }
        /*** based on user id -end*/
        this.setState({ externalNotes, selectedDisclosures: filteredArray, validation: validation, disclosuresList: disclosuresLists });
      }

      this.setState({
        disclosuresConfirmationModal: true,
        levelName: level,
        modalListingGroupId: listingGroupId,
        modalEventId: eventId,
        myModalTitle: 'Update Disclosures',
        myModalCancelBtn: 'Cancel',
        myModalSubmitBtn: 'Update'
      });
    }
    else {
      this.commonAlertModal(true, 'Disclosure cannot be updated.');
    }
  }

  async inHandDateListing(eventId, eventDate, counts) {
    let checkInhandDateResponse = await checkInhandDateIsEqual(eventId);
    if (checkInhandDateResponse && checkInhandDateResponse.response) {
      let inHandDate = new Date(parseInt(checkInhandDateResponse.inHandDate));
      if (inHandDate) { await this.setState({ updateInhandDate: inHandDate }) }
    }
    if (counts) {
      this.setState({
        inHandDateConfirmationModal: true,
        modalEventId: eventId,
        modalEventDate: eventDate,
        myModalTitle: 'Update In-Hand Date',
        myModalCancelBtn: 'Cancel',
        myModalSubmitBtn: 'Update',
        myModalSubmitBtnActive: ""
      });
    }
    else {
      this.commonAlertModal(true, 'In-Hand date cannot be updated.');
    }
  }

  async inHandDateModal(bool) {
    if (bool) {
      let updateInhandDate = this.state.updateInhandDate;
      const datetime = new Date(updateInhandDate);
      var year = datetime.getFullYear();
      var month = (datetime.getMonth() + 1) < 10 ? '0' + (datetime.getMonth() + 1) : (datetime.getMonth() + 1);
      var date = (datetime.getDate()) < 10 ? '0' + datetime.getDate() : datetime.getDate();
      let inHand = year + '-' + month + '-' + date;
      let resData = await updateinhandDateInListings(inHand, this.state.modalEventId, levelNames.event, []);

      if (!resData.isError) {
        //this.commonAlertModal(true, resData.msg);
        this.setState({ updateInhandDate: null });
      }
    }
    var tempValidationState = this.state.validation;
    tempValidationState.inHandDate = false;
    this.setState({ validation: tempValidationState, inHandDateConfirmationModal: false, updateInhandDate: '', inhandShowConfModal: false });
  }

  updateInhandDate(e) {
    var starting = new Date(e);
    var tempValidationState = this.state.validation;
    tempValidationState.inHandDate = false;
    this.setState({ updateInhandDate: starting, myModalSubmitBtnActive: "active", validation: tempValidationState});
  }

  async splitsListing(listingGroupId, level, eventId, counts) {
    if (counts) {
      let assignState = {
        splitsConfirmationModal: true,
        levelName: level,
        modalListingGroupId: listingGroupId,
        modalEventId: eventId,
        myModalTitle: 'Update Splits',
        myModalCancelBtn: 'Cancel',
        myModalSubmitBtn: 'Update',
        selectedSplits: ''
      }
      if (level == levelNames.listing) {
        let icon_Values = await getListingIconId(listingGroupId);
        let listLevelIcon = this.state.listLevelIcon;
        listLevelIcon.splits_id = icon_Values.splits_id ? icon_Values.splits_id : '';
        assignState.listLevelIcon = listLevelIcon;
        assignState.selectedSplits = listLevelIcon.splits_id;
      }
      this.setState(assignState);
    }
    else {
      this.commonAlertModal(true, 'Splits cannot be updated.');
    }
  }

  async changeDisclosures(e) {
    var disclosuresSelected = [];
    let validation = this.state.validation;
    validation.disclosure = true;
    e && e.map(disclosure => (
      disclosuresSelected.push(disclosure.value)
    ))
    if (disclosuresSelected.length > 0) {
      validation.disclosure = false;
    }
    this.setState({ selectedDisclosures: e, validation: validation });
  }
  filterOptions = (options, filter) => {
    if (!filter) {
      return options;
    }
    const re = new RegExp(filter, "i");
    return options.filter(({ label }) => label && label.match(re));
  };

  async changeSplits(e) {
    let validation = this.state.validation;
    validation.split = true;
    if (e.value) {
      validation.split = false
    }
    await this.setState({ selectedSplits: e.value, validation: validation });
  }

  async handleExternalNotesChange(e) {
    this.setState({ externalNotes: e.target.value });
  }

  async disclosuresNotesModal(bool) {
    let validation = this.state.validation;
    validation.disclosure = false;
    if (bool) {
      let resData = await updateDisclosuresInListings(this.state.modalListingGroupId, this.state.disclosureValues, this.state.externalNotes, this.state.levelName, this.state.modalEventId);
      if (!resData.isError) {
        this.commonAlertModal(true, resData.msg);
      }
      validation.disclosure = filterAllDeliveredListingsBySeller;
      this.setState({ selectedDisclosureValues: false, modalListingGroupId: 0, modalEventId: 0, validation: validation, disclosuresListModal: false });
    }
    else {
      this.setState({ selectedDisclosureValues: false, modalListingGroupId: 0, modalEventId: 0, validation: validation, disclosuresListModal: false });
    }
  }

  async handleEnterEventForModals(e, modalType) {
    if (e.key === "Enter") {
      if (modalType == "disclosures")//Completed - Listing & Event Level
      {
        this.disclosuresModal(true);
      }
      else if (modalType == "disclosuresConfirmation")//Completed - Listing & Event Level
      {
        this.disclosuresNotesModal(false);
      }
      else if (modalType == "inHand")//Completed - Event Level
      {
        this.setState({ inhandShowConfModal: true, inHandDateConfirmationModal: false });
      }
      else if (modalType == "inHandConfirmation")//Completed - Event Level
      {
        this.inHandDateModal(true);
      }
      else if (modalType == "internalNotes")//Completed - Listing Level
      {
        this.internalNotesModal(true);
      }
      else if (modalType == "splits")//Completed - Listing & Event Level
      {
        this.splitsModal(true);
      }
      else if (modalType == "price")//Completed - Event Level
      {
        this.confirmationPriceUpdate(true);
      }
      else if (modalType == "priceConfirmation")//Completed - Event Level
      {
        this.lastConfPriceUpdate(true);
      }
      else if (modalType == "priceLastConfirmation")//Completed - Event Level
      {
        this.updateEventLevelPrice(true);
      }
      else if (modalType == "share")//Completed - Event Level
      {
        this.shareConfirm(true);
      }
      else if (modalType == "shareConfirmation")//Completed - Event Level
      {
        this.shareModal(true);
      }
      else if (modalType == "flag")//Completed - Listing Level
      {
        this.flagIssueFormSubmit.click();
      }
      else if (modalType == "flagConfirmation")//Completed - Listing Level
      {
        this.createNewIssue();
      }
      else if (modalType == "flagLastConfirmation")//Completed - Listing Level
      {
        this.setState({ showIssueFlagSuccess: false, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } });
      }
      
    }
  }

  async disclosuresModal(bool) {
    let validation = this.state.validation;
    validation.disclosure = false;
    if (bool) {
      let disclosures = [];
      let disclosuresLabel = [];
      let selectedDisclosureValues = this.state.selectedDisclosures;
      if (selectedDisclosureValues.length > 0) {
        for (var disclosureCount = 0; disclosureCount < selectedDisclosureValues.length; disclosureCount++) {
          disclosures.push(selectedDisclosureValues[disclosureCount].value);
          disclosuresLabel.push(selectedDisclosureValues[disclosureCount].label);
        }
        this.setState({ disclosuresListModal: true, disclosuresConfirmationModal: false, disclosureValues: disclosures, disclosuresLabel: disclosuresLabel });
      }
      else {
        validation.disclosure = true;
        this.setState({ disclosuresConfirmationModal: true, validation: validation });
      }
    }
    else {
      this.setState({ disclosuresConfirmationModal: false, validation: validation, modalListingGroupId: 0, modalEventId: 0 });
    }

  }

  async splitsModal(bool) {
    let modalStatus = false;
    let validation = this.state.validation;
    if (bool && (this.state.selectedSplits == '' || this.state.selectedSplits == null)) {
      modalStatus = true;
      if (validation.split == false) {
        validation.split = true;
        this.setState({ validation: validation });
      }
    }
    else if (bool) {
      modalStatus = false;
      validation.split = false;
      let resData = await updateSplitsInListings(this.state.modalListingGroupId, this.state.selectedSplits, this.state.levelName, this.state.modalEventId);
      if (!resData.isError) {
        this.commonAlertModal(true, resData.msg);
      }
      this.setState({ splitsConfirmationModal: false, modalListingGroupId: 0, modalEventId: 0, validation: validation });
    }
    else {
      modalStatus = true;
      validation.split = false;
      this.setState({ splitsConfirmationModal: false, modalListingGroupId: 0, modalEventId: 0, validation: validation });
    }


  }

  //Price On Click Event - Event Level
  async eventLevelPrice(eventId, totalAvailableTkts, rowData) {
    let checkPriceModal = rowData; // To show the price modal if the event has unsold listing
    let showPriceModal = false;
    let priceChangeListingGroupIds = []
    if (checkPriceModal && checkPriceModal.listingDtls && checkPriceModal.listingDtls.length > 0) {
      for (const checkListStatus of checkPriceModal.listingDtls) {
        priceChangeListingGroupIds.push(checkListStatus.listingGroupId)
        if (checkListStatus.sold && checkListStatus.sold.length == 1 && checkListStatus.sold[0] == listingConst.noValue) {
          showPriceModal = true;
        }
      }
    }
    if (totalAvailableTkts > 0 && showPriceModal) {
      this.setState({
        eventLevelPriceConfirmationModal: true,
        modalEventId: eventId,
        myModalTitle: 'Update Price',
        myModalCancelBtn: 'Cancel',
        priceUpdateType: '%',
        myModalSubmitBtn: 'Update',
        eventLevelPriceUpdate: true,
        eventLevelUpdatePrice: '',
        changeByEvent: true,
        changeByList: false,
        priceChangeListingGroupIds: priceChangeListingGroupIds
      });
    }
    else {
      this.commonAlertModal(true, 'Price cannot be updated.');
    }
  }

  async handleEventLevelPriceUpdate(e) {
    let validation = this.state.validation;
    validation.price = true;
    if (e.target.value && e.target.value > 0) {
      validation.price = false;
      validation.updatePrice = false;
    }
    if (e.target.value.length <= listingConst.maxLength_amount) {
      this.setState({ eventLevelPrice: e.target.value, validation: validation, eventLevelUpdatePrice: '' });
    }
  }
  async confirmationPriceUpdate(bool) {
    let validation = this.state.validation;
    if (bool) {
      if (this.state.eventLevelPrice && this.state.eventLevelPrice >= 0) {
        validation.price = false;
        this.setState({ eventLevelPriceConfirmationModal: false, priceShowConfModal: true, validation: validation });
      }
      else if (this.state.eventLevelUpdatePrice && this.state.eventLevelPrice >= 0) {
        validation.updatePrice = false;
        this.setState({ eventLevelPriceConfirmationModal: false, priceShowConfModal: true, validation: validation });
      }
      else {
        validation.price = true; validation.updatePrice = true;
        this.setState({ validation: validation });
      }
    }
    else {
      validation.price = false;
      this.setState({ eventLevelPriceConfirmationModal: false, eventLevelPrice: '', listingGroupIds: [], validation: validation, listingGroupIdsObject: { event_id: 0, listing_id: [] } });
    }
  }
  async lastConfPriceUpdate(bool) {

    if (bool) {
      // this.updateEventLevelPrice(true);
      this.setState({ priceShowConfModal: false, lastConfirmation: true, })
    }
    else {
      this.setState({ priceShowConfModal: false, eventLevelPrice: '', listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } });
    }
  }
  async updateEventLevelPrice(bool) {
    let userId = this.context.user.id;
    let validation = this.state.validation;
    let modalStatus = true;
    if (bool) {
      if (this.state.eventLevelPrice && this.state.eventLevelPrice > 0) {
        let resData = {};
        if (this.state.eventLevelPriceUpdate) {
          resData = await updateEventLevelListingPrice(this.state.modalEventId, this.state.eventLevelPrice, userId);
        }
        else {
          resData = await updateMultiListLevelListingPrice(this.state.listingGroupIdsObject.listing_id, this.state.listingGroupIdsObject.event_id, this.state.eventLevelPrice, userId);
        }
        validation.price = false;
        modalStatus = false;
        if (!resData.isError) {
          // this.commonAlertModal(true, resData.msg);
          let userFilterListingsData = await getUserFilterListings(userId, this.state.searchData);
          var eventCountDetail = await getEventCount(userId, this.state.searchData);
          var eventCount = 0;
          if (eventCountDetail && eventCountDetail.countInfo) {
            eventCount = parseInt(eventCountDetail.countInfo.count)
          }
          this.setState({ eventLevelPriceConfirmationModal: modalStatus, modalEventId: 0, eventLevelPrice: '', validation: validation, filterListingsData: userFilterListingsData, listingGroupIds: [], lastConfirmation: false, listingGroupIdsObject: { event_id: 0, listing_id: [] }, eventCount });
        }
        else {
          this.setState({ lastConfirmation: modalStatus, listingGroupIdsObject: { event_id: 0, listing_id: [] } });
          this.commonAlertModal(true, resData.msg);
        }
      }
      else if (this.state.eventLevelUpdatePrice && this.state.eventLevelUpdatePrice != '') {
        let resData = {};
        if (this.state.eventLevelPriceUpdate) {
          resData = await updateEventLevelListingPrice(this.state.modalEventId, this.state.eventLevelUpdatePrice, userId, this.state.priceUpdateType, this.state.priceChangeListingGroupIds);
        }
        else {
          resData = await updateMultiListLevelListingPrice(this.state.listingGroupIdsObject.listing_id, this.state.listingGroupIdsObject.event_id, this.state.eventLevelUpdatePrice, userId, this.state.priceUpdateType);
        }
        validation.price = false;
        modalStatus = false;
        if (!resData.isError) {
          // this.commonAlertModal(true, resData.msg);
          let userFilterListingsData = await getUserFilterListings(userId, this.state.searchData);
          var eventCountDetail = await getEventCount(userId, this.state.searchData);
          var eventCount = 0;
          if (eventCountDetail && eventCountDetail.countInfo) {
            eventCount = parseInt(eventCountDetail.countInfo.count)
          }
          this.setState({ eventLevelPriceConfirmationModal: modalStatus, modalEventId: 0, eventLevelPrice: '', validation: validation, filterListingsData: userFilterListingsData, listingGroupIds: [], lastConfirmation: false, listingGroupIdsObject: { event_id: 0, listing_id: [] }, eventCount });
        }
        else {
          this.setState({ lastConfirmation: modalStatus });
          this.commonAlertModal(true, resData.msg);
        }
      }
      else {
        modalStatus = true;
        validation.price = true;
        this.setState({ eventLevelPriceConfirmationModal: modalStatus, validation: validation, listingGroupIds: [], lastConfirmation: false, listingGroupIdsObject: { event_id: 0, listing_id: [] } });

      }
    } else {
      modalStatus = false;
      validation.price = false;
      this.setState({ eventLevelPriceConfirmationModal: modalStatus, modalEventId: 0, eventLevelPrice: '', listingGroupIds: [], validation: validation, lastConfirmation: false, listingGroupIdsObject: { event_id: 0, listing_id: [] } });
    }
  }

  //Single Event Card Link to Add Listing Page
  goToAddListing = (eventId) => {
    window.open(`/add-listing/${eventId}`);
  }

  toggleListing = (eventId) => {
    var classDetails = document.getElementById('event-listing-mini-table-' + eventId);

    if (classDetails.classList.contains('show-event-listing')) {
      classDetails.classList.remove('show-event-listing');
      classDetails.classList.add('hide-event-listing');
    } else {
      classDetails.classList.add('show-event-listing');
      classDetails.classList.remove('hide-event-listing');
    }
  }

  //Handle Change Event for Events List
  async handleEventChange(e) {
    let eventId = e.target.value;
    if (e.target.checked && eventId > 0) {
      await this.setState({ eventIds: [...this.state.eventIds, eventId] });
    }
    else {
      var array = [...this.state.eventIds];
      var index = array.indexOf(eventId);
      if (index !== -1) {
        array.splice(index, 1);
        await this.setState({ eventIds: array });
      }
    }
  }

  //Multiple Event Cards Link to Add Listing Page
  createListingGroup() {
    window.open('add-listing/' + this.state.eventIds);
  }

  //Right Click Event - Mark As Sold - Confirmation
  async markAsSold(listingGroupId, listingStatusViagogoHold) {
    const selectedViagogoDisclosures = [],
      selectedViagogoTags = [];

    if (listingStatusViagogoHold) {
      let listingGroupData = await getListingGroupDataBasedOnListingGroupId(listingGroupId);
      let resultData = listingGroupData.listingDetails[0];
      //In Hand Date Format
      /* if (resultData.in_hand && resultData.in_hand != "" && resultData.in_hand != null) {
        var starting = new Date(resultData.in_hand);
        let date = starting.getDate() < 9 ? ('0' + starting.getDate()) : starting.getDate();
        let month = starting.getMonth() < 9 ? '0' + (starting.getMonth() + 1) : starting.getMonth() + 1;
        var inHandDate = starting.getFullYear() + '-' + month + '-' + date;
        resultData.in_hand = inHandDate;
        console.log(resultData.in_hand);
        //this.setState({ viagogoInHandDate: inHandDate });
      } */

      resultData && resultData.disclosureids && this.state.getDisclosures && this.state.getDisclosures.length > 0
        && this.state.getDisclosures.map((item, i) => {
          if (resultData.disclosureids.includes(parseInt(item.id))) {
            var combined = { label: item.disclosure_name, value: item.id }
            selectedViagogoDisclosures.push(combined);
          }
        })

      resultData && resultData.tagids && this.state.getTags && this.state.getTags.length > 0
        && this.state.getTags.map((item, i) => {
          if (resultData.tagids.includes(parseInt(item.id))) {
            var combined = { label: item.tag_name, value: item.id }
            selectedViagogoTags.push(combined);
          }
        })

      await this.setState({ viagogoListingGroupData: resultData, selectedViagogoDisclosures: selectedViagogoDisclosures, selectedViagogoTags: selectedViagogoTags, showViagogoModal: true });
    }
    else {
      let checkedObj = this.state.listingGroupIdsObject;
      checkedObj.listing_id.push(listingGroupId)
      this.setState({
        markAsSoldConfirmationModal: true,
        modalListingGroupId: listingGroupId,
        listingGroupIdsObject: checkedObj,
        myModalTitle: 'Are you sure want to update this listing group as SOLD ? ',
        myModalCancelBtn: 'Cancel',
        myModalSubmitBtn: 'Update'
      });
    }
  }

  //Right Click Event - Mark As Sold - Update
  async updateMarkAsSold(bool) {
    let userId = this.context.user.id;
    if (bool) {
      if (this.state.listingStatusViagogoHold) {
        var createListingresponse = await createViagogoBrokerOrder(this.state.viagogoFormFieldsData);

        let userFilterListingsData = await getUserFilterListings(userId, this.state.searchData);
        var eventCountDetail = await getEventCount(userId, this.state.searchData);
        var eventCount = 0;
        if (eventCountDetail && eventCountDetail.countInfo) {
          eventCount = parseInt(eventCountDetail.countInfo.count)
        }
        this.setState({ showViagogoModal: false, markAsSoldConfirmationModal: false, showCommonInfoModal: true, commonInfoModalMessage: createListingresponse["message"], modalListingGroupId: 0, filterListingsData: userFilterListingsData, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] }, eventCount });
      }
      else {
        if (this.state.modalListingGroupId > 0) {
          let resData = await updateListingGroupMarkAsSold(this.state.listingGroupIdsObject.listing_id);
          if (!resData.isError) {
            let userFilterListingsData = await getUserFilterListings(userId, this.state.searchData);
            var eventCountDetail = await getEventCount(userId, this.state.searchData);
            var eventCount = 0;
            if (eventCountDetail && eventCountDetail.countInfo) {
              eventCount = parseInt(eventCountDetail.countInfo.count)
            }
            this.setState({ markAsSoldConfirmationModal: false, modalListingGroupId: 0, filterListingsData: userFilterListingsData, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] }, eventCount });
          }
        }
        else {
          this.setState({ markAsSoldConfirmationModal: false, modalListingGroupId: 0, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } });
        }
      }
    } else {
      this.setState({ markAsSoldConfirmationModal: false, modalListingGroupId: 0, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } });
    }
  }

  onPageChanged = async (data) => {

    const { currentPage, totalPages, pageLimit } = data;
    let userId = this.context.user.id;
    let assignData = this.state.searchData;
    assignData.currentPage = currentPage;
    assignData.pageLimit = this.state.pageLimit;
    let searchedData = await getUserFilterListings(userId, assignData);
    var eventCountDetail = await getEventCount(userId, assignData);
    var eventCount = 0;
    if (eventCountDetail && eventCountDetail.countInfo) {
      eventCount = parseInt(eventCountDetail.countInfo.count)
    }

    this.setState({ filterListingsData: searchedData, eventCount });

    this.setState({ currentPage, pageLimit, totalPages });
  }

  submitInHandDate = async (e) => {

    if(this.state.updateInhandDate){
      await this.setState({ inhandShowConfModal: true, inHandDateConfirmationModal: false });
    }else{
      var tempValidationState = this.state.validation;
      tempValidationState.inHandDate = true;
      this.setState({ validation: tempValidationState});
    }
  }

  //Sort model start
  async sortModal(bool) {

    if (bool) {
      this.setState({ sortModal: true });
    } else {
      this.setState({ sortModal: false });
    }

  }
  //Sort model end

  async handleSortModal(bool, eventId) {
    let selectedArrow = '';
    let enableFilterSubmit = false;
    if (this.state.eventLevelFilter && this.state.eventLevelFilter.length > 0) {
      this.state.eventLevelFilter.map((row) => {
        if (eventId == row.eventId) {
          selectedArrow = row.sort;
          enableFilterSubmit = true;
        }
      })
    }
    if (bool) {
      await this.setState({ sortModal: true, filterEventId: eventId, selectedArrow: selectedArrow, enableFilterSubmit: enableFilterSubmit });
    } else {
      await this.setState({ sortModal: false, enableFilterSubmit: enableFilterSubmit, selectedArrow: selectedArrow });
    }

  }

  async handleCheckedElement(event) {
    const eventLevelFilterArray = this.state.eventLevelFilter;
    let filterListingElement = [
      { value: filterListingData.urgent, isChecked: false },
      { value: filterListingData.sold, isChecked: false },
      { value: filterListingData.available, isChecked: false },
      { value: filterListingData.inactive, isChecked: false }
    ];
    filterListingElement.forEach(list => {
      list.isChecked = false;
      if (list.value === event.target.value) { list.isChecked = true }
    })
    let sortings = '';
    let checkEventStatus = false;
    let newOrder = [];
    if (eventLevelFilterArray && eventLevelFilterArray.length) {
      eventLevelFilterArray.map((row) => {
        newOrder.push(row.eventId)
      });
    }
    if (newOrder.includes(this.state.filterEventId)) {
      let eventIdIndex = newOrder.indexOf(this.state.filterEventId)
      let rowValues = eventLevelFilterArray[eventIdIndex];
      rowValues.status = filterListingElement;
      eventLevelFilterArray[eventIdIndex] = rowValues;
    }
    else {
      eventLevelFilterArray.push({ eventId: this.state.filterEventId, status: filterListingElement, sort: sortings });
    }

    await this.setState({
      filterListingElement: filterListingElement, enableFilterSubmit: true, eventLevelFilter: eventLevelFilterArray, selectFlaggedListing: false, selectViagogoListing: false
    });
  }

  eventLevelFilterCall(listing, index) {
    let radioChecked = false;
    this.state.eventLevelFilter && this.state.eventLevelFilter.length > 0 && this.state.eventLevelFilter.map((row) => {
      if (this.state.filterEventId == row.eventId) {
        row.status && row.status.length > 0 && row.status.map((childRadio, childIndex) => {
          if (listing.value == childRadio.value) {
            radioChecked = childRadio.isChecked
          }
        })
      }
    })
    return (
      <>
        <input key={index} type="radio" onChange={(e) => this.handleCheckedElement(e)} className="form-check-input" value={listing.value} checked={radioChecked} id={listing.value} />
        <label className="form-check-label" htmlFor={listing.value} >{listing.value}</label>
      </>
    )
  }
  async setSortingElement(toggleKey) {
    const eventLevelFilterArray = this.state.eventLevelFilter;
    let checkEventStatus = false;
    let checkEventSortings = [];
    let filterListingElement = [
      { value: filterListingData.urgent, isChecked: false },
      { value: filterListingData.sold, isChecked: false },
      { value: filterListingData.available, isChecked: false },
      { value: filterListingData.inactive, isChecked: false }
    ];
    let newOrder = [];
    if (eventLevelFilterArray && eventLevelFilterArray.length) {
      eventLevelFilterArray.map((row) => {
        newOrder.push(row.eventId)
      });
    }
    if (newOrder.includes(this.state.filterEventId)) {
      let eventIdIndex = newOrder.indexOf(this.state.filterEventId)
      let sortingValues = eventLevelFilterArray[eventIdIndex];
      sortingValues.sort = toggleKey;
      eventLevelFilterArray[eventIdIndex] = sortingValues;
    }
    else {
      eventLevelFilterArray.push({ eventId: this.state.filterEventId, status: filterListingElement, sort: toggleKey });
    }
    const stateCopy = this.state.sortListingType;
    const nextValue = true;
    Object.keys(stateCopy).forEach(key => stateCopy[key] = false);
    stateCopy[toggleKey] = nextValue;
    await this.setState({ sortListingType: stateCopy, enableFilterSubmit: true, eventLevelFilterArray: eventLevelFilterArray, selectedArrow: '' });
  }

  async resetSorting(e) {
    let eventLevelFilter = this.state.eventLevelFilter;
    eventLevelFilter && eventLevelFilter.length && eventLevelFilter.map((row, indexNumber) => {
      if (row.eventId == this.state.filterEventId) {
        eventLevelFilter.splice(indexNumber, 1);
      }
    })
    await this.setState({ selectAllListing: false, selectFlaggedListing: false, selectViagogoListing: false, eventLevelFilter: eventLevelFilter, selectedArrow: '', enableFilterSubmit: false });
    let filterEventId = this.state.filterEventId;
    var rowDetail = this.state.filterListingsData.filter((row) => (row.event_id == filterEventId))
    var eventDetails = rowDetail[0];
    var listingDetails = eventDetails.listingDtls;
    var filteredListingDetails = eventDetails.listingDtls;
    let resetSortingState = {
      enableFilterSubmit: false,
      sortListingType: {
        sortListingDataBySectionInAscending: false,
        sortListingDataBySectionInDescending: false,
        sortListingDataByRowInAscending: false,
        sortListingDataByRowInDescending: false,
        sortListingDataByPriceInAscending: false,
        sortListingDataByPriceInDescending: false
      },
      filterListingElement: [
        { value: filterListingData.urgent, isChecked: false },
        { value: filterListingData.sold, isChecked: false },
        { value: filterListingData.available, isChecked: false },
        { value: filterListingData.inactive, isChecked: false }
      ],

    }
    resetSortingState.filteredListingDetails = {};
    if (filteredListingDetails) {
      resetSortingState.filteredListingDetails = filteredListingDetails.sort((a, b) => (a.listingGroupId > b.listingGroupId ? 1 : -1));
    }
    let filterListingElement = this.state.filterListingElement;
    if (this.state.filterListingsData && this.state.filterListingsData.length > 0) {
      var rowDetail = this.state.filterListingsData.filter((row) => (row.event_id == filterEventId));
      var filteredListingDetails = [];
      if (rowDetail && rowDetail.length) {
        this.state.filterListingsData.map((row) => {
          if (row.event_id == filterEventId) {
            row.filteredListingDetails = resetSortingState.filteredListingDetails
          }
        })
        await this.setState({ filterListingsData: [...this.state.filterListingsData], sortModal: false })
        this.setState({ ...filterListingElement });
      }
      if (this.state.selectAllListing === true) {
        await this.selectAllListingData();
      }
      if (this.state.selectFlaggedListing === true) {
        await this.selectFlaggedListingData();
      }
      if (this.state.selectViagogoListing === true) {
        await this.selectViagogoListingData();
      }

      if (this.state.selectAllListing === false) {
        await this.setState({ listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } })
      }
      await this.setState({ sortModal: false });
    }
    await this.setState(resetSortingState);
  }

  async filterAndSortListingElement(e) {
    let filterEventId = this.state.filterEventId;
    let filterListingElement = this.state.filterListingElement;

    if (this.state.filterListingsData && this.state.filterListingsData.length > 0) {
      var rowDetail = this.state.filterListingsData.filter((row) => (row.event_id == filterEventId))
      var eventLevelFilterDetail = this.state.eventLevelFilter.filter((row) => (row.eventId == filterEventId))
      var eventDetails = rowDetail[0];
      var listingDetails = eventDetails.listingDtls;
      var filteredListingDetails = [];

      var selectedFilterData = filterListingElement.filter(list => list.isChecked === true)

      if (selectedFilterData && selectedFilterData.length && rowDetail.length) {

        if (selectedFilterData.find(list => list.value == filterListingData.sold)) {
          if (listingDetails && listingDetails.length > 0) {
            filteredListingDetails = listingDetails.filter((res) => (res.availabletkts == 0));
          }
        }
        if (selectedFilterData.find(list => list.value == filterListingData.available)) {
          if (listingDetails && listingDetails.length > 0) {
            filteredListingDetails = listingDetails.filter((res) => (res.availabletkts > 0));
          }
        }
        if (selectedFilterData.find(list => list.value == filterListingData.inactive)) {
          if (filteredListingDetails && filteredListingDetails.length > 0) {
            filteredListingDetails = this.filterInactiveListing(filteredListingDetails);
          } else {
            filteredListingDetails = this.filterInactiveListing(listingDetails);
          }
        }

        // filterListingElement = filterListingElement.forEach(list => {
        //   list.isChecked = false
        // })
        this.state.filterListingsData.map((row) => {
          if (row.event_id == filterEventId) {
            row.filteredListingDetails = filteredListingDetails
          }
        })
        await this.setState({ filterListingsData: [...this.state.filterListingsData], sortModal: false })
        this.setState({ ...filterListingElement });
      }


      if (this.state.sortListingType.sortListingDataBySectionInAscending === true) {
        this.sortListingDataBasedOnTypeInAscendingOrder(filterListingData.section);
      }

      if (this.state.sortListingType.sortListingDataBySectionInDescending === true) {
        this.sortListingDataBasedOnTypeInDescendingOrder(filterListingData.section);
      }

      if (this.state.sortListingType.sortListingDataByRowInAscending === true) {
        this.sortListingDataBasedOnTypeInAscendingOrder(filterListingData.row);
      }

      if (this.state.sortListingType.sortListingDataByRowInDescending === true) {
        this.sortListingDataBasedOnTypeInDescendingOrder(filterListingData.row);
      }

      if (this.state.sortListingType.sortListingDataByPriceInAscending === true) {
        this.sortListingDataBasedOnTypeInAscendingOrder(filterListingData.price);
      }

      if (this.state.sortListingType.sortListingDataByPriceInDescending === true) {
        this.sortListingDataBasedOnTypeInDescendingOrder(filterListingData.price);
      }

      if (this.state.selectAllListing === true) {
        await this.selectAllListingData();
      }

      if (this.state.selectAllListing === false) {
        this.setState({ listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } })
      }

      if (this.state.selectFlaggedListing === true) {
        await this.selectFlaggedListingData();
      }

      if (this.state.selectViagogoListing === true) {
        await this.selectViagogoListingData();
      }

      this.setState({ sortModal: false, selectAllListing: false });

    }
    else {
      this.setState({ sortModal: false });
    }
  }

  filterInactiveListing(listingDetails) {
    if (listingDetails && listingDetails.length > 0) {
      return listingDetails.filter((res) => (res.listing_inactive === true));
    }
    else return [];
  }

  sortListingDataBasedOnTypeInAscendingOrder(type) {
    let filterEventId = this.state.filterEventId;
    var rowDetail = this.state.filterListingsData.filter((row) => (row.event_id == filterEventId))
    var eventDetails = rowDetail[0];
    var listingDetails = eventDetails.listingDtls;
    var filteredListingDetails = eventDetails.filteredListingDetails;


    if (filteredListingDetails && filteredListingDetails.length > 0) {

      var isInteger = filteredListingDetails.every(list => (isNaN(list[type]) === false))

      if (isInteger) {
        filteredListingDetails = filteredListingDetails.sort(function (list, row) { return Number(list[type]) - Number(row[type]) });
      } else {
        filteredListingDetails = this.sortListingInAscendingOrder(filteredListingDetails, type);
      }
    }
    else {
      if (listingDetails && listingDetails.length > 0) {
        var isInteger = listingDetails.every(list => (isNaN(list[type]) === false))

        if (isInteger) {
          filteredListingDetails = listingDetails.sort(function (list, row) { return Number(list[type]) - Number(row[type]) });
        } else {
          filteredListingDetails = this.sortListingInAscendingOrder(listingDetails, type);
        }
      }
    }

    this.changeStateDataAfterSorting(filterEventId, filteredListingDetails);
    this.setState({ filterListingsData: [...this.state.filterListingsData] });

  }

  sortListingDataBasedOnTypeInDescendingOrder(type) {
    let filterEventId = this.state.filterEventId;
    var rowDetail = this.state.filterListingsData.filter((row) => (row.event_id == filterEventId))
    var eventDetails = rowDetail[0];
    var listingDetails = eventDetails.listingDtls;
    var filteredListingDetails = eventDetails.filteredListingDetails;

    if (filteredListingDetails && filteredListingDetails.length > 0) {
      var isInteger = filteredListingDetails.every(list => (isNaN(list[type]) === false))

      if (isInteger) {
        filteredListingDetails = filteredListingDetails.sort(function (list, row) { return Number(row[type]) - Number(list[type]) });
      } else {
        filteredListingDetails = this.sortListingInDescendingOrder(filteredListingDetails, type);
      }
    }
    else {
      var isInteger = listingDetails.every(list => (isNaN(list[type]) === false))

      if (isInteger) {
        filteredListingDetails = listingDetails.sort(function (list, row) { return Number(row[type]) - Number(list[type]) });
      } else {
        if (listingDetails && listingDetails.length > 0) {
          filteredListingDetails = this.sortListingInDescendingOrder(listingDetails, type)
        }
      }
    }
    this.changeStateDataAfterSorting(filterEventId, filteredListingDetails);
    this.setState({ filterListingsData: [...this.state.filterListingsData] });
  }

  selectAllListingData() {
    let filterEventId = this.state.filterEventId;
    var rowDetail = this.state.filterListingsData.filter((row) => (row.event_id == filterEventId))
    var eventDetails = rowDetail[0];
    var listingDetails = eventDetails.listingDtls;
    var filteredListingDetails = eventDetails.filteredListingDetails;
    let checkedRows = this.state.listingGroupIdsObject;
    checkedRows.listing_id = [];

    if (filteredListingDetails && filteredListingDetails.length > 0) {
      filteredListingDetails.map(listing => {
        checkedRows.listing_id.push(listing.listingGroupId)
      })
    } else {
      if (listingDetails && listingDetails.length) {
        listingDetails.map(listing => {
          checkedRows.listing_id.push(listing.listingGroupId)
        })
      }
    }
    checkedRows.event_id = filterEventId;

    this.setState({ listingGroupIdsObject: checkedRows, listingGroupIds: checkedRows.listing_id })
  }

  // Flag function Start
  async selectFlaggedListingData() {
    let filterEventId = this.state.filterEventId;
    if (this.state.filterListingsData && this.state.filterListingsData.length > 0) {
      var rowDetail = this.state.filterListingsData.filter((row) => (row.event_id == filterEventId))
      var eventLevelFilterDetail = this.state.eventLevelFilter.filter((row) => (row.eventId == filterEventId))
      var eventDetails = rowDetail[0];
      var listingDetails = eventDetails.listingDtls;
      var filteredListingDetails = [];
      if (listingDetails && listingDetails.length > 0) {
        if (this.state.selectFlaggedListing) {
          filteredListingDetails = await listingDetails.filter((res) => (res.is_flagged == true));
        }
      }
      this.state.filterListingsData.map((row) => {
        if (row.event_id == filterEventId) {
          row.filteredListingDetails = filteredListingDetails
        }
      })
      await this.setState({ filterListingsData: [...this.state.filterListingsData], sortModal: false })
      await this.setState({ sortModal: false });
    }
  }
  // Flag function End

// Viagogo function Start
async selectViagogoListingData() {
  let filterEventId = this.state.filterEventId;
  if (this.state.filterListingsData && this.state.filterListingsData.length > 0) {
    var rowDetail = this.state.filterListingsData.filter((row) => (row.event_id == filterEventId))
    var eventLevelFilterDetail = this.state.eventLevelFilter.filter((row) => (row.eventId == filterEventId))
    var eventDetails = rowDetail[0];
    var listingDetails = eventDetails.listingDtls;
    var filteredListingDetails = [];
    if (listingDetails && listingDetails.length > 0) {
      if(this.state.selectViagogoListing){
        filteredListingDetails = await listingDetails.filter((res) => (res.listing_status.status == "viagogo_hold"));
      }
    }
    this.state.filterListingsData.map((row) => {
      if (row.event_id == filterEventId) {
        row.filteredListingDetails = filteredListingDetails
      }
    })
    await this.setState({ filterListingsData: [...this.state.filterListingsData], sortModal: false })
    await this.setState({ sortModal: false});
  }
}
// Viagogo function end

  sortListingInDescendingOrder(listingDetails, type) {
    return listingDetails.sort(function (list, data) {
      if (data[type] < list[type]) { return -1; }
      if (data[type] > list[type]) { return 1; }
      return 0;
    })
  }

  sortListingInAscendingOrder(listingDetails, type) {
    return listingDetails.sort(function (list, data) {
      if (list[type] < data[type]) { return -1; }
      if (list[type] > data[type]) { return 1; }
      return 0;
    })
  }

  changeStateDataAfterSorting(filterEventId, filteredListingDetails) {
    this.state.filterListingsData.map((row) => {
      if (row.event_id == filterEventId) {
        row.filteredListingDetails = filteredListingDetails
      }
    })

    const stateCopy = this.state.sortListingType;
    Object.keys(stateCopy).forEach(key => stateCopy[key] = false);
    this.setState({ sortModal: false, sortListingType: stateCopy, selectAllListing: false })
  }

  //channelManagmentIncressOnchange
  async channelManagmentIncressOnchange() {
    let updatePrice = this.state.eventLevelUpdatePrice ? parseInt(this.state.eventLevelUpdatePrice) : 0;
    let validation = this.state.validation;
    validation.price = false; validation.updatePrice = false;
    this.setState({ eventLevelUpdatePrice: updatePrice + 1, eventLevelPrice: '', validation: validation })

  }

  //channelManagmentDecressOnchange
  async channelManagmentDecressOnchange() {
    let updatePrice = this.state.eventLevelUpdatePrice ? parseInt(this.state.eventLevelUpdatePrice) : 0;
    let validation = this.state.validation;
    validation.price = false; validation.updatePrice = false;

    this.setState({ eventLevelUpdatePrice: updatePrice - 1, eventLevelPrice: '', validation: validation })

  }

  async updatePriceToggleOnchange(e) {
    let toggleValue = '';
    if (e) {
      toggleValue = '%';
    }
    else {
      toggleValue = '$';
    }
    this.setState({ eventLevelPrice: '', priceUpdateType: toggleValue });
  }

  async viagogoStatus(listGroupId, status) {
    let updatedPrice = await updateViagogoStatus(listGroupId, status);
    let userId = this.context.user.id;
    let userFilterListingsData = await getUserFilterListings(userId, this.state.searchData);
    var eventCountDetail = await getEventCount(userId, this.state.searchData);
    var eventCount = 0;
    if (eventCountDetail && eventCountDetail.countInfo) {
      eventCount = parseInt(eventCountDetail.countInfo.count)
    }
    await this.setState({ modalEventId: 0, filterListingsData: userFilterListingsData, eventCount, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } });
  }

  async flagStatus(listGroupId, status, eventId) {
    if(status && !this.state.isFlagged && !this.state.isMultipleFlag ){
      if( this.state.listingGroupIds && this.state.listingGroupIds.length){
        await this.changeFlag( this.state.listingGroupIds && this.state.listingGroupIds.length > 0 ?  this.state.listingGroupIds : [this.state.listIdRC] , this.state.selectedListingEventId, 1);
      }else{
        await this.changeFlag( listGroupId , eventId, 1);
      }
    } else{  
      if(!this.state.listingGroupIds.length || (this.state.listingGroupIds.length === 1 && this.state.listingGroupIds[0] === parseInt(listGroupId)) ){
        await updateFlagStatus(listGroupId, status);
        let userId = this.context.user.id;
        let userFilterListingsData = await getUserFilterListings(userId, this.state.searchData);
        var eventCountDetail = await getEventCount(userId, this.state.searchData);
        var eventCount = 0;
        if (eventCountDetail && eventCountDetail.countInfo) {
          eventCount = parseInt(eventCountDetail.countInfo.count)
        }
        await this.setState({ modalEventId: 0, filterListingsData: userFilterListingsData, eventCount, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } });
      }
    }
  }

  async issueFlagModal(e, field) {
    let issueState = this.state.issueModalValues;
    if (field == 'issueNotes') {
      issueState.issueNotes = e.target.value;
    }
    else {
      issueState[field] = e;
    }
    await this.setState({ issueModalValues: issueState });
  }
  async issueFlagConfirm(formValue) {
    let flagRows = 1;
    let issueValues = {
      assignee: formValue.assign.value,
      issueTag: formValue.issueTag.value,
      issue_type_id: formValue.issueType,
      issueNotes: formValue.issueNotes,
      assigner: parseInt(this.context.user.id)
    }
    await this.setState({
      showIssueFlagConfirm: true, createIssueModal: false, issueValues: issueValues, rowIds: flagRows
    });
  }
  async changeFlag(id, event_id, status) {

    let selectedRows = this.state.listingGroupIds;
    if (!this.state.listingGroupIds || this.state.listingGroupIds.length < 1) {
      selectedRows = id;
    }
    await this.setState({
      createIssueModal: true, showIssueFlagConfirm: false, listingGroupIds: selectedRows, flagIconStatus: status, issueModalValues: { assign: [], issueType: [], issueTag: [], issueNotes: '' }, listingGroupIdsObject: { event_id: event_id, listing_id: selectedRows }
    })
  }

  async createNewIssue() {
    let createIssueflag = await createIssueFlag(this.state.listingGroupIds, this.state.issueValues, this.state.selectedListingEventId);
    let flagIssueResponse = this.state.flagIssueResponse;
    if (createIssueflag && !createIssueflag.isError) {
      let userId = this.context.user.id;
      let userFilterListingsData = await getUserFilterListings(userId, this.state.searchData);
      var eventCountDetail = await getEventCount(userId, this.state.searchData);
      var eventCount = 0;
      if (eventCountDetail && eventCountDetail.countInfo) {
        eventCount = parseInt(eventCountDetail.countInfo.count)
      }
      flagIssueResponse.title = 'New Issue Submitted';
      flagIssueResponse.msg = 'The Assignee has been notified. This listing as been marked as QC';
      this.setState({
        showIssueFlagSuccess: true, flagIssueResponse: flagIssueResponse, showIssueFlagConfirm: false,
        filterListingsData: userFilterListingsData, eventCount
      })
    }
    else {
      flagIssueResponse.title = 'Issue Submission Failed ';
      flagIssueResponse.msg = 'Listing submission failed to create';
      this.setState({ showIssueFlagSuccess: true, flagIssueResponse: flagIssueResponse, showIssueFlagConfirm: false })
    }
  }
  renderListingDiv(days, monthNames, event_id, listingDetails) {
    return (
      listingDetails && listingDetails.length > 0 ? listingDetails.map(res => (<tr className={`listingContextMenu rightClick-Menu ${res.sold.length == 1 && res.sold[0] == listingConst.yesValue ? 'listing-sold' : (res.listing_status && res.listing_status.status === allListingStatus.viagogo_hold ? 'listing-viagogo-hold' : (res.listing_status && res.listing_status.status === allListingStatus.inactive ? 'listing-inactive' : 'listing-active'))}
    `}

        onMouseDown={(e) => this.contextMenuEvent(e, res)} key={'listingContextMenu_' + res.listingGroupId} >
        <td className="listing-checkbox"><input value={res.listingGroupId} className="form-control" type="checkbox" onChange={(e) => this.handleListingGroupChange(e, event_id, res.listingGroupId, [res.listing_inactive], res.listing_status.status, res.sold, res.is_flagged)}
          checked={this.state.listingGroupIdsObject.listing_id && this.state.listingGroupIdsObject.listing_id.includes(res.listingGroupId) ? 'checked' : ''}
        /></td>
        {res.seat_type_id != 2 ?
          <>
            <td className="rightClick-Menu" >
              <h3 className="seat-ava rightClick-Menu">{res.section}</h3>
            </td>
            <td className="rightClick-Menu">
              <h3 className="seat-ava rightClick-Menu">{res.row}</h3>
            </td>
            <td className="rightClick-Menu">
              <h3 className="seat-ava rightClick-Menu">{res.seatdtl}</h3>
            </td>
          </> :
          <td className="rightClick-Menu" colSpan="3" >
            <h3 className="seat-ava rightClick-Menu">{seatTypeNames[1]}</h3>
          </td>
        }
        <td className="price-font price-txt">
          <label type="label" onMouseOver={(e) => this.showTooltip(res.listingGroupId, true, res.last_price_change)}
            onMouseLeave={(e) => this.showTooltip(res.listingGroupId, false, false)}
            onClick={(e) => this.setState({ toolTipId: 0 })}
            onDoubleClick={
              (e) => this.convertToInput(
                'dynamicUpdate_' + res.listingGroupId,
                res.listingGroupId,
                'price', res.price ? res.price.toFixed(2) : '0.00', event_id)}
          >
            {this.state.dynamicUpdate['dynamicUpdate_' + res.listingGroupId + 'price'] && res.sold[0] == 'no' ?
              <div className="edit-price-item">
                <input type="number" autoFocus onChange={(e) => this.updateColumn(e, 'dynamicUpdate_' + res.listingGroupId, 'price')} onKeyDown={(e) => this.avoidEvalue(e, 'dynamicUpdate_' + res.listingGroupId + 'price', res.listingGroupId, 'price', event_id)} className={'form-control custom-price-input'} value={this.state.dynamicUpdate['dynamicUpdate_' + res.listingGroupId + '_price']}
                  onBlur={(e) => this.convertToLabel('dynamicUpdate_' + res.listingGroupId + 'price', res.listingGroupId, 'price', event_id)}
                />
                <div className="edit-save-sec">
                  <button type="" className="btn edit-btn" onClick={(e) => this.state.dynamicUpdate.listValue ? this.setState({ priceUpdateConfirmationModal: true }) : ''
                  }><TickIcon /></button>
                </div>
              </div>
              :
              (this.state.dynamicUpdate['dynamicUpdate_' + res.listingGroupId + '_price'] ? '$' + this.state.dynamicUpdate['dynamicUpdate_' + res.listingGroupId + '_price'] : (res.price && res.price > 0) ? '$' + res.price.toFixed(2) : '$' + 0.00)}
          </label>
          <div className="price-update-box" style={{ display: this.state.toolTipId == res.listingGroupId ? "block" : "none" }}>
            <div className="price-card-box">
              <h6 className="price-update-text">Price Updated: <span>


                {res.last_price_change && res.last_price_change.length ? moment.utc(res.last_price_change[0]).local().format('MM/DD/YYYY @ hh:mm a ') + ' ' + (moment && moment.tz.guess() && moment.tz.guess().length && moment.tz(moment.tz.guess()).zoneAbbr().length ? moment.tz(moment.tz.guess()).zoneAbbr() : '') : ''}


              </span></h6>
              <div className="change-by-text">
                <h5>By User: <span>{res.price_done_by ? res.price_done_by : 'No update'}</span></h5>
              </div>
              <div className={`price-change-text `}>
                <h5 >From $ {res.last_price ? res.last_price.toFixed(2) : 'No update'} to <span className={`${res.price < res.last_price ? "negative" : "positive"}`}>$ {res.price ? res.price.toFixed(2) : 'No update'}</span></h5>
              </div>
            </div>
          </div>
        </td>
        <td className="semi-bold" title={res.last_price_change[0] ? (days[(new Date(res.last_price_change[0])).getDay()] + '- ' + monthNames[(new Date(res.last_price_change[0])).getMonth()] + ' ' + (new Date(res.last_price_change[0])).getDate() + ', ' + (new Date(res.last_price_change[0])).getFullYear() + ' ' + ((new Date(res.last_price_change[0])).getHours() < 10 ? '0' + (new Date(res.last_price_change[0])).getHours() : (new Date(res.last_price_change[0])).getHours()) + ':' + (new Date(res.last_price_change[0])).getMinutes()) : 'No Update'

        }>${(res.last_price && res.last_price > 0) ? res.last_price.toFixed(2) : 0}

        </td>
        <td className="semi-bold">${(res.face_value && res.face_value > 0) ? res.face_value : 0}</td>
        <td className="semi-bold">${res.cost && !isNaN(res.cost) ? res.cost.toFixed(2) : '0.00'}</td>
        <td className={res.margin > 0 ? "margin-color-green" : (res.margin < 0 ? "margin-color-red" : "margin-color")}>{res.margin}%</td>
        <td>
          {res.availabletkts == 0 ? <h3 className="sold-unsold-txt">Sold Out</h3> : ''}
          {res.availabletkts != 0 && (res.inactive[0] == true || res.listing_status.status == allListingStatus.inactive) ? <h3 className="inactive-text">Inactive</h3> : ''}

          <h3 className="finish-quantity cost-font">{res.availabletkts} / {res.soldtkts}</h3>
        </td>
        <td className="semi-bold e-ticket-txt">{res.tickettypename ? res.tickettypename : ''}</td>
        <td>
          <div className="icon-list-Table">
            <ul className="icon-Unlist">
              <li className="listOfIcon" title="Disclosure" onClick={() => this.disclosuresListing(res.listingGroupId, 'listing', 0, true)}>
                <img src={iconDisclosures} />
              </li>
              <li className="listOfIcon" title="Splits" onClick={() => this.splitsListing(res.listingGroupId, 'listing', 0, true)}>
                <img src={iconSplits} />
              </li>
              <li className="listOfIcon" title="PO">
                {
                  res.purchaseid && res.purchaseid[0] != null ?
                    <Link to={'/purchase/purchase-detail/' + res.purchaseid[0]} target="_blank"><img src={iconPurchaseOrder} /></Link> :
                    <img src={iconPurchaseOrder} />
                }
              </li>
              <li className="listOfIcon" title="Internal Notes" onClick={() => this.internalNotesListing(res.listingGroupId, 'listing', 0)}>
                <img src={iconNotes} />
              </li>
              {res.sold.length == 1 && res.sold[0] == listingConst.yesValue ? '' :
                res.listing_status && res.listing_status.status === allListingStatus.viagogo_hold ? (
                  <li className="listOfIcon" title="Viagogo Hold" onClick={(e) => this.viagogoStatus([res.listingGroupId], 2)}>
                    <img src={iconViagogoHold} />
                  </li>) :
                  (res.listing_status.status ?
                    <li className="listOfIcon" title="Viagogo active" onClick={(e) => this.viagogoStatus([res.listingGroupId], 5)}  >
                      <img src={iconViagogoActive} />
                    </li> : <li className="listOfIcon" title="Viagogo"  >
                      <img src={iconViagogoActive} />
                    </li>)
              }
              {res.listing_status && res.listing_status.status === allListingStatus.sold && (
                <li className="listOfIcon ctxtm-disabled" title="Share" disabled={true} >
                  <img src={iconShare} />
                </li>
              )}
              {res.listing_status && res.listing_status.status === allListingStatus.active && (
                <li className="listOfIcon" title="Pause Share" onClick={() => this.shareListing(res.listingGroupId, 'pauseShare', event_id, 'listing', true)}>
                  <img src={iconUnShare} />
                </li>
              )}
              {res.listing_status && res.listing_status.status === allListingStatus.inactive && (
                <li className="listOfIcon" title="Share" onClick={() => this.shareListing(res.listingGroupId, 'share', event_id, 'listing', true)}>
                  <img src={iconShare} />
                </li>
              )}
              {/* {res.inactive[0] ?
                  (
                    <li className="listOfIcon" title="Share" onClick={() => this.shareListing(res.listingGroupId, 'share', event_id, 'listing', true)}>
                      <img src={iconShare} />
                    </li>
                  ) :
                  (
                    <li className="listOfIcon" title="Pause Share" onClick={() => this.shareListing(res.listingGroupId, 'pauseShare', event_id, 'listing', true)}>
                      <img src={iconUnShare} />
                    </li>
                  )
                } */}
              {/* <li className="listOfIcon" title="Flag On">
                  <img src={iconFlagOn} onClick={(e) => this.changeFlag(res.listingGroupId, 2)}/>
                </li> */}


                {res.is_flagged ? (
                  <li className="listOfIcon" title="Flag On" onClick={(e) => this.flagStatus([res.listingGroupId], false, event_id )}>
                    <img src={iconFlagOn} />
                  </li>) :
                  (
                    <li className="listOfIcon" title="Flag Off" onClick={(e) => this.flagStatus([res.listingGroupId], true, event_id )}>
                      <img src={iconFlagOff} />
                    </li>)
                }

              {/* {res.sold.length == 1 && res.sold[0] == listingConst.yesValue ? '' :
                  <li className="listOfIcon" title="Flag Off" onClick={(e) => this.changeFlag([res.listingGroupId], event_id, 1)} >
                    <img src={iconFlagOff} />
                  </li>
                } */}
            </ul>
          </div>
        </td>
        <td>
          <div className="tags-item">

            {res.tags && res.tags.length > 0 ? res.tags.map(tag => (
              <div className="tags-btn">{tag}</div>
            )) : ''}
          </div>
        </td>
      </tr>
      )) : (<tr>
        <td colSpan="13">No Data Found.</td>
      </tr>)
    )
  }
  async showTooltip(listing_group_id, status, priceChangedOn) {

    let list_id = 0;
    if (status == true && priceChangedOn && priceChangedOn.length && priceChangedOn[0]) {
      list_id = listing_group_id;
    }
    this.setState({ toolTipId: list_id })
  }

  render() {
    const { suggestions } = this.state

    const week_days = weekDays;
    const ticketTypeDetails = [];
    this.state.ticketTypeDetails && this.state.ticketTypeDetails.map(tickets =>
      ticketTypeDetails.push({ label: tickets.name, value: tickets.id },)
    )
    const genres = [];
    this.state.genreDetails && this.state.genreDetails.map(genre =>
      genres.push({ label: genre.name, value: genre.id },)
    );


    const disclosureValues = [];

    this.state.getDisclosures && this.state.getDisclosures.length > 0
      && this.state.getDisclosures.map((item, i) => {
        var combined = { label: item.disclosure_name, value: item.id, key: item.id.toString() }
        disclosureValues.push(combined);
      })

    const AttrValues = [];
    this.state.getAttributes && this.state.getAttributes.length > 0
      && this.state.getAttributes.map((item, i) => {
        var combined = { label: item.attribute_name, value: item.id, key: item.id.toString() }
        AttrValues.push(combined);
      })
    const tagValues = [];
    this.state.getTags && this.state.getTags.length > 0
      && this.state.getTags.map((item, i) => {
        var combined = { label: item.tag_name, value: item.id }
        tagValues.push(combined);
      })

    const days = daysFull;
    const monthNames = monthNamesShort;
    var addUserVisible = this.state.addUserVisible;
    const { visible } = this.state;
    const { ticket_type_id, disclosureids } = this.state.listDataDetails;

    const allUsersList = [];
    this.state.allUserDetails && this.state.allUserDetails.length > 0
      && this.state.allUserDetails.map((user, i) => {
        var combined = { label: user.email, value: user.id }
        allUsersList.push(combined);
      })

    const allSplitsList = [];
    this.state.splitsList && this.state.splitsList.length > 0
      && this.state.splitsList.map((user, i) => {
        var combined = { label: user.name, value: user.id }
        allSplitsList.push(combined);
      })
    return !this.state.loading ? (

      <div>

        <Modal size="lg" show={addUserVisible} onHide={(e) => this.viewListing(false)} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Listing ID:{this.state.listIdRC} | Days Old: {this.state.listDataDetails.diffDays} </Modal.Title>
          </Modal.Header>
          <Modal.Body>

            <Formik
              initialValues={{
                seller_id: 1,
                buyer_id: 1,
                eventId: this.state.listDataDetails.event_id,
                ticket_type_id: this.state.listDataDetails.ticket_type_id ? this.state.listDataDetails.ticket_type_id : '',
                inhand_date: this.state.listDataDetails.in_hand ? this.state.listDataDetails.in_hand : '',
                split: this.state.listDataDetails.splits ? this.state.listDataDetails.splits : '',
                seat_type_id: this.state.listDataDetails.seat_type_id ? this.state.listDataDetails.seat_type_id : '',
                quantity: this.state.listDataDetails.quantity,
                ticketing_system: this.state.listDataDetails.ticket_system_id ? this.state.listDataDetails.ticket_system_id : '',
                ticketing_system_accounts: this.state.listDataDetails.ticket_system_account ? this.state.listDataDetails.ticket_system_account : '',
                hide_seat_number: this.state.listDataDetails.hide_seat_numbers,
                row: this.state.listDataDetails.row,
                group_cost: this.state.listDataDetails.group_cost,
                unit_cost: this.state.listDataDetails.unit_cost,
                price: this.state.listDataDetails.price,
                face_value: this.state.listDataDetails.face_value,
                seat_start: this.state.listDataDetails.seat_start,
                seat_end: this.state.listDataDetails.seat_end,
                section: this.state.listDataDetails.section,
                po_id: this.state.listDataDetails.PO_ID,
                disclosureids: this.state.selectedDiscolsures,
                attrbtids: this.state.selectedAttributes,
                selectedTags: this.state.selectedTags,
                internal_notes: this.state.listDataDetails.internal_notes,
                external_notes: this.state.listDataDetails.external_notes,

              }}
              enableReinitialize={true}

              validationSchema={Yup.object().shape({

                ticket_type_id: Yup.string()
                  .required('Ticket type is required'),

                inhand_date: Yup.string()
                  .required('Inhand date is required'),

                seat_type_id: Yup.string()
                  .required('Seat type value is required'),

                split: Yup.string()
                  .required('Split value is required'),

                quantity: Yup.string()
                  .required('Quantity value is required'),

                ticketing_system: Yup.string()
                  .required('Ticketing system is required'),

                ticketing_system_accounts: Yup.string()
                  .required('Account is required'),

                hide_seat_number: Yup.string()
                  .required('Hide seat number is required'),

                seat_start: Yup
                  .number()
                  .integer()
                  .required('Seat start is required'),

                seat_end: Yup
                  .number()
                  .integer()
                  .nullable()
                  .min(Yup.ref('seat_start'), 'Seat end must be greater than seat start')
                  .required('Seat end is required'),

              })}


              onSubmit={async fields => {
                fields.list_id = this.state.listIdRC;
                let disclosureids = [];
                let attrbtids = [];
                let selectedTags = [];

                fields && fields.disclosureids && fields.disclosureids.map(
                  (item) => {
                    disclosureids.push(item.value)
                  }
                );
                fields && fields.attrbtids && fields.attrbtids.map(
                  (item) => {
                    attrbtids.push(item.value)
                  }
                );
                fields && fields.selectedTags && fields.selectedTags.map(
                  (item) => {
                    selectedTags.push(item.value)
                  }
                );
                fields.attrbtids = attrbtids;
                fields.disclosureids = disclosureids;
                fields.selectedTags = selectedTags;

                var data = { "seatInformationsJSON": this.state.inputList }
                const listdata = await { ...fields };
                var createListingresponse = await updateListing(listdata, this.state.getChannels);
                if (!createListingresponse.isError) {
                  this.commonAlertModal(true, createListingresponse.msg);
                  this.viewListing(false)
                }
              }}

              render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                <div>
                  <div className="ava-ticket-detail">
                    <div className="check_ticket">
                      <div className="ticket-details-item">
                        <div className="ticket-header">
                          <div className="d-flex align-items-center">
                            <div className="addlisting addlisting-left-header"><h1>{this.state.listDataDetails.event_name}</h1></div>
                          </div>
                        </div>
                        <div className="ava--detail">
                          <div className="addlisting">
                            <h1 className="addlisting-date--time">
                              {days[(new Date(this.state.listDataDetails.eventdate + ' UTC')).getDay()] + '- ' + monthNames[(new Date(this.state.listDataDetails.eventdate + ' UTC')).getUTCMonth()] + ' ' + (new Date(this.state.listDataDetails.eventdate + ' UTC')).getDate() + ', ' + (new Date(this.state.listDataDetails.eventdate + ' UTC')).getFullYear() + ' ' + ((new Date(this.state.listDataDetails.eventdate + ' UTC')).getHours() < 10 ? '0' + (new Date(this.state.listDataDetails.eventdate + ' UTC')).getHours() : (new Date(this.state.listDataDetails.eventdate + ' UTC')).getHours()) + ':' + (new Date(this.state.listDataDetails.eventdate + ' UTC')).getMinutes()
                              }
                            </h1>
                            <h2 className="addlisting-loaction-text">{this.state.listDataDetails.venue_name + ', ' + this.state.listDataDetails.city + ', ' + this.state.listDataDetails.state + ', ' + this.state.listDataDetails.country}</h2>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <Form>

                    <div className="row fliter-bottom m-0">

                      <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                        <label>Ticket Type *</label>
                        <select
                          name="ticket_type_id"
                          className={'formik-input-user-form form-control' + (errors.ticket_type_id && touched.ticket_type_id ? ' is-invalid' : '')}
                          value={values.ticket_type_id}
                          disableSearch={true}
                          onChange={(e) => this.changeHandler(e, 'ticket_type_id')}
                          onBlur={handleBlur}
                        >
                          <option value="">Select Ticket Types *</option>
                          {this.state.ticketTypeDetails && this.state.ticketTypeDetails.length > 0
                            && this.state.ticketTypeDetails.map((item, i) => {
                              return (
                                <option key={i} selected={ticket_type_id == item.id} value={item.id}>{item.name}</option>
                              )
                            })}
                        </select>
                        <ErrorMessage name="ticket_type_id" component="div" className="invalid-feedback" />
                      </div>

                      <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                        <label>Inhand Date *</label>
                        <input
                          className={'formik-input-user-form form-control' + (errors.inhand_date && touched.inhand_date ? ' is-invalid' : '')}
                          type="date"
                          name="inhand_date"
                          value={values.inhand_date}
                          readOnly={this.state.listDataDetails.sold}
                          placeholder="Inhand Date"
                          onChange={this.state.listDataDetails.sold ? this.avoidOnchange : handleChange}
                          onBlur={handleBlur}
                        />
                        <ErrorMessage name="inhand_date" component="div" className="invalid-feedback" />
                      </div>

                      <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                        <label>Split</label>
                        <select
                          className={'formik-input-user-form form-control' + (errors.split && touched.split ? ' is-invalid' : '')}
                          name="split"
                          value={values.split}
                          onChange={(e) => this.changeHandler(e, 'splits')}
                        >
                          <option value="">Select Splits</option>
                          {this.state.splitsList && this.state.splitsList.length > 0
                            && this.state.splitsList.map((item, i) => {
                              return (
                                <option key={i} value={item.id}>{item.name}</option>
                              )
                            })}
                        </select>
                        <Select
                          className={'formik-input-user-form create-select-box' + (errors.issueType && touched.issueType ? ' is-invalid' : '')}
                          placeholder={"Issue Type"}
                          style={{ width: "100%" }}
                          name="issueType"
                          value={this.state.issueModalValues.issueType ? this.state.issueModalValues.issueType : []}
                          options={this.state.issueTypes}
                          onChange={(e) => this.issueFlagModal(e, 'issueType')}
                        />
                        <ErrorMessage name="split" component="div" className="invalid-feedback" />
                      </div>

                      <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                        <label>Seat Type</label>
                        <select
                          className={'formik-input-user-form form-control' + (errors.seat_type_id && touched.seat_type_id ? ' is-invalid' : '')}
                          name="seat_type_id"
                          value={values.seat_type_id}
                          onChange={(e) => this.changeHandler(e, 'seat_type_id')}
                          onBlur={handleBlur}>
                          <option value="">Select Seat Type</option>
                          <option value="1">sequential</option>
                          <option value="2">odd/even</option>
                          <option value="3">piggyback</option>
                          <option value="4">GA</option>
                        </select>
                        <ErrorMessage name="seat_type_id" component="div" className="invalid-feedback" />
                      </div>

                      <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                        <label>Quantity</label>
                        <input
                          className={'formik-input-user-form form-control' + (errors.quantity && touched.quantity ? ' is-invalid' : '')}
                          type="text"
                          name="quantity"
                          placeholder="Quantity"
                          value={values.quantity}
                          onChange={(e) => this.changeHandler(e, 'seat_type_id')}
                          onBlur={handleBlur} />
                        <ErrorMessage name="quantity" component="div" className="invalid-feedback" />
                      </div>

                      <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                        <label>Ticketing System</label>
                        <select
                          name="ticketing_system"
                          className={'formik-input-user-form form-control' + (errors.ticketing_system && touched.ticketing_system ? ' is-invalid' : '')}
                          value={values.ticketing_system}
                          onChange={(e) => this.changeHandler(e, 'ticket_system_id')}
                          onBlur={handleBlur}
                        >
                          <option>Select Ticketing System</option>
                          {this.state.getTicketingSystems && this.state.getTicketingSystems.length > 0
                            && this.state.getTicketingSystems.map((item, i) => {
                              return (
                                <option key={i} value={item.id}>{item.ticketing_system_name}</option>
                              )
                            })}
                        </select>
                        <ErrorMessage name="ticketing_system" component="div" className="invalid-feedback" />
                      </div>

                      <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                        <label>Account</label>
                        <select
                          name="ticketing_system_accounts"
                          className={'formik-input-user-form form-control' + (errors.ticketing_system_accounts && touched.ticketing_system_accounts ? ' is-invalid' : '')}
                          value={values.ticketing_system_accounts}
                          onChange={(e) => this.changeHandler(e, 'ticket_system_account')}
                          onBlur={handleBlur}
                        >
                          <option>Select Accounts</option>
                          {this.state.getTicketingSystemAccounts && this.state.getTicketingSystemAccounts.length > 0
                            && this.state.getTicketingSystemAccounts.map((item, i) => {
                              return (
                                <option key={i} value={item.id}>{item.email}</option>
                              )
                            })}
                        </select>
                        <ErrorMessage name="ticketing_system_accounts" component="div" className="invalid-feedback" />
                      </div>

                      <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                        <label>Hide Seat Number</label>
                        <select
                          name="hide_seat_number"
                          className={'formik-input-user-form form-control' + (errors.hide_seat_number && touched.hide_seat_number ? ' is-invalid' : '')}
                          value={values.hide_seat_number}
                          onChange={(e) => this.changeHandler(e, 'hide_seat_numbers')}
                          onBlur={handleBlur}
                        >
                          <option value="">Select</option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                        <ErrorMessage name="hide_seat_number" component="div" className="invalid-feedback" />
                      </div>

                      {/* append field start */}
                      {this.state.inputList && this.state.inputList.map((x, i) => {
                        return (
                          <div className="row add-listing-append-form-border">

                            <div className="form-group add-listing-form-group  add-listing-form-group-four-width">
                              <label>Section</label>
                              <input
                                type="text"
                                name="section"
                                placeholder="Section"
                                className={'formik-input-user-form form-control' + (errors.section && touched.section ? ' is-invalid' : '')}
                                value={values.section}
                                onChange={(e) => this.changeHandler(e, 'section')}
                                onBlur={handleBlur}
                              />
                              <ErrorMessage name="section" component="div" className="invalid-feedback" />
                            </div>

                            <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                              <label>Row</label>
                              {/* <select
                                className="form-control"
                                name="row"

                                value={values.row}
                                onChange={(e) => this.changeHandler(e, 'row')}

                              >
                                <option value="">Select</option>
                                <option value="1">Yes</option>
                                <option value="0" selected>No</option>
                              </select> */}
                              <input
                                type="text"
                                name="row"
                                placeholder="Row"
                                className={'formik-input-user-form form-control' + (errors.section && touched.section ? ' is-invalid' : '')}
                                value={values.row}
                                onChange={(e) => this.changeHandler(e, 'row')}
                                onBlur={handleBlur}
                              />

                              <ErrorMessage name="row" component="div" className="invalid-feedback" />
                            </div>

                            <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                              <label>Seat Start</label>
                              <input
                                type="number"
                                name="seat_start"
                                className={'formik-input-user-form form-control' + (errors.seat_start && touched.seat_start ? ' is-invalid' : '')}
                                value={values.seat_start}
                                onChange={(e) => this.changeHandler(e, 'seat_start')}
                                onBlur={handleBlur}
                                placeholder="From" />
                              <ErrorMessage name="seat_start" component="div" className="invalid-feedback" />
                            </div>

                            <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                              <label>Seat End</label>
                              <input
                                type="number"
                                name="seat_end"
                                placeholder="To"
                                className={'formik-input-user-form form-control' + (errors.seat_end && touched.seat_end ? ' is-invalid' : '')}
                                value={values.seat_end}
                                onChange={(e) => this.changeHandler(e, 'seat_end')}
                                onBlur={handleBlur}
                              />
                              <ErrorMessage name="seat_end" component="div" className="invalid-feedback" />
                            </div>


                            <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                              <label>Price</label>
                              <input
                                type="number"
                                name="price"
                                placeholder="$"
                                className={'formik-input-user-form form-control' + (errors.price && touched.price ? ' is-invalid' : '')}
                                value={values.price}
                                onChange={(e) => this.changeHandler(e, 'price')}
                                onBlur={handleBlur}
                              />
                              <ErrorMessage name="price" component="div" className="invalid-feedback" />
                            </div>


                            <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                              <label>Face Value</label>
                              <input
                                type="number"
                                name="face_value"
                                placeholder="Face Value"
                                className={'formik-input-user-form form-control' + (errors.face_value && touched.face_value ? ' is-invalid' : '')}
                                value={values.face_value}
                                onChange={(e) => this.changeHandler(e, 'face_value')}
                                onBlur={handleBlur}
                              />
                              <ErrorMessage name="face_value" component="div" className="invalid-feedback" />
                            </div>

                            <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                              <label>Cost</label>
                              <input
                                type="number"
                                name="group_cost"
                                placeholder="$"
                                className={'formik-input-user-form form-control' + (errors.group_cost && touched.group_cost ? ' is-invalid' : '')}
                                value={values.group_cost}
                                onChange={(e) => this.changeHandler(e, 'group_cost')}
                                onBlur={handleBlur}
                              />
                              <ErrorMessage name="group_cost" component="div" className="invalid-feedback" />
                            </div>

                            <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                              <label>Unit Cost</label>
                              <input
                                type="number"
                                name="unit_cost"
                                placeholder="$"
                                className={'formik-input-user-form form-control' + (errors.unit_cost && touched.unit_cost ? ' is-invalid' : '')}
                                value={values.unit_cost}
                                onChange={(e) => this.changeHandler(e, 'unit_cost')}
                                onBlur={handleBlur}
                              />
                              <ErrorMessage name="unit_cost" component="div" className="invalid-feedback" />
                            </div>

                            <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                              <label>Disclousres</label>
                              {/* <select
                              name="disclosure"
                              className={'formik-input-user-form form-control' + (errors.disclosure && touched.disclosure ? ' is-invalid' : '')}
                              value={values.disclosure}
                              onChange={(e) => this.appendedFormDisclosureOnchange(e, i)}
                              onBlur={handleBlur}
                            >
                              <option value="">Select Disclousres</option>
                              {this.state.getDisclosures && this.state.getDisclosures.length > 0
                                && this.state.getDisclosures.map((item, i) => {
                                  return (
                                    <option key={i} value={item.id}>{item.disclosure_name}</option>
                                  )
                                })}
                            </select> */}

                              <MultiSelect
                                options={disclosureValues}
                                name="disclosure"
                                className="form-control" selectAllLabel='All Disclousres'
                                value={values.disclosureids}
                                onChange={this.discSelected}
                                labelledBy="Select Disclosures"
                              />


                              <ErrorMessage name="disclosure" component="div" className="invalid-feedback" />
                            </div>

                            <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                              <label>Attributes</label>
                              {/* <select
                              name="attribute"
                              className={'formik-input-user-form form-control' + (errors.attribute && touched.attribute ? ' is-invalid' : '')}
                              value={values.attribute}
                              onChange={(e) => this.appendedFormAttributeOnchange(e, i)}
                              onBlur={handleBlur}
                            >
                              <option>Select Attributes</option>
                              {this.state.getAttributes && this.state.getAttributes.length > 0
                                && this.state.getAttributes.map((item, i) => {
                                  return (
                                    <option key={i} value={item.id}>{item.attribute_name}</option>
                                  )
                                })}
                            </select> */}
                              <ErrorMessage name="attribute" component="div" className="invalid-feedback" />


                              <MultiSelect
                                options={AttrValues}
                                name="attribute"
                                className="form-control" selectAllLabel='All Disclousres'
                                value={values.attrbtids}
                                onChange={this.attrSelected}
                                labelledBy="Select Attributes"
                              />
                            </div>

                            <div className="form-group add-listing-form-group add-listing-form-group-four-width">
                              <label>PO ID</label>
                              <input
                                type="text"
                                name="po_id"
                                placeholder="PO ID"
                                className={'formik-input-user-form form-control' + (errors.po_id && touched.po_id ? ' is-invalid' : '')}
                                value={values.po_id}
                                onChange={(e) => this.changeHandler(e, 'PO_ID')}
                                onBlur={handleBlur}
                              />
                              <ErrorMessage name="po_id" component="div" className="invalid-feedback" />
                            </div>



                            <div className="form-group add-listing-form-group-full-width add-listing-form-group">
                              <label>Tags</label>
                              {/* <input
                              type="text"
                              name="tags"
                              placeholder="Tags"
                              className={'formik-input-user-form form-control' + (errors.tags && touched.tags ? ' is-invalid' : '')}
                              value={values.tags}
                              onChange={(e) => this.appendedFormTagsOnchange(e, i)}
                              onBlur={handleBlur}
                            />
                            <ErrorMessage name="tags" component="div" className="invalid-feedback" /> */}
                              <MultiSelect
                                options={tagValues}
                                name="tags"
                                className="form-control" selectAllLabel='All Disclousres'
                                value={values.selectedTags}
                                onChange={(e) => this.setState({ selectedTags: e })}
                                labelledBy="Select Disclosures"

                              />
                            </div>
                            {/* // this.tagSelected */}
                            <div className="form-group add-listing-form-group-half-width add-listing-form-group">
                              <label>Internal Notes</label>
                              <textarea
                                name="internal_notes"
                                className={'formik-input-user-form form-control add-listing-text-area' + (errors.internal_notes && touched.internal_notes ? ' is-invalid' : '')}
                                value={values.internal_notes}
                                onChange={(e) => this.appendedFormInternalNotesOnchange(e, i)}
                                onBlur={handleBlur}
                              > </textarea>
                              <ErrorMessage name="internal_notes" component="div" className="invalid-feedback" />
                            </div>

                            <div className="form-group add-listing-form-group-half-width add-listing-form-group">
                              <label>External Notes</label>
                              <textarea
                                name="external_notes"
                                className={'formik-input-user-form form-control add-listing-text-area' + (errors.external_notes && touched.external_notes ? ' is-invalid' : '')}
                                value={values.external_notes}
                                onChange={(e) => this.appendedFormExternalNotesOnchange(e, i)}
                                onBlur={handleBlur}
                              ></textarea>
                              <ErrorMessage name="external_notes" component="div" className="invalid-feedback" />
                            </div>

                            {i ?
                              <div className="col-sm-4">
                                <button type="button" className="btn btn-danger rounded-pill" onClick={() => this.handleRemoveClick(i)} >Remove</button>
                              </div>
                              :
                              ''
                            }


                          </div>

                        );
                      })}


                      {/* append field end */}

                    </div>

                    <br />

                    {/* --------------------- */}

                    {/* Channel Managment Section Attachment Start */}

                    {/* <h2 className="addlisting-loaction-text">Attachments</h2> */}

                    <div className="row">
                      {/*                       
                      {this.state.inputList && this.state.inputList.map((x, i) => {
                        return (
                          <div className="col-sm-6">
                            <div className="add-listing-group-name"><h4>Group {i + 1}</h4></div>

                            <Row>
                              <Col md="12" className="form-group">
                                <label className="form-label-userprofile">
                                  Seat 1
                                </label>
                                <div className="channel-markup-row input-row">

                                  <FormInput
                                    type="text"
                                    placeholder="Ticket Document"
                                    style={{
                                      width: "50%",
                                      borderRadius: "10px"
                                    }}
                                    value={this.state.ebayMarkupValue}

                                  />

                                  <FormInput
                                    type="file"
                                    style={{
                                      width: "50%",
                                      borderRadius: "10px"
                                    }}
                                    name="attachment"
                                    onChange={(e) => this.appendedFormAttachmentOnchange(e, i)}
                                  />

                                  &nbsp;&nbsp;&nbsp;
                                  <i className="fa fa-paperclip" style={{
                                    fontSize: "25px",
                                    color: "#3bcfd0"
                                  }} aria-hidden="true"></i>
                                </div>
                              </Col>
                            </Row>

                          </div>
                        );
                      })} */}

                    </div>


                    {/* Channel Managment Section Attachment End */}




                    {/* Channel Managment Section Start */}

                    <div className="row">
                      <div>
                        <h2 className="addlisting-loaction-text">Channel Managment</h2>
                        <Card
                          style={{
                            borderRadius: "10px"
                          }}
                          small
                          className="mb-4"
                          bordered={false}
                        >

                          <Row>

                            {/* {this.state.channelManagmentInputList && this.state.channelManagmentInputList.map((x, i) => {




                              return (




                                <Col md="12" className="form-group">
                                  <div className="channel-markup-row input-row">
                                    <label className="form-label-userprofile">
                                      {x.channel_markup_name}
                                    </label>
                                    <Switch
                                      checkedChildren="%"
                                      unCheckedChildren="$"
                                      defaultChecked
                                      style={{
                                        marginLeft: "10px",
                                        marginRight: "20px",
                                        backgroundColor: "#5f8ff0"
                                      }}
                                      onChange={(e) => this.channelManagmentToggleOnchange(e, i)}
                                    />

                                    <FormInput
                                      type="number"
                                      min="0"
                                      placeholder={x.channel_markup_type_id && x.channel_markup_type_id == 1 ? '%' : '$'}
                                      style={{
                                        width: "50%",
                                        borderRadius: "25px"
                                      }}
                                      value={x.markup_amount}
                                      onChange={(value) => this.channelManagmentOnchange(value, i)}
                                    />

                                  </div>
                                </Col>


                              );
                            })}
 */}

                            {/* ---- */}

                            {this.state.getChannels && this.state.getChannels.map((cm_values, i) =>

                              <Col md="12" className="form-group">
                                <div className="channel-markup-row input-row">
                                  <label className="form-label-userprofile col-sm-2">
                                    {cm_values.channel_name}
                                  </label>
                                  <Switch
                                    checkedChildren="$"
                                    unCheckedChildren="%"
                                    defaultChecked
                                    style={{
                                      marginLeft: "10px",
                                      marginRight: "20px",
                                      backgroundColor: "#5f8ff0"
                                    }}
                                    checked={cm_values.channel_markup_type_id == 2 ? true : false}
                                    onChange={(e) => this.channelSlide(e, i)}
                                  />
                                  <FormInput
                                    type="number"
                                    min="0"
                                    placeholder="$"
                                    style={{
                                      width: "50%",
                                      borderRadius: "25px"
                                    }}
                                    value={cm_values.markup_amount ? cm_values.markup_amount : 0}
                                    onChange={(e) => this.channelManagmentOnchange(e, i)}
                                  />
                                </div>
                              </Col>

                            )}

                            {/* --- */}
                          </Row>

                        </Card>
                      </div>
                    </div>


                    {/* Channel Managment Section End */}
                    {/* --------------------- */}
                    <div className="row">
                      <div>
                        <button className="btn btn-success rounded-pill"><Link to="/listings"> View PO</Link></button>
                        <button className="btn btn-success rounded-pill"><Link to="/listings"> View on Phunnel</Link></button>
                        <button className="btn btn-success rounded-pill"><Link to="/listings"> View on Viagogo</Link></button>
                      </div>
                    </div>
                    <br />
                    <div className="row">
                      <div>
                        <button type="submit" className="btn btn-success rounded-pill">Update listing</button>
                        <button type="button" onClick={(e) => this.viewListing(false)} className="btn btn-success rounded-pill">Cancel </button>
                      </div>
                    </div>
                  </Form>
                </div>
              )}
            />
          </Modal.Body>
        </Modal>




        {/* Price Update Confirmation Modal */}
        <Modal className="price-confirmation--modal" show={this.state.priceUpdateConfirmationModal} onHide={(e) => this.priceUpdateModal(false)} onShow={(e) => { this.updateModelStatus(e) }} backdrop="static">
          <Modal.Header closeButton>
          </Modal.Header>
          <Modal.Body  >
            <div className="update-price-item">
              <h3>Are you sure you want to update the price on the listing?</h3>
              <h4>From ${this.state.dynamicUpdate.listOldValue} to <span>${this.state.dynamicUpdate.listValue ? this.state.dynamicUpdate.listValue : this.state.dynamicUpdate.listOldValue}</span></h4>
            </div>
          </Modal.Body>

          <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={(e) => this.priceUpdateModal(false)}>
              Cancel
            </Button>
            <Button type="submit" className="btn btn-primary" variant="primary" onClick={(e) => this.priceUpdateModal(true)} ref={this.state.innerRef}>
              Submit
            </Button>
          </Modal.Footer>
        </Modal>

        {/* share Update Confirmation Modal */}
        <Modal className="share-confirmation--modal" show={this.state.sharingConfirmationModal} onHide={(e) => this.shareModal(false)} onShow={(e) => { this.updateModelStatus(e) }} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title> Are you sure? </Modal.Title>
          </Modal.Header>
          <Modal.Body onKeyDown={(e) => this.handleEnterEventForModals(e, 'shareConfirmation')} tabIndex="0">
            <div className="form-group">
              <h3 className="share-unshare-text">
                Do you still want to {this.state.myModalSubmitBtn} all listings from the event.
              </h3>
            </div>
          </Modal.Body>

          <Modal.Footer className="modal-btn-align notes-modal">
            <Button variant="secondary" onClick={(e) => this.shareModal(false)}>
              Cancel
            </Button>
            <Button variant="primary" onClick={(e) => this.shareModal(true)} ref={this.state.innerRef}>
              Submit
            </Button>
          </Modal.Footer>
        </Modal>

        {/* Share & Pause Share Confirmation Modal */}
        <Modal className="share-comfirmation" show={this.state.shareConfirmationModal} onHide={(e) => this.shareConfirm(false)} onShow={(e) => { this.updateModelStatus(e) }} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>{this.state.myModalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body onKeyDown={(e) => this.handleEnterEventForModals(e, 'share')} tabIndex="0">
            <div className="form-group">
              <h3 className="share-unshare-text">
                {this.state.shareModalBody}
              </h3>
            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button variant="primary" onClick={(e) => this.shareConfirm(true)} ref={this.state.innerRef}>
              {this.state.myModalSubmitBtn}
            </Button>
          </Modal.Footer>
        </Modal>

        {/* Internal Notes Confirmation Modal */}
        <Modal className="mytickets-list-popup" show={this.state.internalNotesConfirmationModal} onHide={(e) => this.internalNotesModal(false)} onShow={(e) => { this.updateModelStatus(e) }} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>{this.state.myModalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body onKeyDown={(e) => this.handleEnterEventForModals(e, 'internalNotes')}>
            <div className="form-group">
              <label>Internal Notes</label>
              <textarea className="form-control" value={this.state.internalNotes} onChange={this.handleInternalNotesChange} onKeyDown={(e) => this.handleEnterEventForModals(e, 'internalNotes')} />
            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align notes-modal">
            <Button variant="secondary" onClick={(e) => this.internalNotesModal(false)}>
              {this.state.myModalCancelBtn}
            </Button>
            <Button variant="primary" onClick={(e) => this.internalNotesModal(true)} ref={this.state.innerRef}>
              {this.state.myModalSubmitBtn}
            </Button>
          </Modal.Footer>
        </Modal>

        {/* Price Update Confirmation Modal - Event Level */}
        <Modal className="price-update-modal" show={this.state.eventLevelPriceConfirmationModal} onHide={(e) => this.confirmationPriceUpdate(false)} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Update Price</Modal.Title>
          </Modal.Header>
          <Modal.Body onKeyDown={(e) => this.handleEnterEventForModals(e, 'price')} tabIndex="0">
            <div className="change-price-item price-modalbody-section">
              <div className="form-group">
                <label>New Price</label>
                <input className={this.state.validation.price ? "custom-b-radius form-control is-invalid" : "custom-b-radius form-control"} value={this.state.eventLevelPrice} onChange={this.handleEventLevelPriceUpdate} required autoFocus />
                {
                  this.state.validation.price ? <div className="invalid-feedback" style={{ display: "block" }}>{this.state.eventLevelPrice <= 0 ? 'New Price or update price should be a valid number' : 'New Price or Update Price is required'}</div> : ''
                }
              </div>
              <div className="form-group">
                <label>Update Price</label>
                <div className="d-flex">
                  <div className="price-change-txt">
                    <input className="form-control" type="number" value={this.state.eventLevelUpdatePrice}
                      onChange={(e) => this.setState({ eventLevelUpdatePrice: e.target.value, eventLevelPrice: '' })}
                    />
                  </div>

                  <button className="price-add-btn primary-border-color"><AddIcon onClick={(value) => this.channelManagmentIncressOnchange()} /></button>
                  <button className="price-mini-btn"><MinusIcon onClick={(value) => this.channelManagmentDecressOnchange()} /></button>
                  <div className="button-switch">
                    <Switch className="price-switch-btn"
                      checkedChildren="%"
                      unCheckedChildren="$"
                      defaultChecked
                      onChange={(e) => this.updatePriceToggleOnchange(e)}
                    />
                  </div>
                </div>
                {
                  this.state.validation.updatePrice ? <div className="invalid-feedback" style={{ display: "block" }}>{this.state.eventLevelUpdatePrice != '' ? 'New Price or update price should be a valid number' : 'New Price or Update Price is required'}</div> : ''
                }
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            {/* <Button variant="secondary" onClick={(e) => this.confirmationPriceUpdate(false)}>
              Cancel
            </Button> */}
            {/* onClick={(e) => this.updateEventLevelPrice(true)} */}
            <Button variant="primary" onClick={(e) => this.confirmationPriceUpdate(true)}>
              Update
            </Button>
          </Modal.Footer>
        </Modal>


        {/* Price Change Confirmation Modal - Event Level */}
        <Modal className="price-update-modal price-confirmation--modal" show={this.state.priceShowConfModal} onHide={(e) => this.lastConfPriceUpdate(false)} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Price change confirmation</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="change-price-item price-modal-stmt" onKeyDown={(e) => this.handleEnterEventForModals(e, 'priceConfirmation')} tabIndex="0">
              {this.state.changeByEvent ? this.state.priceByEvent : this.state.priceByList}
              <span className={(this.state.eventLevelPrice > 0 || this.state.eventLevelUpdatePrice > 0) ? `pos-price ${this.state.priceUpdateType == '%' ? 'percent' : ''}` : `neg-price`}>
                {this.state.eventLevelPrice ? '$' + this.state.eventLevelPrice : ''}
                {this.state.eventLevelUpdatePrice ? (this.state.priceUpdateType == '$' ? this.state.priceUpdateType + ' ' + this.state.eventLevelUpdatePrice : this.state.eventLevelUpdatePrice + ' ' + this.state.priceUpdateType) : ''}

              </span>
            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={(e) => this.lastConfPriceUpdate(false)}>
              Cancel
            </Button>
            <Button variant="primary" onClick={(e) => this.lastConfPriceUpdate(true)}>
              Submit
            </Button>
          </Modal.Footer>
        </Modal>

        {/* In-Hand Change Confirmation Modal - Event Level */}
        <Modal className="price-update-modal price-confirmation--modal" show={this.state.inhandShowConfModal} onHide={(e) => this.inHandDateModal(false)}  onShow={(e) => { this.updateModelStatus(e) }} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>In-Hand Date Confirmation</Modal.Title>
          </Modal.Header>
          <Modal.Body onKeyDown={(e) => this.handleEnterEventForModals(e, 'inHandConfirmation')} tabIndex="0">
            <h4 className="change-date-text stylefont-weight-bold">Change all In-Hand Dates in the event to <span>{moment.utc(this.state.updateInhandDate).local().format('DD/MM/YYYY')}</span></h4>

          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={(e) => this.inHandDateModal(false)}>
              Cancel
            </Button>
            <Button variant="primary" onClick={(e) => this.inHandDateModal(true)} ref={this.state.innerRef}>
              Submit
            </Button>
          </Modal.Footer>
        </Modal>

        {/* Last confirmation-start*/}
        <Modal className="price-update-modal price-confirmation--modal" show={this.state.lastConfirmation} onHide={(e) => this.updateEventLevelPrice(false)} backdrop="static">
          <Modal.Header closeButton onKeyDown={(e) => this.handleEnterEventForModals(e, 'priceLastConfirmation')} tabIndex="0">
            <Modal.Title>Are you Sure?</Modal.Title>
          </Modal.Header>
          <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={(e) => this.updateEventLevelPrice(false)}>
              Cancel
            </Button>
            <Button variant="primary" onClick={(e) => this.updateEventLevelPrice(true)} ref={this.state.innerRef}>
              Submit
            </Button>
          </Modal.Footer>
        </Modal>


        {/* Last confirmation-end*/}
        {/* Disclosures Confirmation Modal */}
        <Modal show={this.state.disclosuresConfirmationModal} onHide={(e) => this.disclosuresModal(false)} className="mytickets-list-popup" onShow={(e) => { this.updateModelStatus(e) }} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>{this.state.myModalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body onKeyDown={(e) => this.handleEnterEventForModals(e, 'disclosures')} tabIndex="0">
            <div className="form-group">
              <label>Disclosures</label>
              <MultiSelect
                options={this.state.disclosuresList}
                className={`form-control ${this.state.validation.disclosure ? 'is-invalid' : ''}`}
                //selectAllLabel='Select All'
                value={this.state.selectedDisclosures}
                onChange={this.changeDisclosures}
                labelledBy="Select Disclosures"
                hasSelectAll={false}
                filterOptions={this.filterOptions}
              />
              {
                this.state.validation.disclosure ? <div className="invalid-feedback" style={{ display: "block" }}> Disclosure is required</div> : ''
              }
            </div>
            <div className="form-group">
              <label>External Notes</label>
              <textarea className="form-control" value={this.state.externalNotes} onChange={this.handleExternalNotesChange} onKeyDown={(e) => this.handleEnterEventForModals(e, 'disclosures')} />
            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button className={this.state.selectedDisclosures.length!=0 || (this.state.externalNotes!=undefined && this.state.externalNotes!='') ? "active" : ""} variant="primary" onClick={(e) => this.disclosuresModal(true)} ref={this.state.innerRef}>
              {this.state.myModalSubmitBtn}
            </Button>
          </Modal.Footer>
        </Modal>
        {/* Disclosures Confirmation Modal */}
        <Modal show={this.state.disclosuresListModal} onHide={(e) => this.disclosuresNotesModal(false)} className="mytickets-list-popup" onShow={(e) => { this.updateModelStatus(e) }} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Disclosures Confirmation</Modal.Title>
          </Modal.Header>
          <Modal.Body onKeyDown={(e) => this.handleEnterEventForModals(e, 'disclosuresConfirmation')} tabIndex="0">
            {this.state.levelName == levelNames.event ? <h5 className="disclosure-content-view">Update the disclosures on all listings: </h5> : <h5 className="disclosure-content-view">Update the disclosures on this listing: </h5>}

            <ul className="disclosures-item-body">
              {
                this.state.disclosuresLabel && this.state.disclosuresLabel.length && this.state.disclosuresLabel.map((item, index) => {
                  return (
                    <li key={index} className="disclosure-list-text"> {item}</li>
                  )
                })

              }
            </ul>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={(e) => this.disclosuresNotesModal(false)} >Cancel</Button>
            <Button className="btn btn-success info-btn-listing" type="submit"  onClick={() => this.disclosuresNotesModal(true)} ref={this.state.innerRef}>Submit</Button>

          </Modal.Footer>
        </Modal>




        {/* Inhand date Confirmation Modal */}
        <Modal className="inHandmodal" show={this.state.inHandDateConfirmationModal} onHide={(e) => this.inHandDateModal(false)} onShow={(e) => { this.updateModelStatus(e) }} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>{this.state.myModalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="form-group" onKeyDown={(e) => this.handleEnterEventForModals(e, 'inHand')} tabIndex="0">
              <label>In-Hand Date</label>
              <div className="modal-datepicker">
                <img src={iconCalendarImg} className="datemodal-icon" />
                <DatePicker
                  selectsRange={false}
                  selected={this.state.updateInhandDate}
                  value={this.state.updateInhandDate}
                  className="dateSinglepicker"
                  onChange={(e) => this.updateInhandDate(e)}
                  isClearable={false}
                  onKeyDown={(e) => this.avoidOnchange(e)}
                  maxDate={new Date(this.state.modalEventDate)}
                  dateFormat="dd-MM-yyyy"
                  placeholderText=" DD-MM-YYYY"
                />
                 {this.state.validation.inHandDate ? <div className="invalid-feedback" style={{ display: "block" }}> In-Hand date is required</div> : ''}
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button className={this.state.myModalSubmitBtnActive} variant="primary" 
            onClick={(e) => this.submitInHandDate(e)} ref={this.state.innerRef}>
              {this.state.myModalSubmitBtn}
            </Button>
          </Modal.Footer>
        </Modal>

        {/* Splits Confirmation Modal */}
        <Modal className="split-modal-popup" show={this.state.splitsConfirmationModal} onHide={(e) => this.splitsModal(false)} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>{this.state.myModalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body onKeyDown={(e) => this.handleEnterEventForModals(e, 'splits')} tabIndex="0">
            <div className="form-group">
              <label>Splits</label>
              {/* <select
                name="splitId"
                className={this.state.validation.split ? 'custom-b-radius split--box form-control is-invalid' : 'custom-b-radius split--box form-control'}
                onChange={(e) => this.changeSplits(e)}
                value={this.state.selectedSplits}
              >
                <option value="">Select Splits</option>
                {this.state.splitsList && this.state.splitsList.length > 0
                  && this.state.splitsList.map((item, i) => {
                    return (
                      <option key={i} value={item.id}>{item.name}</option>
                    )
                  })}
              </select> */}

              <Select
                className={'formik-input-user-form create-select-box'}
                placeholder={"Split Type"}
                style={{ width: "100%" }}
                name="split"
                // value={this.state.issueModalValues.issueType ? this.state.issueModalValues.issueType : []}

                options={allSplitsList}
                onChange={(e) => this.changeSplits(e)}
              // onChange={(e) => this.issueFlagModal(e, 'issueType')}
              />
              {/* don't remove the code TODO - for splits
                this.state.validation.split ? <div className="invalid-feedback" style={{ display: "block" }}>Splits type is required</div> : ''
              */}

            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button variant="primary" onClick={(e) => this.splitsModal(true)}>
              {this.state.myModalSubmitBtn}
            </Button>
          </Modal.Footer>
        </Modal>

        {/* Right Click Event - Mark As Sold - Confirmation Modal */}
        <Modal show={this.state.markAsSoldConfirmationModal} onHide={(e) => this.updateMarkAsSold(false)} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>{this.state.myModalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={(e) => this.updateMarkAsSold(false)}>
              {this.state.myModalCancelBtn}
            </Button>
            <Button variant="primary" className="btn btn-success info-btn-listing" onClick={(e) => this.updateMarkAsSold(true)}>
              {this.state.myModalSubmitBtn}
            </Button>
          </Modal.Footer>
        </Modal>

        <Alert
          header={"Alert"}
          body={this.state.commonInfoModalMessage}
          show={this.state.showCommonInfoModal}
          submit={() => this.handleAlertSubmitButtonModal(true)}
          closeButton={() => this.handleAlertSubmitButtonModal(true)}
        />

        {/* common alert modal   * /}
        <Modal show={this.state.commonAlertModal} className="inhandconfirm-modal" >
          <Modal.Header>
            <Modal.Title>In-Hand Date Confirmation</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {/* <p className="info-txt-userlisting">{this.state.alertTitle}</p> * /}
            <p className="info-txt-userlisting">Change all In-Hand Dates in the event toy <span>24/06/2021</span></p>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={() => this.commonAlertModal()}>Cancel</Button>
            <Button className="btn btn-success info-btn-listing" onClick={() => this.commonAlertModal(false, '')}>Submit</Button>
          </Modal.Footer>
        </Modal>

        {/* common alert modal   */}
        <Modal show={this.state.removeListingModal} onHide={(e) => this.setState({ removeListingModal: false, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } })} onShow={(e) => { this.updateModelStatus(e) }} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Are you sure?</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h4 style={{ textAlign: 'center' }} >Do you really want to remove the listing ? </h4>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={() => this.setState({ removeListingModal: false, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } })}>
              Cancel
            </Button>
            <Button variant="primary" className="btn btn-success info-btn-listing" onClick={(e) => this.removeListing()} ref={this.state.innerRef}>
              Submit
            </Button>
            {/* <button className="btn btn-success rounded-pill" onClick={() => this.setState({ removeListingModal: false, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } })}>Cancel</button>
            <button className="btn btn-success info-btn-listing" onClick={() => this.removeListing()}>OK</button> */}
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.changeEventStatusStatus} onHide={(e) => this.setState({ changeEventStatusStatus: false, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } })} onShow={(e) => { this.updateModelStatus(e) }} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Listing Status</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h4 style={{ textAlign: 'center' }} >Do you really want to change listing status as {this.state && this.state.listInActive ? 'Active' : 'Inactive'} ?</h4>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={() => this.setState({ changeEventStatusStatus: false })}>Cancel</Button>
            <Button className="btn btn-success info-btn-listing" onClick={() => this.changeListingGroupDbStatus()} ref={this.state.innerRef} >OK</Button>
          </Modal.Footer>
        </Modal>

        {/* Sort Modal */}
        <Modal className="sortTableModal" show={this.state.sortModal} onHide={(e) => this.sortModal(false)} size="lg" aria-labelledby="contained-modal-title-vcenter" centered backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Filter by Listing Status: </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="filter-sort-modal">
              <div className="check-sec-sort">
                <form>
                  <div className="checkbox-group">
                    {
                      this.state.filterListingElement.map((listing, index) => {
                        return (

                          <div key={index} className="form-check">
                            {this.eventLevelFilterCall(listing, index)}
                          </div>
                        )
                      })
                    }

                  </div>
                </form>
              </div>
              <div className="sort-grid-section3">
                <div className="divi-grid-items">
                  <h5 className="sort-title">Sort price:</h5>
                  <div className="d-flex" style={{ marginBottom: '15px' }}>
                    <div className="sort-value">$ - $$$</div>
                    <button className="btn up-arrow" onClick={() => this.setSortingElement(sortListingData.priceInAscending)} > {(this.state.selectedArrow == sortListingData.priceInAscending || this.state.sortListingType.sortListingDataByPriceInAscending) ? <UpArrowFill /> : <div className="inactive-arrow"><UpArrowBorder /></div>} </button>
                  </div>
                  <div className="d-flex" style={{ marginBottom: '15px' }}>
                    <div className="sort-value">$$$ - $</div>
                    <button className="btn down-arrow" onClick={() => this.setSortingElement(sortListingData.priceInDescending)} >{this.state.selectedArrow == sortListingData.priceInDescending || this.state.sortListingType.sortListingDataByPriceInDescending ? <div className="inactive-arrow" style={{ marginTop: '3px' }}><UpArrowFill /></div> : <UpArrowBorder />}</button>
                  </div>
                </div>
                <div className="divi-grid-items">
                  <h5 className="sort-title">Sort Section:</h5>
                  <div className="d-flex" style={{ marginBottom: '15px' }}>
                    <div className="sort-value">Ascending</div>
                    <button className="btn up-arrow" onClick={() => this.setSortingElement(sortListingData.sectionInAscending)}  > {this.state.selectedArrow == sortListingData.sectionInAscending || this.state.sortListingType.sortListingDataBySectionInAscending ? <UpArrowFill /> : <div className="inactive-arrow"><UpArrowBorder /></div>} </button>
                  </div>
                  <div className="d-flex" style={{ marginBottom: '15px' }}>
                    <div className="sort-value">Desending</div>
                    <button className="btn down-arrow" onClick={() => this.setSortingElement(sortListingData.sectionInDescending)} > {this.state.selectedArrow == sortListingData.sectionInDescending || this.state.sortListingType.sortListingDataBySectionInDescending ? <div className="inactive-arrow" style={{ marginTop: '3px' }}><UpArrowFill /></div> : <UpArrowBorder />} </button>
                  </div>
                </div>
                <div className="divi-grid-items">
                  <h5 className="sort-title">Sort Row:</h5>
                  <div className="d-flex" style={{ marginBottom: '15px' }}>
                    <div className="sort-value">Ascending</div>
                    <button className="btn up-arrow" onClick={() => this.setSortingElement(sortListingData.rowInAscending)}> {this.state.selectedArrow == sortListingData.rowInAscending || this.state.sortListingType.sortListingDataByRowInAscending ? <UpArrowFill /> : <div className="inactive-arrow"><UpArrowBorder /></div>} </button>
                  </div>
                  <div className="d-flex" style={{ marginBottom: '15px' }}>
                    <div className="sort-value">Desending</div>
                    <button className="btn down-arrow" onClick={() => this.setSortingElement(sortListingData.rowInDescending)} > {this.state.selectedArrow == sortListingData.rowInDescending || this.state.sortListingType.sortListingDataByRowInDescending ? <div className="inactive-arrow" style={{ marginTop: '3px' }}><UpArrowFill /></div> : <UpArrowBorder />} </button>
                  </div>
                </div>
              </div>
              <div className="select-checkbox-filter">
                <h4 className="view-title-text">View</h4>
                  <form>
                    <div className="checkbox-grid-item">
                      <div className="select-all-radio-btn">
                          <label className="form-check-container">View All Listings
                            <input type="radio" checked={this.state.selectAllListing} onClick={() => this.setState({ selectAllListing: !this.state.selectAllListing, enableFilterSubmit: true })} name="radio" />
                            <span className="checkmark"></span>
                          </label>
                      </div>
                      <div className="select-all-radio-btn">
                          <label className="form-check-container">View ViaGogo Listings
                            <input type="radio" checked={this.state.selectViagogoListing} onClick={() => this.setState({ selectViagogoListing: !this.state.selectViagogoListing, enableFilterSubmit: true, eventLevelFilter: [] })} name="radio" />
                            <span className="checkmark"></span>
                          </label>
                      </div>
                      <div className="select-all-radio-btn">
                          <label className="form-check-container">View Flagged Listings
                            <input type="radio" checked={this.state.selectFlaggedListing} onClick={() => this.setState({ selectFlaggedListing: !this.state.selectFlaggedListing, enableFilterSubmit: true, eventLevelFilter: [] })} name="radio" />
                            <span className="checkmark"></span>
                          </label>
                      </div>
                    </div>
                  </form>
                { this.state.enableFilterSubmit || this.state.selectFlaggedListing || this.state.selectViagogoListing ? <div className="ml-auto"><button type="button" className="reset-btn-text" title="Reset" onClick={(e) => this.resetSorting(e)}>Reset</button></div> : ''}

              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button className={`sort-submit-btn ${this.state.enableFilterSubmit == false ? "disable-sort-btn" : ''}`} onClick={(e) => this.filterAndSortListingElement(e)}>
              Submit
            </Button>
          </Modal.Footer>
        </Modal>
        {/* create issue Modal-start */}
        <Modal className="price-confirmation--modal create-issues-modal" show={this.state.createIssueModal} onHide={(e) => this.setState({ createIssueModal: false })} >
          <Modal.Header closeButton>
            <h3>Create New Issue</h3>
          </Modal.Header>
          <Modal.Body  >
            <Formik
              initialValues={{
                assign: this.state.issueModalValues.assign && this.state.issueModalValues.assign.value ? this.state.issueModalValues.assign : '',

                issueType: this.state.issueModalValues.issueType && this.state.issueModalValues.issueType.value ? this.state.issueModalValues.issueType : '',

                issueTag: this.state.issueModalValues.issueTag && this.state.issueModalValues.issueTag.length > 0 ? this.state.issueModalValues.issueTag : '',

                issueNotes: this.state.issueModalValues.issueNotes
              }}
              enableReinitialize={true}

              validationSchema={Yup.object().shape({

                assign: Yup.string()
                  .required('Assign is required'),
                issueType: Yup.string()
                  .required('Issue type is required'),
                issueTag: Yup.string()
                  .required('Issue tag is required'),
                issueNotes: Yup.string()
                  .required('Issue notes is required'),
              })}


              onSubmit={async (fields) => {
                this.issueFlagConfirm(fields);
              }
              }

              render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                <Form onKeyDown={(e) => this.handleEnterEventForModals(e, 'flag')} tabIndex="0">

                  <div className="form-group">
                    <div className="row">
                      <div className="col">
                        <label>Assign</label>

                        <Select
                          className={'formik-input-user-form create-select-box' + (errors.assign && touched.assign ? ' is-invalid' : '')}
                          placeholder="Assign"
                          style={{ width: "100%" }}
                          name="assign"
                          value={this.state.issueModalValues.assign ? this.state.issueModalValues.assign : []}
                          options={this.state.allAssigneeDetails}
                          onChange={(e) => this.issueFlagModal(e, 'assign')}
                          allowClear
                          showSearch
                          isClearable
                        />
                        <ErrorMessage name="assign" component="div" className="invalid-feedback" />
                      </div>
                      <div className="col">
                        <label>Issue type</label>
                        <Select
                          className={'formik-input-user-form create-select-box' + (errors.issueType && touched.issueType ? ' is-invalid' : '')}
                          placeholder="Issue Type"
                          style={{ width: "100%" }}
                          name="issueType"
                          value={this.state.issueModalValues.issueType ? this.state.issueModalValues.issueType : []}
                          options={this.state.issueTypes}
                          onChange={(e) => this.issueFlagModal(e, 'issueType')}
                          allowClear
                          showSearch
                          isClearable
                        />
                        <ErrorMessage name="issueType" component="div" className="invalid-feedback" />
                      </div>
                    </div>

                  </div>
                  <div className="form-group">
                    <div className="row">
                      <div className="col">
                        <label>Issue Tags</label>
                        <Select
                          className={'formik-input-user-form create-select-box' + (errors.issueTag && touched.issueTag ? ' is-invalid' : '')}
                          placeholder="Tags"
                          style={{ width: "100%" }}
                          name="issueTag"
                          value={values.issueTag}
                          options={tagValues}
                          onChange={(e) => this.issueFlagModal(e, 'issueTag')}
                          allowClear
                          showSearch
                          isClearable
                          isMulti
                        />
                        <ErrorMessage name="issueTag" component="div" className="invalid-feedback" />
                      </div>

                    </div>

                  </div>
                  <div className="form-group">
                    <div className="row">
                      <div className="col">
                        <label>Issue Notes</label>
                        <textarea className={'formik-input-user-form form-control issue-flag-notes' + (errors.issueNotes && touched.issueNotes ? ' is-invalid' : '')} name="issueNotes" value={this.state.issueModalValues.issueNotes} onChange={(e) => this.issueFlagModal(e, 'issueNotes')} >

                        </textarea>
                        <ErrorMessage name="issueNotes" component="div" className="invalid-feedback" />
                      </div>
                    </div>
                  </div>
                  <div className="modal-btn-align issue-footer">
                    <Button type="submit" className="create-issues-btn secondary-bg-color" variant="secondary" ref={node => (this.flagIssueFormSubmit = node)} >
                      Submit New Issue
                    </Button>
                  </div>
                </Form>
              )}
            />
          </Modal.Body>

          <Modal.Footer className="modal-btn-align">


          </Modal.Footer>
        </Modal>
        {/* create issue Modal-end */}

        {/* Issue flag create confirmation - start */}
        <Modal show={this.state.showIssueFlagConfirm} onHide={(e) => this.setState({ showIssueFlagConfirm: false, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } })} >
          <Modal.Header closeButton>
            <Modal.Title>Submit New Issue</Modal.Title>
          </Modal.Header>
          <Modal.Body onKeyDown={(e) => this.handleEnterEventForModals(e, 'flagConfirmation')} tabIndex="0">
            <h4 style={{ textAlign: 'center' }} > This will unshare the listing and mark as inactive</h4>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={() => this.setState({ showIssueFlagConfirm: false, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } })}>Cancel</Button>
            <Button className="btn btn-success info-btn-listing" onClick={(e) => this.createNewIssue()} >OK</Button>
          </Modal.Footer>
        </Modal>
        {/* Issue flag create confirmation - end */}
        {/* Issue flag create success - start */}
        <Modal show={this.state.showIssueFlagSuccess} onHide={(e) => this.setState({ showIssueFlagSuccess: false, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } })} >
          <Modal.Header closeButton>
            <Modal.Title>{this.state.flagIssueResponse.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body onKeyDown={(e) => this.handleEnterEventForModals(e, 'flagLastConfirmation')} tabIndex="0">
            <h4 style={{ textAlign: 'center' }} >{this.state.flagIssueResponse.msg}</h4>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">

            <Button className="btn btn-success info-btn-listing" onClick={(e) => this.setState({ showIssueFlagSuccess: false, listingGroupIds: [], listingGroupIdsObject: { event_id: 0, listing_id: [] } })} >Continue</Button>
          </Modal.Footer>
        </Modal>
        {/* Issue flag create success - end */}
        {/* viagogo eTicket Viagogo Order modal popups */}
        <Modal className="viagogo-orders-modal"
          show={this.state.showViagogoModal} onHide={(e) => this.setState({ showViagogoModal: false })}
          backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Viagogo Orders</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Formik
              initialValues={{
                loggedInUserId: this.context.user.id,
                id: this.state.viagogoListingGroupData.id ? this.state.viagogoListingGroupData.id : 0,
                event_id: this.state.viagogoListingGroupData.event_id ? this.state.viagogoListingGroupData.event_id : 0,
                eventname: this.state.viagogoListingGroupData.eventname ? this.state.viagogoListingGroupData.eventname : '',
                eventdate: this.state.viagogoListingGroupData.eventdate ? this.state.viagogoListingGroupData.eventdate : '',
                performername: this.state.viagogoListingGroupData.performername ? this.state.viagogoListingGroupData.performername : '',
                venuename: this.state.viagogoListingGroupData.venuename ? this.state.viagogoListingGroupData.venuename : '',
                city: this.state.viagogoListingGroupData.city ? this.state.viagogoListingGroupData.city : '',
                state: this.state.viagogoListingGroupData.state ? this.state.viagogoListingGroupData.state : '',
                country: this.state.viagogoListingGroupData.country ? this.state.viagogoListingGroupData.country : '',
                ticketing_system_name: this.state.viagogoListingGroupData.ticketing_system_name ? this.state.viagogoListingGroupData.ticketing_system_name : '',
                tickettypename: this.state.viagogoListingGroupData.tickettypename ? this.state.viagogoListingGroupData.tickettypename : '',
                section: this.state.viagogoListingGroupData.section ? this.state.viagogoListingGroupData.section : '',
                row: this.state.viagogoListingGroupData.row ? this.state.viagogoListingGroupData.row : '',
                quantity: this.state.viagogoListingGroupData.quantity ? this.state.viagogoListingGroupData.quantity : '',
                seat_start: this.state.viagogoListingGroupData.seat_start ? this.state.viagogoListingGroupData.seat_start : '',
                seat_end: this.state.viagogoListingGroupData.seat_end ? this.state.viagogoListingGroupData.seat_end : '',
                soldprice: this.state.viagogoListingGroupData.soldprice ? this.state.viagogoListingGroupData.soldprice : '',
                viagogoOtherCost: 0,
                viagogoFees: 0,
                in_hand: this.state.viagogoListingGroupData.in_hand ? this.state.viagogoListingGroupData.in_hand : '',
                delivery_status_id: this.state.viagogoListingGroupData.delivery_status_id ? this.state.viagogoListingGroupData.delivery_status_id : '',
                viagogoChannelReferenceId: '',
                viagogoDisclosures: [],
                viagogoTags: [],
                internal_notes: this.state.viagogoListingGroupData.internal_notes ? this.state.viagogoListingGroupData.internal_notes : ''
              }}
              enableReinitialize={true}

              validationSchema={Yup.object().shape({

                quantity: Yup.string()
                  .required('Quantity is required'),

                /* seat_start: Yup
                .number()
                .integer()
                .required('Seat start is required'),

                seat_end: Yup
                .number()
                .integer()
                .nullable()
                .min(Yup.ref('seat_start'), 'Seat end must be greater than seat start')
                .required('Seat end is required'), */

                soldprice: Yup.string()
                  .required('Sold Price is required'),

                /* delivery_status_id: Yup.string()
                  .required('Delivery Status is required'), */
              })}
              //TODO - Willcall & other ticket types viagogo order form completion
              onSubmit={async fields => {
                const formFieldsData = await { ...fields };
                this.setState({
                  showViagogoModal: false,
                  viagogoFormFieldsData: formFieldsData,
                  markAsSoldConfirmationModal: true,
                  myModalTitle: 'Are you sure want to update this listing group as SOLD ? ',
                  myModalCancelBtn: 'Cancel',
                  myModalSubmitBtn: 'Update'
                });
              }}

              render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                <Form>
                  <div className="event-order-list">
                    <div className="event-detail-top">
                      <div>
                        <h3 className="event--title--text" name="viagogoEventName">{values.eventname}</h3>
                        <h4 className="event--date--text" name="viagogoEventDate">
                          {
                            (values.eventdate) ?
                              ((values.timezone && values.timezone[0]) ?
                                moment.utc(values.eventdate).tz(values.timezone[0]).format('dddd MMM Do YYYY @ h:mm A z') : moment.utc(values.eventdate).format('dddd MMM Do YYYY @ h:mm A z')) : ''
                          }
                        </h4>
                        <h5 className="event--location--text" name="viagogoPerformerName">{values.performername}, {values.venuename}, {values.city}, {values.state}, {values.country}</h5>
                      </div>
                      <div>
                        <h5 className="event--right--text">Event ID: {values.event_id}</h5>
                        <h5 className="event--right--text">Assigned: Jake Beman</h5>
                      </div>
                    </div>
                    <div className="event-list-detail">
                      <div className="event--grid-list-detail">
                        <div>
                          <h4 className="event--listing--text">Lisiting ID: <span>{values.id}</span></h4>
                          <h4 className="event--listing--text">Ticketing System: <span>{values.ticketing_system_name}</span></h4>
                          <h4 className="event--listing--text">Ticket Type: <span>{values.tickettypename}</span></h4>
                        </div>
                        {this.state.listingSeatTypeViagogoHold != 'General Admission' ?
                          (
                            <div className="event-sec-row">
                              <div>
                                <p className="text--section">Sec:</p>
                                <div className="section-value-box no-box-section">
                                  <h3>{values.section}</h3>
                                </div>
                              </div>
                              <div>
                                <p className="text--section">Row: </p>
                                <div className="section-value-box no-box-section">
                                  <h3>{values.row}</h3>
                                </div>
                              </div>
                            </div>
                          ) :
                          <div className="general-admin-btn">
                            <button className="btn general-btn">General Admission</button>
                          </div>
                        }
                      </div>
                      {this.state.listingTicketTypeViagogoHold == 'eTransfer' ?
                        (
                          <div className="e-transfer-grid">
                            <div className="customer-details-item">
                              <label>Customer Name: </label>
                              <input className="form-control" type="text" placeholder="Name" />
                            </div>
                            <div className="customer-details-item">
                              <label>Customer email: </label>
                              <input className="form-control" type="text" placeholder="Email" />
                            </div>
                          </div>
                        ) : ''}

                    </div>
                  </div>
                  <div className="select-modal-popup">
                    <div className="select-grid-section">
                      <div className="form-group select-area-fields">
                        <label>Quantity</label>
                        <input
                          type="text"
                          name="quantity"
                          min='0'
                          className={'formik-input-user-form form-control' + (errors.quantity && touched.quantity ? ' is-invalid' : '')}
                          placeholder="Quantity"
                          value={values.quantity}
                          onChange={(e) => this.viagogoChangeHandler(e, listingConst.quantity)} />
                        <ErrorMessage name="quantity" component="div" className="invalid-feedback" />
                      </div>
                      {this.state.listingSeatTypeViagogoHold != 'General Admission' ?
                        (
                          <>
                            <div className="form-group select-area-fields">
                              <label>Seat Start</label>
                              <input
                                type="text"
                                name="seat_start"
                                min='0'
                                className={'formik-input-user-form form-control' + (errors.seat_start && touched.seat_start ? ' is-invalid' : '')}
                                placeholder="Seat Start"
                                value={values.seat_start}
                                onChange={(e) => this.viagogoChangeHandler(e, listingConst.seat_start)} />
                              <ErrorMessage name="seat_start" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group select-area-fields">
                              <label>Seat End</label>
                              <input
                                type="text"
                                name="seat_end"
                                min='0'
                                className={'formik-input-user-form form-control' + (errors.seat_end && touched.seat_end ? ' is-invalid' : '')}
                                placeholder="Seat End"
                                value={values.seat_end}
                                onChange={(e) => this.viagogoChangeHandler(e, listingConst.seat_end)} />
                              <ErrorMessage name="seat_end" component="div" className="invalid-feedback" />
                            </div>
                          </>
                        ) : ''}
                      <div className="form-group select-area-fields">
                        <label>Sold Price</label>
                        <input
                          type="text"
                          name="soldprice"
                          min='0'
                          className={'formik-input-user-form form-control' + (errors.soldprice && touched.soldprice ? ' is-invalid' : '')}
                          placeholder="Sold Price"
                          value={values.soldprice}
                          onChange={(e) => this.viagogoChangeHandler(e, listingConst.soldprice)} />
                        <ErrorMessage name="soldprice" component="div" className="invalid-feedback" />
                      </div>
                      <div className="form-group select-area-fields">
                        <label>Other Costs</label>
                        <input
                          type="text"
                          min='0'
                          className="form-control"
                          placeholder="Other Costs"
                          value="" />
                      </div>
                      <div className="form-group select-area-fields">
                        <label>Fees</label>
                        <input
                          type="text"
                          min='0'
                          className="form-control"
                          placeholder="Fees"
                          value="" />
                      </div>
                      <div className="form-group select-area-fields">
                        <label>In-Hand Date</label>
                        <div className="d-flex" >
                          <div className="date--PickQr">
                            <span className="custom-calendar-img">
                              <img src={iconInhand} />
                            </span>
                            <div>
                              <DatePicker
                                selectsRange={false}
                                selected={new Date(values.in_hand)}
                                className="dateSinglepicker"
                                onChange={(e) => this.viagogoChangeHandler(e, listingConst.in_hand)}
                                isClearable={new Date(values.in_hand) ? true : false}
                                onKeyDown={(e) => this.avoidOnchange(e)}
                                minDate={new Date(this.state.from_dateOnly)}
                                maxDate={new Date(this.state.to_dateOnly)}
                                dateFormat="dd/MM/yyyy"
                                placeholderText=" DD/MM/YYYY"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group select-area-fields">
                        <label>Delivery Status</label>
                        <Select
                          className={'formik-input-user-form create-select-box' + (errors.delivery_status_id && touched.delivery_status_id ? ' is-invalid' : '')}
                          placeholder={"Delivery Status"}
                          style={{ width: "100%" }}
                          name="delivery_status_id"
                          value={values.delivery_status_id ? values.delivery_status_id : []}
                          options={this.state.deliveryStatusList}
                          onChange={(e) => this.viagogoChangeHandler(e, 'delivery_status_id')}
                        />
                        <ErrorMessage name="split" component="div" className="invalid-feedback" />
                      </div>
                      <div className="form-group select-area-fields">
                        <label>Channel Reference ID</label>
                        <input
                          type="text"
                          min='0'
                          className="form-control"
                          placeholder="Channel Reference ID"
                          value="" />
                      </div>
                    </div>
                    <div className="form-group select-area-fields">
                      <label>Disclosure</label>
                      <MultiSelect
                        options={disclosureValues}
                        value={this.state.selectedViagogoDisclosures ? this.state.selectedViagogoDisclosures : []}
                        name="disclosure"
                        className="form-control" selectAllLabel='All Disclousres'
                        disabled="disabled"
                        labelledBy="Select Disclosures"
                      />
                    </div>
                    <div className="form-group select-area-fields">
                      <label>Tags</label>
                      <MultiSelect
                        options={tagValues}
                        value={this.state.selectedViagogoTags ? this.state.selectedViagogoTags : []}
                        name="Tags"
                        disabled="disabled"
                        className="form-control" selectAllLabel='Tags'
                        labelledBy="Select Tags"
                      />
                    </div>
                    <div className="form-group select-area-fields">
                      <label>Notes</label>
                      <textarea className="fomr-control" row="3">{values.internal_notes}</textarea>
                    </div>
                  </div>
                  <div className="row">
                    <div>
                      <button type="submit" className="btn btn-success rounded-pill">Submit</button>
                      <button type="button" onClick={(e) => this.setState({ showViagogoModal: false })} className="btn btn-primary rounded-pill">Cancel</button>
                    </div>
                  </div>
                </Form>
              )}
            />
          </Modal.Body>
          {/* <Modal.Footer className="modal-btn-align">
            <Button className="viagogo-orderSubmit-btn" >Submit</Button>
          </Modal.Footer> */}
        </Modal>

        <div className="loader-bg">
          <ClipLoader color={'#4a88c5'} loading={this.state.setLoader} size={40} />
        </div>

        <UserContext.Consumer>
          {context => (
            <div>
              <div className="myTicket-item">
                <div className="search-item">
                  <div className="search-form">
                    <form onSubmit={e => { e.preventDefault(); }}>
                      <div className="input-group form-group" style={{ position: 'relative' }}>
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <span className="ant-input-suffix">
                              <SearchIcon />
                            </span>
                          </span>
                        </div>
                        <input type="text" className="form-control" onChange={this.listSearchChange} placeholder="Search by Event ID, Performer, Venue, City, Tags" value={this.state.displayText} />
                        {suggestions && suggestions.length > 0 &&
                          < div className="TypeAheadDropDown">
                            {suggestions && this.state.locationText == '' &&
                              this.renderSuggestions()
                            }
                          </div>
                        }
                        {this.state.searchText &&
                          <button className="search-close-icon" onClick={(e) => this.listSearchChange()} >X</button>
                        }
                      </div>
                    </form>
                  </div>
                  <div className="date-range">
                    <div className="date-filter-itemQr">
                      <div className="form-group-gtrsd" style={{ 'position': 'relative' }}>
                        <form>
                          <div className="date-filter-top d-flex align-items-end">
                            <div className="d-flex" style={{
                              'marginBottom': '1rem'
                            }}>
                              <div className="date--PickQr">
                                <span className="custom-calendar-img">
                                  <img src={iconInhand} />
                                </span>
                                <div>
                                  <DatePicker
                                    selectsRange={true}
                                    startDate={this.state.startDate}
                                    endDate={this.state.endDate}
                                    onChangeRaw={(e) => this.avoidOnchange(e)}
                                    onChange={this.dateRangeDisplay}
                                    isClearable={true}
                                    dateFormat="dd/MM/yyyy"
                                    placeholderText=" DD/MM/YYYY - DD/MM/YYYY"
                                  />
                                </div>
                              </div>
                              <div className={this.state.advancefilter ? "filter-icon fill-color" : "filter-icon"} onClick={this.toggleAdvanced}>
                                <span className="line-filter-icon"><Filter /></span>
                                <span className="fill-filter-icon"><FilterFill /></span>
                              </div>
                            </div>
                            <div style={{
                              'width': '260px',
                              'marginLeft': 'auto',
                              'height': '50px',
                              'marginBottom': '1rem'
                            }}>
                              <Link to="/events/find" target="_blank">
                                <button type="button" className="btn btn-primary stylefont-weight-medium" >I Can&apos;t Find My Event</button>
                              </Link>
                              &emsp;
                            </div>
                            {

                              this.state.showPagination &&
                              <div classNmae="pagination-section">
                                <div className="pagination-box">
                                  <div className="d-flex align-items-center" style={{ height: '100%', gap: '20px', justifyContent: 'space-between' }}>


                                    <div key={'index1'} className="pagination-item">
                                      <button className="btn prev-btn stylefont-weight-medium" type="button" onClick={(e) => { this.previousPage(true); this.setState({ previousPageInActive: true }) }} disabled={this.state.previousPageInActive}> First</button>
                                    </div>
                                    <div key={'index'} >
                                      <button className="btn prev-btn-icon stylefont-weight-medium" onClick={(e) => { this.previousPage(false); this.setState({ previousPageInActive: true }) }} disabled={this.state.previousPageInActive} type="button">
                                        <img src={prevArrow} alt="prev-arrow" />
                                      </button>
                                    </div>
                                    <div className="pagination-number-item">
                                      {((this.state.paginationStates.currentPage - 1) * this.state.pageLimit) + 1}-
                                      {((this.state.paginationStates.currentPage - 1) * this.state.pageLimit) + this.state.rowCount}
                                    </div>
                                    <div key={'index2'} >
                                      <button type="button" className="btn next-btn-icon stylefont-weight-medium" disabled={this.state.nextPageInActive} onClick={(e) => { this.nextPage(false); this.setState({ nextPageInActive: true }) }} ><img src={nextArrow} alt="next-arrow" /></button>
                                    </div>
                                    <div key={'index3'} className="pagination-item">
                                      <button type="button" className="btn next-btn stylefont-weight-medium" disabled={this.state.nextPageInActive} onClick={(e) => { this.nextPage(true); this.setState({ nextPageInActive: true }) }} >Last</button>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            }


                          </div>
                          <div className={this.state.advancefilter ? "fliter-bottom listing-filter-item active" : "fliter-bottom listing-filter-item"} style={{ 'transition': '2s' }}>
                            <div className="close-filter">
                              <Button className="filter-close-btn" type="button" onClick={this.advanceFilterSubmit} ><CloseIcon /></Button>
                            </div>
                            <div className="row">
                              <div className="col-lg-7 col-md-12 col-12">
                                <div className="form-group">
                                  <label>Day of Week</label>
                                  <div className="form-day-week">
                                    <div className="day-checkbox-sec">
                                      <div className="form-check">
                                        <input type="checkbox" className="form-check-input" onChange={(e) => this.daySelected(e)} value="all" checked={this.state.selectedDays && this.state.selectedDays.length == 7 ? true : false} />
                                        <label className="form-check-label">Select All</label>
                                      </div>
                                      {
                                        weekDays.map((weekDay, index) => (
                                          <div key={index} className="form-check">
                                            <input type="checkbox" className="form-check-input" onChange={(e) => this.daySelected(e)} value={weekDay.value} checked={(this.state.selectedDays && this.state.selectedDays.length == 7 || this.state.selectedDays.includes(weekDay.value)) ? true : false} />
                                            <label className="form-check-label">{weekDay.label}</label>
                                          </div>
                                        ))
                                      }
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-5 col-md-12 col-12">
                                <div className="form-group">
                                  <label>Ticket Type</label>
                                  <div className="eticket-checkbox-sec">
                                    <div className="form-check">
                                      <input type="radio" className="form-check-input" name="ticketType" value="1" onClick={(e) => this.advanceFilter(e, listingConst.ticket_type)} checked={this.state.searchData.ticket_type == 1 ? true : false} />
                                      <label className="form-check-label">eTicket</label>
                                    </div>
                                    <div className="form-check">
                                      <input type="radio" className="form-check-input" name="ticketType" value="2" onClick={(e) => this.advanceFilter(e, listingConst.ticket_type)} checked={this.state.searchData.ticket_type == 2 ? true : false} />
                                      <label className="form-check-label">eTransfer</label>
                                    </div>
                                    <div className="form-check">
                                      <input type="radio" className="form-check-input" name="ticketType" value="3" onClick={(e) => this.advanceFilter(e, listingConst.ticket_type)} checked={this.state.searchData.ticket_type == 3 ? true : false} />
                                      <label className="form-check-label">Hard Stock</label>
                                    </div>
                                    <div className="form-check">
                                      <input type="radio" className="form-check-input" name="ticketType" value="4" onClick={(e) => this.advanceFilter(e, listingConst.ticket_type)} checked={this.state.searchData.ticket_type == 4 ? true : false} />
                                      <label className="form-check-label">Will Call</label>
                                    </div>
                                    <div className="form-check">
                                      <input type="radio" className="form-check-input" name="ticketType" value="5" onClick={(e) => this.advanceFilter(e, listingConst.ticket_type)} checked={this.state.searchData.ticket_type == 5 ? true : false} />
                                      <label className="form-check-label">Local Pickup</label>
                                    </div>
                                    <div className="form-check">
                                      <input type="radio" className="form-check-input" name="ticketType" value="6" onChange={(e) => this.advanceFilter(e, listingConst.ticket_type)} checked={this.state.searchData.ticket_type == 6 ? true : false} />
                                      <label className="form-check-label">QR Code</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-6 col-md-12 col-12">
                                <div className="form-group genre-dropdown gener-select-item">
                                  <label>Genre</label>


                                  <Select
                                    {...this.props}
                                    placeholder={"Select Genre"}
                                    filterOption={this.dropdownFilterOption}
                                    value={this.state.multiSelect.genre}
                                    options={genres}
                                    onChange={(e) => this.advanceFilter(e, listingConst.category, listingConst.genre)}
                                    allowClear
                                    showSearch
                                    isClearable
                                  />
                                </div>
                              </div>
                              <div className="col-lg-6 col-md-12 col-12">
                                <div className="section-row-item filter-grid-item-sec">
                                  <div className="form-group quantity-filter-item  input-close-item">
                                    <label>Quantity</label>
                                    <input
                                      type="number"
                                      min='0'
                                      className="form-control"
                                      onChange={(e) => this.advanceFilter(e, listingConst.quantity, listingConst.quantity)}
                                      placeholder="0"
                                      value={this.state.searchData.quantity} />
                                    {this.state.searchData[listingConst.quantity] != '' && !isNaN(this.state.searchData[listingConst.quantity]) && <span onClick={() => {
                                      let searchData = this.state.searchData;
                                      searchData[listingConst.quantity] = ''
                                      this.setState({ searchData })
                                    }} className="filter-close-btn">X</span>}
                                  </div>
                                  <div className="form-group section-filter-item section-input-item">
                                    <label>Section</label>
                                    <input className="form-control" type="text" placeholder="Section" onChange={(e) => this.advanceFilter(e, listingConst.section)} value={this.state.searchData.section} />
                                    {this.state.searchData[listingConst.section] && <span onClick={() => {
                                      let searchData = this.state.searchData;
                                      searchData[listingConst.section] = ''
                                      this.setState({ searchData })
                                    }} className="filter-close-btn">X</span>}
                                  </div>
                                  <div className="form-group row-filter-item ml-auto section-input-item">
                                    <label>Row</label>
                                    <input className="form-control" type="text" placeholder="Row" onChange={(e) => this.advanceFilter(e, listingConst.row)} value={this.state.searchData.row} />
                                    {this.state.searchData[listingConst.row] && <span onClick={() => {
                                      let searchData = this.state.searchData;
                                      searchData[listingConst.row] = ''
                                      this.setState({ searchData })
                                    }} className="filter-close-btn">X</span>}
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-6 col-md-12 col-12">
                                <div className="form-group filter-datepicker">
                                  <label>In-Hand Date</label>
                                  <div className="single-datepicker">
                                    <span className="custom-calendar-img">
                                      <img src={iconInhand} />
                                    </span>
                                    <DatePicker
                                      selectsRange={false}
                                      selected={this.state.singleDate}
                                      className="dateSinglepicker"
                                      onChange={(e) => this.advanceFilter(e, listingConst.in_hand)}
                                      isClearable={this.state.singleDate ? true : false}
                                      onKeyDown={(e) => this.avoidOnchange(e)}
                                      minDate={new Date(this.state.from_dateOnly)}
                                      maxDate={new Date(this.state.to_dateOnly)}
                                      dateFormat="dd/MM/yyyy"
                                      placeholderText=" DD/MM/YYYY"
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-6 col-md-12 col-12">
                                <div className="tag-grid-section">
                                  <div className="form-group tag-dropdown genre-dropdown gener-select-item">
                                    <label>Tags</label>
                                    <Select
                                      {...this.props}
                                      placeholder={"Select Tags"}
                                      style={{ width: "100%" }}
                                      name="tags"
                                      filterOption={this.dropdownFilterOption}
                                      value={lodashFilter(tagValues, tag => lodashIncludes(lodashGet(this.state, `searchData.${listingConst.tagIds}`, []), Number(tag.value)))}
                                      options={tagValues}
                                      onChange={(e) => this.advanceFilter(e, listingConst.tagIds)}
                                      // className={(errors.tags && touched.tags ? ' is-invalid' : '')}
                                      allowClear
                                      showSearch
                                      isClearable
                                      isMulti
                                    />
                                  </div>
                                  <div className="form-group tag-dropdown genre-dropdown gener-select-item">
                                    <label>Assigned to</label>
                                    {/* //TODO  Need clarification - 25-10-2021*/}
                                    <Select
                                      placeholder={"Select User"}
                                      filterOption={this.dropdownFilterOption}
                                      value={this.state.assignedTo && this.state.assignedTo.value ? [this.state.assignedTo] : []}
                                      options={allUsersList}
                                      // onChange={(e) => this.advanceFilter(e, listingConst.category, listingConst.genre)}
                                      onChange={(e) => this.setState({ assignedTo: e })}
                                      allowClear
                                      showSearch
                                      isClearable />
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-6 col-md-12 col-12">
                                <div className="d-flex inventory-hold-sec">
                                  <div className="form-group invendory-attach-item">
                                    <label>Inventory Attached</label>
                                    <div className="yes-no-checkbox">
                                      <div className="form-check">
                                        <input type="radio" name="inventory" className="form-check-input" onClick={(e) => this.advanceFilter(e, listingConst.inventory_attached, listingConst.inv_attach)} checked={this.state.searchData.inventory_attached && this.state.searchData.inventory_attached == listingConst.yesValue ? true : false} value={listingConst.yesValue} />
                                        <label className="form-check-label">Yes</label>
                                      </div>
                                      <div className="form-check">
                                        <input type="radio" name="inventory" className="form-check-input" onClick={(e) => this.advanceFilter(e, listingConst.inventory_attached, listingConst.inv_attach)} checked={this.state.searchData.inventory_attached && this.state.searchData.inventory_attached == listingConst.noValue ? true : false} value={listingConst.noValue} />
                                        <label className="form-check-label">No</label>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="form-group hold-filter-item ml-auto">
                                    <label>Hold</label>
                                    <div className="yes-no-checkbox">
                                      <div className="form-check">
                                        <input type="radio" name="hold" className="form-check-input" onClick={(e) => this.advanceFilter(e, listingConst.hold)} checked={this.state.searchData.hold == listingConst.yesValue ? true : ''} value={listingConst.yesValue} />
                                        <label className="form-check-label">Yes</label>
                                      </div>
                                      <div className="form-check">
                                        <input type="radio" name="hold" className="form-check-input" onClick={(e) => this.advanceFilter(e, listingConst.hold)} checked={this.state.searchData.hold == listingConst.noValue ? true : ""} value={listingConst.noValue} />
                                        <label className="form-check-label">No</label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-6 col-md-12 col-12">
                                <div className="form-group invendory-attach-item complete-event-section">
                                  <div className="form-check-radio">
                                    <input type="radio" name="inventory" className="form-control" onClick={(e) => this.advanceFilter(e, listingConst.completedEvents)} checked={this.state.searchData.completedEvents} />
                                    <label>Show All Completed Events</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="submit-reset-btn-item">
                              <button className={this.state.searchData.isSearchEnable ? "btn filter-submit-btn primary-bg-color active" : 'btn filter-submit-btn primary-bg-color'} type="button" onClick={this.advanceFilterSubmit}>Submit</button>
                              <a type="button" onClick={this.advanceFilterReset} className="btn filter-reset-btn">Reset</a>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>


                  </div>
                  <div className="ticket-viewitem">


                    {
                      this.state.filterListingsData && this.state.filterListingsData.length > 0 ? this.state.filterListingsData.map((row) => (
                        <div className="card" key={'card_' + row.event_id}>
                          <div className="card-body p-0">
                            <div>
                              <div className="ava-ticket-detail tic-detail-sec">
                                <div className="check_ticket">
                                  {/* <div className="left-radio-btn">
                                    <form>
                                      <div className="form-group checkbox-item">
                                        <input className="styled-checkbox" onChange={(e) => this.handleEventChange(e)} id={'checkbox_' + row.event_id} type="checkbox" value={row.event_id} checked={this.state.eventIds.includes(row.event_id.toString())} />
                                        <label htmlFor={'checkbox_' + row.event_id}></label>
                                      </div>
                                    </form>
                                  </div> */}
                                  <div className="ticket-details-item">
                                    <div className="ticket-header">
                                      <div className="d-flex align-items-start" onClick={() => this.toggleListing(row.event_id)}>
                                        <div className="left-header secondary-font-family" onClick={() => this.goToAddListing(row.event_id)}><h1>{row.eventname}</h1>
                                        </div>
                                        <div className="ml-auto event-id-item" ><h5 className="event-id-number stylefont-weight-medium"  >Event ID: {row.event_id}</h5></div>
                                      </div>
                                      <div className="place-date" >
                                        <h1 className="date--time  primary-color stylefont-weight-medium">
                                          {/* {
                                            moment.utc(row.eventdate).local().format('dddd - MMM Do YYYY, hh:mm A') + ' '
                                          }
                                          {moment && moment.tz.guess() && moment.tz.guess().length && moment.tz(moment.tz.guess()).zoneAbbr().length ? moment.tz(moment.tz.guess()).zoneAbbr() : ''} */}
                                          {row.timezone && row.timezone[0] ? moment.utc(row.eventdate).tz(row.timezone[0]).format('dddd MMM D, YYYY @ h:mm A z') + ' ' : moment.utc(row.eventdate).format('dddd MMM D, YYYY @ h:mm A z') + ' '}
                                        </h1>
                                        <h5 className="loaction-text stylefont-weight-bold secondary-color">
                                          {row.venuename + ((row.venuename != "") ? ', ' : '') + row.city + ', ' + row.state + ', ' + row.country}</h5>
                                      </div>
                                    </div>
                                    <div className="ava--detail">
                                      <div className="ticket--item" onClick={() => this.toggleListing(row.event_id)}>
                                        <div className="tickets-detail-grid">
                                          <div>
                                            <div className="static-detail">
                                              <h6 className="stylefont-weight-medium">Available</h6>
                                            </div>
                                            <div className="dynamic-detail"><span className="availableBtn stylefont-weight-semibold">{row.totalavailabletkts ? row.totalavailabletkts : 0}</span></div>
                                          </div>
                                          <div>
                                            <div className="static-detail">
                                              <h6 className="stylefont-weight-medium">Tickets Sold</h6>
                                            </div>
                                            <div className="dynamic-detail"><span className="ticketSold stylefont-weight-semibold">{row.totalsoldtkts ? row.totalsoldtkts : 0}</span></div>
                                          </div>
                                          <div>
                                            <div className="static-detail">
                                              <h6 className="stylefont-weight-medium">Days to Event</h6>
                                            </div>
                                            <div className="dynamic-detail"><span className="sevenDays stylefont-weight-semibold">{row.totalsoldtktsinlast7days ? row.totalsoldtktsinlast7days : 0}</span></div>
                                          </div>
                                          <div>
                                            <div className="static-detail">
                                              <h6 className="stylefont-weight-medium">Urgent Delivery</h6>
                                            </div>
                                            <div className="dynamic-detail"><span className="daysEvent stylefont-weight-semibold">{row.awaitingdelivery ? row.awaitingdelivery : 0}</span></div>
                                          </div>
                                          <div>
                                            <div className="static-detail">
                                              <h6 className="stylefont-weight-medium">Velocity</h6>
                                            </div>
                                            <div className="dynamic-detail"><span className="awaitDelivery stylefont-weight-semibold">{row.daystoevent > 0 ? row.daystoevent : 0}</span></div>
                                          </div>
                                        </div>
                                      </div>

                                      <div className="right-side-icon">
                                        <ul className="icon-Unlist">
                                          <li className="listOfIcon disclosure-icon-img" onClick={() => this.disclosuresListing(0, 'event', row.event_id, (row.listingDtls.length > 0 ? true : false))}>
                                            <Tooltip className="listicon-tooltip" placement="top" title={disclosuresTip}>
                                              <img src={iconDisclosures} />
                                            </Tooltip>

                                          </li>
                                          <li className="listOfIcon inhand-icon-img" onClick={() => this.inHandDateListing(row.event_id, row.eventdate, (row.listingDtls.length > 0 ? true : false))} >
                                            <Tooltip className="listicon-tooltip" placement="top" title={inHandDateTip}>
                                              <img src={iconInhand} />
                                            </Tooltip>
                                          </li>
                                          <li className="listOfIcon splits-icon-img" onClick={() => this.splitsListing(0, 'event', row.event_id, (row.listingDtls.length > 0 ? true : false))}>
                                            <Tooltip className="listicon-tooltip" placement="top" title={splitsTip}>
                                              <img src={iconSplits} />
                                            </Tooltip>
                                          </li>
                                          <li className="listOfIcon purchase-icon-img">
                                            <Link to="/purchases" target="_blank">
                                              <Tooltip className="listicon-tooltip" placement="top" title={purchaseOrdersTip}>
                                                <img src={iconPurchaseOrder} />
                                              </Tooltip>
                                            </Link>
                                          </li>
                                          <li className="listOfIcon price-icon-img" onClick={() => this.eventLevelPrice(row.event_id, row.totalavailabletkts, row)}>
                                            <Tooltip className="listicon-tooltip" placement="top" title={priceEventTip}>
                                              <img src={iconPrice} />
                                            </Tooltip>
                                          </li>

                                          {row.totalseats != row.totalsoldtkts ? (row.inactive[0] ?
                                            (
                                              <li className="listOfIcon share-icon-img" onClick={() => this.shareListing(0, 'share', row.event_id, 'event', (row.listingDtls.length > 0 ? true : false))}>
                                                <Tooltip className="listicon-tooltip" placement="top" title={shareTip}>
                                                  <img src={iconShare} />
                                                </Tooltip>
                                              </li>
                                            ) :
                                            (
                                              <li className="listOfIcon share-icon-img" onClick={() => this.shareListing(0, 'pauseShare', row.event_id, 'event', (row.listingDtls.length > 0 ? true : false))}>
                                                <Tooltip className="listicon-tooltip" placement="top" title={unShareTip}>
                                                  <img src={iconUnShare} />
                                                </Tooltip>
                                              </li>
                                            )) :
                                            (
                                              <li className="listOfIcon share-icon-img" >
                                                <Tooltip className="listicon-tooltip" placement="top" title={shareTip}>
                                                  <img src={iconShare} />
                                                </Tooltip>
                                              </li>
                                            )

                                          }
                                          <li className="listOfIcon">

                                            <Link target="_blank" to={`/add-listing/${row.event_id}`}>
                                              <Tooltip className="listicon-tooltip add-listing-icon" placement="top" title={addListingTip}>
                                                <img src={iconAddListing} />
                                              </Tooltip>
                                            </Link>
                                          </li>
                                        </ul>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="ava-ticket-list hide-event-listing" id={`event-listing-mini-table-${row.event_id}`}>
                                <div className="seat_information-item table-responsive-sec">
                                  <table className="table">
                                    <thead>
                                      <tr>
                                        <th style={{ width: '80px', textAlign: 'center' }}>
                                          <span onClick={() => this.handleSortModal(true, row.event_id)} ><FilterSort /></span>
                                        </th>
                                        <th style={{ width: '80px', textAlign: 'center' }}>Sec</th>
                                        <th style={{ width: '100px' }}>Row</th>
                                        <th style={{ width: '100px' }}>Seat</th>
                                        <th style={{ width: '100px' }}>Current Price</th>
                                        <th style={{ width: '100px' }}>Last Price Update</th>
                                        <th style={{ width: '100px' }}>Face Value</th>
                                        <th style={{ width: '100px' }}>Cost</th>
                                        <th style={{ width: '100px' }}>Margin</th>
                                        <th style={{ width: '100px' }}>Available Sold</th>
                                        <th style={{ width: '100px' }}>Delivery Type</th>
                                        <th style={{ width: '250px' }}>Listing Settings</th>
                                        <th style={{ width: '250px' }}>Tags</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {row.filteredListingDetails ? this.renderListingDiv(days, monthNames, row.event_id, row.filteredListingDetails) : this.renderListingDiv(days, monthNames, row.event_id, row.listingDtls)}
                                    </tbody>
                                  </table>
                                </div>
                              </div>

                            </div>

                          </div>
                        </div>)) : <div className="card">
                        <div className="card-body">
                          <div>

                            <div className="ava-ticket-detail">
                              <div className="check_ticket">
                                <h4 className="no-date-text stylefont-weight-medium">No Data Found.</h4>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    }
                    {/* {this.state.filterListingsData && this.state.filterListingsData.length > 0 && (
                      <div className="d-flex flex-row align-items-center">
                        <div className="bottom-pagination-item">
                          <Pagination totalRecords={this.state.eventCount} eventCount={this.state.filterListingsData.length} pageLimit={this.state.pageLimit} pageNeighbours={0} onPageChanged={this.onPageChanged.bind(this)} firstEventId = {this.state.filterListingsData[0].event_id} lastEventId = { this.state.filterListingsData[this.state.filterListingsData.length - 1].event_id} />
                        </div>
                      </div>
                    )} */}
                    {

                      this.state.showPagination &&
                      <div classNmae="pagination-section">
                        <div className="pagination-box">
                          <div className="d-flex align-items-center" style={{ height: '100%', gap: '20px', justifyContent: 'space-between' }}>


                            <div key={'index1'} className="pagination-item">
                              <button className="btn prev-btn stylefont-weight-medium" type="button" onClick={(e) => { this.previousPage(true); this.setState({ previousPageInActive: true }) }} disabled={this.state.previousPageInActive}> First</button>
                            </div>
                            <div key={'index'} >
                              <button className="btn prev-btn-icon stylefont-weight-medium" onClick={(e) => { this.previousPage(false); this.setState({ previousPageInActive: true }) }} disabled={this.state.previousPageInActive} type="button">
                                <img src={prevArrow} alt="prev-arrow" />
                              </button>
                            </div>
                            <div className="pagination-number-item">
                              {((this.state.paginationStates.currentPage - 1) * this.state.pageLimit) + 1}-
                              {((this.state.paginationStates.currentPage - 1) * this.state.pageLimit) + this.state.rowCount}
                            </div>
                            <div key={'index2'} >
                              <button type="button" className="btn next-btn-icon stylefont-weight-medium" disabled={this.state.nextPageInActive} onClick={(e) => { this.nextPage(false); this.setState({ previousPageInActive: true }) }} ><img src={nextArrow} alt="next-arrow" /></button>
                            </div>
                            <div key={'index3'} className="pagination-item">
                              <button type="button" className="btn next-btn stylefont-weight-medium" disabled={this.state.nextPageInActive} onClick={(e) => { this.nextPage(true); this.setState({ nextPageInActive: true }) }} >Last</button>
                            </div>
                          </div>
                        </div>
                      </div>

                    }
                  </div>
                </div>
              </div>
            </div>
          )}
        </UserContext.Consumer>

        {
          visible ? <div ref={ref => { this.root = ref }} className="contextMenu">
            {this.state.listingGroupIds.length <= 1 && (
              <Link to={'/view-listing/' + this.state.listIdRC} target='_blank' className='noDecoration'>
                <div className="contextMenu--option">
                  Listing Details
                </div>
              </Link>
            )}
            {this.state.listingGroupIds.length <= 1 && this.state.isFlagged && (this.state.poid_link != 0 || this.state.poid_link != '') && (
              <Link to={'/purchase/purchase-detail/' + this.state.poid_link} target='_blank' className='noDecoration'>
                <div className="contextMenu--option">
                  Purchase Order Detail
                </div>
              </Link>
            )}
            {(((this.state.listingSold && this.state.listingGroupIds.length < 1) || (this.state.showPriceMenu && this.state.listingGroupIds.length)) ?
              <div className="contextMenu--option ctxtm-disabled" disabled={true}>Price</div> :
              <div className="contextMenu--option" onClick={(e) => this.multiPriceModal(this.state.listIdRC, this.state.listEventIdRC)}>Price</div>
            )}           

            {(this.state.isMultipleFlag == false && this.state.isFlagged == false) &&
              (
                (!this.state.listingSold && !this.state.listInActive)
                  ?
                  <div className={"contextMenu--option"} onClick={(e) => this.markAsSold(this.state.listIdRC, this.state.listingStatusViagogoHold)} disabled={this.state.listingSold}>Mark as sold</div>
                  :
                  <div className={"contextMenu--option ctxtm-disabled"} disabled={this.state.listingSold}>Mark as sold</div>
              )
            }


            {this.state.listingGroupIds.length <= 1 && (
              this.state.listingSold ?
                <div className="contextMenu--option ctxtm-disabled" disabled={this.state.listingSold} > {this.state.listInActive ? 'Active' : 'Inactive'}</div>
                :
                <div onClick={(e) => this.changeListingGroupStatus(this.state.listIdRC)} className="contextMenu--option" > {this.state.listInActive ? 'Active' : 'Inactive'}</div>
            )}
            {this.state.listingGroupIds.length > 1 && this.state.listRowActiveInactive && this.state.listRowActiveInactive[0].displayOption == true && !this.state.listingSold && (
              <div onClick={(e) => this.changeListingGroupStatus(this.state.listIdRC)} className="contextMenu--option">{this.state.listRowActiveInactive[0].type ? 'Active' : 'Inactive'}</div>
            )}


            {this.state.listingGroupIds.length <= 1 && !this.state.isFlagged ? (<div className="contextMenu--option" onClick={(e) => this.setState({ removeListingModal: true })} >Remove Listings</div>) : (<div className={"contextMenu--option ctxtm-disabled"} disabled={true}  >Remove Listings</div>)}

            {!this.state.isFlagged && !this.state.isMultipleFlag && (
              <div className="contextMenu--option" onClick={(e) => this.changeFlag(this.state.listingGroupIds && this.state.listingGroupIds.length > 0 ? this.state.listingGroupIds : [this.state.listIdRC], this.state.selectedListingEventId, 1)} >
                Flag
              </div>
            )}

          </div> : ''
        }

      </div >
    ) : (
      <Loader />
    );
  }
}

export default withTranslation()(MyListings, document.getElementsByClassName('listingContextMenu'));
