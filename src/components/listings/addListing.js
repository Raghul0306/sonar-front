import React, { Component } from "react";
// import loadable from "@loadable/component";
import moment from 'moment-timezone'
import UserContext from "../../contexts/user.jsx";
import { Row, Col, FormInput } from "shards-react";
import Button from 'antd/es/button';
import Modal from 'antd/es/modal';
import Switch from 'antd/es/switch';

import Select from 'react-select'
import { withTranslation } from "react-i18next";
import Loader from "../Loader/Loader";
import "../../assets/formik-form-style.css";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';
import ClipLoader from "react-spinners/ClipLoader";
import { Confirm, Info } from "../../components/modal/common-modal";
import {
  getAddListingsFilter, getAllTicketTypes, allGenreDetails, getUserFilterListings, getAllEventsArrayList, eventSearches, getAllTagList, getAllTicketingSystemList, getAllDisclosureList, getAllAttributeList, getAllSeatTypesList, allTicketingSystemAccounts, getAllChannels, createListing, listingattachment, Createattribute, Createdisclosure, Createticketingsystem, Createtag, getAllSplits, getEventDetailsBasedOnEventId, createTicketingSystemAccount, getRecentListingDetailsBasedOnEventId
} from "../../services/listingsService.js";
import { getCountries } from "../../services/userService";
import { listingConst, preselectedSeat, preselectedHideSeatNumber, preselectedSplit, monthNamesShort, daysFull, seatTypes, responseStatus, listingGroupInfo } from "../../constants/constants";
import { 
  // Redirect, 
  withRouter } from 'react-router-dom'
//Design Starts

import styled from 'styled-components'
import { FilterCircle } from '@styled-icons/bootstrap/FilterCircle'
import { Search } from '@styled-icons/bootstrap/Search'
import { Speakerphone } from '@styled-icons/heroicons-outline/Speakerphone'
import { Calendar } from '@styled-icons/boxicons-regular/Calendar'
import { Notepad } from '@styled-icons/boxicons-regular/Notepad'
import { DollarCircle } from '@styled-icons/boxicons-regular/DollarCircle'
import { PauseCircle } from '@styled-icons/boxicons-regular/PauseCircle'
import { AddCircle } from '@styled-icons/fluentui-system-regular/AddCircle'
import { Directions } from '@styled-icons/boxicons-regular/Directions'
import { Edit } from '@styled-icons/boxicons-solid/Edit'
import { FileUpload } from '@styled-icons/material-outlined/FileUpload';
import { Add } from '@styled-icons/material-rounded/Add';
import { Minus } from '@styled-icons/heroicons-solid/Minus';
import { Attachment } from '@styled-icons/entypo/Attachment';

//React date picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
//React date picker
// Multi select for day of week
// import MultiSelect from "react-multi-select-component";
//import { Multiselect } from 'multiselect-react-dropdown';


// icon images import
import iconCalendar from "../../assets/myTicketsIconCalendar.png";

const { Option } = Select;

const Filter = styled(FilterCircle)`
  color: #4a88c5;
  width:40px;
  height:40px;
`
const SearchIcon = styled(Search)`
  color: #9f9f9f;
  width:20px;
  height:20px;
`
const SpeakerIcon = styled(Speakerphone)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const CalendarIcon = styled(Calendar)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const NotePadIcon = styled(Notepad)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const DollarCircleIcon = styled(DollarCircle)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const PauseCircleIcon = styled(PauseCircle)`
color: #9f9f9f;
width:30px;
height:30px;
`
const AddCircleIcon = styled(AddCircle)`
color: #9f9f9f;
width:30px;
height:30px;
`
const DirectionsIcon = styled(Directions)`
color: #9f9f9f;
width:30px;
height:30px;
`
const EditIcon = styled(Edit)`
color: #9f9f9f;
width:30px;
height:30px;
`
const UploadIcon = styled(FileUpload)`
color: #9f9f9f;
width:30px;
height:30px;
`
const AddIcon = styled(Add)`
color: #2C91EE;
width:30px;
height:30px;
`
const MinusIcon = styled(Minus)`
color: #BEBEBE;
width:30px;
height:30px;
`
const AttachmentIcon = styled(Attachment)`
color: #ffffff;
width:23px;
height:25px;
`

//Design Ends

export class MyAddListingForms extends Component {
  static contextType = UserContext;
  state = {
    loading: true,
    event_id: '',
    rows: [],
    suggestions: [],
    searchText: '',
    inputList: [],
    channelManagmentInputList: [],
    eventDetails: [],
    selectedEventMaxDate: '',
    attachmentInputList: [],
    attributesInputList: [],
    disclosuresInputList: [],
    createdListingGroupIds: [],
    addPurchaseOrder: false
  };

  constructor(props) {
    super(props)
    this.listSearchChange = this.listSearchChange.bind(this);
    this.selectLisitng = this.selectLisitng.bind(this);
    this.toggleAdvanced = this.toggleAdvanced.bind(this);
    this.dateRangeDisplay = this.dateRangeDisplay.bind(this);
    this.advanceFilter = this.advanceFilter.bind(this);
    this.daySelected = this.daySelected.bind(this);
    this.handleAddClick = this.handleAddClick.bind(this);
    this.handleRemoveClick = this.handleRemoveClick.bind(this);
    this.cancelAddListingOnclick = this.cancelAddListingOnclick.bind(this);
    this.appendedFormSectionOnchange = this.appendedFormSectionOnchange.bind(this);
    this.appendedFormRowOnchange = this.appendedFormRowOnchange.bind(this);
    this.appendedFormSeatStartOnchange = this.appendedFormSeatStartOnchange.bind(this);
    this.appendedFormSeatEndOnchange = this.appendedFormSeatEndOnchange.bind(this);
    this.appendedFormPriceOnchange = this.appendedFormPriceOnchange.bind(this);
    this.appendedFormFaceValueOnchange = this.appendedFormFaceValueOnchange.bind(this);
    this.appendedFormCostOnchange = this.appendedFormCostOnchange.bind(this);
    this.appendedFormUnitCostOnchange = this.appendedFormUnitCostOnchange.bind(this);
    this.appendedFormDisclosureOnchange = this.appendedFormDisclosureOnchange.bind(this);
    this.appendedFormAttributeOnchange = this.appendedFormAttributeOnchange.bind(this);
    this.appendedFormPOIDOnchange = this.appendedFormPOIDOnchange.bind(this);
    this.appendedFormTagsOnchange = this.appendedFormTagsOnchange.bind(this);
    this.appendedFormInternalNotesOnchange = this.appendedFormInternalNotesOnchange.bind(this);
    this.appendedFormExternalNotesOnchange = this.appendedFormExternalNotesOnchange.bind(this);
    this.channelManagmentInputarray = this.channelManagmentInputarray.bind(this);
    this.channelManagmentOnchange = this.channelManagmentOnchange.bind(this);
    this.channelManagmentIncressOnchange = this.channelManagmentIncressOnchange.bind(this);
    this.channelManagmentDecressOnchange = this.channelManagmentDecressOnchange.bind(this);
    this.channelManagmentToggleOnchange = this.channelManagmentToggleOnchange.bind(this);
    this.appendedFormAttachmentOnchange = this.appendedFormAttachmentOnchange.bind(this);
    this.appendedFormAttachmentTextOnchange = this.appendedFormAttachmentTextOnchange.bind(this);
    this.AddDisclosureOnclick = this.AddDisclosureOnclick.bind(this);
    this.AddTicketingSystemAccountOnclick = this.AddTicketingSystemAccountOnclick.bind(this);
    this.AddAttributeOnclick = this.AddAttributeOnclick.bind(this);
    this.AddTagOnclick = this.AddTagOnclick.bind(this);
    this.singleSelect = this.singleSelect.bind(this);
    this.unitCostCalculator = this.unitCostCalculator.bind(this);
    this.avoidOnchange = this.avoidOnchange.bind(this);
    this.formValidationExternal = this.formValidationExternal.bind(this);
    this.disableAddlistingButton = this.disableAddlistingButton.bind(this);
    this.hideSeatNumber = this.hideSeatNumber.bind(this);

    this.AddTicketingSystemOnclick = this.AddTicketingSystemOnclick.bind(this);
    this.AppendModelSubmitOnclick = this.AppendModelSubmitOnclick.bind(this);
    this.AppendModelCancelOnclick = this.AppendModelCancelOnclick.bind(this);
    this.handlesubmitButtonModal = this.handlesubmitButtonModal.bind(this);
    this.handleInfoSubmitButtonModal = this.handleInfoSubmitButtonModal.bind(this);
    this.setQuantity = this.setQuantity.bind(this);
    this.setAttachmentsForGA = this.setAttachmentsForGA.bind(this);
    this.dropdownFilterOption = this.dropdownFilterOption.bind(this);
    this.toggleSwitch = this.toggleSwitch.bind(this);
    this.removeAttachment = this.removeAttachment.bind(this);

    this.state = {
      listingsData: [],
      filterListingsData: [],
      searchText: '',
      displayText: '',
      startDate: null,
      endDate: null,
      inhand_date: '',
      seat_type_value: preselectedSeat.value,
      advancefilter: false,
      quantities: [],
      avoidMultiEmptyCall: false,
      selectedDays: [],
      quantity_array: [],
      quantity: '0',
      quantity_ga: "",
      singleDate: '',
      searchData: {
        searches: "",
        tags: "",
        section: "",
        row: "",
        quantity: '',
        from_date: "",
        to_date: "",
        day_of_week: "",
        in_hand: "",
        inventory_attached: "",
        ticket_type: "",
        category: ""
      },
      inputList: [{
        section: "",
        row: "",
        seat_start: "",
        seat_end: "",
        price: "0",
        face_value: "0",
        cost: "1",
        unit_cost: "0",
        disclosure: "",
        attribute: "",
        po_id: "",
        tags: "",
        internal_notes: "",
        external_notes: "",
        attachments: [],
        userId: ""
      }],
      eventDetails: [],
      channelManagmentInputList: [],
      attachmentInputList: [],
      inputListValidation: [],
      isAppendModelPopup: {
        'isModalVisible': false,
        'title': '',
        'type': ''
      },
      singleSelect: {
        ticket_type_id: [],
        seat_type_id: [preselectedSeat],
        ticketing_system: [],
        ticketing_system_accounts: [],
        hide_seat_number: preselectedHideSeatNumber[0],
        splitId: [preselectedSplit]

      },
      singleinputList: [{
        seat_start: [],
        seat_end: [],
      }],
      channelManage: [],
      setLoader: false, //loader,
      disable_addlist: false,

      InfoSubmitButtonModal: false,
      infocloseButtonModal: false,
      closeButtonModal: false,
      submitButtonModal: false,
      datafields: "",
      infomodalmsg: "",
      Is_show_localpickup: false,
      disableTicketingAccount: true,
      Is_show_sectiondiv: true,
      addPurchaseOrder: false,
      createdListingGroupIds: [],
      addGroupButtonId: 0,
      responseModalMessage: '',
      statusModalMessage: false,
      newAttribute: '',
      listingDetailsBasedOnEventId: [],
      removeNewGroup: false,

    }
  }
  handlesubmitButtonModal = async (modalpopupstatus) => {

    if (modalpopupstatus) {
      var listdata = this.state.datafields;
      this.setMyPageLoader(true);
      this.disableAddlistingButton(true);
      var createListingresponse = await createListing(listdata);
      if (!createListingresponse.isError) {
        if (createListingresponse.attachmentData) {
          const attachment_response = await listingattachment(listdata.seatInformationsJSON, createListingresponse.attachmentData); // s3 bugket
          if (attachment_response) {
            if (this.state.addPurchaseOrder) {
              if (createListingresponse.listingGroupIds && createListingresponse.listingGroupIds.length) {
                this.setMyPageLoader(false);
                this.setState({ createdListingGroupIds: createListingresponse.listingGroupIds, infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: createListingresponse.msg });
              } else {
                this.setMyPageLoader(false);
                await this.setState({ infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: createListingresponse.msg });
                return;
              }
            } else {
              this.setMyPageLoader(false);
              await this.setState({ infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: createListingresponse.msg });
              return;
            }
          }
          else {  // file attachment message
            this.setMyPageLoader(false);
            await this.setState({ infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: listingConst.add_attachment_error });
            return;
          }
        }
        else {
          if (this.state.addPurchaseOrder) {
            if (createListingresponse.listingGroupIds && createListingresponse.listingGroupIds.length) {
              this.setMyPageLoader(false);
              this.setState({ createdListingGroupIds: createListingresponse.listingGroupIds, infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: createListingresponse.msg });
            } else {
              this.setMyPageLoader(false);
              await this.setState({ infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: createListingresponse.msg });
              return;
            }
          } else {
            this.setMyPageLoader(false);
            await this.setState({ infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: listingConst.add_attachment_error });
            return;
          }
        }
      } else {
        this.setMyPageLoader(false);
        await this.setState({ infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: listingConst.add_attachment_error });
        return;
      }
    } else {
      await this.setState({ closeButtonModal: false });

    }

  }

  handleInfoSubmitButtonModal = async (modalpopupstatus) => {
    await this.setState({
      InfoSubmitButtonModal: true,
      infocloseButtonModal: false,
    });

    if (this.state.addPurchaseOrder && this.state.createdListingGroupIds && this.state.createdListingGroupIds.length) {

      this.props.history.push({
        pathname: '/purchase/create-order/' + this.state.createdListingGroupIds.toString(),
        search: this.state.createdListingGroupIds.toString()
      })
    } else {
      this.props.history.push("/listings", { state: this.state.createdListingGroupIds });
    }

  }

  async componentDidMount() {
    this.setMyPageLoader(true);
    this.props.history.push({ pageTitle: this.props.t("addlisting.title"), pageSubTitle: this.props.t("title"), redirectPath: "listings" });
    const userId = this.context.user.id;
    let quantityArray = []
    for (var quantityCount = 0; quantityCount < 100; quantityCount++) {  // Dynamic quantity 1-100
      quantityArray[quantityCount] = quantityCount + 1;
    }

    //Row Array Start
    let rowArray = [];
    for (var rowCount = 0; rowCount < 100; rowCount++) {  // Dynamic quantity 1-100
      rowArray[rowCount] = rowCount + 1;
    }
    //Row Array End

    let userFilterListingsData;
    let ticketTypeDetails;
    let getAllEventsArrayLists;
    let genreDetails;
    let getTags;
    let getTicketingSystems;
    let getDisclosures;
    let getAttributes;
    let getTicketingSystemAccounts = [];
    let getChannels;
    let getAllSeatTypes;
    let getCountryList;
    let getSplitsList;

    //Event details
    let eventIds = this.props.eventId;
    let newEventIdsArr = eventIds.split(',');
    if (newEventIdsArr.length > 0) {
      var event_details = await getEventDetailsBasedOnEventId(eventIds);
      if (event_details && event_details.length > 0) {
        this.setState({ eventDetails: event_details, selectedEventMaxDate: event_details[0].eventdate })
      }
      var listingDetails = await getRecentListingDetailsBasedOnEventId(newEventIdsArr[0])
      if (listingDetails && listingDetails.length > 0) {
        listingDetails = listingDetails[0];
        let setAutoFillListing = {};
        if (listingDetails.seat_type_id) {
          setAutoFillListing.seat_type_value = listingDetails.seat_type_id;
        }
        if (listingDetails.in_hand != null) {
          var inHandDate = listingDetails.in_hand.split('T');
          if (inHandDate[0]) { listingDetails.inHandDate = new Date(inHandDate); } else {
            listingDetails.inHandDate = "";
          }
        }
        if (listingDetails.tickettype_id && listingDetails.tickettype_id == listingConst.localpickup_id) {
          setAutoFillListing.Is_show_localpickup = true;
        }
        setAutoFillListing.listingDetailsBasedOnEventId = listingDetails;
        setAutoFillListing.singleDate = listingDetails.inHandDate;
        setAutoFillListing.inhand_date = listingDetails.inHandDate

        await this.setState(setAutoFillListing)
      }
    }

    // this.setState({ loading: false });
    ticketTypeDetails = await getAllTicketTypes();
    genreDetails = await allGenreDetails();
    userFilterListingsData = await getAddListingsFilter();
    getAllEventsArrayLists = await getAllEventsArrayList();
    getTags = await getAllTagList(userId);
    getTicketingSystems = await getAllTicketingSystemList();
    getDisclosures = await getAllDisclosureList(userId);
    getAttributes = await getAllAttributeList(userId);
    //getTicketingSystemAccounts = await allTicketingSystemAccounts(userId, this.context.user.id);
    getChannels = await getAllChannels();
    getChannels.sort(function (x, y) { return x.channel_name == listingConst.yadara ? -1 : y.channel_name == listingConst.yadara ? 1 : 0; });
    getAllSeatTypes = await getAllSeatTypesList();
    getCountryList = await getCountries();
    getSplitsList = await getAllSplits();

    if (this.state.listingDetailsBasedOnEventId) {
      var ticketTypeDetail = [];
      var splitDetails = [];
      var seatType = [];
      var ticketSystem = [];
      var hide_seat_numbers = preselectedHideSeatNumber[0];

      if (ticketTypeDetails && ticketTypeDetails.length) {
        var ticket_type_id = ticketTypeDetails.filter((ticketType) => ticketType.id == this.state.listingDetailsBasedOnEventId.tickettype_id);

        ticket_type_id && ticket_type_id.length && ticket_type_id.map(tickets =>
          ticketTypeDetail.push({ label: tickets.name, value: tickets.id })
        )
      }

      if (getSplitsList && getSplitsList.length) {
        var split_type_id = getSplitsList.filter((split) => split.id == this.state.listingDetailsBasedOnEventId.splits_id);


        split_type_id && split_type_id.length && split_type_id.map(splits =>
          splitDetails.push({ label: splits.name, value: splits.id },)
        )
        if (!splitDetails.length) {
          splitDetails = [preselectedSplit]
        }
      }

      if (getAllSeatTypes && getAllSeatTypes.length) {
        var seat_type_id = getAllSeatTypes.filter((seatType) => seatType.id == this.state.listingDetailsBasedOnEventId.seat_type_id);

        seat_type_id && seat_type_id.length > 0 && seat_type_id.map(seatTypes => seatType.push({ label: seatTypes.name, value: seatTypes.id }))
        if (!seatType.length) {
          seatType = [preselectedSeat]
        } else {
          if (seatType && seatType.length > 0 && seatType[0] && seatType[0].value === seatTypes.generalAdmission.id) {
            this.setState({ Is_show_sectiondiv: false });
          } else {
            this.setState({ Is_show_sectiondiv: true });
          }
        }
      }

      if (getTicketingSystems && getTicketingSystems.length) {
        var ticket_system_id = getTicketingSystems.filter((ticketingSystem) => ticketingSystem.id == this.state.listingDetailsBasedOnEventId.ticket_system_id);

        ticket_system_id && ticket_system_id.length && ticket_system_id.map(ticSystem => ticketSystem.push({ label: ticSystem.ticketing_system_name, value: ticSystem.id }))

        if (ticketSystem && ticketSystem.length) {
          getTicketingSystemAccounts = await allTicketingSystemAccounts(ticketSystem[0].value, this.context.user.id);
          this.setState({ disableTicketingAccount: false });
        }
        else {
          this.setState({ disableTicketingAccount: true });
        }

      }

      if (this.state.listingDetailsBasedOnEventId.hide_seat_numbers) {
        hide_seat_numbers = [listingDetails.hide_seat_numbers]
      }

      var singleSelect = {
        ticket_type_id: ticketTypeDetail,
        seat_type_id: seatType,
        ticketing_system: ticketSystem,
        ticketing_system_accounts: [],
        hide_seat_number: hide_seat_numbers,
        splitId: splitDetails
      }

      this.setState({ singleSelect })
    }

    await this.setState({
      ticketTypeDetails: ticketTypeDetails,
      genreDetails: genreDetails,
      filterListingsData: userFilterListingsData,
      quantities: quantityArray,
      getTags: getTags,
      getAllSeatTypes: getAllSeatTypes,
      getTicketingSystems: getTicketingSystems,
      getDisclosures: getDisclosures,
      getAttributes: getAttributes,
      getTicketingSystemAccounts: getTicketingSystemAccounts,
      getChannels: getChannels,
      rows: rowArray,
      getCountryList: getCountryList,
      getSplitsList: getSplitsList,
      userId: this.context.user.id
    });
    this.channelManagmentInputarray();
    this.setMyPageLoader(false)
  }
  async componentDidUpdate() {
    this.setHistoryPageTitle();
  }
  setHistoryPageTitle = async () => {
    if (!this.props.history.location.pageTitle) {
      this.props.history.push({ pageTitle: this.props.t("addlisting.title"), pageSubTitle: this.props.t("title"), redirectPath: "listings" });
    }
  }

  //Add Discloures Popup
  async AddDisclosureOnclick() {

    var modelpopup = {
      'isModalVisible': true,
      'title': 'Add Disclosures',
      'type': 'disclosure'
    }
    this.setState({ isAppendModelPopup: modelpopup })

  }

  async AddTicketingSystemAccountOnclick() {

    var modelpopup = {
      'isModalVisible': true,
      'title': 'Add Ticketing System Account',
      'type': 'ticketingSystemAccount'
    }
    this.setState({ isAppendModelPopup: modelpopup })

  }

  //open Attribute Popup
  async AddAttributeOnclick() {
    var modelpopup = {
      'isModalVisible': true,
      'title': 'Add Attributes',
      'type': 'attribute'
    }
    this.setState({ isAppendModelPopup: modelpopup })
  }

  //open Tags Popup
  async AddTagOnclick() {
    var modelpopup = {
      'isModalVisible': true,
      'title': 'Add Tags',
      'type': 'tag'
    }
    this.setState({ isAppendModelPopup: modelpopup })
  }

  /**
  * Page : Add Listing
  * Function For : Model Add Ticketing system Model popup
  * Ticket No : TIC-113
  */
  //Add Ticketing System Popup
  async AddTicketingSystemOnclick() {

    var modelpopup = {
      'isModalVisible': true,
      'title': 'Add ticketing system',
      'type': 'ticketing_system'
    }
    this.setState({ isAppendModelPopup: modelpopup })
  }


  /**
 * Page : Add Listing
 * Function For : Close Model Append Model popup
 * Ticket No : TIC-113
 */
  //Cancel Append Model Popup
  async AppendModelSubmitOnclick() {

    let updatedAppendModelPopup = this.state.isAppendModelPopup;
    updatedAppendModelPopup.isModalVisible = false;
    await this.setState({
      isAppendModelPopup: updatedAppendModelPopup
    });

  }

  /**
* Page : Add Listing
* Function For : Cancel Model Append Model popup
* Ticket No : TIC-113
*/
  //Cancel Append Model Popup
  async AppendModelCancelOnclick() {
    let updatedAppendModelPopup = this.state.isAppendModelPopup;
    let getTicketingSystemAccounts = [];

    updatedAppendModelPopup.isModalVisible = false;
    await this.setState({
      isAppendModelPopup: updatedAppendModelPopup
    });
    let getTicketingSystems = await getAllTicketingSystemList();
    let getDisclosures = await getAllDisclosureList(this.context.user.id);
    let getAttributes = await getAllAttributeList(this.context.user.id);
    let getTags = await getAllTagList(this.context.user.id);

    if (this.state.singleSelect.ticketing_system && this.state.singleSelect.ticketing_system[0] && this.state.singleSelect.ticketing_system[0].value) {
      getTicketingSystemAccounts = await allTicketingSystemAccounts(this.state.singleSelect.ticketing_system[0].value, this.context.user.id);
    }

    await this.setState({
      getTicketingSystems: getTicketingSystems,
      getDisclosures: getDisclosures,
      getAttributes: getAttributes,
      getTags: getTags,
      getTicketingSystemAccounts: getTicketingSystemAccounts,
      newAttribute: '', newTags: ''
    });
  }

  /**
   * Page : Add Listing
   * Function For : Unit Cost Calculation (Cost / Seat Quantity)
   * Ticket No : TIC-113
   */
  async unitCostCalculator(params) {

    var seat_start = this.state.inputList[params].seat_start;
    var seat_end = this.state.inputList[params].seat_end;
    var cost = this.state.inputList[params].cost;
    var unit_cost = '';

    if (this.state.singleSelect.seat_type_id[0].value === seatTypes.generalAdmission.id) {
      var unit_cost = Number(cost) / this.state.quantity_ga;

      unit_cost = parseFloat(unit_cost).toFixed(2);

      let updatedInputList = this.state.inputList;
      updatedInputList[params].unit_cost = unit_cost;
      await this.setState({
        inputList: updatedInputList
      });

    } else {
      if (Number(seat_start) <= Number(seat_end)) {
        if (cost != '') {

          var seat_count = Number(seat_end) - Number(seat_start);
          var new_seat_count = seat_count + 1;
          var unit_cost = Number(cost) / new_seat_count;

          unit_cost = parseFloat(unit_cost).toFixed(2);

          let updatedInputList = this.state.inputList;
          updatedInputList[params].unit_cost = unit_cost;
          await this.setState({
            inputList: updatedInputList
          });
        }
      }
    }
  }

  async setAttachmentsForGA() {
    let elements = this.state.inputList;
    if (elements && elements.length) {

      if (this.state.singleSelect.seat_type_id && this.state.singleSelect.seat_type_id[0] && this.state.singleSelect.seat_type_id[0].value == seatTypes.generalAdmission.id) {
        for (var elementCount = 0; elementCount < elements.length; elementCount++) {
          const items = [];
          if (this.state.singleSelect.ticket_type_id && this.state.singleSelect.ticket_type_id[0] && this.state.singleSelect.ticket_type_id[0].value == 1 && Number(this.state.quantity_ga) > 0) {
            for (var quantityGACount = 1; quantityGACount <= Number(this.state.quantity_ga); quantityGACount++) {
              var data = {
                'attachment_seatno': quantityGACount,
                'attachment_text': '',
                'attachment_file': '',
                'attachment_file_data': ''
              }
              items.push(data);
            }
          }
          elements[elementCount].attachments = items;
        }
        await this.setState({
          inputList: elements
        });
      }
      else {
        //Attachment Section
        let updatedInputList = this.state.inputList;
        if (updatedInputList && updatedInputList.length) {

          for (var statearraynumber = 0; statearraynumber < updatedInputList.length; statearraynumber++) {
            if(updatedInputList[statearraynumber].attachments.length == 0) 
            {
              const updated_seat_end = updatedInputList[statearraynumber].seat_end;
              const updated_seat_start = updatedInputList[statearraynumber].seat_start;
              const items = [];
              if (this.state.singleSelect.ticket_type_id && this.state.singleSelect.ticket_type_id[0] && this.state.singleSelect.ticket_type_id[0].value == 1 && Number(updated_seat_start) > 0) {
                for (var seatCount = Number(updated_seat_start); seatCount <= Number(updated_seat_end); seatCount++) {
                  var data = {
                    'attachment_seatno': seatCount,
                    'attachment_text': '',
                    'attachment_file': '',
                    'attachment_file_data': ''
                  }
                  items.push(data);
                }
              }
              let updatedinputList = this.state.inputList;
              updatedinputList[statearraynumber].attachments = items;
              await this.setState({
                inputList: updatedinputList
              });
            }
          }
        }
        //Attachment section
      }
    }
  }

  async unitCostCalculatorForGA() {
    if (this.state.singleSelect.seat_type_id && this.state.singleSelect.seat_type_id.length && this.state.singleSelect.seat_type_id[0].value === seatTypes.generalAdmission.id) {
      let updatedInputList = this.state.inputList;
      var quantity_ga = this.state.quantity_ga;
      if (quantity_ga == "") { quantity_ga = 1 }

      for (var inputDataCount = 0; inputDataCount < updatedInputList.length; inputDataCount++) {
        var unit_cost = Number(updatedInputList[inputDataCount].cost) / quantity_ga;
        updatedInputList[inputDataCount].unit_cost = unit_cost;
      }

      await this.setState({
        inputList: updatedInputList
      });
    }
    else {
      let updatedInputList = this.state.inputList;

      for (var inputDataCount = 0; inputDataCount < updatedInputList.length; inputDataCount++) {
        var seat_start = updatedInputList[inputDataCount].seat_start;
        var seat_end = updatedInputList[inputDataCount].seat_end;
        var cost = updatedInputList[inputDataCount].cost;
        var unit_cost = '';

        if (Number(seat_start) <= Number(seat_end)) {
          if (cost != '') {
            var seat_count = Number(seat_end) - Number(seat_start);
            var new_seat_count = seat_count + 1;
            var unit_cost = Number(cost) / new_seat_count;
            unit_cost = parseFloat(unit_cost).toFixed(2);
            updatedInputList[inputDataCount].unit_cost = unit_cost;
          }
        }
      }

      await this.setState({
        inputList: updatedInputList
      });
    }

  }

  async channelManagmentInputarray() {
    {
      this.state.getChannels && this.state.getChannels.length > 0
        && this.state.getChannels.map((channelitem, i) => {
          var data = {
            "channel_id": channelitem.id,
            "channel_markup_name": channelitem.channel_name,
            "channel_markup_type_id": "1",
            "markup_amount": "0",
            "switch": false
          }
          if (channelitem.channel_name == listingConst.yadara) {
            data.switch = true
          }
          this.setState({ channelManagmentInputList: [...this.state.channelManagmentInputList, data] })
        })
    }
  }

  toggleSwitch(e, statearraynumber) {
    let updatedChanelInputList = this.state.channelManagmentInputList;
    updatedChanelInputList[statearraynumber].switch = e;
    if (e === false) {
      updatedChanelInputList[statearraynumber].markup_amount = 0;
    }
    this.setState({
      channelManagmentInputList: updatedChanelInputList
    });
  }

  //channelManagmentOnchange
  async channelManagmentOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let maxLength = listingConst.maxLength_percent;
    if (this.state.channelManagmentInputList[statearraynumber].channel_markup_type_id == listingConst.channel_markup_type_two) {
      maxLength = listingConst.maxLength_amount;
    }
    if (statearrayvalue.length <= maxLength && e.nativeEvent.data != 'e') {//avoid length of the value more than specified 
      let updatedChanelInputList = this.state.channelManagmentInputList;
      updatedChanelInputList[statearraynumber].markup_amount = statearrayvalue;
      await this.setState({
        channelManagmentInputList: updatedChanelInputList
      });
    }

  }

  //channelManagmentIncressOnchange
  async channelManagmentIncressOnchange(e, statearraynumber) {
    let updatedChanelInputList = this.state.channelManagmentInputList;
    updatedChanelInputList[statearraynumber].markup_amount = parseInt(updatedChanelInputList[statearraynumber].markup_amount) + 1;
    await this.setState({
      channelManagmentInputList: updatedChanelInputList
    });
  }

  //channelManagmentDecressOnchange
  async channelManagmentDecressOnchange(e, statearraynumber) {
    let updatedChanelInputList = this.state.channelManagmentInputList;
    if (updatedChanelInputList[statearraynumber].markup_amount && updatedChanelInputList[statearraynumber].markup_amount > 0) {
      updatedChanelInputList[statearraynumber].markup_amount = parseInt(updatedChanelInputList[statearraynumber].markup_amount) - 1;
      await this.setState({
        channelManagmentInputList: updatedChanelInputList
      });
    }
  }

  async channelManagmentToggleOnchange(value, statearraynumber) {

    let channelManageState = this.state.channelManage;
    var statearrayvalue = value;
    if (statearrayvalue) {
      var channel_markup_type_id_value = listingConst.channel_markup_type_one;
      channelManageState[statearraynumber] = listingConst.hundred  // for percent
    } else {
      var channel_markup_type_id_value = listingConst.channel_markup_type_two;
      channelManageState[statearraynumber] = listingConst.max_nine; // for maxlength 5 and max value 99999
    }
    let updatedChanelInputList = this.state.channelManagmentInputList;
    updatedChanelInputList[statearraynumber].channel_markup_type_id = channel_markup_type_id_value;
    await this.setState({
      channelManagmentInputList: updatedChanelInputList,
      channelManage: channelManageState
    });
  }

  toggleAdvanced() {
    this.setState({ advancefilter: !this.state.advancefilter });
  }

  // Dynamic Form Function start
  async handleAddClick(i) {

    var duplicateItem = this.state.inputList[this.state.inputList.length - 1];
    var items = {
      section: duplicateItem.section,
      row: duplicateItem.row,
      seat_start: duplicateItem.seat_start,
      seat_end: duplicateItem.seat_end,
      price: duplicateItem.price,
      face_value: duplicateItem.face_value,
      cost: duplicateItem.cost,
      unit_cost: duplicateItem.unit_cost,
      disclosure: duplicateItem.disclosure,
      attribute: duplicateItem.attribute,
      po_id: duplicateItem.po_id,
      zone_seating: duplicateItem.zone_seating,
      tags: duplicateItem.tags,
      internal_notes: duplicateItem.internal_notes,
      external_notes: duplicateItem.external_notes,
      attachments: []
    };
    var singleItems = {
      seat_start: [],
      seat_end: [],
    };



    await this.setState({ inputList: [...this.state.inputList, items], singleinputList: [...this.state.singleinputList, singleItems], addGroupButtonId: this.state.addGroupButtonId + 1 }) //another array
    this.setAttachmentsForGA();
    let seatEnd = { target: { value: duplicateItem.seat_end } }
    await this.appendedFormSeatEndOnchange(seatEnd, i + 1);
    // this.formValidationExternal();
  }

  async handleRemoveClick(indexValue) {
    //Input Field Remove
    this.state.inputList.splice(indexValue, 1);
    let singleinputListState = this.state.singleinputList;
    delete singleinputListState[indexValue];
    var singleItems = singleinputListState.filter(function (item) {
      return item !== undefined;
    });
    this.setState({ inputList: this.state.inputList, singleinputList: singleItems })
    //Quantity Remove
    this.state.quantity_array.splice(indexValue, 1);
    this.setState({ quantity_array: this.state.quantity_array })
    const quantity_array = this.state.quantity_array;
    const quantity_sum_result = quantity_array.reduce((total, currentValue) => total = total + currentValue.quantity, 0);
    let updatedquantity = this.state.quantity;
    updatedquantity = quantity_sum_result;
    await this.setState({
      quantity: updatedquantity,
      addGroupButtonId: this.state.addGroupButtonId - 1,
      removeNewGroup: true
    });
    this.formValidationExternal();


  }

  //Section
  async appendedFormSectionOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].section = statearrayvalue;
    await this.setState({
      inputList: updatedInputList
    });

    //Validation array update
    var inputListValidationArray = this.state.inputListValidation;
    if (inputListValidationArray && statearrayvalue != "") {
      var index = inputListValidationArray.findIndex(function (o) {
        return o.id === statearraynumber && o.name === listingGroupInfo.section;
      })
      if (index !== -1) inputListValidationArray.splice(index, 1);
    }
  }

  //Row
  async appendedFormRowOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].row = statearrayvalue;
    await this.setState({
      inputList: updatedInputList
    });

    //Validation array update
    var inputListValidationArray = this.state.inputListValidation;
    if (inputListValidationArray && statearrayvalue != "") {
      var index = inputListValidationArray.findIndex(function (o) {
        return o.id === statearraynumber && o.name === listingGroupInfo.row;
      })
      if (index !== -1) inputListValidationArray.splice(index, 1);
    }
  }


  //Seat Start
  async appendedFormSeatStartOnchange(e, statearraynumber) {

    var statearrayvalue = parseInt(e.target.value);
    var numaric = statearrayvalue + 1;
    let updatedInputList = this.state.inputList;

    //Clear Validation Array
    var inputListValidationArray = this.state.inputListValidation;

    if (inputListValidationArray) {
      var index = inputListValidationArray.findIndex(function (o) {
        return o.id === statearraynumber && o.name === listingGroupInfo.seatStart;
      })
      if (index !== -1) inputListValidationArray.splice(index, 1);
    }

    if (inputListValidationArray) {
      var index = inputListValidationArray.findIndex(function (o) {
        return o.id === statearraynumber && o.name === listingGroupInfo.seatEnd;
      })
      if (index !== -1) inputListValidationArray.splice(index, 1);
    }

    if (numaric > 1) {
      updatedInputList[statearraynumber].seat_start = statearrayvalue;
      await this.setState({
        inputList: updatedInputList
      });

      //Quantity Start
      const updated_seat_end = updatedInputList[statearraynumber].seat_end;
      const updated_seat_start = updatedInputList[statearraynumber].seat_start;
      if (Number(updated_seat_start) <= Number(updated_seat_end)) {
        const Quantity = Number(updated_seat_end) - Number(updated_seat_start);
        var validationdata = { 'id': statearraynumber, 'quantity': Quantity + 1 };
        var temp_quantity_array = this.state.quantity_array;
        temp_quantity_array[statearraynumber] = validationdata;
        this.setState({ quantity_array: temp_quantity_array })
        const quantity_array = this.state.quantity_array;
        await this.unitCostCalculator(statearraynumber);
        //Attachment Section
        const elements = this.state.quantity_array;
        if (elements && elements.length && elements[statearraynumber]) {
          const items = [];
          if (this.state.singleSelect.ticket_type_id && this.state.singleSelect.ticket_type_id[0] && this.state.singleSelect.ticket_type_id[0].value == 1 && Number(updated_seat_start) > 0) {
            for (var seatCount = Number(updated_seat_start); seatCount <= Number(updated_seat_end); seatCount++) {
              var data = {
                'attachment_seatno': seatCount,
                'attachment_text': '',
                'attachment_file': '',
                'attachment_file_data': ''
              }
              items.push(data);
            }
          }
          let updatedinputList = this.state.inputList;
          updatedinputList[statearraynumber].attachments = items;
          await this.setState({
            inputList: updatedinputList
          });
        }
        //Attachment section
        const quantity_sum_result = quantity_array.reduce((total, currentValue) => total = total + currentValue.quantity, 0);
        let updatedquantity = this.state.quantity;
        updatedquantity = quantity_sum_result;
        await this.setState({
          quantity: updatedquantity
        });
        //this.setState({ quantity: ''+quantity_sum_result })
      }
      //Quantity End

      var seat_end = updatedInputList[statearraynumber].seat_end;
      if (statearrayvalue && seat_end) {
        if (statearrayvalue > Number(seat_end)) {
          var validationdata = {
            id: statearraynumber,
            name: 'seat_start',
            message: 'Seat Start value should not greater than seat end value'
          }
        }
        // else if (statearrayvalue > listingConst.hundred) {
        //   var validationdata = {
        //     id: statearraynumber,
        //     name: 'seat_start',
        //     message: 'Seat Start value should not greater than 99'
        //   }
        // }
      }
      else {
        var validationdata = {
          id: statearraynumber,
        }
      }
      this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })


    } else {

      updatedInputList[statearraynumber].seat_start = 0;
      await this.setState({
        inputList: updatedInputList
      });
      var validationdata = {
        id: statearraynumber,
        name: 'seat_start',
        message: 'Enter valid number'
      }
      this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })
    }

    //Quantity End
  }

  //Seat End
  async appendedFormSeatEndOnchange(e, statearraynumber) {
    var statearrayvalue = parseInt(e.target.value);
    var numaric = statearrayvalue + 1;

    let updatedInputList = this.state.inputList;

    //Clear Validation Array
    var inputListValidationArray = this.state.inputListValidation;
    if (inputListValidationArray) {
      var index = inputListValidationArray.findIndex(function (o) {
        return o.id === statearraynumber && o.name === listingGroupInfo.seatEnd;
      })
      if (index !== -1) inputListValidationArray.splice(index, 1);
    }
    if (inputListValidationArray) {
      var index = inputListValidationArray.findIndex(function (o) {
        return o.id === statearraynumber && o.name === listingGroupInfo.seatStart;
      })
      if (index !== -1) inputListValidationArray.splice(index, 1);
    }

    if (numaric > 1) {

      updatedInputList[statearraynumber].seat_end = statearrayvalue;
      await this.setState({
        inputList: updatedInputList
      });

      //Quantity Start
      const updated_seat_end = updatedInputList[statearraynumber].seat_end;
      const updated_seat_start = updatedInputList[statearraynumber].seat_start;
      let updatedinputList = this.state.inputList;
      if (Number(updated_seat_start) <= Number(updated_seat_end)) {
        const Quantity = Number(updated_seat_end) - Number(updated_seat_start);
        var validationdata = { 'id': statearraynumber, 'quantity': Quantity + 1 };
        var temp_quantity_array = this.state.quantity_array;
        temp_quantity_array[statearraynumber] = validationdata;
        this.setState({ quantity_array: temp_quantity_array })
        const quantity_array = this.state.quantity_array;
        await this.unitCostCalculator(statearraynumber);
        //Attachment Section
        const elements = this.state.quantity_array;
        if (elements && elements.length && elements[statearraynumber]) {
          const items = [];
          if (this.state.singleSelect.ticket_type_id && this.state.singleSelect.ticket_type_id[0] && this.state.singleSelect.ticket_type_id[0].value == 1 && Number(updated_seat_start) > 0) {
            for (var seatCount = Number(updated_seat_start); seatCount <= Number(updated_seat_end); seatCount++) {
              var data = {
                'attachment_seatno': seatCount,
                'attachment_text': '',
                'attachment_file': '',
                'attachment_file_data': ''
              }
              items.push(data);
            }
          }
          updatedinputList[statearraynumber].attachments = items;
          await this.setState({
            inputList: updatedinputList
          });
        }
        //Attachment section
        const quantity_sum_result = quantity_array.reduce((total, currentValue) => total = total + currentValue.quantity, 0);
        let updatedquantity = this.state.quantity;
        updatedquantity = quantity_sum_result;
        await this.setState({
          quantity: updatedquantity
        });
      }


      var seat_start = updatedInputList[statearraynumber].seat_start;
      if (statearrayvalue && seat_start) {
        if (statearrayvalue < seat_start) {
          var validationdata = {
            id: statearraynumber,
            name: 'seat_end',
            message: 'Seat end value should not less than seat start value'
          }
        } else if (((statearrayvalue - seat_start) + 1) > listingConst.max_quantity) {
          var validationdata = {
            id: statearraynumber,
            name: 'seat_end',
            message: 'Seat quantity should not exceed than total of ' + listingConst.max_quantity
          }
        }
      }
      else {
        var validationdata = {
          id: statearraynumber,
        }
      }
      this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })

    } else {

      updatedInputList[statearraynumber].seat_end = 0;
      await this.setState({
        inputList: updatedInputList
      });
      var validationdata = {
        id: statearraynumber,
        name: 'seat_end',
        message: 'Enter valid number'
      }
      this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })
    }

    //Quantity End
  }

  //Price
  async appendedFormPriceOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    if (statearrayvalue.length <= listingConst.maxLength_amount && e.nativeEvent.data != 'e') {//max length 5 digits
      updatedInputList[statearraynumber].price = statearrayvalue;
      await this.setState({
        inputList: updatedInputList
      });
    }
    //Validation array update
    var inputListValidationArray = this.state.inputListValidation;
    if (inputListValidationArray && statearrayvalue != "") {
      var index = inputListValidationArray.findIndex(function (o) {
        return o.id === statearraynumber && o.name === listingGroupInfo.price;
      })
      if (index !== -1) inputListValidationArray.splice(index, 1);
    }
  }

  //Face Value
  async appendedFormFaceValueOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    if (statearrayvalue.length <= listingConst.maxLength_amount && e.nativeEvent.data != 'e') {//max length 5 digits
      updatedInputList[statearraynumber].face_value = statearrayvalue;
      await this.setState({
        inputList: updatedInputList
      });
    }
  }

  //Cost
  async appendedFormCostOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    if (statearrayvalue.length <= listingConst.maxLength_amount && e.nativeEvent.data != 'e') {//max length 5 digits
      updatedInputList[statearraynumber].cost = statearrayvalue;
      await this.setState({
        inputList: updatedInputList
      });
      await this.unitCostCalculator(statearraynumber);
    }

  }

  //Unit Cost
  async appendedFormUnitCostOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].unit_cost = statearrayvalue;
    await this.setState({
      inputList: updatedInputList
    });
  }

  //Disclosure
  async appendedFormDisclosureOnchange(e, statearraynumber) {
    var statearrayvalue = e;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].disclosure = JSON.stringify(statearrayvalue);
    await this.setState({
      inputList: updatedInputList
    });
  }

  //Attribute
  async appendedFormAttributeOnchange(e, statearraynumber) {
    var statearrayvalue = e;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].attribute = JSON.stringify(statearrayvalue);
    await this.setState({
      inputList: updatedInputList
    });
  }

  //PO ID
  async appendedFormPOIDOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].po_id = statearrayvalue;
    await this.setState({
      inputList: updatedInputList
    });
  }

  //Tags
  async appendedFormTagsOnchange(e, statearraynumber) {
    var statearrayvalue = e;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].tags = JSON.stringify(statearrayvalue);
    await this.setState({
      inputList: updatedInputList
    });
  }

  //Intrnal Notes
  async appendedFormInternalNotesOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].internal_notes = statearrayvalue;
    await this.setState({
      inputList: updatedInputList
    });
  }

  //External Notes
  async appendedFormExternalNotesOnchange(e, statearraynumber) {
    var statearrayvalue = e.target.value;
    let updatedInputList = this.state.inputList;
    updatedInputList[statearraynumber].external_notes = statearrayvalue;
    await this.setState({
      inputList: updatedInputList
    });
  }

  //Attachment
  async appendedFormAttachmentOnchange(e, statearraynumber) {

    let timestamp = new Date().getTime();
    let attachmentfile_data = e.target.files[0];


    let attachmentfile = e.target.value;
    // let attachmentfilename = timestamp + "_" + attachmentfile_data.name;
    let attachmentfilename = attachmentfile_data ? (timestamp + "_" + attachmentfile_data.name) : '';
    let attachmentseatno = e.target.id;
    var temp_attachments = this.state.inputList[statearraynumber].attachments;
    temp_attachments && temp_attachments.length > 0
      && temp_attachments.map((attachitem, i) => {
        if (attachitem.attachment_seatno === Number(attachmentseatno)) {
          var data = {
            "attachment_file": attachmentfilename,
            "attachment_file_data": attachmentfile_data,
            "attachment_seatno": attachitem.attachment_seatno,
            "attachment_text": attachitem.attachment_text
          }
          let updatedAttachmentInputList = this.state.inputList;
          updatedAttachmentInputList[statearraynumber].attachments[i] = data;
          this.setState({
            inputList: updatedAttachmentInputList
          });
        }
      })
  }

  async appendedFormAttachmentTextOnchange(e, statearraynumber) {

    let attachmentname = e.target.value;
    let attachmentseatno = e.target.id;
    var temp_attachments = this.state.inputList[statearraynumber].attachments;
    temp_attachments && temp_attachments.length > 0
      && temp_attachments.map((attachitem, i) => {
        if (attachitem.attachment_seatno === Number(attachmentseatno)) {
          var data = {
            "attachment_file": attachitem.attachment_file,
            "attachment_file_data": attachitem.attachment_file_data,
            "attachment_seatno": attachitem.attachment_seatno,
            "attachment_text": attachmentname
          }
          let updatedAttachmentInputList = this.state.inputList;
          updatedAttachmentInputList[statearraynumber].attachments[i] = data;
          this.setState({
            inputList: updatedAttachmentInputList
          });
        }
      })
  }

  async removeAttachment(event, index) {
    var attachments = this.state.inputList[index].attachments;

   attachments && attachments.length > 0
      && attachments.map(async (item, i) => {
        if (item.attachment_seatno === Number(event.target.dataset.id)) {
          var data = {
            "attachment_file": '',
            "attachment_file_data": '',
            "attachment_seatno": item.attachment_seatno,
            "attachment_text": item.attachment_text
          }
          let updatedAttachmentInputList = this.state.inputList;
          updatedAttachmentInputList[index].attachments[i] = data;
          await this.setState({
            inputList: updatedAttachmentInputList
          });
        }
      })
  }

  // Dynamic Form Function end

  /**
   * 
   * @param {*} e 
   * Date filter 'startingDate' is only required, to 'endingDate' is optional and removal 
   */
  async dateRangeDisplay(e) {//Date picker   

    var startingDate = '';
    if (e) {
      var starting = new Date(e);
      let month = starting.getMonth() + 1;
      let date = starting.getDate();
      if (month < 10) {
        month = '0' + month;
      }
      if (date < 10) {
        date = '0' + date;
      }
      startingDate = starting.getFullYear() + '-' + month + '-' + date;
    }
    this.setState({ inhand_date: startingDate, singleDate: e });

  }

  /**
   * 
   * @param {*} e 
   * Selecting days in multi option
   */
  async daySelected(e) {
    var day_select = [];
    e && e.map(days => (
      day_select.push(days.value)
    ))
    let filteredData;
    let stateHolder = this.state.searchData;
    stateHolder['day_of_week'] = day_select;
    this.setState({ selectedDays: e, searchData: stateHolder });
    filteredData = await getAddListingsFilter(this.state.searchData);
    this.setState({ filterListingsData: filteredData });
  }
  /**
   * Method name : listSearchChange
   * Searching the event id, name, etc by user search
   */
  async listSearchChange(e) {   // For autofill search suggestion

    let filteredData = {};
    let suggestions = [];
    const value = e.target.value;
    let stateHolder = this.state.searchData;
    stateHolder.searches = value;
    this.setState({ locationText: '', searchText: value, displayText: value, searchData: stateHolder });
    if (value.length > 3) {
      suggestions = await eventSearches({ searches: value });
    }
    else {
      suggestions = [];
      if (value.length == 0) {
        filteredData = await getAddListingsFilter(stateHolder);
        this.setState({ filterListingsData: filteredData });
      }
    }
    this.setState(() => ({
      suggestions,
    }));
  }

  /**
   * 
   * @param {*} keyword 
   * // Refresh the listing based on search filter suggestion onclick
   */
  async selectLisitng(keyword = false) {
    await this.setState({ suggestions: null, displayText: keyword.toString(), });
    let allSearchData = this.state.searchData;
    allSearchData.searches = keyword.toString();
    await this.setState({ searches: allSearchData })
    let searchedData = await getAddListingsFilter(this.state.searchData);
    this.setState({ filterListingsData: searchedData });
  }


  /**
   * filtering keyword column in data table
   * @param {*} e 
   * @param {*} keyword 
   * all params are required
   */

  async advanceFilter(e, keyword) {
    let filteredData;
    let stateHolder = this.state.searchData;
    if (keyword == listingGroupInfo.quantity) {
      stateHolder[keyword] = parseInt(e.target.value)
    }
    else { stateHolder[keyword] = e.target.value; }
    this.setState({ searchData: stateHolder });
    filteredData = await getAddListingsFilter(this.state.searchData);
    this.setState({ filterListingsData: filteredData });
  }

  /**
  * 
  * @param {*} keyword 
  * // Cancel the listing form
  */

  async cancelAddListingOnclick() {

    await this.setState({
      inputList: [{
        section: "",
        row: "",
        seat_start: "",
        seat_end: "",
        price: "0",
        face_value: "0",
        cost: "1",
        unit_cost: "0",
        disclosure: "",
        attribute: "",
        po_id: "",
        tags: "",
        internal_notes: "",
        external_notes: "",
        attachments: [],
      }],
      inhand_date: '',
      quantity: '0',
      attachmentInputList: [],
      channelManagmentInputList: [],
      singleDate: '',
      singleSelect: {
        ticket_type_id: [],
        seat_type_id: [preselectedSeat],
        ticketing_system: [],
        ticketing_system_accounts: [],
        hide_seat_number: preselectedHideSeatNumber[0],
        splitId: [preselectedSplit]

      },
      singleinputList: [{
        seat_start: [],
        seat_end: [],
      }],
    });
    await this.channelManagmentInputarray();
  }
  /**
   * Modified the design for single selects similar to multiselect - functionality
   * @param {*} e  input field value
   * @param {*} keyword which input field value is going to be modified / added
   */
  async singleSelect(e, keyword) {
    let stateHolder = this.state.singleSelect;
    let selectedValue;
    let val = this.state.singleSelect[keyword];

    if (e) { selectedValue = [e]; } else if (val) { selectedValue = val; } else { selectedValue = []; }

    let newValue = selectedValue.filter(ar => !val.find(rm => (rm.label === ar.label && ar.value === rm.value)));
    stateHolder[keyword] = newValue;
    if (keyword == listingConst.seat_type_id) {
      this.setState({ seat_type_value: newValue[0] ? newValue[0].value : '', singleSelect: stateHolder });
    }
    else {
      this.setState({ singleSelect: stateHolder });
    }

    if (keyword == listingConst.ticket_type_id) {
      if (newValue && newValue[0] && newValue[0].value === listingConst.localpickup_id) {
        this.setState({ Is_show_localpickup: true });
      } else {
        this.setState({ Is_show_localpickup: false });
      }
    }

    if (keyword == listingConst.seat_type_id) {
      if (newValue && newValue.length > 0 && newValue[0] && newValue[0].value === seatTypes.generalAdmission.id) {
        this.setState({ Is_show_sectiondiv: false });
      } else {
        this.setState({ Is_show_sectiondiv: true });
      }
      await this.unitCostCalculatorForGA();
      this.setAttachmentsForGA();
    }

    if (keyword == listingConst.ticket_type_id) {
      if (newValue[0] && newValue[0].label !== listingConst.etickets) { // Remove all attachments if ticket type is not "eTickets"
        for (var inputListCount = 0; inputListCount < this.state.inputList.length; inputListCount++) {
          let updatedInputList = this.state.inputList;
          updatedInputList[inputListCount].attachments = [];
          await this.setState({
            inputList: updatedInputList
          });
        }
      }
      else {// Re adding all attachments if ticket type is "eTickets" based on seat start,end
        if (this.state.singleSelect.seat_type_id && this.state.singleSelect.seat_type_id.length > 0 && this.state.singleSelect.seat_type_id[0] && this.state.singleSelect.seat_type_id[0].value === seatTypes.generalAdmission.id) {
          this.setAttachmentsForGA();
        }
        else {
          var updatedInputList = this.state.inputList;
          this.state.inputList.length > 0 && this.state.inputList.map(async (seats, i) => {
            const updated_seat_end = updatedInputList[i].seat_end;
            const updated_seat_start = updatedInputList[i].seat_start;
            ////////////////////

            const elements = this.state.quantity_array;

            if (elements && elements.length && elements[i]) {
              const items = [];

              if (this.state.singleSelect.ticket_type_id && this.state.singleSelect.ticket_type_id[0] && this.state.singleSelect.ticket_type_id[0].value == 1) {
                for (var seatCount = Number(updated_seat_start); seatCount <= Number(updated_seat_end); seatCount++) {
                  var data = {
                    'attachment_seatno': seatCount,
                    'attachment_text': '',
                    'attachment_file': '',
                    'attachment_file_data': ''
                  }
                  items.push(data);
                }
              }

              if (updatedInputList != '' || updatedInputList != undefined) {
                updatedInputList[i].attachments = items;
                await this.setState({
                  inputList: updatedInputList
                });
              }

            }

          })
          await this.setState({ inhand_date: new Date(), singleDate: new Date() });
        }
      }
    }

    if (keyword == listingConst.ticketingSystem) {
      if (newValue && newValue[0]) {
        let getTicketingSystemAccounts = await allTicketingSystemAccounts(newValue[0].value, this.context.user.id);
        this.setState({ disableTicketingAccount: false, getTicketingSystemAccounts });
      }
      else {
        this.setState({ disableTicketingAccount: true });
      }

      this.setState(prevState => ({
        singleSelect: {
          ...prevState.singleSelect,
          ticketing_system_accounts: []
        }
      }))

    }
    if (keyword == listingConst.ticketing_system_accounts) {
      if (newValue && newValue[0]) {
        this.setState(prevState => ({
          singleSelect: {
            ...prevState.singleSelect,
            ticketing_system_accounts: [newValue[0]]
          }
        }))

      }
    }

  }

  async setQuantity(e) {
    await this.setState({ quantity_ga: e.target.value });
    this.unitCostCalculatorForGA();
    this.setAttachmentsForGA();
  }

  filterOptions = (options, filter) => {
    if (!filter) {
      return options;
    }
    const re = new RegExp(filter, "i");
    return options.filter(({ label }) => label && label.match(re));
  };

  dropdownFilterOption = ({ label }, string) => {
    if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };
  /**
   * To avoid user change the readonly/disable attributes forcefully
   */
  async avoidOnchange(e) {
    e.preventDefault();
    return false;
  }

  /**
   * Loader Function
   */
  async setMyPageLoader(bool) {
    if (bool) {
      this.setState({ setLoader: true });
      document.body.classList.add('loader-background');
    }
    else {
      this.setState({ setLoader: false });
      document.body.classList.remove('loader-background');
    }
  }
  async disableAddlistingButton(bool) {
    this.setState({ disable_addlist: bool })
  }

  async hideSeatNumber(value) {
    await this.setState({
      singleSelect: {
        ...this.state.singleSelect,
        hide_seat_number: [value]
      }
    })
  }

  async formValidationExternal() {
    if (this.state.inputList) {

      await this.setState({ inputListValidation: "", quantityInputValidation: "" });

      this.state.inputList && this.state.inputList.length > 0
        && this.state.inputList.map(async (seatitem, i) => {

          if (this.state.singleSelect && this.state.singleSelect.seat_type_id && this.state.singleSelect.seat_type_id.length) {

            if (this.state.singleSelect.seat_type_id[0].value !== seatTypes.generalAdmission.id) {

              if (seatitem.section == "") {
                var validationdata = {
                  id: i,
                  name: 'section',
                  message: 'Section is required'
                }
                await this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })

              }

              if (seatitem.row == "") {
                var validationdata = {
                  id: i,
                  name: 'row',
                  message: 'Row is required'
                }
                await this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })
              }

              if (seatitem.seat_start == "") {
                var validationdata = {
                  id: i,
                  name: 'seat_start',
                  message: 'Seat start is required'
                }
                await this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })
              }

              if (seatitem.seat_end == "") {
                var validationdata = {
                  id: i,
                  name: 'seat_end',
                  message: 'Seat end is required'
                }
                await this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })
              }

            } else {
              if (this.state.quantity_ga == "") {

                var validationdata = {
                  name: 'quantity_ga',
                  message: 'Quantity is required'
                }
                this.setState({ quantityInputValidation: [validationdata] })
              } else if (this.state.quantity_ga <= "0") {

                var validationdata = {
                  name: 'quantity_ga',
                  message: 'Enter valid number'
                }
                this.setState({ quantityInputValidation: [validationdata] })
              } else if (this.state.quantity_ga > listingConst.max_quantity) {
                var validationdata = {
                  name: 'quantity_ga',
                  message: 'Maximum quantity is ' + listingConst.max_quantity
                }
                this.setState({ quantityInputValidation: [validationdata] })
              }
              else {
                this.setState({ quantityInputValidation: "" })
              }

            }
          }

          if (seatitem.cost == "" || seatitem.cost < 1) {
            var validationdata = {
              id: i,
              name: 'cost',
              message: 'Cost is required'
            }
            await this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })
          }
          if (seatitem.price == "" || seatitem.price < 1) {
            var validationdata = {
              id: i,
              name: 'price',
              message: 'Price is required'
            }
            await this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })
          }




          // Seat Start Lessthan & Graterthan condtion start 
          let updatedInputList = this.state.inputList;
          var seat_end = updatedInputList[i].seat_end;
          if (Number(seatitem.seat_start) && Number(seat_end)) {
            if (Number(seatitem.seat_start) > Number(seat_end)) {
              var validationdata = {
                id: i,
                name: 'seat_start',
                message: 'Seat Start value should not greater than seat end value'
              }

              await this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })
            }
          }
          // Seat Start Lessthan & Graterthan condtion end 


          // Seat End Lessthan & Graterthan condtion start 
          var seat_start = updatedInputList[i].seat_start;
          if (Number(seatitem.seat_end) && Number(seat_start)) {
            if (Number(seatitem.seat_end) < Number(seat_start)) {
              var validationdata = {
                id: i,
                name: 'seat_end',
                message: 'Seat end value should not less than seat start value'
              }

              await this.setState({ inputListValidation: [...this.state.inputListValidation, validationdata] })
            }
          }
          // Seat End Lessthan & Greaterthan condtion end 


        })

    }

  }

  render() {
    const { suggestions } = this.state
    const days = daysFull;
    const monthNames = monthNamesShort;



    const disclosureValues = [];
    this.state.getDisclosures && this.state.getDisclosures.length > 0
      && this.state.getDisclosures.map((item, itemCount) => {
        disclosureValues.push({ value: item.id, label: item.disclosure_name });
      })

    const AttrValues = [];
    this.state.getAttributes && this.state.getAttributes.length > 0
      && this.state.getAttributes.map((item, i) => {
        var combined = { label: item.attribute_name, value: item.id, key: item.id.toString() }
        AttrValues.push(combined);
      })
    const tagValues = [];
    this.state.getTags && this.state.getTags.length > 0
      && this.state.getTags.map((item, i) => {
        var combined = { label: item.tag_name, value: item.id }
        tagValues.push(combined);
      })
    const ticketTypeDetails = [];
    this.state.ticketTypeDetails && this.state.ticketTypeDetails.length && this.state.ticketTypeDetails.map(tickets =>
      ticketTypeDetails.push({ label: tickets.name, value: tickets.id },)
    )

    const ticketSystemAccounts = [];
    this.state.getTicketingSystemAccounts && this.state.getTicketingSystemAccounts.length && this.state.getTicketingSystemAccounts.map(sysAccounts => ticketSystemAccounts.push({ label: sysAccounts.email, value: sysAccounts.id },)
    )

    const seatType = [];
    this.state.getAllSeatTypes && this.state.getAllSeatTypes.length > 0 && this.state.getAllSeatTypes.map(seatTypes => seatType.push({ label: seatTypes.name, value: seatTypes.id }))

    const splitDetails = [];
    this.state.getSplitsList && this.state.getSplitsList.length && this.state.getSplitsList.map(splits =>
      splitDetails.push({ label: splits.name, value: splits.id },)
    )

    const seatTypeSelected = preselectedSeat ? preselectedSeat : [];

    const ticketSystem = [];
    this.state.getTicketingSystems && this.state.getTicketingSystems.length && this.state.getTicketingSystems.map(ticSystem => ticketSystem.push({ label: ticSystem.ticketing_system_name, value: ticSystem.id },)
    )


    const seatStarts = [];
    for (var seatCount = 0; seatCount < 100; seatCount++) {  // Dynamic quantity 1-100
      seatStarts.push({ label: seatCount + 1, value: seatCount + 1 },)
    }




    return !this.state.loading ? (
      <div>
        <UserContext.Consumer>
          {context => (
            <div>
              <div className="loader-bg">
                <ClipLoader color={'#4a88c5'} loading={this.state.setLoader} size={40} />
              </div>
              <div className="myTicket-item">
                <div className="search-item">

                  {/* Add Listing Form Start */}
                  <div className="add-listing-fliter-bottom">
                    <Formik
                      initialValues={{
                        seller_id: (this.state.userId && this.state.userId.length) ? this.state.userId : "",
                        //TODO: To be mapped with buyer id in future. Commented this due to association error in backend
                        buyer_id: (this.state.singleSelect.ticketing_system_accounts.length > 0 && this.state.singleSelect.ticketing_system_accounts[0]) ? this.state.singleSelect.ticketing_system_accounts[0].value : [],
                        eventIds: this.props.eventId,
                        ticket_type_id: (this.state.singleSelect.ticket_type_id.length > 0 && this.state.singleSelect.ticket_type_id[0]) ? this.state.singleSelect.ticket_type_id[0].value : '',
                        inhand_date: this.state.inhand_date,
                        splitId: (this.state.singleSelect.splitId.length > 0 && this.state.singleSelect.splitId[0]) ? this.state.singleSelect.splitId[0].value : '',
                        seat_type_id: this.state.seat_type_value,
                        quantity: "0",
                        ticketing_system: (this.state.singleSelect.ticketing_system.length && this.state.singleSelect.ticketing_system[0]) ? this.state.singleSelect.ticketing_system[0].value : [],
                        ticketing_system_accounts: (this.state.singleSelect.ticketing_system_accounts.length > 0 && this.state.singleSelect.ticketing_system_accounts[0]) ? this.state.singleSelect.ticketing_system_accounts[0].value : [],

                        hide_seat_number: (this.state.singleSelect.hide_seat_number[0]) ? this.state.singleSelect.hide_seat_number[0] : false,
                        quantity_ga: this.state.quantity_ga ? this.state.quantity_ga : "",


                        seatInformationsJSON: "",
                        channelmarkupsJSON: ""
                      }}
                      enableReinitialize={true}
                      onReset={this.cancelAddListingOnclick}

                      validationSchema={Yup.object().shape({

                        ticket_type_id: Yup.string()
                          .required('Ticket type is required'),

                        inhand_date: Yup.string()
                          .required('Inhand date is required'),

                        seat_type_id: Yup.string()
                          .required('Seat type value is required'),

                        splitId: Yup.string()
                          .required('Split value is required'),

                        ticketing_system: Yup.string()
                          .required('Ticketing system is required'),

                        ticketing_system_accounts: Yup.string()
                          .required('Account is required'),

                        hide_seat_number: Yup.string()
                          .required('Hide seat number is required'),

                      })}

                      onSubmit={async (fields) => {

                        var data = {
                          "seatInformationsJSON": this.state.inputList,
                          "channelmarkupsJSON": this.state.channelManagmentInputList,
                          "quantity": '' + this.state.quantity + ''
                        }
                        await this.formValidationExternal();
                        if (this.state.inputListValidation == "" && this.state.quantityInputValidation == "") {
                          const listdata = await { ...fields, ...data };
                          await this.setState({
                            closeButtonModal: true,
                            datafields: listdata
                          });
                        }
                      }}

                      render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                        <Form autoComplete="off">
                          <div className="">
                            <div className="add-listing-card-section">
                              <div className="card">
                                {this.state.eventDetails && this.state.eventDetails.length > 0
                                  && this.state.eventDetails.map((eventitem, i) => {
                                    return (
                                      <>
                                        <div className="card-header">
                                          <div className="addlisting-title-item">
                                            <div className="listing-title-divinner">
                                              <div className="left-title">
                                                <h2 className="listing-card-title">{eventitem.eventname}</h2>
                                              </div>
                                              <div className="right-side-id ml-auto">
                                                <p>Event ID: {eventitem.event_id}</p>
                                              </div>
                                            </div>
                                            <div className="d-flex">
                                              <h3 className="card-location-item">
                                                {eventitem.venuename + ', ' + eventitem.city + ', ' + eventitem.state + ', ' + eventitem.country}
                                              </h3>
                                              <h3 className="card-date-item">
                                                {eventitem.timezone && eventitem.timezone[0] ? moment.utc(eventitem.eventdate).tz(eventitem.timezone[0]).format('dddd MMM Do YYYY @ h:mm A z') : moment.utc(eventitem.eventdate).format('dddd MMM Do YYYY @ h:mm A z')}
                                              </h3>
                                            </div>
                                          </div>
                                        </div>
                                      </>
                                    )
                                  })}
                                <div className="card-body">
                                  <div className="form-data-grid-4">
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Ticket Type <span>*</span></label>
                                        <Select
                                          {...this.props}
                                          placeholder={"Ticket Type"}
                                          style={{ width: "100%" }}
                                          name="ticket_type_id"
                                          filterOption={this.dropdownFilterOption}
                                          value={this.state.singleSelect.ticket_type_id}
                                          options={ticketTypeDetails}
                                          onChange={(e) => this.singleSelect(e, 'ticket_type_id')}
                                          className={(errors.ticket_type_id && touched.ticket_type_id ? ' is-invalid' : '')}
                                          allowClear
                                          showSearch
                                          isClearable
                                        />
                                        <ErrorMessage name="ticket_type_id" component="div" className="invalid-feedback" />
                                      </div>
                                    </div>
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Inhand Date <span>*</span></label>

                                        <div className="date--PickQr">
                                          {/* <CalendarIcon /> */}
                                          <span className="custom-calendar-img">
                                            <img src={iconCalendar} />
                                          </span>

                                          <DatePicker
                                            name="inhand_date"
                                            selectsRange={false}
                                            className={'formik-input-user-form form-control singleDate' + (errors.inhand_date && touched.inhand_date ? ' is-invalid' : '')}
                                            selected={this.state.singleDate}
                                            onChange={this.dateRangeDisplay}
                                            onKeyDown={(e) => this.avoidOnchange(e)}
                                            maxDate={new Date(this.state.selectedEventMaxDate)}
                                            highlightDates={new Date()}
                                            isClearable={this.state.singleDate ? true : false} disabledKeyboardNavigation
                                            dateFormat="dd/MM/yyyy"
                                            placeholderText=" DD/MM/YYYY"
                                          /><ErrorMessage name="inhand_date" component="div" className="invalid-feedback" style={{ display: 'block' }} />

                                        </div>

                                      </div>
                                    </div>
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Splits <span>*</span></label>
                                        <Select
                                          {...this.props}
                                          placeholder={"Splits"}
                                          style={{ width: "100%" }}
                                          name="splitId"
                                          filterOption={this.dropdownFilterOption}
                                          value={this.state.singleSelect.splitId}
                                          options={splitDetails}
                                          onChange={(e) => this.singleSelect(e, 'splitId')}
                                          className={(errors.splitId && touched.splitId ? ' is-invalid' : '')}
                                          allowClear
                                          showSearch
                                          isClearable
                                        />
                                        <ErrorMessage name="splitId" component="div" className="invalid-feedback" />
                                      </div>
                                    </div>
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Seat Type <span>*</span></label>
                                        <Select
                                          {...this.props}
                                          placeholder={"Seat Type"}
                                          style={{ width: "100%" }}
                                          name="seat_type_id"
                                          filterOption={this.dropdownFilterOption}
                                          value={this.state.singleSelect.seat_type_id}
                                          options={seatType}
                                          onChange={(e) => this.singleSelect(e, 'seat_type_id')}
                                          className={(errors.seat_type_id && touched.seat_type_id ? ' is-invalid' : '')}
                                          allowClear
                                          showSearch
                                          isClearable
                                        />
                                        <ErrorMessage name="seat_type_id" component="div" className="invalid-feedback" />
                                      </div>
                                    </div>

                                    {this.state.Is_show_sectiondiv ?
                                      <div className="di-form-items">
                                        <div className="form-group add-listing-form-group">
                                          <label>Quantity <span>*</span></label>
                                          <input
                                            className={'formik-input-user-form form-control' + (errors.quantity && touched.quantity ? ' is-invalid' : '')}
                                            type="text"
                                            placeholder="Quantity"
                                            value={this.state.quantity}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            readOnly />
                                          <ErrorMessage name="quantity" component="div" className="invalid-feedback" />
                                        </div>
                                      </div>
                                      :
                                      <div className="di-form-items">
                                        <div className="form-group add-listing-form-group">
                                          <label>Quantity *</label>
                                          <input
                                            className={`formik-input-user-form form-control ${this.state.quantityInputValidation && this.state.quantityInputValidation.map((validationitems, z) => {
                                              if (validationitems.name == listingGroupInfo.quantityGA) {
                                                return (' addlisting-border ');
                                              }
                                            }
                                            )}`}
                                            type="number"
                                            id="quantity_ga"
                                            name="quantity_ga"
                                            placeholder="Quantity"
                                            value={this.state.quantity_ga}
                                            onChange={(e) => this.setQuantity(e)}
                                            onBlur={handleBlur}
                                            onKeyDown={e => (e.keyCode === 69 || e.keyCode === 190 || e.keyCode === 110) && e.preventDefault()}
                                            disabled={this.state.Is_show_sectiondiv} />

                                          {this.state.quantityInputValidation && this.state.quantityInputValidation.map((validationitems, z) => {
                                            return (
                                              <div className="add-listing-validation-error"> {validationitems.message} </div>
                                            )
                                          })}

                                          <ErrorMessage name="quantity_ga" component="div" className="invalid-feedback" />
                                        </div>
                                      </div>
                                    }

                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Ticketing System <span>*</span>
                                        </label>
                                        <Select
                                          {...this.props}
                                          placeholder={"Ticketing System"}
                                          style={{ width: "100%" }}
                                          name="ticketing_system"
                                          filterOption={this.dropdownFilterOption}
                                          value={this.state.singleSelect.ticketing_system}
                                          options={ticketSystem}
                                          onChange={(e) => this.singleSelect(e, 'ticketing_system')}
                                          className={(errors.ticketing_system && touched.ticketing_system ? ' is-invalid' : '')}
                                          allowClear
                                          showSearch
                                          isClearable
                                        />
                                        <ErrorMessage name="ticketing_system" component="div" className="invalid-feedback" />
                                      </div>
                                    </div>
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Account <span>*</span> <i className="fa fa-plus-circle add-disclosure-icon" title="Add Ticketing System Account" disabled={this.state.disableTicketingAccount} onClick={this.AddTicketingSystemAccountOnclick} aria-hidden="true"></i> </label>
                                        <Select
                                          {...this.props}
                                          placeholder={"Account"}
                                          style={{ width: "100%" }}
                                          name="ticketing_system_accounts"
                                          filterOption={this.dropdownFilterOption}
                                          value={this.state.singleSelect.ticketing_system_accounts}
                                          options={ticketSystemAccounts}
                                          onChange={(e) => this.singleSelect(e, 'ticketing_system_accounts')}
                                          className={(errors.ticketing_system_accounts && touched.ticketing_system_accounts ? ' is-invalid' : '')}
                                          allowClear
                                          showSearch
                                          isClearable
                                        />
                                        <ErrorMessage name="ticketing_system_accounts" component="div" className="invalid-feedback" />
                                      </div>
                                    </div>
                                    <div className="di-form-items">
                                      <div className="form-group add-listing-form-group">
                                        <label>Hide Seat Number <span>*</span></label>

                                        <Switch
                                          className="filter-btn-hide--seat"
                                          name="hide_seat_number"
                                          checkedChildren="ON"
                                          unCheckedChildren="OFF"
                                          onChange={(e) => this.hideSeatNumber(e)}
                                          checked={this.state.singleSelect.hide_seat_number[0]}
                                        />

                                        <ErrorMessage name="hide_seat_number" component="div" className="invalid-feedback" />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>


                              {/* append field start */}
                              {this.state.inputList.map((x, i) => {
                                return (
                                  <>
                                    <div className="card">
                                      <div className="card-header add-group-listing-card-header">
                                        <h2 className="add-group-list-title">Group #{i + 1}</h2>
                                      </div>
                                      <div className="card-body">
                                        <div className="add-listing-form-items">
                                          <div className="form-data-grid-4">

                                            {this.state.Is_show_sectiondiv &&
                                              <>

                                                <div className="di-form-items">
                                                  <div className="form-group add-listing-form-group">
                                                    <label>Section <span>*</span></label>

                                                    <input
                                                      type="text"
                                                      name="section"
                                                      placeholder="Section"
                                                      className={`formik-input-user-form form-control ${this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                        if (validationitems.id == i && validationitems.name == listingGroupInfo.section) {
                                                          return (' addlisting-border ');
                                                        }
                                                      }
                                                      )}`}

                                                      value={x.section}
                                                      onChange={(e) => this.appendedFormSectionOnchange(e, i)}
                                                      onBlur={handleBlur}
                                                    />
                                                    {/* Validation Section Start */}
                                                    {this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                      if (validationitems.id == i && validationitems.name == listingGroupInfo.section) {
                                                        return (
                                                          <div className="add-listing-validation-error"> {validationitems.message} </div>
                                                        )
                                                      }
                                                    })}
                                                    {/* Validation Section End */}

                                                    <ErrorMessage name="section" component="div" className="invalid-feedback" />
                                                  </div>
                                                </div>
                                                <div className="di-form-items">
                                                  <div className="form-group add-listing-form-group">
                                                    <label>Row <span>*</span></label>
                                                    <input
                                                      type="text"
                                                      name="row"
                                                      placeholder="Row"
                                                      className={` formik-input-user-form form-control ${this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                        if (validationitems.id == i && validationitems.name == listingGroupInfo.row) {
                                                          return (' addlisting-border ');
                                                        }
                                                      }
                                                      )}`}
                                                      value={x.row}
                                                      onChange={(e) => this.appendedFormRowOnchange(e, i)}
                                                      onBlur={handleBlur}
                                                    />

                                                    {/* Validation Section Start */}
                                                    {this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                      if (validationitems.id == i && validationitems.name == listingGroupInfo.row) {
                                                        return (
                                                          <div className="add-listing-validation-error"> {validationitems.message} </div>
                                                        )
                                                      }
                                                    })}
                                                    {/* Validation Section End */}

                                                    <ErrorMessage name="row" component="div" className="invalid-feedback" />
                                                  </div>
                                                </div>
                                                <div className="di-form-items">
                                                  <div className="form-group add-listing-form-group">
                                                    <label>Seat Start <span>*</span></label>

                                                    <input
                                                      type="number"
                                                      name="seat_start"
                                                      key={("seat_start" + i + 1).toString()}
                                                      placeholder="Seat Start"
                                                      className={` formik-input-user-form form-control ${this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                        if (validationitems.id == i && validationitems.name == listingGroupInfo.seatStart) {
                                                          return (' addlisting-border ');
                                                        }

                                                      }
                                                      )}`}
                                                      value={x.seat_start}
                                                      onChange={(e) => this.appendedFormSeatStartOnchange(e, i)}
                                                      onKeyDown={e => (e.keyCode === 69 || e.keyCode === 190 || e.keyCode === 110) && e.preventDefault()}
                                                      onBlur={handleBlur}
                                                    />
                                                    {/* Validation Section Start */}
                                                    {this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                      if (validationitems.id == i && validationitems.name == listingGroupInfo.seatStart) {
                                                        return (
                                                          <div className="add-listing-validation-error"> {validationitems.message} </div>
                                                        )
                                                      }
                                                    })}
                                                    {/* Validation Section End */}
                                                    <ErrorMessage name="seat_start" component="div" className="invalid-feedback" />
                                                  </div>
                                                </div>
                                                <div className="di-form-items">
                                                  <div className="form-group add-listing-form-group">
                                                    <label>Seat End <span>*</span></label>

                                                    <input
                                                      type="number"
                                                      name="seat_end"
                                                      key={("seat_end" + i + 1).toString()}
                                                      placeholder="Seat End"
                                                      className={`formik-input-user-form form-control ${this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                        if (validationitems.id == i && validationitems.name == listingGroupInfo.seatEnd) {
                                                          return (' addlisting-border ');
                                                        }

                                                      }
                                                      )}`}
                                                      value={x.seat_end}
                                                      onChange={(e) => this.appendedFormSeatEndOnchange(e, i)}
                                                      onKeyDown={e => (e.keyCode === 69 || e.keyCode === 190 || e.keyCode === 110) && e.preventDefault()}
                                                      onBlur={handleBlur}
                                                    />
                                                    {/* Validation Section Start */}
                                                    {this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                      if (validationitems.id == i && validationitems.name == listingGroupInfo.seatEnd) {
                                                        return (
                                                          <div className="add-listing-validation-error"> {validationitems.message} </div>
                                                        )
                                                      }
                                                    })}
                                                    <ErrorMessage name="seat_end" component="div" className="invalid-feedback" />
                                                    {/* Validation Section End */}
                                                  </div>
                                                </div>
                                              </>
                                            }
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group">
                                                <label>Price <span>*</span></label>
                                                <input
                                                  className={`formik-input-user-form form-control ${this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                    if (validationitems.id == i && validationitems.name == listingGroupInfo.price) {
                                                      return (' addlisting-border ');
                                                    }

                                                  }
                                                  )}`}
                                                  type="number"
                                                  name="price"
                                                  placeholder="$"
                                                  value={x.price}
                                                  onChange={(e) => this.appendedFormPriceOnchange(e, i)}
                                                  onBlur={handleBlur}
                                                  onKeyDown={e => (e.keyCode === 69 || e.keyCode === 190) && e.preventDefault()}
                                                  min={0}
                                                />
                                                <ErrorMessage name="price" component="div" className="invalid-feedback" />

                                                {/* Validation Section Start */}
                                                {this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                  if (validationitems.id == i && validationitems.name == listingGroupInfo.price) {
                                                    return (
                                                      <div className="add-listing-validation-error"> {validationitems.message} </div>
                                                    )
                                                  }
                                                })}
                                                {/* Validation Section End */}

                                              </div>
                                            </div>
                                            <div className="di-form-items">

                                              <div className="form-group add-listing-form-group">
                                                <label>Face Value</label>
                                                <input
                                                  type="number"
                                                  name="face_value"
                                                  placeholder="Face Value"
                                                  className={`formik-input-user-form form-control ${(errors.face_value && touched.face_value ? ' is-invalid' : '')}`}
                                                  value={x.face_value}
                                                  onChange={(e) => this.appendedFormFaceValueOnchange(e, i)}
                                                  onKeyDown={e => (e.keyCode === 69 || e.keyCode === 190) && e.preventDefault()}
                                                  onBlur={handleBlur}
                                                  min="0"
                                                />
                                                <ErrorMessage name="face_value" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group">
                                                <label>Cost <span>*</span></label>
                                                <input
                                                  type="number"
                                                  name="cost"
                                                  placeholder="$"
                                                  // className={'formik-input-user-form form-control' + (errors.cost && touched.cost ? ' is-invalid' : '')}
                                                  className={`formik-input-user-form form-control ${this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                    if (validationitems.id == i && validationitems.name == listingGroupInfo.cost) {
                                                      return (' addlisting-border ');
                                                    }

                                                  }
                                                  )}`}
                                                  value={x.cost}
                                                  onChange={(e) => this.appendedFormCostOnchange(e, i)}
                                                  onKeyDown={e => (e.keyCode === 69 || e.keyCode === 190) && e.preventDefault()}
                                                  onBlur={handleBlur}
                                                  min="1"
                                                />
                                                <ErrorMessage name="cost" component="div" className="invalid-feedback" />

                                                {/* Validation Section Start */}
                                                {this.state.inputListValidation && this.state.inputListValidation.map((validationitems, z) => {
                                                  if (validationitems.id == i && validationitems.name == listingGroupInfo.cost) {
                                                    return (
                                                      <div className="add-listing-validation-error"> {validationitems.message} </div>
                                                    )
                                                  }
                                                })}
                                                {/* Validation Section End */}
                                              </div>
                                            </div>
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group">
                                                <label>Unit Cost</label>
                                                <input
                                                  type="number"
                                                  name="unit_cost"
                                                  placeholder="$"
                                                  className={'formik-input-user-form form-control' + (errors.unit_cost && touched.unit_cost ? ' is-invalid' : '')}
                                                  value={x.unit_cost}
                                                  onChange={(e) => this.appendedFormUnitCostOnchange(e, i)}
                                                  onBlur={handleBlur}
                                                  min="0"
                                                  readOnly
                                                />
                                                <ErrorMessage name="unit_cost" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group multi-select-item">
                                                <label>
                                                  Disclosures
                                                  {/* <i className="fa fa-plus-circle add-disclosure-icon" title="Add Disclosures" onClick={this.AddDisclosureOnclick} aria-hidden="true"></i> */}
                                                </label>

                                                <Select
                                                  {...this.props}
                                                  placeholder={"Disclosures"}
                                                  style={{ width: "100%" }}
                                                  name="disclosures"
                                                  filterOption={this.dropdownFilterOption}
                                                  value={this.state.inputList[i].disclosure ? JSON.parse(this.state.inputList[i].disclosure) : []}
                                                  options={disclosureValues}
                                                  onChange={(e) => this.appendedFormDisclosureOnchange(e, i)}
                                                  className={(errors.disclosures && touched.disclosures ? ' is-invalid' : '')}
                                                  allowClear
                                                  showSearch
                                                  isMulti
                                                  isClearable
                                                />
                                                <ErrorMessage name="disclosure" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group multi-select-item">
                                                <label>
                                                  Attributes
                                                  <i className="fa fa-plus-circle add-disclosure-icon" title="Add Attributes" onClick={this.AddAttributeOnclick} aria-hidden="true"></i>
                                                </label>


                                                <Select
                                                  {...this.props}
                                                  placeholder={"Attributes"}
                                                  style={{ width: "100%" }}
                                                  name="attributes"
                                                  filterOption={this.dropdownFilterOption}
                                                  value={this.state.inputList[i].attribute ? JSON.parse(this.state.inputList[i].attribute) : []}
                                                  options={AttrValues}
                                                  onChange={(e) => this.appendedFormAttributeOnchange(e, i)}
                                                  className={(errors.attributes && touched.attributes ? ' is-invalid' : '')}
                                                  allowClear
                                                  showSearch
                                                  isClearable
                                                  isMulti
                                                />

                                                <ErrorMessage name={"attribute_" + i + 1} component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group">
                                                <label>PO ID</label>
                                                <input
                                                  type="text"
                                                  name="po_id"
                                                  placeholder="PO ID"
                                                  className={'formik-input-user-form form-control' + (errors.po_id && touched.po_id ? ' is-invalid' : '')}
                                                  value={x.po_id}
                                                  onChange={(e) => this.appendedFormPOIDOnchange(e, i)}
                                                  onBlur={handleBlur}
                                                  maxlength={20}
                                                />
                                                <ErrorMessage name="po_id" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                            <div className="di-form-items">
                                              <div className="form-group add-listing-form-group multi-select-item">
                                                <label>
                                                  Tags
                                                  <i className="fa fa-plus-circle add-disclosure-icon" title="Add Tags" onClick={this.AddTagOnclick} aria-hidden="true"></i>
                                                </label>
                                                <Select
                                                  {...this.props}
                                                  placeholder={"Tags"}
                                                  style={{ width: "100%" }}
                                                  name="tags"
                                                  filterOption={this.dropdownFilterOption}
                                                  value={this.state.inputList[i].tags ? JSON.parse(this.state.inputList[i].tags) : []}
                                                  options={tagValues}
                                                  onChange={(e) => this.appendedFormTagsOnchange(e, i)}
                                                  className={(errors.tags && touched.tags ? ' is-invalid' : '')}
                                                  allowClear
                                                  showSearch
                                                  isClearable
                                                  isMulti
                                                />
                                                <ErrorMessage name="tags" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                          </div>
                                          <div className="form-data-grid-2">
                                            <div className="div-two-form--item">
                                              <div className="form-group add-listing-form-group">
                                                <label>Internal Notes</label>
                                                <textarea
                                                  name="internal_notes"
                                                  className={'formik-input-user-form form-control add-listing-text-area' + (errors.internal_notes && touched.internal_notes ? ' is-invalid' : '')}
                                                  value={x.internal_notes}
                                                  onChange={(e) => this.appendedFormInternalNotesOnchange(e, i)}
                                                  onBlur={handleBlur}
                                                  placeholder="Note here"
                                                ></textarea>
                                                <ErrorMessage name="internal_notes" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                            <div className="div-two-form--item">
                                              <div className="form-group add-listing-form-group">
                                                <label>External Notes</label>
                                                <textarea
                                                  name="external_notes"
                                                  className={'formik-input-user-form form-control add-listing-text-area' + (errors.external_notes && touched.external_notes ? ' is-invalid' : '')}
                                                  value={x.external_notes}
                                                  onChange={(e) => this.appendedFormExternalNotesOnchange(e, i)}
                                                  onBlur={handleBlur}
                                                  placeholder="Note here"
                                                ></textarea>
                                                <ErrorMessage name="external_notes" component="div" className="invalid-feedback" />
                                              </div>
                                            </div>
                                          </div>
                                          {i ?
                                            <div>
                                              <button type="button" className="btn btn-danger group-remove-btn" onClick={() => this.handleRemoveClick(i)} >Cancel</button>
                                            </div>
                                            :
                                            ''
                                          }
                                          {
                                            this.state.addGroupButtonId == i && <div className="submit-btn-addlisting-section">
                                              <button type="button" className="btn add-newgroup-btn" onClick={(e) => this.handleAddClick(i)}>Add New Group</button>
                                            </div>
                                          }

                                        </div>
                                      </div>
                                    </div>
                                  </>
                                );
                              })}


                              {/* append field end */}


                            </div>
                          </div>



                          {/* Attachment Section Attachment Start */}








                          <div className="row m-0 justifyContent-item">




                            {/* my attachment start */}


                            {this.state.inputList && this.state.inputList.map((x, i) => {

                              //Attachment Loop
                              var attachments = x.attachments;
                              const items = [];
                              if (attachments && attachments.length) {
                                for (var attachmentCount = 0; attachmentCount < attachments.length; attachmentCount++) {
                                  items.push(



                                    <div className="upload--item">
                                      {this.state.Is_show_sectiondiv ?
                                        <label className="form-label-userprofile">
                                          Seat {attachmentCount + 1}
                                        </label> : <label className="form-label-userprofile">
                                          Ticket {attachmentCount + 1}
                                        </label>}
                                      <div className="upload-item channel-markup-row input-row">
                                        <div className="ticket-count">
                                          <FormInput
                                            type="text"
                                            placeholder="Ticket Number"
                                            id={attachments[attachmentCount].attachment_seatno}
                                            value={attachments[attachmentCount].attachment_text}
                                            onChange={(e) => this.appendedFormAttachmentTextOnchange(e, i)}
                                          />
                                        </div>
                                        {attachments[attachmentCount].attachment_file_data && attachments[attachmentCount].attachment_file_data.name ? 
                                          <>
                                            <div class="attach-btn">
                                              <AttachmentIcon/> {attachments[attachmentCount].attachment_file_data.name}
                                            </div> 
                                            <div className="file-replace-section">
                                              <div className='file file--upload'>
                                                <FormInput
                                                  type="file"
                                                  name="attachment"
                                                  id={attachments[attachmentCount].attachment_seatno}
                                                  onChange={(e) => this.appendedFormAttachmentOnchange(e, i)}
                                                />
                                              </div>
                                              <span className="replace-order-btn">Replace</span>
                                            </div>
                                            {attachments[attachmentCount] && attachments[attachmentCount].attachment_seatno &&
                                              <div className="delete-section">
                                                <Button className="invoice-delete--btn" data-id={attachments[attachmentCount].attachment_seatno} onClick={ (e) => this.removeAttachment(e, i) } >Delete</Button>
                                              </div>
                                            }
                                          </>
                                          :
                                        <div className="ticket-attached-btn">
                                          <div className='file file--upload'>
                                            <label htmlFor='input-file'>
                                              <AttachmentIcon />
                                            </label>
                                            <FormInput
                                              type="file"
                                              name="attachment"
                                              id={attachments[attachmentCount].attachment_seatno}
                                              onChange={(e) => this.appendedFormAttachmentOnchange(e, i)}
                                            />
                                          </div>
                                          <span className="attach-item">Attach</span>
                                        </div>
                                        }                
                                        
                                      </div>
                                    </div>


                                  );
                                }
                              }



                              {/* my attachment end */ }

                              return (
                                items && items.length ?
                                  <div className="card bottom-switch-card-section" >
                                    <div>
                                      <div className="card-body">
                                        <div className="channel-management-sec mt-0">

                                          <div className="channel-title">
                                            <h4>Attached Tickets</h4>
                                          </div>
                                          <div className="add-listing-group-name"><h4>Group #{i + 1}</h4></div>
                                          <Row className="m-0">
                                            <Form>
                                              {items}
                                            </Form>
                                          </Row>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  : ''
                              );
                            })}


                            {/* Attachment Section Attachment End */}


                            {this.state.Is_show_localpickup ?
                              <div className="card bottom-switch-card-section">
                                <div className="card-body">
                                  <div className="channel-management-sec mt-0">
                                    <div className="channel-title">
                                      <h4>Pickup Information</h4>
                                    </div>
                                    <div className="channel-inner-sec">
                                      <Form >
                                        <h3 className="channel-left-title">Contact Info:</h3>
                                        <div className="pickup-information-sec">
                                          <div className="form-group">
                                            <input className="form-control" type="text" placeholder="Name" />
                                          </div>
                                          <div className="form-group">
                                            <input className="form-control" type="email" placeholder="Email Address" />
                                          </div>
                                          <div className="form-group">
                                            <input className="form-control" type="text" placeholder="Phone Number" />
                                          </div>
                                        </div>
                                        <h3 className="channel-left-title">Pickup Address:</h3>
                                        <div className="pickup-information-sec">
                                          <div className="form-group">
                                            <input className="form-control" type="text" placeholder="Street Address 1*" />
                                          </div>
                                          <div className="form-group">
                                            <input className="form-control" type="text" placeholder="Street Address 2 " />
                                          </div>
                                          <div className="form-group">
                                            <div className="d-flex">
                                              <div className="city--item">
                                                <input className="form-control" type="text" placeholder="City*" />
                                              </div>
                                              <div className="state--item">
                                                <input className="form-control" type="text" placeholder="State/Country*" />
                                              </div>
                                              <div className="zipcode--item">
                                                <input className="form-control" type="text" placeholder="Zip Code*" />
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </Form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              : ''}

                            <div className="card bottom-switch-card-section">
                              <div className="card-body">
                                <div className="channel-management-sec">
                                  <div className="channel-title">
                                    <h4>Channel Management</h4>
                                  </div>


                                  {this.state.channelManagmentInputList && this.state.channelManagmentInputList.map((x, i) => {
                                    return (
                                      <div className="channel-inner-sec">
                                        <div className="row align-items-center" key={x.channel_markup_name}>
                                          <div className="col-md-4">
                                            <h3 className="channel-left-title">{x.channel_markup_name}</h3>
                                          </div>
                                          <div className="col-md-4 p-0">
                                            <div className="d-flex">
                                              <div className="left-switch-item">
                                                <div className="button-switch">
                                                  <Switch className="filter-btn-hide--seat"
                                                    checkedChildren="ON"
                                                    unCheckedChildren="OFF"
                                                    checked={x.switch}
                                                    onChange={(e) => this.toggleSwitch(e, i)}
                                                  />
                                                </div>
                                              </div>
                                              {x.switch &&
                                                <div className="right-switch-item">
                                                  <div className="button-switch">
                                                    <Switch className="price-switch-btn"
                                                      checkedChildren="%"
                                                      unCheckedChildren="$"
                                                      // defaultChecked
                                                      key={'channel_' + i}
                                                      onChange={(e) => this.channelManagmentToggleOnchange(e, i)}
                                                    />
                                                  </div>
                                                </div>}
                                            </div>
                                          </div>
                                          <div className="col-md-4">
                                            <div className="plus-minus-btn">
                                              <div className="price-change-txt">
                                                <input className="form-control"
                                                  type="number"
                                                  min="0"
                                                  max={this.state.channelManage[i] ? this.state.channelManage[i] : 100}
                                                  key={'channel_value' + i}
                                                  placeholder={x.channel_markup_type_id && x.channel_markup_type_id == 1 ? '%' : '$'}
                                                  value={x.markup_amount}
                                                  disabled={!x.switch}
                                                  onChange={(value) => this.channelManagmentOnchange(value, i)}
                                                />
                                              </div>
                                              <button className="price-add-btn primary-border-color" type="button" disabled={!x.switch} onClick={(value) => this.channelManagmentIncressOnchange(value, i)}><AddIcon /></button>
                                              <button className="price-mini-btn" type="button" disabled={!x.switch} onClick={(value) => this.channelManagmentDecressOnchange(value, i)}><MinusIcon /></button>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                    );
                                  }
                                  )}

                                </div>
                              </div>
                            </div>
                          </div>

                          {/* Channel Management Section End */}

                          <div className="list-creat--btn-section">
                            <button className="btn creating--list-btn" disabled={this.state.disable_addlist} >Create Listing</button>
                            <button className="btn creat-purchase--list-btn" onClick={(e) => this.setState({ addPurchaseOrder: true })}>Create Listing and Purchase Order</button>
                            <button className="btn cancel--list-btn" type="reset" >Cancel</button>
                          </div>
                          {/* <div className="row">
                            <div className="col-sm-2">
                              <button type="submit"className="btn btn-success rounded-pill" >Create listing</button>
                            </div>
                            <div className="col-sm-2">
                              <button type="reset" className="btn btn-secondary  rounded-pill">Cancel</button>
                            </div>
                          </div><br />
                          <div className="row">
                            <div className="col-sm-4">
                              <button type="button" className="btn btn-primary btn-block rounded-pill">Create listing and Create PO</button>
                            </div>
                          </div> */}
                        </Form>
                      )}
                    />
                  </div>
                  {/* Add Listing Form End */}


                  {    /**
                 * Page : Add Listing
                 * Function For : Insert Lookup Data for Ticketing System, Disclosures,  Disclosures Model popup
                 * Ticket No : TIC-113, 124
                 */
                  }

                  <Modal
                    visible={this.state.statusModalMessage}
                    onCancel={(e) => this.setState({
                      responseModalMessage: '',
                      statusModalMessage: false
                    })}
                    footer={false}
                    maskClosable={true}
                    closable={false}
                    maskClosable={false}
                    className="add--listing-page-modal">
                    <Row>
                      <Col md="12" className="form-group">
                        <div className="update-price-item">
                          <h3 className="success-para">{this.state.responseModalMessage}</h3>
                        </div>
                      </Col>
                      <Col md="12" className="form-group text-center">
                        <button type="button" onClick={(e) => this.setState({ responseModalMessage: '', statusModalMessage: false })} className="btn success-btn-close">Close</button>
                      </Col>

                    </Row>
                  </Modal>

                  <Modal
                    visible={this.state.removeNewGroup}
                    onCancel={(e) => this.setState({

                      removeNewGroup: false
                    })}
                    footer={false}
                    maskClosable={true}
                    closable={false}
                    maskClosable={false}
                    className="add--listing-page-modal">
                    <Row>
                      <Col md="12" className="form-group">
                        <div className="update-price-item">
                          <h3 className="success-para">{this.props.t("listing.group.remove")}</h3>
                        </div>
                      </Col>
                      <Col md="12" className="form-group text-center">
                        <button type="button" onClick={(e) => this.setState({ removeNewGroup: false, })} className="btn success-btn-close">Close</button>
                      </Col>

                    </Row>
                  </Modal>


                  {/* Lookup data Model Popup Start*/}
                  <Modal
                    title={this.state.isAppendModelPopup.title}
                    visible={this.state.isAppendModelPopup.isModalVisible}
                    onCancel={this.AppendModelCancelOnclick}
                    footer={false}
                    maskClosable={false}
                    maskClosable={false}
                    className="add--listing-page-modal">


                    {(() => {
                      if (this.state.isAppendModelPopup.type == listingGroupInfo.attribute) {
                        return (
                          <div key={'attribute'} >
                            {/* Attribute Form Start */}
                            <Formik
                              initialValues={{
                                attribute_name: this.state.newAttribute
                              }}
                              enableReinitialize={true}
                              validationSchema={Yup.object().shape({
                                attribute_name: Yup.string()
                                  .required('Attribute name is required')
                              })}

                              onSubmit={async (values, { resetForm }) => {
                                //resetForm({ attribute_name: '' })
                                if (this.state.newAttribute) {
                                  var data = [{ "attribute_name": this.state.newAttribute }];
                                  var response = await Createattribute(data, this.state.userId);
                                }
                                this.setState({
                                  responseModalMessage: response.message,
                                  statusModalMessage: true,
                                  newAttribute: ''
                                })
                                if (response.status == responseStatus.ok) {
                                  this.AppendModelCancelOnclick();
                                }

                              }}


                              render={({ errors, status, touched, values, handleChange, handleBlur, handleSubmit }) => (
                                <Form>

                                  <Row>
                                    <Col md="12" className="form-group">
                                      <label htmlFor="attribute_name">Attribute Name</label>
                                      <Field
                                        name="attribute_name"
                                        className={'formik-input-modal-form form-control' + (errors.attribute_name && touched.attribute_name ? ' is-invalid' : '')}
                                        placeholder="Attribute"
                                        value={values.attribute_name}
                                        onChange={(e) => this.setState({ newAttribute: e.target.value })}
                                        onBlur={handleBlur}
                                      />
                                      <ErrorMessage name="attribute_name" component="div" className="invalid-feedback" />
                                    </Col>

                                    <Col md="12" className="form-group text-center">
                                      <button type="submit" className="btn btn-primary rounded-pill">Submit</button>
                                    </Col>
                                  </Row>

                                </Form>
                              )}
                            />
                            {/* Attribute Form Start */}
                          </div>
                        )
                      } else if (this.state.isAppendModelPopup.type == listingGroupInfo.disclosure) {
                        return (
                          <div key={'disclosure'}>

                            {/* Disclosure Form Start */}
                            <Formik
                              initialValues={{
                                disclosure_name: ''
                              }}

                              validationSchema={Yup.object().shape({
                                disclosure_name: Yup.string()
                                  .required('Disclosure name is required')
                              })}

                              onSubmit={async (values, { resetForm }) => {
                                resetForm({ disclosure_name: '' })
                                if (values.disclosure_name) {
                                  var data = [{ "disclosure_name": values.disclosure_name }];
                                  var response = await Createdisclosure(data, this.state.userId);
                                }

                                if (response.status == responseStatus.ok) {
                                  this.AppendModelCancelOnclick();
                                }

                              }}

                              render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                                <Form>

                                  <Row>
                                    <Col md="12" className="form-group">
                                      <label htmlFor="disclosure_name">Disclosure Name</label>
                                      <Field
                                        name="disclosure_name"
                                        className={'formik-input-modal-form form-control' + (errors.disclosure_name && touched.disclosure_name ? ' is-invalid' : '')}
                                        placeholder="Disclosure"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                      />
                                      <ErrorMessage name="disclosure_name" component="div" className="invalid-feedback" />
                                    </Col>

                                    <Col md="12" className="form-group text-center">
                                      <button type="submit" className="btn btn-primary rounded-pill">Submit</button>
                                    </Col>
                                  </Row>

                                </Form>
                              )}
                            />
                            {/* Disclosure Form Start */}

                          </div>
                        )
                      } else if (this.state.isAppendModelPopup.type == listingGroupInfo.ticketSystem) {
                        return (
                          <div key={'ticketing_system'}>

                            {/* Ticketing system Form Start */}
                            <Formik
                              initialValues={{
                                ticketing_system_name: '',
                                country_id: ''
                              }}

                              validationSchema={Yup.object().shape({
                                ticketing_system_name: Yup.string()
                                  .required('Ticketing system name is required'),
                                country_id: Yup.string()
                                  .required('Country is required')
                              })}

                              onSubmit={async (values, { resetForm }) => {
                                resetForm({ ticketing_system_name: '', country_id: '' })
                                if (values.ticketing_system_name) {
                                  var data = [{
                                    "ticketing_system_name": values.ticketing_system_name,
                                    "country_id": values.country_id
                                  }];
                                  var response = await Createticketingsystem(data);
                                }

                                if (response.status == responseStatus.ok) {
                                  this.AppendModelCancelOnclick();
                                }

                              }}

                              render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                                <Form>
                                  <Row>
                                    <Col md="12" className="form-group">
                                      <label htmlFor="ticketing_system_name">Ticketing System  Name</label>
                                      <Field
                                        name="ticketing_system_name"
                                        className={'formik-input-modal-form form-control' + (errors.ticketing_system_name && touched.ticketing_system_name ? ' is-invalid' : '')}
                                        placeholder="Ticketing System"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                      />
                                      <ErrorMessage name="ticketing_system_name" component="div" className="invalid-feedback" />
                                    </Col>

                                    <Col md="12" className="form-group">
                                      <label htmlFor="ticketing_system_name">Country</label>

                                      <select
                                        name="country_id"
                                        className={'formik-input-modal-form form-control' + (errors.country_id && touched.country_id ? ' is-invalid' : '')}
                                        value={values.country_id}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                      >
                                        <option value="">Select Country</option>
                                        {this.state.getCountryList && this.state.getCountryList.length > 0
                                          && this.state.getCountryList.map((item, i) => {
                                            return (
                                              <option key={i} value={item.id}>{item.name}</option>
                                            )
                                          })}
                                      </select>

                                      <ErrorMessage name="country_id" component="div" className="invalid-feedback" />
                                    </Col>

                                    <Col md="12" className="form-group text-center">
                                      <button type="submit" className="btn btn-primary rounded-pill">Submit</button>
                                    </Col>
                                  </Row>
                                </Form>
                              )}
                            />
                            {/* Ticketing system Form Start */}

                          </div>
                        )
                      } else if (this.state.isAppendModelPopup.type == listingGroupInfo.tag) {
                        return (
                          <div key={'tag'}>

                            {/* Ticketing system Form Start */}
                            <Formik
                              initialValues={{
                                tag_name: this.state.newTags
                              }}

                              validationSchema={Yup.object().shape({
                                tag_name: Yup.string()
                                  .required('Tag name is required')
                              })}
                              enableReinitialize={true}

                              onSubmit={async (values, { resetForm }) => {
                                // resetForm({ tag_name: '' })
                                if (this.state.newTags) {
                                  var data = [{
                                    "tag_name": this.state.newTags
                                  }];
                                  var response = await Createtag(data, this.state.userId);
                                }
                                this.setState({
                                  responseModalMessage: response.message,
                                  statusModalMessage: true,
                                  newTags: ''
                                })
                                if (response.status == responseStatus.ok) {
                                  this.AppendModelCancelOnclick();
                                }

                              }}

                              render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                                <Form>
                                  <Row>
                                    <Col md="12" className="form-group">
                                      <label htmlFor="tag_name">Tag Name</label>
                                      <Field
                                        name="tag_name"
                                        className={'formik-input-modal-form form-control' + (errors.tag_name && touched.tag_name ? ' is-invalid' : '')}
                                        placeholder="Enter tag name"
                                        value={this.state.newTags}
                                        onChange={(e) => this.setState({ newTags: e.target.value })}
                                        onBlur={handleBlur}
                                      />
                                      <ErrorMessage name="tag_name" component="div" className="invalid-feedback" />
                                    </Col>

                                    <Col md="12" className="form-group text-center">
                                      <button type="submit" className="btn btn-primary rounded-pill">Submit</button>
                                    </Col>
                                  </Row>
                                </Form>
                              )}
                            />
                            {/* Ticketing system Form Start */}

                          </div>
                        )
                      } else if (this.state.isAppendModelPopup.type == listingGroupInfo.ticketingSystemAccount) {
                        return (
                          <div key={'ticketingSystemAccount'}>

                            {/* Disclosure Form Start */}
                            <Formik
                              initialValues={{
                                email: '',
                              }}

                              validationSchema={Yup.object().shape({
                                email: Yup.string()
                                  .required('User Email is required')
                              })}

                              onSubmit={async (values, { resetForm }) => {
                                var response = [];
                                if (values.email && this.state.singleSelect.ticketing_system && this.state.singleSelect.ticketing_system[0]) {
                                  var data = { "email": values.email, "ticket_system_id": this.state.singleSelect.ticketing_system[0].value };
                                  response = await createTicketingSystemAccount(data, this.state.userId);
                                }

                                if (response.status == responseStatus.ok) {
                                  resetForm({ email: '' })
                                  this.AppendModelCancelOnclick();
                                }
                                if (response.status == responseStatus.error) {
                                  this.setState({ userEmailError: response.message });
                                } else {
                                  this.setState({ userEmailError: null });
                                }

                              }}

                              render={({ errors, status, touched, values, handleChange, handleBlur }) => (

                                <Form>

                                  <Row>
                                    <Col md="12" className="form-group text-center">
                                      <label htmlFor="email">Email</label>
                                      <Field
                                        name="email"
                                        className={'formik-input-modal-form form-control' + (errors.email && touched.email ? ' is-invalid' : '')}
                                        placeholder="User Email"
                                        onChange={(e) => { handleChange(e); this.setState({ userEmailError: '' }) }}

                                        onBlur={handleBlur}
                                      />
                                      {values.email && this.state.userEmailError && <div style={{ color: "red" }}> {this.state.userEmailError} </div>}
                                      <ErrorMessage name="email" component="div" className="invalid-feedback" />
                                    </Col>

                                    <Col md="12" className="form-group">
                                      <button type="submit" className="btn btn-primary rounded-pill">Submit</button>
                                    </Col>
                                  </Row>

                                </Form>
                              )}
                            />
                            {/* Ticketing system Form Start */}

                          </div>
                        )
                      }

                    })()}

                  </Modal>

                  {/* Lookup data Model Popup End*/}


                  {/* Common Modal popup start */}

                  {/* Comfirm Modal */}
                  <Confirm
                    header={"Confirm"}
                    body={"Are you sure to submit"}
                    show={this.state.closeButtonModal}
                    submit={() => this.handlesubmitButtonModal(true)}
                    cancel={() => this.handlesubmitButtonModal(false)}
                    closeButton={() => this.setState({ closeButtonModal: false })}
                  />

                  {/* Info Modal */}
                  <Info
                    header={"Info"}
                    body={this.state.infomodalmsg}
                    show={this.state.infocloseButtonModal}
                    submit={() => this.handleInfoSubmitButtonModal(true)}
                    closeButton={() => this.handleInfoSubmitButtonModal(true)}
                  />

                  {/* Common Modal popup end */}


                </div>
              </div>
            </div>
          )}
        </UserContext.Consumer>
      </div >
    ) : (
      <Loader />
    );
  }
}

export default withRouter(withTranslation()(MyAddListingForms))
