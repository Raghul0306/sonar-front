import React, { Component } from "react";
import { getTokenUser } from "./../services/userService";
import { Container } from "shards-react";
import { onboardingStage } from "../constants/constants";

import { OnboardingResetPassForm } from "../components/onboarding/resetPass";
import { OnboardingAcceptTermsForm } from "../components/onboarding/acceptTerms";
import { OnboardingAcceptPrivacyForm } from "../components/onboarding/acceptPrivacy";
import { OnboardingSellerAgreementForm } from "../components/onboarding/sellerAgreement";
import { BankAccountSyncForm } from "../components/onboarding/bankAccountSync";
import CompanyInformation from "../components/onboarding/companyInformation";
import WelcomeCard from "../components/onboarding/welcomeCard";
import * as moment from "moment-timezone";
import ClipLoader from "react-spinners/ClipLoader";

export class VerifyToken extends Component {

    constructor(props) {
        super(props);
        this.state = {
            stage: '',
            user: []
        }

        this.changeCardStage = this.changeCardStage.bind(this);
    }

    calculateStage(userInfo) {
        let currentStage;
        if (userInfo.needsToChangePassword) {
            currentStage = onboardingStage.zero;
        }
        else if (userInfo.company_id == null || userInfo.street_address_1 == null || !userInfo.street_address_1) {
            currentStage = onboardingStage.one;
        }
        else if (userInfo.needsToAcceptTerms) {
            currentStage = onboardingStage.two;
        }
        else if (userInfo.needsToAcceptPrivacy) {
            currentStage = onboardingStage.three;
        }
        else if (userInfo.needsToAcceptSeller) {
            currentStage = onboardingStage.four;
        }
        else if (userInfo.needsToSyncBank) {
            currentStage = onboardingStage.five;
        }
        else {
            currentStage = onboardingStage.six;
        }

        this.setState({ stage: currentStage });
    }

    async componentDidMount() {
        let tokenString = window.location.pathname.replace('/verify-email/', '');
        let token_user = tokenString.length > 0 ? await getTokenUser(tokenString) : this.invalidAccessRedirection();
        if (!token_user.status) {
            this.invalidAccessRedirection();
        } else {
            this.calculateStage(token_user.token_user);
            this.setState({ user: token_user.token_user, tokenString });
        }
    }

    invalidAccessRedirection() {
        window.location.href = window.location.origin;
    }

    changeCardStage() {
        let currentStage = this.state.stage;
        this.setState({ stage: currentStage + 1 });
    }


    render() {

        return this.state.stage !== "" ? (
            <div style={{
                display: "flex",
                flexDirection: 'column',
                alignItems: "center",
                justifyContent:'flex-start',
                height: "100vh",
                overflow:'hidden',
                overflowY:'auto'
            }}>

                <Container fluid className="main-content-container px-4 pb-4">
                    {this.state.stage == 0 && (
                        <OnboardingResetPassForm user={this.state.user} stage={this.state.stage} setUser={this.changeCardStage} />
                    )}
                    {this.state.stage == 1 && (
                        <CompanyInformation user={this.state.user} stage={this.state.stage} setUser={this.changeCardStage} />
                    )}
                    {this.state.stage == 2 && (
                        <OnboardingAcceptTermsForm user={this.state.user} stage={this.state.stage} setUser={this.changeCardStage} />
                    )}
                    {this.state.stage == 3 && (
                        <OnboardingAcceptPrivacyForm user={this.state.user} stage={this.state.stage} setUser={this.changeCardStage} />
                    )}
                    {this.state.stage == 4 && (
                        <OnboardingSellerAgreementForm user={this.state.user} stage={this.state.stage} setUser={this.changeCardStage} />
                    )}
                    {this.state.stage == 5 && (
                        <BankAccountSyncForm user={this.state.user} stage={this.state.stage} setUser={this.changeCardStage} />
                    )}
                    {this.state.stage == 6 && (
                        <WelcomeCard user={this.state.user} stage={this.state.stage} setUser={this.changeCardStage} tokenString={this.state.tokenString} />
                    )}
                </Container>
                <footer class="main-footer d-flex p-2 px-3 bg-white border-top" style={{ marginTop: 'auto', width: '100%' }}>
                    <div class="container-fluid">
                        <div class="row">
                            <span class="copyright ml-auto my-auto mr-2">Copyright &copy; {moment.utc(new Date).format("YYYY")} Phunnel Inc</span>
                        </div>
                    </div>
                </footer>
            </div>
        ) : (
            <div className="loader-bg">
                <ClipLoader color={'#4a88c5'} loading={this.state.setLoader} size={40} />
            </div>
        );

    }
}
export default VerifyToken;
