import React, { Component } from "react";
import Button from 'antd/es/button';
import Input from 'antd/es/input';
import { withTranslation } from 'react-i18next';
import {
    getEbayAuthorizationURL,
} from "../../services/ebayService";

class ebayAuth extends Component {
  constructor(props) {
    super(props);
    this.state = {
        clientId: "",
        clientSecret: "",
        redirectUri: "",
        ebayAuthUrl: ""
    };
  }
  
  setClientId = event => {
      this.setState({clientId: event.target.value});
  }
  setClientSecret = event => {
      this.setState({clientSecret: event.target.value});
  }
  setRedirectUri = event => {
      this.setState({redirectUri: event.target.value});
  }

  submitEbayCredentials = async () => {
    this.setState({
      submitted: true,
      ebayAuthUrl: ""
    }, async () => {
      this.setState({
        ebayAuthUrl: await getEbayAuthorizationURL(this.state.clientId, this.state.clientSecret, this.state.redirectUri, "PRODUCTION")
      }, () => {
        if(this.state.ebayAuthUrl) {
          this.setState({
            submitted: false
          });
          return true;
        }
        return false;
      });
    });
  }

  render = () => {
    return (
        <>
          <Input addonBefore="Ebay Developer Client ID" value={this.state.clientId} onChange={this.setClientId} placeholder="Client ID" />
          <Input addonBefore="Ebay Developer Client Secret" value={this.state.clientSecret} onChange={this.setClientSecret} placeholder="Client Secret" />
          <Input addonBefore="Ebay Developer Redirect URI" value={this.state.redirectUri} onChange={this.setRedirectUri} placeholder="Redirect URI" />
          <Button type="primary" onClick={this.submitEbayCredentials}>Link Ebay Developer Account</Button>
          {this.state.ebayAuthUrl
            ? <a href={this.state.ebayAuthUrl} target="__blank">Your ebay authorization URL</a>
            : this.state.submitted ? <p>Please wait, generating ebay authorization URL<i class="fas fa-spinner fa-pulse"></i></p>
            : <></>
          }
        </>
    );
  }
}

export default withTranslation()(ebayAuth);