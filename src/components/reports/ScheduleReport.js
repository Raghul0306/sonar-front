import React, { useState, useContext } from "react";

import Button from 'antd/es/button';
import DatePicker from 'antd/es/date-picker';
import TimePicker from 'antd/es/time-picker';

import { useTranslation } from "react-i18next";
import UserContext from "../../contexts/user.jsx";
import Select from 'react-select'
// style icon
import styled from 'styled-components'
import calendarImg from "../../assets/myTicketsIconCalendar.png"
import clockImg from "../../assets/MyTickets-icon_clock.png"
import { Calendar } from '@styled-icons/boxicons-regular/Calendar'
import { Time } from '@styled-icons/ionicons-outline/Time'

const CalendarIcon = styled(Calendar)`
  color: #9f9f9f;
  width:25px;
  height:25px;
`
const TimeIcon = styled(Time)`
  color: #9f9f9f;
  width:25px;
  height:25px;
`

const { Option } = Select;


function ScheduleReport(props) {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const [frequency, SetFrequency] = useState("daily");
  const [type, setType] = useState("sales");
  const [plType, setPlType] = useState("item");

  const handleFrequency = value => {
    SetFrequency(value);
  };

  function handleChange(value) {
    setType(value);
  }
  function handlePlChange(value) {
    setPlType(value);
  }

  function dropdownFilterOption ({ label }, string) {
    if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };

  const reportList = [
    {value:1,label:"Sales Report"},
    {value:2,label:"Listings Report"},
    {value:3,label:"Delivery Report"},
    {value:4,label:"Delivery Report (Open Orders)"},
    {value:5,label:"Profit & Loss Report"}
  ]
  const frequencyList = [
    {value:1,label:"Daily"},
    {value:2,label:"Weekly"},
    {value:3,label:"Monthly"},
    {value:4,label:"Yearly"}
  ]


  return (
    <>
      {/* <div className="modalWithHead-floating-cancel-button">
        <Button shape="circle" size="large" onClick={props.handleCancel}>
          <CloseRoundedIcon
            style={{
              fontSize: "23px"
            }}
          />
        </Button>
      </div> */}
      
      <div className="popup-body modal-report-body">
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "repeat(3, 1fr)",
            width: "100%",
            marginTop: "10px",
            marginBottom: "10px",
          }}
        >
          <div className="report-custom-field" style={{ marginBottom: "10px" }}>
            <label className="form-label-userprofile">
              {t("reports.scheduleReport.reportType")}<span>*</span>
            </label>
            <div>
                <Select
                {...props}
                placeholder={t("reports.scheduleReport.reportType")}
                style={{ width: "100%" }}
                name="reportType"
                filterOption={dropdownFilterOption}
                options={reportList}
                allowClear
                showSearch
                isClearable
                />
              </div>
          </div>
          <div className="report-custom-field">
            <label className="form-label-userprofile">
            {t("form.startDate")} <span>*</span>
            </label>
            <div className="calendar-icon-field">
              <span className="calendar-icon"><img src={calendarImg}/></span>
              <DatePicker size="sm" />
            </div>
          </div>
          <div className="report-custom-field">
            <label className="form-label-userprofile">
            {t("form.endDate")}<span>*</span>
            </label>
            <div className="calendar-icon-field">
              <span className="calendar-icon"><img src={calendarImg}/></span>
              <DatePicker size="sm"/>
            </div>
          </div>
        </div>
        <div style={{
            display: "grid",
            gridTemplateColumns: "repeat(3, 1fr)",
            width: "100%",
            marginTop: "2rem",
            marginBottom: "10px",
          }}
        >
          <div className="report-custom-field">
            <label className="form-label-userprofile">Frequencyg <span>*</span></label>
              <Select
              {...props}
              placeholder={"Frequency"}
              style={{ width: "100%" }}
              name="frequency"
              filterOption={dropdownFilterOption}
              options={frequencyList}
              allowClear
              showSearch
              isClearable
              />
          </div>
          <div className="report-custom-field">
            <div className="schedule-reports-row" style={{ marginBottom: "10px" }}>
              {frequency !== "weekly" && (
                <div>
                  <label className="form-label-userprofile">
                    {t("reports.scheduleReport.Schedule")} <span>*</span>
                  </label>
                  <div className="calendar-icon-field">
                    <span className="calendar-icon"><img src={calendarImg}/></span>
                        <DatePicker
                          style={{ width: "100%" }}
                          // onChange={onChange}
                          picker={frequency}
                        />
                  </div>
                </div>
              )}
            </div>
          </div>
            <div className="report-custom-field">
              <label className="form-label-userprofile">
                {t("reports.scheduleReport.ScheduleTime")} <span>*</span>
              </label>
              <div className="calendar-icon-field">
                <span className="calendar-icon"><img src={clockImg}/></span>
                  <TimePicker
                  style={{ width: "100%" }}
                  // onChange={}
                />
              </div>
            </div>
        </div>
        <div className="text-center">
          <Button
            style={{ marginTop: "15px" }}
            type="primary"
            onClick={props.onSubmit}
            className="update-report-btn"
          >
            {t("reports.update")}
          </Button>
        </div>
      </div>
    </>
  );
}

export default ScheduleReport;
