import React, { useEffect, useState, useContext } from "react";
import loadable from "@loadable/component";
import UserContext from "../../contexts/user.jsx";
import { AgGridReact } from "ag-grid-react";
import moment from "moment";
import { Link } from "react-router-dom";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import Input from 'antd/es/input';
import { userRoles, userRolesLower, userRolesArray } from '../../constants/constants';
import {
  updateUserFirstName,
  updateUserLastName,
  updateUserNotes,
  sendMagicLink,
  updateUser,
  getUserById
} from "../../services/userService";

//For Moving to production, we are commenting this out since this is unused and old code 
//import { getNumberOfUserListings,  getNumberOfDeliveredUserListings } from "../../services/listingsService";

import { getAllUsers, getAllUsersByRole } from "../../services/usersService";
import { useTranslation } from "react-i18next";
import { autoSizeAllColumns, onGridSizeChanged } from "./DataGridHelpers";
import Loader from "../Loader/Loader";
//import Modal from 'antd/es/modal';
const SearchIcon = loadable(() => import("@material-ui/icons/Search"));

const AllMarketPlaceUsersGrid = (params) => {

  const { user } = useContext(UserContext);

  const { t } = useTranslation();
  // const {columnDefs, rowData} = store;
  const [rowData, setRowData] = useState([]);
  const [column, setColumn] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [rowSelection] = useState("single");
  //const [notesModalVisible, setNotesModalVisible] = useState(false);

  const [loading, setLoading] = useState(false);

  const phoneFormat = (phoneNumber) => {
    if (phoneNumber.value) {
      return '(' + phoneNumber.value.substr(0, 3) + ') ' + phoneNumber.value.substr(3, 3) + '-' + phoneNumber.value.substr(6);
    }
    else { return '-' }

  }

  function viewUserDetails(params) {
    window.open("/user/" + params.data.id, '_self');
  }

  useEffect(() => {
    setLoading(false);
  }, []);

  const columnDefs = [];

  useEffect(() => {
    if (user) {
      if (user.userRole == userRolesLower.admin) {
        columnDefs.push({
          headerName: t("seller.company"),
          field: "company_name"
        },
          {
            headerName: t("userInfo.firstName"),
            field: "first_name",
            onCellValueChanged: function (params) {
              updateFirstName(params);
            },
            editable: true
          },
          {
            headerName: t("userInfo.lastName"),
            field: "last_name",
            onCellValueChanged: function (params) {
              updateLastName(params);
            },
            editable: true
          },
          {
            headerName: t("userInfo.email"),
            field: "email"
          },
          {
            headerName: t("userInfo.phone"),
            field: "phone",
            cellRenderer: "phoneFormat"
          },
          {
            headerName: t("userInfo.role"),
            field: "user_role",
            valueFormatter: params => userRolesArray[parseInt(params.data.user_role) - 1],
          },
          {
            headerName: t("userInfo.notes"),
            field: "notes"
          },
          {
            headerName: t("userInfo.lastLogin"),
            field: "last_login",
            valueFormatter: (params) => {
              return params.data && params.data.last_login ? moment(params.data.last_login).format('MM/DD/YYYY HH:mm') : '-';
            }
          })
      }
      else {
        columnDefs.push({
          headerName: t("seller.company"),
          field: "company_name"
        },
          {
            headerName: t("userInfo.firstName"),
            field: "first_name",
            onCellValueChanged: function (params) {
              updateFirstName(params);
            },
            editable: true
          },
          {
            headerName: t("userInfo.lastName"),
            field: "last_name",
            onCellValueChanged: function (params) {
              updateLastName(params);
            },
            editable: true
          },
          {
            headerName: t("userInfo.email"),
            field: "email"
          },
          {
            headerName: t("userInfo.phone"),
            field: "phone"
          },
          {
            headerName: t("userInfo.role"),
            field: "user_role",
            valueFormatter: params => userRolesArray[parseInt(params.data.user_role) - 1],
          },
          {
            headerName: t("userInfo.notes"),
            field: "notes",
            onCellValueChanged: function (params) {
              updateNotes(params);
            },
            editable: true
          },
          {
            headerName: t("userInfo.lastLogin"),
            field: "last_login",
            valueFormatter: (params) => {
              return params.data.last_login ? moment(params.data.last_login).format('MM/DD/YYYY HH:mm') : '-';
            }
          })
      }
      setColumn(columnDefs)

    }

  }, [user]);


  const viewProfile = ({ data: { id } }) => {
    const { t } = useTranslation();
    return (
      <Link className="text-info" to={`/user/${id}`}>
        {t("userInfo.viewDetails")}
      </Link>
    );
  };

  const sendMagicLinkAndAlertUser = async email => {
    // call the API function to send magic link if we have an email.
    if (email) {
      await sendMagicLink(email)
    }
    // let user know if we have an email on file that matches, they should get one.
    alert(t("login.success"));
  };

  const resetPassword = props => {
    const { t } = useTranslation();
    return (
      <span
        style={{ cursor: "pointer", textDecoration: "underline" }}
        // onClick={async () =>
        //   await sendMagicLinkAndAlertUser(props.api.getSelectedRows()[0].email)
        // }
        className="reset-password w-100 h-auto p-0"
      >
        {t("userInfo.password.reset")}
      </span>
    );
  };

  //For Moving to production, we are commenting this out since this is unused and old code 

/*  const calculateDeliveryPercentage = async performerId => {
    console.info(`calculating delivery for performer ${performerId}`);
    const allUserListings = await getNumberOfUserListings(performerId);
    const confirmedDeliveredUserListings = await getNumberOfDeliveredUserListings(
      performerId
    );
    if (allUserListings === 0) {
      return `No user listings`;
    }
    return `${(
      (confirmedDeliveredUserListings / allUserListings) *
      100
    ).toFixed(2)}%`;
  }; */
  // const viewNotes = props => {
  //   const { t } = useTranslation();
  //   return (
  //     // <a href="/" className="text-warning w-100 h-auto p-0">
  //     //   {t("userInfo.notes")}
  //     // </a>
  //     <span onClick={() => setNotesModalVisible(true)}>{t("userInfo.notes")} </span>
  //   );
  // };

  // const handleNotesOk = e => {
  //   setNotesModalVisible(false);
  // };

  // const handleNotesCancel = e => {
  //   setNotesModalVisible(false);
  // };
  const handleSearch = e => {
    setSearchValue(e.target.value);
  };

  const updateFirstName = async function (params) {
    console.log(
      `We will update the price in the DB to ${params.newValue} if it's formatted right`
    );
    updateUserFirstName(params.data.id, params.newValue);
    return params.newValue;
  };
  const updateLastName = async function (params) {
    console.log(
      `We will update the price in the DB to ${params.newValue} if it's formatted right`
    );
    updateUserLastName(params.data.id, params.newValue);
    return params.newValue;
  };
  const updateNotes = async function (params) {
    console.log(
      `We will update the notes in the DB to ${params.newValue} if it's formatted right`
    );
    updateUserNotes(params.data.id, params.newValue);
    return params.newValue;
  };

  const updateUserRole = async data => {
    const newUser = await getUserById(data.id);
    delete newUser.fullName;
    await updateUser({ ...newUser, userRole: data.userRole });
    alert(t("userInfo.updateUserRole"));
  };


  const frameworkComponents = {
    viewProfile,
    resetPassword,
    phoneFormat
  };
  //For Moving to production, we are commenting this out since this is unused and old code
  /* async function fetchData() {

    let data = '';
    if (user.userRole == userRoles.admin) {
      data = await getAllUsersByRole(userRoles.marketplace_user);
    }
    else data = await getAllUsers();

    // calculate delivery percentage for each
    for (let user in data) {
      data[user].deliveryPercentage = await calculateDeliveryPercentage(
        parseInt(user)
      );
    }
    setRowData(data);
    setLoading(false);
    return;
  } */
  useEffect(() => {
    setLoading(true);
    async function fetchData() {
      let data = '';
      if (user.userRole == userRolesLower.super_admin) {
        data = await getAllUsersByRole(2);
      }
      else if (user.userRole == userRolesLower.admin) {
        data = await getAllUsersByRole(3);
      }
      else if (user.userRole == userRolesLower.broker) {
        data = await getAllUsersByRole(4);
      }
      else {
        data = await getAllUsers();
      }

      //For Moving to production, we are commenting this out since this is unused and old code 

      // calculate delivery percentage for each
    /*  for (let user in data) {
        data[user].deliveryPercentage = await calculateDeliveryPercentage(
          parseInt(user)
        );
      } */
      await setRowData(data);
      setLoading(false);
      return;
    }


    fetchData();
    params.createFunctionToCall(() => fetchData)
  }, [params.createFunctionToCall]);

  return !loading ? (
    <div
      style={{
        height: "calc(100vh - 14rem)",
        width: "100%",
        backgroundColor: "inherit"
      }}
      className="ag-theme-balham user-ag-grid-section"
      id="grid-wrapper"
    >
      <div className="user-search-item">
        <div className="search-field-sec">
          <div className="search-top-input" style={{ position: 'relative' }}>
            <Input
              value={searchValue}
              className="search-input stylefont-weight-medium"
              onChange={handleSearch}
              placeholder={t("search.searchUsers")}
              suffix={<SearchIcon />}
              bordered={false}
            />
            {searchValue &&
              <button className="search-close-icon" onClick={(e) => setSearchValue('')} >X</button>}
          </div>
        </div>
        <div className="add-new-user-item">
          <button onClick={params.click} className="btn add-newuser-btn primary-bg-color stylefont-weight-medium" >Add New User</button>
        </div>
      </div>
      <AgGridReact
        // properties
        columnDefs={column}
        rowData={rowData}
        defaultColDef={{ filter: true, resizable: false }}
        quickFilterText={searchValue}
        frameworkComponents={frameworkComponents}
        // events
        rowSelection={rowSelection}
        // onFirstDataRendered={onGridSizeChanged}
        //onGridSizeChanged={onGridSizeChanged}
        onRowClicked={viewUserDetails}
        rowHeight="60"
        headerHeight="65"
        sortable={true}
      ></AgGridReact>

    </div>
  ) : (
    <Loader />
  );
};

export default AllMarketPlaceUsersGrid;
