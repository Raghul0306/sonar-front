import React, { useEffect, useState, useContext } from "react";
import UserContext from "../../contexts/user.jsx";
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import { withRouter } from "react-router-dom";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import { useTranslation } from "react-i18next";
import { getAllCompanies, saveCompanyNameDetails, getAllSuperAdminDetails, saveAssignedSuperAdmin, findCompanyEmailExists } from "../../services/companyService";
import { getCountries, getStates, getCities, getCountry, getCity, getState } from "../../services/userService";
import Loader from "../Loader/Loader";
import editIcon from "../../assets/edit-icon.png"
import childCompanyIcon from "../../assets/market-place-icon/market-marketplace.png"
import { useForm } from "react-hook-form";
import Select from 'react-select'
import { yupResolver } from "@hookform/resolvers";
import * as yup from "yup";
import { Modal, Button } from 'react-bootstrap';

const CompanyGrid = (data) => {

  const { user } = useContext(UserContext);

  const { t } = useTranslation();
  const [rowData, setRowData] = useState([]);
  const [columnData, setColumn] = useState([]);
  const [rowSelection] = useState("single");
  const [companyName, setCompanyName] = useState("");
  const [companyPhoneNumber, setCompanyPhoneNumber] = useState("");
  const [companyEmail, setCompanyEmail] = useState('');
  const [companyAdress, setCompanyAdress] = useState("");
  const [companyAddress2, setCompanyAddress2] = useState("");
  const [companyCity, setCompanyCity] = useState("");
  const [companyCountry, setCompanyCountry] = useState("");
  const [companyState, setCompanyState] = useState("");
  const [companyZipcode, setCompanyZipcode] = useState("");
  const [parentCompanyId, setParentCompanyId] = useState(0);
  const [parentCompanyName, setParentCompanyName] = useState("");
  const [emailDuplicate, setEmailDuplicate] = useState(false);

  const [countryList, setCountryList] = useState('');
  const [stateList, setStateList] = useState('');
  const [cityList, setCityList] = useState('');
  const [isSubmitted, setIsSubmitted] = useState(false);

  const [loading, setLoading] = useState(true);
  const [gridApi, setGridApi] = useState(null);
  const [gridColumnApi, setGridColumnApi] = useState(null);
  const [companyModal, setCompanyModal] = useState(false);
  const [companyData, setCompanyData] = useState([]);
  const [assignSuperAdminModal, setAssignSuperAdminModal] = useState(false);
  const [superAdminData, setSuperAdminData] = useState([]);
  // const [superAdminSelected, setSuperAdminSelected] = useState([]);
  const [assignedSuperAdmin, setAssignedSuperAdmin] = useState({ label: "Select Super Admin", value: "" });
  const [responseMessage, setResponseMessage] = useState('');
  const [isChildCompanyCreation, setIsChildCompanyCreation] = useState(false);
  const [adminResponseModal, setAdminResponseModal] = useState(false)
  const [showErrorMessage, setShowErrorMessage] = useState(false)

  const SignupSchema = yup.object().shape({
    companyName: yup
      .string().required("Company Name is required")
      .min(2, 'Company Name should be a minimum of 3 characters'),
    address: yup
      .string().required("Address is required"),
    zipcode: yup.string()
      .required("Zipcode is required")
      .matches(/^[a-zA-Z0-9]+$/, "Zip Code must be only alphanumerics")
      .min(3, 'Zipcode should be a minimum of 3 digits')
      .max(8, 'Zipcode should be a maximum of 8 digits'),
    phoneNumber: yup.string()
      .required("Phone number is required")
      .matches(/^[0-9]+$/, "Phone number must be only digits")
      .min(10, 'Phone number should be a minimum of 10 digits')
      .max(15, 'Phone number should be a maximum of 15 digits'), 
    email: yup
      .string()
      .required("Email is required")
      .email("Invalid Email Format"),
  });

  const onGridReady = (params) => {
    setGridApi(params.api);
    setGridColumnApi(params.columnApi);

    params.api.sizeColumnsToFit();
  };

  function handleEmailChange(event) {
    setEmailDuplicate(false)
    setCompanyEmail(event.target.value);
  }

  function dropdownFilterOption({ label }, string) {
    if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };

  useEffect(() => {
    getCompanyDetails();
    getSuperAdminDetails();
    onLoadCountryDropDown();
    // onLoadStateDropDown();
    // onLoadCityDropDown();
  }, []);

  async function getCompanyDetails() {
    var companyDetails = await getAllCompanies();
    if (companyDetails) {
      setRowData(companyDetails);
    }
    setLoading(true);
  }


  async function onLoadCountryDropDown() {
    let countries = await getCountries();
    if (countries) {
      const countriesList = [];
      countries.length && countries.map(items => {
        countriesList.push({ label: items.name, value: items.id },)
      }
      )
      await setCountryList(countriesList);
    }
  }

  async function onLoadStateDropDown(countryId) {

    let states = await getStates(countryId);
    if (states) {
      const stateList = [];
      states.length && states.map(items => {
        stateList.push({ label: items.name, value: items.id })
      });
      await setStateList(stateList);
    }

  }

  async function onLoadCityDropDown(countryId, stateId) {

    let cities = await getCities(countryId, stateId);
    if (cities) {
      const cityList = [];
      cities && cities.length && cities.map(items => {
        cityList.push({ label: items.name, value: items.id })
      });
      await setCityList(cityList);
    }
  }

  async function getSuperAdminDetails() {
    var superAdminDetails = await getAllSuperAdminDetails();
    if (superAdminDetails) {

      let superAdminOptions = []
      for (const optionsAdmin of superAdminDetails) {
        superAdminOptions.push({ label: optionsAdmin.first_name + ' ' + optionsAdmin.last_name, value: optionsAdmin.id });
        if (assignedSuperAdmin.value == optionsAdmin.id) {
          setAssignedSuperAdmin([{ label: optionsAdmin.first_name + ' ' + optionsAdmin.last_name, value: optionsAdmin.id }]);
        }
      }
      setSuperAdminData(superAdminOptions);
    }
    setLoading(true);
  }

  const tableActions = props => {
    return (
      <div>
        <button type="button" title="Edit" className="report-edit-btn btn" onClick={() => handleEditCompanyModal(props) } > <img style={{
        width: "30px",height:'auto'
      }} src={editIcon}/> </button>
      {/* TODO -  Dont Remove this code - Temporarily Commented this Assign Super Admin Button */}
      {/* <button style={{ display: ((props.data.usercompany && props.data.usercompany > 0) || props.data.parent_company_id > 0) ? "none" : 'inline-flex' }} type="button" title="Assign Super Admin" className="report-edit-btn btn" onClick={() => handleAssignSuperAdmin(props) }> <img src={assignIcon} style={{width: "60px",height:'auto'}}/> </button> */}
      <button style={{ display: props.data.usercompany && props.data.usercompany > 0 ? "inline-flex" : 'none' }} type="button" title="Create Child Company" className="report-edit-btn btn" onClick={() => handleCreateChildCompany(props) }> <img src={childCompanyIcon} style={{width: "60px"}}/> </button>
      </div>
   );
  };

  const handleEditCompanyModal = async props => {
    if (props && props.data) {
      setCompanyData(props.data)
      setCompanyName(props.data.company_name)
      setCompanyEmail(props.data.email)
      setCompanyPhoneNumber(props.data.phone)
      setCompanyAdress(props.data.address1)
      setCompanyAddress2(props.data.address2)
      setCompanyZipcode(props.data.zip_code)
      if (props.data.country) {
        var selectedCountry = await getCountry(props.data.country);
        setCompanyCountry([{ label: selectedCountry.name, value: selectedCountry.id }])
      }
      if (props.data.state) {
        var selectedState = await getState(props.data.state);
        setCompanyState([{ label: selectedState.name, value: selectedState.id }])
      }
      if (props.data.city) {
        var selectedCity = await getCity(props.data.city);
        setCompanyCity([{ label: selectedCity.name, value: selectedCity.id }])
      }
      setCompanyModal(true)
    }
  }

  const handleAssignSuperAdmin = props => { 
    setShowErrorMessage(false);
    if (props && props.data) {
      setCompanyData(props.data);
      setAssignedSuperAdmin({ label: "Select Super Admin", value: "" })
      setAssignSuperAdminModal(true)
    }
  }
  
  const handleCreateChildCompany = props => {
    if(props && props.data)
    {
      setParentCompanyId(props.data.id);
      setParentCompanyName(props.data.company_name);
      setIsChildCompanyCreation(true);
      setCompanyModal(true)
    }
  }

  const handleCompanyDetailChange = (e) => {

    const name = e.target.name;

    if (name === "companyName") {
      setCompanyName(e.target.value);
    } else if (name === "address") {
      setCompanyAdress(e.target.value);
    } else if (name === "address2") {
      setCompanyAddress2(e.target.value);
    } else if (name === "zipcode") {
      setCompanyZipcode(e.target.value);
    } else if (name === "phoneNumber") {
      setCompanyPhoneNumber(e.target.value);
    }
  }


  const handleCountryChange = async (e) => {
    await setCompanyState("");
    await setCompanyCity("");
    await setStateList([]);
    await setCityList([]);
    if (e) {
      await setCompanyCountry(e);
      await onLoadStateDropDown(e.value)
    } else {
      await setCompanyCountry("");
    }
  }

  const handleStateChange = async (e) => {
    await setCompanyCity("");
    await setCityList([]);
    if (e && companyCountry) {
      await setCompanyState(e);
      await onLoadCityDropDown(companyCountry.value, e.value)
    } else {
      await setCompanyState("");
    }
  }

  const handleCityChange = async (e) => {
    await setCompanyCity(e);
  }



  const { register, handleSubmit, errors, touched } = useForm({
    resolver: yupResolver(SignupSchema)
  });

  const onSubmit = async data => {

    if (companyCountry && companyState && companyCity) {
      var companyId = 0;
      if(companyData) { companyId = companyData.id; }

      let emailExists = await findCompanyEmailExists(companyId, data.email);
      if (emailExists.length) {
        setEmailDuplicate(true);
        setCompanyEmail('');
      }
      else {
        
        if(companyCountry) { data.country = companyCountry.value; }
        if(companyState) { data.state = companyState.value; }
        if(companyCity) { data.city = companyCity.value; }
        if(isChildCompanyCreation) { data.parentCompanyId = parentCompanyId }
        var userId = user.id;
        data.companyId = companyId;
        
        let response = await saveCompanyNameDetails(data, userId);
        if (!response.isError) {
          setCompanyModal(false);
          setCompanyName('');
          setCompanyPhoneNumber('');
          setCompanyEmail('');
          setCompanyAdress('');
          setCompanyAddress2('');
          setCompanyCity('');
          setCompanyCountry('');
          setCompanyState('');
          setCompanyZipcode('');
          setIsSubmitted(false);
          getCompanyDetails(user.id);
          setIsChildCompanyCreation(false);
        }
      }
    }
  }

  const createNewCompany = (e) => {
    setIsChildCompanyCreation(false);
    setCompanyModal(true);
  }

  const changeAssignSuperAdmin = (e) => {
    // const value = e.target.value;
    setAssignedSuperAdmin(e);
    if (e.value) {
      setShowErrorMessage(false);
    }
  }

  const companyFormValidation = (e) => {
    setIsSubmitted(true);
  }

  const submitAssignedSuperAdmin = async () => {
    setIsSubmitted(true);
    var companyId = companyData.id;
    var userId = user.id;
    if (assignedSuperAdmin.value) {
      let inputData = { companyId: companyId, superAdminId: assignedSuperAdmin.value };
      let response = await saveAssignedSuperAdmin(inputData, userId);
      if (!response.isError || response.isError) {
        setResponseMessage(response.message);
        setAdminResponseModal(true);
        setAssignSuperAdminModal(false);
        setAssignedSuperAdmin({});
        getCompanyDetails(user.id);
      }
    }
    else {
      setShowErrorMessage(true);
    }

  }

  const toggleNewCompany = async (state) => {
    setCompanyData([])
    setCompanyName('');  //reset all values on modal show
    setCompanyAdress('');
    setCompanyAddress2('');
    setCompanyCity('');
    setCompanyCountry('');
    setCompanyState('');
    setCompanyZipcode('');
    setIsSubmitted(false);    
    setIsChildCompanyCreation(false);
    setCompanyModal(state);
  }

  const frameworkComponents = {
    tableActions
  };

  return loading ? (
<div>

<div className="add-new-user-item" style={{ marginBottom: "10px",textAlign:'right'}}>
<button onClick={() => toggleNewCompany(true) } className="btn add-newuser-btn primary-bg-color stylefont-weight-medium"> Add New Company</button> 
{/* createNewCompany() */}
</div>

    <div
      style={{
        height: "calc(100vh - 18rem)",
        width: "100%",
        backgroundColor: "inherit"
      }}
      id="grid-wrapper"
      className="ag-theme-balham common-ag-grid-table company-create-grid"
    >
        <AgGridReact
          rowData={rowData}
          defaultColDef={{ width: 100 }}
          onGridReady={onGridReady}
          rowHeight="60"
          headerHeight="65"
          pagination={true}
          paginationPageSize={25}
          frameworkComponents={frameworkComponents}
        >
          <AgGridColumn headerName={t("company.title")}  field="company_name" minWidth ={150}/>
          <AgGridColumn headerName={t("company.parentCompany")}  field="parentcompanyname" minWidth ={150}/>
          <AgGridColumn headerName={t("company.email")}  field="email" minWidth ={150}/>
          <AgGridColumn headerName={t("company.actions")} cellRenderer= "tableActions" minWidth ={150}/>
          
        </AgGridReact>

        <Modal show={companyModal} onHide={() => setCompanyModal(false) } backdrop="static" className="create-company-modal">
          <Modal.Header closeButton>
            <Modal.Title>Company Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>

            <form onSubmit={handleSubmit(onSubmit)} autoComplete="nofill" >

          <div className="row" style={{ display: isChildCompanyCreation ? "block" : 'none' }} >
              <div className="form-group col-md-12">
                <label>Parent Company Name <span style={{ color: "red" }}>*</span></label>
                <input type="text" className="form-control" ref={register} placeholder="Company Name" name="companyName" id="parentCompanyName" value={parentCompanyName} disabled />
              </div>
            </div>

            <div className="row">
                <div className="form-group col-md-6">
                  <label>Company Name <span style={{ color: "red" }}>*</span></label>
                  <input type="text" className="form-control" onChange={(e) => handleCompanyDetailChange(e)} 
                  ref={register} placeholder="Company Name" name="companyName" id="companyName" value={companyName} autoComplete="nofill"  />

                    <div className="invalid-feedback" style={{ display: errors.companyName && errors.companyName.message ? "block" : 'none' }} >
                      <p>{errors.companyName && errors.companyName.message ? errors.companyName.message : ''}</p>
                    </div>
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="phoneNumber">Phone Number <span style={{ color: "red" }}>*</span></label>
                  <input type="text" className="form-control" id="phoneNumber" placeholder="XXX-XXX-XXXX" name="phoneNumber" ref={register} value={companyPhoneNumber} onChange={(e) => handleCompanyDetailChange(e)} autoComplete="nofill" />
                  <div className="invalid-feedback" style={{ display: errors.phoneNumber && errors.phoneNumber.message ? "block" : 'none' }} >
                    <p>{errors.phoneNumber && errors.phoneNumber.message ? errors.phoneNumber.message : ''}</p>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="form-group col-md-12">
                  <label htmlFor="email">Email <span style={{ color: "red" }}>*</span></label>
                  <input type="email" className="form-control" id="email" placeholder="Email" name="email" ref={register} value={companyEmail} onChange={handleEmailChange} autoComplete="nofill" />
                  <div className="invalid-feedback" style={{ display: errors.email && errors.email.message ? "block" : 'none' }} >
                    <p>{errors.email && errors.email.message ? errors.email.message : ''}</p>
                  </div>
                  <span className="invalid-feedback" style={{ display: emailDuplicate && !errors.email ? "block" : 'none' }} >
                    <p> Email is already Exists</p>
                  </span>
                </div>
              </div>

              <div className="row justify-content-between">
                <div className="form-group col-md-6">
                  <label>Address <span style={{ color: "red" }}>*</span></label>
                  <input type="text" className="form-control" onChange={(e) => handleCompanyDetailChange(e)} ref={register} placeholder="Address" name="address" value={companyAdress} autoComplete="nofill" />

                  <div className="invalid-feedback" style={{ display: errors.address && errors.address.message ? "block" : 'none' }} >
                    <p>{errors.address && errors.address.message ? errors.address.message : ''}</p>
                  </div>

                </div>
                <div className="form-group col-md-6">
                  <label>Address 2</label>
                  <input type="text" className="form-control" onChange={(e) => handleCompanyDetailChange(e)} ref={register} placeholder="Address 2" name="address2" value={companyAddress2}  autoComplete="nofill" />
                </div>
              </div>

          <div className="row justify-content-between">
            <div className="form-group company-select-item col-md-6">
              <label>Country <span style={{ color: "red" }}>*</span></label>
              <Select
              placeholder={"Select Country"}
              style={{ width: "100%" }}
              name="country" autoComplete="nofill"
              filterOption={dropdownFilterOption}
              value={companyCountry}
              options={countryList}
              onChange={(e) => handleCountryChange(e)}
              className={(errors.country && touched.country ? ' is-invalid' : '')}
              allowClear
              showSearch
              isClearable
              />
            <div className="invalid-feedback" style={{ display: !companyCountry && isSubmitted ? "block" : 'none' }} ><p>Country is required</p></div>
            </div>

            <div className="form-group company-select-item col-md-6">
              <label>City <span style={{ color: "red" }}>*</span></label>
              <Select
              placeholder={"Select City"}
              style={{ width: "100%" }}
              name="city" autoComplete="nofill"
              filterOption={dropdownFilterOption}
              value={companyCity}
              options={cityList}
              onChange={(e) => handleCityChange(e)}
              className={(errors.country && touched.country ? ' is-invalid' : '')}
              allowClear
              showSearch
              isClearable
              />
              <div className="invalid-feedback" style={{ display: !companyCity && isSubmitted ? "block" : 'none' }} ><p>City is required</p></div>
              
            </div>

            <div className="form-group company-select-item select-overlay col-md-6">
              <label>State <span style={{ color: "red" }}>*</span></label>
              <Select
              placeholder={"Select State"}
              style={{ width: "100%" }}
              name="state" autoComplete="nofill"
              filterOption={dropdownFilterOption}
              value={companyState}
              options={stateList}
              onChange={(e) => handleStateChange(e)}
              className={(errors.country && touched.country ? ' is-invalid' : '')}
              allowClear
              showSearch
              isClearable
              />
              <div className="invalid-feedback" style={{ display: !companyState && isSubmitted ? "block" : 'none' }} ><p>State is required</p></div>
            </div>

              <div className="form-group col-md-6">
                <label>Zipcode <span style={{ color: "red" }}>*</span></label>
                <input type="text" className="form-control" onChange={(e) => handleCompanyDetailChange(e)} ref={register} placeholder="Zipcode" name="zipcode" value={companyZipcode} autoComplete="nofill" />
              
                <div className="invalid-feedback" style={{ display: errors.zipcode && errors.zipcode.message ? "block" : 'none' }} >
                <p>{errors.zipcode && errors.zipcode.message ? errors.zipcode.message : ''}</p>
                </div>
              
              </div>

          </div>
          <div className="company-detail-submit-btn">
            <Button variant="secondary" className="btn addnew-btn" onClick={() => setCompanyModal(false) }>
              Cancel
            </Button>
            <Button variant="primary" type="submit" onClick={() => companyFormValidation()} className="btn btn-success info-btn-listing">
              Submit
            </Button>
          </div>

            </form>


          </Modal.Body>

        </Modal>

        <Modal show={assignSuperAdminModal} onHide={() => setAssignSuperAdminModal(false) } backdrop="static" className="assign-admin-modal">
          <Modal.Header closeButton>
            <Modal.Title>Assign Super Admin</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="form-group">
              <label>Assign Super Admin</label>
              <Select
                name="superAdmin"
                // className='formik-input-user-form form-control'
                placeholder={"Select Super Admin"}
                value={[assignedSuperAdmin]}
                options={superAdminData}
                onBlur={true}
                disableSearch={true}
                onChange={(e) => changeAssignSuperAdmin(e)}
              >
              </Select>
              {showErrorMessage &&
                <span style={{ color: "red", textAlign: "center" }}>Super Admin is required</span>}
            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={() => setAssignSuperAdminModal(false)}>
              Cancel
            </Button>
            <Button variant="primary" className="btn btn-success info-btn-listing" onClick={(e) => submitAssignedSuperAdmin()}  >
              Submit
            </Button>
          </Modal.Footer>
        </Modal>


        <Modal show={adminResponseModal} onHide={() => setAdminResponseModal(false)} className="assign-admin-modal" backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Assign Super Admin</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h4>{responseMessage}</h4>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button variant="primary" className="btn btn-success info-btn-listing" onClick={(e) => setAdminResponseModal(false)}>
              Ok
            </Button>
          </Modal.Footer>
        </Modal>

      </div>
    </div>
  ) : (
    <Loader />
  );
};

export default withRouter(CompanyGrid);
