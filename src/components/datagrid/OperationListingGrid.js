import React, { Component } from "react";
import UserContext from "../../contexts/user.jsx";
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import { withTranslation } from "react-i18next";
import { getOperationFilterListings, removeQCHoldStatus, updateCostForListings, bulkUpdateNotesAndTags, updateDisclosureInBulk, updateSplitsInBulk, updateOperationListingFromGridCell } from "../../services/operationService";
import { getAllTagList, updateInternalNotesListings, fetchListById, getAllTicketTypes, getAllSeatTypesList, updateinhandDateInListings, updateSeatType, updateStockType, getAllDisclosureList, getAllSplits, allTicketingSystemAccounts } from "../../services/listingsService";
import * as moment from "moment-timezone";
import { Modal, Button } from 'react-bootstrap';
import OperationSettingButton from "../operation/operationSettingButton";
import OperationSeatTypeDropdown from "../operation/operationSeatTypeDropdown";
import OperationStockTypeDropdown from "../operation/operationStockTypeDropdown";
import OperationSplitDropdown from "../operation/operationSplitDropdown";
import OperationOwnerDropdown  from "../operation/OperationOwnerDropdown";
import  TagsDropdown  from "../operation/tagsDropdown";
import  DisclosureDropdown  from "../operation/disclosureDropdown";
import OperationAccountDropdown from "../operation/operationAccountDropdown";
import { TagAndNotesModalForm } from "../operation/tagAndNotesModalForm";
import { InHandModalForm } from "../operation/inHandModalForm";
import { SeatTypeModalForm } from "../operation/seatTypeModalForm";
import { StockTypeModalForm } from "../operation/stockTypeModalForm"
import { CostModalForm } from "../operation/costModalForm"
import { DisclosureModalForm } from "../operation/disclosureModalForm"
import { SplitsModalForm } from "../operation/splitsModalForm"
import { listingConst, levelNames, operationConstantData, operationGridConstant, seatTypes, dateValidationString } from "../../constants/constants";
import { OperationEditSeatInfo } from "../operation/operationEditSeatInfo";
import OperationEditGridDate from "../operation/OperationEditInHandDate";
import { getAllCompanies } from "../../services/companyService";
import  OperationEditTextField  from "../operation/OperationEditTextField";
import Icons from 'antd/es/icon';
const cellClassRules = {
    'header-cell stylefont-weight-bold': 'data.section === "big-title"'
  };

export class OperationListingGrid extends Component {
    static contextType = UserContext;

    constructor(props) {
        super(props);

        this.isHeaderRow = this.isHeaderRow.bind(this);
        this.getOperationListings = this.getOperationListings.bind(this);
        this.getSelectedtags = this.getSelectedtags.bind(this);
        this.customMenu = this.customMenu.bind(this);
        this.getContextSingleMenuItems = this.getContextSingleMenuItems.bind(this);
        this.onSelectionChanged = this.onSelectionChanged.bind(this);
        this.internalNotesModal = this.internalNotesModal.bind(this);
        this.updateInHandDate = this.updateInHandDate.bind(this);
        this.updateSeatTypeModal = this.updateSeatTypeModal.bind(this);
        this.updateStockTypeModal = this.updateStockTypeModal.bind(this);
        this.setStockTypesChange = this.setStockTypesChange.bind(this);
        this.showCostConfirmationModal = this.showCostConfirmationModal.bind(this);
        this.handleCostOnChange = this.handleCostOnChange.bind(this);
        this.updateListingCost = this.updateListingCost.bind(this);
        this.disclosuresModal = this.disclosuresModal.bind(this);
        this.changeDisclosures = this.changeDisclosures.bind(this);
        this.disclosuresNotesModal = this.disclosuresNotesModal.bind(this);
        this.changeSeatNumber = this.changeSeatNumber.bind(this);
        this.autoColumnResizeOnload = this.autoColumnResizeOnload.bind(this);
        this.autoColumnResize = this.autoColumnResize.bind(this);
        this.onGridReady = this.onGridReady.bind(this);

        this.state = {
          gridApi: null, 
          gridColumnApi: null, 
            rowData: [],
            allSeatTypes: [],
            listingData: [],
            loading:true,
            defaultColDef: {
                sortable: true,
                filter: true,
                resizable: true,
                width: 100
            },
            frameworkComponents: {
                changeQCHoldStatus:this.changeQCHoldStatus,
                stockTypeDropdown:OperationStockTypeDropdown,
                seatTypeDropdown:OperationSeatTypeDropdown,
                splitDropdown:OperationSplitDropdown,
                changeSeatNumber:this.changeSeatNumber,
                ownerDropdown:OperationOwnerDropdown,
                accountDropdown:OperationAccountDropdown,
                disclosureDropdown:DisclosureDropdown,
                displayValueAsButton:this.displayValueAsButton,
                changeDate:OperationEditGridDate,
                tagsDropdown: TagsDropdown,
                operationEditTextField: OperationEditTextField,
            },
            qcModal:false,
            notesModal:false,
            inHandModal:false,
            inHandConfirmationModal:false,
            seatTypeConfirmationModal:false,
            seatTypeModal:false,
            showCostModal:false,
            stockTypeModal:false,
            stockTypeConfirmationModal:false,
            costConfirmationModal:false,
            disclosureModal:false,
            disclosureConfirmationModal:false,
            singleListingTag:false,
            confirmSplitModal:false,
            allTags:[],
            selectedTags:[],
            selectedInhandDate:null,
            selectedTicketTypes:[],
            selectedListingId:[],
            selectedStockTypes:[],
            allStockTypes:[],
            selectedDisclosures:[],
            disclosuresList:[],
            disclosureValues:[],
            disclosuresLabel:[],
            selectedSplits:[],
            allSplitsList:[],
            internalNotes:"",
            externalNotes:"",
            newCost:"",
            maxEventDate:"",
            seatStart:"",
            seatEnd:"",
            validation:{
                disclosure: false, 
                cost:false, 
                split:false
            },
            selectedRows:[],
            editRowData:{
                listingGroupId:"",
                section:"",
                row:"",
                seatStart:"",
                seatEnd:"",
                cost:"",
                externalNotes:"",
                disclosures:[],
                tags:[],
                inHandDate:""
            },
            seatNumberInfo:{
              seatStart:"",
              seatEnd:""
            },
            searchData: { tagIds:[] },
            last4DigitsState:'',
            costUpdateValue:'',
            isListingSearch:false,
            currentPage:1,
            pageLimit:15,            
            totalEventCount:0,
        }
    }

  static getDerivedStateFromProps(nextProps, state) { 
    if (nextProps.operationListingSearch && nextProps.operationListingdata && nextProps.operationListingdata.data) {
      return {
        ...state,
        searchData: nextProps.filterParams,
        rowData: nextProps.operationListingdata.data,
        isListingSearch: nextProps.operationListingSearch
      };
    }
    else if (!nextProps.operationListingSearch && nextProps.filterListingData && nextProps.filterListingData.data) {
      return {
        ...state,
        searchData: nextProps.filterParams,
        rowData: nextProps.filterListingData.data,
        isListingSearch: nextProps.operationListingSearch
      };
    }
    else {
      return {
        ...state,
        searchData: nextProps.filterParams,
        isListingSearch: nextProps.operationListingSearch
      };
    }
  }

    async componentDidMount() {
        const userId = this.context.user.id;
        await this.getOperationListings(userId, this.state.searchData);
        
        //get all ticket types 
        var getStockTypes = await getAllTicketTypes();
        const stockTypesValues = [];
        getStockTypes && getStockTypes.length > 0 && getStockTypes.map((item) => {
            var combined = { label: item.name, value: item.id }
            stockTypesValues.push(combined);
        })

        //get all seat types 
        var getSeatTypes = await getAllSeatTypesList();
        const seatTypeValues = [];
        getSeatTypes && getSeatTypes.length > 0 && getSeatTypes.map((item) => {
            var combined = { label: item.name, value: item.id }
            seatTypeValues.push(combined);
        })
        
        var getTags = await getAllTagList(userId);
        var getDisclosures = await getAllDisclosureList(userId);
        var getSplits = await getAllSplits();
        var allCompaniesList = await getAllCompanies(userId);
        
        //get all tags
        const tagValues = [];
        getTags && getTags.length > 0 && getTags.map((item) => {
            var tagOption = { label: item.tag_name, value: parseInt(item.id) }
            tagValues.push(tagOption);
        })
        
        //get all disclosures
        var allDisclosures = [];
        getDisclosures && getDisclosures.length > 0 && getDisclosures.map((item) => {
            var obj = { label: item.disclosure_name, value: parseInt(item.id) };
            allDisclosures.push(obj);
        })

        //get all splits
        const splitValues = [];
        getSplits && getSplits.length > 0 && getSplits.map((item) => {
            var combined = { label: item.name, value: item.id }
            splitValues.push(combined);
        })

        const companiesList = [];
        allCompaniesList && allCompaniesList.length > 0 && allCompaniesList.map((company) => {
            var combined = { label: company.company_name, value: company.id }
            companiesList.push(combined);
        })

        var ticketingSystemAccounts = await allTicketingSystemAccounts(0,userId,true);
        const accountsList = [];
        ticketingSystemAccounts && ticketingSystemAccounts.length > 0 && ticketingSystemAccounts.map((item, i) => {
            var combined = { label: item.email, value: item.id }
            accountsList.push(combined);
          })

        this.setState({
            allTags:tagValues,
            allSplitsList:splitValues,
            disclosuresList:allDisclosures,
            allSeatTypes:seatTypeValues,
            allStockTypes:stockTypesValues,
            allCompaniesList:companiesList,
            ticketingSystemAccountDetails:accountsList
        })

        /* Note : Please dont remove this code.
        //Ag-grid Scroll Event
        document.querySelector(".ag-body-viewport").addEventListener('scroll', this.autoColumnResizeOnload); */
    }

    onPageChange = async (currentPage) => {
      if(currentPage <= Math.ceil(this.state.totalEventCount/this.state.pageLimit)) {
        await this.setState({currentPage});
        this.getOperationListings(this.context.user.id, this.state.searchData);
      }
    }

    async getOperationListings(userId, params = {}) {
        params.currentPage = this.state.currentPage;
        params.pageLimit = this.state.pageLimit;
        var getOperationFilterListingList = await getOperationFilterListings(userId, params);
        if(getOperationFilterListingList && getOperationFilterListingList.data){
            this.setState({rowData:getOperationFilterListingList.data,totalEventCount:getOperationFilterListingList.totalEventCount});
        }
    }

    isHeaderRow(props) {
        return props.data.section === 'big-title';
    }
    isGeneralAdmissionRow(params) {      
        return params.data.seatTypeValue && params.data.seatTypeValue.value==seatTypes.generalAdmission.id;
    }

    changeQCHoldStatus = props => { 
        if (!this.isHeaderRow(props)) {
          return (
          <OperationSettingButton
            props={props}
            qcModalStatus={this.state.qcModal}
            notesModal={this.state.notesModal}
            click={()=>this.handleClick(props)}
            handleNotesOnClick={()=>this.displayNotesModal(props)}
            selectedListingData={this.state.listingData}
          />
          );
        }
      };

    handleClick = async props => {
        await this.setState({gridApi: props.api, gridColumnApi: props.columnApi});
        if(props && props.data) {
          await this.setState({listingData:props.data, qcModal:true}) 
        }
        this.state.gridApi.redrawRows();
    };
    
    displayNotesModal = async(props) => {   
      await this.setState({gridApi: props.api, gridColumnApi: props.columnApi});
        if(props && props.data) {
          this.getSelectedtags(parseInt(props.data.listingGroupId));
          this.setState({ listingData:props.data, singleListingTag:true, notesModal:true }) 
        }
        this.state.gridApi.redrawRows();
    };

    async getSelectedtags(listingGroupId){
        var userselectedTags = [];
        let listData = await fetchListById(parseInt(listingGroupId));
        var getAllTags = await getAllTagList(this.context.user.id);
          if(listData){
            this.setState({selectedTags:[]});
            listData.tagids && getAllTags.map((item, i) => {
              if (listData.tagids.includes(parseInt(item.id))) {
                var combined = { label: item.tag_name, value: item.id }
                userselectedTags.push(combined);
              }
            })
            this.setState({internalNotes:listData.internal_notes})
          }
          if(userselectedTags.length){
            this.setState({selectedTags:JSON.stringify(userselectedTags)})
          }
    }

    setTicketTypesChange(option) {
        if(option) this.setState({selectedTicketTypes:[option]}) 
        else this.setState({selectedTicketTypes:[]})
    }

    getListingGroupIdsArray(){
        var listingGroupIdsArray = [];      
        this.state.selectedRows && this.state.selectedRows.map(row => {
            if(row.listingGroupId){ listingGroupIdsArray.push(row.listingGroupId) }
        })
        return listingGroupIdsArray;
    }  

    async updateSeatTypeModal(bool) {

        if (bool && this.state.selectedListingId && this.state.selectedTicketTypes) {
          await updateSeatType(this.state.selectedTicketTypes[0].value, this.state.selectedListingId);      
        }
        this.setState({ seatTypeConfirmationModal:false });
        if(this.state.isListingSearch) { this.props.renderPage(); }
        else { this.getOperationListings(this.context.user.id, this.state.searchData); }
    }
    
    async showSeatTypeConfirmationModal(bool){
        this.setState({ seatTypeModal:false })
        if (bool) {
            this.setState({ seatTypeConfirmationModal:true }) 
        } 
    }
    
    showStockTypeConfirmationModal(bool){
        this.setState({ stockTypeModal:false })
        if (bool) {
            this.setState({ stockTypeConfirmationModal:true }) 
        } 
    }
    
    async setStockTypesChange(option) {
        if(option) this.setState({selectedStockTypes:[option]});
        else this.setState({selectedStockTypes:[]})
    }

    async updateStockTypeModal(bool) {

        if (bool && this.state.selectedListingId && this.state.selectedStockTypes) {
          await updateStockType(this.state.selectedStockTypes[0].value, this.state.selectedListingId);      
        }
        this.setState({ stockTypeConfirmationModal:false });
        if(this.state.isListingSearch) { this.props.renderPage(); }
        else { this.getOperationListings(this.context.user.id, this.state.searchData); }
      }
    
      async showInHandConfirmationModal(bool) {
        this.setState({ inHandModal:false }) 
        if (bool) {
            this.setState({ inHandConfirmationModal:true })  
        }    
      }
      
      async updateInHandDate(bool) {    
        if (bool) {
          const datetime = new Date(this.state.selectedInhandDate);
          var year = datetime.getFullYear();
          var month = (datetime.getMonth() + 1) < 10 ? '0' + (datetime.getMonth() + 1) : (datetime.getMonth() + 1);
          var date = (datetime.getDate()) < 10 ? '0' + datetime.getDate() : datetime.getDate();
          let inHand = year + '-' + month + '-' + date;
    
          var listingGroupIdArray = this.getListingGroupIdsArray();
          
          let resData = await updateinhandDateInListings(inHand, this.state.selectedRows[0].eventId, levelNames.listing, listingGroupIdArray);
    
          if (resData && !resData.isError) {
            this.setState({ selectedInhandDate:null, selectedRows:[] })
          }
        }  
        this.setState({inHandConfirmationModal:false});
        if(this.state.isListingSearch) { this.props.renderPage(); }
        else { this.getOperationListings(this.context.user.id, this.state.searchData); }
      }

    async internalNotesModal(bool) {
        if (bool) {
          if(this.state.singleListingTag && this.state.listingData && this.state.listingData.listingGroupId>0){
            await updateInternalNotesListings(this.state.listingData.listingGroupId, this.state.internalNotes, levelNames.listing, this.state.listingData.eventId, this.state.selectedTags);    
          }
          if(!this.state.singleListingTag && this.state.selectedRows && this.state.selectedRows.length){
            var listingGroupIdsArray = this.getListingGroupIdsArray();  
            await bulkUpdateNotesAndTags(listingGroupIdsArray, this.state.internalNotes, this.state.selectedTags); 
          }    
          this.setState({selectedTags:[],internalNotes:""})
        }
        this.setState({ notesModal:false });
        if(this.state.isListingSearch) { this.props.renderPage(); }
        else { this.getOperationListings(this.context.user.id, this.state.searchData); }
        this.state.gridApi.redrawRows();
      }

    handleCostOnChange(e) {
        var costValidation = {cost: true}
        this.state.validation.cost=true;
        if (e.target.value && e.target.value > 0) {
          costValidation = {cost: false}
        }
        if (e.target.value.length <= listingConst.maxLength_amount) {
          this.setState({newCost:e.target.value, validation:costValidation})
        }
    }
    
    displaySplitsModal(bool) {
        
        var splitsValidation = {split: true}
        if (bool && (this.state.selectedSplits == '' || this.state.selectedSplits == null)) {
         
          if (this.state.validation.split == false) {
            splitsValidation = {split: true}
            this.setState({ validation:splitsValidation })
          }
        }
        else if (bool) {
          splitsValidation = {split: false}          
          this.setState({confirmSplitModal:true, validation:splitsValidation}) 
        }
        else {
          splitsValidation = {split: false}  
        }
        this.setState({ splitsModal:false, validation:splitsValidation })
    }
    
    async updateSplits(bool){
        if (bool) {
            var splitsValidation = {split: false} 
            var listingGroupIdArray = this.getListingGroupIdsArray();
            var selectedSplitValue = this.state.selectedSplits && this.state.selectedSplits.value ? this.state.selectedSplits.value : 0;
            await updateSplitsInBulk(listingGroupIdArray, selectedSplitValue);
            this.setState({ selectedSplits:[], validation:splitsValidation, selectedRows:[] })
          
            if(this.state.isListingSearch) { this.props.renderPage(); }
            else { this.getOperationListings(this.context.user.id, this.state.searchData); }
        }
        this.setState({ confirmSplitModal:false }) 
    }
      
    changeSplitsOnChange(e) {
        var splitsValidation = {split: true}
        if (e && e.value) {
          splitsValidation = {split: false}      
        }    
        this.setState({ selectedSplits:e, validation:splitsValidation });
    }

    async disclosuresModal(bool) {
    
        var disclosureValidation = {disclosure: false}
        if (bool) {
          let disclosures = [];
          let disclosureLabel = [];
          let selectedDisclosureValues = this.state.selectedDisclosures;
          if (selectedDisclosureValues.length > 0) {
            selectedDisclosureValues = JSON.parse(this.state.selectedDisclosures)
            for (var disclosure of selectedDisclosureValues) {
              disclosures.push(disclosure.value.toString());
              disclosureLabel.push(disclosure.label);
            }
            this.setState({ disclosureModal:false, disclosureConfirmationModal:true, disclosureValues:disclosures, disclosuresLabel:disclosureLabel });
          }
          else {
            disclosureValidation = {disclosure: true}
            this.setState({ disclosureModal:true, validation:disclosureValidation });
          }
        }
        else {
          this.setState({ disclosureModal:false, validation:disclosureValidation });
        }
    }

    async gridUpdateSeatTypeModal(selectedSeatTypes, selectedListingId) {
        await updateSeatType(selectedSeatTypes, [selectedListingId]);
        if(this.state.isListingSearch) { this.props.renderPage(); }
        else { this.getOperationListings(this.context.user.id, this.state.searchData); }
    }
    async gridUpdateStockTypeModal(selectedTicketTypes, selectedListingId) { 
        await updateStockType(selectedTicketTypes, [selectedListingId]);
    }
    async gridUpdateSplitValue( selectedListingId, selectedSplits ) { 
        await updateSplitsInBulk([selectedListingId], selectedSplits);
    }
    async gridUpdateCostValue(selectedListingId, newCost) { 
      await updateCostForListings(selectedListingId, newCost);
    }
    async gridUpdateTagsValue(listingGroupId, selectedTags) { 
      await updateInternalNotesListings(listingGroupId, "", levelNames.listing, 0, selectedTags); 
    }
    async gridUpdateDisclosureValue(listingGroupId, selectedDisclosureValues) { 
      if(selectedDisclosureValues && selectedDisclosureValues.length){
        var disclosureValues =[];
        for (var disclosure of selectedDisclosureValues) {
          disclosureValues.push(disclosure.value.toString());
        }
        await updateDisclosureInBulk([listingGroupId], disclosureValues, "");
      }
    }
    async editCellValueBasedOnType( listingGroupId, columnName, value ) { 
      var inputList = {};
      if(columnName==operationGridConstant.inHandDate){
        var isValidDate = this.validateDate(value);
        if(isValidDate){
          var inHandDate = await value.split("/").reverse().join("/");
          inputList[columnName] = inHandDate;
          await updateOperationListingFromGridCell(listingGroupId, columnName, inputList);
        }
      }
      if(columnName==operationGridConstant.purchaseOrderDate){
        var purchaseDate = await value.split("/").reverse().join("/");
        var isValidPurchaseOrderDate = this.validateDate(value);
        if(isValidPurchaseOrderDate){
          inputList[columnName] = purchaseDate;
          await updateOperationListingFromGridCell(listingGroupId, columnName, inputList);
        }
      }
      if(columnName==operationGridConstant.seatNumberInfo){
        if(this.state.seatNumberInfo.seatStart !="" && this.state.seatNumberInfo.seatEnd !=""){
          var seatStart = this.state.seatNumberInfo.seatStart;
          var seatEnd = this.state.seatNumberInfo.seatEnd;
          if(parseInt(seatEnd)>=parseInt(seatStart)){
            inputList["seatStart"] = parseInt(seatStart);
            inputList["seatEnd"] = parseInt(seatEnd);
            await updateOperationListingFromGridCell(listingGroupId, columnName, inputList);
            this.props.renderPage(); 
          }
        }
      }
      
      if(columnName!=operationGridConstant.inHandDate && columnName!=operationGridConstant.purchaseOrderDate && columnName!=operationGridConstant.seatNumberInfo){
          inputList[columnName] = value;
          await updateOperationListingFromGridCell(listingGroupId, columnName, inputList);        
      }
      
    }
    /* To validate given string date */
    validateDate(date) {
      return dateValidationString.test(date);
    }
    
      async changeDisclosures(e) {
        var selectedDisclosures = e;
        var disclosuresSelected = [];
        var disclosureValidation = {disclosure: true}
       
        e && e.map(disclosure => (
          disclosuresSelected.push(disclosure.value)
        ))
        if (disclosuresSelected.length > 0) {
          disclosureValidation = { disclosure: false }
        }
        if(e.length){
            selectedDisclosures = JSON.stringify(e)
        }
        this.setState({ selectedDisclosures, validation:disclosureValidation })
      }
    
      async disclosuresNotesModal(bool) {
        var disclosureValidation = {disclosure:false};
        if (bool) {
          var listingGroupIdArray = this.getListingGroupIdsArray();
          await updateDisclosureInBulk(listingGroupIdArray, this.state.disclosureValues, this.state.externalNotes); 
          this.setState({ externalNotes:"", disclosureConfirmationModal:false, validation:disclosureValidation, selectedRows:[], selectedDisclosures:[] })
          
          if(this.state.isListingSearch) { this.props.renderPage(); }
          else { this.getOperationListings(this.context.user.id, this.state.searchData); }
        }
        else {
            this.setState({ disclosureConfirmationModal:false }) 
        }
      }

    changeQCStatus = async() => { 
        var listingGroupId = this.state.listingData.listingGroupId;
        
        let qcStatusResponse = await removeQCHoldStatus(listingGroupId);
        if(qcStatusResponse && !qcStatusResponse.isError){
          let operationListingdata = await this.getOperationListings(this.context.user.id);
          
          if(operationListingdata && operationListingdata.data){
            this.setState({rowData:operationListingdata.data});
          }
          this.setState({qcModal:false})

          if(this.state.isListingSearch) { this.props.renderPage(); }
          else { this.getOperationListings(this.context.user.id, this.state.searchData); }
        }
      }

    setListingCost(e){
        if (e.key == 'e') {
          e.preventDefault();
        }
    }
    showCostConfirmationModal(bool) {
        this.setState({showCostModal:false})
        if (bool) {
            this.setState({costConfirmationModal:true})
        }    
    }

    async updateListingCost(bool) {
        this.setState({costConfirmationModal:false})
        if (bool) {
          var listingGroupIdArray = this.getListingGroupIdsArray();
          let resData = await updateCostForListings(listingGroupIdArray, this.state.newCost);
    
          if (resData && !resData.isError) {
            this.setState({newCost:"", selectedRows:[] });
            
          if(this.state.isListingSearch) { this.props.renderPage(); }
          else { this.getOperationListings(this.context.user.id, this.state.searchData); }
          }
        }        
    }

      async customMenu(menu, selectedRow) {
        await this.setState({selectedRows:selectedRow})
        var selectedRowEvent = []
        let data = {
          ...this.state.searchData,
          currentPage:this.state.currentPage,
          pageLimit:this.state.pageLimit
        }
        await getOperationFilterListings(this.context.user.id, data).then(result => {
          if(result && result.data) selectedRowEvent = result.data; 
        });
        if(menu==listingConst.inhand_date){
         var eventDetails = selectedRowEvent.filter((row) => { return (row.eventId === selectedRow[0].eventId && row.section === "big-title") });
         if(eventDetails && eventDetails.length){
          for (let event of eventDetails) {
            this.setState({maxEventDate:event.eventDate})
          }
         }
         this.setState({inHandModal:true}) 
        }
        if(menu==listingConst.tags){ 
            this.setState({singleListingTag:false, notesModal:true});
        }
        if(menu==listingConst.group_cost){
            this.setState({showCostModal:true})   
        }
        if(menu==listingConst.disclosure){
            this.setState({ disclosureModal:true}) 
        }
        if(menu==listingConst.split){
            this.setState({ splitsModal:true})  
        }
        if(menu==listingConst.viewListing){
          var listingGroupId = selectedRow[0].listingGroupId;
          window.open("view-listing/" + listingGroupId);
        }
        if(menu==listingConst.viewPO){
          var purchaseOrderId = await selectedRow[0].purchaseOrder;
          if( purchaseOrderId !== null ){ window.open("/purchase/purchase-detail/" + purchaseOrderId );}       
        }
    
        //Seat Type
        if(menu==operationConstantData.seatType){
          if(selectedRow){
            var listingGroupId = [];
            selectedRow.length && selectedRow.map((item) => {
              if(item.listingGroupId){
                listingGroupId.push(item.listingGroupId);
              }
            })
            this.setState({ seatTypeModal:true}) 
            this.setState({ selectedListingId:listingGroupId})
         }}
    
         //Stock Type
        if(menu==operationConstantData.stockType){
          if(selectedRow){
            var listingGroupId = [];
            selectedRow.length && selectedRow.map((item) => {
              if(item.listingGroupId){
                listingGroupId.push(item.listingGroupId);
              }
            })
            this.setState({ stockTypeModal:true})
            this.setState({ selectedListingId:listingGroupId}) 
         }}
         if(this.state.isListingSearch) { this.props.renderPage(); }
         else { this.getOperationListings(this.context.user.id, this.state.searchData); }
      }

    changeSeatNumber = props => {
          var seatNumberInfo = {};
          seatNumberInfo.seatStart = props.data.seatStart;
          seatNumberInfo.seatEnd = props.data.seatEnd; 
          this.setState({seatNumberInfo})
          
          return (
            <OperationEditSeatInfo 
              props={props}
              seatNumberInfo={seatNumberInfo}
              onSeatChange={(data)=>{
                  let editRowData = this.state.editRowData;
                  editRowData.listingGroupId = props.data.listingGroupId;
                  this.setState({ editRowData });
                  this.setState({seatNumberInfo:data})
              }}
            />
        );      
    };

  displayValueAsButton = props => {
    if (props.value) {
      let values = props.value.split(",");
      return <div className="tags-section-column">
        {values && values.length && values.map(
          (item) => (
            <button className="myticket-tags-btn stylefont-weight-medium btn">{item}</button>
          ))}</div>
    }
    else {
      return '-';
    }
  };

    getContextSingleMenuItems = (gridOptions)  => {

        var selectedRowData = gridOptions.api.getSelectedRows();
        var listingDetails = selectedRowData.filter((row) => row.listingGroupId>0 );  
        var result = [
          {
            name: "Rows not selected",
            tooltip:"Select any rows"
          }
        ];
        if( listingDetails.length==1 ){
          result = [
            {
              name: operationConstantData.tagsAndNotes,
              action: () => { this.customMenu(listingConst.tags, selectedRowData)}
    
            },
            {
              name: operationConstantData.listingDetails,
              action: () => { this.customMenu(listingConst.viewListing, selectedRowData)}
            },
            {
              name: operationConstantData.purchaseOrderDetails,
              action: () => { this.customMenu(listingConst.viewPO, selectedRowData)}
            }
          ];
        }
        else if( listingDetails.length>1 ){
            result = [
              {
                name: operationConstantData.disclosures,
                action: () => { this.customMenu(listingConst.disclosure, selectedRowData)}
              },
              {
                name: operationConstantData.splits,
                action: () => { this.customMenu(listingConst.split, selectedRowData)}
              },
              {
                name: operationConstantData.inhandDate,
                action: () => { this.customMenu(listingConst.inhand_date, selectedRowData)}
              },
              {
                name: operationConstantData.cost,
                action: () => { this.customMenu(listingConst.group_cost, selectedRowData)}
              },
              {
                name: operationConstantData.tagsAndNotes,
                action: () => { this.customMenu(listingConst.tags, selectedRowData)}
              },
              {
                name: operationConstantData.seatType,
                action: () => { this.customMenu(operationConstantData.seatType, selectedRowData)}
              },
              {
                name: operationConstantData.stockType,
                action: () => { this.customMenu(operationConstantData.stockType, selectedRowData)}
              }
            ];
        }
        return result;
      }
    onSelectionChanged(gridOptions){
        var selectedRow = gridOptions.api.getSelectedRows();
        this.setState({selectedRows:selectedRow});
    }
    onGridReady = async (params) => {
      this.setState({gridApi: params.api, gridColumnApi: params.columnApi});
      this.state.gridApi.sizeColumnsToFit();
    };

    autoColumnResizeOnload = (params) => { //// Auto width  onload      
      setTimeout(async() => {
        this.state.gridColumnApi.autoSizeAllColumns();
      }, 100);       
    }

    autoColumnResize = (params) => {
      var allColumnIds = [];      
      this.state.gridColumnApi.getAllColumns().forEach(function (column) {
        allColumnIds.push(column.colId);
      });
      this.state.gridColumnApi.autoSizeColumns(allColumnIds, false);
      return false;
    };
    

    eventCellRenderer = params => {
      if(params.data.section){
          var eventDate = '';
          if (params.data.timezone) {
            eventDate = moment.utc(params.data.eventDate).tz(params.data.timezone).format('dddd - MMM Do YYYY, h:mm A z');
          }
          else { // If timezone is not available, then UTC will show,
            eventDate = moment.utc(params.data.eventDate).format('dddd - MMM Do YYYY, h:mm A z');
          }
          let cellContent = '';
          cellContent +='<span class="event-name-grid">'+params.data.eventName+'</span> &nbsp;';
          cellContent +='<span class="event-date-grid">'+eventDate+'</span> &nbsp;';
          cellContent +='<span class="event-location-grid">'+params.data.location+'</span> &nbsp;';
         return cellContent;
      }
    }

    render() {
        let maximumPage = Math.ceil(this.state.totalEventCount/this.state.pageLimit);
        return (<>
            <div
                style={{
                    height: "calc(100vh - 10rem)",
                    width: "100%",
                    backgroundColor: "inherit"
                }}
                id="grid-wrapper"
                className="ag-theme-balham common-ag-grid-table operation-grid-item"
            >
                <AgGridReact
                    rowData={this.state.rowData}
                    defaultColDef={this.state.defaultColDef}
                    rowHeight="60"
                    headerHeight="65"
                    suppressColumnVirtualisation={true}
                    frameworkComponents={this.state.frameworkComponents}
                    getContextMenuItems={this.getContextSingleMenuItems}
                    rowSelection= "multiple"
                    onSelectionChanged= {this.onSelectionChanged}
                    enableRangeSelection={true}
                    //onGridReady={this.onGridReady}
                    //onFirstDataRendered={this.autoColumnResizeOnload}
                    resizable={false}
                    >
                    <AgGridColumn
                      pinned = "left"
                      headerCheckboxSelection= {false}
                      headerCheckboxSelectionFilteredOnly = {true}
                      checkboxSelection = {true}
                      width ={45}
                      cellClassRules={cellClassRules}
                    />
                    <AgGridColumn
                        cellClassRules={cellClassRules}
                        headerName=""
                        cellRenderer={this.eventCellRenderer} 
                        //field="locationValue"
                        colSpan={(params) => {
                        if (this.isHeaderRow(params)) {
                            return 21;
                        } else {
                            return 0;
                        }
                        }}
                        width={30}
                    />
                    <AgGridColumn
                        headerName="PO ID"
                        field="purchaseOrder"
                        width ={100}
                        editable={false}
                    />
                    <AgGridColumn headerName="Purchase Date"  field="purchaseDateValue" width ={110} cellEditor="changeDate" 
                      editable={(params) => {return params.data.purchaseOrder!=null} } 
                      onCellValueChanged={(params) => { 
                        if(params.newValue != params.oldValue && params.newValue!="") {
                            this.editCellValueBasedOnType(params.data.listingGroupId, operationGridConstant.purchaseOrderDate, params.newValue)
                        }
                      }} 
                      cellEditorParams={{
                        cellRenderer: 'changeDate',
                        type: operationGridConstant.purchaseOrderDate,
                      }}
                    />
                    <AgGridColumn headerName="In-Hand Date" field="inHandDateValue"  width ={110} editable={true} cellEditor="changeDate"
                      onCellValueChanged={(params) => {
                        if(params.oldValue != params.newValue && params.newValue!="") {
                            this.editCellValueBasedOnType(params.data.listingGroupId, operationGridConstant.inHandDate, params.newValue)
                        }
                      }}
                      cellEditorParams={{
                        cellRenderer: 'changeDate',
                        type: operationGridConstant.inHandDate,
                      }}
                    />
                    <AgGridColumn headerName="Sec" field="sec" width ={90} editable={(params) => {return params.data.seatTypeValue && params.data.seatTypeValue.value!=seatTypes.generalAdmission.id} }
                        onCellValueChanged={(params) => { 
                          if(params.newValue != params.oldValue && params.newValue!="") {
                              this.editCellValueBasedOnType(params.data.listingGroupId, operationGridConstant.section, params.newValue);
                          }
                        }}
                        cellStyle = {cellStyleColor}
                        colSpan={(params) => {
                          if (this.isGeneralAdmissionRow(params)) {
                              return 3;
                          } else {
                              return 0;
                          }
                        }}
                        valueGetter={ function (params) {
                          if(params.data.seatTypeValue && params.data.seatTypeValue.value==seatTypes.generalAdmission.id){
                            return operationConstantData.generalAdmission;
                          }else{
                            return params.data.sec;
                          }
                        }}
                    />
                        
                    <AgGridColumn headerName="Row" field="row" width ={90} editable={(params) => {return params.data.seatTypeValue.value!=seatTypes.generalAdmission.id} }
                        onCellValueChanged={(params) => { 
                          if(params.newValue != params.oldValue && params.newValue!="") {
                              this.editCellValueBasedOnType(params.data.listingGroupId, operationGridConstant.row, params.newValue);
                          }
                        }}  
                        cellStyle = {cellStyleColor} />
                    <AgGridColumn headerName="Seat" field="seat" width ={80} editable={(params) => {return params.data.seatTypeValue.value!=seatTypes.generalAdmission.id} } cellEditor="changeSeatNumber" 
                    suppressKeyboardEvent={ params => params.editing}
                        onCellValueChanged={(params) => {
                            this.editCellValueBasedOnType(params.data.listingGroupId, operationGridConstant.seatNumberInfo, this.state.seatNumberInfo);
                        }} 
                        valueGetter={ (params) => {
                            if (!this.isHeaderRow(params)) {  
                                if(params.data.seatStart && params.data.seatEnd) return params.data.seatStart +"-"+ params.data.seatEnd; 
                                else return ("0-0")
                            }
                        }}
                        cellStyle= {cellStyleColor}/>
                    <AgGridColumn headerName="Quantity" field="quantity" width ={110} editable={(params) => {return params.data.seatTypeValue.value==seatTypes.generalAdmission.id} } cellEditor="operationEditTextField"
                        onCellValueChanged={(params) => { 
                          if(params.newValue != params.oldValue && params.newValue!="") {
                              this.editCellValueBasedOnType(params.data.listingGroupId, operationGridConstant.quantity, parseInt(params.newValue));
                          }
                        }} 
                        cellEditorParams={{
                          cellRenderer: 'operationEditTextField',
                          type: operationGridConstant.quantity
                        }} 
                    />
                    <AgGridColumn headerName="Cost" field="cost" width ={80} editable={true} 
                        cellEditor="operationEditTextField"        
                        cellRenderer={ (params) => {
                          return parseFloat(params.data.cost)?parseFloat(params.data.cost).toFixed(2):'';         
                        }}          
                        onCellValueChanged={async (params) => { 
                         if(params.newValue != params.oldValue && params.newValue!="" ) {
                            this.gridUpdateCostValue([params.data.listingGroupId], params.newValue.toString());
                          }
                        }}
                        cellEditorParams={{
                          cellRenderer: 'operationEditTextField',
                          type: operationConstantData.cost,
                        }}
                    />
                    <AgGridColumn headerName="External Ref ID" field="externalId" width ={110}
                        editable={(params) => {return params.data.purchaseOrder!=null} }
                        onCellValueChanged={(params) => { 
                          if(params.newValue != params.oldValue && params.newValue!="") {
                              this.editCellValueBasedOnType(params.data.listingGroupId, operationGridConstant.externalReference, params.newValue)                            
                          }
                        }}  
                    />
                    <AgGridColumn className="react-dropdown" headerName="Seat Type" field="seatTypeValue" width ={130} editable={true} cellEditor="seatTypeDropdown"
                        valueGetter={ function (params) {  
                            if(params.data.seatTypeValue) return params.data.seatTypeValue.label;
                        }}
                        onCellValueChanged={(params) => { 
                          params.api.redrawRows();
                          if(params.oldValue != params.newValue) {
                            if( params.data.seatTypeValue && params.data.seatTypeValue.value){
                              this.gridUpdateSeatTypeModal(params.data.seatTypeValue.value, params.data.listingGroupId)
                            } 
                          }
                        }}  
                        cellEditorParams={{
                            cellRenderer: 'seatTypeDropdown',
                            option: this.state.allSeatTypes,
                        }}
                    />
                    <AgGridColumn headerName="Stock Type" className="react-dropdown" field="ticketType" width ={130} editable={true} cellEditor="stockTypeDropdown"
                        valueGetter={ function (params) {  
                            if(params.data.ticketType) return params.data.ticketType.label; 
                        }}
                        onCellValueChanged={(params) => {
                          if(params.oldValue != params.newValue) {
                            if( params.data.ticketType && params.data.ticketType.value){
                              this.gridUpdateStockTypeModal(params.data.ticketType.value, params.data.listingGroupId)
                            } 
                          }
                        }}
                        cellEditorParams={{
                            cellRenderer: 'stockTypeDropdown',
                            option: this.state.allStockTypes
                        }}
                    />
                    <AgGridColumn headerName="Owner" field="companyValue" className="react-dropdown" width ={200} editable={false} cellEditor="ownerDropdown"
                        valueGetter={ function (params) {  
                            if(params.data.companyValue) return params.data.companyValue.label; 
                        }}
                        onCellValueChanged={(params) => {
                          if(params.oldValue != params.newValue) {
                            if( params.data.ticketingSystemAccount && params.data.ticketingSystemAccount.value){
                              this.editCellValueBasedOnType(params.data.listingGroupId, operationGridConstant.owner, params.data.companyValue.value)
                            } 
                          }
                        }}
                        cellEditorParams={{
                            cellRenderer: 'ownerDropdown',
                            option: this.state.allCompaniesList
                        }}
                    />
                    <AgGridColumn headerName="Account" field="ticketingSystemAccount" width ={200} editable={true} cellEditor="accountDropdown"
                        valueGetter={ function (params) {  
                            if(params.data.ticketingSystemAccount) return params.data.ticketingSystemAccount.label; 
                        }}
                        onCellValueChanged={(params) => {
                          if(params.oldValue != params.newValue) {
                            if( params.data.ticketingSystemAccount && params.data.ticketingSystemAccount.value){
                              this.editCellValueBasedOnType(params.data.listingGroupId, operationGridConstant.ticketingSystemAccount, params.data.ticketingSystemAccount.value)
                            } 
                          }
                        }}
                        cellEditorParams={{
                            cellRenderer: 'accountDropdown',
                            option: this.state.ticketingSystemAccountDetails
                        }}
                    />
                    <AgGridColumn headerName="Assign" field="assign" width ={100} editable={false} />
                    <AgGridColumn headerName="Issue" field="issue" width ={100} editable={false} />
                    <AgGridColumn headerName="Last 4 Digits" field="lastDigits" width ={100} 
                     cellEditor="operationEditTextField"
                      editable={(params) => {return params.data.purchaseOrder!=null} }
                      onCellValueChanged={async (params) => {
                        if(params.newValue != params.oldValue && params.newValue!="" ) {                           
                            this.editCellValueBasedOnType(params.data.listingGroupId, operationGridConstant.last4Digits, params.newValue)                   
                        }
                      }} 
                      cellEditorParams={{
                        cellRenderer: 'operationEditTextField',
                        type: operationGridConstant.last4Digits,
                      }}
                    />
                    
                    <AgGridColumn className="react-dropdown" headerName="Splits" field="splitValue" width ={150} editable={true} cellEditor="splitDropdown"
                        valueGetter={ function (params) {  
                            if(params.data.splitValue) return params.data.splitValue.label;
                        }}
                        onCellValueChanged={(params) => {
                          if(params.oldValue != params.newValue) {
                            if( params.data.splitValue && params.data.splitValue.value){
                              this.gridUpdateSplitValue( params.data.listingGroupId, params.data.splitValue.value)
                            } 
                          }
                        }}  
                        cellEditorParams={{
                            cellRenderer: 'splitDropdown',
                            option: this.state.allSplitsList,
                        }}
                    />
                    
                    <AgGridColumn headerName="Tags" field="tagValue" width ={400} suppressSizeToFit = "true" editable={true} cellRenderer= "displayValueAsButton"
                        valueGetter={ function (params) {  
                          if(params.data.tagValue) return params.data.tagValue.map(tag => tag.label).toString();  
                        }} 
                        cellEditor = "tagsDropdown" 
                            onCellValueChanged={(params) => {
                              if(params.oldValue != params.newValue) {
                                if( params.data.tagValue && params.data.tagValue.length){
                                    this.gridUpdateTagsValue( params.data.listingGroupId, JSON.stringify(params.data.tagValue))
                                } 
                              } 
//this.autoColumnResize(params);
                            }} 
                            cellEditorParams={{
                                cellRenderer: 'tagsDropdown',
                                option: this.state.allTags,
                            }}
                    />
                   
                    <AgGridColumn headerName="Disclosures" field="disclosureValue" width ={400} editable={true} cellRenderer= "displayValueAsButton"
                        valueGetter={ function (params) {  
                          if(params.data.disclosureValue) return params.data.disclosureValue.map(disclosure => disclosure.label).toString();  
                        }} 
                        cellEditor = "disclosureDropdown" 
                            onCellValueChanged={(params) => { 
                              if(params.oldValue != params.newValue) {
                                if( params.data.disclosureValue && params.data.disclosureValue.length){
                                    this.gridUpdateDisclosureValue( params.data.listingGroupId, params.data.disclosureValue)
                                } 
                              } 
//this.autoColumnResize(params);
                            }} 
                            cellEditorParams={{
                                cellRenderer: 'disclosureDropdown',                                
                                option: this.state.disclosuresList,
                            }}
                    />
                    <AgGridColumn headerName="Settings" cellRenderer= "changeQCHoldStatus" width ={250}/>
                    
                    </AgGridReact>
                    </div>
                    {this.state.totalEventCount && <div className="container-fluid">
                      <div className="custom-ag-grid-pagination">
                        <div>
                          <span className="ag-pg-text-info">
                            <span className="custom-pagination-operation-page">{(this.state.currentPage*this.state.pageLimit)-this.state.pageLimit+1}  to {this.state.currentPage*this.state.pageLimit < this.state.totalEventCount ? this.state.currentPage*this.state.pageLimit : this.state.totalEventCount} of {this.state.totalEventCount}</span>
                          </span>
                          <span>
                            <span className={`pagination-icon ${this.state.currentPage === 1 && 'inactive'}`} onClick={()=>this.state.currentPage !== 1 && this.onPageChange(1)}><Icons type="vertical-right"  /></span>
                            <span className={`pagination-icon ${this.state.currentPage === 1 && 'inactive'}`} onClick={()=>this.state.currentPage !== 1 &&this.onPageChange(this.state.currentPage-1)}><Icons type="left"  /></span>
                            <span className="custom-pagination-operation-page">Page {this.state.currentPage} of {maximumPage}</span>
                            <span className={`pagination-icon ${this.state.currentPage === maximumPage &&'inactive'}`} onClick={()=>this.state.currentPage !== maximumPage && this.onPageChange(this.state.currentPage+1)}><Icons type="right"   /></span>                    
                            <span className={`pagination-icon ${this.state.currentPage === maximumPage &&'inactive'}`} onClick={()=>this.state.currentPage !== maximumPage &&this.onPageChange(maximumPage)}><Icons type="vertical-left" /></span>
                          </span>                        
                        </div>
                      </div>
                    </div>}
                    
            {/**Qc modal confirmation */}

            <Modal show={this.state.qcModal} onHide={() => { this.setState({qcModal:false}); this.state.gridApi.redrawRows(); }}>
            <Modal.Header closeButton>
                <Modal.Title>Are you sure?</Modal.Title>
            </Modal.Header>
            {/* <Modal.Body>
                <h4 style={{ textAlign: 'center' }} className="confirmation-channel-text" >Do you really want to remove QC tag ? </h4>
            </Modal.Body> */}
            <Modal.Footer className="modal-btn-align">
                <Button variant="secondary" onClick={() =>{ this.setState({qcModal:false}); this.state.gridApi.redrawRows(); }}>
                Cancel
                </Button>
                <Button variant="primary" className="btn btn-success info-btn-listing" onClick={(e) => this.changeQCStatus()}>
                Submit
                </Button>
            </Modal.Footer>
            </Modal>

            <TagAndNotesModalForm 
                showModal={this.state.notesModal} 
                tagOption={this.state.allTags} 
                values={this.state.internalNotes} 
                selectedTags = {this.state.selectedTags}
                tagsOnchange={(e) => this.setState({selectedTags: JSON.stringify(e)}) } 
                hideModal={ (e) => { this.internalNotesModal(e); this.state.gridApi.redrawRows(); }} 
                notesOnChange={(e) => this.setState({internalNotes:e.target.value}) } 
            />    
            <InHandModalForm 
                showModal={this.state.inHandModal}
                inhandDate={this.state.selectedInhandDate} 
                modalEventDate = {this.state.maxEventDate}
                inhandDateOnChange={(e) => this.setState({selectedInhandDate:new Date(e)}) } 
                hideModal={ (e) => this.showInHandConfirmationModal(e)} 
                showInhandConfirmation={ this.state.inHandConfirmationModal } 
                hideConfirmationModal={ (e) => this.updateInHandDate(e)} 
            />    

            <SeatTypeModalForm
                showModal={this.state.seatTypeModal}
                ticketTypeOption={this.state.allSeatTypes} 
                selectedSeatTypes = {this.state.selectedTicketTypes}
                ticketTypesOnchange={(e) => this.setTicketTypesChange(e) } 
                hideModal={ (e) => this.showSeatTypeConfirmationModal(e)} 
                showSeatTypeConfirmation={ this.state.seatTypeConfirmationModal } 
                hideConfirmationModal={ (e) => this.updateSeatTypeModal(e)} 
            />  

            <StockTypeModalForm
                showModal={this.state.stockTypeModal}
                stockTypesOption={this.state.allStockTypes} 
                selectedStockTypes = {this.state.selectedStockTypes}
                StockTypesOnchange={(e) => this.setStockTypesChange(e) } 
                hideModal={ (e) => this.showStockTypeConfirmationModal(e)} 
                showStockTypeConfirmation={ this.state.stockTypeConfirmationModal } 
                hideConfirmationModal={ (e) => this.updateStockTypeModal(e)} 
            />

            <CostModalForm
                showModal={this.state.showCostModal}
                costOnChange = {(e) => this.handleCostOnChange(e) } 
                validation = {this.state.validation}
                costOnKeyDown = {(e) => this.setListingCost(e) } 
                value={this.state.newCost}
                hideModal={ (e) => this.showCostConfirmationModal(e) } 
                showCostConfirmationModal={this.state.costConfirmationModal}
                costUpdate={(e) => this.updateListingCost(e)}
            />

            <DisclosureModalForm
                showModal={this.state.disclosureModal}
                hideModal={ (e) => this.disclosuresModal(e) }
                validation = {this.state.validation}
                handleExternalNotesChange={(e) => this.setState({externalNotes:e.target.value}) } 
                externalNotes = {this.state.externalNotes}
                disclosureOnChange = {(e) => this.changeDisclosures(e) }
                selectedDisclosures = {this.state.selectedDisclosures}
                disclosuresList={this.state.disclosuresList}
                disclosureConfirmationModal={this.state.disclosureConfirmationModal}
                updateDisclosuresModal= {(e) => this.disclosuresNotesModal(e) }
                disclosuresLabel = { this.state.disclosuresLabel }
            />

            <SplitsModalForm
                showModal={this.state.splitsModal}
                splitsList={this.state.allSplitsList}
                validation = {this.state.validation}
                splitsOnChange = {(e) => this.changeSplitsOnChange(e) }
                onHide={(e) => this.displaySplitsModal(e) }
                selectedSplits={this.state.selectedSplits}
                confirmSplitModal={this.state.confirmSplitModal}
                hideConfirmationModal={(e) => this.updateSplits(e) }
            />

        </>
        );
    }
}

// cell color function
function cellStyleColor(params) {
  var color = numberToColor(params.value);
  return { color: color };
}
function numberToColor(val) {
    return '#2C91EE';
}

export default withTranslation()(OperationListingGrid, document.getElementsByClassName('listingContextMenu'));
