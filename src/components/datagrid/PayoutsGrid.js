import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import loadable from "@loadable/component";
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import { listStripePayouts } from "../../services/stripeService";
import { useTranslation } from "react-i18next";
import Input from 'antd/es/input';
import Loader from "../Loader/Loader";
import { addEvents, getAllEventStatus, getVenueFilter, venueSearchFilter } from "../../services/eventService";
import { Button, Modal } from 'react-bootstrap';

import styled from 'styled-components'
import { Envelope } from '@styled-icons/bootstrap/Envelope'
import { Printer } from '@styled-icons/bootstrap/Printer'

const EnvelopeIcon = styled(Envelope)`
  color: #ffffff;
  width:30px;
  height:30px;
`
const PrinterIcon = styled(Printer)`
  color: #ffffff;
  width:30px;
  height:30px;
`



const SearchIcon = loadable(() => import("@material-ui/icons/Search"));

const PayoutsGrid = () => {
  const { t } = useTranslation();
  const [remittanceData, setRemittanceData] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [loading, setLoading] = useState(true);
  const [viewReport, setReportModal] = useState(false);



  const handleSearch = ({ target: { value } }) => {
    setSearchValue(value);
  };
  const [gridApi, setGridApi] = useState(null);
  const [gridColumnApi, setGridColumnApi] = useState(null);
  const [rowData, setRowData] = useState(null);

  const onGridReady = (params) => {
    setGridApi(params.api);
    setGridColumnApi(params.columnApi);

    const updateData = (data) => {
      setRowData(data);
    };

    fetch(
      'https://www.ag-grid.com/example-assets/master-detail-dynamic-data.json'
    )
      .then((resp) => resp.json())
      .then((data) => updateData(data));
  };

  const onFirstDataRendered = (params) => {
    setTimeout(function () {
      params.api.getDisplayedRowAtIndex(1).setExpanded(true);
    }, 0);
  };

  //Opening the order details page
  function viewOrderDetails(params) {
    setReportModal(true);
  }

  // function sizeColumnsToFit(): void;
  const columnDefs = [
    {
      headerName: "Payout ID",
      field: "id",
      width: 500
    },
    {
      headerName: "Transaction ID",
      field: "transId",
      width: 180
    },
    {
      headerName: t("payouts.payout_date"),
      field: "date",
      width: 180
    },
    {
      headerName: t("payouts.payout_amount"),
      field: "amount",
      width: 180
    },
    {
      headerName: t("payouts.seller_name"),
      field: "sellerName",
      width: 180
    },
    {
      headerName: t("payouts.seller_phone"),
      field: "sellerPhone",
      width: 180
    },
    {
      headerName: t("payouts.receipt"),
      field: "Receipt",
      width: 180,
      cellRenderer: "LinkComponent"
    }
  ];

  const rowSample = [
    { id: "0923471", transId: "1039582", date: "09/03/2021", amount: "$ 230", sellerName: "Tim Miller", sellerPhone: "(413) 509-6793", Receipt: "View Receipt" },
    { id: "0923471", transId: "1039582", date: "09/03/2021", amount: "$ 230", sellerName: "Tim Miller", sellerPhone: "(413) 509-6793", Receipt: "View Receipt" },
    { id: "0923471", transId: "1039582", date: "09/03/2021", amount: "$ 230", sellerName: "Tim Miller", sellerPhone: "(413) 509-6793", Receipt: "View Receipt" },
    { id: "0923471", transId: "1039582", date: "09/03/2021", amount: "$ 230", sellerName: "Tim Miller", sellerPhone: "(413) 509-6793", Receipt: "View Receipt" },
    { id: "0923471", transId: "1039582", date: "09/03/2021", amount: "$ 230", sellerName: "Tim Miller", sellerPhone: "(413) 509-6793", Receipt: "View Receipt" },
    { id: "0923471", transId: "1039582", date: "09/03/2021", amount: "$ 230", sellerName: "Tim Miller", sellerPhone: "(413) 509-6793", Receipt: "View Receipt" },
    { id: "0923471", transId: "1039582", date: "09/03/2021", amount: "$ 230", sellerName: "Tim Miller", sellerPhone: "(413) 509-6793", Receipt: "View Receipt" }
  ];



  useEffect(() => {
    const startFunc = async () => {
      const date = Date.now();
      const stripePayouts = await listStripePayouts(date, 100);
      await setRemittanceData(stripePayouts);
      await setLoading(false);
    }
    startFunc();
  }, []);

  // ag grid auto width
  const onGridSizeChanged = (params) => {
    var gridWidth = document.getElementById('grid-wrapper').offsetWidth;
    var columnsToShow = [];
    var columnsToHide = [];
    var totalColsWidth = 0;
    var allColumns = params.columnApi.getAllColumns();
    for (var columnInfoCount = 0; columnInfoCount < allColumns.length; columnInfoCount++) {
      var column = allColumns[columnInfoCount];
      totalColsWidth += column.getMinWidth();
      if (totalColsWidth > gridWidth) {
        columnsToHide.push(column.colId);
      } else {
        columnsToShow.push(column.colId);
      }
    }
    params.columnApi.setColumnsVisible(columnsToShow, true);
    params.columnApi.setColumnsVisible(columnsToHide, false);
    params.api.sizeColumnsToFit();
  };
  return !loading ? (
    <div>
      <div
        style={{
          height: "calc(100vh - 14rem)",
          width: "100%",
          backgroundColor: "inherit"
        }}
        id="grid-wrapper"
        className="ag-theme-alpine ag-theme-balham market-place-admin ag-custom-balham"
      >
        <div className="search-field-sec">
          <div className="search-top-input" style={{ position: 'relative', maxWidth: '80%' }}>
            <Input
              value={searchValue}
              className="mb-3 payout-search"
              onChange={handleSearch}
              placeholder={t("search.title")}
              suffix={<SearchIcon />}
              bordered={false}
            />
            {searchValue &&
              <button className="search-close-icon" onClick={(e) => setSearchValue('')} >X</button>}
          </div>
        </div>
        <AgGridReact
          defaultColDef={{ width: 300 }}
          masterDetail={true}
          embedFullWidthRows={true}
          isRowMaster={function (dataItem) {
            return dataItem ? dataItem.callRecords.length > 0 : false;
          }}
          defaultColDef={{ flex: 1 }}
          // onGridSizeChanged={onGridSizeChanged}
          getRowNodeId={function (data) {
            return data.account;
          }}
          detailCellRendererParams={{
            detailGridOptions: {
              columnDefs: [
                { field: 'Order ID' },
                { field: 'Listing ID' },
                {
                  field: 'Order Date',
                },
                {
                  field: 'Order Amount',
                  minWidth: 150,
                },
                {
                  field: 'Order Fees',
                  minWidth: 150,
                },
              ],

              defaultColDef: { flex: 1 },
            },
            getDetailRowData: function (params) {
              params.successCallback(params.data.callRecords);
            }
          }}
          defaultColDef={{ resizable: false }}
          onGridReady={onGridReady}
          onFirstDataRendered={onGridSizeChanged}
          // onGridSizeChanged={onGridSizeChanged}
          rowData={rowData}
          rowHeight="60"
          headerHeight="65"
          sortable={true}
        >
          <AgGridColumn field="name" cellRenderer="agGroupCellRenderer" />
          <AgGridColumn field="account" />
          <AgGridColumn field="calls" />
          <AgGridColumn
            field="minutes"
            valueFormatter="x.toLocaleString() + 'm'"
          />
        </AgGridReact>
      </div>

      {/* modal for event update */}
      <Modal className="view-receipt-modal"
        title="Payout Receipt"
        show={viewReport}
        footer={null}
        closable={true}
        width="50%"><Modal.Header closeButton><Modal.Title>Payout Receipt</Modal.Title></Modal.Header>
        <Modal.Body>
          <div className="receipt-grid-item">
            <div className="receipt-payment-detail">
              <div className="list-details-view">
                <div className="pay-list-left-view">
                  <h6>Payout ID:</h6>
                </div>
                <div className="pay-list-right-view">
                  <h6>0923471</h6>
                </div>
                <div className="pay-list-left-view">
                  <h6>Seller ID:</h6>
                </div>
                <div className="pay-list-right-view">
                  <h6>1039480</h6>
                </div>
                <div className="pay-list-left-view">
                  <h6>Transaction ID:</h6>
                </div>
                <div className="pay-list-right-view">
                  <h6>1039582</h6>
                </div>
                <div className="pay-list-left-view">
                  <h6>Payout Date:</h6>
                </div>
                <div className="pay-list-right-view">
                  <h6>09/03/2021</h6>
                </div>
                <div className="pay-list-left-view">
                  <h6>Amount:</h6>
                </div>
                <div className="pay-list-right-view">
                  <h6>$230.00</h6>
                </div>
                <div className="pay-list-left-view">
                  <h6>Seller Name:</h6>
                </div>
                <div className="pay-list-right-view">
                  <h6>Tim Miller</h6>
                </div>
                <div className="pay-list-left-view">
                  <h6>Seller Phone:</h6>
                </div>
                <div className="pay-list-right-view">
                  <h6>(413) 509-6793</h6>
                </div>
              </div>
              <div>
                <Link to="">
                  <Button color="white" className="is-rounded view-seller-btn">
                    <span>View Seller Profile</span>
                  </Button>
                </Link>
              </div>
            </div>
            <div className="email-receipt">
              <div className="email-generate-item">
                <h5>Email Receipt</h5>
                <p>Generate an email invoice of this order.</p>
                <Button className="generate-email-btn"><EnvelopeIcon /> Generate Email</Button>
              </div>
              <div className="email-generate-item">
                <h5>Print Receipt</h5>
                <p>Download a PDF version of this invoice.</p>
                <Button className="print-receipt-btn"><PrinterIcon /> Print Receipt</Button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </div >
  ) : (
    <Loader />
  );

};

export default PayoutsGrid;