import React, { useEffect, useState, useContext } from "react";
import moment from "moment";
import loadable from "@loadable/component";
import UserContext from "../../contexts/user.jsx";
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import { useHistory, withRouter } from "react-router-dom";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import Input from 'antd/es/input';
import { userRoles, userRolesLower, userRolesArray } from '../../constants/constants';
import Modal from 'antd/es/modal';
import {
  updateUserFirstName,
  updateUserLastName,
  updateUserNotes,
  sendMagicLink,
  updateUser,
  getUserById
} from "../../services/userService";
//For Moving to production, we are commenting this out since this is unused and old code
//import {  getNumberOfUserListings,  getNumberOfDeliveredUserListings} from "../../services/listingsService";
import { getAllUsers, getAllUsersByRole } from "../../services/usersService";
import { useTranslation } from "react-i18next";
import { autoSizeAllColumns, onGridSizeChanged } from "./DataGridHelpers";
import Loader from "../Loader/Loader";
const SearchIcon = loadable(() => import("@material-ui/icons/Search"));

const AllUsersGrid = (params) => {

  const { user } = useContext(UserContext);

  const { t } = useTranslation();
  const [rowData, setRowData] = useState([]);
  const [columnData, setColumn] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [rowSelection] = useState("single");

  const [loading, setLoading] = useState(false);
  const [mailSuccessModal, setMailSuccessModal] = useState(false);
  const [mailSuccessMessage, setMailSuccessMessage] = useState('');

  useEffect(() => {
    setLoading(false);
  }, []);

  useEffect(() => {
    let columnDefs = [];
    columnDefs = [{
      headerName: t("seller.company"),
      field: "company_name"
    },
    {
      headerName: t("userInfo.firstName"),
      field: "first_name",
      onCellValueChanged: function (params) {
        updateFirstName(params);
      },
      editable: true
    },
    {
      headerName: t("userInfo.lastName"),
      field: "last_name",
      onCellValueChanged: function (params) {
        updateLastName(params);
      },
      editable: true
    },
    {
      headerName: t("userInfo.email"),
      field: "email"
    },
    {
      headerName: t("userInfo.phone"),
      field: "phone"
    },
    {
      headerName: t("userInfo.role"),
      field: "user_role",
      valueFormatter: params => userRolesArray[parseInt(params.data.user_role) - 1],
    },
    {
      headerName: t("userInfo.notes"),
      field: "notes"
    },
    {
      headerName: t("userInfo.lastLogin"),
      field: "last_login",
      valueFormatter: (params) => {
        return params.data.last_login ? moment(params.data.last_login).format('MM/DD/YYYY HH:mm') : '-';
      }
    }]

    setColumn(columnDefs);

  }, [user]);


  const viewProfile = props => {
    const { t } = useTranslation();
    return (
      <span
        style={{ cursor: "pointer", textDecoration: "underline" }}
        className="reset-password w-100 h-auto p-0"
        onClick={e => redirectPage(props.data.id)}
      >
        {t("userInfo.viewDetails")}
      </span>
    );
  };

  const history = useHistory();

  const redirectPage = id => {
    history.push({ pathname: "./user/" + id });
  }

  const sendRestMail = async (email) => {
    var response = await sendMagicLink(email);
    setMailSuccessModal(true)
    // let user know if we have an email on file that matches, they should get one.
    if (response.reset_status) {
      setMailSuccessMessage(t("login.success"));
    } else {
      setMailSuccessMessage(t("login.tryagain"));
    }
  }

  const resetPassword = props => {
    const { t } = useTranslation();
    return (
      <span
        style={{ cursor: "pointer", textDecoration: "underline" }}
        className="reset-password w-100 h-auto p-0"
        onClick={e => sendRestMail(props.data.email)}
      >
        {t("userInfo.password.reset")}
      </span>
    );
  };
//For Moving to production, we are commenting this out since this is unused and old code
  /*const calculateDeliveryPercentage = async performerId => {
    console.info(`calculating delivery for performer ${performerId}`);
    const allUserListings = await getNumberOfUserListings(performerId);
    const confirmedDeliveredUserListings = await getNumberOfDeliveredUserListings(
      performerId
    );
    if (allUserListings === 0) {
      return `No user listings`;
    }
    return `${(
      (confirmedDeliveredUserListings / allUserListings) *
      100
    ).toFixed(2)}%`;
  };*/

  const handleSearch = e => {
    setSearchValue(e.target.value);
  };

  const updateFirstName = async function (params) {
    console.log(
      `We will update the price in the DB to ${params.newValue} if it's formatted right`
    );
    updateUserFirstName(params.data.id, params.newValue);
    return params.newValue;
  };
  const updateLastName = async function (params) {
    console.log(
      `We will update the price in the DB to ${params.newValue} if it's formatted right`
    );
    updateUserLastName(params.data.id, params.newValue);
    return params.newValue;
  };
  const updateNotes = async function (params) {
    console.log(
      `We will update the notes in the DB to ${params.newValue} if it's formatted right`
    );
    updateUserNotes(params.data.id, params.newValue);
    return params.newValue;
  };

  const updateUserRole = async data => {
    const newUser = await getUserById(data.id);
    delete newUser.fullName;
    await updateUser({ ...newUser, userRole: data.userRole });
    alert(t("userInfo.updateUserRole"));
  };

  function viewUserDetails(params) {
    window.open("/user/" + params.data.id);
  }

  const frameworkComponents = {
    viewProfile,
    resetPassword
  };
  //For Moving to production, we are commenting this out since this is unused and old code
  /*async function fetchData() {

    let data = '';
    if (user.userRole == userRoles.broker) {
      data = await getAllUsersByRole(userRoles.broker_user);
    }
    else if (user.userRole == userRoles.marketplace_admin) {
      data = await getAllUsersByRole(userRoles.marketplace_user);
    }
    else if (user.userRole == userRoles.super_admin) {
      data = await getAllUsersByRole(2);
    }
    else { data = await getAllUsers(); }

    // calculate delivery percentage for each
    for (let user in data) {
      data[user].deliveryPercentage = await calculateDeliveryPercentage(
        parseInt(user)
      );
    }
    setRowData(data);
    setLoading(false);
    return;
  }*/
  useEffect(() => {
    setLoading(true);
    async function fetchData() {
      let data = '';
      /* if (user.userRole == userRolesLower.broker) {
        data = await getAllUsersByRole(4);
      }
      else if (user.userRole == userRolesLower.marketplace_admin) {
        data = await getAllUsersByRole(10);
      }
      else if (user.userRole == userRolesLower.super_admin) {
        data = await getAllUsersByRole(2);
      }
      else if (user.userRole == userRolesLower.admin) {
        data = await getAllUsersByRole(3);
      }
      else if (user.userRole == userRolesLower.accounting_admin) {
        data = await getAllUsersByRole(8);
      }
      else if (user.userRole == userRolesLower.processing_admin) {
        data = await getAllUsersByRole(9);
      }
      else if (user.userRole == userRolesLower.support_admin) {
        data = await getAllUsersByRole(14);
      }
      else { data = await getAllUsers(); } */

      if (user.userRole == userRolesLower.super_admin) {
        data = await getAllUsersByRole(2);
      }
      else {
        data = await getAllUsers();
      }
      
      //For Moving to production, we are commenting this out since this is unused and old code

      // calculate delivery percentage for each
     /* for (let user in data) {
        data[user].deliveryPercentage = await calculateDeliveryPercentage(
          parseInt(user)
        );
      }*/
      setRowData(data);
      setLoading(false);
      return;
    }


    fetchData();
    params.createFunctionToCall(() => fetchData)
  }, [params.createFunctionToCall]);


  return !loading ? (
    <div
      style={{
        height: "calc(100vh - 17rem)",
        width: "100%",
        backgroundColor: "inherit"
      }}
      className="ag-theme-alpine ag-theme-balham common-ag-grid-table"
      id="grid-wrapper"
    >
      <div className="user-search-item">
        <div className="search-field-sec">
          <div className="search-top-input" style={{ position: 'relative' }}>
            <Input
              value={searchValue}
              className="search-input stylefont-weight-medium"
              style={{ width: "100%" }}
              onChange={handleSearch}
              placeholder={t("Search User")}
              suffix={<SearchIcon />}
              bordered={false}
            />
            {searchValue &&
              <button className="search-close-icon"   onClick={(e) => setSearchValue('')} >X</button>}
          </div>
        </div>
        {(user.userRole != userRolesLower.broker_user && user.userRole != userRolesLower.accounting_user && user.userRole != userRolesLower.processing_user && user.userRole != userRolesLower.marketplace_user && user.userRole != userRolesLower.support_user) && (
          <div className="add-new-user-item">
            <button onClick={params.click} className="btn add-newuser-btn primary-bg-color stylefont-weight-medium">Add New User</button>
          </div>
        )}
      </div>

      <AgGridReact
        masterDetail={true}

        // isRowMaster={function (dataItem) {
        //   return dataItem ? dataItem.callRecords.length > 0 : false;
        // }}
        defaultColDef={{ flex: 1 }}
        detailCellRendererParams={{
          detailGridOptions: {
            columnDefs: [
              { field: 'company_name' },
              { field: 'first_name' },
              {
                field: 'last_name',
                minWidth: 150,
              },
              {
                field: 'email',
              },
              {
                field: 'user_role',
                minWidth: 150,
              },
            ],
            defaultColDef: { flex: 1 },
          },
          // getDetailRowData: function (params) {
          //   params.successCallback(params.data.callRecords);
          // },
        }}
        // onGridReady={onGridReady}
        // onFirstDataRendered={onFirstDataRendered}
        // rowData={rowData}
        // properties
        columnDefs={columnData}
        rowData={rowData}
        defaultColDef={{ filter: true, resizable: false }}
        quickFilterText={searchValue}
        frameworkComponents={frameworkComponents}
        onRowClicked={viewUserDetails}
        // events
        rowSelection={rowSelection}
        onFirstDataRendered={onGridSizeChanged}
        // onGridSizeChanged={onGridSizeChanged}
        rowHeight="60"
        headerHeight="65"
        sortable={true}
      >

      </AgGridReact>
      <Modal className="add-user-modal"
        title={"Forgot Password"}
        visible={mailSuccessModal}
        cancelButtonProps={{ style: { display: 'none' } }}
        onOk={(e) => setMailSuccessModal(false)}
        onCancel={(e) => setMailSuccessModal(false)}
        destroyOnClose={true}
        maskClosable={false}

      >
        <p>{mailSuccessMessage} </p>
      </Modal>
    </div>
  ) : (
    <Loader />
  );
};

export default withRouter(AllUsersGrid);
