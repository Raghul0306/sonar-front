import React, { useEffect, useState } from "react";
import loadable from "@loadable/component";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import { listStripePayouts } from "../../services/stripeService";
import { useTranslation } from "react-i18next";
// import { onGridSizeChanged } from "./DataGridHelpers";
import moment from "moment";
import Loader from "../Loader/Loader"; import styled from 'styled-components';
import { operationConstantData } from "../../constants/constants";
const PurchaseDetailGrid = (props) => {
  const { t } = useTranslation();
  const [remittanceData, setRemittanceData] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [loading, setLoading] = useState(true);
  const [gridApi, setGridApi] = useState({});
  const [rowData, setRowData] = useState([]);

  const handleSearch = ({ target: { value } }) => {
    setSearchValue(value);
  };

  const onFirstDataRendered = (params) => {
    params.api.sizeColumnsToFit();
  };
  const columnDefs = [
    {
      headerName: "Event Name",
      field: "eventname",
      cellRenderer: function (params) {
        let keyData = params.data.eventname;
        return keyData;
      },
      minWidth: 198
    },
    {
      headerName: "Event Date",
      field: "eventdate",
      valueGetter: getCreatedAt,
      minWidth: 122
    },
    {
      headerName: "Reference ID",
      field: "referenceid",
      minWidth:139
    },
    {
      headerName: "Listing ID",
      field: "listing_group_id",
      minWidth:137
    },
    {
      headerName: "Sec",
      field: "sec",
      cellRenderer: function (params) {
        return params.data.seat_type_name == operationConstantData.generalAdmission ? operationConstantData.generalAdmission : params.data.sec;
      },
      colSpan: params => params.data.seat_type_name == operationConstantData.generalAdmission ? 3 : 1,
      cellStyle:{"color":"#2C91EE"},      
      minWidth: 80
    },
    {
      headerName: "Row",
      cellStyle:{"color":"#2C91EE"},
      field: "row",
      minWidth: 63,
      cellRenderer: function (params) {
        return params.data.seat_type_name == operationConstantData.generalAdmission ? '' : params.data.row;
      },
    },
    {
      headerName: "Seats ",
      cellStyle:{"color":"#2C91EE"},
      field: "seat",
      minWidth:81,
      cellRenderer: function (params) {
        return params.data.seat_type_name == operationConstantData.generalAdmission ? '' : params.data.seat;
      },
    },
    {
      headerName: "Cost ",
      field: "cost",
      cellStyle:{"color":"#00A399"},
      cellRenderer: function (params) {
        return parseFloat(params.data.cost).toFixed(2);
      },
      minWidth:93
    },
    {
      headerName: "Unit Cost ",
      field: "unitcost",
      cellStyle:{"color":"#00A399"},
      cellRenderer: function (params) {
        return parseFloat(params.data.unitcost).toFixed(2);
      },
      minWidth: 100
    },
    {
      headerName: "Quantity ",
      field: "quantity",
      minWidth:116
    },
  ];
  const staticCellStyle = {color: '#00A399',  };

  let orderList = props.orderList && props.orderList.length ? props.orderList : '';

  function getCreatedAt(params) {
    return params.data.eventdate
      ? moment(params.data.eventdate).format("MM/DD/YYYY")
      : "";
  }
  useEffect(() => {
    const startFunc = async () => {
      const date = Date.now();
      const stripePayouts = await listStripePayouts(date, 100);
      await setRemittanceData(stripePayouts);
      await setLoading(false);
    }
    startFunc();
    setRowData(orderList)
  }, [orderList]);


  return !loading ? (
    <div>
      <div
        style={{
          height: "calc(100vh - 14rem)",
          width: "100%",
          backgroundColor: "inherit",
          paddingBottom:'60px'
        }}
        id="grid-wrapper"
        className="ag-theme-balham custom-table-purchase purchase-order-table"
      >
        <AgGridReact
          // properties
          columnDefs={columnDefs}
          rowData={rowData}
          defaultColDef={{ filter: true, resizable: false }}
          quickFilterText={searchValue}
          // events
          onFirstDataRendered={onFirstDataRendered}
          // onGridSizeChanged={onGridSizeChanged}
          rowHeight="60"
          headerHeight="65"
          sortable={true}
        />
      </div>

    </div>
  ) : (
    <Loader />
  );
};

export default PurchaseDetailGrid;
