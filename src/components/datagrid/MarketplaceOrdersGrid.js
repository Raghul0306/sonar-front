import React, { useEffect, useState, useContext } from "react";
import imgIcon from '../../assets/myTicketsIconCalendar.png';
import loadable from "@loadable/component";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import getFormattedDate from "../../helpers/getFormattedDate";
import {
  filterAllOrdersBySeller,
  updateOrder,
  getOrderById,
  getAllMarketPlaceOrders
} from "../../services/ordersService";
import UserContext from "../../contexts/user.jsx";
import { useTranslation } from "react-i18next";
import { onGridSizeChanged } from "./DataGridHelpers";
import Button from 'antd/es/button';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import { OrderInfo } from "../orders/orderInfo";
import { createShippingLabel } from "../../services/shippingService";
import { getUserById, getBuyerById } from "../../services/userService";
import Loader from "../Loader/Loader";
import "../../assets/ui-stylesheet.css";

import styled from 'styled-components'
import { Calendar } from '@styled-icons/ionicons-outline/Calendar'
// date picker
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import { delivery_status, delivery_status_name } from "../../constants/constants"

const OrderFilter = loadable(() =>
  import("../../components/orders/OrderFilter")
);
const SearchIcon = loadable(() => import("@material-ui/icons/Search"));

const delivered = ({ value }) => {
  const { t } = useTranslation();
  // return value ? <span className="order-delivery-status">Delivered<br />12/08/2021</span> : <span className="order-delivery-status">{t("form.no")}</span>;
  if (value.status == delivery_status.completed) {
    return <span className="order-delivery-status">{delivery_status_name.delivered}<br />{value.date}</span>
  }
  else if (value.status == delivery_status.pending) {
    return <span className="order-delivery-status">{delivery_status_name.pending}</span>
  }
  else if (value.status == delivery_status.cancelled) {
    return <span className="order-delivery-status">{delivery_status_name.cancelled}</span>
  }
  else if (value.status == delivery_status.incomplete) {
    return <span className="order-delivery-status">{delivery_status_name.incomplete}</span>
  }
  else {
    return <span className="order-delivery-status">-</span>
  }
};

const channel = ({ value }) => {
  const { t } = useTranslation();
  return <span>{t("userInfo.channelMarkups.phunnel")}</span>;
};

const phoneFormat = (phoneNumber) => {
  return '(' + phoneNumber.value.substr(0, 3) + ') ' + phoneNumber.value.substr(3, 3) + '-' + phoneNumber.value.substr(6);

}
const CalendarIconInput = styled(Calendar)`
  color: #3a3a3b;
  width:30px;
  height:30px;
`

const GenerateShippingLabel = async order => {
  if (!order.shippingLabelUrl) {
    // get buyer
    let buyer = await getBuyerById(order.buyerId);
    delete buyer.id;
    delete buyer.notes;
    delete buyer.dob;
    delete buyer.email;
    delete buyer.phone;
    delete buyer.cognitoId;
    if (!buyer.streetAddress2) {
      buyer.streetAddress2 = "";
    }
    // seller is the current user
    let seller = await getUserById(order.sellerId);
    delete seller.fullName;
    delete seller.userRole;
    delete seller.email;
    delete seller.stripeId;
    delete seller.about;
    delete seller.id;
    if (!seller.streetAddress2) {
      seller.streetAddress2 = "";
    }
    // how do we tell which type of shipping the user wants?
    const deliveryType = "ups_ground";
    // create label out of the two
    const shippingLabel = await createShippingLabel(
      JSON.stringify(seller).replace(/"([^"]+)":/g, "$1:"),
      JSON.stringify(buyer).replace(/"([^"]+)":/g, "$1:"),
      deliveryType
    );
    // put the shipping label ID on the order after it's created
    order.shippingLabelId = shippingLabel.label_id;
    order.shippingLabelUrl = shippingLabel.label_download.pdf;
    // get rid of all the graphQL "magic" fields
    delete order.buyerByBuyerId;
    delete order.sellerBySellerId;
    delete order.listingByListingId;
    delete order.buyerByBuyerId;
    delete order.userBySellerId;
    // now update our order with the shipping label URL and ID
    await updateOrder(order);
  } else {
    // meaningfully nothing, the shipping label is already created
    //console.log("Already have a shipping label, so showing that");
  }
  // now that we have the shipping label, show it in a new window:
  window.open(order.shippingLabelUrl, "_blank");
};

const formattedDate = ({ value }) => {
  return getFormattedDate(value, "/");
};

const MarketplaceOrdersGrid = () => {
  const { t } = useTranslation();
  let [rowData, setRowData] = useState([]);
  const [allOrders, setAllOrders] = useState(1)
  const [openOrders, setOpenOrders] = useState(0)
  const [overOrders, setOverOrders] = useState(0)
  const [orderFilterID, setOrderIds] = useState();

  // checkbox filter
  // eslint-disable-next-line
  const [undeliveredValue, setUndeliveredValue] = useState(true);
  const [deliveredValue, setDeliveredValue] = useState(true);
  const [orderInfoVisible, setOrderInfoVisible] = useState(false);
  const [selectedOrder, setSelectedOrder] = useState(false);
  // for enforcing search box filter
  const [gridApi, setGridApi] = useState({});

  const [dateRange, setDateRange] = useState([null, null]);
  const [startDate, endDate] = dateRange;
  const [loading, setLoading] = useState(true);
  const [displayText, setDisplayText] = useState('');

  const {
    user: { id }
  } = useContext(UserContext);

  const columnDefs = [
    {
      headerName: t("orders.date"),
      field: "orderdate",
      headerClass: 'ag-theme-custom-text-center',
      cellStyle: { justifyContent: 'center' },
      minWidth: 100
    },
    {
      headerName: t("orders.id"),
      field: "marketplaceorderid",
      headerClass: 'ag-theme-custom-text-center',
      cellStyle: { justifyContent: 'center' },
      minWidth: 100
    },
    {
      headerName: t("listing.id"),
      field: "listing_group_id",
      headerClass: 'ag-theme-custom-text-center',
      cellStyle: { justifyContent: 'center' },
      minWidth: 130
    },
    {
      headerName: t("seller.company"),
      field: "sellercompany",
      minWidth: 130
    },
    {
      headerName: t("seller.name"),
      field: "sellerusername",
      minWidth: 130
    },
    {
      headerName: t("seller.email"),
      field: "selleremail",
      minWidth: 180
    },
    {
      headerName: t("seller.phone"),
      field: "sellerphonenumber",
      cellRenderer: "phoneFormat",
      headerClass: 'ag-theme-custom-text-center',
      cellStyle: { justifyContent: 'center' },
      minWidth: 120
    },
    {
      headerName: t("orders.shippingStatus"),
      field: "deliverystatusname",
      cellRenderer: "delivered",
      headerClass: 'ag-theme-custom-text-center',
      cellStyle: { justifyContent: 'center' },
      minWidth: 180
    }
  ];

  const frameworkComponents = {
    delivered,
    formattedDate,
    channel,
    phoneFormat
  };

  function deliveryStatus(params) {
    let value = params.data.delivery_status_id;
    let fieldValue = '';
    fieldValue = { status: value, date: params.data.deliverydate }
    return fieldValue;
  }
  useEffect(() => {
    fetchFilteredData();
  }, [startDate, endDate, orderFilterID, allOrders, openOrders, overOrders]);

  const fetchFilteredData = async () => {
    let data;
    let dateFrom = '';
    let dateTo = '';
    let orderStatus = [];//[allOrders, openOrders, overOrders];
    if (openOrders || overOrders) {
      //orderStatus.push(1);
      if (openOrders) {//pending orders - not completed
        orderStatus.push(delivery_status.pending, delivery_status.incomplete);
      }
      if (overOrders) {//completed orders
        orderStatus.push(delivery_status.completed);
      }
    }
    else if (allOrders) {
      orderStatus.push(delivery_status.all);
    }

    if (startDate && endDate) {
      dateFrom = new Date(startDate);
      dateFrom = dateFrom.getFullYear() + '/' + (dateFrom.getMonth() + 1) + '/' + dateFrom.getDate();
      dateTo = new Date(endDate);
      dateTo = dateTo.getFullYear() + '/' + (dateTo.getMonth() + 1) + '/' + dateTo.getDate();
    }

    data = await getAllMarketPlaceOrders(orderFilterID, dateFrom, dateTo, orderStatus);
    if (data)
      setRowData(data.data);
    setLoading(false);
    return;
  };



  // if(openOrders){
  //    rowData = rowData && rowData.filter(rowDataItem => {
  //     return (
  //       rowDataItem.delivery_status_id === 3
  //     );
  //   });
  // }
  // if(overOrders){
  //     rowData = rowData && rowData.filter(rowDataItem => {

  //     return (
  //       rowDataItem.delivery_status_id === 1
  //     );
  //   });
  // }


  /**
   * Function which adds the new context menu shortcut,
   * originally developed for the card info modal.
   * NOTE: Must include all default right-click menu items
   * you want in this function.
   * @param {*} params
   */
  function getContextMenuItems(params) {
    var result = [
      {
        // custom item
        name: t("orders.details"),
        action: function () {
          setSelectedOrder(params.node.data);
          setOrderInfoVisible(true);
        },
        icon: {}
      },
      {
        // custom item
        name: t("orders.printShippingLabel"),
        action: async function () {
          //setSelectedOrder(params.node.data);
          const theOrder = await getOrderById(params.node.data.id);
          await GenerateShippingLabel(theOrder);
          setSelectedOrder(theOrder);
        },
        icon: {}
      },
      "export"
    ];
    return result;
  }
  function closeOrderInfoModal() {
    setOrderInfoVisible(false);
    // we have new grid data, so reload the page
    //document.location.reload();
  }
  // free text search bar
  const onSearchInputChange = async (searchString) => {
    let searchText = '';
    if (searchString) {
      searchText = searchString.target.value.toString()
    }
    setDisplayText(searchText)
    await setOrderIds(searchText);
  };
  const onGridReady = params => {
    // for search filtering
    setGridApi(params.api);
  };
  function orderStatusOpen(e) {
    setAllOrders(0);
    setOverOrders(0);
    let value = 0;
    if (e.target.checked == true) {
      value = 1;
    } else {
      setAllOrders(1);
    }
    setOpenOrders(value);
  }
  function orderStatusOver(e) {//completed orders
    setOpenOrders(0);
    setAllOrders(0);
    let value = 0;
    if (e.target.checked == true) {
      value = 1;
    } else {
      setAllOrders(1);
    }
    setOverOrders(value);
  }
  const getRowClass = {
    'order-grid-pending': function (params) { return (params.data.delivery_status_id == 3 || params.data.delivery_status_id == 4); }
  }

  function orderStatusAll(e) {
    setOpenOrders(0);
    setOverOrders(0);
    let value = 0;
    if (e.target.checked == true) {
      value = 1;
    }
    setAllOrders(value);
  }
  function viewOrderDetails(params) {
    window.open("/marketplace-orders/" + params.data.marketplaceorderid);
  }
  /**
  * To avoid user change the readonly/disable attributes forcefully
  */
  function avoidOnchange(e) {
    e.preventDefault();
    return false;
  }
  return !loading ? (
    <div
      style={{
        height: "calc(100vh - 18rem)",
        width: "100%",
        backgroundColor: "inherit"
      }}
      id="grid-wrapper"
      className="ag-theme-balham ag-grid-header"
    >
      <div style={{ display: "grid", gridAutoColumns: "100%", marginBottom: '20px' }}>
        <div className="search-top-input" style={{ position: 'relative' }}>
          <Input className="search-input stylefont-weight-medium"
            style={{
              width: "100%"
            }}
            // options={[{ value: "text 1" }, { value: "text 2" }]}
            onChange={text => onSearchInputChange(text)}
            addonBefore={t("orders.search")}
            placeholder={t("Enter Order ID, Channel, Listing ID, External Reference ID")}
            suffix={<SearchIcon />}
            value={displayText}
            bordered={false}
          />
          {displayText &&
            <button className="search-close-icon" onClick={(e) => onSearchInputChange()} >X</button>}
        </div>

        <div className="filter-item-sec">
          <div className="row m-0">
            <div className="date-range-picker d-flex">
              <div className="order-left">
                <span className="ant-input-group-addon">Orders Date</span>
              </div>
              <div className="date-picker--right">
                <span className="icon-calendar">
                  <img
                    src={imgIcon}
                  />
                </span>
                <DatePicker
                  selectsRange={true}
                  startDate={startDate}
                  endDate={endDate}
                  onKeyDown={avoidOnchange}
                  onChange={(update) => {
                    setDateRange(update);
                  }}
                  dateFormat="dd/MM/yyyy"
                  isClearable={true}
                />
              </div>

            </div>
            <div className="order--check-box">
              <div className="all--order-item">
                <div className="form-check">
                  <input type="checkbox" className="form-check-input" checked onChange={orderStatusAll} checked={allOrders} />
                  <label className="form-check-label">All Orders</label>
                </div>
              </div>
            </div>
            <div className="order--check-box">
              <div className="open--order-item">
                <div className="form-check">
                  <input type="checkbox" className="form-check-input" onChange={orderStatusOpen} checked={openOrders} />
                  <label className="form-check-label">Open Orders</label>
                </div>
              </div>
            </div>
            <div className="order--check-box">
              <div className="complete--order-item">
                <div className="form-check">
                  <input type="checkbox" className="form-check-input" onChange={orderStatusOver} checked={overOrders} />
                  <label className="form-check-label">Complete Orders</label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <AgGridReact
        // properties
        columnDefs={columnDefs}
        rowData={rowData}
        // defaultColDef={{ filter: true }}
        defaultColDef={{ resizable: false }}
        // quickFilterText={search}
        frameworkComponents={frameworkComponents}
        // events
        onFirstDataRendered={onGridSizeChanged}
        onGridReady={onGridReady}
        onRowClicked={viewOrderDetails}
        getContextMenuItems={getContextMenuItems}
        // onGridSizeChanged={onGridSizeChanged}
        rowHeight="60"
        headerHeight="65"
        rowClassRules={getRowClass}
        sortable={true}
      ></AgGridReact>
      <Modal
        title={t("orders.details")}
        visible={orderInfoVisible}
        onCancel={closeOrderInfoModal}
        footer={
          <Button key="submit" type="primary" onClick={closeOrderInfoModal}>
            Ok
          </Button>
        }
        width="60%"
        closable={false}
        maskClosable={false}
      >
        <OrderInfo
          order={selectedOrder}
          handleCancel={closeOrderInfoModal}
          printShippingLabel={GenerateShippingLabel}
        />
      </Modal>
    </div>
  ) : (
    <Loader />
  );
};

export default MarketplaceOrdersGrid;
