// import React, { useEffect, useState } from 'react'
// import { AgGridReact } from "ag-grid-react";
// import "ag-grid-enterprise";
// import "ag-grid-community/dist/styles/ag-grid.css";
// import "ag-grid-community/dist/styles/ag-theme-balham.css";

// import {
//   getEventsReportData,
//   calcPotentialRev,
//   sortIntoBroker,
//   getEventsByPerformer
// } from "../../services/eventService";

// import { TopBrokersChart } from "../charts/topBrokers";

// // class ListingInsightsGrid extends Component {
// //   constructor(props) {
// //     super(props);
// //     this.state = {
// //       columnDefs: [
// //         {
// //           headerName: "Event Name",
// //           field: "posName",
// //           filter: "agTextColumnFilter"
// //         },
// //         {
// //           headerName: "Event Date",
// //           field: "date",
// //           filter: "agDateColumnFilter"
// //         },
// //         {
// //           headerName: "Event ID",
// //           field: "id",
// //           filter: "agTextColumnFilter"
// //         },
// //         {
// //           headerName: "Performer",
// //           field: "performerByPerformer.name",
// //           filter: "agTextColumnFilter"
// //         },
// //         {
// //           headerName: "Opponent",
// //           field: "performerByOpponent.name"
// //         },
// //         {
// //           headerName: "Category",
// //           field: "categoryByCategory.name",
// //           filter: "agTextColumnFilter"
// //         },
// //         {
// //           headerName: "Total Listings",
// //           field: "listingsByEventId.totalCount",
// //           cellRenderer: "agGroupCellRenderer"
// //         }
// //       ],
// //       detailCellRendererParams: {
// //         detailGridOptions: {
// //           autoGroupColumnDef: {
// //             headerName: " Broker ",
// //             cellRendererParams: {
// //               suppressCount: true
// //             }
// //           },
// //           columnDefs: [
// //             {
// //               headerName: "Broker",
// //               field: "userBySellerId.fullName",
// //               rowGroup: true,
// //               filter: "agTextColumnFilter"
// //             },
// //             { headerName: "Section", field: "section", filter: true },
// //             { headerName: "Row", field: "row", filter: true },
// //             {
// //               headerName: "Cost",
// //               field: "cost",
// //               filter: true,
// //               valueFormatter: this.currencyFormatter,
// //               aggFunc: "sum"
// //             },
// //             {
// //               headerName: "Price",
// //               field: "price",
// //               filter: true,
// //               valueFormatter: this.currencyFormatter,
// //               aggFunc: "sum"
// //             },
// //             {
// //               headerName: "Potential ROI",
// //               field: "potentialRev",
// //               filter: true,
// //               aggFunc: this.expandToSee
// //             }
// //           ],
// //           onFirstDataRendered(params) {
// //             params.api.sizeColumnsToFit();
// //           }
// //         },
// //         getDetailRowData: function(params) {
// //           params.successCallback(params.data.listingsByEventId.nodes);
// //         }
// //       },
// //       autoGroupColumnDef: {
// //         // headerName: "Model",
// //         // field: "model",
// //         // cellRenderer:'agGroupCellRenderer',
// //         // cellRendererParams: {
// //         //   checkbox: true
// //         // }
// //       },
// //       defaultColDef: {
// //         sortable: true,
// //         resizable: false
// //       },
// //       sideBar: "columns",
// //       rowData: []
// //     };
// //   }

// //   currencyFormatter(params) {
// //     return "$" + params.value;
// //   }

// //   expandToSee() {
// //     return "Expand for ROI details";
// //   }

// //   onGridReady = params => {
// //     this.gridApi = params.api;
// //     this.gridColumnApi = params.columnApi;
// //     setTimeout(function() {
// //       var rowCount = 0;
// //       params.api.forEachNode(function(node) {
// //         node.setExpanded(rowCount++ === 1);
// //       });
// //     }, 500);
// //   };

// //   componentDidMount() {
// //     //    fetch('https://api.myjson.com/bins/15psn9')
// //     //  .then(result => result.json())
// //     //  .then(rowData => this.setState({rowData}))]
// //     getEventsByPerformer().then(events => {
// //       this.setState({ topBrokers: sortIntoBroker(events) });
// //       getEventsReportData().then(result => {
// //         result.forEach(function(event) {
// //           event.listingsByEventId.nodes.forEach(function(listing) {
// //             listing.potentialRev = calcPotentialRev(listing);
// //           });
// //         });
// //         let topEvents = this.getTopEvents(result);
// //         let topGrid = [];
// //         topEvents.forEach(event => {
// //           topGrid.push({ x: event.posName });
// //         });
// //         this.setState({ rowData: result, topListings: topGrid });
// //       });
// //     });

// //     //  .then(rowData => this.setState({rowData}))
// //   }

// //   compare(a, b) {
// //     if (a.listingsByEventId.totalCount > b.listingsByEventId.totalCount) {
// //       return -1;
// //     }
// //     if (a.listingsByEventId.totalCount < b.listingsByEventId.totalCount) {
// //       return 1;
// //     }
// //     return 0;
// //   }

// //   getTopEvents(rowData) {
// //     let fullArray = rowData.sort(this.compare);
// //     return fullArray.slice(0, 5);
// //   }

// //   onButtonClick = e => {
// //     const selectedNodes = this.gridApi.getSelectedNodes();
// //     const selectedData = selectedNodes.map(node => node.data);
// //     const selectedDataStringPresentation = selectedData
// //       .map(node => node.make + " " + node.model)
// //       .join(", ");
// //     alert(`Selected nodes: ${selectedDataStringPresentation}`);
// //   };

// //   render() {
// //     return (
// //       <div style={{ width: "100%", height: "80vh" }}>
// //         <div style={{ display: "flex", width: "100%" }}>
// //           <div>
// //             {" "}
// //             Top Events:
// //             {this.state.topListings &&
// //               this.state.topListings.map(listing =>
// //                 listing.x ? (
// //                   <div key={listing.y}>{listing.x}</div>
// //                 ) : (
// //                   <div>0</div>
// //                 )
// //               )}
// //           </div>

// //           <div style={{width: "500px", height: "300px" }}>
// //               <div className="text-center">Top Brokers</div>
// //                {this.state.topBrokers &&<TopBrokersChart data={this.state.topBrokers}/> }
// //           </div>
// //         </div>
// //         <div style={{ width: "100%", height: "80%" }}>
// //           <div
// //             style={{
// //               height: "100%",
// //               paddingTop: "35px",
// //               boxSizing: "border-box"
// //             }}
// //           >
// //             <div
// //               id="myGrid"
// //               style={{
// //                 height: "100%",
// //                 width: "100%"
// //               }}
// //               className="ag-theme-balham"
// //             >
// //               <AgGridReact
// //                 columnDefs={this.state.columnDefs}
// //                 defaultColDef={this.state.defaultColDef}
// //                 sideBar={this.state.sideBar}
// //                 onGridReady={this.onGridReady}
// //                 rowData={this.state.rowData}
// //                 masterDetail={true}
// //                 detailCellRendererParams={this.state.detailCellRendererParams}
// //               />
// //             </div>
// //           </div>
// //         </div>
// //       </div>
// //     );
// //   }
// // }

// const ListingInsightsGrid = () => {
//   const [columnDefs, setColumnDefs] = useState(0);
//   const [detailCellRendererParams, setDetailCellRendererParams] = useState(1);
//   const [defaultColDef, setDefaultColDef] = useState(2);
//   const [sideBar, setSideBar] = useState(3);
//   const [rowData, setRowData] = useState(4);
//   const [gridApi, setGridApi] = useState(5);
//   const [gridColumnApi, setGridColumnApi] = useState(6);

//   setColumnDefs([
//     {
//       headerName: "Event Name",
//       field: "posName",
//       filter: "agTextColumnFilter"
//     },
//     {
//       headerName: "Event Date",
//       field: "date",
//       filter: "agDateColumnFilter"
//     },
//     {
//       headerName: "Event ID",
//       field: "id",
//       filter: "agTextColumnFilter"
//     },
//     {
//       headerName: "Performer",
//       field: "performerByPerformer.name",
//       filter: "agTextColumnFilter"
//     },
//     {
//       headerName: "Opponent",
//       field: "performerByOpponent.name"
//     },
//     {
//       headerName: "Category",
//       field: "categoryByCategory.name",
//       filter: "agTextColumnFilter"
//     },
//     {
//       headerName: "Total Listings",
//       field: "listingsByEventId.totalCount",
//       cellRenderer: "agGroupCellRenderer"
//     }
//   ])
//   setDetailCellRendererParams({
//     detailGridOptions: {
//       autoGroupColumnDef: {
//         headerName: " Broker ",
//         cellRendererParams: {
//           suppressCount: true
//         }
//       },
//       columnDefs: [
//         {
//           headerName: "Broker",
//           field: "userBySellerId.fullName",
//           rowGroup: true,
//           filter: "agTextColumnFilter"
//         },
//         { headerName: "Section", field: "section", filter: true },
//         { headerName: "Row", field: "row", filter: true },
//         {
//           headerName: "Cost",
//           field: "cost",
//           filter: true,
//           valueFormatter: currencyFormatter,
//           aggFunc: "sum"
//         },
//         {
//           headerName: "Price",
//           field: "price",
//           filter: true,
//           valueFormatter: currencyFormatter,
//           aggFunc: "sum"
//         },
//         {
//           headerName: "Potential ROI",
//           field: "potentialRev",
//           filter: true,
//           aggFunc: expandToSee
//         }
//       ],
//       onFirstDataRendered(params) {
//         params.api.sizeColumnsToFit();
//       }
//     },
//     getDetailRowData: function(params) {
//       params.successCallback(params.data.listingsByEventId.nodes);
//     }
//   })
//   setDefaultColDef({
//     sortable: true,
//     resizable: false
//   })
//   setSideBar("columns")
//   setRowData([])
// const  currencyFormatter = function (params) {
//     return "$" + params.value;
//   }

//  const expandToSee = function() {
//     return "Expand for ROI details";
//   }

//     const onGridReady = params => {
//     setGridApi(params.api);
//     setGridColumnApi(params.columnApi);
//     setTimeout(function() {
//       var rowCount = 0;
//       params.api.forEachNode(function(node) {
//         node.setExpanded(rowCount++ === 1);
//       });
//     }, 500);
//   }
//   useEffect(() => {
//     getEventsReportData().then(result => {
//       result.forEach(function(event) {
//         event.listingsByEventId.nodes.forEach(function(listing) {
//           listing.potentialRev = calcPotentialRev(listing);
//         });
//       });
//       setRowData(result)
//     });
//   }, [])
// return (
//   <div style={{ width: "100%", height: "80vh" }}>

//         <div style={{ width: "100%", height: "80%" }}>
//           <div
//             style={{
//               height: "100%",
//               paddingTop: "35px",
//               boxSizing: "border-box"
//             }}
//           >
//             <div
//               id="myGrid"
//               style={{
//                 height: "100%",
//                 width: "100%"
//               }}
//               className="ag-theme-balham"
//             >
//               <AgGridReact
//                 columnDefs={columnDefs}
//                 defaultColDef={defaultColDef}
//                 sideBar={sideBar}
//                 onGridReady={onGridReady}
//                 rowData={rowData}
//                 masterDetail={true}
//                 detailCellRendererParams={detailCellRendererParams}
//               />
//             </div>
//           </div>
//         </div>
//       </div>
// )
// }
// export default ListingInsightsGrid;

import React, { useEffect, useState } from "react";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";

import {
  getEventsReportData
  // calcPotentialRev,
  // sortIntoBroker,
  // getEventsByPerformer
} from "../../services/eventService";
import { autoSizeAllColumns } from "./DataGridHelpers";

function ListingInsightsGrid() {
  // const {columnDefs, rowData} = store;
  const [rowData, setRowData] = useState([]);

  const columnDefs = [
    {
      headerName: "Event Name",
      field: "posName",
      filter: "agTextColumnFilter"
    },
    {
      headerName: "Event Date",
      field: "date",
      filter: "agDateColumnFilter"
    },
    {
      headerName: "Event ID",
      field: "id",
      filter: "agTextColumnFilter"
    },
    {
      headerName: "Performer",
      field: "performerByPerformer.name",
      filter: "agTextColumnFilter"
    },
    {
      headerName: "Opponent",
      field: "performerByOpponent.name"
    },
    {
      headerName: "Category",
      field: "categoryByCategory.name",
      filter: "agTextColumnFilter"
    },
    {
      headerName: "Total Listings",
      field: "listingsByEventId.totalCount",
      cellRenderer: "agGroupCellRenderer"
    }
  ];

  useEffect(() => {
    // getEventsReportData().then(result => {
    //   result.forEach(function(event) {
    //     event.listingsByEventId.nodes.forEach(function(listing) {
    //       listing.potentialRev = calcPotentialRev(listing);
    //     });
    //   });
    //   setRowData(result)
    // });
    async function fetchData() {
      // You can await here
      let events = await getEventsReportData();
      setRowData(events);
      return;
      // ...
    }
    fetchData();
  }, []);

  // row data will be provided via redux on this.props.rowData
  return (
    <div
      style={{ height: 400, width: 900, marginTop: 15 }}
      className="ag-theme-balham"
    >
      <AgGridReact
        // properties
        columnDefs={columnDefs}
        rowData={rowData}
        defaultColDef={{ filter: true, resizable: false }}
        // events
        onFirstDataRendered={autoSizeAllColumns}
      ></AgGridReact>
    </div>
  );
}

export default ListingInsightsGrid;
