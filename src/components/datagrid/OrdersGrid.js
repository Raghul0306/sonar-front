import React, { useEffect, useState, useContext, useRef } from "react";
import loadable from "@loadable/component";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import getFormattedDate from "../../helpers/getFormattedDate";
import {
  filterAllOrders,
  updateOrder,
  getOrderById
} from "../../services/ordersService";
import { createShippingLabel } from "../../services/shippingService";
import {
  getUserById,
  sendEmailToUser
} from "../../services/userService";
import {
  updateListingSold,
  updateListingInactive,
  getListingById
} from "../../services/listingsService";
import UserContext from "../../contexts/user.jsx";
import { useTranslation } from "react-i18next";
import { autoSizeAllColumns,onGridSizeChanged } from "./DataGridHelpers";
import Button from 'antd/es/button';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import { OrderInfo } from "../orders/orderInfo";
import Loader from "../Loader/Loader";
import moment from "moment";
import { userRoles, userRolesLower } from '../../constants/constants';
const OrderFilter = loadable(() =>
  import("../../components/orders/OrderFilter")
);
const SearchIcon = loadable(() => import("@material-ui/icons/Search"));

const delivered = ({ value }) => {
  const { t } = useTranslation();
  return value ? <span>{t("form.yes")}</span> : <span>{t("form.no")}</span>;
};

const channel = ({ value }) => {
  const { t } = useTranslation();
  return <span>{t("userInfo.channelMarkups.phunnel")}</span>;
};

const formattedDate = ({ value }) => {
  return getFormattedDate(value, "/");
};

const OrdersGrid = () => {
  const { t } = useTranslation();
  const [rowData, setRowData] = useState([]);

  // date filter
  const today = new Date();
  const oneWeekAgo = new Date();
  oneWeekAgo.setDate(today.getDate() - 180);
  const [startdateValue, setStartdateValue] = useState(oneWeekAgo);
  const [enddateValue, setEnddateValue] = useState(today);
  // for enforcing search box filter
  const [gridApi, setGridApi] = useState({});
  // checkbox filter
  const [deliveredValue, setDeliveredValue] = useState(true);
  const [orderInfoVisible, setOrderInfoVisible] = useState(false);
  const [orderRefundVisible, setOrderRefundVisible] = useState(false);
  const [selectedOrder, setSelectedOrder] = useState(false);
  const [sellerSearchValue, setSellerSearchValue] = useState(null);
  const sellerSearchInputRef = useRef();

  const [loading, setLoading] = useState(false);
  const { user } = useContext(UserContext);
  const columnDefs = [
    // {
    //   headerName: t("orders.channel"),
    //   cellRenderer: "channel"
    // },
    {
      headerName: t("orders.id"),
      field: "id",
      filter: "agNumberColumnFilter"
    },
    {
      headerName: t("orders.date"),
      field: "createdAt",
      filter: "agNumberColumnFilter",
      valueGetter: getCreatedAt
    },
    {
      headerName: t("orders.channel"),
      field: "channel",
      valueGetter: getChannel
      // filter: "agTextColumnFilter"
    },
    {
      headerName: t("listing.id"),
      field: "listingByListingId.id",
      filter: "agNumberColumnFilter"
    },
    {
      headerName: t("card.name"),
      field: "cardByCardId.name",
      filter: "agTextColumnFilter",
      chartDataType: "category",
      valueGetter: getCardNameValue,
      sortable: true
    },
    {
      headerName: t("listing.quantity"),
      field: "listingByListingId.quantity",
      filter: "agNumberColumnFilter"
    },
    {
      headerName: t("listing.cost"),
      field: "listingByListingId.price",
      filter: "agNumberColumnFilter"
    },
    {
      headerName: t("listing.retail"),
      field: "listingByListingId.price",
      filter: "agNumberColumnFilter"
    },
    {
      headerName: t("seller.name"),
      field: "userBySellerId.fullName",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("seller.email"),
      field: "userBySellerId.email",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("seller.phone"),
      field: "userBySellerId.phone",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("buyer.name"),
      field: "userByBuyerId.fullName",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("buyer.email"),
      field: "userByBuyerId.email",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("buyer.phone"),
      field: "userByBuyerId.phone",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("orders.shippingLabelId"),
      field: "shippingLabelId",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("orders.delivered"),
      field: "listingByListingId.delivered",
      cellRenderer: "delivered"
    },
    {
      headerName: t("orders.cancelled"),
      field: "cancelled"
    }
  ];

  const frameworkComponents = {
    delivered,
    formattedDate,
    channel
  };

  function getCreatedAt(params) {
    return moment(params.data.createdAt).format("YYYY-MM-DD");
  }

  function getChannel(params) {
    return params.data && params.data.ebayOrderId ? "Ebay" : "Phunnel";
  }

  function getCardNameValue(params) {
    return [params.data.name]
      .filter(function(val) {
        return val;
      })
      .join(" ");
  }

  // checkboxes filter
  const rowDataFiltered = rowData.filter(rowDataItem => {
    if (deliveredValue) {
      return true;
    }
    if (!!rowDataItem.listingByListingId.delivered) {
      return true;
    }
    if (!!rowDataItem.listingByListingId.delivered) {
      return true;
    }
    return false;
  });

  const GenerateShippingLabel = async order => {
    if (!order.shippingLabelUrl) {
      // get buyer
      let buyer = await getUserById(order.buyerId);
      delete buyer.fullName;
      delete buyer.userRole;
      delete buyer.email;
      delete buyer.stripeId;
      delete buyer.about;
      delete buyer.id;
      if (!buyer.streetAddress2) {
        buyer.streetAddress2 = "";
      }
      // seller is the current user
      let seller = await getUserById(order.sellerId);
      delete seller.fullName;
      delete seller.userRole;
      delete seller.email;
      delete seller.stripeId;
      delete seller.about;
      delete seller.id;
      if (!seller.streetAddress2) {
        seller.streetAddress2 = "";
      }
      // how do we tell which type of shipping the user wants?
      const deliveryType = "ups_ground";
      // create label out of the two
      const shippingLabel = await createShippingLabel(
        JSON.stringify(seller).replace(/"([^"]+)":/g, "$1:"),
        JSON.stringify(buyer).replace(/"([^"]+)":/g, "$1:"),
        deliveryType
      );
      // put the shipping label ID on the order after it's created
      order.shippingLabelId = shippingLabel.label_id;
      order.shippingLabelUrl = shippingLabel.label_download.pdf;
      // get rid of all the graphQL "magic" fields
      delete order.buyerByBuyerId;
      delete order.sellerBySellerId;
      delete order.listingByListingId;
      delete order.buyerByBuyerId;
      delete order.userBySellerId;
      // now update our order with the shipping label URL and ID
      await updateOrder(order);
    } else {
      // meaningfully nothing, the shipping label is already created
      //console.log("Already have a shipping label, so showing that");
    }
    // now that we have the shipping label, show it in a new window:
    window.open(order.shippingLabelUrl, "_blank");
  };

  // data fetch, date filter
  useEffect(() => {
    setLoading(true);
    const fetchFilteredData = async () => {
      let data = await filterAllOrders(
        getFormattedDate(startdateValue),
        getFormattedDate(enddateValue)
      );
      let listingByOrdersCalls = [];
      data && data.length && data.map(order =>
        listingByOrdersCalls.push(getListingById(order.listingId))
      );
      Promise.all(listingByOrdersCalls)
        .then(listings => {
          let ordersWithListings = data.map((order, index) => {
            const { cardByCardId, name } = listings[index];
            return { ...order, cardByCardId, name };
          });
          setRowData(ordersWithListings);
          setLoading(false);
        })
        .catch(err => console.log(err));
    };

    fetchFilteredData();
  }, [startdateValue, enddateValue]);
  /**
   * Function which adds the new context menu shortcut,
   * originally developed for the card info modal.
   * NOTE: Must include all default right-click menu items
   * you want in this function.
   * @param {*} params
   */
  function getContextMenuItems(params) {
    var result = [
      {
        // custom item
        name: t("orders.details"),
        action: async function() {
          setSelectedOrder(await getOrderById(params.node.data.id));
          setOrderInfoVisible(true);
        },
        icon: {}
      },
      {
        // custom item
        name: t("orders.printShippingLabel"),
        action: async function() {
          //setSelectedOrder(params.node.data);
          const theOrder = await getOrderById(params.node.data.id);
          setSelectedOrder(theOrder);
          await GenerateShippingLabel(theOrder);
        },
        icon: {}
      },
      {
        // custom item
        name: t("orders.emailShippingLabel"),
        action: async function() {
          //setSelectedOrder(params.node.data);
          // const theOrder = await getOrderById(params.node.data.id);
          // setSelectedOrder(theOrder)
          // await GenerateShippingLabel(theOrder);
          sendEmailShippingLabel(params.node.data);
        },
        icon: {}
      },
      {
        // custom item
        name: t("orders.cancel"),
        action: async function() {
          //setSelectedOrder(params.node.data);
          setSelectedOrder(await getOrderById(params.node.data.id));
          setOrderRefundVisible(true);
        },
        icon: {}
      },
      "export"
    ];
    return result;
  }

  const sendEmailShippingLabel = orderData => {
    const emailHtml =
      "<div><h1>Shipping label Url: </h1><a href='" +
      orderData.shippingLabelUrl +
      "'>" +
      orderData.shippingLabelUrl +
      "<a/></div>";
    sendEmailToUser(
      orderData.userBySellerId.email,
      emailHtml,
      "Shipping label url"
    );
    alert(t("orders.shippinglabelSent"));
  };
  function closeOrderInfoModal() {
    setOrderInfoVisible(false);
    // we have new grid data, so reload the page
    //document.location.reload();
  }
  function closeOrderRefundModal() {
    setOrderRefundVisible(false);
    // we have new grid data, so reload the page
    //document.location.reload();
  }
  // free text search bar
  const onSearchInputChange = searchString => {
    // look up cards that match the string and type
    gridApi.setQuickFilter(searchString.target.value.toString());
    // still need to factor in type somehow with quick filter
  };

  const onSellerSearchInputChange = searchString => {
    // look up cards that match the string and type
    gridApi.setQuickFilter(searchString.target.value.toString());
    // still need to factor in type somehow with quick filter
  };

  const onGridReady = params => {
    // for search filtering
    setGridApi(params.api);
    if (sellerSearchValue) {
      onSellerSearchInputChange(sellerSearchValue);
      sellerSearchInputRef.current.props.onChange(sellerSearchValue);
    }
  };

  const refundOrder = async () => {
    const listID = selectedOrder.listingByListingId.id;
    updateListingSold(listID, false);
    updateListingInactive(listID, true);
    // ensure the order is cancelled by patching the order
    updateOrder({
      id: selectedOrder.id,
      cancelled: true
    });
    setOrderRefundVisible(false);
    // alert the user we are done refunding the order
    alert(t("orders.refundSuccess"));
  };

  return (
    <div>
      {user && user.userRole === userRoles.admin && (
        <Input
          ref={sellerSearchInputRef}
          style={{
            width: "80%",
            margin: "8px 0 0"
          }}
          options={[{ value: "text 1" }, { value: "text 2" }]}
          onChange={text => {
            text.persist();
            setSellerSearchValue(text);
            onSellerSearchInputChange(text);
          }}
          addonBefore={t("orders.sellerSearch")}
          placeholder={t("orders.sellerSearchPlaceholder")}
          suffix={<SearchIcon />}
          bordered={false}
        />
      )}
      {loading && <Loader />}
      {!loading && (
        <>
          <Input
            style={{
              width: "80%",
              margin: "8px 0 0"
            }}
            options={[{ value: "text 1" }, { value: "text 2" }]}
            onChange={text => onSearchInputChange(text)}
            addonBefore={t("orders.search")}
            placeholder={t("orders.id")}
            suffix={<SearchIcon />}
            bordered={false}
          />

          <div
            style={{
              height: "calc(100vh - 15.5rem)",
              width: "100%",
              backgroundColor: "inherit"
            }}
            className="ag-theme-balham"
            id="grid-wrapper"
          >
            <OrderFilter
              checkboxes={[
                {
                  value: deliveredValue,
                  setValue: setDeliveredValue,
                  name: t("orders.completed")
                }
              ]}
              rangeDatePicker={{
                startdateValue,
                setStartdateValue,
                enddateValue,
                setEnddateValue
              }}
            />
            <AgGridReact
              // properties
              columnDefs={columnDefs}
              rowData={rowDataFiltered}
              // defaultColDef={{ filter: true }}
              defaultColDef={{ resizable: false }}
              // quickFilterText={search}
              frameworkComponents={frameworkComponents}
              // events
              onFirstDataRendered={onGridSizeChanged}
              onGridReady={onGridReady}
              getContextMenuItems={getContextMenuItems}
              rowHeight="45"
              headerHeight="50"
            ></AgGridReact>
            <Modal
              title={
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  <div style={{ margin: "0 !important" }}>
                    {`${t("orders.id")}: ${selectedOrder.id}`}
                  </div>
                  <Button
                    type="primary"
                    onClick={async () => {
                      await GenerateShippingLabel(selectedOrder);
                    }}
                  >
                    Print Recepit
                  </Button>
                </div>
              }
              visible={orderInfoVisible}
              onCancel={closeOrderInfoModal}
              footer={false}
              width="50%"
              closable={false}
              maskClosable={false}
            >
              <OrderInfo
                order={selectedOrder}
                handleCancel={closeOrderInfoModal}
                printShippingLabel={GenerateShippingLabel}
              />
            </Modal>
            <Modal
              title={t("orders.details")}
              visible={orderRefundVisible}
              onCancel={closeOrderRefundModal}
              footer={false}
              width="500px"
              closable={false}
              style={{ textAlign: "center" }}
              maskClosable={false}
            >
              <h4 style={{ paddingVertical: 10, marginBottom: 10 }}>
                {t("orders.refundMessage")}
              </h4>
              <Button
                type="primary"
                size="large"
                style={{ margin: "20px 20px 0px 0px" }}
                onClick={refundOrder}
              >
                {t("form.yes")}
              </Button>
              <Button size="large" onClick={closeOrderRefundModal}>
                {t("form.no")}
              </Button>
            </Modal>
          </div>
        </>
      )}
    </div>
  );
};

export default OrdersGrid;
