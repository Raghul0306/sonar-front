import React, { useEffect, useState, useContext } from "react";
import imgIcon from '../../assets/myTicketsIconCalendar.png';
// import { Link } from "react-router-dom";
import loadable from "@loadable/component";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import getFormattedDate from "../../helpers/getFormattedDate";

import {
  filterAllOrdersBySeller, brokerOrders,
  updateOrder,
  getOrderById
} from "../../services/ordersService";
import UserContext from "../../contexts/user.jsx";
import { useTranslation } from "react-i18next";
import { autoSizeAllColumns, onGridSizeChanged } from "./DataGridHelpers";
import Button from 'antd/es/button';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import { OrderInfo } from "../orders/orderInfo";
import { createShippingLabel } from "../../services/shippingService";
import { getUserById, getBuyerById } from "../../services/userService";
import Loader from "../Loader/Loader";
import moment from "moment";
import { delivery_status, delivery_status_name } from "../../constants/constants"

import styled from 'styled-components'
import { Calendar } from '@styled-icons/ionicons-outline/Calendar'
// date picker
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

const Example = () => {
  const [startDate, setStartDate] = useState(new Date());
};

const OrderFilter = loadable(() =>
  import("../../components/orders/OrderFilter")
);
const SearchIcon = loadable(() => import("@material-ui/icons/Search"));

const delivered = ({ value }) => {
  const { t } = useTranslation();
  // return value ? <span className="order-delivery-status">Delivered<br />12/08/2021</span> : <span className="order-delivery-status">{t("form.no")}</span>;
  if (value.status == delivery_status.completed) {
    return <span className="order-delivery-status">{delivery_status_name.delivered}<br />{value.date}</span>
  }
  else if (value.status == delivery_status.pending) {
    return <span className="order-delivery-status">{delivery_status_name.pending}</span>
  }
  else if (value.status == delivery_status.cancelled) {
    return <span className="order-delivery-status">{delivery_status_name.cancelled}</span>
  }
  else if (value.status == delivery_status.incomplete) {
    return <span className="order-delivery-status">{delivery_status_name.incomplete}</span>
  }
  else {
    return <span className="order-delivery-status">-</span>
  }
};

const phoneFormat = (phoneNumber) => {
  return '(' + phoneNumber.value.substr(0, 3) + ') ' + phoneNumber.value.substr(3, 3) + '-' + phoneNumber.value.substr(6);

}
const channel = ({ value }) => {
  const { t } = useTranslation();
  return <span>{t("userInfo.channelMarkups.phunnel")}</span>;
};

const CalendarIconInput = styled(Calendar)`
  color: #3a3a3b;
  width:30px;
  height:30px;
`

const GenerateShippingLabel = async order => {
  if (!order.shippingLabelUrl) {
    // get buyer
    let buyer = await getBuyerById(order.buyerId);
    delete buyer.id;
    delete buyer.notes;
    delete buyer.dob;
    delete buyer.email;
    delete buyer.phone;
    delete buyer.cognitoId;
    if (!buyer.streetAddress2) {
      buyer.streetAddress2 = "";
    }
    // seller is the current user
    let seller = await getUserById(order.sellerId);
    delete seller.fullName;
    delete seller.userRole;
    delete seller.email;
    delete seller.stripeId;
    delete seller.about;
    delete seller.id;
    if (!seller.streetAddress2) {
      seller.streetAddress2 = "";
    }
    // how do we tell which type of shipping the user wants?
    const deliveryType = "ups_ground";
    // create label out of the two
    const shippingLabel = await createShippingLabel(
      JSON.stringify(seller).replace(/"([^"]+)":/g, "$1:"),
      JSON.stringify(buyer).replace(/"([^"]+)":/g, "$1:"),
      deliveryType
    );
    // put the shipping label ID on the order after it's created
    order.shippingLabelId = shippingLabel.label_id;
    order.shippingLabelUrl = shippingLabel.label_download.pdf;
    // get rid of all the graphQL "magic" fields
    delete order.buyerByBuyerId;
    delete order.sellerBySellerId;
    delete order.listingByListingId;
    delete order.buyerByBuyerId;
    delete order.userBySellerId;
    // now update our order with the shipping label URL and ID
    await updateOrder(order);
  } else {
    // meaningfully nothing, the shipping label is already created
    //console.log("Already have a shipping label, so showing that");
  }
  // now that we have the shipping label, show it in a new window:
  window.open(order.shippingLabelUrl, "_blank");
};

const formattedDate = ({ value }) => {
  return getFormattedDate(value, "/");
};

const SpecificOrdersGrid = () => {
  const { t } = useTranslation();
  const [rowData, setRowData] = useState([]);
  const [orderFilter, setOrderIds] = useState();
  const [orderFilterState, setSearchDelay] = useState(true);

  // date filter
  const today = new Date();
  const oneWeekAgo = new Date();
  oneWeekAgo.setDate(today.getDate() - 7);
  // const [startdateValue, setStartdateValue] = useState(oneWeekAgo);
  // const [enddateValue, setEnddateValue] = useState(today);
  const [dateRange, setDateRange] = useState([null, null]);
  const [startdateValue, enddateValue] = dateRange;
  // checkbox filter
  // eslint-disable-next-line
  const [undeliveredValue, setUndeliveredValue] = useState(true);
  const [deliveredValue, setDeliveredValue] = useState(true);
  const [orderInfoVisible, setOrderInfoVisible] = useState(false);
  const [selectedOrder, setSelectedOrder] = useState(false);
  // for enforcing search box filter
  const [gridApi, setGridApi] = useState({});

  const [loading, setLoading] = useState(true);

  const [allOrders, setAllOrders] = useState(1)
  const [openOrders, setOpenOrders] = useState(0)
  const [overOrders, setOverOrders] = useState(0)
  const [displayText, setDisplayText] = useState('');
  const {
    user: { id }
  } = useContext(UserContext);

  const columnDefs = [
    {
      headerName: t("orders.channel"),
      // cellRenderer: "channel"
      field: "channel_name",
      minWidth: 100
    },
    {
      headerName: t("orders.date"),
      field: "created_at",
      valueGetter: getCreatedAt,
      headerClass: 'ag-theme-custom-text-center',
      cellStyle: { justifyContent: 'center' },
      minWidth: 130
    },
    {
      headerName: t("orders.id"),
      field: "id",
      headerClass: 'ag-theme-custom-text-center',
      cellStyle: { justifyContent: 'center' },
      minWidth: 130
    },
    {
      headerName: t("purchases.references"),
      field: "external_channel_reference_id",
      headerClass: 'ag-theme-custom-text-center',
      cellStyle: { justifyContent: 'center' },
      minWidth: 130
    },
    {
      headerName: t("listing.id"),
      field: "listing_group_id",
      headerClass: 'ag-theme-custom-text-center',
      cellStyle: { justifyContent: 'center' },
      minWidth: 130
    },
    {
      headerName: t("buyer.name"),
      field: "buyer_name",
      minWidth: 130
    },
    {
      headerName: t("buyer.email"),
      field: "buyer_email",
      minWidth: 180
    },
    {
      headerName: t("buyer.phone"),
      field: "phone_number",
      cellRenderer: "phoneFormat",
      headerClass: 'ag-theme-custom-text-center',
      cellStyle: { justifyContent: 'center' },
      minWidth: 120
    },
    {
      headerName: t("orders.delivery_status"),
      field: "delivery_status_id",
      headerClass: 'ag-theme-custom-text-center',
      cellRenderer: "delivered",
      cellStyle: { justifyContent: 'center' },
      minWidth: 150
    }
  ];

  const frameworkComponents = {
    delivered,
    formattedDate,
    channel,
    phoneFormat
  };


  function getCreatedAt(params) {
    return params.data.created_at
      ? moment(params.data.created_at).format("MM/DD/YYYY")
      : "";
  }
  function deliveryStatus(params) {
    let value = params.data.delivery_status_id;
    let fieldValue = '';
    fieldValue = { status: value, date: params.data.delivery_at }
    return fieldValue;
  }
  // checkboxes filter
  const rowDataFiltered = rowData && rowData.filter(rowDataItem => {
    if (undeliveredValue && deliveredValue) {
      return true;
    }
    if (undeliveredValue && !!!rowDataItem.listingByListingId.delivered) {
      return true;
    }
    if (deliveredValue && !!rowDataItem.listingByListingId.delivered) {
      return true;
    }
    return false;
  });

  // data fetch, date filter
  useEffect(() => {
    fetchFilteredData();
  }, [startdateValue, enddateValue, orderFilter, allOrders, openOrders, overOrders]);

  /**
   * Fetching broker orders lists
   * @returns Row data for datatable
   * TIC-337
   */
  const fetchFilteredData = async () => {
    let data;
    // let dateFrom = startdateValue ? new Date(startdateValue) : '';
    // let dateTo = enddateValue ? new Date(enddateValue) : '';

    let dateFrom = startdateValue ? new Date(startdateValue) : '';
    dateFrom = dateFrom ? dateFrom.getFullYear() + '-' + (dateFrom.getMonth() + 1) + '-' + dateFrom.getDate() : '';
    let dateTo = enddateValue ? new Date(enddateValue) : '';
    dateTo = dateTo ? dateTo.getFullYear() + '-' + (dateTo.getMonth() + 1) + '-' + dateTo.getDate() : '';
    let orderStatus = [];//[allOrders, openOrders, overOrders];
    if (openOrders || overOrders) {
      //orderStatus.push(1);
      if (openOrders) {//pending orders - not completed
        orderStatus.push(delivery_status.pending, delivery_status.incomplete);
      }
      if (overOrders) {//completed orders
        orderStatus.push(delivery_status.completed);
      }
    }
    else if (allOrders) {
      orderStatus.push(delivery_status.all);
    }
    data = await brokerOrders(orderFilter, dateFrom, dateTo, orderStatus);
    setLoading(false);
    setRowData(data);
    return;
  };

  //Opening the order details page
  function viewOrderDetails(params) {
    window.open("/broker-orders/" + params.data.id);
  }

  //   if (startdateValue || enddateValue) {
  //     fetchFilteredData();
  //   }
  // }, [startdateValue, enddateValue]);

  /**
   * Function which adds the new context menu shortcut,
   * originally developed for the card info modal.
   * NOTE: Must include all default right-click menu items
   * you want in this function.
   * @param {*} params
   */
  function getContextMenuItems(params) {
    var result = [
      {
        // custom item
        name: t("orders.details"),
        action: function () {
          setSelectedOrder(params.node.data);
          setOrderInfoVisible(true);
        },
        icon: {}
      },
      {
        // custom item
        name: t("orders.printShippingLabel"),
        action: async function () {
          //setSelectedOrder(params.node.data);
          const theOrder = await getOrderById(params.node.data.id);
          await GenerateShippingLabel(theOrder);
          setSelectedOrder(theOrder);
        },
        icon: {}
      },
      "export"
    ];
    return result;
  }
  function closeOrderInfoModal() {
    setOrderInfoVisible(false);
    // we have new grid data, so reload the page
    //document.location.reload();
  }
  function orderStatusAll(e) {
    setOpenOrders(0);
    setOverOrders(0);
    let value = 0;
    if (e.target.checked == true) {
      value = 1;
    }
    setAllOrders(value);
  }
  function orderStatusOpen(e) {
    setAllOrders(0);
    setOverOrders(0);
    let value = 0;
    if (e.target.checked == true) {
      value = 1;
    } else {
      setAllOrders(1);
    }
    setOpenOrders(value);
  }
  function orderStatusOver(e) {//completed orders
    setOpenOrders(0);
    setAllOrders(0);
    let value = 0;
    if (e.target.checked == true) {
      value = 1;
    } else {
      setAllOrders(1);
    }
    setOverOrders(value);
  }

  /**
  * To avoid user change the readonly/disable attributes forcefully
  */
  function avoidOnchange(e) {
    e.preventDefault();
    return false;
  }
  // free text search bar
  const onSearchInputChange = async (searchString) => {
    let searchText = '';
    if (searchString) {
      searchText = searchString.target.value.toString()
    }
    setDisplayText(searchText)
    await setOrderIds(searchText)
    // await setSearchDelay(false);
  };


  const getRowClass = {
    'order-grid-pending': function (params) { return (params.data.delivery_status_id == 3 || params.data.delivery_status_id == 4); }

  };


  const onGridReady = params => {
    // for search filtering
    setGridApi(params.api);
  };


  return !loading ? (
    <div
      style={{
        height: "calc(100vh - 18rem)",
        width: "100%",
        backgroundColor: "inherit"
      }}
      id="grid-wrapper"
      className="ag-theme-balham ag-grid-header"
    >
      <div style={{ display: "grid", gridAutoColumns: "100%", marginBottom: '20px' }}>
        <div className="search-top-input" style={{ position: 'relative' }}>
          <Input className="search-input stylefont-weight-medium"
            style={{
              width: "100%"
            }}
            options={[{ value: "text 1" }, { value: "text 2" }]}
            onChange={text => onSearchInputChange(text)}
            addonBefore={t("orders.search")}
            placeholder={t("Enter Order ID, Channel, Listing ID, External Reference ID")}
            suffix={<SearchIcon />}
            value={displayText}
            bordered={false}
          />
          {displayText &&
            <button className="search-close-icon" onClick={(e) => onSearchInputChange()} >X</button>}

        </div>
        <div className="filter-item-sec">
          <div className="row m-0">
            <div className="date-range-picker d-flex">
              <div className="order-left">
                <span className="ant-input-group-addon">Orders Date</span>
              </div>
              <div className="date-picker--right">
                <span className="icon-calendar">
                <img
                    src={imgIcon}
                  />
                </span>
                <DatePicker
                  selectsRange={true}
                  startDate={startdateValue}
                  endDate={enddateValue}
                  // onChangeRaw={(e) => this.avoidOnchange(e)}
                  onChange={(update) => { setDateRange(update); }}
                  isClearable={true}
                  dateFormat="dd/MM/yyyy"
                  onKeyDown={avoidOnchange}
                  placeholderText=" DD/MM/YYYY - DD/MM/YYYY" />
              </div>
            </div>
            <div className="order--check-box">
              <div className="all--order-item">
                <div className="form-check">
                  <input type="checkbox" className="form-check-input" checked onChange={orderStatusAll} checked={allOrders} />
                  <label className="form-check-label">All Orders</label>
                </div>
              </div>
            </div>
            <div className="order--check-box">
              <div className="open--order-item">
                <div className="form-check">
                  <input type="checkbox" className="form-check-input" onChange={orderStatusOpen} checked={openOrders} />
                  <label className="form-check-label">Open Orders</label>
                </div>
              </div>
            </div>
            <div className="order--check-box">
              <div className="complete--order-item">
                <div className="form-check">
                  <input type="checkbox" className="form-check-input" onChange={orderStatusOver} checked={overOrders} />
                  <label className="form-check-label">Complete Orders</label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <AgGridReact
        // properties
        columnDefs={columnDefs}
        rowData={rowData}
        // defaultColDef={{ filter: true }}
        defaultColDef={{ resizable: false }}
        // quickFilterText={search}
        frameworkComponents={frameworkComponents}
        // events
        onFirstDataRendered={onGridSizeChanged}
        onGridReady={onGridReady}
        // getContextMenuItems={getContextMenuItems}
        onRowClicked={viewOrderDetails}
        // onGridSizeChanged={onGridSizeChanged}
        rowHeight="60"
        headerHeight="65"
        rowClassRules={getRowClass}
        sortable={true}
      ></AgGridReact>
      <Modal
        title={t("orders.details")}
        visible={orderInfoVisible}
        onCancel={closeOrderInfoModal}
        footer={
          <Button key="submit" type="primary" onClick={closeOrderInfoModal}>
            Ok
          </Button>
        }
        width="60%"
        closable={false}
        maskClosable={false}
      >
        <OrderInfo
          order={selectedOrder}
          handleCancel={closeOrderInfoModal}
          printShippingLabel={GenerateShippingLabel}
        />
      </Modal>
    </div>
  ) : (
    <Loader />
  );
};

export default SpecificOrdersGrid;
