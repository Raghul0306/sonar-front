export const autoSizeAllColumns = params => {
    var allColumnIds = [];
    params.columnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });
    // the second argument ensures we take the headers into account when auto-sizing the graph
    params.columnApi.autoSizeColumns(allColumnIds, false);
};



export const onGridSizeChanged = (params) => {
  var gridWidth = document.getElementById('grid-wrapper').offsetWidth;
  var columnsToShow = [];
  var columnsToHide = [];
  var totalColsWidth = 0;
  var allColumns = params.columnApi.getAllColumns();
  for (var columnInfoCount = 0; columnInfoCount < allColumns.length; columnInfoCount++) {
    var column = allColumns[columnInfoCount];
    totalColsWidth += column.getMinWidth();
    
    if (totalColsWidth > gridWidth) {
      columnsToHide.push(column.colId);
    } else {
      columnsToShow.push(column.colId);
    }
  }
  params.columnApi.setColumnsVisible(columnsToShow, true);
  params.columnApi.setColumnsVisible(columnsToHide, false);
  params.api.sizeColumnsToFit();
};