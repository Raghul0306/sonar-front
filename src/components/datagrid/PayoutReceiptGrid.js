import React, { useEffect, useState } from "react";
import loadable from "@loadable/component";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import { listStripePayouts } from "../../services/stripeService";
import { useTranslation } from "react-i18next";
import { onGridSizeChanged } from "./DataGridHelpers";
import Loader from "../Loader/Loader";

const PayoutReceiptGrid = () => {
  const { t } = useTranslation();
  const [remittanceData, setRemittanceData] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [loading, setLoading] = useState(true);
  const [viewReport, setReportModal] = useState(false);



  const handleSearch = ({ target: { value } }) => {
    setSearchValue(value);
  };
  

  // function sizeColumnsToFit(): void;
  const columnDefs = [
    {
      headerName: "Order ID",
      field: "orderId",
      width: 180
    },
    {
      headerName: "Listing ID",
      field: "listingId",
      width: 180
    },
    {
      headerName: "Company",
      field: "company",
      width: 180
    },
    {
      headerName: "Order Date",
      field: "orderDate",
      width: 180
    },
    {
      headerName: "Order Amount",
      field: "orderamount",
      width: 180
    },
    {
      headerName: "Order Fees",
      field: "orderfees",
      width: 180
    }
  ];

  const rowSample = [
    { orderId: "029485", listingId: "532034", company: "Eventellect", orderDate: "06/05/2021", orderamount: "$ 60.05", orderfees: "$ 10.50"},
    { orderId: "029485", listingId: "532034", company: "Eventellect", orderDate: "06/05/2021", orderamount: "$ 60.05", orderfees: "$ 10.50"},
    { orderId: "029485", listingId: "532034", company: "Eventellect", orderDate: "06/05/2021", orderamount: "$ 60.05", orderfees: "$ 10.50"},
    { orderId: "029485", listingId: "532034", company: "Eventellect", orderDate: "06/05/2021", orderamount: "$ 60.05", orderfees: "$ 10.50"},
    { orderId: "029485", listingId: "532034", company: "Eventellect", orderDate: "06/05/2021", orderamount: "$ 60.05", orderfees: "$ 10.50"},
    { orderId: "029485", listingId: "532034", company: "Eventellect", orderDate: "06/05/2021", orderamount: "$ 60.05", orderfees: "$ 10.50"},
    { orderId: "029485", listingId: "532034", company: "Eventellect", orderDate: "06/05/2021", orderamount: "$ 60.05", orderfees: "$ 10.50"}
  ];



  useEffect(() => {
    const startFunc = async () => {
      const date = Date.now();
      const stripePayouts = await listStripePayouts(date, 100);
      await setRemittanceData(stripePayouts);
      await setLoading(false);
    }
    startFunc();
  }, []);
  

  return !loading ? (
    <div>
      <div
        style={{
          height: "calc(100vh - 14rem)",
          width: "100%",
          backgroundColor: "inherit"
        }}
        id="grid-wrapper"
        className="ag-theme-alpine ag-theme-balham ag-custom-balham buyer-order-grid"
      >
        <AgGridReact
          // properties
          columnDefs={columnDefs}
          rowData={rowSample}

          defaultColDef={{ filter: true, resizable: false }}
          quickFilterText={searchValue}
          // events
          onFirstDataRendered={onGridSizeChanged}
          rowHeight="60"
          headerHeight="65"      
          // onGridSizeChanged={onGridSizeChanged}
          sortable={true}


        />
      </div>

    </div>
  ) : (
    <Loader />
  );

};

export default PayoutReceiptGrid;