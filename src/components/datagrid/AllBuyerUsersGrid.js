import React, { useEffect, useState } from "react";
import loadable from "@loadable/component";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import Input from 'antd/es/input';
import { useTranslation } from "react-i18next";
import { getBuyerUsers } from "../../services/usersService";
import { updateBuyerNotes, sendMagicLink } from "../../services/userService";
import { onGridSizeChanged } from "./DataGridHelpers";
import Loader from "../Loader/Loader";
const SearchIcon = loadable(() => import("@material-ui/icons/Search"));

const transactionList = props => {
  
  const transactionIds = props.data.transactionsByBuyerId.map(
    transaction => transaction.id
  );

  return <span>{transactionIds.join(", ")}</span>;
};

const transactionSummary = props => {
  return <span>{props.data.transactionsByBuyerId.length}</span>;
};

const transactionValueSummary = props => {
  const transactionIds = props.data.transactionsByBuyerId.map(
    transaction => [transaction.listingByListingId[0].price]
  );

  return transactionIds.length ? (
    <span>${transactionIds.reduce((a, b) => a + b)}</span>
  ) : (
    <span>$0</span>
  );
};

// const resetPassword = props => {
//   const { t } = useTranslation();
//   return (
//     <a href="/" className="w-100 h-auto p-0 reset-password ">
//       {t("userInfo.password.reset")}
//     </a>
//   );
// };



const updateNotes = async function(params) {
  console.log(
    `We will update the notes in the DB to ${params.newValue} if it's formatted right`
  );
  updateBuyerNotes(params.data.id, params.newValue);
  return params.newValue;
};

const fullName = props => {
  return (
    <p style={{ margin: 0 }}>
      {props.data.first_name} {props.data.last_name}
    </p>
  );
};

const AllBuyerUsersGrid = () => {
  const { t } = useTranslation();
  const [rowData, setRowData] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(false);
  }, []);

  const handleSearch = e => {
    setSearchValue(e.target.value);
  };

  const sendMagicLinkAndAlertUser = async email => {
    // call the API function to send magic link if we have an email.
    await sendMagicLink(email);
    // let user know if we have an email on file that matches, they should get one.
    alert(t("login.success"));
  };
  
  const resetPassword = props => {
    const { t } = useTranslation();
    return (
      <span
        style={{ cursor: "pointer", textDecoration: "underline" }}
        onClick={async () =>
          await sendMagicLinkAndAlertUser(props.data.email)
        }
        className="reset-password w-100 h-auto p-0"
      >
        {t("userInfo.password.reset")}
      </span>
    );
  };

  const columnDefs = [
    {
      headerName: t("buyer.name"),
      cellRenderer: "fullName"
    },
    {
      headerName: t("buyer.email"),
      field: "email"
    },
    {
      headerName: t("userInfo.phone"),
      field: "phone_number"
    },
    // {
    //   headerName: t("orders.buyerDetails.dob"),
    //   field: "dob"
    // },
    {
      headerName: t("orders.buyerDetails.city"),
      field: "city"
    },
    {
      headerName: t("orders.buyerDetails.country"),
      field: "country"
    },
    {
      headerName: t("buyer.openOrders"),
      field: "transactionsByBuyerId",
      cellRenderer: "transactionList",
      // valueFormatter: ({ value }) => {
      //   const transactionIds = value.map(transaction => transaction.id);
      //   return transactionIds.join(", ");
      // },

      getQuickFilterText: ({ value }) => {
        const transactionIds = value.map(transaction => transaction.id);
        return transactionIds.join(", ");
      }
    },
    {
      headerName: t("buyer.lifetimeTransactions"),
      cellRenderer: "transactionSummary"
    },
    {
      headerName: t("buyer.lifetimeValue"),
      cellRenderer: "transactionValueSummary"
    },
    {
      headerName: t("userInfo.password.reset"),
      cellRenderer: "resetPassword"
    },
    {
      headerName: t("userInfo.notes"),
      field: "notes",
      onCellValueChanged: function(params) {
        updateNotes(params);
      },
      editable: true
    }
  ];

  const frameworkComponents = {
    transactionList,
    resetPassword,
    transactionValueSummary,
    transactionSummary,
    fullName
  };

  useEffect(() => {
    async function fetchData() {
      let data = await getBuyerUsers();
      setRowData(data);
      return;
    }

    fetchData();
  }, []);
  return !loading ? (
    <div
      style={{
        height: "calc(100vh - 14rem)",
        width: "100%",
        backgroundColor: "inherit"
      }}
      id="grid-wrapper"
      className="ag-theme-balham"
    >
      <div className="user-search-item">
        <div className="search-field-sec">
        <div className="search-top-input" style={{ position: 'relative' }}>
        <Input
            value={searchValue}
            className="search-input stylefont-weight-medium mb-3"
            style={{ width: "100%" }}
            onChange={handleSearch}
            placeholder={t("search.searchUsers")}
            suffix={<SearchIcon />}
            bordered={false}
          />
           {searchValue &&
              <button className="search-close-icon"   onClick={(e) => setSearchValue('')} >X</button>}
          </div>
        </div>
      </div>
      
      <AgGridReact
        // properties
        columnDefs={columnDefs}
        rowData={rowData}
        defaultColDef={{ filter: true, resizable: false }}
        quickFilterText={searchValue}
        frameworkComponents={frameworkComponents}
        // events
        onFirstDataRendered={onGridSizeChanged}
        // onGridSizeChanged={onGridSizeChanged}
        rowHeight="60"
        headerHeight="65"
      ></AgGridReact>
    </div>
  ) : (
    <Loader />
  );
};

export default AllBuyerUsersGrid;
