import React, { useEffect, useState, useContext } from "react";
import loadable from "@loadable/component";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import { listStripePayouts } from "../../services/stripeService";
import UserContext from "../../contexts/user.jsx";
import { getAllPurchaseOrdersLists } from "../../services/purchaseOrderService";
import { useTranslation } from "react-i18next";
import moment from "moment";
import Input from 'antd/es/input';
import DatePicker from 'antd/es/date-picker';
import Loader from "../Loader/Loader"; import styled from 'styled-components'
import { Calendar } from '@styled-icons/ionicons-outline/Calendar'
import { viewInvoiceAttachment } from '../../services/ordersService'
import { fileTypes, currencySymbol } from "../../constants/constants";
import { Attachment } from '@styled-icons/entypo/Attachment'
import iconCalendar from "../../assets/myTicketsIconCalendar.png";
// import { onGridSizeChanged } from "./DataGridHelpers";

const SearchIcon = loadable(() => import("@material-ui/icons/Search"));
// const OrderFilter = loadable(() => import("../orders/OrderFilter"));
const { RangePicker } = DatePicker;
const dateFormatList = ['DD/MM/YYYY', 'DD/MM/YY'];
const dateFormat = 'YYYY/MM/DD';

const CalendarIconInput = styled(Calendar)`
  color: #3a3a3b;
  width:30px;
  height:30px;
`
const AttachmentIcon = styled(Attachment)`
  color: #2C91EE;
  width:25px;
  height:25px;
  margin:0 4px 0 0;
`
const purchasesGrid = () => {
  const { user } = useContext(UserContext);
  const { t } = useTranslation();
  const [rowData, setRowData] = useState([]);
  const [remittanceData, setRemittanceData] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [startDate, setstartDate] = useState("");
  const [endDate, setendDate] = useState("");
  const [loading, setLoading] = useState(true);
  const [gridApi, setGridApi] = useState({});

  const handleSearch = async (e) => {
    var searchinputvalue = '';
    if (e && e.target && e.target.value) {
      searchinputvalue = e.target.value;
    }
    await setSearchValue(searchinputvalue);
    let userId;
    if (user && user.id) { userId = user.id; } else { userId = ""; }
    var allPurchaseOrdersListsData = await getAllPurchaseOrdersLists(searchinputvalue, startDate, endDate, userId);
    await setRowData(allPurchaseOrdersListsData);
  };

  const handleDateRange = async (value, dateString) => {
    await setstartDate(dateString[0]);
    await setendDate(dateString[1]);
    let userId;
    if (user && user.id) { userId = user.id; } else { userId = ""; }
    var allPurchaseOrdersListsData = await getAllPurchaseOrdersLists(searchValue, dateString[0], dateString[1], userId);
    await setRowData(allPurchaseOrdersListsData);

  };

  async function viewAttachment(orderAttachmentid, fileurl) {
    let attachments = await viewInvoiceAttachment(orderAttachmentid);
    let fileSplit = fileurl.split(".");
    let fileExtension = fileSplit && fileSplit.length ? fileSplit[fileSplit.length - 1] : '';
    if (attachments && attachments.data) {
      let pdfWindow = window.open("");
      pdfWindow.document.write(
        "<iframe width='100%' height='100%' src='data:" + fileTypes[fileExtension] + ";base64, " +
        attachments.data + "'></iframe>"
      )
    }
  }
  const attachmentsRender = (params) => {
    if (params.data.attachment_url) {
      return (
        <AttachmentIcon title="Invoice" />
      );
    }
    else return '-';
  }
  const addCurrencySymbol = (params) => {
    if (params.data.amount) {
      return currencySymbol.usd + ' ' + parseFloat(params.data.amount).toFixed(2);
    }
    else return '-';

  }
  const tagsColumn = props => {
    if (props.value) {
      let tagValues = props.value.split(",");
      return <div className="tags-section-column">
        {tagValues && tagValues.length && tagValues.map(
          (item) => (
            <button className="myticket-tags-btn stylefont-weight-medium btn">{item}</button>
          ))}</div>
    }
    else {
      return '-';
    }
  };
  const purchaseDate = (params) => {
    if (params.data.purchaseDate) {
      return params.data.purchaseDate.replace(/-/g, "/");
    }
  }
  const onFirstDataRendered = (params) => {
    params.api.sizeColumnsToFit();
  };

  const columnDefs = [
    {
      headerName: "PO ID",
      field: "id",
      minWidth: 105,
    },
    {
      headerName: "Reference ID",
      field: "listingId",
      minWidth: 130
    },
    {
      headerName: "Event ID",
      field: "eventId",
      minWidth: 115
    },
    {
      headerName: "PO Date",
      field: "purchaseDate",
      minWidth: 110,
      cellRenderer: "purchaseDate",
    },
    {
      headerName: "Amount",
      field: "amount",
      minWidth: 130,
      cellRenderer: "addCurrencySymbol",
    },
    {
      headerName: "Method",
      field: "purchaseMethod",
      minWidth: 110
    },
    {
      headerName: "Last 4 Digits",
      field: "lastFourDigits",
      minWidth: 129
    },
    {
      headerName: "Invoice",
      field: "attachment_url",
      cellRenderer: "attachmentsRender",
      minWidth: 110

    },
    {
      headerName: "Tags ",
      field: "tags",
      minWidth: 250,
      cellRenderer: "tagsColumn"

    }
  ];


  useEffect(() => {
    const startFunc = async () => {
      const date = Date.now();
      const stripePayouts = await listStripePayouts(date, 100);
      await setRemittanceData(stripePayouts);
      await setLoading(false);
      let userId;
      if (user && user.id) { userId = user.id; } else { userId = ""; }
      var allPurchaseOrdersLists = await getAllPurchaseOrdersLists(searchValue, startDate, endDate, userId);
      await setRowData(allPurchaseOrdersLists);
    }
    startFunc();
  }, []);

  // free text search bar
  const onSearchInputChange = searchString => {
    // look up cards that match the string and type
    gridApi.setQuickFilter(searchString.target.value.toString());
    // still need to factor in type somehow with quick filter
  };



  const frameworkComponents = {
    attachmentsRender,
    addCurrencySymbol,
    tagsColumn,
    purchaseDate
  }
  // ag grid auto width

  const viewPurchaseOrderDetails = (params) => {
    window.open("/purchase/purchase-detail/" + params.data.id);
  }
  return !loading ? (
    <div>
      <div className="purchase-order-gridgap" style={{ display: "grid", gridTemplateColumns: "1fr 1fr", marginBottom: '23px', marginTop: '15', alignItems: "center" }}>
        <div className="purchase-order-search-item">
          <div className="left-search-content stylefont-weight-medium">
            <div><SearchIcon /> <span>{t("purchases.search")}</span></div>
          </div>
          <div className="right-input-search">
            <div className="search-top-input" style={{ position: 'relative' }}>
              <Input
                className="purchase-search search-input stylefont-weight-medium"
                value={searchValue}
                onChange={text => handleSearch(text)}
                placeholder={"Enter PO ID, Reference ID, Listing ID, Tags"}
                bordered={"false"}
              />
              {searchValue &&
                <button className="search-close-icon" style={{ border: "0", height: '45px' }} onClick={(e) => handleSearch()} >X</button>}

            </div>
          </div>
        </div>
        <div className="date-range--picker">
          <div className="input-design-picker">
            <span className="add-date-txt">Added Date</span>
            <div className="date-picker-purchase">
              <span><img src={iconCalendar} /></span>
              <RangePicker
                defaultValue={[moment(new Date(), dateFormat), moment(new Date(), dateFormat)]}
                onChange={handleDateRange}
                format={dateFormat}
              />
            </div>
          </div>
        </div>
      </div>
      <div
        style={{
          height: "calc(100vh - 14rem)",
          width: "100%",
          backgroundColor: "inherit"
        }}
        id="grid-wrapper"
        className="ag-theme-balham purchase-order-table"
      >
        <AgGridReact
          // properties
          columnDefs={columnDefs}
          rowData={rowData}
          unSortIcon={true}
          defaultColDef={{ filter: true, resizable: false }}
          // events
          onFirstDataRendered={onFirstDataRendered}
          frameworkComponents={frameworkComponents}
          onCellClicked={viewPurchaseOrderDetails}
          // onGridSizeChanged={onGridSizeChanged}
          rowHeight="60"
          headerHeight="65"
          sortable={true}
        />
      </div>

    </div>
  ) : (
    <Loader />
  );
};

export default purchasesGrid;
