import React, { useEffect, useState } from "react";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import { autoSizeAllColumns } from "./DataGridHelpers";

import {
  getAllAuctionEvents
  // calcPotentialRev,
  // sortIntoBroker,
  // getEventsByPerformer
} from "../../services/auctionEventService";
import Input from 'antd/es/input';
import { useTranslation } from "react-i18next";

function AuctionEventsGrid() {
  const { t } = useTranslation();
  // const {columnDefs, rowData} = store;
  const [rowData, setRowData] = useState([]);
  // for enforcing search box filter
  const [gridApi, setGridApi] = useState({});

  const columnDefs = [
    {
      headerName: t("auction.name"),
      field: "name",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("form.startDate"),
      field: "startDate",
      filter: "agDateColumnFilter"
    },
    {
      headerName: t("form.endDate"),
      field: "endDate",
      filter: "agDateColumnFilter"
    },
    {
      headerName: t("auction.minGrade"),
      field: "minGrade",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("auction.category"),
      field: "categoryByCategory.name",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("auction.performer"),
      field: "performerByPerformer.name",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("auction.brand"),
      field: "setBySet.brandByBrandId.name",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("auction.set"),
      field: "setBySet.name",
      filter: "agTextColumnFilter"
    },
    {
      headerName: t("auction.year"),
      field: "setBySet.year"
    }
  ];

  // free text search bar
  const onSearchInputChange = searchString => {
    // look up cards that match the string and type
    gridApi.setQuickFilter(searchString.target.value.toString());
    // still need to factor in type somehow with quick filter
  };
  const onGridReady = params => {
    // for search filtering
    setGridApi(params.api);
  };
  useEffect(() => {
    // getEventsReportData().then(result => {
    //   result.forEach(function(event) {
    //     event.listingsByEventId.nodes.forEach(function(listing) {
    //       listing.potentialRev = calcPotentialRev(listing);
    //     });
    //   });
    //   setRowData(result)
    // });
    async function fetchData() {
      // You can await here
      let auctionEvents = await getAllAuctionEvents();
      setRowData(auctionEvents);
      return;
      // ...
    }
    fetchData();
  }, []);

  // row data will be provided via redux on this.props.rowData
  return (
    <div>
      <Input
        style={{ width: "70%", margin: "16px 0 0" }}
        options={[{ value: "text 1" }, { value: "text 2" }]}
        onChange={text => onSearchInputChange(text)}
        addonBefore={t("auction.search")}
        placeholder={t("auction.searchExample")}
      />
      <div
        style={{ height: "60vh", marginTop: 15 }}
        className="ag-theme-balham" id="grid-wrapper"
      >
        <AgGridReact
          // properties
          columnDefs={columnDefs}
          rowData={rowData}
          defaultColDef={{ filter: true, resizable: false }}
          // events
          onGridReady={onGridReady}
          onFirstDataRendered={autoSizeAllColumns}
        ></AgGridReact>
      </div>
    </div>
  );
}

export default AuctionEventsGrid;
