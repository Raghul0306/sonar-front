import React, { useEffect, useState, useContext } from "react";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import { withRouter } from "react-router-dom";
import { getFutureRemittanceData } from "../../services/remittanceService";
import { listStripeBrokerPayouts } from "../../services/stripeService";
import getFormattedNumber from "../../helpers/getFormattedNumber";
import getFormattedUnixDate from "../../helpers/getFormattedUnixDate";
import UserContext from "../../contexts/user.jsx";
import { useTranslation } from "react-i18next";

const RemittanceGrid = ({
  match: {
    params: { id }
  }
}) => {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const { userRole } = user;
  const [remittanceData, setRemittanceData] = useState([]);
  const [futureRemittance, setFutureRemittance] = useState([]);

  const allRemittance = ["ADMIN", "SUPPORT"];
  const userRemittance = ["BUYER", "SELLER"];

  const hasPermission = user => {
    if (userRemittance.includes(userRole) && user.id === Number(id))
      return true;

    if (allRemittance.includes(userRole) && !!id) return true;
  };

  const columnDefs = [
    {
      headerName: t("remittance.id"),
      field: "id",
      width: 500
    },
    {
      headerName: t("remittance.created"),
      field: "created",
      width: 180,
      valueFormatter: ({ value }) => {
        return getFormattedUnixDate(value);
      }
    },
    {
      headerName: t("remittance.payout"),
      field: "arrival_date",
      width: 180,
      valueFormatter: ({ value }) => {
        return getFormattedUnixDate(value);
      }
    },
    {
      headerName: t("remittance.amount"),
      field: "amount",
      width: 180,
      valueFormatter: ({ value }) => {
        return getFormattedNumber(value / 100, "digital");
      },
      cellStyle: ({ value }) => (value < 0 ? { color: "red" } : null)
    }
  ];

  // const dataSource = rowData ? [
  //   {
  //     key: rowData.id,
  //     period: getFormattedNumber(rowData.totalForPeriod, "digital"),
  //     paid: getFormattedNumber(rowData.amountToBePaid, "digital"),
  //     balances: getFormattedNumber(rowData.outstandingBalance, "digital"),
  //     value: getFormattedNumber(rowData.totalOrderValue, "digital"),
  //     fees: getFormattedNumber(rowData.totalFees, "digital")
  //   }
  // ] : [];

  const columns = [
    {
      title: t("remittance.gross"),
      dataIndex: "value",
      key: "value",
      align: "center"
    },
    {
      title: t("remittance.net"),
      dataIndex: "paid",
      key: "paid",
      align: "center"
    },
    {
      title: t("remittance.outstanding"),
      dataIndex: "balances",
      key: "balances",
      align: "center"
    }
  ];

  /**
   * TODO: These are pulling from the main account, when we have live user data i'll switch
   * to the broker based calls
   */
  useEffect(() => {
    // setRowData(getRemittanceData());
    const date = Date.now();
    listStripeBrokerPayouts(date, 100, user.stripeId).then(data => setRemittanceData(data));
  }, []);

  useEffect(() => {
    getFutureRemittanceData().then(data => setFutureRemittance(data));
  }, [futureRemittance]);

  /**
        * Page : Remittance
        * Function For : To stop the redirect to home page
        * Ticket No : TIC-338
  */ 

  return (
    <>
      {hasPermission(user) ? (
        <div className="row">
          <div className="col-lg-12 col-md-12">
            {/* <Table
              rowKey={record => record.id + "record"}
              columns={columns}
              dataSource={futureRemittance}
              pagination={false}
              bordered
            /> */}
            <div className="remittance-grid-top">
              {columns.map((col, key) => (
                <div className="remittance-grid-top-body">
                  <label className="remittance-grid-top-value">
                    {futureRemittance && futureRemittance[key]
                      ? "$" + futureRemittance[key][col.dataIndex]
                      : "-"}
                  </label>
                  <label className="remittance-grid-top-title">
                    {col.title}
                  </label>
                </div>
              ))}
            </div>
          </div>

          <div className="col-lg-12 col-md-12"
            style={{ marginTop: "20px", paddingBottom: "5px" }}
          >
            <div
              style={{
                height: "calc(100vh - 15rem)"
              }}
              id="grid-wrapper"
              className="ag-theme-balham"
            >
              <AgGridReact
                // properties
                columnDefs={columnDefs}
                rowData={remittanceData}
                defaultColDef={{ filter: true, resizable: false }}
                // events
                sortable={true}
                rowHeight="45"
                headerHeight="50"
              />
            </div>
          </div>
        </div>
      ) : (
        ''
      )}
    </>
  );
};

export default withRouter(RemittanceGrid);
