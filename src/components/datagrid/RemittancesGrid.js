import React, { useEffect, useState } from "react";
import loadable from "@loadable/component";
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import { listStripePayouts } from "../../services/stripeService";
import getFormattedNumber from "../../helpers/getFormattedNumber";
import getFormattedUnixDate from "../../helpers/getFormattedUnixDate";
import { useTranslation } from "react-i18next";
import { autoSizeAllColumns,onGridSizeChanged } from "./DataGridHelpers";
import Input from 'antd/es/input';
import Loader from "../Loader/Loader";
const SearchIcon = loadable(() => import("@material-ui/icons/Search"));

const RemittancesGrid = () => {
  const { t } = useTranslation();
  const [remittanceData, setRemittanceData] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [loading, setLoading] = useState(true);

  const handleSearch = ({ target: { value } }) => {
    setSearchValue(value);
  };

  
  const [gridApi, setGridApi] = useState(null);
  const [gridColumnApi, setGridColumnApi] = useState(null);
  const [rowData, setRowData] = useState(null);

  const onGridReady = (params) => {
    setGridApi(params.api);
    setGridColumnApi(params.columnApi);

    const updateData = (data) => {
      setRowData(data);
    };

    fetch(
      'https://www.ag-grid.com/example-assets/master-detail-dynamic-data.json'
    )
      .then((resp) => resp.json())
      .then((data) => updateData(data));
  };

  const onFirstDataRendered = (params) => {
    setTimeout(function () {
      params.api.getDisplayedRowAtIndex(1).setExpanded(true);
    }, 0);
  };
  // ag grid auto width
  const onGridSizeChanged = (params) => {
    var gridWidth = document.getElementById('grid-wrapper').offsetWidth;
    var columnsToShow = [];
    var columnsToHide = [];
    var totalColsWidth = 0;
    var allColumns = params.columnApi.getAllColumns();
    for (var columnInfoCount = 0; columnInfoCount < allColumns.length; columnInfoCount++) {
      var column = allColumns[columnInfoCount];
      totalColsWidth += column.getMinWidth();
      if (totalColsWidth > gridWidth) {
        columnsToHide.push(column.colId);
      } else {
        columnsToShow.push(column.colId);
      }
    }
    params.columnApi.setColumnsVisible(columnsToShow, true);
    params.columnApi.setColumnsVisible(columnsToHide, false);
    params.api.sizeColumnsToFit();
  };

  const onBtClearMilaCalls = () => {
    var milaSmithRowNode = gridApi.getRowNode('177001');
    var milaSmithData = milaSmithRowNode.data;
    milaSmithData.callRecords = [];
    gridApi.applyTransaction({ update: [milaSmithData] });
  };

  const onBtSetMilaCalls = () => {
    var milaSmithRowNode = gridApi.getRowNode('177001');
    var milaSmithData = milaSmithRowNode.data;
    milaSmithData.callRecords = [
      {
        name: 'susan',
        callId: 579,
        duration: 23,
        switchCode: 'SW5',
        direction: 'Out',
        number: '(02) 47485405',
      },
      {
        name: 'susan',
        callId: 580,
        duration: 52,
        switchCode: 'SW3',
        direction: 'In',
        number: '(02) 32367069',
      },
    ];
    gridApi.applyTransaction({ update: [milaSmithData] });
  };

  return (
    <div style={{ width: '100%', height: '100%' }}>
      <div style={{ display: 'flex', flexDirection: 'column', height: '100%' }}>
        <div
          id="grid-wrapper" 
          style={{
            height: "calc(100vh - 14rem)",
            width: '100%',
          }}
          className="ag-theme-alpine ag-custom-balham"
        >
          <AgGridReact
            masterDetail={true}
            isRowMaster={function (dataItem) {
              return dataItem ? dataItem.callRecords.length > 0 : false;
            }}
            defaultColDef={{ flex: 1 }}
            getRowNodeId={function (data) {
              return data.account;
            }}
            detailCellRendererParams={{
              detailGridOptions: {
                columnDefs: [
                  { field: 'callId' },
                  { field: 'direction' },
                  {
                    field: 'number',
                    minWidth: 150,
                  },
                  {
                    field: 'duration',
                    valueFormatter: "x.toLocaleString() + 's'",
                  },
                  {
                    field: 'switchCode',
                    minWidth: 150,
                  },
                ],
                
                defaultColDef: { flex: 1 },
              },
              getDetailRowData: function (params) {
                params.successCallback(params.data.callRecords);
              }                
            }}
            defaultColDef={{ resizable: false }}
            onGridReady={onGridReady}
            onFirstDataRendered={onGridSizeChanged}
            // onGridSizeChanged={onGridSizeChanged}
            rowData={rowData}
            rowHeight="60"
            headerHeight="65"
            sortable={true}
          >
            <AgGridColumn field="name" cellRenderer="agGroupCellRenderer" />
            <AgGridColumn field="account" />
            <AgGridColumn field="calls" />
            <AgGridColumn
              field="minutes"
              valueFormatter="x.toLocaleString() + 'm'"
            />
          </AgGridReact>
        </div>
      </div>
    </div>
  );
};

export default RemittancesGrid;
