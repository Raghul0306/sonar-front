import React, { useEffect, useState, useContext } from "react";
import UserContext from "../../contexts/user.jsx";
import { AgGridReact } from "ag-grid-react";
import { withRouter } from "react-router-dom";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import { useTranslation } from "react-i18next";
import { onGridSizeChanged } from "./DataGridHelpers";
import editIcon from "../../assets/edit-icon.png"
import cancelIcon from "../../assets/cancel-icon.png"
import Loader from "../Loader/Loader";
const ReportGrid = (params) => {

  const { user } = useContext(UserContext);

  const { t } = useTranslation();
  const [rowData, setRowData] = useState([]);
  const [columnData, setColumn] = useState([]);
  const [rowSelection] = useState("single");

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
  }, []);

  useEffect(() => {
          const columnDefs = [{
             headerName: "Scheduled Reports",
             field: "ScheduledReports"
          },
          {
            headerName: "Scheduled On",
            field: "ScheduledOn"
          },
          {
            headerName: "Scheduled Time",
            field: "ScheduledTime"
          },
          {
            headerName: "Frequency",
            field: "Frequency"
          },
          {
            headerName: "Edit / Cancel",
            field: "EditCancel",
            cellRenderer: "actionColumn"
          }

        ]
          setColumn(columnDefs);

  }, [user]);

  const rowDataSample = [
    {ScheduledReports : "Sales Report", ScheduledOn :"06/05/2021",ScheduledTime : "9:00 am CT", Frequency : "Weekly", EditCancel : "edit" },
    {ScheduledReports : "Listing Report", ScheduledOn :"06/06/2021",ScheduledTime : "9:00 am CT", Frequency : "Monthly", EditCancel :"cancel" },
    {ScheduledReports : "Sales Report", ScheduledOn :"06/05/2021",ScheduledTime : "9:00 am CT", Frequency : "Weekly", EditCancel : "edit cancel"},
    {ScheduledReports : "Listing Report", ScheduledOn :"06/06/2021",ScheduledTime : "9:00 am CT", Frequency : "Monthly", EditCancel : "edit cancel"}
]
const actionColumn = props => {
   
  return (
    <div className="edit-cancel-column">
      <button className="report-edit-btn btn"><img src={editIcon}/></button>
      <button className="report-cancel-btn btn"><img src={cancelIcon}/></button>
    </div>
  );
};

  useEffect(() => {
    let data = '';
    setRowData(data);
    setLoading(true);

  }, []);

  const frameworkComponents = {
    actionColumn
  };

  
  return loading ? (
    <div
      style={{
        height: "calc(100vh - 18rem)",
        width: "100%",
        backgroundColor: "inherit"
      }}
      id="grid-wrapper"
      className="ag-theme-balham common-ag-grid-table"
    >
      
      <AgGridReact
         defaultColDef={{ flex: 1 }}
         masterDetail={true}
         sortable={true}
         frameworkComponents={frameworkComponents}
        columnDefs={columnData}
        rowData={rowDataSample}
        rowSelection={rowSelection}
        onFirstDataRendered={onGridSizeChanged}
        // onGridSizeChanged={onGridSizeChanged}
        rowHeight="60"
        headerHeight="65"
      ></AgGridReact>
    </div>
  ) : (
    <Loader />
  );
};

export default withRouter(ReportGrid);
