import React, { useEffect, useState } from "react";
import loadable from "@loadable/component";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import { listStripePayouts } from "../../services/stripeService";
import { useTranslation } from "react-i18next";
import { onGridSizeChanged } from "./DataGridHelpers";
import Loader from "../Loader/Loader";
import styled from 'styled-components'
import { Envelope } from '@styled-icons/bootstrap/Envelope'
import { Printer } from '@styled-icons/bootstrap/Printer'

const EnvelopeIcon = styled(Envelope)`
  color: #ffffff;
  width:30px;
  height:30px;
`
const PrinterIcon = styled(Printer)`
  color: #ffffff;
  width:30px;
  height:30px;
`

const BuyerOrderGrid = () => {
  const { t } = useTranslation();
  const [remittanceData, setRemittanceData] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [loading, setLoading] = useState(true);
  const [viewReport, setReportModal] = useState(false);



  const handleSearch = ({ target: { value } }) => {
    setSearchValue(value);
  };
  

  // function sizeColumnsToFit(): void;
  const columnDefs = [
    {
      headerName: "Order Date",
      field: "orderDate",
      width: 500
    },
    {
      headerName: "Order ID",
      field: "orderId",
      width: 180
    },
    {
      headerName: "Listing ID",
      field: "listingId",
      width: 180
    },
    {
      headerName: "Event Name",
      field: "eventName",
      width: 180
    },
    {
      headerName: "Event Date",
      field: "eventDate",
      width: 180
    },
    {
      headerName: "Delivery Status",
      field: "deliveryStatus",
      width: 180
    }
  ];

  const rowSample = [
    { orderDate: "06/03/2021", orderId: "675890", listingId: "343210", eventName: "New York Yankees @ Boston Red Sox", eventDate: "06/05/2021", deliveryStatus: "Delivered 06/08/2021" },
    { orderDate: "06/03/2021", orderId: "675890", listingId: "343210", eventName: "New York Yankees @ Boston Red Sox", eventDate: "06/05/2021", deliveryStatus: "Delivered 06/08/2021" },
    { orderDate: "06/03/2021", orderId: "675890", listingId: "343210", eventName: "New York Yankees @ Boston Red Sox", eventDate: "06/05/2021", deliveryStatus: "Delivered 06/08/2021" },
    { orderDate: "06/03/2021", orderId: "675890", listingId: "343210", eventName: "New York Yankees @ Boston Red Sox", eventDate: "06/05/2021", deliveryStatus: "Delivered 06/08/2021" },
    { orderDate: "06/03/2021", orderId: "675890", listingId: "343210", eventName: "New York Yankees @ Boston Red Sox", eventDate: "06/05/2021", deliveryStatus: "Delivered 06/08/2021" },
    { orderDate: "06/03/2021", orderId: "675890", listingId: "343210", eventName: "New York Yankees @ Boston Red Sox", eventDate: "06/05/2021", deliveryStatus: "Delivered 06/08/2021" },
    { orderDate: "06/03/2021", orderId: "675890", listingId: "343210", eventName: "New York Yankees @ Boston Red Sox", eventDate: "06/05/2021", deliveryStatus: "Delivered 06/08/2021" },
    
    
  ];



  useEffect(() => {
    const startFunc = async () => {
      const date = Date.now();
      const stripePayouts = await listStripePayouts(date, 100);
      await setRemittanceData(stripePayouts);
      await setLoading(false);
    }
    startFunc();
  }, []);
  
  return !loading ? (
    <div>
      <div
        style={{
          height: "calc(100vh - 14rem)",
          width: "100%",
          backgroundColor: "inherit"
        }}
        id="grid-wrapper"
        className="ag-theme-balham ag-custom-balham buyer-order-grid"
      >
        <AgGridReact
          // properties
          columnDefs={columnDefs}
          rowData={rowSample}

          defaultColDef={{ filter: true, resizable: false }}
          quickFilterText={searchValue}
          // events
          onFirstDataRendered={onGridSizeChanged}
          rowHeight="60"
          headerHeight="65"      
          // onGridSizeChanged={onGridSizeChanged}
          sortable={true}

        />
      </div>

    </div>
  ) : (
    <Loader />
  );

};

export default BuyerOrderGrid;