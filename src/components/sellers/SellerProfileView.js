import React, { Component } from "react";
import loadable from "@loadable/component";
import Card from 'antd/es/card';

import "../../assets/formik-form-style.css";
import "react-datepicker/dist/react-datepicker.css";
//Design Starts
import styled from 'styled-components'
import { FilterCircle } from '@styled-icons/bootstrap/FilterCircle'
import { Search } from '@styled-icons/bootstrap/Search'
import { Calendar } from '@styled-icons/ionicons-outline/Calendar'
import { UpArrow } from '@styled-icons/boxicons-solid/UpArrow';
import { Close } from '@styled-icons/ionicons-outline/Close';

const OperationGird = loadable(() =>
  import("../datagrid/OperationListingGrid")
);

const Filter = styled(FilterCircle)`
  color: #4a88c5;
  width:40px;
  height:40px;
`
const SearchIcon = styled(Search)`
  color: #001F1D;
  width:20px;
  height:20px;
`
const CalendarIconInput = styled(Calendar)`
  color: #3a3a3b;
  width:30px;
  height:30px;
`
const UpArrowFill = styled(UpArrow)`
color: #2C91EE;
width:20px;
height:20px;
`
const CloseIcon = styled(Close)`
color: #556573;
width:30px;
height:30px;
`
export class OperationFilter extends Component {
  constructor(props) {
    super(props)

}
  render() {
    return (
      <>
        <div className="seller-profile-grid">
            <Card>
                <div className="seller-account-detail">
                    <h4 className="seller-info-title secondary-font-family">Seller Account</h4>
                    <div className="delivery-status-grid">
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Seller Name:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>Jacob Beman</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Company Name:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>Eventellect</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Address:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>2056 North Ave, Suite #250, Chicago, IL,USA, 60634</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Email Address:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>jacob@gmail.com</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Phone Number:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>(423) 560-9701</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Last Login:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6 className="green-txt-success">08/03/2021 11:00 am</h6>
                        </div>
                    </div>
                </div>
            </Card>
            <Card>
                <div className="seller-statistics">
                    <h4 className="seller-info-title secondary-font-family">Seller Statistics</h4>
                    <div className="delivery-status-grid">
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Seller Rank:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>#3</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Total Revenue:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6 className="green-txt-success">$ 5,650.50</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Total Sales:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <span><h6><span className="success-box">205</span></h6></span>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Delivery % :</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>85 %</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Total Cancelled Orders:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6><span className="warning-box">12</span></h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Total Substituted Orders:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6><span className="primary-box">23</span></h6>
                        </div>
                    </div>
                </div>
            </Card>
            <Card>
                <div className="seller-statistics">
                    <h4 className="seller-info-title secondary-font-family">Channel Markup</h4>
                    <div className="delivery-status-grid">
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Yadara:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>30 %</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Phunnel:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>20 %</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Stubhub:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>10 %</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Viagogo:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>10 %</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Vividseats:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>5 %</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Seatgeek:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>25 %</h6>
                        </div>
                    </div>
                </div>
            </Card>
        </div>
        <div className="notes-section">
            <Card>
                <div className="seller-notes">
                    <h4 className="seller-info-title secondary-font-family">Seller Notes</h4>
                    <div className="comments-txt">
                        <ul className="p-0">
                            <li className="list-of-comments">
                                <div className='comments-section'>
                                    <h4 className="user-title">Jacob Beman  <span>06/05/2021 @ 7:00PM CT</span> </h4>
                                    <p className="comments--txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in.</p>
                                    <div className="comments-footer">
                                        <a href="" className="edit-comments">Edit</a>
                                        <a href="" className="delete-comments">Delete</a>
                                    </div>
                                </div>
                            </li>
                            <li className="list-of-comments">
                                <div className='comments-section'>
                                    <h4 className="user-title">Tim Miller <span> 06/03/2021 @ 4:00PM CT</span></h4>
                                    <p className="comments--txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in.</p>
                                    <div className="comments-footer">
                                        <a href="" className="edit-comments">Edit</a>
                                        <a href="" className="delete-comments">Delete</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </Card>
        </div>
      </>    
    );
  }
}

export default (OperationFilter)
