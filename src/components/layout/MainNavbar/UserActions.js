import React from "react";
import { Link, withRouter } from "react-router-dom";
// import loadable from "@loadable/component";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Collapse,
  NavItem,
  NavLink,
} from "shards-react";
// import Button from 'antd/es/button';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
// import Divider from 'antd/es/divider';
import Form from 'antd/es/form';

import { SupportModalForm } from "../../insightsDash/SupportModalContent";
import UserContext from "../../../contexts/user.jsx";
import { withTranslation } from "react-i18next";
// import NewListingsGrid from "../../datagrid/NewListingsGrid";
// import OpenOrdersGrid from "../../datagrid/OpenOrdersGrid";
import profileIcon from "../../../assets/profile-icons/profile-icon.png"
import SupportIcon from "../../../assets/profile-icons/support-icon.png"
import PrivacyPolicy from "../../../assets/profile-icons/privacy-policy-icon.png"
import TermsConditions from "../../../assets/profile-icons/terms-conditions-icon.png"
import SellerAgreement from "../../../assets/profile-icons/seller-agreement-icon.png"
import LogoutIcon from "../../../assets/profile-icons/logout-icon.png"
import guideIcon from "../../../assets/profile-icons/profile_guide icon.png"
// const UserListingsGrid = loadable(() => import("../../../datagrid/userListingsGrid"));
import { saveUploadListingsFile } from "../../../services/userActionsService.js";
import { fileTypes, currentPathNames, sidebarTabNames, buttonProps, responseStatus } from "../../../constants/constants";
import { brokerSupportMail } from "../../../services/usersService";
import SampleCSVFile from "../../../assets/sampledocument.csv";

import styled from 'styled-components'
import { IosArrowLeft } from '@styled-icons/fluentui-system-filled/IosArrowLeft';
import { Envelope } from '@styled-icons/bootstrap/Envelope'
import { Printer } from '@styled-icons/bootstrap/Printer'
import notificationIcon from '../../../images/notifications.svg'

const Leftarrow = styled(IosArrowLeft)`
color: #2C91EE;
width:18px;
height:18px;
  `;
const EnvelopeIcon = styled(Envelope)`
  color: #fff;
  width:25px;
  height:25px;
`
const PrinterIcon = styled(Printer)`
  color: #fff;
  width:25px;
  height:25px;
`
class UserActions extends React.Component {
  constructor(props) {
    super(props);
    let current_path = window.location.pathname.split("/");
    let showListLinks = false;
    let uploadLinks = false;
    if ((current_path[1] != '' || current_path[1] != null) && (current_path[1] == currentPathNames.listings || current_path[1] == currentPathNames.operation)) {
      showListLinks = true;
    }
    if ((current_path[1] != '' || current_path[1] != null) && ((current_path[1] == currentPathNames.addListings && current_path[2] != '') || current_path[1] == currentPathNames.addListing || current_path[1] == currentPathNames.listings || current_path[1] == currentPathNames.operation)) {
      uploadLinks = true;
    }
    var pageTitle = this.props.history.location.pageTitle;
    // invoice modal


    this.state = {
      visible: false,
      privacyModalVisible: false,
      termsModalVisible: false,
      sellerHandbookModalVisible: false,
      sellerAgreementModalVisible: false,
      activeDoc: false,
      fileType: "docx",
      showMotificationsModal: false,
      activeTab: sidebarTabNames.listings,
      showListLinks: showListLinks,
      uploadLinks: uploadLinks,
      // pageTitle: this.props.pageTitle
      pageTitle: pageTitle,
      uploadListingsConfirmationModal: false,
      selectedUploadListingsFile: [],
      uploadfile: '',
      uploadFiletypeError: false,
      uploadFileEmptyError: false,
      supportModal: false,
      userContext: {
        userEmail: '',
        orderId: '',
        retransfer: true
      },
      orderDetailsId: '',
      issueDescription: '',
      brokerOrderInvoice: false,
      mailSent: false,
      addFileType: 0,
    };

    this.toggleUserActions = this.toggleUserActions.bind(this);
    this.uploadListingsModal = this.uploadListingsModal.bind(this);
    this.saveUploadListings = this.saveUploadListings.bind(this);
    this.uploadListings = this.uploadListings.bind(this);

  }
  componentDidMount() {

    let orderDetailsId = this.props.history.location.orderDetails && this.props.history.location.orderDetails.order_id ? this.props.history.location.orderDetails.order_id : '';

    this.setState({ orderDetailsId: orderDetailsId })


  }
  componentDidUpdate() {

    let orderDetailsIdUpdate = this.props.history.location.orderDetails && this.props.history.location.orderDetails.order_id ? this.props.history.location.orderDetails.order_id : '';
    if (orderDetailsIdUpdate && this.state.orderDetailsId != orderDetailsIdUpdate) {
      let userContextDetails = this.state.userContext;
      userContextDetails.userEmail = this.props.history.location.orderDetails.userContext.email;
      userContextDetails.orderId = this.props.history.location.orderDetails.order_id;
      this.setState({ orderDetailsId: orderDetailsIdUpdate })
    }
  }
  showSupportModal = () => {
    this.setState({
      supportModalVisible: true
    });
  };
  showGradeModal = () => {
    this.setState({
      gradeModalVisible: true
    });
  };

  handleSupportOk = e => {
    this.setState({
      supportModalVisible: false
    });
  };
  handleSupportSuccess = e => {
    this.setState({
      successVisible: true
    });
  };
  handleSupportSuccessOk = (e, redirect) => {
    this.setState({
      successVisible: false
    });
    if (redirect) {
      this.props.history.push("/listings");
    }
  };

  handleSupportCancel = e => {
    this.setState({
      supportModalVisible: false
    });
  };


  handleGradeOk = e => {
    this.setState({
      gradeModalVisible: false
    });
  };

  handleGradeCancel = e => {
    this.setState({
      gradeModalVisible: false
    });
  };

  handlePrivacyOk = e => {
    this.setState(
      {
        privacyModalVisible: false
      },
      this.setState({
        fileType: "png"
      })
    );
  };

  handlePrivacyCancel = e => {
    this.setState(
      {
        privacyModalVisible: false
      },
      this.setState({
        fileType: "png"
      })
    );
  };

  handleTermsOk = e => {
    this.setState(
      {
        termsModalVisible: false
      },
      this.setState({
        fileType: "png"
      })
    );
  };

  handleTermsCancel = e => {
    this.setState(
      {
        termsModalVisible: false
      },
      this.setState({
        fileType: "png"
      })
    );
  };

  handleSellerHandbookOk = e => {
    this.setState(
      {
        sellerHandbookModalVisible: false
      },
      this.setState({
        fileType: "png"
      })
    );
  };

  handleSellerHandbookCancel = e => {
    this.setState(
      {
        sellerHandbookModalVisible: false
      },
      this.setState({
        fileType: "png"
      })
    );
  };

  handleSellerAgreementOk = e => {
    this.setState(
      {
        sellerAgreementModalVisible: false
      },
      this.setState({
        fileType: "png"
      })
    );
  };

  handleSellerAgreementCancel = e => {
    this.setState(
      {
        sellerAgreementModalVisible: false
      },
      this.setState({
        fileType: "png"
      })
    );
  };

  toggleUserActions() {
    this.setState({
      visible: !this.state.visible
    });
  }

  async uploadListingsModal(bool) {
    await this.setState({
      uploadListingsConfirmationModal: bool,
      selectedUploadListingsFile: [],
      addFileType: this.state.addFileType + 1
    });
  }

  async saveUploadListings() {
    if (this.state.selectedUploadListingsFile != '') {
      if (!this.state.uploadFiletypeError) {
        var userId = 1;//TODO
        const attachments = await saveUploadListingsFile(userId, this.state.selectedUploadListingsFile);
        this.setState({
          uploadListingsConfirmationModal: false, uploadFileEmptyError: false
        })
      }
    }
    else {
      this.setState({ uploadFileEmptyError: true });
    }
  }

  async uploadListings(e) {
    if (e.target.files && e.target.files[0]) {
      if (e.target.files[0].type == fileTypes.xlsx_listing || e.target.files[0].type == fileTypes.csv_listing) {
        await this.setState({ selectedUploadListingsFile: e.target.files[0], uploadFiletypeError: false, uploadFileEmptyError: false });
      }
      else {
        this.setState({ selectedUploadListingsFile: [], uploadFiletypeError: true, uploadFileEmptyError: false })
      }
    }
    else {
      this.setState({ selectedUploadListingsFile: [], uploadFiletypeError: false, uploadFileEmptyError: false })
    }
  }

  submitSupportTicket = async (e) => {
    e.preventDefault();
    if (this.state.issueDescription === "") {
      alert("Issue description is required");
      return;
    }

    if (this.props.history.location.orderDetails && this.props.history.location.orderDetails.userContext && this.props.history.location.orderDetails.userContext.email) {
      let response = await brokerSupportMail('Broker Order Details Id: ' + 1, this.state.issueDescription, this.props.history.location.orderDetails.userContext.email);
      if (response && response.status == responseStatus.success) {
        this.setState({ supportModal: false, issueDescription: '', mailSent: true, mailResponse: this.props.t("support.emailSent") });
      }
      else {
        this.setState({ supportModal: false, issueDescription: '', mailSent: true, mailResponse: this.props.t("support.emailError") });
      }
    }
    else {
      this.setState({ supportModal: false, issueDescription: '', mailSent: true, mailResponse: this.props.t("support.emailError") });
    }

  }

  showBOInvoice = async () => {
    this.setState({ brokerOrderInvoice: true });
  }
  attachmentRender = () => {
    return (
      <input type="file" name={`uploadListings_${this.state.addFileType}`} accept=".csv,.xlsx,.xls" onChange={(e) => this.uploadListings(e)} />
    )
  }

  render() {
    return (
      <div className="middle-align page-top-title-section pl-db-50">
        {this.props.history.location.redirectPath &&

          <Link className="back-btn btn" to={"/" + this.props.history.location.redirectPath} >
            <Leftarrow />
          </Link>
        }
        <div className="top-page-title">
          <h4 className="page-title-txt secondary-font-family">{this.props.history.location.pageTitle}</h4>
        </div>
        <div className="right-notification-item">
          <span>
            {this.state.showListLinks ? (<Link target="_blank" to="/add-listings" className="addList-btn">Add Listings</Link>) : ''}
            {this.state.uploadLinks ? (<Link to="#" className="uploadTic-btn" onClick={(e) => this.uploadListingsModal(true)}>Upload Listings</Link>) : ''}
          </span>

          {/* Broker order buttons- start */}
          {this.props.history.location.orderDetails && this.props.history.location.orderDetails.order_id &&
            <div className="top-button-item" style={{ float: "right" }}>
              <button className="view-order-btn primary-bg-color stylefont-weight-medium" onClick={(e) => this.showBOInvoice()} >Invoice</button>
              {this.state.userContext.retransfer &&
                <button className="view-order-btn primary-bg-color stylefont-weight-medium"  >Retransfer</button>
              }
              <button className="view-order-btn primary-bg-color stylefont-weight-medium" onClick={(e) => this.setState({ supportModal: true })} >Need Help?</button>
            </div>
          }
          {/* Broker order buttons- end */}
          {/* Purchase Order creation button - start */}
          {this.props.history.location.buttonProps && this.props.history.location.buttonProps == buttonProps.purchase &&
            <Link className="creat_purchase-order primary-bg-color stylefont-weight-medium" to="/purchase/create-order" target="_blank">
              {this.props.t("purchases.add")}
            </Link>
          }
          {/* Purchase Order creation button - end*/}

          <UserContext.Consumer>
            {context => (
              <>
                <NavItem tag={Dropdown} caret toggle={this.toggleUserActions}>
                  <DropdownToggle
                    caret
                    tag={NavLink}
                    className="text-nowrap px-3"
                  >
                    <span className="d-none d-md-inline-block notification-txt">
                      {context.user.fullName}
                    </span>
                  </DropdownToggle>
                  <Collapse
                    tag={DropdownMenu}
                    right
                    small
                    open={this.state.visible}
                  >
                    <DropdownItem tag={Link} to="/user-profile">
                      {/* <i className="material-icons">&#xE7FD;</i> */}
                      <img className="profile-drop-img" src={profileIcon} />
                      {this.props.t("userProfile.profile")}
                    </DropdownItem>


                    <DropdownItem onClick={() => this.showSupportModal()}>
                      {/* <i className="material-icons">contact_support</i> */}
                      <img className="profile-drop-img" src={SupportIcon} />
                      {this.props.t("support.contact")}
                    </DropdownItem>

                    <DropdownItem tag={Link} to="/privacy-policy">
                      {/* <i className="material-icons">lock</i> */}
                      <img className="profile-drop-img" src={PrivacyPolicy} />
                      {this.props.t("support.privacyPolicy")}
                    </DropdownItem>

                    <DropdownItem tag={Link} to="/terms-conditon">
                      {/* <i className="material-icons">announcement</i> */}
                      <img className="profile-drop-img" src={TermsConditions} />
                      {this.props.t("support.terms")}
                    </DropdownItem>

                    <DropdownItem tag={Link} to="/seller-agreement">
                      {/* <i className="material-icons">thumb_up_alt</i> */}
                      <img className="profile-drop-img" src={SellerAgreement} />
                      {this.props.t("support.agreement")}
                    </DropdownItem>

                    <DropdownItem>
                      <img className="profile-drop-img" src={guideIcon} />
                    {/* <i className="material-icons">Guide</i> */}
                    {this.props.t("support.guide")}
                  </DropdownItem>
                  {/*<DropdownItem onClick={() => this.showTermsModal()}>
                    <i className="material-icons">announcement</i>
                    {this.props.t("support.terms")}
                  </DropdownItem>
                  
                  <DropdownItem onClick={() => this.showSellerAgreementModal()}>
                    <i className="material-icons">thumb_up_alt</i>
                    {this.props.t("support.agreement")}
                  </DropdownItem>*/}

                    <DropdownItem
                      onClick={() => context.logout()}
                      className=""
                    >
                      {/* <i className="material-icons">&#xE879;</i>{" "} */}
                      <img className="profile-drop-img" src={LogoutIcon} />
                      {this.props.t("form.logout")}
                    </DropdownItem>
                  </Collapse>
                </NavItem>
                <div style={{ position: "relative" }}>
                  {this.state.showMotificationsModal && (
                    <img
                      onClick={() =>
                        this.setState({ showMotificationsModal: false })
                      }
                      style={{ cursor: "pointer" }}
                      alt="notifications"
                      className="notification-icon"
                      src={notificationIcon}
                    ></img>
                  )}
                  {!this.state.showMotificationsModal && (
                    <img
                      onClick={() =>
                        this.setState({ showMotificationsModal: true })
                      }
                      style={{ cursor: "pointer" }}
                      alt="notifications"
                      className="notification-icon"
                      src={notificationIcon}
                    ></img>
                  )}
                  <Modal
                    style={{ position: "absolute", top: "6%", right: "3%" }}
                    width='50%'
                    title={"Notifications"}
                    visible={this.state.showMotificationsModal}
                    onOk={() => this.setState({ showMotificationsModal: false })}
                    onCancel={() =>
                      this.setState({ showMotificationsModal: false })
                    }
                    closable={true}
                    footer={false}
                    maskClosable={false}
                  >
                    <div style={{ border: '1px solid black' }}>
                      <div style={{ display: 'flex', border: '1px solid black' }}>
                        <div
                          style={this.state.activeTab === sidebarTabNames.listings ?
                            { margin: '0px 10px', cursor: 'pointer', fontWeight: "bold" } :
                            { margin: '0px 10px', cursor: 'pointer' }}
                          onClick={() => this.setState({ activeTab: sidebarTabNames.listings })}
                        >{this.props.t("listings.new")}</div>
                        <div
                          style={this.state.activeTab === sidebarTabNames.orders ?
                            { margin: '0px 10px', cursor: 'pointer', fontWeight: "bold" } :
                            { margin: '0px 10px', cursor: 'pointer' }}
                          onClick={() => this.setState({ activeTab: sidebarTabNames.orders })}
                        >{this.props.t("orders.open")}</div>
                      </div>
                     {this.state.activeTab === sidebarTabNames.listings && <div></div>}
                     {this.state.activeTab === sidebarTabNames.orders &&  <div></div>}
                      {/* {this.state.activeTab === sidebarTabNames.listings && <NewListingsGrid />} */}
                       
                      {/* {this.state.activeTab === sidebarTabNames.orders && <OpenOrdersGrid />} */}
                    </div>
                    {/* <Divider /> */}
                  </Modal>
                </div>
                <Modal
                  title={this.props.t("support.contact")}
                  visible={this.state.supportModalVisible}
                  onOk={this.handleSupportOk}
                  onCancel={this.handleSupportCancel}
                  closable={true}
                  footer={false}
                  destroyOnClose={true}
                  className="contact-support"
                  maskClosable={false}
                >
                  <SupportModalForm
                    handleCancel={this.handleSupportCancel}
                    handleSuccess={(e) => this.handleSupportSuccess(e)}
                    userEmail={context.user.email}
                  />
                </Modal>

                <Modal
                  // title={'Your support message has been sent!'}
                  visible={this.state.successVisible}
                  onOk={(e) => this.handleSupportSuccessOk(e, true)}
                  onCancel={(e) => this.handleSupportSuccessOk(e, false)}
                  closable={true}
                  footer={false}
                  className="contact-support"
                >
                  <div>
                    <h3 className="support-success">Your support message has been sent!</h3>
                    <p className="success-txt">Our support team will email you back as soon as possible</p>
                  </div>
                  <div className="text-center">
                    <button key="Ok" className="ant-btn success-btn--gr" type="primary" onClick={(e) => this.handleSupportSuccessOk(e, true)}>Go to MyTickets</button>
                  </div>
                </Modal>

                <Modal
                  title={this.props.t("support.gradingPolicy")}
                  visible={this.state.gradeModalVisible}
                  onOk={this.handleGradeOk}
                  onCancel={this.handleGradeCancel}
                  maskClosable={false}
                >
                  <div>
                    This is the super detailed grading policy. Please don't be
                    mean.
                  </div>
                </Modal>
                <Modal
                  title="Upload Listings"
                  visible={this.state.uploadListingsConfirmationModal}
                  onOk={(e) => this.saveUploadListings()}
                  width="600px"
                  onCancel={(e) => this.uploadListingsModal(false)}
                  className="uploading-modal-item"
                  maskClosable={false}
                >
                  <div className="text-with-link">
                    <span>Upload a CSV that adheres to the MyTickets format. <Link to={SampleCSVFile} target="_blank" download>Link to sample file.</Link></span>
                  </div>
                  <div className="upload--section">
                    <div className="choose-file-item">

                      {this.state.uploadListingsConfirmationModal && this.attachmentRender()}
                      <button className="file-upload--btn btn">Attachment</button>
                    </div>
                    <div className="file-name-section">
                      {!this.state.uploadFiletypeError &&
                        <span>{this.state.selectedUploadListingsFile && this.state.selectedUploadListingsFile.name ? this.state.selectedUploadListingsFile.name : ''}</span>
                      }
                      {this.state.uploadFiletypeError && <span style={{ color: "#dc3545" }}> Supported formats : CSV, XLS, XLSX </span>
                      }
                      {this.state.uploadFileEmptyError && <span style={{ color: "#dc3545" }}> File is required </span>
                      }
                    </div>
                  </div>
                </Modal>
                <div>
                  {/* invoice order modal popup */}
                  {this.props.history.location.orderDetails && this.props.history.location.orderDetails.order_id &&
                    <Modal
                      title='Contact Support'
                      visible={this.state.supportModal}
                      onCancel={(e) => this.setState({ supportModal: false })}
                      closable={true}
                      footer={false}
                      destroyOnClose={true}
                      className="contact-support"
                      maskClosable={false}
                    >
                      <Form name="support">
                        <Form.Item  >
                          <span className="purchase-order-form-label">
                            Order ID
                          </span>
                          <Input value={this.state.userContext.orderId} name="orderId"
                          />
                        </Form.Item>
                        <Form.Item>
                          <span className="purchase-order-form-label">
                            {this.props.t("support.email")}
                          </span>
                          <Input name="email"
                            value={this.state.userContext.userEmail}
                          />
                        </Form.Item>
                        <Form.Item>
                          <span className="purchase-order-form-label" style={{width:'100%'}}>
                            {this.props.t("support.message")}
                              <Input.TextArea style={{width:'100%'}} value={this.state.issueDescription} onChange={(e) => this.setState({ issueDescription: e.target.value })} />
                          </span>
                        </Form.Item>
                        <Form.Item validateStatus="success">
                          <div
                            style={{
                              textAlign: "center",
                              marginTop: "10px"
                            }}
                          >
                            <button
                              style={{ marginTop: "15px" }}
                              type="primary"
                              htmlType="submit"
                              disabled={this.state.issueDescription != '' ? false : true}
                              className="ant-btn contact-support-btn primary-bg-color stylefont-weight-bold"
                              onClick={(e) => this.submitSupportTicket(e)}
                            >
                              {this.props.t("support.contact")}
                            </button>
                          </div>
                        </Form.Item>
                      </Form>
                    </Modal>

                  }
                  <Modal
                    visible={this.state.mailSent}
                    onCancel={(e) => this.setState({

                      mailSent: false
                    })}
                    footer={false}
                    maskClosable={true}
                    closable={false}
                    maskClosable={false}
                    className="add--listing-page-modal">
                    <div>
                      <div className="form-group">
                        <div className="update-price-item">
                          <h3 className="success-para">{this.props.t("support.emailSent")}</h3>
                        </div>
                      </div>
                      <div className="form-group text-center">
                        <button type="button" onClick={(e) => this.setState({ mailSent: false, })} className="btn success-btn-close">Close</button>
                      </div>

                    </div>
                  </Modal>
                </div>
                {/* purchase order modal popup */}
                <Modal
                  title='Invoice'
                  visible={this.state.brokerOrderInvoice}
                  onCancel={(e) => this.setState({ brokerOrderInvoice: false })}
                  closable={true}
                  footer={false}
                  destroyOnClose={true}
                  className="purchase-order-modal"
                  maskClosable={false}
                >
                  <div className="receipt-item">
                    <h5 className="receipt-title">Email Invoice</h5>
                    <p className="receipt-txt">Generate an email invoice of this order.</p>
                    <button className="ant-btn receipt-btn"><EnvelopeIcon /> Generate Email</button>
                  </div>
                  <div className="receipt-item">
                    <h5 className="receipt-title">Print Invoice</h5>
                    <p className="receipt-txt">Download a PDF version of this invoice.</p>
                    <button className="ant-btn receipt-btn"><PrinterIcon /> Print Invoice</button>
                  </div>

                </Modal >

              </>
            )}
          </UserContext.Consumer>
        </div>
      </div>
    );
  }
}

export default withRouter(withTranslation()(UserActions));
