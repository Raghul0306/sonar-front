import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Navbar } from "shards-react";

import NavbarNav from "./NavbarNav";

const MainNavbar = ({ layout, stickyTop }) => {
  const classes = classNames("main-navbar", "relative-top");

  return (
    <div className={classes}>
      <div className="container-fluid p-0">
        <Navbar type="light" className="align-items-stretch p-0">
          {/* <NavbarSearch /> */}
          <NavbarNav />
          {/* <NavbarToggle /> */}
        </Navbar>
      </div>
    </div>
  );
};

MainNavbar.propTypes = {
  /**
   * The layout type where the MainNavbar is used.
   */
  layout: PropTypes.string,
  /**
   * Whether the main navbar is sticky to the top, or not.
   */
  stickyTop: PropTypes.bool
};

MainNavbar.defaultProps = {
  stickyTop: true
};

export default MainNavbar;
