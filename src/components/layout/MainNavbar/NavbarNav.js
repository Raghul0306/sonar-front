import React from "react";
import { Nav } from "shards-react";

// import Notifications from "./Notifications";
import UserActions from "./UserActions";

export default () => (

  
  /**
      * Page : home
      * Function For : To show the broker list for admins
      * Ticket No : TIC-171
      * TO DO : Need to implement the dsahboard data changes based on the broker selected (Hidden from menu due to design changes)
  */

  <Nav navbar className=" w-100">
    {/* <Notifications /> */}
    {/*<BrokerList />*/}
    <UserActions />
  </Nav>
);
