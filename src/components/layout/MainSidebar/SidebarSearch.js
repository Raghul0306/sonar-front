import React from "react";
import {
  Form,
  FormInput,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from "shards-react";
import { useTranslation } from 'react-i18next';

export default () => {
  const { t } = useTranslation();
  return (
    <Form className="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none" style={{ display: "flex", minHeight: "45px" }}>
      <InputGroup seamless className="ml-3">
        <InputGroupAddon type="prepend">
          <InputGroupText>
            <i className="material-icons">{t('search.title')}</i>
          </InputGroupText>
          <FormInput
            className="navbar-search"
            placeholder={t('search.placeholder')}
            aria-label="Search"
          />
        </InputGroupAddon>
      </InputGroup>
    </Form>
  )
};
