import React from "react";
import { Nav } from "shards-react";

import SidebarNavItem from "./SidebarNavItem";
import UserContext from "../../../contexts/user.jsx";
import { Store } from "../../../flux";

class SidebarNavItems extends React.Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  static contextType = UserContext;

  UNSAFE_componentWillMount() {
    Store.addChangeListener(this.onChange);
    this.setState({
      navItems: Store.getSidebarItems(this.context.user)
    });
  }

  componentWillUnmount() {
    Store.removeChangeListener(this.onChange);
  }

  onChange() {
    this.setState({
      ...this.state,
      navItems: Store.getSidebarItems(this.context.user)
    });
  }

  handleCustomNavBarStart = (index) =>{
    console.log("customNavBarStart => ",index)
    this.setState({customNavBarStart:true,startIndex:index})
  }
  
  toggleBodyClass() {
    document.body.classList.contains('menu-collapsed') ? document.body.classList.remove('menu-collapsed') : document.body.classList.add('menu-collapsed');
  }

  render() {
    const { navItems: items } = this.state;
    return (
      <div className="nav-wrapper">
        <Nav className="nav--no-borders flex-column">
          {items.map((item, idx) =>{ 
            return (
            <SidebarNavItem key={idx} index={idx} onCustomNavBarStart={this.handleCustomNavBarStart} toggleBodyClass ={this.toggleBodyClass} startIndex={this.state.startIndex} customNavBarStart={this.state.customNavBarStart} item={item} />
          )})}
        </Nav>
      </div>
    );
  }
}

export default SidebarNavItems;
