import React from "react";
import PropTypes from "prop-types";
import { Navbar } from "shards-react";
import { withTranslation } from "react-i18next";
import UserContext from "../../../contexts/user.jsx";
import { Dispatcher, Constants } from "../../../flux";
import LogoImage from "../../../assets/mytickets_logo-new.png";
import LogoSmImage from "../../../assets/myticket-small-logo.png";
import yadaraLogoImage from "../../../assets/yadara-logo.png";
import yadaraLogoSmImage from "../../../assets/yadara-small-logo.png";
//import GradedLogoImage from "../../../assets/graded-logo.png";
import { Link } from "react-router-dom";
// import "../../../assets/custom-style.css";
// import "../../../assets/ui-stylesheet.css";
// import "../../../assets/stylesheet-font.css";

import styled from 'styled-components'
import { FilterCircle } from '@styled-icons/bootstrap/FilterCircle'
// import { userRoles } from "../../../constants/constants";
const collapseMenu = styled(FilterCircle)`
  color: #00c8c8;
  width:40px;
  height:40px;
`
class SidebarMainNavbar extends React.Component {
  constructor(props) {
    super(props);

    this.handleToggleSidebar = this.handleToggleSidebar.bind(this);
    this.toggleBodyClass = this.toggleBodyClass.bind(this);
  }

  handleToggleSidebar() {
    Dispatcher.dispatch({
      actionType: Constants.TOGGLE_SIDEBAR
    });
  }
  toggleBodyClass() {
    document.body.classList.contains('menu-collapsed') ? document.body.classList.remove('menu-collapsed') : document.body.classList.add('menu-collapsed');
  }
 

  render() {
    const { hideLogoText } = this.props;
    return (
      <UserContext.Consumer>
      {context => (
        <div className="main-navbar">
          <Navbar
            className="align-items-stretch bg-white flex-md-nowrap p-0 left-sidebar"
            type="light"
          >
            <Link
              className="w-100 mr-0 custom-navbar-brand navbar-brand"
              to={"/home"}
              style={{ lineHeight: "25px" }}
            >
              <div className="d-table m-auto">
                {/* <img
                  id="main-logo"
                  className="d-inline-block align-top mr-1"
                  style={{ maxWidth: "25px" }}
                  src={require("../../../images/shards-dashboards-logo.svg")}
                  alt="Shards Dashboard"
                /> */}
                {!hideLogoText && (
                  <div
                    style={{ alignItems: "center", justifyContent: "center" }}
                    className="ml-1"
                  >
                    <img className="max-logo mytickets-logo"
                      // if the user is a marketplace user, let's use a different logo
                      // (but really they're both the same right now)
                      src={LogoImage}
                      alt={this.props.t("title")}
                    />
                    <img className="min-logo mytickets-logo"
                      // if the user is a marketplace user, let's use a different logo
                      // (but really they're both the same right now)
                      src={LogoSmImage}
                      alt={this.props.t("title")}
                    />
                    <img className="max-logo yadara-logo"
                      // if the user is a marketplace user, let's use a different logo
                      // (but really they're both the same right now)
                      src={yadaraLogoImage}
                      alt={this.props.t("title")}
                    />
                    <img className="min-logo yadara-logo"
                      // if the user is a marketplace user, let's use a different logo
                      // (but really they're both the same right now)
                      src={yadaraLogoSmImage}
                      alt={this.props.t("title")}
                    />
                  </div>
                )}
              </div>
            </Link>
            
            <div className="toggle-menu toggle-item-btn">
                <button className="btn collapseToggleMenu" onClick={this.toggleBodyClass}>
                  <i className="fas fa-chevron-right"></i>
                </button>
              </div>
            {/* eslint-disable-next-line */}
            <a
              className="toggle-sidebar d-sm-inline d-md-none d-lg-none"
              onClick={this.handleToggleSidebar}
            >
              <i className="material-icons">&#xE5C4;</i>
            </a>
          </Navbar>
        </div>
      )}
      </UserContext.Consumer>
    );
  }
}
SidebarMainNavbar.contextType = UserContext;

SidebarMainNavbar.propTypes = {
  /**
   * Whether to hide the logo text, or not.
   */
  hideLogoText: PropTypes.bool
};

SidebarMainNavbar.defaultProps = {
  hideLogoText: false
};

export default withTranslation()(SidebarMainNavbar);
