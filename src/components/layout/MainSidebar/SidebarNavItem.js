import React, { useContext,useEffect} from "react";
import PropTypes from "prop-types";
import { NavLink as RouteNavLink } from "react-router-dom";
import { NavItem, NavLink } from "shards-react";
import UserContext from "../../../contexts/user.jsx";
import useAccess from "../../../hooks/useAccess";
import { marketPlaceReportTypes } from "../../../constants/constants";

const SidebarNavItem = ({ item,onCustomNavBarStart,customNavBarStart,index,startIndex,toggleBodyClass }) => {
  const isAllow = useAccess(item.access);
  const user = useContext(UserContext);

  
  useEffect(()=>{
    if(item.title === marketPlaceReportTypes.name){
      onCustomNavBarStart(index);
    }

  },[item,index])
  if (isAllow) {
    if (
      item.to === "/payout-connection" &&
      user.user.stripeId &&
      user.user.ebayAccessToken
    ) {
      item.to = "/payout-connection/settings";
    }
    return (
      <NavItem
        className={
          `${item.title && item.title === marketPlaceReportTypes.name
            ? "marketplace-navbar-item"
            : ""}  ${customNavBarStart && startIndex < index && " custom-nav-bar"}
          `
        }
      >
        {/* {item.title == 'MyTickets' && <div className="toggle-menu toggle-item-btn">
                <button className="btn collapseToggleMenu" onClick={toggleBodyClass}>
                  <i className="fas fa-chevron-right"></i>
                </button>
              </div>} */}
        <NavLink tag={RouteNavLink} to={item.to}>
          {item.htmlBefore && (
            <div
              className="d-inline-block item-icon-wrapper"
              dangerouslySetInnerHTML={{ __html: item.htmlBefore }}
            />
          )}

          {item.title && <span>{item.title}</span>}
          {item.htmlAfter && (
            <div
              className="d-inline-block item-icon-wrapper"
              dangerouslySetInnerHTML={{ __html: item.htmlAfter }}
            />
          )}
        </NavLink>
      </NavItem>
    );
  }

  return null;
};

SidebarNavItem.propTypes = {
  /**
   * The item object.
   */
  item: PropTypes.object,
  onCustomNavBarStart: PropTypes.any,
  customNavBarStart: PropTypes.bool,
  index: PropTypes.number,
  startIndex:PropTypes.number,
  toggleBodyClass:PropTypes.any
};

export default SidebarNavItem;
