import React from "react";
import PropTypes from "prop-types";
import { Nav, NavItem, NavLink } from "shards-react";
import { Link } from "react-router-dom";
import { Translation, useTranslation } from "react-i18next";

const MainFooter = ({ 
  // contained, 
  menuItems, 
  // copyright 
}) => {
  const { t } = useTranslation();
  return (
    <footer className="main-footer d-flex p-2 px-3 bg-white border-top">
      <div className="container-fluid">
        <div className="d-flex">
          <Nav>
            {menuItems.map((item, idx) => (
              <NavItem key={idx}>
                <NavLink tag={Link} to={item.to}>
                  {item.title}
                </NavLink>
              </NavItem>
            ))}
          </Nav>
          <span className="copyright ml-auto my-auto mr-2">
            {t("copyright")}
          </span>
        </div>
      </div>
    </footer>
  );
};

MainFooter.propTypes = {
  /**
   * Whether the content is contained, or not.
   */
  contained: PropTypes.bool,
  /**
   * The menu items array.
   */
  menuItems: PropTypes.array,
  /**
   * The copyright info.
   */
  copyright: PropTypes.string
};

MainFooter.defaultProps = {
  contained: false,
  copyright: `${(<Translation>{t => t("copyright")}</Translation>)}`,
  menuItems: [
    // {
    //   title: "Home",
    //   to: "#"
    // },
    // {
    //   title: "Services",
    //   to: "#"
    // },
    // {
    //   title: "About",
    //   to: "#"
    // },
    // {
    //   title: "Products",
    //   to: "#"
    // },
    // {
    //   title: "Blog",
    //   to: "#"
    // }
  ]
};

export default MainFooter;
