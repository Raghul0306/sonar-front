import React, { useState, useEffect } from "react";
import { Col } from "shards-react";
import { Link } from "react-router-dom";
import getFormattedNumber from "../../helpers/getFormattedNumber";
import {getHotSellersList} from "../../services/listingsService";
import { useTranslation } from "react-i18next";
import Card from 'antd/es/card';
import List from 'antd/es/list';
import SubjectIcon from "@material-ui/icons/Subject";
import GetAppIcon from "@material-ui/icons/GetApp";

const HotSellers = () => {
  // eslint-disable-next-line
  const [hotSellersListings, setHotSellersListings] = useState([]);
  const { t } = useTranslation();

  useEffect(() => {
    const getAllStats = async () => {
      let allHotSellers = await getHotSellersList();
      allHotSellers && setHotSellersListings(allHotSellers.seller_list);
    };
    getAllStats();
  }, []);

   /**
      * Page : home
      * Function For : Added function to display the top hotsellers
      * Ticket No : TIC-136 & 137
   */

  return (
    <Col lg="12" md="12" sm="12" className="mb-4">
      <div className="small-card-head">
        <h3 className="card-heading">
          {t("marketplaceSellersBuyers.hotSellers")}
        </h3>
        <div className="small-card-head--btns">
          <GetAppIcon
            style={{ padding: "5px", fontSize: "30px", cursor: "pointer" }}
          />
          <SubjectIcon
            style={{ padding: "5px", fontSize: "30px", cursor: "pointer" }}
          />
        </div>
      </div>
      <div className="small-card-body">
        <Link to={`/orders/`} style={{ textDecoration: "none" }}>
          <Card
            bordered={false}
            style={{
              borderRadius: "10px",
              maxHeight: "300px",
              overflowY: "auto",
              backgroundColor: "transparent"
            }}
            bodyStyle={{ padding: "0" }}
          >
            <List className="top-performer-list">
              {hotSellersListings && hotSellersListings.map(({ id, name, price }, index) => (
                <div key={id} className="top-performer-list--item">
                  {index + 1}.{" "}
                  <div className="top-performer-list--item-body">
                    <div>{name}</div>
                    <div>{getFormattedNumber(price, "digital")}</div>
                  </div>
                </div>
              ))}
            </List>
          </Card>
        </Link>
      </div>
    </Col>
  );
};

export default HotSellers;
