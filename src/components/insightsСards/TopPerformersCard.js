import React, { useState, useEffect, useContext } from "react";
import {
  // Button,
  // ButtonGroup,
  // Card,
  // CardBody,
  // CardFooter,
  // CardHeader,
  Col
  // ListGroup,
  // ListGroupItem
} from "shards-react";
import getFormattedNumber from "../../helpers/getFormattedNumber";
import { useTranslation } from "react-i18next";
import {
  getTopFiveSoldListings
} from "../../services/listingsService";
import UserContext from "../../contexts/user.jsx";
import List from "antd/es/list";
import Button from 'antd/es/button';
import Card from 'antd/es/card';


/**
    * Page : home
    * Function For : To display the top events in sales and revenue tab
    * Ticket No : TIC-37
*/

const TopPerformersCard = () => {
  const [show, setShow] = useState(true);
  const { user } = useContext(UserContext);
  const [revenueListings, setRevenueListings] = useState([]);
  const [saleListings, setSaleListings] = useState([]);

  const { t } = useTranslation();

  const handleChange = () => {
    setShow(!show);
  };

  useEffect(() => {
    const populateTopPerformers = async () => {
        let topFiveSoldListingsBySeller = await getTopFiveSoldListings(user.id)
        setRevenueListings(topFiveSoldListingsBySeller ? topFiveSoldListingsBySeller.revenue_list : '');
        setSaleListings(topFiveSoldListingsBySeller ? topFiveSoldListingsBySeller.sale_list : '');
    };
    populateTopPerformers();
  }, []);

  return (
    <Col lg="12" md="12" sm="12" className="mb-4">
      <div className="small-card-head">
        <h3 className="card-heading">{t("chart.topCards")}</h3>
        <div className="small-card-head--btns">
          <Button
            onClick={!show ? handleChange : null}
            type={show ? "primary" : "link"}
            size="small"
            style={{
              fontSize: "13px",
              borderRadius: "15px",
              minWidth: "60px"
            }}
          >
            {t("remittance.revenue")}
          </Button>
          <Button
            onClick={show ? handleChange : null}
            type={!show ? "primary" : "link"}
            size="small"
            style={{
              fontSize: "12px",
              padding: "0",
              borderRadius: "15px",
              minWidth: "60px"
            }}
          >
            {t("notifications.sales.title")}
          </Button>
        </div>
      </div>
      <div className="small-card-body">
        <Card
          bordered={false}
          style={{
            borderRadius: "10px",
            height: "333px",
            overflowY: "auto"
          }}
          bodyStyle={{ padding: "0" }}
        >
          {show ? (
            <List className="top-performer-list">
              {revenueListings && revenueListings.length > 0
                ? revenueListings.map(({ id, name, price }, index) => (
                    <div className="top-performer-list--item">
                      {index + 1}.{" "}
                      <div className="top-performer-list--item-body">
                        <div>{name}</div>
                        <div>{getFormattedNumber(price, "digital")}</div>
                      </div>
                    </div>
                  ))
                : 
                    <div className="top-performer-list--item" style={{display:"block"}}>
                      <div className="top-performer-list--item-body">
                        No records found!!!
                      </div>
                    </div>
              }
            </List>
          ) : (
            <List className="top-performer-list">
              {saleListings && saleListings.length > 0
                ? saleListings.map(({ name, sales }, index) => (
                    <div className="top-performer-list--item">
                      {index + 1}.{" "}
                      <div className="top-performer-list--item-body">
                        <div>{name}</div>
                        <span>{getFormattedNumber(sales, "count")}</span>
                      </div>
                    </div>
                  ))
                : 
                  <div className="top-performer-list--item" style={{display:"block"}}>
                    <div className="top-performer-list--item-body">
                      No records found!!!
                    </div>
                  </div>
                }
            </List>
          )}
        </Card>
      </div>
    </Col>
  );
};

export default TopPerformersCard;
