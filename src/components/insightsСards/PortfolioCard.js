import React, { useEffect, useState, useContext } from "react";
import { Col } from "shards-react";
import { useTranslation } from "react-i18next";
import { protFoloioData } from "../../services/listingsService";
import UserContext from "../../contexts/user.jsx";
import Card from "antd/es/card";
import { Tooltip } from "@material-ui/core";
import HelpIcon from "@material-ui/icons/Help";

const toolTipStyle = {
  fontSize: "18px",
  marginLeft: "5px",
  paddingBottom: "3px",
  color: "rgba(89,89,89, 0.4)"
};

/**
    * Page : home
    * Function For : To get the portfolio insights data
    * Ticket No : TIC-34
*/

const PortfolioCard = () => {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const [inventoryValue, setInventoryValue] = useState("...");
  const [marginData, setMarginValue] = useState("...");
  const [lifetimeSales, setLifetimeSales] = useState("...");
  const [liveListings, setLiveListings] = useState("...");
  const [holdTime, setHoldTime] = useState("...");
  const [bustedOrderDate, setBustedOrderDate] = useState("...");
  const [averageListingAge, setAverageListingAge] = useState("...");

  useEffect(() => {
    const calculateInsightMetrics = async () => {
      // query our database to get information
      let portfolioData = await protFoloioData(user.id);

      // store all the results as states
      if (portfolioData) {
        setInventoryValue(portfolioData.current_inventory_value);
        setMarginValue(portfolioData.margin_value);
        setLifetimeSales(portfolioData.life_time_sales_value);
        setLiveListings(portfolioData.live_listing_value);
        setHoldTime(portfolioData.hold_time_data_value);
        setBustedOrderDate(portfolioData.busted_order_date);
        setAverageListingAge(portfolioData.average_lisitng_age);
      }
    };
    calculateInsightMetrics();
  }, []);
  const portfolioCard = [
    {
      id: 1,
      title: t("chart.collectionValue"),
      amount: `${inventoryValue != null
        ? `$${parseFloat(inventoryValue).toFixed(2)}`
        : "$0"
        }`
    },
    {
      id: 2,
      title: t("chart.portfolioGrowth"),
      amount: `${marginData != null ? parseFloat(marginData).toFixed(2) : "-"
        }`
    },
    {
      id: 3,
      title: t("chart.lifetimeSales"),
      amount: lifetimeSales != null ? lifetimeSales : "-"
    },
    {
      id: 4,
      title: t("chart.totalLiveListings"),
      amount: liveListings != null ? liveListings : "-"
    },
    {
      id: 5,
      title: t("chart.totalListingsInLiveAuctions"),
      amount: holdTime != null ? holdTime : "-"
    },
    {
      id: 6,
      title: t("chart.avgValue"),
      amount: `${bustedOrderDate != null ? bustedOrderDate : "-"
        }`
    },
    {
      id: 7,
      title: t("chart.avgListingAge"),
      amount: `${averageListingAge != null
        ? averageListingAge
        : "-"
        }`
    }
  ];

  return (
    <Col lg="12" md="12" sm="12">
      <Card
        style={{ borderRadius: "10px", marginTop: "45px" }}
        bodyStyle={{ padding: "15px" }}
        bordered={false}
      >
        <div className="portfolio-card--body">
          {portfolioCard.map(({ id, title, amount }, index) => (
            <div className="portfolio-card--body-item" key={id}>
              <div
                style={{
                  fontSize: "20px",
                  fontWeight: "700",
                  color: "#5f8ff0"
                }}
              >
                {amount}
              </div>
              {title === "Portfolio Return" ? (
                <div style={{ fontSize: "13px", color: "#acafc6" }}>
                  {title}
                  <Tooltip title={t("dashboard.tooltips.portfolio")}>
                    <HelpIcon style={toolTipStyle} />
                  </Tooltip>
                </div>
              ) : (
                <div style={{ fontSize: "13px", color: "#acafc6" }}>
                  {title}
                </div>
              )}
            </div>
          ))}
        </div>
      </Card>
    </Col>
  );
};

export default PortfolioCard;
