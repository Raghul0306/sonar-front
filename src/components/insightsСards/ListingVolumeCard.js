import React, { useState, useEffect, useContext } from "react";
import {
  Col
} from "shards-react";
import { Link } from "react-router-dom";
import {orderCardInfo} from "../../services/listingsService";
import UserContext from "../../contexts/user.jsx";
import { useTranslation } from "react-i18next";
import Card from 'antd/es/card';
import List from 'antd/es/list';
import { userRolesLower } from "../../constants/constants";
const ListingVolumeCard = () => {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const [orderInformation, setOrderInformation] = useState({});

  /**
     * Page : home
     * Function For : To show the order status detials for admin and broker users
     * Ticket No : TIC-36
     * TO DO : Need to calculate the order status based on the new db
 */

  useEffect(() => {
    const getOrderInformation = async () => {

      let pastDue = 0;
      let openOrders = 0;
      let completedOrders = 0;
      let delivery = 0;

      let orderDetails = await orderCardInfo(user.id);

       // store all the results as states
       if(orderDetails){
        pastDue = orderDetails.pastDue;
        openOrders = orderDetails.openOrders;
        completedOrders = orderDetails.completedOrders;
        delivery = orderDetails.delivery;
      }
      
      (user.userRole === userRolesLower.admin || user.userRole === userRolesLower.broker_user || user.userRole === userRolesLower.broker || user.userRole === userRolesLower.marketplace_admin || user.userRole === userRolesLower.marketplace_user) &&
        setOrderInformation([
          {
            name: t("listing.pastDue"),
            value: pastDue,
            color: "#f48a8a"
          },
          {
            name: t("listing.openOrders"),
            value: openOrders,
            color: "#42acf8" 
          },
          {
            name: t("orders.completed"),
            value: completedOrders,
            color: "#5f8ff0"
          },
          {
            name: t("listing.delivery"),
            value: delivery,
            color: "#5f8ff0"
          }
        ]);


    };
    getOrderInformation();
  }, []);

  return (
    <Col lg="12" md="12" sm="12" className="mb-4">
      <h3 className="card-heading">{t("delivery.status")}</h3>
      
      {(user.userRole === userRolesLower.admin || user.userRole === userRolesLower.broker_user || user.userRole === userRolesLower.broker || user.userRole === userRolesLower.marketplace_admin || user.userRole === userRolesLower.marketplace_user) && (
        <Card
          style={{
            borderRadius: "10px",
            height: "333px"
          }}
          bodyStyle={{ padding: "15px 10px 10px 10px" }}
          bordered={false}
        >

          {orderInformation.length > 0 && (
            <List>
              {orderInformation.map(({ name, value, color }, i) => {
                return (
                  <List.Item key={i} className="delivery-card-item">
                    <div
                      className="delivery-number"
                      style={{ backgroundColor: color }}
                    >
                      {value}
                    </div>
                    <div className="d-flex justify-content-between w-100 ml-1">
                      {name === t("listing.unsold") ||
                      name === t("listing.awaiting") ? (
                        <Link
                          className="primary-color"
                          to="/orders"
                          style={{ textDecoration: "none", color: "#acafc6" }}
                        >
                          {name}
                        </Link>
                      ) : (
                        <div style={{ color: "#acafc6" }}>{name}</div>
                      )}
                    </div>
                  </List.Item>
                );
              })}
            </List>
          )}
        </Card>
      )}

    </Col>
  );
};

export default ListingVolumeCard;
