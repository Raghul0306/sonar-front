import React, { useEffect, useContext, useState } from "react";
import { Col } from "shards-react";
import { useTranslation } from "react-i18next";
import { VictoryPie, VictoryTooltip } from "victory";
import UserContext from "../../contexts/user.jsx";
import {
  getUnsoldUserListings
} from "../../services/listingsService";
import PaletteColors from "../common/PaletteColors";
import Card from "antd/es/card";
import { Tooltip } from "@material-ui/core";
import HelpIcon from "@material-ui/icons/Help";

/**
  * Page : home
  * Function For : To display the donut chart for Performers & Category
  * Ticket No : TIC-38 & 39
  * TO DO : Need to fetch those data dynamically
*/

const toolTipStyle = {
  fontSize: "18px",
  marginLeft: "5px",
  paddingBottom: "3px",
  color: "rgba(89,89,89, 0.4)"
};

const InventoryPie = () => {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const [pieChartData, setPieChartData] = useState([]);

  useEffect(() => {
    const getAllStats = async () => {
      let allUnsoldUserListings = await getUnsoldUserListings(user.id, 1);
      allUnsoldUserListings && setPieChartData(allUnsoldUserListings.chartData);
    };
    getAllStats();
  }, []);

  // function to get the sum of all values in pie ot calculate percentages.
  const getSum = () => {
    let sum = 0;
    for (var chartDataCount = 0; chartDataCount < pieChartData.length; chartDataCount++) {
      sum += pieChartData[chartDataCount].y;
    }
    return sum;
  };

  return (
    <Col lg="12" md="12" sm="12" className="mb-4" style={{ height: "100%" }}>
      <h3 className="card-heading" style={{ display: "inline-block" }}>
        {t("chart.inventoryPortfolio")}
        <Tooltip
          title={t("dashboard.tooltips.inventoryPortfolio")}
          placement="right-end"
        >
          <HelpIcon style={toolTipStyle} />
        </Tooltip>
      </h3>

      <Card
        style={{
          borderRadius: "10px",
          height: "89%"
        }}
        bodyStyle={{ padding: "30px 10px 10px 10px" }}
        bordered={false}
      >
        <div className="pie-chart-card--body">
          <VictoryPie
            labelComponent={<VictoryTooltip />}
            colorScale={[
              PaletteColors.primary,
              PaletteColors.tertiary,
              PaletteColors.secondary,
              PaletteColors.dark,
              PaletteColors.primaryLight,
              PaletteColors.tertiaryLight,
              PaletteColors.secondaryLight,
              PaletteColors.darkLight,
              PaletteColors.primaryDark,
              PaletteColors.tertiaryDark,
              PaletteColors.secondaryDark,
              PaletteColors.darkDark
            ]}
            radius={({ datum }) => 50 + Math.round((datum.y * 100) / getSum())}
            innerRadius={30}
            data={pieChartData}
            height={300}
          />
        </div>
        <div className="graph-text-body">
          {pieChartData.map((data, i) => (
            <div key={i} className="piechart-text-body--t">
              <label className="graph-text-body--t-percentage">
                {Math.round((data.y * 100) / getSum())} %
              </label>
              <div
                style={{
                  backgroundColor: [
                    PaletteColors.primary,
                    PaletteColors.tertiary,
                    PaletteColors.secondary,
                    PaletteColors.dark,
                    PaletteColors.primaryLight,
                    PaletteColors.tertiaryLight,
                    PaletteColors.secondaryLight,
                    PaletteColors.darkLight,
                    PaletteColors.primaryDark,
                    PaletteColors.tertiaryDark,
                    PaletteColors.secondaryDark,
                    PaletteColors.darkDark
                  ][i],
                  height: "10px",
                  width: "25px",
                  borderRadius: "10px"
                }}
              ></div>
              <label className="graph-text-body--t-label">{data.x.substring(0, 5) + "..." }</label>
            </div>
          ))}
        </div>
      </Card>
    </Col>
  );
};

export default InventoryPie;
