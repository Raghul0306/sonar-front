import React, { useState, useContext, useEffect } from "react";
import { Col } from "shards-react";
import { Link } from "react-router-dom";
import getFormattedNumber from "../../helpers/getFormattedNumber";
import { useTranslation } from "react-i18next";
import { getRevenueData } from "../../services/ordersService";
import UserContext from "../../contexts/user.jsx";
import moment from "moment";
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import { Tooltip } from "@material-ui/core";
import HelpIcon from "@material-ui/icons/Help";

const toolTipStyle = {
  fontSize: "18px",
  marginLeft: "5px",
  paddingBottom: "3px",
  color: "rgba(89,89,89, 0.4)"
};

/**
    * Page : home
    * Function For : To get the Revenue card on the insights page
    * Ticket No : TIC-35
*/

const RevenueCard = () => {
  const { user } = useContext(UserContext);
  const [currentId, setCurrentId] = useState(0);
  const [todayRev, setTodayRev] = useState([]);
  const [weekRev, setWeekRev] = useState([]);
  const [monthRev, setMonthRev] = useState([]);
  const [yearToDateRev, setYearToDateRev] = useState([]);
  const { t } = useTranslation();
  const cards = [
    {
      id: 0,
      period: t("chart.today")
    },
    {
      id: 1,
      period: t("chart.week")
    },
    {
      id: 2,
      period: t("chart.month")
    },
    {
      id: 3,
      period: t("chart.ytd")
    }
  ];
  const currentCard = cards.find(({ id }) => id === currentId);

  // as soon as startDate has actually been set, get the filtered transactions.
  useEffect(() => {
    const getFilteredTransactions = async () => {
      var dataDiff = [moment().format("YYYY-MM-DD"),moment().subtract(1, "days").format("YYYY-MM-DD"), moment().subtract(1, "week").format("YYYY-MM-DD"), moment().subtract(1, "month").format("YYYY-MM-DD"), moment().startOf("year").format("YYYY-MM-DD")]
      let revenueData = await getRevenueData(
         user.id,
         dataDiff
      );

      revenueData && setTodayRev(revenueData.local_today_rev);
      revenueData && setWeekRev(revenueData.local_week_rev);
      revenueData && setMonthRev(revenueData.local_month_rev);
      revenueData && setYearToDateRev(revenueData.local_year_to_date_rev);
    };
    getFilteredTransactions();
  });

  const thisPeriod = [todayRev, weekRev, monthRev, yearToDateRev];

  return (
    <Col lg="12" md="12" sm="12" className="mb-4">
      <div className="small-card-head">
        <h3 className="card-heading" style={{ display: "inline-block" }}>
          {t("remittance.revenue")}
          <Tooltip title={t("dashboard.tooltips.revenue")} placement="right">
            <HelpIcon style={toolTipStyle} />
          </Tooltip>
        </h3>
        <div className="small-card-head--btns">
          {cards.map(({ id, period }) => (
            <Button
              key={id}
              type={currentId === id ? "primary" : "link"}
              size="small"
              onClick={() => {
                setCurrentId(id);
              }}
              style={{
                fontSize: "12px",
                padding: "0",
                borderRadius: "15px",
                minWidth: "50px"
              }}
            >
              {period}
            </Button>
          ))}
        </div>
      </div>
      <div className="small-card-body">
        <Link to={`/specific-orders/`} style={{ textDecoration: "none" }}>
          <Card
            bordered={false}
            style={{ borderRadius: "10px" }}
            bodyStyle={{ padding: "8px" }}
            key={currentCard.id}
          >
            <span className="small-card-body--value">
              {getFormattedNumber(thisPeriod[currentCard.id])}
            </span>
          </Card>
        </Link>
      </div>
    </Col>
  );
};

export default RevenueCard;
