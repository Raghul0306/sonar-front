import React, { useState, useEffect } from "react";
import {
  // Card,
  // CardBody,
  // CardHeader,
  Col
  // ListGroup,
  // ListGroupItem
} from "shards-react";
import { useTranslation } from "react-i18next";
import Card from 'antd/es/card';
import List from 'antd/es/list';

const MarketplaceSupport = () => {
  const { t } = useTranslation();
  const [supportInformation, setSupportInformation] = useState({});

  useEffect(() => {
    setSupportInformation([
      {
        name: t("support.marketplaceCard.openTickets"),
        value: 0,
        color: "#5f8ff0"
      },
      {
        name: t("support.marketplaceCard.urgent"),
        value: 0,
        color: "#f59696"
      },
      {
        name: t("support.marketplaceCard.solved"),
        value: 0,
        color: "#42acf8"
      },
      {
        name: t("support.marketplaceCard.solded"),
        value: 0,
        color: "#36bc9b"
      }
    ]);
  }, []);

  return (
    <Col lg="12" md="12" sm="12" className="mb-4">
      <h3 className="card-heading">{t("support.marketplaceCard.title")}</h3>
      <Card
        style={{
          borderRadius: "10px",
          height: "350px"
        }}
        bodyStyle={{ padding: "10px 10px 10px 10px" }}
        bordered={false}
      >
        {supportInformation.length > 0 && (
          <List>
            {supportInformation.map(({ name, value, color }, i) => {
              return (
                <List.Item key={i} className="delivery-card-item">
                  <div
                    className="delivery-number"
                    style={{ backgroundColor: color }}
                  >
                    {value}
                  </div>
                  <div className="d-flex justify-content-between w-100 ml-1">
                    <div style={{ color: "#acafc6" }}>{name}</div>
                  </div>
                </List.Item>
              );
            })}
          </List>
        )}
      </Card>
    </Col>
  );
};

export default MarketplaceSupport;
