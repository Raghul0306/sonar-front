import React, { useState, useContext, useEffect } from "react";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import { useTranslation } from "react-i18next";
import { getSalesByMonth } from "../../services/ordersService";
import UserContext from "../../contexts/user.jsx";
import Loader from "../Loader/Loader";
import dateformat from "dateformat";
const MarketplaceGrid = () => {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  // eslint-disable-next-line
  const [rowData, setRowData] = useState([]);
  const [loading, setLoading] = useState(false);

  const columnDefs = [
    {
      headerName: t("marketplaceGrid.month"),
      field: "month"
    },
    {
      headerName: t("marketplaceGrid.sales"),
      field: "sales"
    },
    {
      headerName: t("marketplaceGrid.revenue"),
      field: "revenue"
    },
    {
      headerName: t("marketplaceGrid.aov"),
      field: "aov"
    },
    {
      headerName: t("marketplaceGrid.takeRate"),
      field: "takeRate"
    },
    {
      headerName: t("marketplaceGrid.growth"),
      field: "growth"
    }
  ];
  useEffect(() => {
    setLoading(true);
    async function fetchData() {
      let data = await getSalesByMonth();
      data && setRowData(data.sales_list);
      setLoading(false);
    }
    fetchData();
  }, []);

  /**
      * Page : home
      * Function For : Added API service to get the sales and revenue data
      * Ticket No : TIC-134
  */

  return loading ? (
    <Loader />
  ) : (
    <div
      style={{
        height: "calc(100vh - 17rem)",
        width: "100%",
        backgroundColor: "inherit",
        padding: "10px"
      }}
      className="ag-theme-balham"
    >
      <div style={{ paddingBottom: "15px" }} className="card-heading">
        {t("marketplaceGrid.title")}
      </div>
      <AgGridReact
        // properties
        columnDefs={columnDefs}
        rowData={rowData}
        defaultColDef={{ filter: true, resizable: false }}
        // events
        rowHeight="45"
        headerHeight="50"
        sortable={true}
      ></AgGridReact>
    </div>
  );
};

export default MarketplaceGrid;
