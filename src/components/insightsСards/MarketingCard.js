import React, { useState, useEffect } from "react";
import {
  // Card,
  // CardBody,
  // CardHeader,
  Col
  // ListGroup,
  // ListGroupItem
} from "shards-react";
import { useTranslation } from "react-i18next";
import Card from 'antd/es/card';
import List from 'antd/es/list';

const MarketingCard = () => {
  const { t } = useTranslation();
  const [marketingInformation, setmarketingInformation] = useState({});

  useEffect(() => {
    setmarketingInformation([
      {
        name: t("marketing.conversion"),
        value: 0,
        color: "#5f8ff0"
      },
      {
        name: t("marketing.aov"),
        value: 0,
        color: "#f59696"
      },
      {
        name: t("marketing.takeRate"),
        value: 0,
        color: "#42acf8"
      },
      {
        name: t("marketing.lifetimeValue"),
        value: 0,
        color: "#36bc9b"
      }
    ]);
  }, []);

  return (
    <Col lg="12" md="12" sm="12" className="mb-4">
      <h3 className="card-heading">{t("marketing.title")}</h3>
      <Card
        style={{
          borderRadius: "10px",
          height: "350px"
        }}
        bodyStyle={{ padding: "10px 10px 10px 10px" }}
        bordered={false}
      >
        {marketingInformation.length > 0 && (
          <List>
            {marketingInformation.map(({ name, value, color }, i) => {
              return (
                <List.Item key={i} className="delivery-card-item">
                  <div
                    className="delivery-number"
                    style={{ backgroundColor: color }}
                  >
                    {value}
                  </div>
                  <div className="d-flex justify-content-between w-100 ml-1">
                    <div style={{ color: "#acafc6" }}>{name}</div>
                  </div>
                </List.Item>
              );
            })}
          </List>
        )}
      </Card>
    </Col>
  );
};

export default MarketingCard;
