import React, { useState, useEffect, useContext } from "react";
import { Col } from "shards-react";
import { useTranslation } from "react-i18next";
import {
  VictoryBar,
  VictoryChart,
  VictoryTheme,
  VictoryAxis,
  VictoryLine,
  VictoryTooltip
} from "victory";
import Card from "antd/es/card";
import { getAllOrdersBarChart } from "../../services/ordersService";
import UserContext from "../../contexts/user.jsx";
import Loader from "../Loader/Loader";
import dateformat from "dateformat";

const Allocationbar = () => {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const [barChartData, setBarChartData] = useState([]);
  const [lineChartData, setLineChartData] = useState([]);
  const [salesMax, setSalesMax] = useState([]);
  const [revenueMax, setRevenueMax] = useState([]);
  const [calendarPeriod, setCalendarPeriod] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      let data = await getAllOrdersBarChart();
      setLineChartData(data.lineChart);
      setBarChartData(data.barChart);
      setSalesMax(data.salesMax);
      setRevenueMax(data.revenueMax);
      setCalendarPeriod(data.calendarPeriod);
      setLoading(false);
    }
    fetchData();
  }, []);
  const catA = {
    x: calendarPeriod,
    y: [0, 5500]
  };

  const catB = {
    x: calendarPeriod,
    y: [0, 4]
  };

  /**
    * Page : home
    * Function For : Added victory mixed chart (Bar and line) to show the sales and revenue
    * Ticket No : TIC-131
  */

  return !loading ? (
    <Col lg="12" md="12" sm="12" className="mb-4">
      <Card
        style={{
          borderRadius: "10px",
          height: "auto"
        }}
        bodyStyle={{ padding: 0 }}
        bordered={false}
      >
        <div
          style={{ textAlign: "center", paddingTop: "15px", fontSize: "20px" }}
          className="card-heading"
        >
          {t("marketplaceChart.monthlySalesRevenue")}
        </div>
        <div>
          <VictoryChart domainPadding={10} height={180} padding={30}>
            <VictoryAxis
              standalone={false}
              style={{ tickLabels: { fill: "#a9a9a9", fontSize: "6.5px" } }}
            />
            <VictoryAxis
              dependentAxis
              orientation="right"
              standalone={false}
              style={{ tickLabels: { fill: "#a9a9a9", fontSize: "6.5px" } }}
            />

            <VictoryAxis
              dependentAxis
              orientation="left"
              standalone={false}
              style={{ tickLabels: { fill: "#a9a9a9", fontSize: "6.5px" } }}
            />
            

            <VictoryLine
              labelComponent={<VictoryTooltip/>}
              style={{
                data: { stroke: "#36bc9b" },
                parent: { border: "1px solid #36bc9b" }
              }}
              data={lineChartData}
              categories={{ ...catB }}
            />
            <VictoryBar
              categories={{ ...catA }}
              labelComponent={<VictoryTooltip/>}
              style={{
                data: { fill: "#5f8ff0" }
              }}
              standalone={false}
              theme={VictoryTheme.material}
              data={barChartData}
            />
          </VictoryChart>
        </div>
        <div className="graph-text-body">
          <div
            style={{ textAlign: "center" }}
            className="barchart-text-body--t"
          >
            <div
              style={{
                backgroundColor: "#5f8ff0",
                height: "10px",
                width: "25px",
                borderRadius: "10px"
              }}
            ></div>
            <label className="graph-text-body--t-label">
              {t("marketplaceChart.salesVolume")}
            </label>
          </div>
          <div className="barchart-text-body--t">
            <div
              style={{
                backgroundColor: "#36bc9b",
                height: "10px",
                width: "25px",
                borderRadius: "10px"
              }}
            ></div>
            <label className="graph-text-body--t-label">
              {t("marketplaceChart.salesRevenue")}
            </label>
          </div>
        </div>
      </Card>
    </Col>
  ) : (
    <Loader />
  );
};

export default Allocationbar;
