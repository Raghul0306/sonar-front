import React, { useContext, Component } from "react";
import loadable from "@loadable/component";
import Card from "antd/es/card";
import "../../assets/formik-form-style.css";
import "react-datepicker/dist/react-datepicker.css";
//Design Starts
import styled from 'styled-components'
import { FilterCircle } from '@styled-icons/bootstrap/FilterCircle'
import { Search } from '@styled-icons/bootstrap/Search'
import { Calendar } from '@styled-icons/ionicons-outline/Calendar'
import { UpArrow } from '@styled-icons/boxicons-solid/UpArrow';
import { Close } from '@styled-icons/ionicons-outline/Close';

const PayoutReceiptGrid = loadable(() =>
  import("../datagrid/PayoutReceiptGrid")
);

const Filter = styled(FilterCircle)`
  color: #4a88c5;
  width:40px;
  height:40px;
`
const SearchIcon = styled(Search)`
  color: #001F1D;
  width:20px;
  height:20px;
`
const CalendarIconInput = styled(Calendar)`
  color: #3a3a3b;
  width:30px;
  height:30px;
`
const UpArrowFill = styled(UpArrow)`
color: #2C91EE;
width:20px;
height:20px;
`
const CloseIcon = styled(Close)`
color: #556573;
width:30px;
height:30px;
`
export class PayoutReceiptView extends Component {
  constructor(props) {
    super(props)

}
  render() {
    return (
      <>
        <div className="seller-profile-grid-3">
            <Card>
                <div className="seller-account-detail">
                    <h4 className="seller-info-title secondary-font-family">Payout Information</h4>
                    <div className="delivery-status-grid">
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Payout ID:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium stylefont-weight-medium">
                            <h6>0923471</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Transaction ID:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>3056940</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Payout Date:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>06/04/2021</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Payout Period:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>05/28/2021 thru 06/03/2021</h6>
                        </div>
                    </div>
                </div>
            </Card>
            <Card>
                <div className="seller-account-detail">
                    <h4 className="seller-info-title secondary-font-family">Seller Information</h4>
                    <div className="delivery-status-grid">
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Seller ID:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>504956</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Seller Name:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>Jake Beman</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Seller Phone:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>(450) 786-9401</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Seller Email:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6>jacob@gmail.com</h6>
                        </div>
                    </div>
                </div>
            </Card>
            <Card>
                <div className="seller-statistics">
                    <h4 className="seller-info-title secondary-font-family">Payout Statistics</h4>
                    <div className="delivery-status-grid">
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Gross Sales:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6 className="green-txt-success">$ 281.00</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Total Fees:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6 className="warning-txt">$ 50.50</h6>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Total Orders:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <span><h6><span className="primary-box">4</span></h6></span>
                        </div>
                        <div className="seller-left-info stylefont-weight-medium">
                            <h6>Payout Amount:</h6>
                        </div>
                        <div className="seller-right-info stylefont-weight-medium">
                            <h6 className="greenblue-txt">$ 230.50</h6>
                        </div>
                    </div>
                </div>
            </Card>
        </div>
        
        <div className="buyer-order-history">
            <div className="header-history">
                Payout Orders
            </div>
            <PayoutReceiptGrid/>
        </div>
      </>    
    );
  }
}

export default (PayoutReceiptView)
