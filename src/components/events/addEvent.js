import React from "react";
import Tabs from 'antd/es/tabs';
import Input from 'antd/es/input';
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { withTranslation } from "react-i18next";
import { Button, Modal } from 'react-bootstrap';
import Select from 'react-select'
import Cookies from "universal-cookie";
import "../../assets/event_form.css";
import UserContext from "../../contexts/user.jsx";
import { fileTypes, eventClassName, editEventStateVariables, modalPopupEventNames, searchFiltersFor, tabsPane } from "../../constants/constants";

import { getCountries, getStates, getCities } from "../../services/userService";
import { getAllTicketingSystemList, allCategoryDetails, allGenreDetails } from "../../services/listingsService";
import { addEvents, getAllEventStatus, getVenueFilter, venueSearchFilter, imageUpload, getEventFilter, getEventdetail, updateEvents, viewEventAttachment, updateEventStatus } from "../../services/eventService";
import loadable from "@loadable/component";

import iconInhand from "../../assets/myTicketsIconCalendar.png";
import iconTime from "../../assets/MyTickets-icon_clock.png";
// style icon
import styled from 'styled-components'
import { Calendar } from '@styled-icons/boxicons-regular/Calendar'
import { Edit } from '@styled-icons/feather/Edit'
import { Attachment } from '@styled-icons/entypo/Attachment'
import * as moment from "moment-timezone";
import { Confirm, Info } from "../../components/modal/common-modal";
const SearchIcon = loadable(() => import("@material-ui/icons/Search"));

const CalendarIcon = styled(Calendar)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const EditIcon = styled(Edit)`
  color: #ffffff;
  width:20px;
  height:20px;
  padding-bottom: 2px;
`
const AttachmentIcon = styled(Attachment)`
  color: #2C91EE;
  width:20px;
  height:20px;
`

const cookies = new Cookies();
const { TabPane } = Tabs;
export class AddEvent extends React.Component {
  state = {
    loading: true,
    suggestions: [],
    editEventSuggestions: [],
    searchText: '',
    performerSuggestions: []
  };
  constructor(props) {
    super(props);

    this.state = {
      countryList: "",
      selectedCountry: "",
      eventName: "",
      eventStatus: "2",
      eventStatusOption: "",
      performer: "",
      opponent: "",
      eventDate: "",
      eventTime: "",
      venueName: "",
      venueAddress: "",
      venueAddress2: "",
      state: "",
      zip_code: "",
      image: [],
      image_name: "",
      city: "",
      country: "",
      eventLink: "",
      ticketingSystem: "",
      category: "",
      genre: "",
      allTicketingSystem: [],
      allEventStatus: [],
      allGenreList: [],
      allCategoryList: [],
      eventdate_day: "",
      searchData: {
        searches: "",
        performer: ""
      },
      eventUpdate: false,

      eventModelmessage: "",
      modalPopupMode: "",
      InfoSubmitButtonModal: false,
      infocloseButtonModal: false,
      closeButtonModal: false,
      submitButtonModal: false,
      datafields: "",
      userdetails: "",
      defaultKey: "",
      eventSearchText: "",
      eventSearchSuggestion: [],
      eventStatusSearchSuggestion: [],
      editEventFormInformation: [],
      editImageName: "",
      editImage: "",
      eventSelectedDetails: false,
      eventStatusSelectedDetails: false,
      eventSelectedDetailsJson: {},
      eventSearchInputText: "",
      defaultOption: {},
      eventStatusSelectedDetailsJson: {},
      selectedEventId: "",
      enableStatusUpdateModal: true,
      selectedStateDropDown: "",
      selectedCityDropDown: "",
      stateList: [], cityList: [],
      stateListEdit: [], cityListEdit: [],
      selectedCity: "",
      showSuggestion:true, // prevent showing the suggestion, after the event selection : boolean
      showSuggestionEdit:true
    };

    this.avoidOnchange = this.avoidOnchange.bind(this);
    this.eventSearch = this.eventSearch.bind(this);
    this.resetForm = this.resetForm.bind(this);
    this.listSearchChange = this.listSearchChange.bind(this);
    this.selectListing = this.selectListing.bind(this);
    this.selectEditVenueDetail = this.selectEditVenueDetail.bind(this);
    this.selectEvent = this.selectEvent.bind(this);
    this.selectPerformerLisitng = this.selectPerformerLisitng.bind(this);
    this.handlesubmitButtonModal = this.handlesubmitButtonModal.bind(this);
    this.handleInfoSubmitButtonModal = this.handleInfoSubmitButtonModal.bind(this);
    this.handleImageChange = this.handleImageChange.bind(this);
    this.handleEditEventImageChange = this.handleEditEventImageChange.bind(this);
    this.handleEventEdit = this.handleEventEdit.bind(this);
    this.handleViewEventAttachment = this.handleViewEventAttachment.bind(this);
    this.handleEventStatus = this.handleEventStatus.bind(this);
    this.dropdownFilterOption = this.dropdownFilterOption.bind(this);
    this.setHistoryPageTitle = this.setHistoryPageTitle.bind(this);
    this.eventSearchForStatus = this.eventSearchForStatus.bind(this);
    this.selectEventForEventStatus = this.selectEventForEventStatus.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);

    this.onLoadStateDropDown = this.onLoadStateDropDown.bind(this);
    this.onLoadCityDropDown = this.onLoadCityDropDown.bind(this);

  }




  async handleViewEventAttachment() {
    var eventImagelink = this.state.editEventFormInformation.eventImagelink;
    var extension = eventImagelink.split(".").pop();
    var fileName = eventImagelink.split("/").pop();
    await viewEventAttachment(this.state.editEventFormInformation.eventId, fileName).then((res) => {
      if (res && res.data) {
        let pdfWindow = window.open("");
        pdfWindow.document.write(
          "<iframe width='100%' height='100%' src='data:" + fileTypes[extension] + ";base64, " +
          encodeURI(res.data) + "'></iframe>"
        )
      }
    });
  }

  handleInfoSubmitButtonModal = async (modalpopupstatus) => {
    await this.setState({
      InfoSubmitButtonModal: true,
      infocloseButtonModal: false,
    });
  }

  handlesubmitButtonModal = async (modalpopupstatus) => { //Event Creation Form Submit
    if (modalpopupstatus) {
      if (this.state.modalPopupMode === modalPopupEventNames.addEvent) {
        //Add Event
        await this.setState({ submitButtonModal: true, eventModelmessage: "Event added successfully !!!" });
        const response = await addEvents(this.state.datafields, this.state.userdetails);
        if (!response.isError) {
          if (response.attachmentData && response.attachmentData.id) {
            let eventId = response.attachmentData.id;
            if (this.state.image.length != 0) {
              await imageUpload(eventId, this.state.image, this.state.image_name);//uploaded s3 bucket
            }
          }
          this.setMyPageLoader(false);
          await this.setState({ infocloseButtonModal: true, closeButtonModal: false });
        } else {
          await this.setState({ infocloseButtonModal: true, closeButtonModal: false });
        }

      } else if (this.state.modalPopupMode === modalPopupEventNames.editEvent) {
        //Edit Event
        await this.setState({ submitButtonModal: true, eventModelmessage: "Event updated successfully !!!" });
        let eventId = this.state.editEventFormInformation.eventId;
        let response;
        if (this.state.editImage.length != 0) {
          const imageUploads = await imageUpload(eventId, this.state.editImage, this.state.editImageName);//uploaded s3 bucket

          if (imageUploads) {
            response = await updateEvents(this.state.datafields, this.state.userdetails);
          }
        } else {
          response = await updateEvents(this.state.datafields, this.state.userdetails);
        }
        if (!response.isError) {
          this.setMyPageLoader(false);
          await this.setState({ infocloseButtonModal: true, closeButtonModal: false, editEventFormInformation: [], editEventFormInformationDetail: {}, eventSelectedDetails: false, eventSelectedDetailsJson: {} });



        } else {
          await this.setState({ infocloseButtonModal: true, closeButtonModal: false });
        }
      }
      this.resetForm();
      await this.setState({ closeButtonModal: false });

    } else {
      await this.setState({ closeButtonModal: false });

    }

  }
  async componentDidUpdate() {
    this.setHistoryPageTitle();
  }

  async componentDidMount() {
    let countries = await getCountries();
    let getTicketingSystems = await getAllTicketingSystemList();
    let getCategoryDetails = await allCategoryDetails();
    let getGenreDetails = await allGenreDetails();
    let allEventStatus = await getAllEventStatus();
    var tabPaneNumber = tabsPane.tabOne;
    if (this.props.match.params && this.props.match.params.find && this.props.match.params.find == tabsPane.paramKey) {
      tabPaneNumber = tabsPane.tabTwo;
    }
    this.setState({ defaultKey: tabPaneNumber });


    if (countries) {
      const countriesList = [];
      countries.length && countries.map(items => {
        countriesList.push({ label: items.name, value: items.id })
      });

      const allEventStatusList = [];
      allEventStatus && allEventStatus.length && allEventStatus.map(items => {
        allEventStatusList.push({ label: items.event_status, value: items.id })
      });

      const allTicketingSystemsList = [];
      getTicketingSystems && getTicketingSystems.length && getTicketingSystems.map(items => {
        allTicketingSystemsList.push({ label: items.ticketing_system_name, value: items.id })
      });

      const allCategoryList = [];
      getCategoryDetails && getCategoryDetails.length && getCategoryDetails.map(items => {
        allCategoryList.push({ label: items.name, value: items.id })
      });

      const allGenreList = [];
      getGenreDetails && getGenreDetails.length && getGenreDetails.map(items => {
        allGenreList.push({ label: items.name, value: items.id })
      });

      await this.setState({
        countryList: countriesList,
        allTicketingSystem: allTicketingSystemsList,
        allEventStatus: allEventStatusList,
        allCategoryList: allCategoryList,
        allGenreList: allGenreList
      });
    }

  }
  setHistoryPageTitle = async () => {
    if (!this.props.history.location.pageTitle) {
      this.props.history.push({ pageTitle: this.props.t("events.title"), pageSubTitle: this.props.t("title") });
    }
  }
  async listSearchChange(e, type) {   // For autofill search suggestion

    let suggestions = [];
    let editEventSuggestions = [];
    let performerSuggestions = [];
    const value = e.target.value;
    let stateHolder = this.state.searchData;
    stateHolder.searches = value;
    this.setState({ locationText: '', searchText: value, displayText: value, searchData: stateHolder });

    if (type == searchFiltersFor.venue) {
      this.setState({ venueName: value });
      if (value.length > 2) {

        suggestions = await venueSearchFilter({ searches: value, type: type });

      }
      else {
        suggestions = [];
        if (value.length == 0) {
          this.setState({ city: "", state: "", venueAddress: "", venueAddress2: "", country: "", zip_code: "" })
        }
      }
      this.setState(() => ({
        suggestions,
      }));

    } else if (type == searchFiltersFor.venueName) {

      let editEventFormInformationDetail = this.state.editEventFormInformation;
      editEventFormInformationDetail.venueName = e.target.value;
      this.setState({ ...this.state.editEventFormInformation, editEventFormInformationDetail });

      if (value.length > 2) {
        editEventSuggestions = await venueSearchFilter({ searches: value, type: type });
      }
      else {
        editEventSuggestions = [];
        if (value.length == 0) {
          editEventFormInformationDetail.venueAddress = "";
          editEventFormInformationDetail.venueAddress2 = "";
          editEventFormInformationDetail.venueCity = "";
          editEventFormInformationDetail.venueZipcode = "";
          editEventFormInformationDetail.venueState = "";
          editEventFormInformationDetail.venueCountry = "";
          this.setState({ ...this.state.editEventFormInformation, editEventFormInformationDetail });
        }
      }
      this.setState(() => ({
        editEventSuggestions,
      }));


    }
    else {
      this.setState({ performer: value });
      if (value.length > 2) {
        this.setMyPageLoader(true);
        performerSuggestions = await venueSearchFilter({ searches: value, type: type });
        this.setMyPageLoader(false);
      }
      else {
        performerSuggestions = [];
      }
      this.setState(() => ({
        performerSuggestions,
      }));
    }
  }


  async onLoadStateDropDown(countryId, stateId, cityId) {
    if (countryId && countryId.value) {
      let states = await getStates(countryId.value);

      if (states) {
        const stateList = [];
        states.length && states.map(items => {
          if (stateId === items.id) {
            this.setState({ selectedStateDropDown: { label: items.name, value: items.id }, selectedState: items.id });
          }
          stateList.push({ label: items.name, value: items.id })
        });
        await this.setState({
          stateList: stateList
        });
        if (cityId) {
          await this.onLoadCityDropDown(countryId.value, this.state.selectedState, cityId)
        }
      }
    }
    else {
      await this.setState({
        stateList: [],
        cityList: []
      });
    }

  }

  async onLoadCityDropDown(countryId, stateId, cityId) {

    let cities = await getCities(countryId, stateId);
    if (cities) {
      const cityList = [];
      cities.length && cities.map(items => {

        if (cityId === items.id) {
          this.setState({ selectedCityDropDown: { label: items.name, value: items.id }, selectedCity: items.id });
        }
        cityList.push({ label: items.name, value: items.id })
      });
      await this.setState({
        cityList: cityList
      });
    }
  }

  renderSuggestions = () => {
    const { suggestions } = this.state;

    if (suggestions.length === 0) {
      return null;
    }

    return (
      <ul>
        {suggestions.map(events => <li onClick={() => this.selectListing(events)} >{events}</li>)}
      </ul>
    )
  }

  renderEditEventSuggestions = () => {
    const { editEventSuggestions } = this.state;

    if (editEventSuggestions.length === 0) {
      return null;
    }

    return (
      <ul>
        {editEventSuggestions.map(events => <li onClick={() => this.selectEditVenueDetail(events)} >{events}</li>)}
      </ul>
    )
  }


  renderPerformerSuggestions = () => {
    const { performerSuggestions } = this.state;

    if (performerSuggestions.length === 0) {
      return null;
    }

    return (
      <ul>
        {performerSuggestions.map((performer, key) => <li key={key} onClick={() => this.selectPerformerLisitng(performer)} >{performer}</li>)}
      </ul>
    )
  }

  async selectPerformerLisitng(keyword = false) {
    this.setState({ performerSuggestions: null, performer: keyword.toString() });
    let allSearchData = this.state.searchData;

    allSearchData.searches = keyword.toString();
    this.setState({ searches: allSearchData })
  }

  async selectEditVenueDetail(keyword = false) {
    await this.setState({ editEventSuggestions: null, venueName: keyword.toString(), });
    let allSearchData = this.state.searchData;

    allSearchData.searches = keyword.toString();
    await this.setState({ searches: allSearchData })
    this.setMyPageLoader(true);

    let searchedData = await getVenueFilter(this.state.searchData);

    if (searchedData) {
      var venue_country_id = "";
      if (searchedData.country) {
        await this.state.countryList && this.state.countryList.length > 0 &&
          this.state.countryList.map((item) => {
            if (item.label == searchedData.country) {
              venue_country_id = item;
            }
          })
      }
      let editEventFormInformationDetail = this.state.editEventFormInformation;
      editEventFormInformationDetail.venueName = searchedData.name;
      editEventFormInformationDetail.venueId = searchedData.id;
      editEventFormInformationDetail.venueAddress = searchedData.address;
      editEventFormInformationDetail.venueAddress2 = searchedData.address2;
      editEventFormInformationDetail.venueCity = searchedData.city;
      editEventFormInformationDetail.venueZipcode = searchedData.zip_code;
      editEventFormInformationDetail.venueState = searchedData.state;
      editEventFormInformationDetail.venueCountry = venue_country_id;
      this.setState({ ...this.state.editEventFormInformation, editEventFormInformationDetail });
    }

    this.setMyPageLoader(false);
  }

  async selectListing(keyword = false) {
    await this.setState({ suggestions: null, venueName: keyword.toString() });
    let allSearchData = this.state.searchData;

    allSearchData.searches = keyword.toString();
    await this.setState({ searches: allSearchData })
    this.setMyPageLoader(true);

    let searchedData = await getVenueFilter(this.state.searchData);

    if (searchedData) {
      var venue_country_id = "";
      if (searchedData.country) {
        var venue_country = this.state.countryList.find(c => (c.value === searchedData.country));
        if (venue_country) {
          venue_country_id = venue_country;
          await this.onLoadStateDropDown(venue_country_id, searchedData.state, searchedData.city);
        }
      }
      this.setState({ city: searchedData.city, state: searchedData.state, venueAddress: searchedData.address, venueAddress2: searchedData.address2, country: venue_country_id, zip_code: searchedData.zip_code })
    }

    this.setMyPageLoader(false);
  }

  async setMyPageLoader(bool) {
    if (bool) {
      this.setState({ setLoader: true });
      document.body.classList.add('loader-background');
    }
    else {
      this.setState({ setLoader: false });
      document.body.classList.remove('loader-background');
    }
  }
  async avoidOnchange(e) {
    e.preventDefault();
    return false;
  }

  async eventSearchForStatus(e) {
    this.setMyPageLoader(true);
    let searchEventText = '';
    if (e && e.target && e.target.value) {
      searchEventText = e.target.value
    }
    await this.setState({ eventSearchInputText: searchEventText, showSuggestion: true });
    if (this.state.eventSearchInputText !== "") {
      const getEventFilterdata = await getEventFilter(this.state.eventSearchInputText);
      await this.setState({ eventStatusSearchSuggestion: getEventFilterdata  });
    } else {
      await this.setState({ eventSearchInputText: "", eventStatusSearchSuggestion: []});
    }
    this.setMyPageLoader(false);
  }

  async eventSearch(e) {
    this.setMyPageLoader(true);
    let searchEventEditText = '';
    if (e && e.target.value) {
      searchEventEditText = e.target.value;
    }
    await this.setState({ eventSearchText: searchEventEditText,showSuggestionEdit:true });
    if (this.state.eventSearchText !== "") {
      const getEventFilterdata = await getEventFilter(this.state.eventSearchText);
      await this.setState({ eventSearchSuggestion: getEventFilterdata });
    } else {
      await this.setState({ eventSearchText: "", eventSearchSuggestion: [] });
    }
    this.setMyPageLoader(false);
  }

  async selectEventForEventStatus(eventId, eventfilterdata) {
    if (eventId) {
      var eventIds = '' + eventId;
      this.setMyPageLoader(true);
      const getEventdetailData = await getEventdetail(eventIds);

      var event_status_id = getEventdetailData.data.eventsDtls.event_status_id;
      var eventDetailId = getEventdetailData.data.eventsDtls.id;
      if (event_status_id && event_status_id != "" && event_status_id != null) {
        var eventStatusOption = this.state.allEventStatus.find(status => status.value == event_status_id)
        this.setState({ eventStatusOption, defaultOption: eventStatusOption, selectedEventId: eventDetailId })
      }

      this.setState({ eventStatusSelectedDetailsJson: eventfilterdata, eventSelectedDetailsJson: {} });
      await this.setState({ eventSearchInputText: "", eventStatusSearchSuggestion: [], showSuggestion:false });

      this.setMyPageLoader(false);
    }
  }

  async selectEvent(eventId, eventfilterdata) {
    if (eventId) {
      this.setMyPageLoader(true);
      var eventIds = '' + eventId;
      const getEventdetailData = await getEventdetail(eventIds);
      this.setState({ eventSelectedDetails: true, eventSelectedDetailsJson: eventfilterdata });
      if (getEventdetailData) {

        var eventEditDate = ""; var eventEditDateTime = ""; var eventEditDateDay = ""; var eventTicketSystemId = ""; var eventCountryId = ""; var CategoryId = ""; var genreId = "";
        if (getEventdetailData.data.eventsDtls.date) {
          let dateTime = getEventdetailData.data.venueDtls.timezone ? moment.utc(getEventdetailData.data.eventsDtls.date).tz(getEventdetailData.data.venueDtls.timezone) : moment.utc(getEventdetailData.data.eventsDtls.date);
          eventEditDate = new Date(dateTime.format('YYYY-MM-DD 00:00:00'));
          eventEditDateDay = dateTime.format('dddd');
          eventEditDateTime = dateTime.format('HH:mm:ss');
        }

        await this.state.allTicketingSystem && this.state.allTicketingSystem.length > 0 &&
          this.state.allTicketingSystem.map((item) => {
            if (item.value == getEventdetailData.data.eventsDtls.ticket_system_id) {
              eventTicketSystemId = item;
            }
          })
          await this.state.allCategoryList && this.state.allCategoryList.length > 0 &&
          this.state.allCategoryList.map((item) => {
            if (item.value == getEventdetailData.data.eventsDtls.category_id) {
              CategoryId = item;
            }
          })
          await this.state.allGenreList && this.state.allGenreList.length > 0 &&
          this.state.allGenreList.map((item) => {
            if (item.value == getEventdetailData.data.eventsDtls.genre_id) {
              genreId = item;
            }
          })
        await this.state.countryList && this.state.countryList.length > 0 &&
          this.state.countryList.map((item) => {
            if (item.value == getEventdetailData.data.venueDtls.country) {
              eventCountryId = item;

            }
          })

        if (eventCountryId) {

          await this.handleCountryChangeEdit(eventCountryId, getEventdetailData.data.venueDtls.state, getEventdetailData.data.venueDtls.city, true);
        }

        await this.setState({
          editEventFormInformation: {
            eventId: getEventdetailData.data.eventsDtls.id,
            eventName: getEventdetailData.data.eventsDtls.name,
            eventDate: eventEditDate,
            eventTime: eventEditDateTime,
            eventDay: eventEditDateDay,
            eventStatus: "2",
            venueId: getEventdetailData.data.eventsDtls.venue_id,
            venueName: getEventdetailData.data.venueDtls.venue_name,
            venueAddress: getEventdetailData.data.venueDtls.address,
            venueAddress2: getEventdetailData.data.venueDtls.address2,
            venueCity: this.state.editEventFormInformation.venueCity,
            venueZipcode: getEventdetailData.data.venueDtls.zip_code,
            venueState: this.state.editEventFormInformation.venueState,
            venueCountry: eventCountryId,

            ticketSystemId: eventTicketSystemId,
            category:CategoryId,
            genre:genreId,


            eventLink: getEventdetailData.data.eventsDtls.event_link,
            eventImagelink: getEventdetailData.data.eventsDtls.image
          }
        });
        this.setMyPageLoader(false);
        await this.setState({ eventSearchText: "", eventSearchSuggestion: [],showSuggestionEdit :false, });
      } else {
        alert("Data not found !!!")
      }
    }
  }
  //Event Edit form
  async handleEventEdit(e, statename) {
    let editEventFormInformationDetail = this.state.editEventFormInformation;

    if (statename === editEventStateVariables.eventName) {
      editEventFormInformationDetail.eventName = e.target.value;
    }
    else if (statename === editEventStateVariables.eventDate) {
      editEventFormInformationDetail.eventDate = e;
    }
    else if (statename === editEventStateVariables.eventTime) {
      editEventFormInformationDetail.eventTime = e.target.value;
    }
    else if (statename === editEventStateVariables.venueAddress) {
      editEventFormInformationDetail.venueAddress = e.target.value;
    }
    else if (statename === editEventStateVariables.venueAddress2) {
      editEventFormInformationDetail.venueAddress2 = e.target.value;
    }
    else if (statename === editEventStateVariables.venueCity) {
      editEventFormInformationDetail.venueCity = e;
    }
    else if (statename === editEventStateVariables.venueZipCode) {
      editEventFormInformationDetail.venueZipcode = e.target.value;
    }
    else if (statename === editEventStateVariables.venueState) {

      editEventFormInformationDetail.venueState = e;
      editEventFormInformationDetail.venueCity = '';
      await this.handleStateChangeEdit(e)
    }
    else if (statename === editEventStateVariables.venueCountry) {
      if (e) {
        editEventFormInformationDetail.venueCountry = e;
        editEventFormInformationDetail.venueState = '';
        editEventFormInformationDetail.venueCity = '';
        await this.handleCountryChangeEdit(e);

      } else {
        editEventFormInformationDetail.venueCountry = "";
        editEventFormInformationDetail.venueState = '';
        editEventFormInformationDetail.venueCity = '';
        await this.handleCountryChangeEdit();
      }
    }
    else if (statename === editEventStateVariables.ticketingSystemId) {
      if (e) {
        editEventFormInformationDetail.ticketSystemId = e;
      } else {
        editEventFormInformationDetail.ticketSystemId = "";
      }
    }
    else if (statename === editEventStateVariables.category) {
      if (e) {
        editEventFormInformationDetail.category = e;
      } else {
        editEventFormInformationDetail.category = "";
      }
    }
    else if (statename === editEventStateVariables.genre) {
      if (e) {
        editEventFormInformationDetail.genre = e;
      } else {
        editEventFormInformationDetail.genre = "";
      }
    }
    else if (statename === editEventStateVariables.eventLink) {
      editEventFormInformationDetail.eventLink = e.target.value;
    }

    await this.setState({ ...this.state.editEventFormInformation, editEventFormInformationDetail });
  }

  async resetForm() {

    await this.setState({
      eventName: "",
      eventStatus: "2",
      performer: "",
      opponent: "",
      eventDate: "",
      eventTime: "",
      venueName: "",
      venueAddress: "",
      venueAddress2: "",
      state: "",
      city: "",
      zip_code: "",
      country: "",
      eventLink: "",
      ticketingSystem: "",
      category: "",
      genre: "",
      eventdate_day: "",
      eventStatusOption: "",
      eventStatusSelectedDetailsJson: {},
      enableStatusUpdateModal: true,
      selectedCityDropDown: [],
      selectedStateDropDown: [],
      cityList: [], cityListEdit: [], stateListEdit: [], stateList: [],
      selectedEventId: ""
    });
  }
  handleModal() {
    if (!this.state.enableStatusUpdateModal) {
      this.setState({ eventUpdate: !this.state.eventUpdate })
    }
  }

  async handleEventStatusUpdate() {
    var eventStatusId = this.state.eventStatusOption;
    var selectedEventId = this.state.selectedEventId;

    var response = await updateEventStatus(selectedEventId, eventStatusId);

    if (response && !response.isError) {
      this.setMyPageLoader(false);
      this.setState({ eventUpdate: false })
      await this.setState({ eventStatusOption: "", selectedEventId: "", eventStatusSelectedDetailsJson: {}, enableStatusUpdateModal: true });
    }
    else {
      this.setState({ eventUpdate: false, selectedEventId: "", eventStatusOption: "" })
    }

  }

  handleEditEventImageChange = async (e) => {
    let curDate = new Date();
    let myDate = curDate.getDate();
    let myMonth = curDate.getMonth() + 1;
    let myYear = curDate.getFullYear();
    let myTime = curDate.getTime();

    let myCurDateTime = myDate + "" + myMonth + "" + myYear + "" + myTime;

    let attachmentfile_data = e.target.files[0];
    let attachmentfile_name = attachmentfile_data.name ? (myCurDateTime + "_" + attachmentfile_data.name) : '';

    await this.setState({
      editImageName: attachmentfile_name,
      editImage: attachmentfile_data
    });
  }

  handleImageChange = async (e) => {
    let curDate = new Date();
    let myDate = curDate.getDate();
    let myMonth = curDate.getMonth() + 1;
    let myYear = curDate.getFullYear();
    let myTime = curDate.getTime();

    let myCurDateTime = myDate + "" + myMonth + "" + myYear + "" + myTime;

    let attachmentfile_data = e.target.files[0];
    let attachmentfile_name = attachmentfile_data.name ? (myCurDateTime + "_" + attachmentfile_data.name) : '';

    await this.setState({
      image_name: attachmentfile_name,
      image: attachmentfile_data
    });
  }

  handleEventStatus = async (e) => {
    if (e && this.state.selectedEventId) {
      await this.setState({
        eventStatus: e.value,
        eventStatusOption: e,
        enableStatusUpdateModal: false
      });
    } else {
      await this.setState({
        eventStatusOption: "",
        enableStatusUpdateModal: true
      });
    }
  }

  dropdownFilterOption = ({ label, value, data }, string) => {
    if (this.state.value1 === data) {
      return false;
    } else if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };

  async handleCountryChange(country) {
    if (country) {

      this.setState({
        selectedCountryDropDown: country,
        country: country,
        selectedState: "",
        selectedStateDropDown: [],
        stateList: [], cityList: [],
        selectedCity: "",
        selectedCityDropDown: []
      });

      let states = await getStates(country ? country.value : '');
      if (states) {
        const stateList = [];
        states.length && states.map(items => {
          stateList.push({ label: items.name, value: items.id })
        });
        await this.setState({
          stateList: stateList
        });
      }

    } else {
      this.setState({
        selectedCountryDropDown: country,
        country: "",

        selectedState: "",
        selectedStateDropDown: null,
        stateList: [], cityList: [],
        selectedCity: "",
        selectedCityDropDown: null
      });
    }
  }

  async handleCountryChangeEdit(country, state, city, onload = false) {

    if (country) {


      let states = await getStates(country ? country.value : '');
      if (states) {
        const stateList = [];
        states.length && states.map(items => {
          stateList.push({ label: items.name, value: items.id })
        });
        await this.setState({
          stateListEdit: stateList,
          cityListEdit: []
        });
        var venue_state = this.state.stateListEdit.find(c => (c.value === state));
        let editEventState = this.state.editEventFormInformation;
        editEventState.venueState = venue_state;
        editEventState.venueCountry = country;
        await this.setState({ editEventFormInformation: editEventState });
        if (onload) { await this.onLoadCityDropDownEdit(country.value, state, city) }
      }
    }
    else {
      await this.setState({
        stateListEdit: [],
        cityListEdit: []
      });
    }

  }
  async onLoadCityDropDownEdit(countryId, stateId, cityId) {
    let cities = await getCities(countryId, stateId);
    if (cities) {
      const cityList = [];
      var selectedCity = {};
      cities.length && cities.map(items => {
        if (cityId == items.id) {
          selectedCity = { label: items.name, value: items.id };
        }
        cityList.push({ label: items.name, value: items.id })
      });

      let editEventCity = this.state.editEventFormInformation;
      editEventCity.venueCity = selectedCity
      await this.setState({
        cityListEdit: cityList,
        editEventFormInformation: editEventCity
      });

    }
  }
  async handleStateChangeEdit(state) {

    if (state) {
      this.setState({
        selectedStateDropDown: state,
        selectedState: state.value,
        selectedCity: "",
        selectedCityDropDown: null
      });

      let cities = await getCities(this.state.editEventFormInformation.venueCountry.value, state.value);
      if (cities) {
        const cityList = [];
        cities.length && cities.map(items => {
          cityList.push({ label: items.name, value: items.id })
        });
        await this.setState({
          cityListEdit: cityList
        });
      }
    } else {

      await this.setState({
        selectedStateDropDown: [],
        selectedState: "",
        cityList: [],
        selectedCity: "",
        selectedCityDropDown: []
      });
    }

  }

  async handleStateChange(state) {

    if (state) {
      this.setState({
        selectedStateDropDown: state,
        selectedState: state.value,
        selectedCity: "",
        selectedCityDropDown: null
      });

      let cities = await getCities(this.state.country.value, state.value);
      if (cities) {
        const cityList = [];
        cities.length && cities.map(items => {
          cityList.push({ label: items.name, value: items.id })
        });
        await this.setState({
          cityList: cityList
        });
      }
    } else {

      await this.setState({
        selectedStateDropDown: [],
        selectedState: "",
        cityList: [],
        selectedCity: "",
        selectedCityDropDown: []
      });
    }

  }
  async handleCityChange(city) {
    if (city) {
      this.setState({
        selectedCity: city.value,
        selectedCityDropDown: city
      });
    } else {
      this.setState({
        selectedCityDropDown: [],
        selectedCity: ""
      });
    }
  }


  render() {
    const { suggestions, performerSuggestions, editEventSuggestions } = this.state
    var editEventSearchClass;
    if (this.state.eventSearchSuggestion && this.state.eventSearchSuggestion.data) {
      editEventSearchClass = eventClassName.listOfStatus + " " + eventClassName.searchResultActive;
    } else {
      editEventSearchClass = eventClassName.listOfStatus;
    }

    return this.state.defaultKey && (
      <UserContext.Consumer>
        {context => (

          <div className="container-userprofile  pl-db-50">
            {/* <h1 className="card-heading-userprofile">Events</h1> */}
            <div style={{ minHeight: "80vh", height: "100%" }}>
              <div className="ant-card event-card-item-side ant-card-bordered">
                {/* tab section start */}
              <div className="ant-card-body">
                <Tabs defaultActiveKey={this.state.defaultKey}>
                  <TabPane
                    tab={
                      <span>
                        Event Status
                      </span>
                    }
                    key="1"
                  >

                    <div className="event-section">
                      <div className="card-header">
                        <h3 className="card-title secondary-font-family">Event Status</h3>
                      </div>
                      <div className="event-status-sec event-grid-section">
                        <div className="status-card-section">
                          <label htmlFor="eventStatus" className="form-label-userprofile" style={{ visibility: 'hidden' }}>
                            Event Name
                          </label>
                          <div className="list-event--search">
                            <div className="event-sec-status-search form-group">
                              {this.state.eventStatusSelectedDetails == false ?
                                <>
                                  <div className="search-top-input" style={{ position: 'relative' }}>
                                    <Input
                                      className="search-input stylefont-weight-medium"
                                      style={{ width: "100%" }}
                                      placeholder={("Search Event ID, Event Name, Performer, Venue")}
                                      suffix={<SearchIcon />}
                                      bordered={false}
                                      onChange={(e) => this.eventSearchForStatus(e)}
                                      value={this.state.eventSearchInputText}
                                    />
                                    {this.state.eventSearchInputText &&
                                      <button className="event-clear search-close-icon " onClick={(e) => this.eventSearchForStatus()} >X</button>}
                                  </div>
                                  {this.state.showSuggestion && this.state.eventStatusSearchSuggestion && this.state.eventStatusSearchSuggestion.data && this.state.eventStatusSearchSuggestion.data.length > 0 &&
                                    <ul className={editEventSearchClass}>

                                      {this.state.eventStatusSearchSuggestion && this.state.eventStatusSearchSuggestion.data && this.state.eventStatusSearchSuggestion.data.map((eventfilterdata, eventfilterdatacount) => {
                                        return (
                                          <li className="list-item">
                                            <div className="event--items" onClick={() => this.selectEventForEventStatus(eventfilterdata.id, eventfilterdata)} >
                                              <div className="date--day">
                                                <h5>{eventfilterdata.eventDay}</h5>
                                                <h5>{eventfilterdata.eventDate}</h5>
                                              </div>
                                              <div className="event-title-location">
                                                <h4 className="event-header-txt stylefont-weight-medium">{eventfilterdata.eventName}</h4>
                                                <div className="event-id-location">
                                                  <div className="event-location-text">
                                                    <h5 className="stylefont-weight-medium">
                                                      {(eventfilterdata.venueName ? eventfilterdata.venueName : '')

                                                        + (eventfilterdata.venueAddress ? ", " + eventfilterdata.venueAddress : '')
                                                        + (eventfilterdata.venueAddress2 ? ", " + eventfilterdata.venueAddress2 : '')
                                                        + (eventfilterdata.venueCity ? ', ' + eventfilterdata.venueCity : "")
                                                        + (eventfilterdata.venueState ? ', ' + eventfilterdata.venueState : '')
                                                        + (eventfilterdata.venueCountry ? ', ' + eventfilterdata.venueCountry : '')
                                                      }


                                                    </h5>
                                                  </div>
                                                  <div className="event-id ">
                                                    <h5 className="stylefont-weight-medium">Event ID: {eventfilterdata.id}</h5>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </li>
                                        )
                                      })}
                                    </ul>}
                                </> :
                                <Input
                                  className="search-input stylefont-weight-medium"
                                  style={{ width: "100%" }}
                                  placeholder={("Search Event ID, Event Name, Performer, Venue")}
                                  suffix={<SearchIcon />}
                                  bordered={false}
                                  onChange={(e) => this.eventSearchForStatus(e)}
                                  value={this.state.eventSearchInputText}
                                />
                              }

                            </div>
                          </div>
                        </div>
                        <div className="update-event-status-item">
                          <div className="event-stat-box">
                            <label htmlFor="eventStatus" className="form-label-userprofile">
                              Update Event Status
                            </label>

                            <div className="form-group">
                              <Select
                                {...this.props}
                                placeholder={"Event Status"}
                                style={{ width: "100%" }}
                                name="eventStatus"
                                filterOption={this.dropdownFilterOption}
                                value={this.state.eventStatusOption}
                                options={
                                  this.state.allEventStatus && this.state.allEventStatus.length > 0 ? this.state.allEventStatus : []
                                }
                                onChange={(e) => this.handleEventStatus(e)}
                                allowClear
                                showSearch
                                isClearable
                              />
                              {/* <ErrorMessage
                        name="eventStatus"
                        component="div"
                        className="invalid-feedback"
                      /> */}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="event-status-sec event-grid-section" style={{ alignItems: 'flex-end' }}>
                        <div className="event-status-form-item">
                          <div className="event-stat-box">
                            <label htmlFor="eventStatus" className="form-label-userprofile">
                              Event Information
                            </label>

                            <div className="text-section">
                              {this.state.eventStatusSelectedDetailsJson.eventName == undefined ? <textarea className="form-control"></textarea> :

                                <div className="event-detail-section">
                                  <div className="d-flex">
                                    <h3 className="order-detail-event-title stylefont-weight-bold">{this.state.eventStatusSelectedDetailsJson.eventName ? this.state.eventStatusSelectedDetailsJson.eventName : ''}</h3>

                                  </div>

                                  <h4 className="event-time-date stylefont-weight-bold primary-color">
                                    {/* {this.state.eventStatusSelectedDetailsJson.fullEventDate ? moment.utc(this.state.eventStatusSelectedDetailsJson.fullEventDate).local().format("dddd MMM D, YYYY @ h:mm A ") : ''}{moment && moment.tz.guess() && moment.tz.guess().length && moment.tz(moment.tz.guess()).zoneAbbr().length ? moment.tz(moment.tz.guess()).zoneAbbr() : ''} */}

                                    {this.state.eventStatusSelectedDetailsJson.fullEventDate && this.state.eventStatusSelectedDetailsJson.timezone ? moment.utc(this.state.eventStatusSelectedDetailsJson.fullEventDate).tz(this.state.eventStatusSelectedDetailsJson.timezone).format('dddd - MMM Do YYYY, hh:mm A z') + ' ' : moment.utc(this.state.eventStatusSelectedDetailsJson.fullEventDate).format('dddd MMM D, YYYY @ h:mm A z') + ' '}
                                  </h4>


                                  <h4 className="event-location stylefont-weight-bold bule-green--color">{
                                    (this.state.eventStatusSelectedDetailsJson.venueName ? this.state.eventStatusSelectedDetailsJson.venueName : '')
                                    + (this.state.eventStatusSelectedDetailsJson.venueAddress ? ", " + this.state.eventStatusSelectedDetailsJson.venueAddress : '')
                                    + (this.state.eventStatusSelectedDetailsJson.venueAddress2 ? ", " + this.state.eventStatusSelectedDetailsJson.venueAddress2 : '')
                                    + (this.state.eventStatusSelectedDetailsJson.venueCity ? ", " + this.state.eventStatusSelectedDetailsJson.venueCity : '')
                                    + (this.state.eventStatusSelectedDetailsJson.venueState ? ", " + this.state.eventStatusSelectedDetailsJson.venueState : '')
                                    + (this.state.eventStatusSelectedDetailsJson.venueCountry ? ", " + this.state.eventStatusSelectedDetailsJson.venueCountry : '')}</h4>
                                  <h6 className="event-location stylefont-weight-regular gray-color mb-0">{"Event ID: " + this.state.eventStatusSelectedDetailsJson.id}</h6>
                                </div>}

                            </div>
                          </div>
                        </div>
                        <div className="event-footer-item">
                          <button type="submit" disabled={this.state.enableStatusUpdateModal} className={`event-update-btn stylefont-weight-bold ${this.state.enableStatusUpdateModal ? 'btn-disable-bg' : 'primary-bg-color'}`} onClick={() => this.handleModal()}>
                            Update Event Status
                          </button>
                          <button type="reset" className="btn event-cancel-btn" onClick={this.resetForm} >
                            Cancel
                          </button>
                        </div>
                      </div>
                    </div>
                  </TabPane>
                  <TabPane
                    tab={
                      <span>
                        Add Event
                      </span>
                    }
                    key="2"
                  >

                    <div className="user-event-card-userprofile">

                      <Formik key={'addEvent'}

                        initialValues={{
                          eventName: (this.state.eventName && this.state.eventName.length) ? this.state.eventName : "",
                          eventStatus: (this.state.eventStatus && this.state.eventStatus.length) ? this.state.eventStatus : "",
                          performer: (this.state.performer && this.state.performer.length) ? this.state.performer : "",
                          opponent: (this.state.opponent && this.state.opponent.length) ? this.state.opponent : "",
                          eventDate: (this.state.eventDate) ? this.state.eventDate : "",
                          eventTime: (this.state.eventTime && this.state.eventTime.length) ? this.state.eventTime : "",
                          venueName: (this.state.venueName && this.state.venueName.length) ? this.state.venueName : "",
                          venueAddress: (this.state.venueAddress && this.state.venueAddress.length) ? this.state.venueAddress : "",
                          venueAddress2: (this.state.venueAddress2 && this.state.venueAddress2.length) ? this.state.venueAddress2 : "",
                          state: (this.state.selectedState) ? this.state.selectedState : "",
                          zip_code: (this.state.zip_code && this.state.zip_code.length) ? this.state.zip_code : "",
                          city: (this.state.selectedCity) ? this.state.selectedCity : "",
                          country: (this.state.country && this.state.country !== null) ? this.state.country.value : "",
                          eventLink: (this.state.eventLink && this.state.eventLink.length) ? this.state.eventLink : "",
                          ticketingSystem: (this.state.ticketingSystem && this.state.ticketingSystem !== null) ? this.state.ticketingSystem.value : "",
                          category: (this.state.category && this.state.category !== null) ? this.state.category.value : "",
                          genre: (this.state.genre && this.state.genre !== null) ? this.state.genre.value : "",
                          eventdate_day: (this.state.eventdate_day) ? this.state.eventdate_day : "",
                        }}

                        enableReinitialize={true}

                        validationSchema={Yup.object().shape({

                          eventName: Yup.string()
                            .required("Event Name is required")
                            .min(3, "Event Name should be a minimum of 3 characters"),

                          eventStatus: Yup.string().required("Event Status is required"),

                          //performer: Yup.string().required("Performer Name is required"),

                          eventDate: Yup.string().required("Event Date is required"),

                          eventTime: Yup.string().required("Event Time is required"),

                          venueName: Yup.string().required("Venue Name is required"),

                          venueAddress: Yup.string().required("Venue Address is required"),

                          state: Yup.string().required("State is required"),

                          eventLink: Yup.string()
                            .matches(
                              /(https?):\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
                              'Enter valid url!'
                            )
                            .required('Please valid event link'),

                          city: Yup.string().required("City is required"),
                          country: Yup.number().required("Country is required").typeError('A Country is required'),
                          ticketingSystem: Yup.string().required("Ticketing System is required"),
                          category: Yup.string().required("Category is required"),
                          genre: Yup.string().required("Genre is required"),

                          zip_code: Yup.string()
                            .required("Zip Code is required")
                            .matches(/^[a-zA-Z0-9]+$/, "Zip Code must be only alphanumerics")
                            .min(3, 'Zip Code should be a minimum of 3 alphanumerics')
                            .max(8, 'Zip Code should be a maximum of 8 alphanumerics'),

                        })}

                        onSubmit={async (fields) => {
                          var getTimeZone = fields.eventTime.split(':');
                          if (getTimeZone.length) {
                            var date = moment(fields.eventDate, "hh:mm:ss A")
                              .add(getTimeZone[0], 'hours')
                              .add(getTimeZone[1], 'minutes')
                              .add(getTimeZone[2], 'seconds');
                            fields.eventDate = date.format('YYYY-MM-DD');

                          }
                          fields.cityName = this.state.selectedCityDropDown.label;
                          fields.stateName = this.state.selectedStateDropDown.label;

                          await this.setState({
                            closeButtonModal: true,
                            datafields: fields,
                            modalPopupMode: "addEvent",
                            userdetails: context.user
                          });
                        }}

                        render={({ errors, status, touched }) => (
                          <>
                            <div className="card-header">
                              <h3 className="card-title secondary-font-family">Add Event</h3>
                            </div>
                            <Form autoComplete="off" >
                              <div className=" event-status-sec event-grid-section" >
                                <div>
                                  <label htmlFor="eventName" className="form-label-userprofile">
                                    Event Name
                                  </label>
                                  <div className="form-group">
                                    <Field
                                      name="eventName"
                                      className={
                                        "formik-input-user-form form-control stylefont-weight-medium " +
                                        (errors.eventName && touched.eventName
                                          ? " is-invalid"
                                          : "")
                                      }
                                      placeholder="Event Name"
                                      value={this.state.eventName}
                                      onChange={(e) => this.setState({ eventName: e.target.value })}
                                    />
                                    <ErrorMessage
                                      name="eventName"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                </div>
                                <div>
                                  <label htmlFor="venueName" className="form-label-userprofile">
                                    Venue Name
                                  </label>

                                  <div className="form-group">
                                    <Field
                                      name="venueName"
                                      className={
                                        "formik-input-user-form form-control stylefont-weight-medium" +
                                        (errors.venueName && touched.venueName
                                          ? " is-invalid"
                                          : "")
                                      }
                                      placeholder="Venue Name"
                                      value={this.state.venueName}
                                      //onChange={(e) => this.setState({venueName: e.target.value})}
                                      onChange={(e) => this.listSearchChange(e, "venue")}
                                    />
                                    {suggestions && suggestions.length > 0 &&
                                      < div className="TypeAheadDropDown">
                                        {suggestions && this.state.locationText == '' &&
                                          this.renderSuggestions()
                                        }
                                      </div>
                                    }
                                    <ErrorMessage
                                      name="venueName"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                </div>
                                <div>
                                  <label htmlFor="eventDate" className="form-label-userprofile">
                                    Event Date
                                  </label>

                                  <div className="form-group">
                                    <div className="date--PickQr custom-date-calendar-field">
                                      <span className="custom-calendar-img"><img src={iconInhand} /></span>
                                      <DatePicker
                                        name="eventDate"
                                        className={
                                          "formik-input-user-form form-control stylefont-weight-medium singleDate" +
                                          (errors.eventDate && touched.eventDate
                                            ? " is-invalid"
                                            : "")
                                        }
                                        onKeyDown={(e) => this.avoidOnchange(e)}
                                        dateFormat="dd-MM-yyyy"
                                        highlightDates={new Date()}
                                        placeholderText=" DD/MM/YYYY"
                                        date={this.state.eventDate}
                                        onChange={date => this.setState({ eventDate: new Date(date), eventdate_day: new Date(date).getDay() })}
                                        selected={this.state.eventDate}
                                      />
                                      <ErrorMessage
                                        name="eventDate"
                                        component="div"
                                        className="invalid-feedback"
                                        style={{ display: "block" }}
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div>
                                  <label
                                    htmlFor="venueAddress"
                                    className="form-label-userprofile"
                                  >
                                    Venue Address
                                  </label>

                                  <div className="form-group">
                                    <Field
                                      name="venueAddress" autoComplete="off"
                                      className={
                                        "formik-input-user-form form-control stylefont-weight-medium" +
                                        (errors.venueAddress && touched.venueAddress
                                          ? " is-invalid"
                                          : "")
                                      }
                                      placeholder="Venue Address"
                                      value={this.state.venueAddress}
                                      onChange={(e) => this.setState({ venueAddress: e.target.value })}
                                    />
                                    <ErrorMessage
                                      name="venueAddress"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                </div>
                                <div>
                                  <label htmlFor="eventTime" className="form-label-userprofile">
                                    Event Time
                                  </label>

                                  <div className="form-group">
                                    <div className="date--PickQr custom-date-calendar-field">
                                      <span className="custom-calendar-img time-picker-item"><img src={iconTime} /></span>
                                      <Field
                                        type="time"
                                        step="2"
                                        name="eventTime"
                                        className={
                                          "formik-input-user-form form-control stylefont-weight-medium" +
                                          (errors.eventTime && touched.eventTime
                                            ? " is-invalid"
                                            : "")
                                        }
                                        placeholder="Event Time"
                                        value={this.state.eventTime}
                                        onChange={(e) => this.setState({ eventTime: e.target.value })}
                                      />
                                      <ErrorMessage
                                        name="eventTime"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div>
                                  <label
                                    htmlFor="venueAddress2"
                                    className="form-label-userprofile"
                                  >
                                    Venue Address2
                                  </label>

                                  <div className="form-group">
                                    <Field
                                      name="venueAddress2"
                                      className={
                                        "formik-input-user-form form-control stylefont-weight-medium" +
                                        (errors.venueAddress2 && touched.venueAddress2
                                          ? " is-invalid"
                                          : "")
                                      }
                                      placeholder="Venue Address2"
                                      value={this.state.venueAddress2}
                                      onChange={(e) => this.setState({ venueAddress2: e.target.value })}
                                    />
                                    <ErrorMessage
                                      name="venueAddress2"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                </div>
                                <div>
                                  <label
                                    htmlFor="ticketingSystem"
                                    className="form-label-userprofile"
                                  >
                                    Ticketing System
                                  </label>

                                  <div className="form-group ticket-system-item">
                                    <Select
                                      {...this.props}
                                      placeholder={"Select Ticketing System"}
                                      style={{ width: "100%" }}
                                      name="ticketingSystem"
                                      filterOption={this.dropdownFilterOption}
                                      value={this.state.ticketingSystem}
                                      options={
                                        this.state.allTicketingSystem && this.state.allTicketingSystem.length > 0 ? this.state.allTicketingSystem : []
                                      }
                                      className={(errors.ticketingSystem && touched.ticketingSystem ? " is-invalid" : "")
                                      }
                                      onChange={(e) => {
                                        e && e !== null ? this.setState({ ticketingSystem: e }) :
                                          this.setState({ ticketingSystem: '' })
                                      }

                                      }
                                      allowClear
                                      showSearch
                                      isClearable
                                    />
                                    <ErrorMessage
                                      name="ticketingSystem"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                </div>
                                <div>
                                  <label
                                    htmlFor="category"
                                    className="form-label-userprofile"
                                  >
                                    Category
                                  </label>

                                  <div className="form-group ticket-system-item">
                                    <Select
                                      {...this.props}
                                      placeholder={"Select Category"}
                                      style={{ width: "100%" }}
                                      name="category"
                                      filterOption={this.dropdownFilterOption}
                                      value={this.state.category}
                                      options={
                                        this.state.allCategoryList && this.state.allCategoryList.length > 0 ? this.state.allCategoryList : []
                                      }
                                      className={(errors.category && touched.category ? " is-invalid" : "")
                                      }
                                      onChange={(e) => {
                                        e && e !== null ? this.setState({ category: e }) :
                                          this.setState({ category: '' })
                                      }

                                      }
                                      allowClear
                                      showSearch
                                      isClearable
                                    />
                                    <ErrorMessage
                                      name="category"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                </div>
                                <div>
                                  <label
                                    htmlFor="ticketingSystem"
                                    className="form-label-userprofile"
                                  >
                                    Genre
                                  </label>

                                  <div className="form-group ticket-system-item">
                                    <Select
                                      {...this.props}
                                      placeholder={"Select Genre"}
                                      style={{ width: "100%" }}
                                      name="genre"
                                      filterOption={this.dropdownFilterOption}
                                      value={this.state.genre}
                                      options={
                                        this.state.allGenreList && this.state.allGenreList.length > 0 ? this.state.allGenreList : []
                                      }
                                      className={(errors.genre && touched.genre ? " is-invalid" : "")
                                      }
                                      onChange={(e) => {
                                        e && e !== null ? this.setState({ genre: e }) :
                                          this.setState({ genre: '' })
                                      }

                                      }
                                      allowClear
                                      showSearch
                                      isClearable
                                    />
                                    <ErrorMessage
                                      name="genre"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                </div>
                                <div className="event-divide--grid--section">
                                  <div>
                                    <label htmlFor="country" className="form-label-userprofile">
                                      {this.props.t("userInfo.address.country")}
                                    </label>
                                    <div className="form-group ticket-system-item">
                                      <Select
                                        {...this.props}
                                        placeholder={"Select Country"}
                                        style={{ width: "100%" }}
                                        name="country"
                                        filterOption={this.dropdownFilterOption}
                                        value={this.state.country}
                                        options={this.state.countryList && this.state.countryList.length > 0 ? this.state.countryList : []}
                                        className={(errors.country && touched.country ? " is-invalid" : "")}

                                        onChange={e => this.handleCountryChange(e)}
                                        allowClear
                                        showSearch
                                        isClearable
                                      />
                                      <ErrorMessage
                                        name="country"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                  <div>
                                    <label htmlFor="state" className="form-label-userprofile">
                                      {this.props.t("userInfo.address.state")}
                                    </label>
                                    <div className="form-group ticket-system-item">

                                      <Select

                                        placeholder={this.props.t("userInfo.address.state")}
                                        style={{ width: "100%" }}
                                        name="state" autoComplete="off"
                                        filterOption={this.dropdownFilterOption}
                                        value={this.state.selectedStateDropDown ? this.state.selectedStateDropDown : ""}
                                        options={this.state.stateList}
                                        onChange={e => this.handleStateChange(e)}
                                        className={(errors.state && touched.state ? ' is-invalid' : '')}
                                        allowClear
                                        showSearch
                                        isClearable
                                      />
                                      <ErrorMessage
                                        name="state"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div>
                                  <label htmlFor="eventLink" className="form-label-userprofile">
                                    Event Link
                                  </label>

                                  <div className="form-group">
                                    <Field
                                      name="eventLink"
                                      className={
                                        "formik-input-user-form form-control stylefont-weight-medium" +
                                        (errors.eventLink && touched.eventLink
                                          ? " is-invalid"
                                          : "")
                                      }
                                      placeholder="Event Link"
                                      value={this.state.eventLink}
                                      onChange={(e) => this.setState({ eventLink: e.target.value })}
                                    />
                                    <ErrorMessage
                                      name="eventLink"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                </div>
                                <div className="event-divide--grid--section">
                                  <div>
                                    <label htmlFor="city" className="form-label-userprofile">
                                      {this.props.t("userInfo.address.city")}
                                    </label>
                                    <div className="form-group country-select-item">
                                      <Select

                                        placeholder={this.props.t("userInfo.address.city")}
                                        style={{ width: "100%" }}
                                        name="city" autoComplete="off"
                                        filterOption={this.dropdownFilterOption}
                                        value={this.state.selectedCityDropDown ? this.state.selectedCityDropDown : ""}
                                        options={this.state.cityList}
                                        onChange={e => this.handleCityChange(e)}
                                        className={(errors.city && touched.city ? ' is-invalid' : '')}
                                        allowClear
                                        showSearch
                                        isClearable
                                      />
                                      <ErrorMessage
                                        name="city"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                  <div>
                                    <label htmlFor="zip_code" className="form-label-userprofile">
                                      {this.props.t("userInfo.address.zip")}
                                    </label>
                                    <div className="form-group">
                                      <Field
                                        name="zipcode"
                                        className={
                                          "formik-input-user-form form-control stylefont-weight-medium" +
                                          (errors.zip_code && touched.zip_code ? " is-invalid" : "")
                                        }
                                        placeholder="Zip Code"
                                        value={this.state.zip_code}
                                        onChange={(e) => this.setState({ zip_code: e.target.value })}
                                      />
                                      <ErrorMessage
                                        name="zip_code"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div>
                                  <label htmlFor="image" className="form-label-userprofile">
                                    Event Image
                                  </label>
                                  <div className="form-group">
                                    <Field
                                      type="file"
                                      name="image"
                                      className="formik-input-user-form form-control stylefont-weight-medium"
                                      placeholder="Image"
                                      onChange={(e) => this.handleImageChange(e)}
                                      accept="image/*"
                                    />
                                    <ErrorMessage
                                      name="zip_code"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="event-footer-item">
                                <button type="submit" className="event-submit-btn primary-bg-color stylefont-weight-medium">
                                  Submit Event
                                </button>
                              </div>

                            </Form>
                          </>
                        )}
                      />

                    </div>
                  </TabPane>

                  {/* Edit Event Form Start */}

                  <TabPane
                    tab={
                      <span>
                        Edit Event
                      </span>
                    }
                    key="3"
                  >
                    <div className="edit-event-section">
                      <div className="user-event-card-userprofile">

                        <Formik key={'editEvent'}
                          initialValues={{
                            eventId: this.state.editEventFormInformation.eventId,
                            eventName: this.state.editEventFormInformation.eventName,
                            eventDate: this.state.editEventFormInformation.eventDate,
                            eventTime: this.state.editEventFormInformation.eventTime,
                            eventDay: this.state.editEventFormInformation.eventDay,
                            eventStatus: this.state.editEventFormInformation.eventStatus,
                            venueId: this.state.editEventFormInformation.venueId,
                            venueName: this.state.editEventFormInformation.venueName,
                            venueAddress: this.state.editEventFormInformation.venueAddress,
                            venueAddress2: this.state.editEventFormInformation.venueAddress2,
                            venueCity: this.state.editEventFormInformation.venueCity ? this.state.editEventFormInformation.venueCity.value : '',
                            venueZipcode: this.state.editEventFormInformation.venueZipcode,
                            venueState: this.state.editEventFormInformation.venueState ? this.state.editEventFormInformation.venueState.value : "",
                            venueCountry: this.state.editEventFormInformation.venueCountry && this.state.editEventFormInformation.venueCountry !== null ? this.state.editEventFormInformation.venueCountry.value : "",
                            ticketSystemId: this.state.editEventFormInformation.ticketSystemId && this.state.editEventFormInformation.ticketSystemId !== null ? this.state.editEventFormInformation.ticketSystemId.value : "",
                            category: this.state.editEventFormInformation.category && this.state.editEventFormInformation.category !== null ? this.state.editEventFormInformation.category.value : "",
                            genre: this.state.editEventFormInformation.genre && this.state.editEventFormInformation.genre !== null ? this.state.editEventFormInformation.genre.value : "",
                            eventLink: this.state.editEventFormInformation.eventLink != '' ? this.state.editEventFormInformation.eventLink : '',
                            eventImagelink: this.state.editEventFormInformation.eventImagelink,
                          }}

                          enableReinitialize={true}

                          validationSchema={Yup.object().shape({
                            eventName: Yup.string()
                              .required("Event Name is required")
                              .min(3, "Event Name should be a minimum of 3 characters"),
                            eventStatus: Yup.string().required("Event Status is required"),
                            eventDate: Yup.string().required("Event Date is required"),
                            eventTime: Yup.string().required("Event Time is required"),
                            venueName: Yup.string().required("Venue Name is required"),
                            venueAddress: Yup.string().required("Venue Address is required"),
                            venueCity: Yup.string().required("City is required"),
                            venueState: Yup.string().required("State is required"),
                            venueCountry: Yup.string().required("Country is required").typeError('A Country is required'),
                            ticketSystemId: Yup.string().required("Ticketing System is required").nullable(),
                            category: Yup.string().required("Category is required").nullable(),
                            genre: Yup.string().required("Genre is required").nullable(),
                            venueZipcode: Yup.string()
                              .required("Zip Code is required")
                              .matches(/^[a-zA-Z0-9]+$/, "Zip Code must be only alphanumerics")
                              .min(3, 'Zip Code should be a minimum of 3 alphanumerics')
                              .max(8, 'Zip Code should be a maximum of 8 alphanumerics'),
                            eventLink: Yup.string()
                              .matches(
                                /(https?):\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
                                'Enter correct url!'
                              )
                              .required('Please valid event link').nullable(),

                          })}

                          onSubmit={async (fields) => {
                            var getTimeZone = fields.eventTime.split(':');
                            if (getTimeZone.length) {
                              var date = moment(fields.eventDate, "hh:mm:ss A")
                                .add(getTimeZone[0], 'hours')
                                .add(getTimeZone[1], 'minutes')
                                .add(getTimeZone[2], 'seconds');
                              fields.eventDate = date.format('YYYY-MM-DD');
                            }
                            fields.stateName = this.state.editEventFormInformation.venueCity.label;
                            fields.cityName = this.state.editEventFormInformation.venueState.label;
                            await this.setState({
                              closeButtonModal: true,
                              datafields: fields,
                              modalPopupMode: "editEvent",
                              userdetails: context.user
                            });
                          }}

                          render={({ errors, values, status, touched }) => (
                            <>
                              <div className="card-header">
                                <h3 className="card-title secondary-font-family">Edit Event</h3>
                              </div>
                              {/* Event Search start */}
                              <div className="event-sec-status-search form-group">
                                <div className="status-card-section">
                                  <div className="list-event--search">
                                    <div className="event-sec-status-search form-group">
                                      {this.state.eventSelectedDetails == false ?
                                        <>
                                          <div className="search-top-input" style={{ position: 'relative' }}>
                                            <Input
                                              className="search-input stylefont-weight-medium"
                                              style={{ width: "100%" }}
                                              placeholder={("Search Event ID, Event Name, Performer, Venue")}
                                              suffix={<SearchIcon />}
                                              bordered={true}
                                              onChange={this.eventSearch}
                                              value={this.state.eventSearchText}

                                            // value={this.state.eventSearchText}
                                            />
                                            {this.state.eventSearchText &&
                                              <button className="event-clear search-close-icon " onClick={(e) => this.eventSearch()} >X</button>}
                                          </div>
                                          { this.state.showSuggestionEdit && this.state.eventSearchSuggestion && this.state.eventSearchSuggestion.data && this.state.eventSearchSuggestion.data.length > 0 &&
                                            <ul className={editEventSearchClass}>

                                              {this.state.eventSearchSuggestion && this.state.eventSearchSuggestion.data && this.state.eventSearchSuggestion.data.map((eventfilterdata, eventfilterdatacount) => {
                                                return (
                                                  <li className="list-item edit-event-list-section">
                                                    <div className="event--items" onClick={() => this.selectEvent(eventfilterdata.id, eventfilterdata)} >
                                                      <div className="date--day">
                                                        <h5>{eventfilterdata.eventDay}</h5>
                                                        <h5>{eventfilterdata.eventDate}</h5>
                                                      </div>
                                                      <div className="event-title-location">
                                                        <h4 className="event-header-txt stylefont-weight-medium">{eventfilterdata.eventName}</h4>
                                                        <div className="event-id-location">
                                                          <div className="event-location-text">
                                                            <h5 className="stylefont-weight-medium">
                                                              {eventfilterdata.venueName + ", " + eventfilterdata.venueAddress + " " + eventfilterdata.venueAddress2 + ", " + eventfilterdata.venueCity + ", " + eventfilterdata.venueState + ", " + eventfilterdata.venueCountry}
                                                            </h5>
                                                          </div>
                                                          <div className="event-id">
                                                            <h5 className="stylefont-weight-medium">Event ID: {eventfilterdata.id}</h5>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </li>
                                                )
                                              })}
                                            </ul>}
                                        </> :
                                        <div className="event--items event-card-item-side" onClick={(e) => this.setState({ eventSelectedDetails: false })} >
                                          <span className="ant-input-suffix">
                                            <SearchIcon />
                                          </span>
                                          <div className="event--items" style={{ width: 'calc(100% - 60px)' }}>
                                            <div className="date--day">
                                              <h5>{this.state.eventSelectedDetailsJson.eventDay}</h5>
                                              <h5>{this.state.eventSelectedDetailsJson.eventDate}</h5>
                                            </div>
                                            <div className="event-title-location">
                                              <h4 className="event-header-txt stylefont-weight-medium">{this.state.eventSelectedDetailsJson.eventName}</h4>
                                              <div className="event-id-location">
                                                <div className="event-location-text">
                                                  <h5 className="stylefont-weight-medium">
                                                    {this.state.eventSelectedDetailsJson.venueName + ", " + this.state.eventSelectedDetailsJson.venueAddress + " " + this.state.eventSelectedDetailsJson.venueAddress2 + ", " + this.state.eventSelectedDetailsJson.venueCity + ", " + this.state.eventSelectedDetailsJson.venueState + ", " + this.state.eventSelectedDetailsJson.venueCountry}
                                                  </h5>
                                                </div>
                                                <div className="event-id">
                                                  <h5 className="stylefont-weight-medium">Event ID: {this.state.eventSelectedDetailsJson.id}</h5>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      }

                                    </div>
                                  </div>
                                </div>
                              </div>

                              {/* Event Search end */}


                              <Form autoComplete="off" >
                                <div className="event-status-sec event-grid-section" >
                                  <div>
                                    <label htmlFor="eventName" className="form-label-userprofile">
                                      Event Name
                                    </label>
                                    <div className="form-group">
                                      <Field
                                        name="eventName"
                                        className={
                                          "formik-input-user-form form-control stylefont-weight-medium" +
                                          (errors.eventName && touched.eventName
                                            ? " is-invalid"
                                            : "")
                                        }
                                        placeholder="Event Name"
                                        value={values.eventName ? values.eventName : ''}
                                        onChange={(e) => this.handleEventEdit(e, "eventName")}
                                      />
                                      <ErrorMessage
                                        name="eventName"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                  <div>
                                    <label htmlFor="venueName" className="form-label-userprofile">
                                      Venue Name
                                    </label>

                                    <div className="form-group">
                                      <Field
                                        name="venueName"
                                        className={
                                          "formik-input-user-form form-control stylefont-weight-medium" +
                                          (errors.venueName && touched.venueName
                                            ? " is-invalid"
                                            : "")
                                        }
                                        placeholder="Venue Name"
                                        value={values.venueName ? values.venueName : ""}
                                        //onChange={(e) => this.setState({venueName: e.target.value})}
                                        onChange={(e) => this.listSearchChange(e, "venueName")}
                                      />
                                      {editEventSuggestions && editEventSuggestions.length > 0 &&
                                        < div className="TypeAheadDropDown">
                                          {editEventSuggestions && this.state.locationText == '' &&
                                            this.renderEditEventSuggestions()
                                          }
                                        </div>
                                      }
                                      <ErrorMessage
                                        name="venueName"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                  <div>
                                    <label htmlFor="eventDate" className="form-label-userprofile">
                                      Event Date
                                    </label>

                                    <div className="form-group">
                                      <div className="date--PickQr custom-date-calendar-field">
                                        <span className="custom-calendar-img"><img src={iconInhand} /></span>
                                        <DatePicker
                                          name="eventDate"
                                          className={
                                            "formik-input-user-form form-control stylefont-weight-medium singleDate" +
                                            (errors.eventDate && touched.eventDate
                                              ? " is-invalid"
                                              : "")
                                          }
                                          onKeyDown={(e) => this.avoidOnchange(e)}
                                          dateFormat="dd-MM-yyyy"
                                          highlightDates={new Date()}
                                          placeholderText=" DD/MM/YYYY"
                                          date={this.state.editEventFormInformation.eventDate}
                                          onChange={(e) => this.handleEventEdit(e, "eventDate")}
                                          selected={this.state.editEventFormInformation.eventDate}
                                        />
                                        <ErrorMessage
                                          name="eventDate"
                                          component="div"
                                          className="invalid-feedback"
                                          style={{ display: "block" }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div>
                                    <label
                                      htmlFor="venueAddress"
                                      className="form-label-userprofile"
                                    >
                                      Venue Address
                                    </label>

                                    <div className="form-group">
                                      <Field
                                        name="venueAddress"
                                        className={
                                          "formik-input-user-form form-control stylefont-weight-medium" +
                                          (errors.venueAddress && touched.venueAddress
                                            ? " is-invalid"
                                            : "")
                                        }
                                        placeholder="Venue Address"
                                        value={values.venueAddress ? values.venueAddress : ''}
                                        onChange={(e) => this.handleEventEdit(e, "venueAddress")}
                                      />
                                      <ErrorMessage
                                        name="venueAddress"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                  <div>
                                    <label htmlFor="eventTime" className="form-label-userprofile">
                                      Event Time
                                    </label>

                                    <div className="form-group">
                                      <div className="date--PickQr custom-date-calendar-field">
                                        <span className="custom-calendar-img time-picker-item"><img src={iconTime} /></span>
                                        <Field
                                          type="time"
                                          step="2"
                                          name="eventTime"
                                          className={
                                            "formik-input-user-form form-control stylefont-weight-medium" +
                                            (errors.eventTime && touched.eventTime
                                              ? " is-invalid"
                                              : "")
                                          }
                                          placeholder="Event Time"
                                          value={values.eventTime ? values.eventTime : ''}
                                          onChange={(e) => this.handleEventEdit(e, "eventTime")}
                                        />
                                        <ErrorMessage
                                          name="eventTime"
                                          component="div"
                                          className="invalid-feedback"
                                        />
                                      </div>
                                    </div>
                                  </div>



                                  <div>
                                    <label
                                      htmlFor="venueAddress2"
                                      className="form-label-userprofile"
                                    >
                                      Venue Address2
                                    </label>

                                    <div className="form-group">
                                      <Field
                                        name="venueAddress2"
                                        className={
                                          "formik-input-user-form form-control stylefont-weight-medium" +
                                          (errors.venueAddress2 && touched.venueAddress2
                                            ? " is-invalid"
                                            : "")
                                        }
                                        placeholder="Venue Address2"
                                        value={values.venueAddress2 ? values.venueAddress2 : ''}
                                        onChange={(e) => this.handleEventEdit(e, "venueAddress2")}
                                      />
                                      <ErrorMessage
                                        name="venueAddress2"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                  <div>
                                    <label
                                      htmlFor="ticketSystemId"
                                      className="form-label-userprofile"
                                    >
                                      Ticketing System
                                    </label>

                                    <div className="form-group ticket-system-item">
                                      <Select
                                        {...this.props}
                                        placeholder={"Select Ticketing System"}
                                        style={{ width: "100%" }}
                                        name="ticketSystemId"
                                        filterOption={this.dropdownFilterOption}
                                        value={this.state.editEventFormInformation.ticketSystemId ? this.state.editEventFormInformation.ticketSystemId : ''}
                                        options={
                                          this.state.allTicketingSystem && this.state.allTicketingSystem.length > 0 ? this.state.allTicketingSystem : []
                                        }
                                        className={(errors.ticketSystemId && touched.ticketSystemId ? " is-invalid" : "")
                                        }
                                        onChange={(e) => this.handleEventEdit(e, "ticketSystemId")}
                                        allowClear
                                        showSearch
                                        isClearable
                                      />
                                      <ErrorMessage
                                        name="ticketSystemId"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>

                                  <div>
                                  <label
                                    htmlFor="category"
                                    className="form-label-userprofile"
                                  >
                                    Category
                                  </label>

                                  <div className="form-group ticket-system-item">
                                    <Select
                                      {...this.props}
                                      placeholder={"Select Category"}
                                      style={{ width: "100%" }}
                                      name="category"
                                      filterOption={this.dropdownFilterOption}
                                      value={this.state.editEventFormInformation.category}
                                      options={
                                        this.state.allCategoryList && this.state.allCategoryList.length > 0 ? this.state.allCategoryList : []
                                      }
                                      className={(errors.category && touched.category ? " is-invalid" : "")
                                      }
                                      onChange={(e) => this.handleEventEdit(e, "category")}
                                      allowClear
                                      showSearch
                                      isClearable
                                    />
                                    <ErrorMessage
                                      name="category"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                </div>
                                <div>
                                  <label
                                    htmlFor="ticketingSystem"
                                    className="form-label-userprofile"
                                  >
                                    Genre
                                  </label>

                                  <div className="form-group ticket-system-item">
                                    <Select
                                      {...this.props}
                                      placeholder={"Select Genre"}
                                      style={{ width: "100%" }}
                                      name="genre"
                                      filterOption={this.dropdownFilterOption}
                                      value={this.state.editEventFormInformation.genre}
                                      options={
                                        this.state.allGenreList && this.state.allGenreList.length > 0 ? this.state.allGenreList : []
                                      }
                                      className={(errors.genre && touched.genre ? " is-invalid" : "")
                                      }
                                      onChange={(e) => this.handleEventEdit(e, "genre")}
                                      allowClear
                                      showSearch
                                      isClearable
                                    />
                                    <ErrorMessage
                                      name="genre"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                </div>
                                <div className="event-divide--grid--section" >
                                  <div>
                                    <label htmlFor="venueCountry" className="form-label-userprofile">
                                      {this.props.t("userInfo.address.country")}
                                    </label>
                                    <div className="form-group ticket-system-item">

                                      <Select
                                        {...this.props}
                                        placeholder={"Select Country"}
                                        style={{ width: "100%" }}
                                        name="venueCountry"
                                        filterOption={this.dropdownFilterOption}
                                        value={this.state.editEventFormInformation.venueCountry ? this.state.editEventFormInformation.venueCountry : ''}
                                        options={this.state.countryList && this.state.countryList.length > 0 ? this.state.countryList : []}
                                        className={(errors.venueCountry && touched.venueCountry ? " is-invalid" : "")}
                                        onChange={(e) => this.handleEventEdit(e, "venueCountry")}
                                        allowClear
                                        showSearch
                                        isClearable
                                      />

                                      <ErrorMessage
                                        name="venueCountry"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                  <div>
                                    <label htmlFor="venueState" className="form-label-userprofile">
                                      {this.props.t("userInfo.address.state")}
                                    </label>
                                    <div className="form-group ticket-system-item">


                                      <Select
                                        placeholder={this.props.t("userInfo.address.state")}
                                        style={{ width: "100%" }}
                                        name="state" autoComplete="off"
                                        filterOption={this.dropdownFilterOption}
                                        value={this.state.editEventFormInformation.venueState ? this.state.editEventFormInformation.venueState : ""}

                                        options={this.state.stateListEdit}
                                        onChange={e => this.handleEventEdit(e, "venueState")}
                                        className={(errors.venueState && touched.venueState ? ' is-invalid' : '')}
                                        allowClear
                                        showSearch
                                        isClearable
                                      />
                                      <ErrorMessage
                                        name="venueState"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                </div>
                                  <div>
                                    <label htmlFor="eventLink" className="form-label-userprofile">
                                      Event Link
                                    </label>

                                    <div className="form-group">
                                      <Field
                                        name="eventLink"
                                        className={
                                          "formik-input-user-form form-control stylefont-weight-medium" +
                                          (errors.eventLink && touched.eventLink
                                            ? " is-invalid"
                                            : "")
                                        }
                                        placeholder="Event Link"
                                        value={this.state.editEventFormInformation.eventLink ? this.state.editEventFormInformation.eventLink : ''}
                                        onChange={(e) => this.handleEventEdit(e, "eventLink")}
                                      />
                                      <ErrorMessage
                                        name="eventLink"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                <div className="event-divide--grid--section" >
                                  <div>
                                    <label htmlFor="venueCity" className="form-label-userprofile">
                                      {this.props.t("userInfo.address.city")}
                                    </label>
                                    <div className="form-group country-select-item">

                                      <Select
                                        placeholder={this.props.t("userInfo.address.city")}
                                        style={{ width: "100%" }}
                                        name="city" autoComplete="off"
                                        filterOption={this.dropdownFilterOption}
                                        value={this.state.editEventFormInformation.venueCity ? this.state.editEventFormInformation.venueCity : ""}
                                        options={this.state.cityListEdit ? this.state.cityListEdit : []}
                                        onChange={e => this.handleEventEdit(e, "venueCity")}
                                        className={(errors.venueCity && touched.venueCity ? ' is-invalid' : '')}
                                        allowClear
                                        showSearch
                                        isClearable
                                      />
                                      <ErrorMessage
                                        name="venueCity"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                  <div>
                                    <label htmlFor="venueZipcode" className="form-label-userprofile">
                                      {this.props.t("userInfo.address.zip")}
                                    </label>
                                    <div className="form-group">
                                      <Field
                                        name="venueZipcode"
                                        className={
                                          "formik-input-user-form form-control stylefont-weight-medium" +
                                          (errors.venueZipcode && touched.venueZipcode ? " is-invalid" : "")
                                        }
                                        placeholder="Zip Code"
                                        value={this.state.editEventFormInformation.venueZipcode ? this.state.editEventFormInformation.venueZipcode : ''}
                                        onChange={(e) => this.handleEventEdit(e, "venueZipcode")}
                                      />
                                      <ErrorMessage
                                        name="venueZipcode"
                                        component="div"
                                        className="invalid-feedback"
                                      />
                                    </div>
                                  </div>
                                </div>
                                  <div>
                                    <label htmlFor="image" className="form-label-userprofile">
                                      Event Image
                                    </label>
                                    <div className="form-group">
                                      <Field
                                        type="file"
                                        name="image"
                                        className="formik-input-user-form form-control stylefont-weight-medium"
                                        placeholder="Image"
                                        onChange={(e) => this.handleEditEventImageChange(e)}
                                        accept="image/*"
                                      />
                                      {this.state.editEventFormInformation.eventImagelink && this.state.editEventFormInformation.eventId ?
                                        <div><br /> <button type="button" onClick={(e) => this.handleViewEventAttachment(e)} className="view-down-load-btn" ><AttachmentIcon /> View </button></div>
                                        : ''}
                                    </div>
                                  </div>
                                </div>
                                <div className="event-footer-item">
                                  <button type="submit" className="event-submit-btn primary-bg-color stylefont-weight-medium">
                                    Update Event
                                  </button>
                                </div>

                              </Form>
                            </>
                          )}
                        />

                      </div>
                    </div>
                  </TabPane>
                  {/* Edit Event Form End */}
                </Tabs>
                </div>
              </div>
            </div>
            {/* tab section end */}
            <div className="small-container-userprofile">
            </div>
            {/* modal for event update */}
            <Modal className="update-event-modal" show={this.state.eventUpdate} onHide={() => this.handleModal()} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
              <Modal.Header closeButton> <h5 className="modal-title">Do you want to propose a status <br></br>update to this event?</h5></Modal.Header>
              <Modal.Body><h4>From {this.state.defaultOption.label} to <span>{this.state.eventStatusOption.label}</span></h4></Modal.Body>
              <Modal.Footer>
                <Button className="event-update-cancel-btn" onClick={() => this.handleModal()}>Cancel</Button>
                <Button className="event-submit-btn primary-bg-color stylefont-weight-medium" onClick={() => this.handleEventStatusUpdate()}>Submit</Button>
              </Modal.Footer>
            </Modal>





            {/* Comfirm Modal */}
            <Confirm
              body={this.state.modalPopupMode == modalPopupEventNames.addEvent ? 'Do you want to add this event' : "Do you want to propose these updates to this event?"}
              show={this.state.closeButtonModal}
              submit={() => this.handlesubmitButtonModal(true)}
              cancel={() => this.handlesubmitButtonModal(false)}
              closeButton={() => this.setState({ closeButtonModal: false })}
              page={'event'}
            />

            {/* Info Modal */}
            <Info
              header={"Event Updated"}
              body={this.state.eventModelmessage}
              show={this.state.infocloseButtonModal}
              submit={() => this.handleInfoSubmitButtonModal(true)}
              closeButton={() => this.handleInfoSubmitButtonModal(true)}
              page={'event'}
            />

          </div>

        )}
      </UserContext.Consumer>
    );
  }
}
export default withTranslation()(AddEvent);
