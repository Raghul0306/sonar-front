import React, { Component } from "react";
import loadable from "@loadable/component";
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import "../../assets/formik-form-style.css";
//React date picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
//Design Starts
import styled from 'styled-components'
import { FilterCircle } from '@styled-icons/bootstrap/FilterCircle'
import { Search } from '@styled-icons/bootstrap/Search'
import { Calendar } from '@styled-icons/ionicons-outline/Calendar'
import { UpArrow } from '@styled-icons/boxicons-solid/UpArrow';
import { Close } from '@styled-icons/ionicons-outline/Close';

const BuyerOrderGrid = loadable(() =>
  import("../datagrid/BuyerOrderGrid")
);

const Filter = styled(FilterCircle)`
  color: #4a88c5;
  width:40px;
  height:40px;
`
const SearchIcon = styled(Search)`
  color: #001F1D;
  width:20px;
  height:20px;
`
const CalendarIconInput = styled(Calendar)`
  color: #3a3a3b;
  width:30px;
  height:30px;
`
const UpArrowFill = styled(UpArrow)`
color: #2C91EE;
width:20px;
height:20px;
`
const CloseIcon = styled(Close)`
color: #556573;
width:30px;
height:30px;
`
export class BuyerProfileView extends Component {
  constructor(props) {
    super(props)

}
  render() {
    return (
      <>
        <div className="profile--grid">
            <Card>
                <div className="account--detail">
                    <h4 className="info-title">Buyer Account</h4>
                    <div className="delivery-status-grid">
                        <div className="left--info">
                            <h6>Buyer Name:</h6>
                        </div>
                        <div className="right--info">
                            <h6>Jacob Beman</h6>
                        </div>
                        <div className="left--info">
                            <h6>Email Address:</h6>
                        </div>
                        <div className="right--info">
                            <h6>jacob@gmail.com</h6>
                        </div>
                        <div className="left--info">
                            <h6>Phone Number:</h6>
                        </div>
                        <div className="right--info">
                            <h6>(423) 560-9701</h6>
                        </div>
                        <div className="left--info">
                            <h6>Last Login:</h6>
                        </div>
                        <div className="right--info">
                            <h6 className="green-txt-success">08/03/2021 11:00 am</h6>
                        </div>
                    </div>
                    <div className="text-center">
                        <Button className="buyer-profile-btn">Reset Password</Button>
                    </div>
                </div>
            </Card>
            <Card>
                <div className="channel-markup-detail">
                    <h4 className="info-title">Buyer Statistics</h4>
                    <div className="delivery-status-grid">
                        <div className="delivery-status-grid">
                            <div className="left--info">
                                <h6>Lifetime Value:</h6>
                            </div>
                            <div className="right--info">
                                <h6 className="green-txt-success">$ 2,150.25</h6>
                            </div>
                            <div className="left--info">
                                <h6>Lifetime Transactions:</h6>
                            </div>
                            <div className="right--info">
                                <h6><span class="primary-box">13</span></h6>
                            </div>
                            <div className="left--info">
                                <h6>Open Orders:</h6>
                            </div>
                            <div className="right--info">
                                <h6><span class="success-box">2</span></h6>
                            </div>
                            <div className="left--info">
                                <h6>Total Cancelled Orders:</h6>
                            </div>
                            <div className="right--info">
                                <h6><span class="warning-box">4</span></h6>
                            </div>
                        </div>
                        <div className="delivery-status-grid">
                            <div className="left--info">
                                <h6>Total Refund Amount:</h6>
                            </div>
                            <div className="right--info">
                                <h6 className="greenblue-txt">$ 2,150.25</h6>
                            </div>
                            <div className="left--info">
                                <h6>Total Refunded Orders:</h6>
                            </div>
                            <div className="right--info">
                                <h6><span class="lightblue-box">3</span></h6>
                            </div>
                            <div className="left--info">
                                <h6>Zendesk Tickets:</h6>
                            </div>
                            <div className="right--info">
                                <h6><span class="dargray-box">5</span></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </Card>
        </div>
        <div className="buyer-grid-gr-item">
            <Card>
                <div className="statistics">
                    <h4 className="info-title">Promo Codes</h4>
                    <div className="promocode-table head-promo-code">
                        <div className="attribute-item">
                            <h5 className="promo-title">Promo:</h5>
                        </div>
                        <div className="attribute-item">
                            <h5 className="promo-title">Offer:</h5>
                        </div>
                        <div className="attribute-item">
                            <h5 className="promo-title">Order Date:</h5>
                        </div>
                        <div className="attribute-item">
                            <h5 className="promo-title">Used ID:</h5>
                        </div>
                    </div>
                    <div className="promocode-table table-body-code">
                        <div className="attribute-item">
                            <h6 className="promo-txt active">YADARA20</h6>
                            <h6 className="promo-txt active">CRICKET15</h6>
                            <h6 className="promo-txt active">YADARA25</h6>
                            <h6 className="promo-txt active">BASEBALL10</h6>
                        </div>
                        <div className="attribute-item">
                            <h6 className="offer-txt">20% Off</h6>
                            <h6 className="offer-txt">15% Off</h6>
                            <h6 className="offer-txt">25% Off</h6>
                            <h6 className="offer-txt">10% Off</h6>
                        </div>
                        <div className="attribute-item">
                            <h6 className="order-id-txt">03/07/2021</h6>
                            <h6 className="order-id-txt">03/07/2021</h6>
                            <h6 className="order-id-txt">03/07/2021</h6>
                            <h6 className="order-id-txt">03/07/2021</h6>
                        </div>
                        <div className="attribute-item">
                            <h6 className="order-id-txt">675890</h6>
                            <h6 className="order-id-txt">675834</h6>
                            <h6 className="order-id-txt">475941</h6>
                            <h6 className="order-id-txt">567492</h6>
                        </div>
                    </div>
                </div>
            </Card>
            <Card>
                <div className="statistics">
                    <h4 className="info-title">Seller Statistics</h4>
                    <div className="profile-grid--3">
                        <div className="offer-code-item">
                            <h5 className="promo-title">Favorite Teams:</h5>
                            <h6 className="offer-txt">New York Yankees</h6>
                            <h6 className="offer-txt">Chicago Bulls</h6>
                            <h6 className="offer-txt">Pittsburgh Steelers</h6>
                            <h6 className="offer-txt">Manchester City</h6>
                        </div>
                        <div className="order--items">
                            <h5 className="promo-title">Favorite Artists:</h5>
                            <h6 className="order-id-txt">Bill Burr</h6>
                            <h6 className="order-id-txt">Lumineers</h6>
                            <h6 className="order-id-txt">Mt. Joy</h6>
                            <h6 className="order-id-txt">The Weekend</h6>
                        </div>
                    </div>
                </div>
            </Card>
        </div>
        <div className="buyer-order-history">
            <div className="header-history">
                Buyer Order History
            </div>
            <BuyerOrderGrid/>
        </div>
      </>    
    );
  }
}

export default (BuyerProfileView)
