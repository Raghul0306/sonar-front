import React, { useState, useContext } from "react";
import { CardHeader,CardBody } from "shards-react";
import Button from 'antd/es/button';
import DatePicker from 'antd/es/date-picker';
import TimePicker from 'antd/es/time-picker';
import Card from 'antd/es/card';
import { useTranslation } from "react-i18next";
import UserContext from "../../contexts/user.jsx";
import Select from 'react-select'
// style icon
import styled from 'styled-components'
import { FilterCircle } from '@styled-icons/bootstrap/FilterCircle'
import iconInhand from "../../assets/myTicketsIconCalendar.png";
import iconTime from "../../assets/MyTickets-icon_clock.png";

const FilterCircleIcon = styled(FilterCircle)`
  color: #4a88c5;
  width:40px;
  height:40px;
`

const { Option } = Select;


function AccountReport(props) {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const [frequency, SetFrequency] = useState("daily");
  const [type, setType] = useState("sales");
  const [plType, setPlType] = useState("item");
  let [show, setShowText] = React.useState(false);

  const handleFrequency = value => {
    SetFrequency(value);
  };

  function handleChange(value) {
    setType(value);
  }
  function handlePlChange(value) {
    setPlType(value);
  }

  const setShow = () => {
      if(show){
        setShowText(false);
      }else{
        setShowText(true)
      }
  }

  function dropdownFilterOption ({ label }, string) {
    if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };

  const reportList = [
    {value:1,label:"Sales Report"},
    {value:2,label:"Listings Report"},
    {value:3,label:"Delivery Report"},
    {value:4,label:"Delivery Report (Open Orders)"},
    {value:5,label:"Profit & Loss Report"}
  ]
  const frequencyList = [
    {value:1,label:"Daily"},
    {value:2,label:"Weekly"},
    {value:3,label:"Monthly"},
    {value:4,label:"Yearly"}
  ]

  return (
    <>
        <Card
        className="down-schedule-report" style={{ width: "100%" }}>
          <CardHeader>
            <div style={{display:"grid",
            gridTemplateColumns:"3fr 1fr"}}>
              <h5 className="card-title secondary-font-family">Reports</h5>
              <div className="accounting-filter-item">
                <span onClick={() => setShow()}><FilterCircleIcon/></span>
              </div>
            </div>
          </CardHeader>
          <CardBody className="p-0">
            <div className={`acounting-item-section ${(show) ? " active" : " inactive"}`}>
              <div className="accounting-reportgrid accounting-reportgrid-bottom" >
                <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                  <label className="form-label-userprofile">User</label>
                  <div>
                      <Select
                        bordered={false}
                        values={type}
                        defaultValue="jake broker_user"
                        onChange={handleChange}
                        style={{ width: "100%" }}
                      >
                        <Option value="broker">jake broker_user</Option>
                        <Option value="listings">john Marketplace_user</Option>
                      </Select>
                    </div>
                </div>
                <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                  <label className="form-label-userprofile">Delivery Type</label>
                  <div>
                      <Select
                        bordered={false}
                        values={type}
                        defaultValue="Delivery Type"
                        onChange={handleChange}
                        style={{ width: "100%" }}
                      >
                        <Option value="deliveryType">Delivery Type</Option>
                        <Option value="deliveryType">Delivery Type</Option>
                        <Option value="deliveryType">Delivery Type</Option>
                      </Select>
                    </div>
                </div>
                <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                  <label className="form-label-userprofile">Event Status</label>
                  <div>
                      <Select
                        bordered={false}
                        values={type}
                        defaultValue="Live"
                        onChange={handleChange}
                        style={{ width: "100%" }}
                      >
                        <Option value="live">Live</Option>
                        <Option value="live">Live</Option>
                        <Option value="live">Live</Option>
                      </Select>
                    </div>
                </div>
                <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                  <label className="form-label-userprofile">Currency Conversion</label>
                  <div>
                      <Select
                        bordered={false}
                        values={type}
                        defaultValue="$USD"
                        onChange={handleChange}
                        style={{ width: "100%" }}
                      >
                        <Option value="usd">$USD</Option>
                        <Option value="usd">$USD</Option>
                        <Option value="usd">$USD</Option>
                      </Select>
                    </div>
                </div>
              </div>
              <div className="text-center">
                <Button
                  style={{ marginTop: "15px" }}
                  type="primary"
                  onClick={props.onSubmit}
                  className="account-report-btn"
                >
                  Submit
                </Button>
                <Button className="account-reset-btn">Reset</Button>
              </div>
            </div>
            <div className="popup-body modal-report-body">
              <div className="accounting-reportgrid">
                <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                  <label className="form-label-userprofile">
                    {t("reports.scheduleReport.reportType")}<span>*</span>
                  </label>
                  <div>
                      <Select
                      {...props}
                      placeholder={t("reports.scheduleReport.reportType")}
                      style={{ width: "100%" }}
                      name="reportType"
                      filterOption={dropdownFilterOption}
                      options={reportList}
                      allowClear
                      showSearch
                      isClearable
                      />
                    </div>
                </div>
                <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                  <label className="form-label-userprofile">
                  {t("form.startDate")} <span>*</span>
                  </label>
                  <div className="calendar-icon-field">
                    <span className="calendar-icon"><img src={iconInhand}/></span>
                    <DatePicker
                        style={{ width: "100%" }}
                    />
                  </div>
                </div>
                <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                  <label className="form-label-userprofile">
                  {t("form.endDate")}<span>*</span>
                  </label>
                  <div className="calendar-icon-field">
                    <span className="calendar-icon"><img src={iconInhand}/></span>
                    <DatePicker
                        style={{ width: "100%" }}
                    />
                  </div>
                </div>
                <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                  <Button className="ant-btn-download">Download</Button>
                </div>
              </div>
              <div className="accounting-reportgrid">
                <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                  <label className="form-label-userprofile">Frequency <span>*</span></label>
                  <Select
                      {...props}
                      placeholder={"Frequency"}
                      style={{ width: "100%" }}
                      name="frequency"
                      filterOption={dropdownFilterOption}
                      options={frequencyList}
                      allowClear
                      showSearch
                      isClearable
                      />
                </div>
                <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                  <div className="schedule-reports">
                    {frequency !== "weekly" && (
                      <>
                        <label className="form-label-userprofile">
                          {t("reports.scheduleReport.Schedule")} <span>*</span>
                        </label>
                        <div className="calendar-icon-field">
                          <span className="calendar-icon"><img src={iconInhand}/></span>
                              <DatePicker
                                style={{ width: "100%" }}
                                // onChange={onChange}
                                picker={frequency}
                              />
                        </div>
                      </>
                    )}
                  </div>
                </div>
                <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                    <label className="form-label-userprofile">
                      {t("reports.scheduleReport.ScheduleTime")} <span>*</span>
                    </label>
                    <div className="calendar-icon-field">
                      <span className="calendar-icon"><img src={iconTime}/></span>
                        <TimePicker
                        style={{ width: "100%" }}
                        // onChange={}
                      />
                    </div>
                  </div>
                <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                  <Button className="ant-btn-schedule">Schedule</Button>
                </div>
              </div>   
            </div>
          </CardBody>
      </Card>
      <div>
        <div className="card-title-accounting mt-3">
          <h5 className="secondary-font-family">Dashboard</h5>
        </div>
        <div className="view-grid-account">
          <Card
            className="down-schedule-report" style={{ width: "100%"}}>
                <div className="account-view-grid">
                  <div className="channel-accouting">
                    <div className="left-account-view">
                      <h3>Channel</h3>
                      <Button className="yadara-btn active">Yadara</Button>
                      <Button className="yadara-btn">Stubhub</Button>
                      <Button className="yadara-btn">Viagogo</Button>
                      <Button className="yadara-btn">LiveFootballTickets</Button>
                      <Button className="yadara-btn">Vibe</Button>
                      <Button className="yadara-btn">Ticombo</Button>
                    </div>
                    <div className="right-account-view">                      
                      <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                        <div className="calendar-icon-field">
                          <span className="calendar-icon"><img src={iconInhand}/></span>
                          <DatePicker
                              style={{ width: "100%" }}
                          />
                        </div>
                      </div>
                      <div class="pickup-status-grid">
                        <div class="pickup-label"><h6>Sales:</h6></div>
                        <div class="pickup-field-item sales-price"><h6>$ 5,760</h6></div>
                        <div class="pickup-label"><h6>Revenue: </h6></div>
                        <div class="pickup-field-item revenue-price"><h6>$ 2,650</h6></div>
                        <div class="pickup-label"><h6>Margin:</h6></div>
                        <div class="pickup-field-item margin-price"><h6>5.65%</h6></div>
                        <div class="pickup-label"><h6>% Delivery:</h6></div>
                        <div class="pickup-field-item"><h6>70%</h6></div>
                        <div class="pickup-label"><h6>Orders:</h6></div>
                        <div class="pickup-field-item">
                          <span className="order-view-acc">25</span>
                        </div>
                        <div class="pickup-label"><h6>Cancelled Orders:</h6></div>
                        <div class="pickup-field-item">
                          <span className="cancel-order-view">5</span>
                        </div>
                        <div class="pickup-label"><h6>Channel Rank:</h6></div>
                        <div class="pickup-field-item"><h6># 1</h6></div>
                      </div>
                    </div>
                  </div>
                </div>
          </Card>
          <Card
            className="down-schedule-report" style={{ width: "100%"}}>
            <div className="account-view-grid">
              <div className="channel-accouting">
                <div className="left-account-view">
                  <h3>Vendor:</h3>
                  <Button className="yadara-btn active">Ticketmaster</Button>
                  <Button className="yadara-btn">AXS</Button>
                  <Button className="yadara-btn">Ticketmaster Australia</Button>
                  <Button className="yadara-btn">See Tickets</Button>
                  <Button className="yadara-btn">Ticketek</Button>
                </div>
                <div className="right-account-view">                      
                  <div className="report-custom-field" style={{ marginBottom: "10px" }}>
                    <div className="calendar-icon-field">
                      <span className="calendar-icon"><img src={iconInhand}/></span>
                      <DatePicker
                          style={{ width: "100%" }}
                      />
                    </div>
                  </div>
                  <div class="pickup-status-grid">
                    <div class="pickup-label"><h6>Total Purchase Value:</h6></div>
                    <div class="pickup-field-item sales-price"><h6>$ 2,650</h6></div>
                    <div class="pickup-label"><h6>Total Fee Paid:</h6></div>
                    <div class="pickup-field-item margin-price"><h6>$ 245</h6></div>
                    <div class="pickup-label"><h6>% Delivery:</h6></div>
                    <div class="pickup-field-item"><h6>70%</h6></div>
                    <div class="pickup-label"><h6>Orders:</h6></div>
                    <div class="pickup-field-item">
                      <span className="order-view-acc">25</span>
                    </div>
                    <div class="pickup-label"><h6>Cancelled Orders:</h6></div>
                    <div class="pickup-field-item">
                      <span className="cancel-order-view">5</span>
                    </div>
                    <div class="pickup-label"><h6>Rank:</h6></div>
                    <div class="pickup-field-item"><h6># 1</h6></div>
                  </div>
                </div>
              </div>
            </div>
          </Card>
        </div>
      </div>
    </>
  );
}

export default AccountReport;
