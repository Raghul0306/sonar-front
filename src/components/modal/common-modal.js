import React from 'react';
import { Modal, Button } from 'react-bootstrap';
//Confirm
const Confirm = (params) => {
    
    return (
        <div>
            {/* Price Update Confirmation Modal */}
            <Modal show={params.show} className="confirmation-modal-common"  backdrop="static">

            <Modal.Header closeButton onClick={params.closeButton}>
            <Modal.Title>{params.header}</Modal.Title>
          </Modal.Header>
            {params.body && params.body.length > 0 ?
            <Modal.Body>
                {params && params.page == 'event'? 
                <div className="comfirmation-txt-content event-page-modaltitle">
                    <p>{params.body}</p>
                </div>
                    :
                    <div className="comfirmation-txt-content">
                        <p>{params.body}</p>
                    </div>}

                </Modal.Body>
                : ''
                }         

            <Modal.Footer className="modal-btn-align">
            <button className="btn btn-secondary info-btn-listing" onClick={params.cancel}>
                Cancel
            </button>
            <button className="btn btn-success info-btn-listing" onClick={params.submit}>
                Submit
            </button>
            </Modal.Footer>

            </Modal>
        </div>
    )
    
}

//Info
const Info = (params) => {
    
    return (
    <div>
        <Modal show={params.show}  backdrop="static">
        <Modal.Header closeButton onClick={params.closeButton}>
        <Modal.Title>{params.header}</Modal.Title>
        </Modal.Header>
        {params.body && params.body.length > 0 ?
        <Modal.Body>
        {params && params.page == 'event'? 
        <p className="info-txt-userlisting event-sucess-text">{params.body}</p>
        :
        <p className="info-txt-userlisting">{params.body}</p>}

        </Modal.Body>
         : ''
         }
        <Modal.Footer className="modal-btn-align">
        <button className="btn btn-success info-btn-listing" onClick={params.submit}>
        {params && params.page == 'event'? 
        'Continue':'Ok'
        }
        </button>
        </Modal.Footer>
        </Modal>
    </div>
    )
    
}

//Alert
const Alert = (params) => {
    
    return (
    <div>
        <Modal show={params.show}  backdrop="static">
        <Modal.Header closeButton onClick={params.closeButton}>
        <Modal.Title>{params.header}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <p className="info-txt-userlisting">{params.body}</p>
        </Modal.Body>
        <Modal.Footer className="modal-btn-align">
        <button className="btn btn-success info-btn-listing" onClick={params.submit}>OK</button>
        </Modal.Footer>
        </Modal>
    </div>
    )
    
}

export {
    Confirm,
    Info,
    Alert
}
