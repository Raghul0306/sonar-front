export default {
    primary: "#5F8FF0",
    primaryLight: "#92C2FF",
    primaryDark: "#2C5CBD",
    secondary: "#42ACF8",
    secondaryLight: "#75DFFF",
    secondaryDark: "#0F79C5",
    tertiary: "#FB8B89",
    tertiaryLight: "#FFBEBC",
    tertiaryDark: "#C85856",
    dark: "#2C2E56",
    darkLight: "#5F6189",
    darkDark: "#000023"
};