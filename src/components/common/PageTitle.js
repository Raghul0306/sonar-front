import React, { useState, useEffect, useContext } from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { Col } from "shards-react";
import { Link } from "react-router-dom";
import styled from 'styled-components'
import { IosArrowLeft } from '@styled-icons/fluentui-system-filled/IosArrowLeft';
import { useHistory, withRouter } from "react-router-dom";
import { currentPathNames } from "../../constants/constants";

const PageTitle = ({ title, subtitle, className, redirectPath, ...attrs }) => {
  const classes = classNames(
    className,
    "text-center",
    "text-md-left",
    "mb-sm-0"
  );

  const Leftarrow = styled(IosArrowLeft)`
    color: #9f9f9f;
    width:30px;
    height:30px;
      `;
  const history = useHistory();
  function previousHistory() { history.push({ pathname: "../listings" }); }
  useEffect(() => {
    history.push({ pageTitle: title, pageSubTitle: subtitle, redirectPath: redirectPath });
  }, []);

  let current_path = window.location.pathname.split("/");
  let showBackButton = false;
  if ((current_path[1] != '' || current_path[1] != null) && (current_path[1] == currentPathNames.addListing)) {
    showBackButton = true;
  }

  return (
    // <Col xs="12" sm="4" className={classes} {...attrs}>
    //   {/* <span className="text-uppercase page-subtitle">{subtitle}</span> */}
    //   <div className="d-flex align-items-center">
    //     {showBackButton && <span onClick={previousHistory}><Leftarrow /></span>}
    //     <h3 className="my-page-title">{title}</h3>
    //   </div>
    // </Col>
    <></>
  );
};

PageTitle.propTypes = {
  /**
   * The page title.
   */
  title: PropTypes.string,
  /**
   * The page subtitle.
   */
  subtitle: PropTypes.string,
  /**
  * The page redirection.
  */
  redirectPath: PropTypes.string,
  /**
   * To show the page functionality button 
   */
  buttonProps: PropTypes.string,
};

export default withRouter(PageTitle);
