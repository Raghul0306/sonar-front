import React from "react";
import ContentLoader from "react-content-loader";

export default function Loader() {
  return <ContentLoader type="facebook" />;
}
