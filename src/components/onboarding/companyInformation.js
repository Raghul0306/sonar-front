import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import Select from 'react-select'
import {
  updateUser,
  getTokenUser,
  getCountries,
  getStates,
  getCities
} from "../../services/userService";
import { userRoleIds } from "../../constants/constants";
import Tooltip from "@material-ui/core/Tooltip";
import "../../assets/formik-form-style.css";
import history from '../../helpers/history';
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import Cookies from "universal-cookie";
import LogoImage from "../../assets/myticket-small-logo.png";
import loadable from "@loadable/component";

const StepNav = loadable(() =>
  import("./StepNav")
);


const CompanyInfoSchema = Yup.object().shape({

  firstName: Yup.string().required("First Name is required"),
  lastName: Yup.string().required("Last Name is required"),
  companyName: Yup.string().required("Company Name is required"),


  phone: Yup.string()
    .required("Phone number is required")
    .matches(/^[0-9]+$/, "Phone number must be only digits")
    .min(10, 'Phone number should be a minimum of 10 digits')
    .max(15, 'Phone number should be a maximum of 15 digits'),
  streetAddress: Yup.string().required("Street Address Name is required"),
  country: Yup.string().required("Country is required"),
  city: Yup.string().required("City is required"),
  state: Yup.string().required("State is required"),
  zipCode: Yup.string()
    .required("Zip Code is required")
    .matches(/^[a-zA-Z0-9]+$/, "ZipCode must be only alphanumerics")
    .min(3, 'ZipCode should be a minimum of 3 alphanumerics')
    .max(8, 'ZipCode should be a maximum of 8 alphanumerics'),

});

const cookies = new Cookies();


function CompanyInformation(props) {
  const [countryList, setCountryList] = useState('')
  var [country, setCountry] = useState("");
  var [stateList, setStateList] = useState("");
  var [cityList, setCityList] = useState("");
  var [userDetails, setUserDetails] = useState("");
  var [selectedCountry, setSelectedCountry] = useState("");
  var [selectedState, setSelectedState] = useState("");
  var [selectedCity, setSelectedCity] = useState("");
  var [firstName, setFirstName] = useState("");
  var [lastName, setLastName] = useState("");
  var [phoneNumber, setPhoneNumber] = useState("");
  var [zipCodeNumber, setZipCode] = useState("");
  var [address1, setAddress1] = useState("");
  var [address2, setAddress2] = useState("");



  useEffect(() => {
    getCountryList();

    async function getUserDetails() {
      let tokenString = window.location.pathname.replace('/verify-email/', '');
      let token_user = tokenString.length > 0 ? await getTokenUser(tokenString) : '';
      if (token_user.status) {
        setUserDetails(token_user.token_user)
      }
    }
    getUserDetails();
  }, []);

  async function getCountryList() {
    let countryList = [];
    let countries = await getCountries();
    if (countries) {
      countries.map((item) => {
        countryList.push({ value: item.id, label: item.name });
      });
      await setCountryList(countryList);
      if (countryList[0].id) { setCountry(countryList[0].id) }
      return countries;
    }
  }

  async function handleCountryChange(country) {
    if (country) {
      await setStateList("");
      await setCityList("");
      let states = await getStates(country.value && country.value.length ? country.value : '');
      if (states) {
        const stateLists = [];
        states.length && states.map(items => {
          stateLists.push({ label: items.name, value: items.id })
        });
        setSelectedState("");
        setSelectedCity("");
        await setStateList(stateLists);
        await setSelectedCountry(country);
      }
      else {
        await setStateList("");
        await setCityList("");
      }
    } else {
      await setSelectedCountry("");
      await setSelectedState("");
      await setSelectedCity("");
      await setStateList("");
      await setCityList("");
    }
  }

  async function handleStateChange(state) {
    if (state) {
      setSelectedState(state);
      let cities = await getCities(selectedCountry.value, state.value);
      if (cities) {
        const cityLists = [];
        cities.length && cities.map(items => {
          cityLists.push({ label: items.name, value: items.id })
        });
        await setCityList(cityLists);
        setSelectedCity("");
      }
    } else {
      await setSelectedState("");
      await setSelectedCity("");
      await setCityList("");
    }
  }

  async function handleCityChange(city) {
    if (city) {
      await setSelectedCity(city);
    } else {
      await setSelectedCity("");
    }
  }

  function dropdownFilterOption({ label }, string) {
    if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };




  const { t } = useTranslation();
  return (
    <Formik
      initialValues={{
        firstName: firstName,
        lastName: lastName,
        companyName: userDetails.company_name,
        phone: phoneNumber,
        streetAddress: address1,
        streetAddress2: address2,
        city: selectedCity ? selectedCity.value : "",
        state: selectedState ? selectedState.value : "",
        zipCode: zipCodeNumber,
        country: selectedCountry ? selectedCountry.value : "",
        email: props.user.email
      }}
      enableReinitialize={true}
      validationSchema={CompanyInfoSchema}
      onSubmit={async (values) => {
        var data = {
          "id": props.user.id,
          "firstName": firstName,
          "lastName": lastName,
          "email": props.user.email,
          "city": selectedCity ? selectedCity.value : "",
          "companyName": values.companyName,
          "country": selectedCountry ? selectedCountry.value : "",
          "phone": phoneNumber,
          "postalCode": zipCodeNumber,
          "state": selectedState ? selectedState.value : "",
          "streetAddress": address1,
          "streetAddress2": address2,
          "needsToSyncBank": true
        }
        if (data) {
          await updateUser(data);
          props.setUser();
        }
      }}
    >
      {(formik) => {
        const { errors, touched, isValid, dirty, values } = formik;
        return (

          <div
            style={{
              display: "flex", justifyContent: "center",
              paddingTop: '25px',
              alignItems: "flex-start",
            }}>

            <div className="card user-info-card-item custom-card-item-grtd"
            // title={t("dashboard.companyInformation.title")}
            >
              <StepNav stage={ props && props.stage ? props.stage : 1 } />

              <div>
                <Form >
                  <div className="information-section">
                    <div className="info-grid-items">
                      <div className="form-group">
                        {/* <Tooltip
                          title={t("dashboard.companyInformation.titleTooltip")}
                          placement="right"
                        >
                        </Tooltip> */}
                          <label>{t("dashboard.companyInformation.firstName")} <span>*</span></label>
                        <Field
                          type="text" autoComplete="off"
                          name="firstName"
                          id="firstName"
                          className={'formik-input-user-form form-control'+ (errors.firstName && touched.firstName ? ' is-invalid' : '')}
                          placeholder="First Name"
                          onChange={(e) => setFirstName(e.target.value)}
                          value={firstName}
                        />
                        <ErrorMessage name="firstName" component="div" className="invalid-feedback" />
                      </div>
                      <div className="form-group">
                        {/* <Tooltip
                          title={t("dashboard.companyInformation.titleTooltip")}
                          placement="right"
                        >
                        </Tooltip> */}
                          <label>{t("dashboard.companyInformation.lastName")} <span>*</span></label>
                        <Field
                          type="text" autoComplete="off"
                          name="lastName"
                          id="lastName"
                          className={'formik-input-user-form form-control'+ (errors.lastName && touched.lastName ? ' is-invalid' : '')}
                          placeholder="Last Name"
                          onChange={(e) => setLastName(e.target.value)}
                          value={lastName}
                        />
                        <ErrorMessage name="lastName" component="div" className="invalid-feedback" />
                      </div>
                      <div className="form-group">
                        {/* <Tooltip
                          title={t("dashboard.companyInformation.titleTooltip")}
                          placement="right"
                        >
                        </Tooltip> */}
                          <label>{t("dashboard.companyInformation.email")} <span>*</span></label>
                        <Field
                          type="text" autoComplete="off"
                          name="email"
                          id="email"
                          className={'formik-input-user-form form-control'}
                          placeholder="Email" readOnly={true} onChange={(e) => e.preventDefault()}
                        />
                        {/* <ErrorMessage name="phone" component="div" className="invalid-feedback" /> */}
                      </div>
                      <div className="form-group">
                        {/* <Tooltip
                          title={t("dashboard.companyInformation.titleTooltip")}
                          placement="right"
                        >
                        </Tooltip> */}
                          <label>{t("dashboard.companyInformation.phone")}<span>*</span></label>
                        <Field
                          type="text" autoComplete="nope"
                          name="phone"
                          id="phone"
                          className={'formik-input-user-form form-control' + (errors.phone && touched.phone ? ' is-invalid' : '')}
                          placeholder="(000) 000-0000"
                          onChange={(e) => setPhoneNumber(e.target.value)}
                          value={phoneNumber}
                        />
                        <ErrorMessage name="phone" component="div" className="invalid-feedback" />
                      </div>
                    </div>
                    <div className="form-group">
                      {/* <Tooltip
                        title={t("dashboard.companyInformation.titleTooltip")}
                        placement="right"
                      >
                      </Tooltip> */}
                        <label>{t("dashboard.companyInformation.name")} <span>*</span></label>
                      <Field
                        type="text"
                        name="companyName" autoComplete="nope"
                        id="companyName"
                        className={'formik-input-user-form form-control' + (errors.companyName && touched.companyName && isValid.companyName ? ' is-invalid' : '')}
                        placeholder="Company Name"
                        value={userDetails.company_name}
                      />
                      <ErrorMessage name="companyName" component="div" className="invalid-feedback" />
                    </div>

                    <div className="info-grid-items">
                      <div className="form-group">
                        {/* <Tooltip
                          title={t("dashboard.companyInformation.titleTooltip")}
                          placement="right"
                        >
                        </Tooltip> */}
                          <label>{t("dashboard.companyInformation.address")} <span>*</span></label>
                        <Field
                          type="text" autoComplete="nope"
                          name="streetAddress"
                          id="streetAddress"
                          onChange={(e) => setAddress1(e.target.value)}
                          className={'formik-input-user-form form-control' + (errors.streetAddress && touched.streetAddress ? ' is-invalid' : '')}
                          placeholder="Street Address1"
                          value={address1}
                        />
                        <ErrorMessage name="streetAddress" component="div" className="invalid-feedback" />
                      </div>


                      <div className="form-group">
                        {/* <Tooltip
                          title={t("dashboard.companyInformation.titleTooltip")}
                          placement="right"
                        >
                        </Tooltip> */}
                          <label>{t("dashboard.companyInformation.address2")}</label>
                        <Field
                          type="text" autoComplete="nope"
                          name="streetAddress2"
                          id="streetAddress2"
                          onChange={(e) => setAddress2(e.target.value)}
                          className={'formik-input-user-form form-control' + (errors.streetAddress2 && touched.streetAddress2 ? ' is-invalid' : '')}
                          placeholder="Street Address2"
                          value={address2}
                        />
                        <ErrorMessage name="streetAddress2" component="div" className="invalid-feedback" />
                      </div>

                      <div className="form-group mb-0">
                        {/* <Tooltip
                          title={t("dashboard.companyInformation.titleTooltip")}
                          placement="right"
                        >
                        </Tooltip> */}
                          <label>{t("dashboard.companyInformation.country")} <span>*</span></label>
                        <Select
                          {...props}
                          placeholder="Country"
                          style={{ width: "100%" }}
                          name="country"
                          filterOption={dropdownFilterOption}
                          value={selectedCountry ? selectedCountry : ""}
                          options={countryList}
                          onChange={e => handleCountryChange(e)}
                          className={(errors.country && touched.country ? ' is-invalid' : '')}
                          allowClear
                          showSearch
                          isClearable
                        />
                        <ErrorMessage name="country" component="div" className="invalid-feedback" />
                      </div>


                      <div className="form-group">
                        {/* <Tooltip
                          title={t("dashboard.companyInformation.titleTooltip")}
                          placement="right"
                        >
                        </Tooltip> */}
                          <label>{t("dashboard.companyInformation.city")} <span>*</span></label>
                        <Select
                          {...props}
                          placeholder="City"
                          style={{ width: "100%" }}
                          name="city"
                          filterOption={dropdownFilterOption}
                          value={selectedCity ? selectedCity : ""}
                          options={cityList}
                          onChange={e => handleCityChange(e)}
                          className={(errors.city && touched.city ? ' is-invalid' : '')}
                          allowClear
                          showSearch
                          isClearable
                        />
                        <ErrorMessage name="city" component="div" className="invalid-feedback" />
                      </div>

                      <div className="form-group select-state-onboarding">
                        {/* <Tooltip
                          title={t("dashboard.companyInformation.titleTooltip")}
                          placement="right"
                        >
                        </Tooltip> */}
                          <label>{t("dashboard.companyInformation.state")} <span>*</span></label>
                        <Select
                          {...props}
                          placeholder="State"
                          style={{ width: "100%" }}
                          name="state"
                          filterOption={dropdownFilterOption}
                          value={selectedState ? selectedState : ""}
                          options={stateList}
                          onChange={e => handleStateChange(e)}
                          className={(errors.state && touched.state ? ' is-invalid' : '')}
                          allowClear
                          showSearch
                          isClearable
                        />
                        <ErrorMessage name="state" component="div" className="invalid-feedback" />
                      </div>


                      <div className="form-group mb-0">
                        {/* <Tooltip
                          title={t("dashboard.companyInformation.titleTooltip")}
                          placement="right"
                        >
                        </Tooltip> */}
                          <label>{t("dashboard.companyInformation.zip")} <span>*</span></label>
                        <Field
                          type="text" autoComplete="off"
                          name="zipCode"
                          id="zipCode"
                          className={'formik-input-user-form form-control' + (errors.zipCode && touched.zipCode ? ' is-invalid' : '')}
                          placeholder="Zip Code"
                          onChange={(e) => setZipCode(e.target.value)}
                          value={zipCodeNumber}
                        />
                        <ErrorMessage name="zipCode" component="div" className="invalid-feedback" />
                      </div>

                    </div>
                  </div>
                  <div className="p-2 text-center top-border-footer">
                    <button
                      type="submit"
                      className="btn btn-primary information-cont-btn stylefont-weight-regular primary-bg-color"
                    > Continue </button>
                  </div>

                </Form>

              </div>
            </div>
          </div>



        );
      }}
    </Formik>
  );
};



export default CompanyInformation;
