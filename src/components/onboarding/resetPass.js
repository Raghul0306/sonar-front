import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';
import { withTranslation } from "react-i18next";
import {
  updateUserPassword,
  updateUserNeedsToChangePassword
} from "../../services/userService";
import Cookies from "universal-cookie";
import loadable from "@loadable/component";
import { fieldTypes } from "../../constants/constants";

const StepNav = loadable(() =>
  import("./StepNav")
);

const cookies = new Cookies();
class ResetPasswordOnboardingForm extends React.Component {
  state = {
    formError: false,
    signIn: true,
    signUp: false,
    fieldType: fieldTypes.password,
    enableButton: false,
    userPassword: '',
    userConfirmPassword: '',
    passwordFieldType: fieldTypes.password,
    confirmPasswordFieldType: fieldTypes.password
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        //To Do
      }
    });
  };

  handleApplicationSubmit = async (values) => {
    if (values.password === values.confirmPassword) {
      await updateUserPassword(this.props.user.email, values.password).then(
        response => {
          this.props.setUser();
        }
      );
    } else {
      alert("passwords do not match");
    }
  };

  componentDidMount() {
    let value = this.context;
    setTimeout(async() => {  
      this.setState({formError: true})
    }, 1000);
    /* perform a side-effect at mount using the value of MyContext */
  }
  changeNewPassword = async (e) => {
    let buttonState = false;
    let passwordValue = ''
    if (e && e.target && e.target.value != '') {
      buttonState = true;
      passwordValue = e.target.value
    }
    await this.setState({ enableButton: buttonState, userPassword: passwordValue });
  }

  changeConfirmPassword = async (e) => {
    let confirmPasswordValue = ''
    if (e && e.target && e.target.value != '') {
      confirmPasswordValue = e.target.value
    }
    await this.setState({ userConfirmPassword: confirmPasswordValue });
  }

  changeType(type) {

    if (type == fieldTypes.password) {
      this.setState({ passwordFieldType: fieldTypes.text });
    }
    else {
      this.setState({ passwordFieldType: fieldTypes.password });
    }

  }
  confirmPassChangeType(type) {
    if (type == fieldTypes.password) {
      this.setState({ confirmPasswordFieldType: fieldTypes.text });
    }
    else {
      this.setState({ confirmPasswordFieldType: fieldTypes.password });
    }

  }

  render() {
    return (
      <div
        style={{
          display: "flex",
          paddingTop: '25px',
          alignItems: "flex-start",
          justifyContent: "center",
          width: "100%"
        }}
      >
        <Formik
          initialValues={{

            password: this.state.userPassword,
            confirmPassword: this.state.userConfirmPassword,
          }}
          validationSchema={Yup.object().shape({

            password: Yup.string()
              .required('Password is required')
              .matches(
                /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
                "Must Contain 8 Characters, at least 1 Uppercase, 1 Lowercase, 1 Number and 1 Special Character"
              ),
            confirmPassword: Yup.string()
              .oneOf([Yup.ref('password'), null], 'Passwords must match')
              .required('Confirm Password is required')
          })}
          enableReinitialize={true}
          onSubmit={fields => {
            this.handleApplicationSubmit(fields)
          }}
          render={({ errors, status, touched }) => (
            <Form className="change-psw--loadpage custom-card-item-grtd">
              <div>
                <div className="change-pws-card-item set-password-section card">
                <StepNav stage={ this.props && this.props.stage ? this.props.stage : 0 } />
                  <div className="card-body">
                      <h2
                        className="set-psw-title"
                      >
                       Set My Password 
                      </h2>
                    <div className="form-group-items">
                      <span className="pws-field-label">New Password:</span>
                      <div className="psw-show-field">
                        <Field
                          type={this.state.passwordFieldType} className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} name="password" style={{ borderRadius: "10px" }}
                          placeholder="Password" onChange={(e) => this.changeNewPassword(e)} value={this.state.userPassword} />
                        <div onClick={(e) => this.changeType(this.state.passwordFieldType)} className="psw-show-btn">{this.state.passwordFieldType == fieldTypes.password ? 'Show' : 'Hide'}</div>
                      </div>
                      <ErrorMessage name="password" style={{ display: "block" }} component="div" className="invalid-feedback" />
                    </div>
                    <div className="form-group-items confirm-field-section">
                      <span className="pws-field-label">Confirm Password:</span>
                      <div className="psw-show-field">
                        <Field className={'form-control' + (errors.confirmPassword && touched.confirmPassword ? ' is-invalid' : '')} onChange={(e) => this.changeConfirmPassword(e)} value={this.state.userConfirmPassword}
                          type={this.state.confirmPasswordFieldType} name="confirmPassword" placeholder="Password" style={{ borderRadius: "10px" }}
                        />
                        <div onClick={(e) => this.confirmPassChangeType(this.state.confirmPasswordFieldType)} className="psw-show-btn">{this.state.confirmPasswordFieldType == fieldTypes.password ? 'Show' : 'Hide'}</div>
                      </div>
                      <ErrorMessage style={{ display: "block" }} name="confirmPassword" component="div" className="invalid-feedback" />
                    </div>
                  </div>
                  <div className="card-footer">
                    <button type="submit"  className={"ant-btn login-form-button ant-btn-primary  stylefont-weight-regular primary-bg-color" + ((Object.keys(errors).length===0 && this.state.formError) ? " active" : "")} style={{ width: "100%" }}>
                      {this.props.t("userInfo.password.change")}
                    </button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        />
      </div>
    );
  }
}
export const OnboardingResetPassForm = withTranslation()(ResetPasswordOnboardingForm);
