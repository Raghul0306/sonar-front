import React from "react";
import * as Yup from 'yup';
import { withTranslation } from "react-i18next";
import {
  updateUserPassword,
  updateUserNeedsToChangePassword
} from "../../services/userService";
import Cookies from "universal-cookie";
import LogoImage from "../../assets/mytickets_logo-new.png";
import { getToken,getUser } from "../../services/checkAuthService";
import UserContext from "../../contexts/user.jsx";
const cookies = new Cookies();
class WelcomeCard extends React.Component {
  state = {
    formError: false,
    signIn: true,
    signUp: false
  };

  componentDidMount() {
    let value = this.context;
  }

  async navigateToDashbaord(){

    let user = this.props.user;
    let loggedInUser = await getToken({email:user.email,verificationToken:this.props.tokenString});

    if(loggedInUser && loggedInUser.token) {
      cookies.set("accessToken", loggedInUser.token, { path: "/" });
      let userDetails = await getUser(user.id);
      this.context.setUser(userDetails);      
      cookies.set("userObject", userDetails, { path: "/" });
      window.location.href='/home';
    }
  }

  render() {
    return (
      <div
        style={{
          display: "flex",
          justifyContent:'center',
          paddingTop:"200px",
          width: "100%"
        }}
      >
        <div>
            <div className="change-pws-card-item welcome-page-card logo-overlay card">
            <div className="border-bottom logo-new-img card-header">
                <h6 className="m-0">
                  <img
                    src={LogoImage}
                    style={{ width: 150 }}
                    alt="logo"
                  />
                </h6>
              </div>
                <div className="card-body">
                    <div className="welcome-content">
                        <h2 className="welcome-title">Welcome to MyTickets!</h2>
                        <h5 className="content-para">Your Broker Account has been created.</h5>
                        <button className="continue-btn" onClick={(e)=> this.navigateToDashbaord(e)}>Continue</button>
                    </div>
                </div>
            </div>
        </div>
      </div>
    );
  }
}

WelcomeCard.contextType = UserContext;
export default withTranslation()(WelcomeCard);
