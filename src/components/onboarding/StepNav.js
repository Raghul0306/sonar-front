import React from "react";
import LogoImage from "../../assets/myticket-small-logo.png";

import Steps from 'antd/es/steps';
// style icon
import styled from 'styled-components'
import { Attachment } from '@styled-icons/entypo/Attachment'
const AttachmentIcon = styled(Attachment)`
  color: #2C91EE;
  width:25px;
  height:25px;
  margin:0 4px 0 0;
`
const { Step } = Steps;

export class StepNav extends React.Component {
        
        constructor(props) {
          super(props);
          this.state = {
            current: 0,
          };
        }
        componentDidMount(){
          if(this.props.stage){
            this.setState({ current:this.props.stage });
          }
        }
        onChange = current => {
          if(!this.props.stage)
            this.setState({ current });
        };
      

    render() {
        const { current } = this.state;

        return (
            <>
              <div className="border-bottom card-header">
                    <div className="card-header">
                      <div className="info-header-grid-item">
                        <span className="info-header-item"><img
                          src={LogoImage}
                          alt={"logo"}
                        /> Sign Up</span>
                      </div>
                      <div className="w-100">
                        <Steps
                          type="navigation"
                          current={current}
                          onChange={this.onChange}
                          className="site-navigation-steps custom-step"
                          >
                          <Step status="process" title="Set Password" icon="false"/>
                          <Step status="process" title="User Info" icon="false"/>
                          <Step status="process" title="Terms of Service" icon="false" />
                          <Step status="process" title="Privacy Policy" icon="false" />
                          <Step status="process" title="Seller Agreement" icon="false" />
                          <Step status="process" title="Connect Bank" icon="false" />
                        </Steps>
                      </div>
                    
                    </div>
                  </div>
               
            </>
        );
    }

}


export default StepNav