import React from "react";
import Form from 'antd/es/form';
import { withTranslation } from "react-i18next";
import UserContext from "../../contexts/user.jsx";
import Cookies from "universal-cookie";
import { updateUserStripeID } from "../../services/userService";
import ConnectBankButton from "../purchases/ConnectBankButton";
import history from '../../helpers/history';
import loadable from "@loadable/component";
const StepNav = loadable(() =>
  import("./StepNav")
);
const cookies = new Cookies();
class BankAccountSync extends React.Component {
  state = {
    formError: false,
    signIn: true,
    signUp: false
  };

  handleApplicationSubmit = e => {
    e.preventDefault();
  };

  componentDidMount() {
    /* perform a side-effect at mount using the value of MyContext */
  }

  render() {
    return (
      <div
        style={{
          display: "flex",
          paddingTop: '25px',
          alignItems: "flex-start",
          justifyContent: "center",
          width: "100%",
        }}
      >
        <UserContext.Consumer>
          {context => (
            <Form onSubmit={this.handleApplicationSubmit}>
              <div >
                <div className=" custom-card-item-grtd" style={{margin:"0 auto"}}>
                  <div  className="user-info-card-item card">
                  <StepNav stage={ this.props && this.props.stage ? this.props.stage : 5 } />
                    <div className="card-body">
                      <div className="welcome-content">
                        <h2 className="welcome-title">Connect Your Bank</h2>
                        <h5 className="content-para">Connect your bank account.</h5>
                      </div>
                      <ConnectBankButton />
                      <div style={{textAlign:"center"}}>
                      <button type="primary"
                      className="skip-step-btn bank-sync-later" onClick={async ()=>{
                        const userObject = await updateUserStripeID(this.props.user.id, "skip");
                          if(userObject.status){
                            // cookies.set("userObject", userObject.userInfo, { path: "/" });
                            this.props.setUser();
                          }
                        }}>Do this Later</button>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
        </Form>)}
        </UserContext.Consumer>
      </div>
    );
  }
}
export const BankAccountSyncForm = Form.create({
  name: "bank_account_sync"
})(withTranslation()(BankAccountSync));
