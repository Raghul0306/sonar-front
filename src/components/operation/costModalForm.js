import React from "react";
import { Modal, Button } from 'react-bootstrap';


function CostModalForm(params) { 
      return <>
      {params && params.showModal===true ? 
        <Modal className="inHandmodal" show={params.showModal} onHide={() => params.hideModal(false) } backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Update Cost</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="form-group operation-cost-modal">
              <label>New Cost</label>
              <input type="text" autoFocus className={params.validation.cost ? "custom-b-radius form-control is-invalid" : "custom-b-radius form-control"} value={params.value} min={1} onChange={ (e) => params.costOnChange(e) } onKeyDown={(e) => params.costOnKeyDown(e) } />
              {
                  params.validation.cost ? <div className="invalid-feedback" style={{ display: "block" }}>{params.value <= 0 ? 'New Cost is required' : 'Cost should be a valid number'}</div> : ''
                }
            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align operation-cost-modal-footer">
            <Button className={params.validation.cost || params.value <= 0 ? "" : "active"} variant="primary" disabled={params.validation.cost || params.value<= 0} onClick={(e) => params.hideModal(true)}>
            Update
            </Button>
          </Modal.Footer>
        </Modal>   
        : '' }

      {params && params.showCostConfirmationModal===true ? 

     
        <Modal className="price-update-modal price-confirmation--modal" show={params.showCostConfirmationModal} onHide={(e) => params.costUpdate(false)} backdrop="static">
        <Modal.Header closeButton>
            <Modal.Title>Cost Change Confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <h4 className="change-date-text stylefont-weight-bold">Change all costs to <span>${params.value ? params.value : ''}</span></h4>

        </Modal.Body>
        <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={(e) => params.costUpdate(false)}>
            Cancel
            </Button>
            <Button variant="primary" onClick={(e) => params.costUpdate(true)}>
            Submit
            </Button>
        </Modal.Footer>
        </Modal>
      : '' }

    </>
    }

export { CostModalForm };