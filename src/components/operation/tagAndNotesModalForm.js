import React from "react";
import { Modal, Button } from 'react-bootstrap';
import Select from 'react-select'

function TagAndNotesModalForm(params) {
  
    return <>
        <Modal className="mytickets-list-popup" show={params.showModal} onHide={() => params.hideModal(false) } backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Edit Tags and Notes</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <div className="di-form-items">
            <div className="form-group add-listing-form-group multi-select-item">
              <label>Tags</label>              
              <Select
                placeholder={"Tags"}
                style={{ width: "100%" }}
                name="tags"
                value={params.selectedTags && params.selectedTags.length ? JSON.parse(params.selectedTags) : []}
                options={params.tagOption}
                onChange={(e) => params.tagsOnchange(e)}
                allowClear
                showSearch
                isClearable
                isMulti
              />              
            </div>
          </div>

          <div className="form-group">
            <label>Internal Notes</label>
            <textarea className="form-control" value={params.values} onChange={ (e) => params.notesOnChange(e) } />
          </div>
          
          </Modal.Body>
          <Modal.Footer className="modal-btn-align tags-modal-popup-footer">
            <Button className={params.selectedTags.length==0 ? "" : "active"} variant="primary" onClick={(e) => params.hideModal(true)}>
              Add Tags and Notes
            </Button>
          </Modal.Footer>
        </Modal>
    </>;
}

export { TagAndNotesModalForm };