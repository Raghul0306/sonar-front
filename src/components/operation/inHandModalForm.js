import React, { useEffect, useState, useContext } from "react";
import { Modal, Button } from 'react-bootstrap';
//React date picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import iconCalendarImg from "../../assets/myTicketsIconCalendar.png";
import * as moment from "moment-timezone";

function InHandModalForm(params) {
   
    return <>
    {params && params.showModal===true ? 
        <Modal className="inHandmodal" show={params.showModal} onHide={() => params.hideModal(false) } backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Update In-Hand Date</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="form-group">
              <label>In-Hand Date</label>
              <div className="modal-datepicker">
                <img src={iconCalendarImg} className="datemodal-icon" />
                <DatePicker
                  selectsRange={false}
                  selected={params.inhandDate}
                  value={params.inhandDate}
                  className="dateSinglepicker"
                  onChange={(e) => params.inhandDateOnChange(e)}
                  isClearable={false}
                  onKeyDown={(e) => e.preventDefault()}
                  maxDate={new Date(params.modalEventDate)}
                  dateFormat="dd-MM-yyyy"
                  placeholderText=" DD-MM-YYYY"
                />
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button className={params.inhandDate? "active" : ""} variant="primary" disabled={!params.inhandDate} onClick={(e) => params.hideModal(true)}>
              Update
            </Button>
          </Modal.Footer>
        </Modal>   
        : '' }   

      {params && params.showInhandConfirmation===true ? 
        <Modal className="price-update-modal price-confirmation--modal" show={params.showInhandConfirmation} onHide={(e) => params.hideConfirmationModal(false)} backdrop="static">
            <Modal.Header closeButton>
                <Modal.Title>In-Hand Date Confirmation</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <h4 className="change-date-text stylefont-weight-bold">Change In-Hand Dates to <span>{moment.utc(params.inhandDate).local().format('DD/MM/YYYY')}</span></h4>

            </Modal.Body>
            <Modal.Footer className="modal-btn-align">
                <Button variant="secondary" onClick={(e) => params.hideConfirmationModal(false)}>
                Cancel
                </Button>
                <Button variant="primary" onClick={(e) => params.hideConfirmationModal(true)}>
                Submit
                </Button>
            </Modal.Footer>
            </Modal>
      : '' }
        </>
    }

export { InHandModalForm };