import React from "react";
import { Modal, Button } from 'react-bootstrap';
import "react-datepicker/dist/react-datepicker.css";
import Select from 'react-select'

function SplitsModalForm(params) {
      return <>
      {params && params.showModal===true ? 
        <Modal className="split-modal-popup operation-inhandmodal" show={params.showModal} onHide={(e) => params.onHide(false)} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Update splits</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="di-form-items form-group">
              <label>Splits</label>
              <Select
                placeholder={"Splits"}
                style={{ width: "100%" }}
                name="tags"
                value={params.selectedSplits  ? params.selectedSplits : []}
                options={params.splitsList}
                onChange={(e) => params.splitsOnChange(e)}
                allowClear
                showSearch  
                isClearable              
              />              
              {
                params.validation.split ? <div className="invalid-feedback" style={{ display: "block" }}>Splits type is required</div> : ''
              }

            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button className={params.selectedSplits && params.selectedSplits.value ? "active" : ""} variant="primary" disabled={params.selectedSplits && params.selectedSplits.value ? false : true} onClick={(e) => params.onHide(true)}>
              Update
            </Button>
          </Modal.Footer>
        </Modal>   
        : '' }

      {params && params.confirmSplitModal===true ? 

     
        <Modal className="price-update-modal price-confirmation--modal" show={params.confirmSplitModal} onHide={(e) => params.hideConfirmationModal(false)} backdrop="static">
        <Modal.Header closeButton>
            <Modal.Title>Add Splits Confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <h4 className="change-date-text stylefont-weight-bold">Add all splits in the event to <span>{params.selectedSplits && params.selectedSplits.value ? params.selectedSplits.label : ''}</span></h4>

        </Modal.Body>
        <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={(e) => params.hideConfirmationModal(false)}>
            Cancel
            </Button>
            <Button variant="primary" onClick={(e) => params.hideConfirmationModal(true)}>
            Submit
            </Button>
        </Modal.Footer>
        </Modal>
      : '' }

    </>
    }

export { SplitsModalForm };