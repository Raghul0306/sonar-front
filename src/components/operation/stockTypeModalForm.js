import React from "react";
import { Modal, Button } from 'react-bootstrap';
import "react-datepicker/dist/react-datepicker.css";
import Select from 'react-select'

function StockTypeModalForm(params) {
      return <>
      {params && params.showModal===true ? 
        <Modal className="inHandmodal operation-inhandmodal" show={params.showModal} onHide={() => params.hideModal(false) } backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Update Stock Type</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="di-form-items form-group add-listing-form-group multi-select-item">
              <label>Stock Type</label>
              <div>
                  <Select
                  placeholder={"Select Stock Type"}
                  style={{ width: "100%" }}
                  name="stock_type"
                  value={params.selectedStockTypes && params.selectedStockTypes.length ? params.selectedStockTypes : []}
                  options={params.stockTypesOption}
                  onChange={(e) => params.StockTypesOnchange(e)}
                  allowClear
                  showSearch
                  isClearable
                  /> 
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button className={params.selectedStockTypes && params.selectedStockTypes.length ? "active" : ""} variant="primary" disabled={params.selectedStockTypes && params.selectedStockTypes.length ? false:true} onClick={(e) => params.hideModal(true)}>
            Update
            </Button>
          </Modal.Footer>
        </Modal>   
        : '' }

      {params && params.showStockTypeConfirmation===true ? 

     
        <Modal className="price-update-modal price-confirmation--modal" show={params.showStockTypeConfirmation} onHide={(e) => params.hideConfirmationModal(false)} backdrop="static">
        <Modal.Header closeButton>
            <Modal.Title>Stock Type Confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <h4 className="change-date-text stylefont-weight-bold">Add all Stock Types in the event to <span>{params.selectedStockTypes && params.selectedStockTypes.length ? params.selectedStockTypes[0].label : ''}</span></h4>

        </Modal.Body>
        <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={(e) => params.hideConfirmationModal(false)}>
            Cancel
            </Button>
            <Button variant="primary" onClick={(e) => params.hideConfirmationModal(true)}>
            Submit
            </Button>
        </Modal.Footer>
        </Modal>
      : '' }

    </>
    }

export { StockTypeModalForm };