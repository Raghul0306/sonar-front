import React, { Component } from "react";
import loadable from "@loadable/component";
import UserContext from "../../contexts/user.jsx";
import Button from 'antd/es/button';
import { Link } from "react-router-dom";
import "../../assets/formik-form-style.css";
//React date picker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
//Design Starts
import styled from 'styled-components'
import { FilterCircle } from '@styled-icons/bootstrap/FilterCircle'
import { Search } from '@styled-icons/bootstrap/Search'
import { Calendar } from '@styled-icons/ionicons-outline/Calendar'
import { UpArrow } from '@styled-icons/boxicons-solid/UpArrow';
import { DownArrow } from '@styled-icons/boxicons-regular/DownArrow';
import { Close } from '@styled-icons/ionicons-outline/Close';
import iconInhand from "../../assets/listing-icon/MyTickets-icon_inhand-date.png";
import { listingConst as listingConstant } from "../../constants/constants";
import lodashGet from 'lodash/get';
import lodashFilter from 'lodash/filter';
import lodashIncludes from 'lodash/includes';
import Select from 'react-select'

import {
  allGenreDetails, getAllTagList, eventSearches, getEventCount, getAllTicketTypes, allTicketingSystemAccounts, getAllSplits
} from "../../services/listingsService.js";

import { getAllCompanies } from "../../services/companyService";

import {
  getOperationFilterListings
} from "../../services/operationService.js";
import {
  getAllUsers
} from "../../services/usersService.js";

const OperationListingGrid = loadable(() =>
  import("../datagrid/OperationListingGrid")
);
const Filter = styled(FilterCircle)`
  color: #4a88c5;
  width:40px;
  height:40px;
`
const SearchIcon = styled(Search)`
  color: #001F1D;
  width:20px;
  height:20px;
`
const CalendarIconInput = styled(Calendar)`
  color: #3a3a3b;
  width:30px;
  height:30px;
`
const UpArrowFill = styled(UpArrow)`
color: #2C91EE;
width:15px;
height:15px;
`

const UpArrowBorder = styled(DownArrow)`
color: #556573;
width:15px;
height:15px;
`
const CloseIcon = styled(Close)`
color: #556573;
width:30px;
height:30px;
`
export class OperationFilter extends Component {
  static contextType = UserContext;
  state = {
    loading: true,
    suggestions: [],
  };
  constructor(props) {
    super(props)
    this.toggleAdvanced = this.toggleAdvanced.bind(this);
    this.listSearchChange = this.listSearchChange.bind(this);
    this.selectListing = this.selectListing.bind(this);
    this.avoidOnchange = this.avoidOnchange.bind(this);
    this.dateRangeDisplay = this.dateRangeDisplay.bind(this);
    this.advanceFilterReset = this.advanceFilterReset.bind(this);
    this.advanceFilterSubmit = this.advanceFilterSubmit.bind(this);
  
  this.state = {
    advancefilter: false,
    searchData: {
      searches: "",
      tags: "",
      section: "",
      row: "",
      quantity: '',
      from_date: "",
      to_date: "",
      day_of_week: "",
      in_hand: "",
      inventory_attached: "",
      ticket_type: "",
      category: "",
      hold: '',
      isSearchEnable: false,
      tagIds:[],
      split:""
    },
    setLoader: false,  //loader
    startDate: null,
    endDate: null,
    operationListingSearch: false,
    searchText:'',
    selectedDays:[],
    multiSelect: {
      ticketType: [],
      genre: [],
      inv_attach: [],
      row: [],
      quantity: [],
      split:[],
      userAssignedTo:[],
      company:[],
      ticketingSystemAccount:[],
    },
    advanceFilterSubmit: false,
  }
}

async componentDidMount() {
  this.setMyPageLoader(true)
  const userId = this.context.user.id;
  let ticketTypeDetails;
  let genreDetails;
  let getTags;
  
  this.setState({ loading: false });
  ticketTypeDetails = await getAllTicketTypes();
  genreDetails = await allGenreDetails();
  getTags = await getAllTagList(userId);
  var allUserList = await getAllUsers(userId);
  var getSplits = await getAllSplits();
  var allCompaniesList = await getAllCompanies(userId);
  
  var ticketingSystemAccounts = await allTicketingSystemAccounts(0,userId,true);
  if(ticketingSystemAccounts && ticketingSystemAccounts.length){
    ticketingSystemAccounts = ticketingSystemAccounts.filter((row, index, list) =>
      index === list.findIndex((account) => (
        account.email === row.email
      ))
    )
  }
  const splitValues = [];
  getSplits && getSplits.length > 0 && getSplits.map((item) => {
      var combined = { label: item.name, value: item.id }
      splitValues.push(combined);
  })
  
  await this.setState({
    ticketTypeDetails: ticketTypeDetails,
    genreDetails: genreDetails,
    getTags: getTags,
    ticketingSystemAccountDetails:ticketingSystemAccounts,
    allUserDetails:allUserList,
    allSplits:splitValues,
    allCompaniesList:allCompaniesList
  });
  this.setMyPageLoader(false);

}
  toggleAdvanced() {
    this.setState({ advancefilter: !this.state.advancefilter });
  }

  //To Show or Hide Loader during API calls
  async setMyPageLoader(bool) {
    if (bool) {
      this.setState({ setLoader: true });
      document.body.classList.add('loader-background');
    }
    else {
      this.setState({ setLoader: false });
      document.body.classList.remove('loader-background');
    }
  }

  /**
   * Method name : listSearchChange
   * Searching the event id, name, etc by user search
   */
   async listSearchChange(e) {   // For autofill search suggestion
    const userId = this.context.user.id;
    let filteredData = {};
    let suggestions = [];
    const value = e && e.target ? e.target.value : '';
    let stateHolder = this.state.searchData;
    stateHolder.searches = value;
    this.setState({ locationText: '', searchText: value, displayText: value, searchData: stateHolder, operationListingdata: [], operationListingSearch: false});
    if (parseInt(value)) {
      this.setMyPageLoader(true);
      suggestions = await eventSearches({ searches: value },true);
      this.setMyPageLoader(false);
    }
    else if (value.length > 3) {
      this.setMyPageLoader(true);
      suggestions = await eventSearches({ searches: value },true);
      this.setMyPageLoader(false);
    }
    else {
      suggestions = [];
      if (value.length == 0) {
        this.setMyPageLoader(true);
        if (this.state.searchAdvance == false) {
          stateHolder['searches'] = '';
        }
        filteredData = await getOperationFilterListings(userId, stateHolder);
        var eventCountDetail = await getEventCount(userId, stateHolder);
        var eventCount = 0;
        if (eventCountDetail && eventCountDetail.countInfo) {
          eventCount = parseInt(eventCountDetail.countInfo.count)
        }
        this.setMyPageLoader(false);
        this.setState({ filterListingsData: filteredData, eventCount, operationListingdata: [], operationListingSearch: false });
      }
    }
    this.setState(() => ({
      suggestions, searchAdvance: false
    }));
  }


/**
 * returns the data in ul list dropdown below the search field
 */
  renderSuggestions = () => {
    const { suggestions } = this.state;
    if (suggestions.length === 0) {
      return null;
    }
    return (
      <ul>
        {suggestions.map(events => <li onClick={() => this.selectListing(events)} >{events}</li>)}
      </ul>
    )
  }

  async avoidOnchange(e) {
    e.preventDefault();
    return false;
  }
  async advanceFilter(event, keyword, state) {
    let eventCall = event;
    if (event) { event = [event]; } else { event = []; }
    
    let stateHolder = this.state.searchData;
    let multiHolder = this.state.multiSelect;
    if (state && keyword != listingConstant.in_hand && keyword != listingConstant.quantity && keyword != listingConstant.inventory_attached && keyword != listingConstant.ticket_type && keyword != listingConstant.purchaseDate) {
      let newValue = event.filter(list => !this.state.multiSelect[state].find(row => (row.label === list.label && list.value === row.value)))
      multiHolder[state] = newValue;
      if (state == listingConstant.quantity) {
        stateHolder[keyword] = newValue[0] ? parseInt(newValue[0].value) : '';
      }
      else if (state == listingConstant.split ||  state == listingConstant.userAssignedTo || state == listingConstant.company) {
        stateHolder[state] = newValue[0] && newValue[0].value ? newValue[0].value.toString() : '';
      }
      else if ( state == listingConstant.ticketingSystemAccount ) {
        stateHolder[state] = newValue[0] && newValue[0].label ? newValue[0].label.toString() : '';
      }
      else {
        stateHolder[keyword] = newValue[0] && newValue[0].value ? newValue[0].value.toString() : '';
      }
    }
    else if (keyword == listingConstant.in_hand) {
      if(eventCall!=null){
        var inHandDate = new Date(eventCall);
        let date = inHandDate.getDate() < 9 ? ('0' + inHandDate.getDate()) : inHandDate.getDate();
        let month = inHandDate.getMonth() < 9 ? '0' + (inHandDate.getMonth() + 1) : inHandDate.getMonth() + 1;
        stateHolder[keyword] = event != null ? inHandDate.getFullYear() + '-' + month + '-' + date : '';
      }else{stateHolder[keyword] = null}
      this.setState({ singleDate: eventCall });
    }
    else if (keyword == listingConstant.purchaseDate) {
      if(eventCall!=null){
        var purchaseDate = new Date(eventCall);
        let date = purchaseDate.getDate() < 9 ? ('0' + purchaseDate.getDate()) : purchaseDate.getDate();
        let month = purchaseDate.getMonth() < 9 ? '0' + (purchaseDate.getMonth() + 1) : purchaseDate.getMonth() + 1;
        stateHolder[keyword] = event != null ? purchaseDate.getFullYear() + '-' + month + '-' + date : '';
      }else{stateHolder[keyword] = null}
      this.setState({ purchaseDate: eventCall });
    }
    else if ([listingConstant.ticket_type, listingConstant.inventory_attached, listingConstant.split, listingConstant.company, listingConstant.userAssignedTo, listingConstant.hold].includes(keyword)) {
      stateHolder[keyword] = (stateHolder[keyword] == eventCall.target.value ? '' : eventCall.target.value);
    } else if (listingConstant.tagIds == keyword) {
      if (event.length) stateHolder[listingConstant.tagIds] = event[0].map(element => Number(element.value));
      else stateHolder[listingConstant.tagIds] = [];
    }
    else {
      stateHolder[keyword] = (keyword == listingConstant.quantity) ? parseInt(eventCall.target.value) : eventCall.target.value;
    }

    if (this.state.searchAdvance == false) {
      stateHolder['searches'] = '';
    }
    stateHolder.isSearchEnable = true;

    this.setState({ searchData: stateHolder, multiSelect: multiHolder });
    this.setMyPageLoader(false);
  }
   //Drop down search filter
   dropdownFilterOption = ({ label, value, data }, string) => {
    if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };

  /**
   * 
   * @param {*} e 
   * Date filter 'startingDate' is only required, to 'endingDate' is optional and removal 
   */
   async dateRangeDisplay(e) {//Date picker
    await this.setState({ startDate: e[0], endDate: e[1] });
    var startingDate = '';
    var startingDateOnly = '';
    var endingDate = '';
    var endingDateOnly = '';
    if (e[0]) {
      var starting = new Date(this.state.startDate);
      let date = starting.getDate() < 9 ? ('0' + starting.getDate()) : starting.getDate();
      let month = starting.getMonth() < 9 ? '0' + (starting.getMonth() + 1) : starting.getMonth() + 1;
      startingDateOnly = starting.getFullYear() + '-' + month + '-' + date
      startingDate = startingDateOnly + ' 00:00:00';
    }
    if (e[1]) {
      var ending = new Date(this.state.endDate);
      let date = ending.getDate() < 9 ? ('0' + ending.getDate()) : ending.getDate();
      let month = ending.getMonth() < 9 ? ('0' + (ending.getMonth() + 1)) : ending.getMonth() + 1;
      endingDateOnly = ending.getFullYear() + '-' + month + '-' + date;
      endingDate = endingDateOnly + ' 23:59:59';
    }
    var dateFilter = {};
    if (startingDate) {
      dateFilter.from_date = startingDate;
    }
    if (endingDate) {
      dateFilter.to_date = endingDate;
    }
    let assignData = this.state.searchData;
    assignData.from_date = startingDate;
    assignData.to_date = endingDate;
    assignData.in_hand = '';
    const userId = this.context.user.id;
    this.setMyPageLoader(true);
    if (this.state.searchAdvance == false) {
      assignData['searches'] = '';
    }
    let searchedData = await getOperationFilterListings(userId, assignData);
    var eventCountDetail = await getEventCount(userId, assignData);
    var eventCount = 0;
    if (eventCountDetail && eventCountDetail.countInfo) {
      eventCount = parseInt(eventCountDetail.countInfo.count)
    }
    this.setMyPageLoader(false);
    this.setState({ searchData: assignData, from_dateOnly: startingDateOnly, to_dateOnly: endingDateOnly, singleDate: '', purchaseDate:'', operationListingdata: searchedData, operationListingSearch: true, eventCount });

  }


  /**
   * Refresh the listing based on search filter suggestion onclick
  */
   async selectListing(keyword = false) {
    const userId = this.context.user.id;
    let allSearchData = this.state.searchData;
    allSearchData.searches = keyword.toString();
    this.setMyPageLoader(true);
    let searchedData = await getOperationFilterListings(userId, this.state.searchData);
    var eventCountDetail = await getEventCount(userId, this.state.searchData);
    var eventCount = 0;
    if (eventCountDetail && eventCountDetail.countInfo) {
      eventCount = parseInt(eventCountDetail.countInfo.count)
    }
    this.setMyPageLoader(false);
    await this.setState({ operationListingdata: searchedData,  operationListingSearch: true, searches: allSearchData, suggestions: null, displayText: keyword.toString(), searchAdvance: true, eventCount });
  }

    /**
   * Reset the values of advanced search filter
   */
     async advanceFilterReset() {
      var searchData = {
        searches: "",
        tags: "",
        section: "",
        row: "",
        quantity: '',
        from_date: this.state.searchData.from_date,
        to_date: this.state.searchData.to_date,
        day_of_week: "",
        in_hand: "",
        inventory_attached: "",
        ticket_type: "",
        category: "",
        hold: '',
        isSearchEnable: false
      }
      var multiSelect = {
        ticketType: [],
        genre: [],
        inv_attach: [],
        row: [],
        quantity: [],
        split:[],
        userAssignedTo:[],
        company:[],
        ticketingSystemAccount:[],
      };
      var changeState = { searchData: searchData, selectedDays: [], operationListingdata: [], operationListingSearch: false, singleDate: null, purchaseDate:null,selectedTags: [], multiSelect: multiSelect, advanceFilterSubmit: false};
      await this.setState(changeState);
      await this.advanceFilterSubmit();
    }
/**
   * Submit the values of advanced search filter
   */
 async advanceFilterSubmit() {
  const userId = this.context.user.id;
  let stateHolder = this.state.searchData;
  
  var filteredData = await getOperationFilterListings(userId, stateHolder);
  this.setMyPageLoader(false);
  
   this.setState({ operationListingdata: filteredData, operationListingSearch: true, filterListingsData: filteredData, searchData: stateHolder, advanceFilterSubmit: true, advancefilter: false });

}
  
  render() {
    const { suggestions } = this.state
    
    const tagValues = [];
    this.state.getTags && this.state.getTags.length > 0
      && this.state.getTags.map((tag, i) => {
        var combined = { label: tag.tag_name, value: tag.id }
        tagValues.push(combined);
      })
    const accountsList = [];
    this.state.ticketingSystemAccountDetails && this.state.ticketingSystemAccountDetails.length > 0
      && this.state.ticketingSystemAccountDetails.map((item, i) => {
        var combined = { label: item.email, value: item.id }
        accountsList.push(combined);
      })
    const allUsersList = [];
    this.state.allUserDetails && this.state.allUserDetails.length > 0
      && this.state.allUserDetails.map((user, i) => {
        var combined = { label: user.email, value: user.id }
        allUsersList.push(combined);
      })
    const companiesList = [];
    this.state.allCompaniesList && this.state.allCompaniesList.length > 0 && this.state.allCompaniesList.map((company) => {
        var combined = { label: company.company_name, value: company.id }
        companiesList.push(combined);
    })
    return (
      <>
        <div className="search-item">
          <div className="search-form">
            <form onSubmit={e => { e.preventDefault(); }}>
              <div className="input-group form-group" style={{ position: 'relative' }}>
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <span className="ant-input-suffix">
                      <SearchIcon />
                    </span>
                  </span>
                </div>
                <input type="text" className="form-control" value={this.state.displayText} onChange={this.listSearchChange} placeholder="Search by Event ID, Performer, Venue and City, Tags, Owner, PO ID, External Reference ID"/>
                {suggestions && suggestions.length > 0 &&
                    < div className="TypeAheadDropDown">
                      {suggestions && this.state.locationText == '' &&
                        this.renderSuggestions()
                      }
                    </div>
                }
                {this.state.searchText &&
                          <button className="search-close-icon" onClick={(e) => this.listSearchChange()} >X</button>
                        }
              </div>
            </form>
          </div>
          <div className="date-range">
            <div className="date-filter-itemQr">
              <div className="form-group" style={{'position' : 'relative'}}>
                <form>
                  <div className="date-filter-top d-flex align-items-end">
                    <div className="d-flex">
                      <div className="date--PickQr">
                        <span className="custom-calendar-img">
                          <img src={iconInhand}  alt="caldendar icon"/>
                        </span>
                        <div>
                          <DatePicker
                            selectsRange={true}
                            startDate={this.state.startDate}
                            endDate={this.state.endDate}
                            onChangeRaw={(e) => this.avoidOnchange(e)}
                            onChange={this.dateRangeDisplay}
                            isClearable={true}
                            dateFormat="dd/MM/yyyy"
                            placeholderText=" DD/MM/YYYY - DD/MM/YYYY"
                          />
                        </div>
                      </div>
                      <div className="filter-icon" onClick={this.toggleAdvanced}>
                        <Filter />
                      </div>
                    </div>
                    
                    <div className="form-group ml-auto mb-0">
                      <Link to="/events/find">
                        <button type="button" className="btn btn-primary stylefont-weight-medium" >Can't find my Event</button>
                      </Link>
                    </div>
                    <div className={this.state.advancefilter ? "fliter-bottom listing-filter-item active" : "fliter-bottom listing-filter-item"} style={{ 'transition': '2s' }}>
                            <div className="close-filter">
                              <Button className="filter-close-btn" type="button" onClick={(e)=>  this.setState({ advanceFilterSubmit: true, advancefilter: false }) } ><CloseIcon /></Button>
                            </div>
                                  <label className="top-label-text">Stock Type</label>
                            <div className="row">
                              <div className="col-lg-7 col-md-12 col-12">
                                <div className="form-group">
                                  <div className="eticket-checkbox-sec">
                                    {this.state.ticketTypeDetails && this.state.ticketTypeDetails.length>0 && this.state.ticketTypeDetails.map((ticket)=>
                                    {return (<div className="form-check">
                                      <input type="radio" className="form-check-input" name="ticketType" value={ticket.id} onClick={(e) => this.advanceFilter(e, listingConstant.ticket_type)} checked={this.state.searchData.ticket_type == ticket.id ? true : false} />
                                      <label className="form-check-label">{ticket.name}</label>
                                    </div>)}
                                    )}
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-5 col-md-12 col-12">
                                <div className="d-flex inventory-hold-sec operation-filter-item">
                                  <div className="form-group hold-filter-item">
                                    <label>Viagogo Hold</label>
                                    <div className="yes-no-checkbox">
                                      <div className="form-check">
                                        <input type="radio" name="hold" className="form-check-input" onClick={(e) => this.advanceFilter(e, listingConstant.hold)} checked={this.state.searchData.hold == listingConstant.yesValue ? true : ''} value={listingConstant.yesValue} />
                                        <label className="form-check-label">Yes</label>
                                      </div>
                                      <div className="form-check">
                                        <input type="radio" name="hold" className="form-check-input" onClick={(e) => this.advanceFilter(e, listingConstant.hold)} checked={this.state.searchData.hold == listingConstant.noValue ? true : ""} value={listingConstant.noValue} />
                                        <label className="form-check-label">No</label>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="form-group invendory-attach-item" style={{width:'100%'}}>
                                    <label>Flagged</label>
                                    <div className="yes-no-checkbox">
                                      <div className="form-check">
                                        <input type="radio" name="inventory" className="form-check-input" onClick={(e) => this.advanceFilter(e, listingConstant.inventory_attached, listingConstant.inv_attach)} checked={this.state.searchData.inventory_attached && this.state.searchData.inventory_attached == listingConstant.yesValue ? true : false} value={listingConstant.yesValue} />
                                        <label className="form-check-label">Yes</label>
                                      </div>
                                      <div className="form-check">
                                        <input type="radio" name="inventory" className="form-check-input" onClick={(e) => this.advanceFilter(e, listingConstant.inventory_attached, listingConstant.inv_attach)} checked={this.state.searchData.inventory_attached && this.state.searchData.inventory_attached == listingConstant.noValue ? true : false} value={listingConstant.noValue} />
                                        <label className="form-check-label">No</label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-7 col-md-12 col-12">
                                <div className="form-group filter-datepicker">
                                  <label>Purchase Date</label>
                                  <div className="single-datepicker">
                                    <span className="custom-calendar-img">
                                      <img src={iconInhand} />
                                    </span>
                                    <DatePicker
                                      selectsRange={false}
                                      selected={this.state.purchaseDate}
                                      className="dateSinglepicker"
                                      onChange={(e) => this.advanceFilter(e, listingConstant.purchaseDate)}
                                      isClearable={this.state.purchaseDate ? true : false}
                                      onKeyDown={(e) => this.avoidOnchange(e)}
                                      minDate={new Date(this.state.from_dateOnly)}
                                      maxDate={new Date(this.state.to_dateOnly)}
                                      dateFormat="dd/MM/yyyy"
                                      placeholderText=" DD/MM/YYYY"
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-5 col-md-12 col-12">
                                <div className="form-group filter-datepicker">
                                  <label>In-Hand Date</label>
                                  <div className="single-datepicker">
                                    <span className="custom-calendar-img">
                                      <img src={iconInhand} />
                                    </span>
                                    <DatePicker
                                      selectsRange={false}
                                      selected={this.state.singleDate}
                                      className="dateSinglepicker"
                                      onChange={(e) => this.advanceFilter(e, listingConstant.in_hand)}
                                      isClearable={this.state.singleDate ? true : false}
                                      onKeyDown={(e) => this.avoidOnchange(e)}
                                      minDate={new Date(this.state.from_dateOnly)}
                                      maxDate={new Date(this.state.to_dateOnly)}
                                      dateFormat="dd/MM/yyyy"
                                      placeholderText=" DD/MM/YYYY"
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-7 col-md-12 col-12">
                                <div className="form-group genre-dropdown gener-select-item">
                                  <label>Ticketing System Account</label>
                                  
                                  <Select
                                    placeholder={"Select Account"}
                                    filterOption={this.dropdownFilterOption}
                                    value={this.state.multiSelect.ticketingSystemAccount}
                                    options={accountsList}
                                    onChange={(e) => this.advanceFilter(e, listingConstant.category, listingConstant.ticketingSystemAccount)}
                                    allowClear
                                    showSearch
                                    isClearable
                                  />
                                </div>
                              </div>
                              <div className="col-lg-5 col-md-12 col-12">
                                <div className="form-group genre-dropdown gener-select-item">
                                  <label>Split Type</label>
                                  
                                  <Select
                                    placeholder={"Select Split Type"}
                                    filterOption={this.dropdownFilterOption}
                                    value={this.state.multiSelect.split}
                                    options={this.state.allSplits}
                                    onChange={(e) => this.advanceFilter(e, listingConstant.category, listingConstant.split)}
                                    allowClear
                                    showSearch
                                    isClearable
                                  />
                                </div>
                              </div>
                              <div className="col-lg-7 col-md-12 col-12">
                                <div className="d-flex section-operation-row-item ">
                                  <div className="form-group section-filter-item genre-dropdown">
                                    <label>Owner</label>
                                    <Select
                                    placeholder={"Select Owner"}
                                    filterOption={this.dropdownFilterOption}
                                    value={this.state.multiSelect.company}
                                    options={companiesList}
                                    onChange={(e) => this.advanceFilter(e, listingConstant.category, listingConstant.company)}
                                    allowClear
                                    showSearch
                                    isClearable
                                  />
                                  </div>
                                  <div className="form-group section-filter-item ml-auto genre-dropdown">
                                    <label>Assigned To</label>
                                    <Select
                                    placeholder={"Select User"}
                                    filterOption={this.dropdownFilterOption}
                                    value={this.state.multiSelect.userAssignedTo}
                                    options={allUsersList}
                                    onChange={(e) => this.advanceFilter(e, listingConstant.category, listingConstant.userAssignedTo)}
                                    allowClear
                                    showSearch
                                    isClearable
                                  />
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-5 col-md-12 col-12">
                                <div className="form-group tag-dropdown genre-dropdown gener-select-item">
                                  <label>Tags</label>
                                  <Select
                                    {...this.props}
                                    placeholder={"Tags"}
                                    style={{ width: "100%" }}
                                    name="tags"
                                    filterOption={this.dropdownFilterOption}
                                    value={lodashFilter(tagValues, tag =>lodashIncludes(lodashGet(this.state, `searchData.${listingConstant.tagIds}`, []), Number(tag.value)))}
                                    options={tagValues}
                                    onBlur={true}
                                    onChange={(e) => this.advanceFilter(e, listingConstant.tagIds)}
                                    // className={(errors.tags && touched.tags ? ' is-invalid' : '')}
                                    allowClear
                                    showSearch
                                    isClearable
                                    isMulti
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="submit-reset-btn-item">
                              <button className={this.state.searchData.isSearchEnable ? "btn filter-submit-btn primary-bg-color active" : 'btn filter-submit-btn primary-bg-color'} type="button" onClick={this.advanceFilterSubmit}>Submit</button>
                              <a type="button" onClick={this.advanceFilterReset} className="btn filter-reset-btn">Reset</a>
                            </div>
                      </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        {/* <div className="mt-4">
          <OperationGird 
          operationListingdata= {this.state.operationListingdata}
          operationListingSearch={this.state.operationListingSearch}
          searchData = {this.state.searchData}
          />
        </div> */}
        <div className="mt-4">
          <OperationListingGrid 
            operationListingdata = {this.state.operationListingdata}
            operationListingSearch = {this.state.operationListingSearch}
            filterParams = {this.state.searchData}
            filterListingData = {this.state.filterListingsData}
            renderPage = {this.advanceFilterSubmit}
          />
        </div>
      </>    
    );
  }
}

export default (OperationFilter)
