
import React, { Component, createRef } from 'react';
import Select from 'react-select';

export default class DisclosureDropdown extends Component {
  constructor(props) {
    super(props);

    this.inputRef = createRef();
    this.state = {
      value: props.data.disclosureValue,
    };
  }

  componentDidMount() {
    setTimeout(() => this.inputRef.current.focus());
  }

   onChangeHandler = (event) => {
     this.setState({ value: event })
  };

  // the final value to send to the grid, on completion of editing
  getValue() {
    return this.state.value ;
  }

  // Gets called once before editing starts, to give editor a chance to
  // cancel the editing before it even starts.
  isCancelBeforeStart() {
    return false;
  }  
  
  render() {
    return (
     
        <Select
            ref={this.inputRef}
            placeholder={"Disclosure"}
            style={{ width: "100%" }}
            name="disclosure"
            className="ag-grid-select-dropdown"
            value={this.state.value}
            options ={this.props.option}
            onChange={(event) => this.onChangeHandler(event)}
            allowClear
            showSearch
            isClearable
            isMulti
            menuPlacement="auto"
        />

    );
  }
}