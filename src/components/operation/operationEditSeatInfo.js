
import React,{useState} from "react";

    function OperationEditSeatInfo(props) { 
        let [stateSeatInfo,setSeatInfo] = useState({seatStart:props.seatNumberInfo.seatStart,seatEnd:props.seatNumberInfo.seatEnd});

        function onTabChange(e){
            if(e.keyCode==9){
                props.props.api.setFocusedCell(7, true);
            }
        }

        return (
            <div className="edit-cancel-column">
                <div className="seat-start-item">
                    <label>Seat Start:</label>
                    <input className="form-control" min={0} type="number" value={stateSeatInfo&&stateSeatInfo.seatStart?stateSeatInfo.seatStart:0} onChange={async (e) =>{
                        await setSeatInfo({...stateSeatInfo,seatStart:e.target.value});
                        props.onSeatChange({seatStart:e.target.value, seatEnd:stateSeatInfo&&stateSeatInfo.seatEnd?stateSeatInfo.seatEnd:props.seatNumberInfo.seatEnd}) 
                        }} 
                        placeholder="" />
                </div>
                <div className="seat-start-item">
                    <label>Seat End:</label>
                    <input className="form-control" id="seatEnd" type="number" autoFocus min={stateSeatInfo&&stateSeatInfo.seatStart?stateSeatInfo.seatStart:props.seatNumberInfo.seatStart} value={stateSeatInfo&&stateSeatInfo.seatEnd?stateSeatInfo.seatEnd:stateSeatInfo.seatStart} 
                    onChange={async (e) =>{
                        await setSeatInfo({...stateSeatInfo,seatEnd:e.target.value});
                        props.onSeatChange({seatStart:stateSeatInfo&&stateSeatInfo.seatStart?stateSeatInfo.seatStart:props.seatNumberInfo.seatStart, seatEnd:e.target.value}) 
                        }} 
                        placeholder="" onKeyDown = {(e) => onTabChange(e) } />
                </div>
            </div>
        );
    }

    export { OperationEditSeatInfo };
