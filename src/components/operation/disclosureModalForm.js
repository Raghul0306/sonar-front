import React from "react";
import { Modal, Button } from 'react-bootstrap';
import Select from 'react-select';

function DisclosureModalForm(params) {
    return <>
      {params && params.showModal===true ? 
        <Modal show={params.showModal} onHide={(e) => params.hideModal(false)} className="mytickets-list-popup opertaion-disclosure-popup" backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Update Disclosures</Modal.Title>
          </Modal.Header>
          <Modal.Body>            

            <div className="di-form-items">
              <div className="form-group add-listing-form-group multi-select-item">
                <label>Disclosures</label>              
                <Select
                  placeholder={"Disclosures"}
                  style={{ width: "100%" }}
                  className={`${params.validation.disclosure ? 'is-invalid' : ''}`}
                  name="disclosures"
                  value={params.selectedDisclosures && params.selectedDisclosures.length ? JSON.parse(params.selectedDisclosures) : []}
                  options={params.disclosuresList}
                  onChange={(e) => params.disclosureOnChange(e)}
                  allowClear
                  showSearch
                  isClearable
                  isMulti
                />
              </div>
              {
                params.validation.disclosure ? <div className="invalid-feedback" style={{ display: "block" }}> Disclosure is required</div> : ''
              }
            </div>
            
            <div className="form-group">
              <label>External Notes</label>
              <textarea className="form-control" value={params.externalNotes} onChange={params.handleExternalNotesChange} />
            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button className={params.selectedDisclosures.length==0 ? "" : "active"} variant="primary" disabled={params.selectedDisclosures.length==0 ? true:false} onClick={(e) => params.hideModal(true)}>
              Update
            </Button>
          </Modal.Footer>
        </Modal> 
      : '' }

    {params && params.disclosureConfirmationModal===true ? 
      <Modal show={params.disclosureConfirmationModal} onHide={(e) => params.updateDisclosuresModal(false)} className="mytickets-list-popup" backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Disclosures Confirmation</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 className="disclosure-content-view">Update the disclosures on all listings:</h5>

            <ul className="disclosures-item-body">
              {
                params.disclosuresLabel && params.disclosuresLabel.length && params.disclosuresLabel.map((item, i) => {
                  return (
                    <li key={i} className="disclosure-list-text"> {item}</li>
                  )
                })

              }
            </ul>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={(e) => params.updateDisclosuresModal(false)} >Cancel</Button>
            <Button className="btn btn-success info-btn-listing" onClick={() => params.updateDisclosuresModal(true)}>Submit</Button>

          </Modal.Footer>
        </Modal>
        : '' }
    </>;
}

export { DisclosureModalForm };