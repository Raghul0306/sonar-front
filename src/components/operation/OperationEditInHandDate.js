import React, { Component, createRef } from 'react';
import { dateValidationString, backButtonEvent, operationGridConstant } from "../../constants/constants";

export default class OperationEditGridDate extends Component {
  constructor(props) {
    super(props);

    this.inputRef = createRef();
    if(props.type== operationGridConstant.inHandDate)
    {
        this.state = {
            value: props.data.inHandDateValue,
        };
    }else{
        this.state = {
            value: props.data.purchaseDateValue,
        };
    }   
  }

  componentDidMount() {
    setTimeout(() => this.inputRef.current.focus());
  }

    onChangeHandler = (event) => {
        this.setState({ value: event })
    };

  // the final value to send to the grid, on completion of editing
  getValue() {
    return this.state.value ;
  }

  // Gets called once before editing starts, to give editor a chance to
  // cancel the editing before it even starts.
  isCancelBeforeStart() {
    return false;
  }  

  // Gets called once when editing is finished
  isCancelAfterEnd() {
    return !(dateValidationString.test(this.state.value));
  }
  
  validateNumberAndForwardSlash(event) {
    var key = window.event ? event.keyCode : event.which;
    var isValid = false;
    if (key >= 48 && key <= 57 || key >= 96 && key <= 105 || key == 8 || key == 46 || key == 110 || key == 190 ) {
        isValid =  true;
    }else{
        event.preventDefault();
    } 
    return isValid;
 }

 async handleOnChange(e) {
        
    if(e.target.value.length==1 && parseInt(e.target.value) > 3 && e.nativeEvent.inputType!=backButtonEvent) {
        var value = 0 +  e.target.value + "/"; 
        this.setState({value:value})
    }else if((e.target.value.length==2 || e.target.value.length==5) && e.nativeEvent.inputType!=backButtonEvent) {
        var inHandValue = e.target.value + "/"; 
        this.setState({value:inHandValue})
    }
    else if(e.target.value.length==4){
        var lastCharacter = e.target.value.substr(e.target.value.length - 1);
        if(parseInt(lastCharacter)>1){
            var value = 0 +  lastCharacter + "/"; 
            value = e.target.value.slice(0, -1) + value;
            this.setState({value:value})
        }else{
            this.setState({value:e.target.value})
        }
    }
    else{
        this.setState({value:e.target.value})
    }
}

  render() {
    return (
        <input className="form-control" ref={this.inputRef} id="inHandDate" maxLength={10} type="text" value={this.state.value} onChange={ (e) => this.handleOnChange(e)} placeholder="DD-MM-YYYY" onKeyDown={ (e) => this.validateNumberAndForwardSlash(e)}  />
    );
  }
}