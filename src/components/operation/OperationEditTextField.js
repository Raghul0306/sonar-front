import React, { Component, createRef } from 'react';
import { operationConstantData, operationGridConstant } from "../../constants/constants";

export default class OperationEditTextField extends Component {
  constructor(props) {
    super(props);

    this.inputQuantity = createRef();
    this.inputDigit = createRef();
    this.inputCost = createRef();

    if(props.type== operationGridConstant.quantity)
    {
        this.state = {
            value: props.data.quantity,
        };
    }else if(props.type== operationGridConstant.last4Digits){
        this.state = {
            value: props.data.lastDigits,
        };
    }else if(props.type== operationConstantData.cost){
        this.state = {
            value: parseFloat(props.data.cost).toFixed(2),
        };
    } 
  }

  componentDidMount() {
    if(this.props.type== operationGridConstant.quantity)
    {
        setTimeout(() => {
          this.inputQuantity.current.focus()
          this.inputQuantity.current.select()
        });
    }else if(this.props.type== operationGridConstant.last4Digits){
        setTimeout(() => {
          this.inputDigit.current.focus()
          this.inputDigit.current.select()
        });
    }else if(this.props.type== operationConstantData.cost){
        setTimeout(() => {
          this.inputCost.current.focus()
          this.inputCost.current.select()
        });
    }
  }

  /* Component Editor Lifecycle methods */
  // the final value to send to the grid, on completion of editing
  getValue() {
    return this.state.value;
  }

  // Gets called once before editing starts, to give editor a chance to
  // cancel the editing before it even starts.
  isCancelBeforeStart() {
    return false;
  }

  // Gets called once when editing is finished 
  // If you return true, then the result of the edit will be ignored.
  isCancelAfterEnd() {
    if(this.props.type== operationGridConstant.quantity)
    {
        return this.state.value < 1;
    }else if(this.props.type== operationGridConstant.last4Digits){
        return this.state.value.length != 4;
    }else if(this.props.type== operationConstantData.cost){
        return this.state.value < 1;
    }
    
  }

  validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    var isValid = false;
    if(this.props.type== operationConstantData.cost){
        if (key >= 48 && key <= 57 || key >= 96 && key <= 105 || key == 8 || key == 46 || key == 110 || key == 190 ) {
            isValid =  true;
        }else{
            event.preventDefault();
        } 
    }else{
        if (key >= 48 && key <= 57 || key >= 96 && key <= 105 || key == 8 || key == 46 ) {
            isValid =  true;
        }else {
            event.preventDefault();
        } 
    }
    return isValid;
 }

  render() {
    if(this.props.type== operationGridConstant.quantity)
    {
        return (
            <input
              className="form-control"
              type="text"
              ref={this.inputQuantity}
              value={this.state.value}
              onChange={(event) => this.setState({ value: event.target.value })}
              style={{ width: '100%' }}
              onKeyDown={ (e) => this.validateNumber(e)}
            />
          );
    }else if(this.props.type== operationGridConstant.last4Digits){
        return (
            <input
              className="form-control"
              type="text"
              maxLength={4}
              ref={this.inputDigit}
              value={this.state.value}
              onChange={(event) => this.setState({ value: event.target.value })}
              style={{ width: '100%' }}
              onKeyDown={ (e) => this.validateNumber(e)}
            />
          );
    }else if(this.props.type== operationConstantData.cost){
        return (
            <input
              className="form-control"
              type="text"
              ref={this.inputCost}
              maxLength={6}
              value={this.state.value}
              onChange={(event) => this.setState({ value: event.target.value })}
              style={{ width: '100%' }}
              onKeyDown={ (e) => this.validateNumber(e)}
            />
          );
    }
    
  }
}
