import React, { Component } from "react";
import barcodeIcon from "../../assets/operation-icon/Operations_Barcode-Inactive@3x.png"
import flagIcon from "../../assets/operation-icon/Operations_unflagged@3x.png"
import notesIcon from "../../assets/operation-icon/Operations_Notes-Inactive@3x.png"
import pdfIcon from "../../assets/operation-icon/Operations_PDF-Inactive@3x.png"
import tickIcon from "../../assets/operation-icon/Operations_QC@3x.png"
import qcActiveIcon from "../../assets/operation-icon/Operations_QC-Check@3x.png"
import qcNotesActiveIcon from "../../assets/operation-icon/Operations_Notes-Active@3x.png"
import viagoholdIcon from "../../assets/operation-icon/Operations_viagogo@3x.png"
    
    export default class OperationSettingButton extends Component {
      constructor(props) {
        super(props);
        
        this.state = {
            qcModalStatus: props.qcModalStatus,
            notesModal:props.notesModal
        };
      }
    
    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps.qcModalStatus !== state.qcModalStatus ) {
            return {
              ...state,
              qcModalStatus: nextProps.qcModalStatus
            };
        }else{
          return null;
        }
    }

      render() {
        return (
            <>
                <button type="button" className="report-edit-btn operation-edit-btn btn" > <img src={barcodeIcon}/> </button>
                <button type="button" className="report-edit-btn operation-edit-btn btn" > <img src={pdfIcon}/> </button>
                <button type="button" className="report-edit-btn operation-edit-btn btn" > <img src={viagoholdIcon}/> </button>
                {/* <button type="button" className="report-edit-btn operation-edit-btn btn" title="Pdf"> <img src={shareIcon}/> </button> */}

                <button type="button" className="report-edit-btn operation-edit-btn btn" onClick={() => { this.props.handleNotesOnClick(); this.setState({notesModal:true})} } > 
                    {this.props.selectedListingData && this.props.props && this.props.props.data && this.props.props.data.listingGroupId && this.props.selectedListingData.listingGroupId===this.props.props.data.listingGroupId ? 
                        <img src={this.state.notesModal ? qcNotesActiveIcon : notesIcon}/>  : <img src={notesIcon}/> 
                    }
                </button>

                <button type="button" className="report-edit-btn operation-edit-btn btn" onClick={() => { this.props.click(); this.setState({qcModalStatus:true})} } > 
                    {this.props.selectedListingData && this.props.props && this.props.props.data && this.props.props.data.listingGroupId && this.props.selectedListingData.listingGroupId===this.props.props.data.listingGroupId ? 
                        <img src={this.state.qcModalStatus ? qcActiveIcon : tickIcon}/>  : <img src={tickIcon}/> 
                    }
                </button>
                <button type="button" className="report-edit-btn operation-edit-btn btn" > <img src={flagIcon}/> </button>
            </>
        );
      }
      
    }