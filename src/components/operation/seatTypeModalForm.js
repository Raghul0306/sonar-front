import React from "react";
import { Modal, Button } from 'react-bootstrap';
import "react-datepicker/dist/react-datepicker.css";
import Select from 'react-select'

function SeatTypeModalForm(params) {
      return <>
      {params && params.showModal===true ? 
        <Modal className="inHandmodal operation-inhandmodal" show={params.showModal} onHide={() => params.hideModal(false) } backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Update Seat Type</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="di-form-items form-group add-listing-form-group multi-select-item">
              <label>Seat Type</label>
              <div>
                  <Select
                  placeholder={"Select Seat Type"}
                  style={{ width: "100%" }}
                  name="seat_type"
                  value={params.selectedSeatTypes && params.selectedSeatTypes.length ? params.selectedSeatTypes : []}
                  options={params.ticketTypeOption}
                  onChange={(e) => params.ticketTypesOnchange(e)}
                  allowClear
                  showSearch
                  isClearable
                  /> 
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer className="modal-btn-align">
            <Button className={params.selectedSeatTypes && params.selectedSeatTypes.length ? "active" : ""} variant="primary" disabled={params.selectedSeatTypes && params.selectedSeatTypes.length ? false:true} onClick={(e) => params.hideModal(true)}>
            Update
            </Button>
          </Modal.Footer>
        </Modal>   
        : '' }

      {params && params.showSeatTypeConfirmation===true ? 

     
        <Modal className="price-update-modal price-confirmation--modal" show={params.showSeatTypeConfirmation} onHide={(e) => params.hideConfirmationModal(false)} backdrop="static">
        <Modal.Header closeButton>
            <Modal.Title>Seat Type Confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <h4 className="change-date-text stylefont-weight-bold">Add all Seat Types in the event to <span>{params.selectedSeatTypes && params.selectedSeatTypes.length ? params.selectedSeatTypes[0].label : ''}</span></h4>

        </Modal.Body>
        <Modal.Footer className="modal-btn-align">
            <Button variant="secondary" onClick={(e) => params.hideConfirmationModal(false)}>
            Cancel
            </Button>
            <Button variant="primary" onClick={(e) => params.hideConfirmationModal(true)}>
            Submit
            </Button>
        </Modal.Footer>
        </Modal>
      : '' }

    </>
    }

export { SeatTypeModalForm };