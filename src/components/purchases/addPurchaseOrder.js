// eslint-disable-next-line
import React from "react";
import Input from 'antd/es/input';
import Form from 'antd/es/form';
import Button from 'antd/es/button';
import DatePicker from 'antd/es/date-picker';
import AutoComplete from 'antd/es/auto-complete';
import Select from 'antd/es/select';
import UserContext from "../../contexts/user.jsx";
import { withTranslation } from "react-i18next";
import {
  createPurchaseOrder,
  createVendor,
  getVendorsByName,
  addListingIdToPurchaseOrderById
} from "../../services/purchaseOrderService";
import { updateListingPurchaseOrderId } from "../../services/listingsService";
import CloseRoundedIcon from "@material-ui/icons/CloseRounded";
import moment from "moment";
import { getPerformerNamesFromPerformerIDList } from "../../helpers/IDListParser";
const { Option } = Select;

const toolTipStyle = {
  fontSize: "18px",
  marginLeft: "5px",
  paddingBottom: "3px",
  color: "rgba(89,89,89, 0.4)"
};

// for dyanmic forms
let id = 0;

class AddPurchaseOrder extends React.Component {
  static contextType = UserContext;
  constructor(props) {
    super(props);
    this.state = {
      vendorList: [],
      selectedVendor: null,
      lineItemFields: 0,
      initialDescriptionValue: false,
      initialQuantity: false,
      initialCost: false,
      fieldValue: null
    };
  }
  /**
   * Used to determine if we are in a "preloaded" Purchase Order or not.
   */
  isDataPassedIn() {
    return this.props.selectedCard && this.props.purchaseOrderListing;
  }

  onValuesChange = ({ fieldName }) => {
    if (!this.state.fieldValue && fieldName) {
      this.setState({
        fieldValue: fieldName
      });
    }
  };
  componentDidMount() {
    // To disable submit button at the beginning.
    //this.props.form.validateFields();
    // create 1 line item when we load in
    this.add();
    // pre-fill the fields only for the first line items.
    if (this.isDataPassedIn()) {
      this.buildItemDescription().then(response => {
        this.setState({
          initialDescriptionValue: response,
          initialQuantity: 1,
          initialCost: this.props.formData.cost
        });
      });
    }
  }
  onSearchInputChange = async searchString => {
    if (searchString.length > 3) {
      // if we limit the amount of vendors we query to 50, the search bar is much more performant
      this.setState(
        {
          vendorList: await getVendorsByName(searchString, 50)
        },
        async () => {
          // populate autocomplete vendor options
          this.setState({
            vendorList: searchString.length < 4 ? [] : this.state.vendorList
          });
        }
      );
    }
  };
  /**
   * Builds the Item Description from the selected card
   * sometimes passed into the purchase order form.
   */
  buildItemDescription = async () => {
    let listingTitle = "";
    if (this.props.selectedCard.setBySet.year) {
      listingTitle += this.props.selectedCard.setBySet.year.toString();
    }
    if (this.props.selectedCard.brandByBrand.name) {
      listingTitle += " " + this.props.selectedCard.brandByBrand.name;
    }
    if (this.props.selectedCard.setBySet.name) {
      listingTitle += " " + this.props.selectedCard.setBySet.name;
    }
    if (this.props.selectedCard.performer) {
      listingTitle +=
        " " +
        (await getPerformerNamesFromPerformerIDList(
          this.props.selectedCard.performer
        ));
    }
    if (this.props.selectedCard.cardNumber) {
      listingTitle += " #" + this.props.selectedCard.cardNumber;
    }
    return listingTitle;
  };
  add = () => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue("keys");
    const nextKeys = keys.concat(id++);
    // can use data-binding to set
    // important! notify form to detect changes
    form.setFieldsValue({
      keys: nextKeys
    });
  };
  remove = k => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue("keys");
    // We need at least one passenger
    if (keys.length === 1) {
      return;
    }
    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k)
    });
  };
  onSelect = vendor => {
    // let the parent copmonent know we have deselected a vendor if we've previously selected one
    this.setState(
      {
        selectedVendor: this.state.vendorList.find(x => x.name === vendor)
      },
      () => {
        // fill in the fields here
        this.props.form.setFieldsValue({
          vendorAddress: this.state.selectedVendor.shippingAddress,
          vendorAddress2: this.state.selectedVendor.shippingAddress2,
          vendorCity: this.state.selectedVendor.shippingAddressCity,
          vendorState: this.state.selectedVendor.shippingAddressState,
          vendorZip: this.state.selectedVendor.shippingAddressZip,
          vendorCountry: this.state.selectedVendor.shippingAddressCountry,
          vendorPhone: this.state.selectedVendor.phone,
          vendorEmail: this.state.selectedVendor.email
        });
      }
    );
  };
  submitForm = async e => {
    // step 1: get the values from the VALID form
    e.preventDefault();
    const { form } = this.props;
    const fieldValues = form.getFieldsValue();
    // can use data-binding to get
    const keys = form.getFieldValue("keys");
    // step 2: massage user input for DB writes
    let vendorData = {
      name: fieldValues.vendorName,
      shippingAddress: fieldValues.vendorAddress,
      shippingAddress2: fieldValues.vendorAddress2,
      shippingAddressZip: fieldValues.vendorZip,
      shippingAddressCity: fieldValues.vendorCity,
      shippingAddressState: fieldValues.vendorState,
      shippingAddressCountry: fieldValues.vendorCountry,
      phone: fieldValues.vendorPhone,
      email: fieldValues.vendorEmail
    };
    if (!fieldValues.purchaseDate) {
      alert("Purchase date is required.");
      return;
    }
    let purchaseOrderData = {
      sellerId: this.context.user.id,
      purchaseDate: fieldValues.purchaseDate ? fieldValues.purchaseDate : null,
      purchaseAmount: fieldValues.purchaseAmount,
      purchaseCurrency: "USD",
      paymentMethod: fieldValues.paymentType ? fieldValues.paymentType : null,
      paymentCardLast4: fieldValues.cardLast4 ? fieldValues.cardLast4 : null,
      paymentGroup: fieldValues.paymentGroup ? fieldValues.paymentGroup : null,
      listingId: fieldValues.listingId ? fieldValues.listingId : null,
      externalReference: fieldValues.notes ? fieldValues.notes : null,
      tags: fieldValues.tags ? fieldValues.tags : null,
      salesTax: fieldValues.salesTax ? fieldValues.salesTax : null,
      shippingandHandling: fieldValues.shipAndHandle
        ? fieldValues.shipAndHandle
        : null,
      fees: fieldValues.otherFees ? fieldValues.otherFees : null,
      // fill this with the field values, for each. ya know?
      lineItems: keys.map(key => {
        return {
          description: fieldValues.itemDescription[key],
          quantity: fieldValues.itemQuantity[key],
          cost: fieldValues.unitCost[key]
        };
      })
    };
    // step 3: create vendor if needed
    let vendor = this.state.selectedVendor;
    if (!this.state.selectedVendor) {
      vendor = await createVendor(
        vendorData.name,
        vendorData.phone,
        vendorData.email,
        vendorData.shippingAddress,
        vendorData.shippingAddress2,
        vendorData.shippingAddressZip,
        vendorData.shippingAddressCity,
        vendorData.shippingAddressState,
        vendorData.shippingAddressCountry
      );
    }

    // step 4: create purchase order, use vendor ID
    let purchaseOrder = await createPurchaseOrder(
      purchaseOrderData.sellerId,
      purchaseOrderData.purchaseDate,
      purchaseOrderData.purchaseAmount === ""
        ? 0
        : parseFloat(purchaseOrderData.purchaseAmount),
      purchaseOrderData.purchaseCurrency,
      purchaseOrderData.paymentMethod,
      purchaseOrderData.paymentCardLast4,
      purchaseOrderData.listingId,
      purchaseOrderData.paymentGroup,
      purchaseOrderData.notes ? purchaseOrderData.notes : "",
      vendor.id,
      purchaseOrderData.tags ? purchaseOrderData.tags : "",
      purchaseOrderData.lineItems,
      purchaseOrderData.salesTax,
      purchaseOrderData.fees,
      purchaseOrderData.shippingandHandling
    );
    // step 5: close modal upon successful completion
    if (purchaseOrder) {
      // make the listing and reference the created purchase order if we pre-populated fields
      if (this.isDataPassedIn()) {
        //const listingWithPurchaseOrderID = await this.createListingWithPurchaseOrder(purchaseOrder.id);
        // link the purchase order to the id.
        await addListingIdToPurchaseOrderById(
          purchaseOrder.id,
          this.props.purchaseOrderListing.id
        );
        await updateListingPurchaseOrderId(
          this.props.purchaseOrderListing.id,
          purchaseOrder.id
        );
      }
      if (typeof this.props.formComplete === "function") {
        this.props.formComplete();
      }
      // show success message
      this.isDataPassedIn()
        ? alert(this.props.t("purchases.listingAddSuccess"))
        : alert(this.props.t("purchases.addSuccess"));
      // try to refresh grid
      document.location.reload();
    }
  };
  render() {
    //const { getFieldDecorator, getFieldValue, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
    const {
      getFieldDecorator,
      getFieldValue,
      getFieldError,
      isFieldTouched
    } = this.props.form;
    const vendorNameError =
      isFieldTouched("vendorName") && getFieldError("vendorName");
    const purchaseAmountError =
      isFieldTouched("purchaseAmount") && getFieldError("purchaseAmount");
    const purchaseDateError =
      isFieldTouched("purchaseDate") && getFieldError("purchaseDate");
    const itemQuantityError =
      isFieldTouched("itemQuantity") && getFieldError("itemQuantity");
    const unitCostError =
      isFieldTouched("unitCost") && getFieldError("unitCost");
    // for dynamic form
    getFieldDecorator("keys", { initialValue: [] });
    const keys = getFieldValue("keys");
    const formItems = keys.map((k, index) => (
      <>
        <Form.Item key={`${k}1`} validateStatus="success">
          <span className="purchase-order-form-label">
            {this.props.t("purchases.itemDescription")}{" "}
          </span>
          {getFieldDecorator(`itemDescription[${k}]`, {
            validateTrigger: ["onChange", "onBlur"],
            initialValue: this.state.initialDescriptionValue
              ? this.state.initialDescriptionValue
              : null
          })(<Input placeholder={this.props.t("purchases.itemDescription")} />)}
        </Form.Item>
        <Form.Item
          key={`${k}2`}
          validateStatus={itemQuantityError ? "error" : ""}
          help={itemQuantityError || ""}
        >
          <span className="purchase-order-form-label">
            {this.props.t("purchases.quantity")}{" "}
          </span>
          {getFieldDecorator(`itemQuantity[${k}]`, {
            validateTrigger: ["onChange", "onBlur"],
            rules: [
              {
                required: true,
                message: this.props.t("purchases.errors.quantity")
              }
            ],
            initialValue: this.state.initialQuantity
              ? this.state.initialQuantity
              : null
          })(<Input placeholder={this.props.t("purchases.quantity")} />)}
        </Form.Item>
        <Form.Item
          key={`${k}3`}
          validateStatus={unitCostError ? "error" : ""}
          help={unitCostError || ""}
        >
          <span className="purchase-order-form-label">
            {this.props.t("purchases.unitCost")}{" "}
          </span>
          {getFieldDecorator(`unitCost[${k}]`, {
            validateTrigger: ["onChange", "onBlur"],
            rules: [
              {
                required: true,
                message: this.props.t("purchases.errors.unitCost")
              }
            ],
            initialValue: this.state.initialCost ? this.state.initialCost : null
          })(<Input placeholder={this.props.t("purchases.unitCost")} />)}
        </Form.Item>
        {k >= 1 && (
          <Button
            dashed
            style={{ margin: "5px 0 5px 0" }}
            onClick={() => this.remove(k)}
          >
            Remove these item fields
          </Button>
        )}
      </>
    ));
    return (
      <UserContext.Consumer>
        {context => (
          <div>
            <div className="modalWithHead-floating-cancel-button">
              <Button
                shape="circle"
                size="large"
                onClick={this.props.handleCancel}
              >
                <CloseRoundedIcon
                  style={{
                    fontSize: "23px"
                  }}
                />
              </Button>
            </div>
            <div
              className="popup-body"
              //  style={{ border: "1px solid green" }}
            >
              <Form
                layout="vertical"
                onSubmit={this.submitForm}
                name="createPurchaseOrder"
                // style={{ border: "1px solid yellow" }}
              >
                <div
                  className="purchase-order-card-container"
                  //   style={{ border: "1px solid blue" }}
                >
                  <div
                    style={{
                      width: "100%",
                      paddingRight: "1rem"
                      //   border: "1px solid red"
                    }}
                  >
                    <h5
                      className="purchase-order-form-headings secondary-font-family"
                      style={{ display: "inline-block" }}
                    >
                      Vendor
                    </h5>
                    <Form.Item
                      validateStatus={vendorNameError ? "error" : ""}
                      help={vendorNameError || ""}
                    >
                      <span className="purchase-order-form-label">
                        {this.props.t("vendor.name")}{" "}
                      </span>
                      {getFieldDecorator("vendorName", {
                        rules: [
                          {
                            required: true,
                            message: this.props.t("vendor.errors.name")
                          }
                        ]
                      })(
                        <AutoComplete
                          style={{ width: "100%" }}
                          dataSource={this.state.vendorList.map(
                            vendor => vendor.name
                          )}
                          onSearch={text => this.onSearchInputChange(text)}
                          onSelect={text => this.onSelect(text)}
                          placeholder={this.props.t("vendor.name")}
                        />
                      )}
                    </Form.Item>
                    <Form.Item validateStatus="success">
                      <span className="purchase-order-form-label">
                        {this.props.t("vendor.address")}{" "}
                      </span>
                      {getFieldDecorator(
                        "vendorAddress",
                        {}
                      )(<Input placeholder={this.props.t("vendor.address")} />)}
                    </Form.Item>
                    <Form.Item
                      validateStatus="success"
                      style={{ paddingBottom: "20px" }}
                    >
                      {getFieldDecorator(
                        "vendorAddress2",
                        {}
                      )(
                        <Input placeholder={this.props.t("vendor.address2")} />
                      )}
                    </Form.Item>
                  </div>
                  <div
                    style={{
                      width: "100%",
                      paddingLeft: "0.8rem"
                      //   border: "1px solid red"
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        // border: "1px solid green",
                        marginTop: "35px"
                      }}
                    >
                      <Form.Item
                        validateStatus="success"
                        style={{ width: "50%", paddingRight: "5px" }}
                      >
                        <span className="purchase-order-form-label">
                          {this.props.t("vendor.city")}{" "}
                        </span>
                        {getFieldDecorator(
                          "vendorCity",
                          {}
                        )(<Input placeholder={this.props.t("vendor.city")} />)}
                      </Form.Item>
                      <Form.Item
                        validateStatus="success"
                        style={{ width: "50%" }}
                      >
                        <span className="purchase-order-form-label">
                          {this.props.t("vendor.state")}{" "}
                        </span>
                        {getFieldDecorator(
                          "vendorState",
                          {}
                        )(<Input placeholder={this.props.t("vendor.state")} />)}
                      </Form.Item>
                    </div>
                    <div style={{ display: "flex", flexDirection: "row" }}>
                      <Form.Item
                        validateStatus="success"
                        style={{ width: "50%", paddingRight: "5px" }}
                      >
                        <span className="purchase-order-form-label">
                          {this.props.t("vendor.zip")}{" "}
                        </span>
                        {getFieldDecorator(
                          "vendorZip",
                          {}
                        )(<Input placeholder={this.props.t("vendor.zip")} />)}
                      </Form.Item>
                      <Form.Item
                        validateStatus="success"
                        style={{ width: "50%" }}
                      >
                        <span className="purchase-order-form-label">
                          {this.props.t("vendor.country")}{" "}
                        </span>
                        {getFieldDecorator(
                          "vendorCountry",
                          {}
                        )(
                          <Input placeholder={this.props.t("vendor.country")} />
                        )}
                      </Form.Item>
                    </div>
                    <div style={{ display: "flex", flexDirection: "row" }}>
                      <Form.Item
                        validateStatus="success"
                        style={{ width: "50%", paddingRight: "5px" }}
                      >
                        <span className="purchase-order-form-label">
                          {this.props.t("vendor.phone")}{" "}
                        </span>
                        {getFieldDecorator(
                          "vendorPhone",
                          {}
                        )(<Input placeholder={this.props.t("vendor.phone")} />)}
                      </Form.Item>
                      <Form.Item
                        validateStatus="success"
                        style={{ width: "50%" }}
                      >
                        <span className="purchase-order-form-label">
                          {this.props.t("vendor.email")}{" "}
                        </span>
                        {getFieldDecorator(
                          "vendorEmail",
                          {}
                        )(<Input placeholder={this.props.t("vendor.email")} />)}
                      </Form.Item>
                    </div>
                  </div>
                </div>
                <div>
                  {/* here */}

                  <h5
                    className="purchase-order-form-headings secondary-font-family"
                    style={{ display: "inline-block", marginBottom: "10px" }}
                  >
                    Payment Information
                  </h5>
                  <div
                    className="purchaseOrder-popup-card-body"
                    style={{ gridGap: "15px" }}
                  >
                    <Form.Item
                      validateStatus={purchaseDateError ? "error" : ""}
                      help={purchaseDateError || ""}
                    >
                      <span className="purchase-order-form-label">
                        {this.props.t("purchases.purchaseDate")}{" "}
                      </span>
                      {getFieldDecorator("purchaseDate", {
                        rules: [
                          {
                            required: true,
                            message: this.props.t("purchases.errors.date")
                          }
                        ],
                        initialValue: this.isDataPassedIn() ? moment() : null
                      })(
                        <DatePicker
                          style={{ width: "90%" }}
                          placeholder={this.props.t("purchases.purchaseDate")}
                        />
                      )}
                    </Form.Item>
                    <Form.Item
                      validateStatus={purchaseAmountError ? "error" : ""}
                      help={purchaseAmountError || ""}
                    >
                      <span className="purchase-order-form-label">
                        {this.props.t("purchases.purchaseAmount")}{" "}
                      </span>
                      {getFieldDecorator("purchaseAmount", {
                        rules: [
                          {
                            required: true,
                            message: this.props.t("purchases.errors.amount")
                          }
                        ]
                      })(
                        <Input
                          placeholder={this.props.t("purchases.purchaseAmount")}
                        />
                      )}
                    </Form.Item>
                    <Form.Item
                      validateStatus="success"
                      style={{ width: "100%" }}
                    >
                      <span className="purchase-order-form-label">
                        {this.props.t("purchases.paymentType")}
                      </span>
                      {getFieldDecorator("paymentType", {
                        initialValue: "Credit Card"
                      })(
                        <Select
                          placeholder={this.props.t("purchases.paymentType")}
                          style={{ width: "100%" }}
                          allowClear
                        >
                          <Option value="Credit Card">Credit Card</Option>
                          <Option value="PayPal">PayPal</Option>
                          <Option value="Cash">Cash</Option>
                          <Option value="Check">Check</Option>
                          <Option value="ACH">ACH</Option>
                          <Option value="Other">Other</Option>
                        </Select>
                      )}
                    </Form.Item>
                  </div>
                  <div
                    className="purchaseOrder-popup-card-body"
                    style={{ gridGap: "15px" }}
                  >
                    {/* other */}
                    <Form.Item validateStatus="success">
                      <span className="purchase-order-form-label">
                        {this.props.t("purchases.paymentGroup")}{" "}
                      </span>
                      {getFieldDecorator(
                        "paymentGroup",
                        {}
                      )(
                        <Input
                          style={{ width: "90%" }}
                          placeholder={this.props.t("purchases.paymentGroup")}
                        />
                      )}
                    </Form.Item>
                    <Form.Item validateStatus="success">
                      <span className="purchase-order-form-label">
                        {this.props.t("purchases.cardLast4")}{" "}
                      </span>
                      {getFieldDecorator(
                        "cardLast4",
                        {}
                      )(
                        <Input
                          placeholder={this.props.t("purchases.cardLast4")}
                        />
                      )}
                    </Form.Item>
                    {/*  */}
                    <Form.Item validateStatus="success">
                      <span className="purchase-order-form-label">
                        {this.props.t("purchases.listingId")}{" "}
                      </span>
                      {getFieldDecorator("listingId", {
                        initialValue: this.isDataPassedIn()
                          ? this.props.formData.listingId
                          : null
                      })(
                        <Input
                          placeholder={this.props.t("purchases.listingId")}
                        />
                      )}
                    </Form.Item>
                  </div>
                  <div
                    className="purchaseOrder-popup-card-body"
                    style={{ gridGap: "15px" }}
                  >
                    <Form.Item
                      validateStatus="success"
                      style={{ width: "91%" }}
                    >
                      <span className="purchase-order-form-label">
                        {this.props.t("purchases.salesTax")}{" "}
                      </span>
                      {getFieldDecorator("salesTax", {
                        initialValue: this.isDataPassedIn()
                          ? this.props.formData.salesTax
                          : null,
                        rules: [
                          {
                            type: "number",
                            message: "Sales Tax must be a number",
                            transform: value => {
                              return Number(value);
                            }
                          }
                        ]
                      })(
                        <Input
                          style={{ width: "100%" }}
                          type="number"
                          placeholder={this.props.t("purchases.salesTax")}
                        />
                      )}
                    </Form.Item>
                    <Form.Item validateStatus="success">
                      <span className="purchase-order-form-label">
                        {this.props.t("purchases.shipAndHandle")}{" "}
                      </span>
                      {getFieldDecorator("shipAndHandle", {
                        initialValue: this.isDataPassedIn()
                          ? this.props.formData.shipAndHandle
                          : null,
                        rules: [
                          {
                            type: "number",
                            message: "Ship and Handle must be a number",
                            transform: value => {
                              return Number(value);
                            }
                          }
                        ]
                      })(
                        <Input
                          type="number"
                          placeholder={this.props.t("purchases.shipAndHandle")}
                        />
                      )}
                    </Form.Item>
                    <Form.Item validateStatus="success">
                      <span className="purchase-order-form-label">
                        {this.props.t("purchases.otherFees")}{" "}
                      </span>
                      {getFieldDecorator("otherFees", {
                        initialValue: this.isDataPassedIn()
                          ? this.props.formData.otherFees
                          : null,
                        rules: [
                          {
                            type: "number",
                            message: "Other fees must be a number",
                            transform: value => {
                              return Number(value);
                            }
                          }
                        ]
                      })(
                        <Input
                          type="number"
                          placeholder={this.props.t("purchases.otherFees")}
                        />
                      )}
                    </Form.Item>
                  </div>
                </div>
                <div>
                  <h5
                    className="purchase-order-form-headings secondary-font-family"
                    style={{ display: "inline-block" }}
                  >
                    Line Item
                  </h5>
                </div>
                <Button style={{ margin: "5px 0 5px 0" }} onClick={this.add}>
                  Add Additional Line Items
                </Button>
                {formItems}
                <h5 className="purchase-order-form-headings secondary-font-family">Notes</h5>
                <Form.Item validateStatus="success">
                  <span className="purchase-order-form-label">
                    {this.props.t("purchases.notes")}{" "}
                  </span>
                  {getFieldDecorator(
                    "notes",
                    {}
                  )(<Input placeholder={this.props.t("purchases.notes")} />)}
                </Form.Item>
                <Form.Item validateStatus="success">
                  <span className="purchase-order-form-label">
                    {this.props.t("purchases.tags")}{" "}
                  </span>
                  {getFieldDecorator(
                    "tags",
                    {}
                  )(<Input placeholder={this.props.t("purchases.tags")} />)}
                </Form.Item>
                <Form.Item validateStatus="success">
                  <Button
                    style={{
                      marginTop: 16
                    }}
                    type="primary"
                    htmlType="submit"
                  >
                    {this.isDataPassedIn()
                      ? this.props.t("purchases.addWithListing")
                      : this.props.t("purchases.add")}
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>
        )}
      </UserContext.Consumer>
    );
  }
}

export default Form.create({ name: "createPurchaseOrder" })(
  withTranslation()(AddPurchaseOrder)
);
