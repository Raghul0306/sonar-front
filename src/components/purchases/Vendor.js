import React, { useState, useEffect, useContext } from "react";

import { useTranslation } from "react-i18next";

import UserContext from "../../contexts/user.jsx";

import Input from 'antd/es/input';
import Form from 'antd/es/form';
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Modal from 'antd/es/modal';
import AutoComplete from 'antd/es/auto-complete';

// style icon
import styled from 'styled-components'
import { Edit } from '@styled-icons/material/Edit'
import Select from 'react-select'
import { updatePurchaseOrderDetailsVendor } from "../../services/purchaseOrderService";
import { getStates, getCities, getCity, getState } from "../../services/userService";

const EditIcon = styled(Edit)`
  color: #aeb1be;
  width:15px;
  height:15px;
`
// const { Option } = Select;
const toolTipStyle = {
  fontSize: "18px",
  marginLeft: "5px",
  paddingBottom: "3px",
  color: "rgba(89,89,89, 0.4)"
};

const Vendor = (props) => {

  const { user } = useContext(UserContext);

  const { t } = useTranslation();

  const [isModalVisible, setIsModalVisible] = useState(false);

  const [countriesDropdown, setCountriesDropdown] = useState([]);
  const [countriesDropdownValue, setCountriesDropdownValue] = useState();
  const [statesDropdown, setStatesDropdown] = useState([]);
  const [citiesDropdown, setCitiesDropdown] = useState([]);


  const [vendorName, setVendorName] = useState('');
  const [vendorId, setVendorId] = useState('');
  const [vendorEmail, setVendorEmail] = useState('');
  const [vendorPhone, setVendorPhone] = useState('');
  const [vendorAddress, setVendorAddress] = useState('');
  const [vendorAddress1, setVendorAddress1] = useState('');
  const [vendorAddress2, setVendorAddress2] = useState('');
  const [vendorCity, setVendorCity] = useState('');
  const [vendorState, setVendorState] = useState('');
  const [vendorCountry, setVendorCountry] = useState('');
  const [vendorCountryId, setVendorCountryId] = useState('');
  const [vendorZip, setVendorZip] = useState('');

  /*Input start */
  const [vendorNameLabel, setVendorNameLabel] = useState('');
  const [vendorEmailLabel, setVendorEmailLabel] = useState('');
  const [vendorPhoneLabel, setVendorPhoneLabel] = useState('');
  const [vendorAddressLabel, setVendorAddressLabel] = useState('');
  const [vendorAddress1Label, setVendorAddress1Label] = useState('');
  const [vendorAddress2Label, setVendorAddress2Label] = useState('');
  const [vendorCityLabel, setVendorCityLabel] = useState('');
  const [vendorStateLabel, setVendorStateLabel] = useState('');
  const [vendorCountryLabel, setVendorCountryLabel] = useState('');
  const [vendorCountryIdLabel, setVendorCountryIdLabel] = useState('');
  const [vendorZipLabel, setVendorZipLabel] = useState('');
  const [countriesDropdownValueUp, setCountriesDropdownValueUp] = useState();
  const [formValidation, setFormValidation] = useState({
    isSubmitted: false,
    isValid: true,
    vendorName: false,
    vendorAddress: false,
    vendorEmail: false,
  });

  /*Input  end */

  const [lableInput, setIsField] = useState(false);
  const [vendorInfoJSON, setVendorInfoJSON] = useState({});
  const [updateVendorInfo, updateVendorInfoJSON] = useState({});

  const showModal = () => {
    // setIsModalVisible(true);
    setIsField(true);

    setVendorNameLabel(vendorName);
    setVendorEmailLabel(vendorEmail);
    setVendorPhoneLabel(vendorPhone);
    setVendorAddressLabel(vendorAddress);
    setVendorAddress1Label(vendorAddress1);
    setVendorAddress2Label(vendorAddress2);
    setVendorCityLabel(vendorCity);
    setVendorStateLabel(vendorState);
    setVendorCountryLabel(vendorCountry);
    setVendorCountryIdLabel(vendorCountryId);
    setVendorZipLabel(vendorZip);
    setCountriesDropdownValueUp(countriesDropdownValue);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  let vendorInfo = props.vendorInfo && props.vendorInfo.length ? props.vendorInfo[0] : '';
  let countries = props.countries && props.countries.length ? props.countries : '';

  useEffect(() => {

    if (vendorInfo) {
      let vendor_name = vendorInfo.vendor_name;
      let vendor_email = vendorInfo.vendor_email;
      let vendor_phone = vendorInfo.vendor_phone;
      // if (vendor_phone) { vendor_phone = '(' + vendor_phone.substr(0, 3) + ') ' + vendor_phone.substr(3, 3) + '-' + vendor_phone.substr(6); }
      // let vendor_address = vendorInfo.vendor_address;

      vendorAddressString();
      setVendorName(vendor_name);
      setVendorId(vendorInfo.vendor_id);
      setVendorEmail(vendor_email);
      setVendorPhone(vendor_phone);
      setVendorAddress1(vendorInfo.address);
      setVendorAddress2(vendorInfo.address2);
      setVendorCity(vendorInfo.city);
      setVendorState(vendorInfo.state);
      setVendorCountry(vendorInfo.country);
      setVendorCountryId(vendorInfo.countryd);
      setVendorZip(vendorInfo.zip);
      CityStateset(vendorInfo.country, vendorInfo.city, vendorInfo.state);

    }
  }, [vendorInfo]);

  async function vendorAddressString() {

    const vendor_city = await getCityRow(vendorInfo.city);
    const vendor_state = await getStateRow(vendorInfo.state);
    let countriesRowSelected = "";
    if (countries && countries.length) {
      for (var countryCount = 0; countryCount < countries.length; countryCount++) {
        if (vendorInfo.country == countries[countryCount].id) {
          countriesRowSelected = countries[countryCount].name;
        }
      }
    }
    let vendor_address = (vendorInfo.address ? vendorInfo.address + ', ' : '') + (vendorInfo.address2 ? vendorInfo.address2 + ', ' : '') + (countriesRowSelected ? countriesRowSelected + ', ' : '') + (vendor_city ? vendor_city + ', ' : '') + (vendor_state ? vendor_state + ', ' : '') + (vendorInfo.zip ? vendorInfo.zip : '');
    setVendorAddress(vendor_address);
  }
  async function CityStateset(country, city, state) {

    await loadStateList(country);
    await loadCityList(country, state);
    let cityDetails = await getCity(city);
    if (cityDetails) {
      setVendorCity({ label: cityDetails.name, value: cityDetails.id });
    }
    let stateDetails = await getState(state);
    if (stateDetails) {
      setVendorState({ label: stateDetails.name, value: stateDetails.id });
    }

  }

  async function loadCityList(country, state) {
    if (country && state) {
      country = '' + country;
      let cities = await getCities(country, state);
      if (cities) {
        const cityList = [];
         cities.length && cities.map(items => {
          cityList.push({ label: items.name, value: items.id })
        });
        setCitiesDropdown(cityList)
      }
    }
  }

  async function getCityRow(city_id) {
    let vendor_city = "";
    let cityRow = await getCity(city_id);
    if (cityRow) { return cityRow.name } else { return vendor_city; }
  }

  async function getStateRow(state_id) {
    let vendor_state = "";
    let stateRow = await getState(state_id);
    if (stateRow) { return  stateRow.name; } else { return vendor_state; }
  }

  async function loadStateList(country) {
    if (country) {
      let states = await getStates(country);
      if (states) {
        const stateList = [];
        states.length && states.map(items => {
          stateList.push({ label: items.name, value: items.id })
        });
        setStatesDropdown(stateList)
      }
    }
  }

  useEffect(() => {

    let countries_row_list = [];
    let countries_row_selected = {};
    if (countries && countries.length) {
      for (var countryCount = 0; countryCount < countries.length; countryCount++) {
        countries_row_list.push({ key: 'country_' + countryCount, value: countries[countryCount].id, label: countries[countryCount].name })
        if (vendorInfo.country == countries[countryCount].id) {
          countries_row_selected = { value: countries[countryCount].id, label: countries[countryCount].name }
        }
      }
    }
    setCountriesDropdown(countries_row_list);
    setCountriesDropdownValue(countries_row_selected);//countriesDropdownValue
  }, [countries]);
  const formValidationCustom = {
    isSubmitted: false,
    isValid: true,
    vendorName: false,
    vendorAddress: false,
    vendorEmail: false,
  }


  const saveInvoice = async () => {

    formValidationCustom.isSubmitted = true;
    formValidationCustom.isValid = true;
    if (!vendorNameLabel) {
      formValidationCustom.vendorName = true;
      formValidationCustom.isValid = false;
    }

    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);


    if (!vendorEmailLabel) {
      formValidationCustom.vendorEmail = true;
      formValidationCustom.isValid = false
      formValidationCustom.emailmessage = 'Email Field is required';
    }
    else if (!pattern.test(vendorEmailLabel)) {
      formValidationCustom.isValid = false;
      formValidationCustom.vendorEmail = true;
      formValidationCustom.emailmessage = 'Enter valid Email address';
    }



    if (!vendorPhoneLabel || isNaN(vendorPhoneLabel.replace(" ", "") )) {
      formValidationCustom.vendorPhone = true;
      formValidationCustom.isValid = false;
      formValidationCustom.phoneMessage = 'Enter valid phone number';
      if(isNaN(vendorPhoneLabel.replace(" ", "") )){
        formValidationCustom.phoneMessage = 'Numbers only allowed';
      }
    }
    else if (vendorPhoneLabel && (vendorPhoneLabel.length < 10 || vendorPhoneLabel.length > 15)) {
      formValidationCustom.vendorPhone = true;
      formValidationCustom.isValid = false;
      formValidationCustom.phoneMessage = '10 to 15 digits are required';
    }

    if (!countriesDropdownValueUp) {
      formValidationCustom.vendorCountry = true;
      formValidationCustom.isValid = false;
      formValidationCustom.zipcountrymsg = 'Country is required';
    }
    if (!vendorZipLabel) {
      formValidationCustom.vendorZip = true;
      formValidationCustom.isValid = false;
      formValidationCustom.zipcountrymsg = 'Zip code is required';
    }
    if (!vendorAddress1Label) {
      formValidationCustom.address1 = true;
      formValidationCustom.isValid = false;
      formValidationCustom.address1msg = 'Address is required';
    }
    if (!vendorCityLabel) {
      formValidationCustom.city = true;
      formValidationCustom.isValid = false;
      formValidationCustom.cityStateMsg = 'City is required';
    }
    if (!vendorStateLabel) {
      formValidationCustom.state = true;
      formValidationCustom.isValid = false;
      formValidationCustom.cityStateMsg = 'State is required';
    }

    await setFormValidation(formValidationCustom);
    if (true == formValidationCustom.isValid) {
      await formSubmit();
    }

  }
  const formSubmit = async () => {
    if (formValidation.isValid == true) {
      setIsField(false);
      setVendorName(vendorNameLabel);
      setVendorEmail(vendorEmailLabel);
      setVendorPhone(vendorPhoneLabel);
      setVendorAddress(vendorAddressLabel);
      setVendorAddress1(vendorAddress1Label);
      setVendorAddress2(vendorAddress2Label);
      setVendorCity(vendorCityLabel);
      setVendorState(vendorStateLabel);
      setCountriesDropdownValue(countriesDropdownValueUp);
      setVendorCountryId(vendorCountryId);
      setVendorCountry(vendorCountry);
      setVendorZip(vendorZipLabel);

       
      let countriesIdValue = ""; let vendorCityValue = ""; let vendorStateValue = "";
      if (countriesDropdownValueUp) {         
        countriesIdValue = countriesDropdownValueUp.value;
      }
      if (vendorCity) { vendorCityValue = vendorCity.value; }
      if (vendorState) { vendorStateValue = vendorState.value; }

      let formData = {
        id: vendorId,
        name: vendorNameLabel,
        address: vendorAddress1Label,
        address2: vendorAddress2Label,
        zip: vendorZipLabel,
        city: vendorCityValue,
        state: vendorStateValue,
        country: countriesIdValue,
        country_id: countriesIdValue,
        phone: vendorPhoneLabel,
        email: vendorEmailLabel
      }
      await updatePurchaseOrderDetailsVendor(formData);
      props.onChildUpdate();
    }
  }

  const closeInvoice = async () => {
    setIsField(false);
    formValidationCustom.isSubmitted = false;
    setFormValidation(formValidationCustom);
  }

  const changeVendorName = async (e) => {

    setVendorNameLabel(e.target.value);
    if (e.target.value && e.target.value != '') {
      formValidationCustom.vendorName = false;
      await setFormValidation(formValidationCustom);
    }
    else {
      formValidationCustom.vendorName = true;
      await setFormValidation(formValidationCustom);
    }

  }
  const changeVendorAddress1 = async (e) => {
    setVendorAddress1Label(e.target.value);
    if (e.target.value && e.target.value != '') {
      formValidationCustom.address1 = false;
      await setFormValidation(formValidationCustom);
    }
    else {
      formValidationCustom.address1 = true;
      await setFormValidation(formValidationCustom);
    }
  }
  const changeVendorAddress2 = async (e) => {
    setVendorAddress2Label(e.target.value);
  }
  const changeVendorCity = async (e) => {
    if (e) {
      await setVendorCity(e);
      formValidationCustom.city = false;
      await setFormValidation(formValidationCustom);
      setVendorCityLabel(e.value);
    } else {
      formValidationCustom.state = true;
      await setFormValidation(formValidationCustom);
      await setVendorCity("");
      formValidationCustom.city = true;
      await setFormValidation(formValidationCustom);
    }
  }
  const changeVendorState = async (e) => {
    if (e) {
      await setVendorState(e);
      await loadCityList(countriesDropdownValueUp.value, e.value)
    } else {
      formValidationCustom.state = true;
      await setFormValidation(formValidationCustom);
      await setVendorState("");
      await loadCityList("");
    }
  }
  const changeVendorEmail = async (e) => {
    setVendorEmailLabel(e.target.value);
    if (e.target.value && e.target.value != '') {
      formValidationCustom.vendorEmail = false;
      await setFormValidation(formValidationCustom);
    }
    else {
      formValidationCustom.vendorEmail = true;
      await setFormValidation(formValidationCustom);
    }
  }
  const changeVendorPhone = async (e) => {
    setVendorPhoneLabel((e.target.value).replace(" ", ""));
    if (e.target.value && e.target.value != '' && !isNaN(e.target.value)) {
      formValidationCustom.vendorPhone = false;
      await setFormValidation(formValidationCustom);
    }
    else {
      formValidationCustom.vendorPhone = true;
      await setFormValidation(formValidationCustom);
    }
  }
  const changeCountry = async (e) => {
    if (e) {
      await setCountriesDropdownValueUp(e);
      await loadStateList(e.value);
      await setVendorCity("");
      await setVendorState("");
      await setVendorZipLabel("");
    } else {
      await setCountriesDropdownValueUp(null);
      await setStatesDropdown("");
      await setCitiesDropdown("");
      await setVendorCity("");
      await setVendorState("");
      await setVendorZipLabel("");
    }
  }
  const changeZip = async (e) => {
    setVendorZipLabel(e.target.value);
    if (e.target.value && e.target.value != '') {
      formValidationCustom.vendorZip = false;
      await setFormValidation(formValidationCustom);
    }
    else {
      formValidationCustom.vendorZip = true;
      await setFormValidation(formValidationCustom);
    }
  }

  return (
    <>
      <Card
        className={lableInput == true ? "order-detail-channel-card purchase-edit-card" : "order-detail-channel-card"}
      >
        <div className="purchase-pay-info">
          <div>
            <h5
              className="purchase-order-form-headings p-0 secondary-font-family primary-color"
              style={{ display: "inline-block" }}
            >
              Vendor Info
            </h5>
          </div>
          <div className="attached-invoice-item">
            {!lableInput ?
              <Button className="btn-edit btn" onClick={showModal}><EditIcon /></Button> :
              <span>
                <Button className="btn-save-tn btn" onClick={saveInvoice}>Save</Button>
                <Button className="btn-edit-tn btn" onClick={closeInvoice}>Cancel</Button>
              </span>
            }
          </div>
        </div>
        <div className="order-grid-section p-0 dynamic_edit-detail">
          {!lableInput ?
            <div className="delivery-status-grid vendor-grid-align" key={'vendor_label'}>
              <div className="left-delivery-grid stylefont-weight-medium"><h6>Vendor Name:</h6></div>
              <div className="right-delivery-grid stylefont-weight-medium"><h6>{vendorName}</h6></div>
              <div className="left-delivery-grid stylefont-weight-medium"><h6>Address:</h6> </div>
              <div className="right-delivery-grid stylefont-weight-medium"><h6>{vendorAddress}</h6></div>
              <div className="left-delivery-grid stylefont-weight-medium"><h6>Email:</h6> </div>
              <div className="right-delivery-grid stylefont-weight-medium"><h6>{vendorEmail}</h6></div>
              <div className="left-delivery-grid stylefont-weight-medium"><h6>Phone Number:</h6> </div>
              <div className="right-delivery-grid stylefont-weight-medium"><h6>{vendorPhone}</h6></div>

            </div> :

            <div className="delivery-status-grid vendor-grid-align" key={'vendor_input'}>
              <div className="left-delivery-grid stylefont-weight-medium"><h6>Vendor Name:</h6></div>
              <div className="right-delivery-grid stylefont-weight-medium">
                <div className={formValidation.vendorName ? "edit-input-box is-in-valid" : "edit-input-box"}>

                  <input type="text" className="form-control" id="vendorName" name="vendorName" value={vendorNameLabel} onChange={(e) => changeVendorName(e)} placeholder="Vendor Name" />
                </div>
                {formValidation && formValidation.isSubmitted && formValidation.vendorName &&
                  <span className="validate-error">Vendor Name is required </span>
                }
              </div>
              <div className="left-delivery-grid stylefont-weight-medium"><h6>Address:</h6> </div>

              <div className="right-delivery-grid stylefont-weight-medium address-edit-item">
                <div className={formValidation.address1 ? "edit-input-box is-in-valid" : "edit-input-box"}>
                  <input type="text" className="form-control" id="vendorAddress1" name="vendorAddress1" value={vendorAddress1Label} onChange={(e) => changeVendorAddress1(e)} placeholder="Address" />
                </div>
                {formValidation && formValidation.isSubmitted && formValidation.address1 &&
                  <span className="validate-error">{formValidation.address1msg} </span>
                }
              </div>
              <div className="left-delivery-grid stylefont-weight-medium"></div>

              <div className="right-delivery-grid stylefont-weight-medium address-edit-item">
                <div className="edit-input-box">
                  <input type="text" className="form-control" id="vendorAddress2" name="vendorAddress2" value={vendorAddress2Label} onChange={(e) => changeVendorAddress2(e)} placeholder="Address 2" />
                </div>
              </div>
              <div className="left-delivery-grid stylefont-weight-medium"></div>
              <div className="right-delivery-grid stylefont-weight-medium address-edit-item">
                <div className="field-grid-sec">
                  <Select options={countriesDropdown} id="vendorCountry" name="vendorCountry" value={countriesDropdownValueUp} onChange={(e) => changeCountry(e)} placeholder="Country" allowClear showSearch />
                  <div className={formValidation.vendorZip ? "edit-input-box is-in-valid" : "edit-input-box"}>
                    <input type="text" id="vendorZip" name="vendorZip" className="form-control" value={vendorZipLabel} onChange={(e) => changeZip(e)} placeholder="Zip code" />
                  </div>
                </div>
                {formValidation && formValidation.isSubmitted && (formValidation.vendorZip || formValidation.vendorCountry) &&
                  <span className="validate-error">{formValidation.zipcountrymsg} </span>
                }
              </div>
              <div className="left-delivery-grid stylefont-weight-medium"></div>
              <div className="right-delivery-grid stylefont-weight-medium address-edit-item">
                <div className="field-grid-sec">
                  <div className={formValidation.state ? "is-in-valid" : ""}>
                    <Select options={statesDropdown} id="vendorState" name="vendorState" value={vendorState} onChange={(e) => changeVendorState(e)} placeholder="State" allowClear showSearch />
                  </div>

                  <div className={formValidation.city ? "edit-city-select-box is-in-valid" : "edit-city-select-box"}>
                    <Select options={citiesDropdown} id="vendorCity" name="vendorCity" value={vendorCity} onChange={(e) => changeVendorCity(e)} placeholder="City" style={{width: "80px"}} allowClear showSearch />
                  </div>
                  {formValidation && formValidation.isSubmitted && (formValidation.city || formValidation.state) &&
                    <span className="validate-error">{formValidation.cityStateMsg} </span>
                  }
                </div>
              </div>


              <div className="left-delivery-grid stylefont-weight-medium"><h6>Email:</h6> </div>
              <div className="right-delivery-grid stylefont-weight-medium">
                <div className={formValidation.vendorEmail ? "edit-input-box is-in-valid" : "edit-input-box"}>
                  <input type="text" className="form-control" id="vendorEmail" name="vendorEmail" value={vendorEmailLabel} onChange={(e) => changeVendorEmail(e)} placeholder="Email" />
                </div>
                {formValidation && formValidation.isSubmitted && formValidation.vendorEmail &&
                  <span className="validate-error">{formValidation.emailmessage} </span>
                }
              </div>
              <div className="left-delivery-grid stylefont-weight-medium"><h6>Phone Number:</h6> </div>
              <div className="right-delivery-grid stylefont-weight-medium">
                <div className={formValidation.vendorPhone ? "edit-input-box is-in-valid" : "edit-input-box"}>
                  <input type="text" className="form-control" id="vendorPhone" name="vendorPhone" value={vendorPhoneLabel} onChange={(e) => changeVendorPhone(e)} placeholder="Phone" />
                </div>
                {formValidation && formValidation.isSubmitted && formValidation.vendorPhone &&
                  <span className="validate-error">{formValidation.phoneMessage} </span>
                }
              </div>


            </div>
          }


        </div>
      </Card>
      {/* modal popup */}
      <Modal title=""
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
        className="payment-info-modal"
        maskClosable={false}

      >
        <Form>
          <div className="purchase-pay-info">
            <div>
              <h5
                className="purchase-order-form-headings secondary-font-family primary-color"
                style={{ display: "inline-block", marginBottom: "10px" }}
              >
                Edit Vendor Information
              </h5>
            </div>
          </div>
          <div
            className="purchase-order-card-container purchase-order-grid-section"
          >
            <div className="custom-form-item">
              {/* input Name */}
              <Form.Item>
                <span className="purchase-order-form-label">
                  {t("vendor.name")}{" "}
                </span>
                <AutoComplete
                  style={{ width: "100%" }}
                  onSearch={text => this.onSearchInputChange(text)}
                  onSelect={text => this.onSelect(text)}
                  placeholder={t("vendor.name")}
                />
              </Form.Item>
              {/* Address */}
              <Form.Item validateStatus="success">
                <span className="purchase-order-form-label">
                  {t("vendor.address")}{" "}
                </span>
                <Input placeholder={t("vendor.address")} />
              </Form.Item>
              {/* address#2 */}
              <Form.Item
                validateStatus="success">
                <span className="purchase-order-form-label">
                  {t("vendor.address")}{" "}
                </span>
                <Input placeholder={t("vendor.address2")} />
              </Form.Item>
            </div>
            <div className="custom-form-item p-0">
              <div className="purchaser-order-gris-2">
                <Form.Item
                  validateStatus="success"
                  style={{ width: "100%" }}
                >
                  <span className="purchase-order-form-label">
                    {t("vendor.city")}{" "}
                  </span>
                  <Input placeholder={t("vendor.city")} />
                </Form.Item>
                <Form.Item
                  validateStatus="success"
                  style={{ width: "100%" }}
                >
                  <span className="purchase-order-form-label">
                    {t("vendor.state")}{" "}
                  </span>
                  <Input placeholder={t("vendor.state")} />
                </Form.Item>
              </div>
              <div className="purchaser-order-gris-2">
                <Form.Item
                  validateStatus="success"
                  style={{ width: "100%" }}
                >
                  <span className="purchase-order-form-label">
                    {t("vendor.zip")}{" "}
                  </span>
                  <Input placeholder={t("vendor.zip")} />
                </Form.Item>
                <Form.Item
                  validateStatus="success"
                  style={{ width: "100%" }}
                >
                  <span className="purchase-order-form-label">
                    {t("vendor.country")}{" "}
                  </span>
                  <Input placeholder={t("vendor.country")} />
                </Form.Item>
              </div>
              <div className="purchaser-order-gris-2">
                <Form.Item
                  validateStatus="success"
                  style={{ width: "100%" }}
                >
                  <span className="purchase-order-form-label">
                    {t("vendor.phone")}{" "}
                  </span>
                  <Input placeholder={t("vendor.phone")} />
                </Form.Item>
                <Form.Item
                  validateStatus="success"
                  style={{ width: "100%" }}
                >
                  <span className="purchase-order-form-label">
                    {t("vendor.email")}{" "}
                  </span>
                  <Input placeholder={t("vendor.email")} />
                </Form.Item>
              </div>
            </div>
          </div>
          <div className="form-footer-item">
            <Button className="cancel-btn-payinfo">Cancel</Button>
            <Button className="submit-btn-payinfo">Submit</Button>
          </div>
        </Form>
      </Modal>
    </>
  );
};

export default Vendor;
