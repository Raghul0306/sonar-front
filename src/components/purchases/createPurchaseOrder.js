// eslint-disable-next-line
import React from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';
import Loader from "../Loader/Loader";
import Input from 'antd/es/input';
import Button from 'antd/es/button';
import AutoComplete from 'antd/es/auto-complete';
import Card from 'antd/es/card';
import UserContext from "../../contexts/user.jsx";
import { withTranslation } from "react-i18next";
import {
  createPurchaseOrder,
  vendorSearchFilter,
  getVendorFilter,
  getListingFilter,
  listingSearchFilter,
  getAllPaymentTypes,
  getAllPaymentGroups,
  invoiceAttachment,
  getPODetails
} from "../../services/purchaseOrderService";
import {
  getCountries,
  getStates, 
  getCities
} from "../../services/userService";
import { getAllTagList } from "../../services/listingsService";
import * as moment from "moment-timezone";
import DatePicker from "react-datepicker";
import Select from 'react-select'
import "react-datepicker/dist/react-datepicker.css";
// style icon
import styled from 'styled-components'
import { Calendar } from '@styled-icons/boxicons-regular/Calendar'
import { Close } from '@styled-icons/ionicons-outline/Close'
import { Attachment } from '@styled-icons/entypo/Attachment'
import { Confirm, Info, Alert } from "../../components/modal/common-modal";
import { Redirect, withRouter } from 'react-router-dom';
import { seatTypes, purchaseOrderVendorStateVariables } from "../../constants/constants";


import iconInhand from "../../assets/myTicketsIconCalendar.png";

const CalendarIcon = styled(Calendar)`
  color: #9f9f9f;
  width:30px;
  height:30px;
`
const CloseIcon = styled(Close)`
  color: #FF5D5D;
  width:30px;
  height:30px;
`
const AttachmentIcon = styled(Attachment)`
  color: #2C91EE;
  width:25px;
  height:25px;
  margin:0 4px 0 0;
`
const { Option } = Select;

const toolTipStyle = {
  fontSize: "18px",
  marginLeft: "5px",
  paddingBottom: "3px",
  color: "rgba(89,89,89, 0.4)"
};

// for dyanmic forms
let id = 0;

class AddPurchaseOrder extends React.Component {
  static contextType = UserContext;
  constructor(props) {
    super(props);
    this.dateRangeDisplay = this.dateRangeDisplay.bind(this);
    this.onSearchVendorName = this.onSearchVendorName.bind(this);
    this.onSearchListing = this.onSearchListing.bind(this);

    this.selectListing = this.selectListing.bind(this);
    this.selectListingId = this.selectListingId.bind(this);

    this.onVendorStatus = this.onVendorStatus.bind(this)
    this.handlesubmitButtonModal = this.handlesubmitButtonModal.bind(this);
    this.handleInfoSubmitButtonModal = this.handleInfoSubmitButtonModal.bind(this);
    this.handleAlertSubmitButtonModal = this.handleAlertSubmitButtonModal.bind(this);
    this.addform = this.addform.bind(this);
    this.removeform = this.removeform.bind(this);
    this.toggleAttachmentForm = this.toggleAttachmentForm.bind(this);
    this.changeAttachmentForm = this.changeAttachmentForm.bind(this);
    this.lastFourDigits = this.lastFourDigits.bind(this);
    this.dropdownFilterOption = this.dropdownFilterOption.bind(this);
    //this.vendorSearchFilter = this.vendorSearchFilter.bind(this);

    this.onLoadStateDropDown = this.onLoadStateDropDown.bind(this);
    this.onLoadCityDropDown = this.onLoadCityDropDown.bind(this);

    this.state = {
      suggestions: [],
      listingSuggestions: [],
      listingSuggestionsIndex: "",
      keys: [],
      listingElements: [],
      countrylist: [],
      stateList: [],
      cityList: [],
      setLoader: false,
      purchaseDate: "",
      paymentMethod: "",
      salesTax: "",
      purchaseAmount: "",
      cardLastFourDigits: "",
      shipAndHandle: "",
      paymentType: "",
      referenceId: "",
      otherFees: "",
      notes: "",
      currencyId: 1,
      tags: "",
      tag_lists: "",
      vendordetail: {
        vendorID: "",
        vendorName: "",
        vendorEmail: "",
        vendorPhone: "",
        vendorAddress1: "",
        vendorAddress2: "",
        vendorCity: "",
        vendorState: "",
        vendorZipcode: "",
        vendorCountry: "",
      },
      vendorsearchData: {
        searchtext: ""
      },
      listingDetails: [{
        listingId: "",
        eventName: "",
        eventDate: "",
        section: "",
        row: "",
        seat_start: "",
        seat_end: "",
        cost: "",
        quantity: "",
        unit_cost: "",
        taxes: "",
        value: 1,
        seat_type_id: ""
      }],
      InfoSubmitButtonModal: false,
      infocloseButtonModal: false,
      alertcloseButtonModal: false,
      closeButtonModal: false,
      submitButtonModal: false,
      datafields: "",
      infomodalmsg: "",
      alertmodalmsg: "",
      IsShowAttachInvoiceForm: false,
      InvoiceAttachName: "Select Attachment File",
      InvoiceAttachFile: "",
      itemsLength: 0
    };
  }
  async componentDidMount() {
    const tags = [];
    for (let tagCount = 10; tagCount < 36; tagCount++) {
      tags.push(<Option value={tagCount.toString(36) + tagCount} selected={false} >{tagCount.toString(36) + tagCount}</Option>);
    }
    this.setState({ tag_lists: tags });

    let countries = await getCountries();
    if (countries) {
      await this.setState({ countrylist: countries });
    }

    const elements = [1];
    await this.setState({ listingElements: elements })

    let getTags = await getAllTagList(this.context.user.id);
    let getPaymentTypes = await getAllPaymentTypes();
    let getPaymentGroups = await getAllPaymentGroups();

    await this.setState({
      getTags: getTags,
      getPaymentTypes: getPaymentTypes,
      getPaymentGroups: getPaymentGroups,
    });

    if (this.props.match.params && this.props.match.params.id) {
      var listingDataId = this.props.match.params.id;
      let listingDataIdArr = listingDataId.split(',');
      const listingData = listingDataIdArr.map((i) => Number(i));

      if (listingData && listingData.length) {
        var listingdata = await getPODetails(listingData);
        if (listingdata && listingdata.value && listingdata.value.length) {

          listingdata.value.map((listing, i) => {
            var eventDate = listing.eventDate.split('T');
            if (eventDate[0]) { listing.eventDate = new Date(eventDate[0]); } else { listing.eventDate = ""; }
            listing.listingId = listing.id;
            listing.value = ++i;
            listing.cost = listing.group_cost;
          })

          this.setState({ listingDetails: listingdata.value })
        }
      }
    }

  }

  async dateRangeDisplay(e) {//Date picker   

    var startingDate = '';
    if (e) {
      var starting = new Date(e);
      let month = starting.getMonth() + 1;
      let date = starting.getDate();
      if (month < 10) {
        month = '0' + month;
      }
      if (date < 10) {
        date = '0' + date;
      }
      startingDate = starting.getFullYear() + '-' + month + '-' + date;
    }
    this.setState({ purchaseDate: e });

  }

  renderListingSuggestions = (index) => {
    const { listingSuggestions } = this.state;

    if (listingSuggestions.length === 0) {
      return null;
    }

    return (
      <ul>
        {listingSuggestions.map(events => <li onClick={() => this.selectListingId(events, index)} >{events}</li>)}
      </ul>
    )
  }

  renderSuggestions = () => {
    const { suggestions } = this.state;

    if (suggestions.length === 0) {
      return null;
    }

    return (
      <ul>
        {suggestions.map(events => <li onClick={() => this.selectListing(events)} >{events}</li>)}
      </ul>
    )
  }

  dropdownFilterOption = ({ label, value, data }, string) => {
    if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };

  async selectListing(keyword = false) {

    let vendorsearchdata = this.state.vendorsearchData;
    vendorsearchdata.searchtext = keyword;
    await this.setState({ suggestions: null, vendorsearchData: vendorsearchdata });
    this.setMyPageLoader(true);
    let searchedData = await getVendorFilter(this.state.vendorsearchData);

    if (searchedData) {
      var vendor_country_id = "";

      if (searchedData.country) {
        var vendor_country = this.state.countrylist.find(c => (c.id === searchedData.country));
        if (vendor_country) {
          vendor_country_id = {label:vendor_country.name,value:vendor_country.id};
        } else {
          vendor_country_id = "";
        }
      }
      let vendordetail = {};
      vendordetail.vendorID = searchedData.id;
      vendordetail.vendorName = searchedData.name;
      vendordetail.vendorEmail = searchedData.email;
      vendordetail.vendorPhone = searchedData.phone;
      vendordetail.vendorAddress1 = searchedData.address;
      vendordetail.vendorAddress2 = searchedData.address2;
      vendordetail.vendorCity = searchedData.city;
      vendordetail.vendorState = searchedData.state;
      vendordetail.vendorZipcode = searchedData.zip;
      vendordetail.vendorCountry = vendor_country_id;
      this.setState({ ...this.state.vendordetail, vendordetail });
    }

    this.setMyPageLoader(false);
  }

  async onSearchListing(e, statename) {
    var searchtext = e.target.value;
    let listingSuggestions = [];
    if (this.state.listingSuggestionsIndex !== statename) {
      await this.setState({ listingSuggestions: listingSuggestions });
    }
    if (searchtext.length) {
      listingSuggestions = await listingSearchFilter(searchtext,this.context.user.id);
      await this.setState({ listingSuggestions: listingSuggestions });
    } else {

    }
    let tempListingDetails = this.state.listingDetails;
    tempListingDetails[statename].listingId = searchtext;
    await this.setState({ listingSuggestions: listingSuggestions, listingSuggestionsIndex: statename, listingDetails: tempListingDetails });
  }

  async onSearchVendorName(searchtext, type) {
    let suggestions = [];

    if (type == purchaseOrderVendorStateVariables.type) {

      if (searchtext.length > 2) {
        suggestions = await vendorSearchFilter(searchtext, type);
      }
      else {
        suggestions = [];
        if (searchtext.length == 0) {
          this.setState(prevState => {
            let vendordetail = Object.assign({}, prevState.vendordetail);
            vendordetail.vendorID = "";
            vendordetail.vendorName = "";
            vendordetail.vendorEmail = "";
            vendordetail.vendorPhone = "";
            vendordetail.vendorAddress1 = "";
            vendordetail.vendorAddress2 = "";
            vendordetail.vendorCity = "";
            vendordetail.vendorState = "";
            vendordetail.vendorZipcode = "";
            vendordetail.vendorCountry = "";
            //return { vendordetail };
          })
        }
      }
      this.setState(() => ({
        suggestions,
      }));

    }
  }

  async onVendorStatus(e, statename) {
    let vendordetail = this.state.vendordetail;

    if (statename === purchaseOrderVendorStateVariables.vendorAddress) {
      vendordetail.vendorAddress1 = e.target.value;
    }
    else if (statename === purchaseOrderVendorStateVariables.vendorAddress2) {
      vendordetail.vendorAddress2 = e.target.value;
    }
    else if (statename === purchaseOrderVendorStateVariables.vendorName) {
      vendordetail.vendorName = e;
    }
    else if (statename === purchaseOrderVendorStateVariables.vendorEmail) {
      vendordetail.vendorEmail = e.target.value;
    }
    else if (statename === purchaseOrderVendorStateVariables.vendorPhone) {
      vendordetail.vendorPhone = e.target.value;
    }
    else if (statename === purchaseOrderVendorStateVariables.vendorCity) {
      vendordetail.vendorCity = e;
    }
    else if (statename === purchaseOrderVendorStateVariables.vendorState) {
      vendordetail.vendorState = e;
      vendordetail.vendorCity = '';
      await this.onLoadCityDropDown(e);
    }
    else if (statename === purchaseOrderVendorStateVariables.vendorZipCode) {
      vendordetail.vendorZipcode = e.target.value;
    }
    else if (statename === purchaseOrderVendorStateVariables.vendorCountry) {
      vendordetail.vendorCountry = e;
      vendordetail.vendorState = '';
      vendordetail.vendorCity = '';
      await this.onLoadStateDropDown(e);
    }

    this.setState({ ...this.state.vendordetail, vendordetail });

  }


  async onLoadStateDropDown(country_id) {
    let countryId = null;
    if(country_id.value){ countryId = country_id.value }
    let states = await getStates(countryId);
      if (states) {
        const stateList = [];
        states && states.length && states.map(items => {
          stateList.push({ label: items.name, value: items.id })
        });
        await this.setState({
          stateList: stateList
        });
      }
  }

  async onLoadCityDropDown(state_id) {

    let countryId = this.state.vendordetail.vendorCountry ? this.state.vendordetail.vendorCountry.value : '';
    let stateId = state_id ? state_id.value : '';
    let cities = await getCities(countryId,stateId);
    if (cities) {
      const cityList = [];
      cities && cities.length && cities.map(items => {
        cityList.push({ label: items.name, value: items.id })
      });
      await this.setState({
        cityList: cityList
      });
    }
  }


  /**
  * Loader Function
  */
  async setMyPageLoader(bool) {
    if (bool) {
      this.setState({ setLoader: true });
      document.body.classList.add('loader-background');
    }
    else {
      this.setState({ setLoader: false });
      document.body.classList.remove('loader-background');
    }
  }

  handleInfoSubmitButtonModal = async (modalpopupstatus) => {
    await this.setState({
      InfoSubmitButtonModal: true,
      infocloseButtonModal: false,
    });
    this.setMyPageLoader(false);
    this.props.history.push("/purchases");
  }

  handleAlertSubmitButtonModal = async (modalpopupstatus) => {
    await this.setState({
      alertcloseButtonModal: false,
    });
    this.setMyPageLoader(false);
  }

  handlesubmitButtonModal = async (modalpopupstatus) => {

    var data = this.state.vendordetail;
    var vendorLocationCountry = {
      vendorCountry: this.state.vendordetail.vendorCountry ? this.state.vendordetail.vendorCountry.value : '',
      vendorState: this.state.vendordetail.vendorState ? this.state.vendordetail.vendorState.value : '',
      vendorCity: this.state.vendordetail.vendorCity ? this.state.vendordetail.vendorCity.value : '',
    }
    data = await { ...data, ...vendorLocationCountry };
    var vendorInformationsJSON = { vendorInformationsJSON : data }
    
    const listdata = await { ...this.state.datafields, ...vendorInformationsJSON };
    listdata.purchaseDate = listdata.purchaseDate ? moment.utc(listdata.purchaseDate).local().format("YYYY-MM-DD") : '';

    if (modalpopupstatus) {
      //listdata = this.state.datafields;
      this.setMyPageLoader(true);
      var createPurchaseOrderresponse = await createPurchaseOrder(listdata);
      if (!createPurchaseOrderresponse.isError) {

        if (this.state.InvoiceAttachFile) {
          if (createPurchaseOrderresponse.data && createPurchaseOrderresponse.data.id) {
            var invoiceUploadAttachmentData = {
              purchaseOrderId: createPurchaseOrderresponse.data.id,
              invoiceAttachName: this.state.InvoiceAttachName,
              invoiceAttachFile: this.state.InvoiceAttachFile,
            }
            await invoiceAttachment(invoiceUploadAttachmentData).then((res) => {
            });
          }
        }
        await this.setState({ infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: createPurchaseOrderresponse.msg });
        return;

      } else {
        this.setMyPageLoader(false);
        await this.setState({ infocloseButtonModal: true, closeButtonModal: false, infomodalmsg: createPurchaseOrderresponse.msg });
        return;
      }
    } else {
      await this.setState({ closeButtonModal: false });
      this.setMyPageLoader(false);

    }

  }



  async addform() {
    var count = this.state.listingDetails.length + 1;
    var items = {
      listingId: "",
      eventName: "",
      eventDate: "",
      section: "",
      row: "",
      seat_start: "",
      seat_end: "",
      cost: "",
      quantity: "",
      unit_cost: "",
      taxes: "",
      value: count,

    }
    this.setState({ listingDetails: [...this.state.listingDetails, items], itemsLength: this.state.itemsLength + 1 }) //another array
  }

  async removeform(index) {
    var elements = this.state.listingElements;
    elements.splice(index, 1);

    var listingdetails = this.state.listingDetails;
    listingdetails.splice(index, 1);

    this.setState({ listingElements: elements, listingDetails: listingdetails, itemsLength: this.state.itemsLength - 1 })
  }

  async toggleAttachmentForm() {
    this.setState(prevState => ({
      IsShowAttachInvoiceForm: !prevState.IsShowAttachInvoiceForm
    }));
  }

  async changeAttachmentForm(e) {

    this.setState({
      InvoiceAttachName: e.target.files[0].name,
      InvoiceAttachFile: e.target.files[0]
    });
  }



  /* Appended form function end */

  async selectListingId(value, index) {
    let tempListingDetails = this.state.listingDetails;
    tempListingDetails[index].listingId = value;
    await this.setState({
      listingDetails: tempListingDetails,
      listingSuggestions: [],
      listingSuggestionsIndex: ""
    });

    let searchedData = await getListingFilter(value);
    if (searchedData) {
      var eventDate = searchedData.eventDate.split('T');
      if (eventDate[0]) { eventDate = new Date(eventDate[0]); } else { eventDate = ""; }
      let tempListingDetails = this.state.listingDetails;
      tempListingDetails[index].listingId = value;
      tempListingDetails[index].eventName = searchedData.eventName;
      tempListingDetails[index].eventDate = eventDate;
      tempListingDetails[index].section = searchedData.section;
      tempListingDetails[index].row = searchedData.row;
      tempListingDetails[index].seat_start = searchedData.seat_start;
      tempListingDetails[index].seat_end = searchedData.seat_end;
      tempListingDetails[index].cost = searchedData.group_cost;
      tempListingDetails[index].unit_cost = searchedData.unit_cost;
      tempListingDetails[index].quantity = searchedData.quantity;
      await this.setState({
        listingDetails: tempListingDetails
      });

    }
  }
  async lastFourDigits(e) {
    if (e && !isNaN(e.target.value) && e.target.value.length < 5) {
      this.setState({ cardLastFourDigits: e.target.value })
    }
    else { return false; }
  }

  /* Appended form function end */

  render() {
    const { suggestions, listingElements, listingSuggestions, listingSuggestionsIndex, listingDetails } = this.state
    const formitems = [];

    const paymentMethodOptions = [];
    this.state.getPaymentGroups && this.state.getPaymentGroups.length > 0
      && this.state.getPaymentGroups.map((item, i) => {
        paymentMethodOptions.push({ value: item.id, label: item.name });
      })

    const paymentTypesOptions = [];
    this.state.getPaymentTypes && this.state.getPaymentTypes.length > 0
      && this.state.getPaymentTypes.map((item, i) => {
        paymentTypesOptions.push({ value: item.id, label: item.name });
      })

    const tagOptions = [];
    this.state.getTags && this.state.getTags.length > 0
      && this.state.getTags.map((item, i) => {
        tagOptions.push({ value: item.id, label: item.tag_name });
      })

    const vendorCountryOptions = [];
    this.state.countrylist && this.state.countrylist.length > 0
      && this.state.countrylist.map((item, i) => {
        return vendorCountryOptions.push({ value: item.id, label: item.name });
      })
    for (var listingCount = 0; listingCount < listingDetails.length; listingCount++) {
      let index = listingCount; let value = listingDetails[listingCount].value;
      formitems.push(

        <Card
          className="createPurchase-order-card line-item-dynamic"
        >
          <div>
            <h5
              className="purchase-order-form-headings secondary-font-family primary-color"
              style={{ display: "inline-block" }}
            >
              Line Item #{value}
            </h5>
          
          </div>
          <div className="line-item-grid-3-section">
            <div className="custom-form-item">
              <label htmlFor="listingId" className="form-label-userprofile">
                Listing Group ID  <span>*</span>
              </label>

              <div className="form-group">
                <Input
                  placeholder="Listing Group ID"
                  className="form-control"
                  name="listingId"
                  value={listingDetails[listingCount].listingId}
                  onChange={(e) => this.onSearchListing(e, index)}
                />
              </div>
              {listingSuggestions && listingSuggestionsIndex == index && listingSuggestions.length > 0 &&
                < div className="TypeAheadDropDown">
                  {
                    this.renderListingSuggestions(index)
                  }
                </div>
              }

            </div>
            <div className="custom-form-item">
              <label htmlFor="eventName" className="form-label-userprofile">
                Event Name
              </label>

              <div className="form-group">
                <Input
                  placeholder="Event Name"
                  className="form-control"
                  name="eventName"
                  value={listingDetails[listingCount].eventName}
                />
              </div>
            </div>
            <div className="custom-form-item">
              <label htmlFor="eventDate" className="form-label-userprofile">
                Event Date
              </label>

              <div className="form-group">
                <div className="date--PickQr">
                  <span className="custom-calendar-img">
                    <img src={iconInhand} alt="icon"/>
                  </span>
                  <DatePicker
                    name="eventDate"
                    className={
                      "formik-input-user-form form-control singleDate"
                      // (errors.eventDate && touched.eventDate
                      //   ? " is-invalid"
                      //   : "")
                    }
                    onKeyDown={(e) => this.avoidOnchange(e)}
                    dateFormat="dd-MM-yyyy"
                    highlightDates={new Date()}
                    placeholderText=" DD/MM/YYYY"
                    date=""
                    onChange={date => this.setState({ eventDate: new Date(date), eventdate_day: new Date(date).getDay() })}
                    selected={listingDetails[listingCount].eventDate}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="line-item-grid-4-section">
            {listingDetails[listingCount].seat_type_id != seatTypes.generalAdmission.id &&
              <>
                <div className="custom-form-item">
                  <label htmlFor="section" className="form-label-userprofile">
                    Section  <span>*</span>
                  </label>

                  <div className="form-group">
                    <Input
                      placeholder="Section"
                      className="form-control"
                      name="section"
                      value={listingDetails[listingCount].section}
                    />
                  </div>
                </div>
                <div className="inner-line-grid--item">
                  <div className="custom-form-item">
                    <label htmlFor="row" className="form-label-userprofile">
                      Row <span>*</span>
                    </label>

                  <div className="form-group">
                    <Input
                      placeholder="Row"
                      className="form-control"
                      name="row"
                      value={listingDetails[listingCount].row}
                    />
                  </div>
                </div>
                <div className="custom-form-item">
                  <label htmlFor="seat_start" className="form-label-userprofile">
                    Seat Start  <span>*</span>
                  </label>

                  <div className="form-group">
                    <Input
                      placeholder="Seat Start"
                      className="form-control"
                      name="seat_start"
                      value={listingDetails[listingCount].seat_start}
                    />
                    </div>
                  </div>
                </div>
                <div className="custom-form-item">
                  <label htmlFor="seat_end" className="form-label-userprofile">
                    Seat End <span>*</span>
                  </label>

                  <div className="form-group">
                    <Input
                      placeholder="Seat End"
                      className="form-control"
                      name="seat_end"
                      value={listingDetails[listingCount].seat_end}
                    />
                  </div>
                </div>
              </>}
            <div className="custom-form-item">
              <label htmlFor="cost" className="form-label-userprofile">
                Cost <span>*</span>
              </label>

              <div className="form-group">
                <Input
                  placeholder="$ 0"
                  className="form-control"
                  name="cost"
                  value={listingDetails[listingCount].cost}
                />
              </div>
            </div>
            <div className="inner-line-grid--item">
            <div className="custom-form-item">
              <label htmlFor="quantity" className="form-label-userprofile">
                Quantity <span>*</span>
              </label>

              <div className="form-group">
                <Input
                  placeholder="0"
                  className="form-control"
                  name="quantity"
                  value={listingDetails[listingCount].quantity}
                />
              </div>
            </div>
            <div className="custom-form-item">
              <label htmlFor="unit_cost" className="form-label-userprofile">
                Unit Cost
              </label>

              <div className="form-group">
                <Input
                  placeholder="$ 0"
                  className="form-control"
                  name="unit_cost"
                  value={listingDetails[listingCount].unit_cost}
                />
              </div>
            </div>
            </div>
            <div className="custom-form-item">
              <label htmlFor="taxes" className="form-label-userprofile">
                Taxes
              </label>

              <div className="form-group">
                <Input
                  placeholder="0"
                  className="form-control"
                  name="taxes"
                  value={listingDetails[listingCount].taxes}
                />
              </div>
            </div>
          </div>

          <div className="line-item-grid-add-btn">
            {this.state.itemsLength == listingCount &&
              <div className="custom-form-item">
                <div className="form-group m-0">
                  <Button onClick={this.addform}>
                    Add Line Item
                  </Button>
                </div>
              </div>
            }
          </div>

          {value > 1 ?
            <Button className="remove-btn"
              dashed
              style={{ margin: "5px 0 5px 0" }}
              onClick={() => this.removeform(index)}
            >
              <CloseIcon />
            </Button>
            : ''}

        </Card>

      )
    }




    return !this.state.loading ? (
      <UserContext.Consumer>
        {context => (
          <div>
            <div
              className="popup-body mt-0"
            //  style={{ border: "1px solid green" }}
            >

              <Formik
                initialValues={{
                  userId: this.context.user.id,
                  purchaseDate: this.state.purchaseDate,
                  paymentMethod: this.state.paymentMethod ? this.state.paymentMethod.value : "",
                  salesTax: this.state.salesTax,
                  purchaseAmount: this.state.purchaseAmount,
                  cardLastFourDigits: this.state.cardLastFourDigits,
                  shipAndHandle: this.state.shipAndHandle,
                  paymentType: this.state.paymentType ? this.state.paymentType.value : "",
                  referenceId: this.state.referenceId,
                  otherFees: this.state.otherFees,
                  notes: this.state.notes,
                  tags: this.state.tags,
                  currencyId: this.state.currencyId,
                  createdBy: this.context.user.id,
                  vendorId: this.state.vendordetail.vendorID,
                  vendorName: this.state.vendordetail.vendorName,
                  vendorEmail: this.state.vendordetail.vendorEmail,
                  vendorPhone: this.state.vendordetail.vendorPhone,
                  vendorAddress1: this.state.vendordetail.vendorAddress1,
                  vendorAddress2: this.state.vendordetail.vendorAddress2,
                  vendorCity: this.state.vendordetail.vendorCity && this.state.vendordetail.vendorCity ? this.state.vendordetail.vendorCity.value : "",
                  vendorState: this.state.vendordetail.vendorState && this.state.vendordetail.vendorState ? this.state.vendordetail.vendorState.value : "",
                  vendorZipcode: this.state.vendordetail.vendorZipcode,
                  vendorCountry: this.state.vendordetail.vendorCountry && this.state.vendordetail.vendorCountry ? this.state.vendordetail.vendorCountry.value : "",
                  listingInformationsJSON: "",
                  InvoiceAttachmentInformationsJSON: {}

                }}
                enableReinitialize={true}

                validationSchema={Yup.object().shape({
                  purchaseDate: Yup.string().required("Purchase date is required"),
                  paymentMethod: Yup.string().required("Payment method is required"),
                  salesTax: Yup.number().required("Sales tax is required").typeError('Give valid number to sales tax'),
                  purchaseAmount: Yup.number().required("Purchase amount is required").typeError('Give valid amount to purchase amount'),
                  cardLastFourDigits: Yup.string()
                    .required("Card last four digits is required")
                    .matches(/^[0-9]+$/, "Card last four digits must be only digits")
                    .min(4, 'Card last four digits should be a minimum of 4 digits')
                    .max(4, 'Card last four digits should be a maximum of 4 digits'),
                  shipAndHandle: Yup.string().required("Ship & Handle fees is required"),
                  paymentType: Yup.string().required("Payment type is required"),
                  referenceId: Yup.string().required("Reference Id is required"),
                  otherFees: Yup.number().required("Other fees is required").typeError('Give valid number to Other fees'),
                  notes: Yup.string().required("Notes is required"),
                  tags: Yup.string().required("Tags is required"),

                  vendorName: Yup.string().required("Vendor name is required"),
                  vendorEmail: Yup.string().email().required("Vendor email is required").typeError('Give valid email'),
                  vendorPhone: Yup.string()
                    .required("Vendor Phone number is required")
                    .matches(/^[0-9]+$/, "Vendor phone number must be only digits")
                    .min(10, 'Vendor phone number should be a minimum of 10 digits')
                    .max(15, 'Vendor phone number should be a maximum of 15 digits')
                    .typeError('Give valid vendor phone number'),
                  vendorAddress1: Yup.string().required("Vendor Address is required").typeError('Give valid vendor address'),
                  vendorCity: Yup.string().required("Vendor city is required").typeError('Give valid vendor city'),
                  vendorState: Yup.string().required("Vendor state is required").typeError('Give valid vendor state'),
                  vendorZipcode: Yup.string()
                    .required("Vendor Zip Code is required")
                    .matches(/^[a-zA-Z0-9]+$/, "Vendor zipCode must be only alphanumerics")
                    .min(3, 'Vendor zipCode should be a minimum of 3 alphanumerics')
                    .max(8, 'Vendor zipCode should be a maximum of 8 alphanumerics')
                    .typeError('Give valid vendor zipcode'),
                  vendorCountry: Yup.string().required("Vendor country is required").typeError('Give valid vendor country'),
                  vendorInformationsJSON: ""
                })}

                onSubmit={async (fields) => {
                  if(!this.state.listingDetails[0].listingId){
                    await this.setState({
                      alertmodalmsg: "Listing Group ID Should not Empty !",
                      alertcloseButtonModal: true
                    });
                  }
                  else{

                  var data = { listingInformationsJSON: this.state.listingDetails }
                  let listdata;
                  listdata = await { ...fields, ...data };

                  if (this.state.InvoiceAttachFile) {
                    let timestamp = new Date().getTime();
                    var valueAttachment = {
                      InvoiceAttachName: timestamp + "_" + this.state.InvoiceAttachName,
                      InvoiceAttachFile: this.state.InvoiceAttachFile
                    }
                    var attachInfo = { InvoiceAttachmentInformationsJSON: valueAttachment }
                    listdata = await { ...listdata, ...attachInfo };
                    await this.setState({ InvoiceAttachName: valueAttachment.InvoiceAttachName });
                  }
                  await this.setState({
                    closeButtonModal: true,
                    datafields: listdata
                  });
                  }
                }}

                render={({ errors, status, touched, values, handleChange, handleBlur }) => (
                  <>
                    <Form autoComplete="off" >

                      {/* Payment Information Start */}

                      <Card
                        className="createPurchase-order-card"
                      >
                        <div className="purchase-pay-info">
                          <div>
                            <h5
                              className="purchase-order-form-headings secondary-font-family primary-color"
                              style={{ display: "inline-block", marginBottom: "10px" }}
                            >
                              Payment Information
                            </h5>
                          </div>

                          {!this.state.IsShowAttachInvoiceForm ?

                            <div className="attached-invoice-item">
                              <Button className="btn-invoice btn" onClick={this.toggleAttachmentForm} >Attach Invoice</Button>
                            </div>
                            :

                            <div className="document--grid-edit-sec">
                              <div className="document--item">
                                <div className="document-info">
                                  <div className="attach-btn">
                                    <AttachmentIcon /> {this.state.InvoiceAttachName}</div>
                                </div>
                              </div>

                              <div className="replace-sec_gr">
                                <div className="replace--item-kjk">
                                  <Input type="file" name="invoiceAttachment" onChange={this.changeAttachmentForm} />
                                </div>
                                <Button className="invoice-replace--btn replace-order-btn">Choose Attachment</Button>
                              </div>
                            </div>


                          }

                        </div>

                        <div className="payment-information-grid-item grid-3-items-purchase">
                          <div className="custom-form-item">

                            <div className="form-group">
                              <span className="purchase-order-form-label">
                                {this.props.t("purchases.purchaseDate")}{" "}
                              </span>

                              <div className="date--PickQr">
                                <span className="custom-calendar-img">
                                  <img src={iconInhand} alt="icon"/>
                                </span>
                                <DatePicker
                                  style={{ width: "100%" }}
                                  name="purchaseDate"
                                  placeholder={this.props.t("purchases.purchaseDate")}
                                  selected={this.state.purchaseDate}
                                  onKeyDown={(e) => e.preventDefault()}
                                  onChange={this.dateRangeDisplay}
                                  dateFormat="dd/MM/yyyy"
                                  placeholderText=" DD/MM/YYYY"
                                  isClearable={this.state.purchaseDate ? true : false} disabledKeyboardNavigation
                                  className={'formik-input-user-form form-control' + (errors.purchaseDate && touched.purchaseDate ? ' is-invalid' : '')}
                                />
                                <ErrorMessage
                                  name="purchaseDate"
                                  component="div"
                                  className="invalid-feedback" style={{ display: 'block' }}
                                />
                              </div>
                            </div>

                            {/* form field */}
                            <div className="form-group">
                              <span className="purchase-order-form-label">
                                {this.props.t("purchases.paymentGroup")}{" "}
                              </span>
                              <Select
                                showSearch
                                filterOption={this.dropdownFilterOption}
                                placeholder={this.props.t("purchases.paymentGroup")}
                                style={{ width: "100%" }}
                                allowClear
                                isClearable
                                name="paymentMethod"
                                onSearch={this.onSearch}
                                value={this.state.paymentMethod}
                                onChange={(e) => this.setState({ paymentMethod: e })}
                                options={paymentMethodOptions}
                                className={(errors.paymentMethod && touched.paymentMethod ? ' is-invalid' : '')}
                              >
                              </Select>

                              <ErrorMessage
                                name="paymentMethod"
                                component="div"
                                className="invalid-feedback"
                              />
                            </div>

                            {/* form field */}
                            <div
                              className="form-group"
                              style={{ width: "100%" }}
                            >
                              <span className="purchase-order-form-label">
                                {this.props.t("purchases.salesTax")}{" "}
                              </span>
                              <Input
                                name="salesTax"
                                value={this.state.salesTax}
                                onChange={(e) => this.setState({ salesTax: e.target.value })}
                                style={{ width: "100%" }}
                                type="number"
                                placeholder={this.props.t("purchases.percentZero")}
                                className={(errors.salesTax && touched.salesTax ? ' is-invalid' : '')}
                              />
                              <ErrorMessage
                                name="salesTax"
                                component="div"
                                className="invalid-feedback"
                              />
                            </div>




                          </div>

                          <div className="custom-form-item">

                            {/* form field */}
                            <div className="form-group">
                              <span className="purchase-order-form-label">
                                {this.props.t("purchases.purchaseAmount")}{" "}
                              </span>

                              <Input
                                name="purchaseAmount"
                                value={values.purchaseAmount}
                                onChange={(e) => this.setState({ purchaseAmount: e.target.value })}
                                className={(errors.purchaseAmount && touched.purchaseAmount ? ' is-invalid' : '')}
                                placeholder={this.props.t("purchases.amountZero")}
                              />
                              <ErrorMessage name="purchaseAmount" component="div" className="invalid-feedback" />
                            </div>


                            {/* form field */}
                            <div className="form-group">
                              <span className="purchase-order-form-label">
                                {this.props.t("purchases.cardLast4")}{" "}
                              </span>
                              <Input
                                placeholder={this.props.t("purchases.cardLast4mask")}
                                name="cardLastFourDigits"
                                value={this.state.cardLastFourDigits}
                                onChange={(e) => this.lastFourDigits(e)}
                                className={(errors.cardLastFourDigits && touched.cardLastFourDigits ? ' is-invalid' : '')}
                              />
                              <ErrorMessage name="cardLastFourDigits" component="div" className="invalid-feedback" />
                            </div>

                            {/* form field */}
                            <div className="form-group">
                              <span className="purchase-order-form-label">
                                {this.props.t("purchases.shipAndHandle")}{" "}
                              </span>

                              <Input
                                type="number"
                                placeholder={this.props.t("purchases.amountZero")}
                                name="shipAndHandle"
                                value={this.state.shipAndHandle}
                                onChange={(e) => this.setState({ shipAndHandle: e.target.value })}
                                className={(errors.shipAndHandle && touched.shipAndHandle ? ' is-invalid' : '')}
                              />
                              <ErrorMessage name="shipAndHandle" component="div" className="invalid-feedback" />

                            </div>

                          </div>

                          <div className="custom-form-item">
                            {/* form field */}
                            <div className="form-group" style={{ width: "100%" }}>
                              <span className="purchase-order-form-label">
                                {this.props.t("purchases.paymentType")}
                              </span>
                              <Select
                                showSearch
                                filterOption={this.dropdownFilterOption}
                                placeholder={this.props.t("purchases.paymentType")}
                                style={{ width: "100%" }}
                                allowClear
                                isClearable
                                name="paymentType"
                                onSearch={this.onSearch}
                                value={this.state.paymentType}
                                options={paymentTypesOptions}
                                onChange={(e) => this.setState({ paymentType: e })}
                                className={(errors.paymentType && touched.paymentType ? ' is-invalid' : '')}
                              >
                                {/* <Option value="" selected>Select</Option> */}


                              </Select>
                              <ErrorMessage name="paymentType" component="div" className="invalid-feedback" />
                            </div>

                            {/* form field */}
                            <div className="form-group">
                              <span className="purchase-order-form-label">
                                {this.props.t("purchases.listingId")}{" "}
                              </span>

                              <Input
                                placeholder={this.props.t("purchases.listingId")}
                                name="referenceId"
                                value={this.state.referenceId}
                                onChange={(e) => this.setState({ referenceId: e.target.value })}
                                className={(errors.referenceId && touched.referenceId ? ' is-invalid' : '')}
                              />
                              <ErrorMessage name="referenceId" component="div" className="invalid-feedback" />
                            </div>



                            {/* form field */}
                            <div className="form-group">
                              <span className="purchase-order-form-label">
                                {this.props.t("purchases.otherFees")}{" "}
                              </span>

                              <Input
                                type="number"
                                placeholder={this.props.t("purchases.amountZero")}
                                name="otherFees"
                                value={this.state.otherFees}
                                onChange={(e) => this.setState({ otherFees: e.target.value })}
                                className={(errors.otherFees && touched.otherFees ? ' is-invalid' : '')}
                              />
                              <ErrorMessage name="otherFees" component="div" className="invalid-feedback" />
                            </div>

                          </div>
                        </div>
                      </Card>
                      {/* Payment Information End */}


                      {/* Vendor Information Start */}

                      <Card
                        className="createPurchase-order-card"
                      >
                        <div>
                          <h5
                            className="purchase-order-form-headings secondary-font-family primary-color"
                            style={{ display: "inline-block" }}
                          >
                            Vendor
                          </h5>
                        </div>
                        <div
                          className="purchase-order-card-container purchase-order-grid-section"
                        >
                          <div className="custom-form-item">
                            {/* input Name */}
                            <div className="form-group">
                              <span className="purchase-order-form-label">
                                {this.props.t("vendor.name")}{" "}
                              </span>

                              <AutoComplete
                                style={{ width: "100%" }}
                                onSearch={text => this.onSearchVendorName(text, "vendor")}
                                placeholder={this.props.t("vendor.name")}
                                name="vendorName"
                                value={this.state.vendordetail.vendorName}
                                onChange={(e) => this.onVendorStatus(e, "vendorName")}
                                className={(errors.vendorName && touched.vendorName ? ' is-invalid' : '')}
                              />

                              <ErrorMessage name="vendorName" component="div" className="invalid-feedback" />

                              {suggestions && suggestions.length > 0 &&
                                < div className="TypeAheadDropDown">
                                  {
                                    this.renderSuggestions()
                                  }
                                </div>
                              }

                            </div>
                            {/* Address */}
                            <div className="form-group">
                              <span className="purchase-order-form-label">
                                {this.props.t("vendor.address")}{" 1"}
                              </span>
                              <Input
                                placeholder={this.props.t("vendor.address")}
                                name="vendorAddress1"
                                value={this.state.vendordetail.vendorAddress1}
                                onChange={(e) => this.onVendorStatus(e, "vendorAddress1")}
                                className={(errors.vendorAddress1 && touched.vendorAddress1 ? ' is-invalid' : '')}
                              />
                              <ErrorMessage name="vendorAddress1" component="div" className="invalid-feedback" />
                            </div>
                            {/* address#2 */}
                            <div className="form-group">
                              <span className="purchase-order-form-label">
                                {this.props.t("vendor.address")}{" 2"}
                              </span>

                              <Input
                                placeholder={this.props.t("vendor.address2")}
                                name="vendorAddress2"
                                value={this.state.vendordetail.vendorAddress2}
                                onChange={(e) => this.onVendorStatus(e, "vendorAddress2")}
                                className={(errors.vendorAddress2 && touched.vendorAddress2 ? ' is-invalid' : '')}
                              />
                              <ErrorMessage name="vendorAddress2" component="div" className="invalid-feedback" />
                            </div>
                          </div>
                          <div className="custom-form-item p-0">
                            <div className="purchaser-order-gris-2 vendor-purchase">
                              <div className="form-group" style={{ width: "100%" }}>
                                <span className="purchase-order-form-label">
                                  {this.props.t("vendor.email")}{" "}
                                </span>
                                <Input
                                  placeholder={this.props.t("vendor.email")}
                                  name="vendorEmail"
                                  value={this.state.vendordetail.vendorEmail}
                                  onChange={(e) => this.onVendorStatus(e, "vendorEmail")}
                                  className={(errors.vendorEmail && touched.vendorEmail ? ' is-invalid' : '')}
                                />
                                <ErrorMessage name="vendorEmail" component="div" className="invalid-feedback" />
                              </div>

                              <div className="form-group" style={{ width: "100%" }}>
                                <span className="purchase-order-form-label">
                                  {this.props.t("vendor.phone")}{" "}
                                </span>
                                <Input
                                  placeholder={this.props.t("vendor.phone")}
                                  name="vendorPhone"
                                  value={this.state.vendordetail.vendorPhone?this.state.vendordetail.vendorPhone.replace(/[\D]/g, ""):''}
                                  onChange={(e) => this.onVendorStatus(e, "vendorPhone")}
                                  className={(errors.vendorPhone && touched.vendorPhone ? ' is-invalid' : '')}
                                />
                                <ErrorMessage name="vendorPhone" component="div" className="invalid-feedback" />
                              </div>
                            </div>
                            <div className="purchaser-order-gris-2 vendor-purchase">
                            <div className="form-group" style={{ width: "100%" }}>
                                <span className="purchase-order-form-label">
                                  {this.props.t("vendor.country")}{" "}
                                </span>
                                <Select
                                  showSearch
                                  filterOption={this.dropdownFilterOption}
                                  placeholder={this.props.t("vendor.country")}
                                  style={{ width: "100%" }}
                                  allowClear
                                  isClearable
                                  name="vendorCountry"
                                  onSearch={this.onSearch}
                                  options={vendorCountryOptions}
                                  value={this.state.vendordetail.vendorCountry}
                                  onChange={(e) => this.onVendorStatus(e, "vendorCountry")}
                                  className={(errors.vendorCountry && touched.vendorCountry ? ' is-invalid' : '')}
                                />
                                <ErrorMessage name="vendorCountry" component="div" className="invalid-feedback" />
                              </div>
                              
                              <div className="form-group" style={{ width: "100%" }}>
                                <span className="purchase-order-form-label">
                                  {this.props.t("vendor.city")}{" "}
                                </span>
                                <Select
                                  {...this.props}
                                  placeholder={this.props.t("vendor.city")}
                                  style={{ width: "100%" }}
                                  name="vendorCity"
                                  filterOption={this.dropdownFilterOption}
                                  value={this.state.vendordetail.vendorCity}
                                  options={this.state.cityList}
                                  onChange={(e) => this.onVendorStatus(e, "vendorCity")}
                                  className={(errors.vendorCity && touched.vendorCity ? ' is-invalid' : '')}
                                  allowClear
                                  showSearch
                                  isClearable
                                />
                                <ErrorMessage name="vendorCity" component="div" className="invalid-feedback" />
                              </div>
                            </div>
                            <div className="purchaser-order-gris-2 vendor-purchase">
                            <div className="form-group" style={{ width: "100%" }}>
                                <span className="purchase-order-form-label">
                                  {this.props.t("vendor.state")}{" "}
                                </span>
                                <Select
                                  {...this.props}
                                  placeholder={this.props.t("vendor.state")}
                                  style={{ width: "100%" }}
                                  name="vendorState"
                                  filterOption={this.dropdownFilterOption}
                                  value={this.state.vendordetail.vendorState}
                                  options={this.state.stateList}
                                  onChange={(e) => this.onVendorStatus(e, "vendorState")}
                                  className={(errors.vendorState && touched.vendorState ? ' is-invalid' : '')}
                                  allowClear
                                  showSearch
                                  isClearable
                                />
                                <ErrorMessage name="vendorState" component="div" className="invalid-feedback" />
                            </div>
                              <div className="form-group" style={{ width: "100%" }}>
                                <span className="purchase-order-form-label">
                                  {this.props.t("vendor.zip")}{" "}
                                </span>
                                <Input
                                  placeholder={this.props.t("vendor.zip")}
                                  name="vendorZipcode"
                                  value={this.state.vendordetail.vendorZipcode}
                                  onChange={(e) => this.onVendorStatus(e, "vendorZipcode")}
                                  className={(errors.vendorZipcode && touched.vendorZipcode ? ' is-invalid' : '')}
                                />
                                <ErrorMessage name="vendorZipcode" component="div" className="invalid-feedback" />
                              </div>
                            </div>

                          </div>
                        </div>
                      </Card>

                      {/* Vendor Information End */}


                      {/* Appended Form Start */}
                      <div className="line-item-form">
                        {formitems}
                      </div>
                      {/* Appended Form End */}


                      {/* Notes section Start */}
                      <Card className="createPurchase-order-card create-purchase-footer">
                        <h5 className="purchase-order-form-headings secondary-font-family primary-color">Notes</h5>
                        <div className="purchaser-order-gris-2">
                          <div className="custom-form-textarea-item p-0">
                            <div className="form-group m-0">
                              <span className="purchase-order-form-label form-label-userprofile">
                                {this.props.t("purchases.notes")}{" "}<span>*</span>
                              </span>
                              <textarea
                                placeholder={this.props.t("purchases.notes")}
                                name="notes"
                                value={this.state.notes}
                                onChange={(e) => this.setState({ notes: e.target.value })}
                                className={(errors.notes && touched.notes ? ' is-invalid' : '')}
                              >{this.state.notes}</textarea>
                              <ErrorMessage name="notes" component="div" className="invalid-feedback" />
                            </div>
                          </div>


                          <div className="custom-form-textarea-item p-0">

                            {/* form field */}
                            <div className="form-group m-0" style={{ width: "100%" }}>
                              <span className="purchase-order-form-label form-label-userprofile">
                                {this.props.t("purchases.tags")} <span>*</span>
                              </span>

                              <Select
                                showSearch
                                filterOption={this.dropdownFilterOption}
                                placeholder={this.props.t("purchases.tags")}
                                style={{ width: "100%" }}
                                allowClear
                                isMulti
                                name="tags"
                                onSearch={this.onSearch}
                                value={this.state.tags}
                                options={tagOptions}
                                onChange={(e) => this.setState({ tags: e })}
                                className={(errors.tags && touched.tags ? ' is-invalid' : '')}
                                isClearable
                              >
                              </Select>
                              <ErrorMessage name="tags" component="div" className="invalid-feedback" />
                            </div>
                          </div>


                        </div>
                      </Card>
                      {/* Notes section End */}


                      {/* Submit section Start */}
                      <div className="purchase-order-submit-section">
                        <div className="form-group">

                          <button type="submit" className="btn event-submit-btn primary-bg-color stylefont-weight-medium">
                            {this.props.t("purchases.add")}
                          </button>

                          {/* <Button
                              style={{
                              marginTop: 16
                              }}
                              type="primary"
                              htmlType="submit"
                              >
                              {this.props.t("purchases.add")}
                              </Button> */}

                        </div>
                      </div>
                      {/* Submit section End */}
                    </Form>
                  </>
                )}
              />

              {/* Common Modal popup start */}

              {/* Comfirm Modal */}
              <Confirm
                header={"Confirm"}
                body={"Are you sure to submit"}
                show={this.state.closeButtonModal}
                submit={() => this.handlesubmitButtonModal(true)}
                cancel={() => this.handlesubmitButtonModal(false)}
                closeButton={() => this.setState({ closeButtonModal: false })}
              />

              {/* Info Modal */}
              <Info
                header={"Info"}
                body={this.state.infomodalmsg}
                show={this.state.infocloseButtonModal}
                submit={() => this.handleInfoSubmitButtonModal(true)}
                closeButton={() => this.handleInfoSubmitButtonModal(true)}
              />

              {/* Alert Modal */}
              <Alert
                header={"Alert"}
                body={this.state.alertmodalmsg}
                show={this.state.alertcloseButtonModal}
                submit={() => this.handleAlertSubmitButtonModal(true)}
                closeButton={() => this.handleAlertSubmitButtonModal(true)}
              />

              {/* Common Modal popup end */}

            </div>
          </div>
        )}
      </UserContext.Consumer>
    ) : (
      <Loader />
    );
  }
}
export default withRouter(withTranslation()(AddPurchaseOrder));
