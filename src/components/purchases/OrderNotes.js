import React, { useState, useEffect, useContext } from "react";


import { useTranslation } from "react-i18next";

import UserContext from "../../contexts/user.jsx";
import Form from 'antd/es/form';
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Modal from 'antd/es/modal';
// style icon
import styled from 'styled-components'
import { Edit } from '@styled-icons/material/Edit'
import Select from 'react-select'
import { updatePurchaseOrderDetails, updatePurchaseOrderTagsById } from "../../services/purchaseOrderService";

const EditIcon = styled(Edit)`
  color: #aeb1be;
  width:15px;
  height:15px;
`
// const { Option } = Select;
const toolTipStyle = {
  fontSize: "18px",
  marginLeft: "5px",
  paddingBottom: "3px",
  color: "rgba(89,89,89, 0.4)"
};


const Vendor = (props) => {

  const { user } = useContext(UserContext);

  const { t } = useTranslation();
  // modal open close

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [orderNotes, setorderNotes] = useState('');
  const [orderTags, setorderTags] = useState([]);
  const [orderTagsIds, setorderTagsIds] = useState([]);

  //Common 
  const [lableInput, setIsField] = useState(false);
  const [orderId, setorderId] = useState();
  const [getPayTags, setPayTags] = useState([]);

  /*label start */
  const [getPayTagsLabel, setPayTagsLabel] = useState([]);
  const [getPayTagsInput, setPayTagsInput] = useState([]);

  /*label end */

  /* input start */
  const [orderNotesInput, setorderNotesInput] = useState(orderNotes);

  /* input end*/

  const showModal = () => {
    // setIsModalVisible(true);
    setIsField(true);
    setorderNotesInput(orderNotes);
    setPayTagsInput(getPayTagsLabel)

  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };


  let orderInfo = props.orderInfo && props.orderInfo.length ? props.orderInfo[0] : '';
  let payTags = props.payTags && props.payTags.length ? props.payTags : false;


  useEffect(() => {

    setorderNotes(orderInfo.notes)
    setorderTags(orderInfo.tags)
    setorderTagsIds(orderInfo.tagids);
    setorderNotesInput(orderInfo.notes)
    setorderId(orderInfo.id);



    if (orderInfo.tagids && orderInfo.tags) {
      let tagsOptions = []
      for (var tagIdCount = 0; tagIdCount < orderInfo.tagids.length; tagIdCount++) {
        tagsOptions.push({ key: 'orderInfo_' + tagIdCount, value: orderInfo.tagids[tagIdCount], label: orderInfo.tags[tagIdCount] })
      }
      setPayTagsLabel(tagsOptions);
    }

  }, [orderInfo]);

  useEffect(() => {
    setPayTags(payTags);
  }, [payTags])

  const saveInvoice = async () => {
    setIsField(false);
    setorderNotes(orderNotesInput);
    setPayTagsLabel(getPayTagsInput)
    let formData = { notes: orderNotesInput, id: orderId }
    let pot_array = []
    for (var valueLength = 0; valueLength < getPayTagsInput.length; valueLength++) {
      pot_array.push(getPayTagsInput[valueLength].value)
    }
    let formDataTags = { tags: pot_array, id: orderId }
    await updatePurchaseOrderDetails(formData);
    await updatePurchaseOrderTagsById(formDataTags);
    props.onChildUpdate();
  }
  const closeInvoice = async () => {
    setIsField(false);
    setorderNotesInput(orderNotes)

  }

  const changeTextarea = (e) => {
    setorderNotesInput(e.target.value)
  }

  const changeTags = (e) => {

    setPayTagsInput(e)
  }
  return (
    <>
      <Card
        className={lableInput == true ? "order-detail-channel-card purchase-edit-card" : "order-detail-channel-card"}
      >
        <div className="purchase-pay-info">
          <div>
            <h5
              className="purchase-order-form-headings secondary-font-family p-0 primary-color"
              style={{ display: "inline-block" }}
            >
              Notes
            </h5>
          </div>
          <div className="attached-invoice-item">
            {lableInput == false ?
              <Button className="btn-edit btn" onClick={showModal}><EditIcon /></Button> :
              <span>
                <Button className="btn-save-tn btn" onClick={saveInvoice}>Save</Button>
                <Button className="btn-edit-tn btn" onClick={closeInvoice}>Cancel</Button>
              </span>
            }
          </div>
        </div>
        <div>
          {lableInput == false ?
            <div className="order-grid-section p-0">
              {orderNotesInput && orderNotesInput.trim() ?
                <p className="order-notes-txt">
                  {orderNotesInput}
                </p>
                :
                <p className="order-notes-txt">
                  Notes:  None
                </p>
              }
              <div className="order-tags-grid ">
                <div className="tags-title">
                  {
                    getPayTagsLabel && getPayTagsLabel.length <= 0 && (<p className="order-notes-txt">Tags:&nbsp;None</p>)
                  }
                  {
                    getPayTagsLabel && getPayTagsLabel.length > 0 && <p className="order-notes-txt">Tags:</p>
                  }
                </div>
                <div className="tags-box-item">
                  <ul className="p-0">
                    {
                      getPayTagsLabel && getPayTagsLabel.length > 0 && getPayTagsLabel.map((tags, i) => {
                        return (
                          <li className="list-of--tags"> <span>{tags.label}</span></li>
                        )
                      })
                    }

                  </ul>
                  
                </div>
              </div>
            </div> :
            <div className="order-grid-section p-0">
              <div className="order-notes-txt">
                <textarea className="form-control edit-text-area" onChange={(e) => changeTextarea(e)} >{orderNotesInput}</textarea>
              </div>
              <div className="order-tags-grid edit-tags-show">
                <div className="tags-title">
                  <h5>Add Tags:</h5>
                </div>
                <div className="tags-box-item">
                  <Select options={getPayTags} value={getPayTagsInput} isMulti onChange={(e) => changeTags(e)} />
                </div>
              </div>
            </div>
          }
        </div>



      </Card>
      {/* modal popup */}
      <Modal title=""
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
        className="payment-info-modal"

      >
        <Form>
          <div className="purchase-pay-info">
            <div>
              <h5
                className="purchase-order-form-headings secondary-font-family primary-color"
                style={{ display: "inline-block", marginBottom: "10px" }}
              >
                Edit Notes
              </h5>
            </div>
          </div>
          <div className="purchaser-order-gris-2">
            <div className="custom-form-textarea-item p-0">
              <Form.Item validateStatus="success">
                <span className="purchase-order-form-label">
                  {t("purchases.notes")}{" "}
                </span>
                <textarea placeholder={t("purchases.notes")} ></textarea>
              </Form.Item>
            </div>
            <div className="custom-form-textarea-item p-0">
              <Form.Item validateStatus="success">
                <span className="purchase-order-form-label">
                  {t("purchases.tags")}{" "}
                </span>
                <textarea placeholder={t("purchases.tags")} ></textarea>
              </Form.Item>
            </div>
          </div>
          <div className="form-footer-item">
            <Button className="cancel-btn-payinfo">Cancel</Button>
            <Button className="submit-btn-payinfo">Submit</Button>
          </div>
        </Form>
      </Modal>
    </>
  );
};

export default Vendor;
