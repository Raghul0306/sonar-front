import React, { Component } from "react";
import loadable from "@loadable/component";
import UserContext from "../../contexts/user.jsx";
import Modal from 'antd/es/modal';
import { withTranslation } from "react-i18next";
import Loader from "../Loader/Loader";
const AddPurchaseOrderForm = loadable(() => import("./addPurchaseOrder"));
const PurchasesGrid = loadable(() => import("../datagrid/purchasesGrid"));

export class MyListings extends Component {
  state = { visible: false, loading: true, reportModalVisible: false };

  showReportModal = () => {
    this.setState({
      reportModalVisible: true
    });
  };
  closeReportModal = () => {
    this.setState({
      reportModalVisible: false
    });
  };
  showListingModal = () => {
    this.setState({
      visible: true
    });
  };

  showUploadModal = () => {
    this.setState({
      uploadVisible: true
    });
  };

  handleOk = e => {
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  handleUploadOk = e => {
    this.setState({
      uploadVisible: false
    });
  };

  handleUploadCancel = e => {
    this.setState({
      uploadVisible: false
    });
  };
  componentDidMount = () => {
    this.setState({ loading: false });
  };
  render() {
    return !this.state.loading ? (
      <div>
        <UserContext.Consumer>
          {context => (
            <div>
              <div style={{}}>
                {/* <Button type="primary" onClick={this.showListingModal}>
                  <CreateOutlinedIcon
                    style={{
                      color: "white",
                      fontSize: "20px",
                      paddingRight: "5px"
                    }}
                  />
                  {this.props.t("purchases.add")}
                </Button> */}
                <Modal
                  title={this.props.t("form.addPurchaseOrder")}
                  visible={this.state.visible}
                  onOk={this.handleOk}
                  footer={null}
                  onCancel={this.handleCancel}
                  closable={false}
                  width="60%"
                  maskClosable={false}
                >
                  <AddPurchaseOrderForm
                    formComplete={this.handleOk}
                    handleCancel={this.handleCancel}
                  />
                </Modal>
              </div>
              <PurchasesGrid />
            </div>
          )}
        </UserContext.Consumer>
      </div>
    ) : (
      <Loader />
    );
  }
}

export default withTranslation()(MyListings);
