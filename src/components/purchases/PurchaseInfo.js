import React, { useState, useEffect, useContext } from "react";
import {
  Col
} from "shards-react";

import { useTranslation } from "react-i18next";

import UserContext from "../../contexts/user.jsx";
import Modal from 'antd/es/modal';
import Button from 'antd/es/button';
import Input from 'antd/es/input';
import Form from 'antd/es/form';
import Card from 'antd/es/card';
import Select from 'antd/es/select';

// style icon
import styled from 'styled-components'
import { Edit } from '@styled-icons/material/Edit'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { updatePurchaseOrderDetails } from "../../services/purchaseOrderService";
import * as moment from "moment-timezone";

const EditIcon = styled(Edit)`
  color: #aeb1be;
  width:15px;
  height:15px;
`
const { Option } = Select;
const toolTipStyle = {
  fontSize: "18px",
  marginLeft: "5px",
  paddingBottom: "3px",
  color: "rgba(89,89,89, 0.4)"
};

const PurchaseInfo = (props) => {

  const { user } = useContext(UserContext);

  const { t } = useTranslation();
  // modal open close

  const [isModalVisible, setIsModalVisible] = useState(false);

  const [lableInput, setIsField] = useState(false);

  //lable
  const [amountLable, setLableAmount] = useState(0);
  const [taxLable, setLableTax] = useState(0);
  const [shLable, setLableSH] = useState(0);
  const [oFeesLable, setLableOFees] = useState(0);
  const [purchaseDateLable, setLableDate] = useState('');
  const [orderId, setorderId] = useState();

  // field values
  const [amountInput, setAmount] = useState(0);
  const [taxInput, setTax] = useState(0);
  const [shInput, setSH] = useState(0);
  const [oFeesInput, setOFees] = useState(0);
  const [purchaseDateInput, setPrcDate] = useState('');
  const [formValidation, setFormValidation] = useState([]);



  const showModal = async () => {
    // setIsModalVisible(true);
    setIsField(true);
    await setAmount(amountLable);
    await setTax(taxLable);
    await setSH(shLable);
    await setOFees(oFeesLable);
    await setPrcDate(new Date(purchaseDateLable));
  };

  const closeInvoice = () => {
    setIsField(false);
  };
  const formValidationCustom = {
    isSubmitted: false,
    isValid: false,
    amountInput: false,
    amountInputMsg: '',

  }
  const saveInvoice = async () => {
    formValidationCustom.isSubmitted = true;
    formValidationCustom.isValid = true;
    if (!amountInput) {
      formValidationCustom.amountInput = true;
      formValidationCustom.isValid = false
      formValidationCustom.amountInputMsg = 'Amount Field is required';
    }
    else if (amountInput < 0) {
      formValidationCustom.amountInput = true;
      formValidationCustom.isValid = false
      formValidationCustom.amountInputMsg = 'Invalid amount';
    }
    else if (isNaN(amountInput)) {
      formValidationCustom.amountInput = true;
      formValidationCustom.isValid = false
      formValidationCustom.amountInputMsg = 'Numbers only';
    }
    if (true == formValidationCustom.isValid) {
      formSubmit();
    }
    setFormValidation(formValidationCustom);
  }
  const formSubmit = async () => {
    setIsField(false);
    setLableAmount(amountInput > 0 ? parseFloat(amountInput).toFixed(2) : 0);
    setLableTax(taxInput > 0 ? taxInput : 0);
    setLableSH(shInput > 0 ? shInput : 0);
    setLableOFees(oFeesInput > 0 ? oFeesInput : 0);
    let purchaseDateNew = purchaseDateInput.getFullYear() + '-' + (purchaseDateInput.getMonth() + 1) + '-' + purchaseDateInput.getDate();
    purchaseDateNew = purchaseDateNew ? moment.utc(purchaseDateNew).format("YYYY-MM-DD") : '';
    let formData = { purchase_amount: amountInput, purchase_date: purchaseDateNew, sales_tax: taxInput, fees: oFeesInput, shipping_and_handling_fee: shInput, id: orderId }
    await updatePurchaseOrderDetails(formData);
    props.onChildUpdate();
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  let purchaseInfo = props.purchaseInfo && props.purchaseInfo.length ? props.purchaseInfo[0] : {};
  let purchaseOrderId = props.orderId ? props.orderId : '';

  useEffect(() => {
    if (purchaseInfo) {
      setLableAmount(purchaseInfo.purchase_amount ? parseFloat(purchaseInfo.purchase_amount).toFixed(2) : '');
      setLableTax(purchaseInfo.sales_tax ? parseFloat(purchaseInfo.sales_tax).toFixed(2) : '0.00');
      setLableSH(purchaseInfo.shipping_and_handling_fee ? parseFloat(purchaseInfo.shipping_and_handling_fee).toFixed(2) : '0.00');
      setLableOFees(purchaseInfo.fees ? parseFloat(purchaseInfo.fees).toFixed(2) : '0.00');
      let purchaseDateInitial = purchaseInfo.purchase_date ? moment.utc(purchaseInfo.purchase_date).format("MM/DD/YYYY") : '';
      setLableDate(purchaseDateInitial);
      setorderId(purchaseInfo.id);
    }
  }, [purchaseInfo]);


  const changeAmount = async (e) => {
    let value = e.target.value;
    await setAmount(parseFloat(value));
  }
  const changeTax = async (e) => {
    await setTax(parseFloat(e.target.value));
  }
  const changeSH = async (e) => {
    await setSH(parseFloat(e.target.value));
  }
  const changeOF = async (e) => {
    await setOFees(parseFloat(e.target.value));
  }
  let ship_handling = 'S&H';
  return (
    <>
      <Card
        className={lableInput == true ? "order-detail-channel-card purchase-edit-card" : "order-detail-channel-card"}
      >
        <div className="purchase-pay-info">
          <div>
            <h5
              className="purchase-order-form-headings secondary-font-family p-0 primary-color"
              style={{ display: "inline-block" }}
            >
              Payment Info
            </h5>
          </div>
          <div className="attached-invoice-item">
            {!lableInput ?
              <Button className="btn-edit btn" onClick={showModal}><EditIcon /></Button> :
              <span>
                <Button className="btn-save-tn btn" onClick={saveInvoice}>Save</Button>
                <Button className="btn-edit-tn btn" onClick={closeInvoice}>Cancel</Button>
              </span>
            }
          </div>
        </div>
        <div className="purchaser-order-gris">
          <div className="order-grid-section p-0">
            {lableInput == false ?
              <div className="delivery-status-grid">
                <div className="left-delivery-grid stylefont-weight-medium"><h6>PO ID:</h6></div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>{purchaseOrderId}</h6></div>
                <div className="left-delivery-grid stylefont-weight-medium"><h6>Purchase Date:</h6></div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>{purchaseDateLable}</h6></div>
                <div className="left-delivery-grid stylefont-weight-medium"><h6>Purchase Amount:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>$ {amountLable}</h6></div>
                <div className="left-delivery-grid stylefont-weight-medium"><h6>Sales Tax:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>{taxLable}%</h6></div>
                <div className="left-delivery-grid stylefont-weight-medium"><h6>{ship_handling}:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>$ {shLable}</h6></div>
                <div className="left-delivery-grid stylefont-weight-medium"><h6>Other Fees:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>$ {oFeesLable}</h6></div>
              </div>
              :

              <div className="delivery-status-grid dynamic_edit-detail" >
                <div className="left-delivery-grid stylefont-weight-medium"><h6>PO ID:</h6></div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>{purchaseOrderId}</h6></div>
                <div className="left-delivery-grid stylefont-weight-medium"><h6>Purchase Date:</h6></div>
                <div className="right-delivery-grid stylefont-weight-medium">

                  <DatePicker
                    name="startDate"
                    // defaultValue={defaultStartDate}
                    format={"MM/DD/YYYY"}
                    onKeyDown={(e) => e.preventDefault()}
                    autoComplete="nofill" 
                    selected={purchaseDateInput ? purchaseDateInput : new Date()}
                    onChange={(date) => setPrcDate(date)}

                  />

                </div>
                <div className="left-delivery-grid stylefont-weight-medium"><h6>Purchase Amount:</h6> </div>

                <div className="right-delivery-grid stylefont-weight-medium">
                  <div className={formValidation.amountInput ? "edit-input-box is-in-valid" : "edit-input-box"}>
                    <h6>$</h6>
                    <input type="number" className="form-control" onChange={(e) => changeAmount(e)} value={amountInput} />
                  </div>
                  {formValidation && formValidation.isSubmitted && formValidation.amountInput &&
                    <span className="validate-error">{formValidation.amountInputMsg} </span>
                  }
                </div>
                <div className="left-delivery-grid stylefont-weight-medium"><h6>Sales Tax:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium">

                  <div className="edit-input-box">

                    <input type="number" min={0} max={99} className="form-control" onChange={(e) => changeTax(e)} value={taxInput} />
                    <h6> %</h6>
                  </div>
                </div>
                <div className="left-delivery-grid stylefont-weight-medium"><h6>{ship_handling}:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium">
                  <div className="edit-input-box">
                    <h6>$</h6>
                    <input type="number" min={0} max={9999} className="form-control" onChange={(e) => changeSH(e)} value={shInput} />
                  </div>
                </div>
                <div className="left-delivery-grid stylefont-weight-medium"><h6>Other Fees:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium">
                  <div className="edit-input-box">
                    <h6>$</h6>
                    <input type="number" min={0} max={9999} className="form-control" onChange={(e) => changeOF(e)} value={oFeesInput} />
                  </div>
                </div>
              </div>}

          </div>
        </div>
      </Card>
      {/* modal popup */}
      <Modal title=""
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
        className="payment-info-modal"

      >
        <Form>
          <div className="purchase-pay-info">
            <div>
              <h5
                className="purchase-order-form-headings secondary-font-family primary-color"
                style={{ display: "inline-block", marginBottom: "10px" }}
              >
                Payment Information
              </h5>
            </div>
            <div className="attached-invoice-item">
              <Button className="btn-edit-invoice btn">Edit Invoice</Button>
            </div>
          </div>
          <div className="payment-information-grid-item">
            <div className="custom-form-item">
              <Form.Item>
                <span className="purchase-order-form-label">
                  {t("purchases.purchaseDate")}{" "}
                </span>
                <DatePicker
                  style={{ width: "100%" }}
                  placeholder={t("purchases.purchaseDate")}
                />
              </Form.Item>
              {/* form field */}
              <Form.Item>
                <span className="purchase-order-form-label">
                  {t("purchases.purchaseAmount")}{" "}
                </span>
                <Input
                  placeholder={t("purchases.purchaseAmount")}
                />
              </Form.Item>
              {/* form field */}
              <Form.Item
                style={{ width: "100%" }}
              >
                <span className="purchase-order-form-label">
                  {t("purchases.paymentType")}
                </span>

                <Select
                  placeholder={t("purchases.paymentType")}
                  style={{ width: "100%" }}
                  allowClear
                >
                  <Option value="Credit Card">Credit Card</Option>
                  <Option value="PayPal">PayPal</Option>
                  <Option value="Cash">Cash</Option>
                  <Option value="Check">Check</Option>
                  <Option value="ACH">ACH</Option>
                  <Option value="Other">Other</Option>
                </Select>
              </Form.Item>
            </div>
            <div className="custom-form-item">
              <Form.Item validateStatus="success">
                <span className="purchase-order-form-label">
                  {t("purchases.paymentGroup")}{" "}
                </span>
                <Input
                  style={{ width: "100%" }}
                  placeholder={t("purchases.paymentGroup")}
                />
              </Form.Item>
              {/* form field */}
              <Form.Item validateStatus="success">
                <span className="purchase-order-form-label">
                  {t("purchases.cardLast4")}{" "}
                </span>
                <Input
                  placeholder={t("purchases.cardLast4")}
                />
              </Form.Item>
              {/* form field */}
              <Form.Item validateStatus="success">
                <span className="purchase-order-form-label">
                  {t("purchases.listingId")}{" "}
                </span>
                <Input
                  placeholder={t("purchases.listingId")}
                />
              </Form.Item>
            </div>

            <div className="custom-form-item">
              <Form.Item
                validateStatus="success"
                style={{ width: "100%" }}
              >
                <span className="purchase-order-form-label">
                  {t("purchases.salesTax")}{" "}
                </span>
                <Input
                  style={{ width: "100%" }}
                  type="number"
                  placeholder={t("purchases.salesTax")}
                />
              </Form.Item>
              {/* form field */}
              <Form.Item validateStatus="success">
                <span className="purchase-order-form-label">
                  {t("purchases.shipAndHandle")}{" "}
                </span>
                <Input
                  type="number"
                  placeholder={t("purchases.shipAndHandle")}
                />
              </Form.Item>
              {/* form field */}
              <Form.Item validateStatus="success">
                <span className="purchase-order-form-label">
                  {t("purchases.otherFees")}{" "}
                </span>
                <Input
                  type="number"
                  placeholder={t("purchases.otherFees")}
                />
              </Form.Item>
            </div>
          </div>
          <div className="form-footer-item">
            <Button className="cancel-btn-payinfo">Cancel</Button>
            <Button className="submit-btn-payinfo">Submit</Button>
          </div>
        </Form>
      </Modal>
    </>
  );
};

export default PurchaseInfo;
