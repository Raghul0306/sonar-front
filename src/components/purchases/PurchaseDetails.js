import React, { useState, useEffect, useContext, setState } from "react";


import { useTranslation } from "react-i18next";

import UserContext from "../../contexts/user.jsx";
import Button from 'antd/es/button';
import Card from 'antd/es/card';//Select
// style icon
import styled from 'styled-components'
import { Edit } from '@styled-icons/material/Edit'

import "react-datepicker/dist/react-datepicker.css";
import { updatePurchaseOrderDetails, allTicketingSystemAccountsByid } from "../../services/purchaseOrderService";


import Select from 'react-select'

const EditIcon = styled(Edit)`
  color: #aeb1be;
  width:15px;
  height:15px;
`
const { Option } = Select;
const toolTipStyle = {
  fontSize: "18px",
  marginLeft: "5px",
  paddingBottom: "3px",
  color: "rgba(89,89,89, 0.4)"
};

const PurchaseDetails = (props) => {

  const { user } = useContext(UserContext);

  const { t } = useTranslation();
  // modal open close

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [lableInput, setIsField] = useState(false);
  const [purchaseDetailsState, setPurchaseDetails] = useState({});
  const [ticketOptions, setTicketOptions] = useState([]);
  const [selectedTicket, setSelectTicket] = useState([]);
  const [selectedTicketName, setSelectTicketName] = useState([]);
  const [selectedAccount, setAccount] = useState();
  const [selectedAccountId, setAccountId] = useState();
  const [referId, setRefId] = useState([]);
  const [paymentMethod, setPayMethod] = useState();
  const [paymentMethodName, setPayMethodName] = useState();
  const [paymentType, setPayType] = useState();
  const [paymentTypeName, setPayTypeName] = useState();
  const [orderId, setorderId] = useState();
  const [last4Digits, set4Digit] = useState();
  const [ticketAccountOptions, setTicketAccountOptions] = useState([]);
  const [selectedNewAccount, setNewAccount] = useState([]);//dropdown option
  const [selectedNewTicket, setNewSelectTicket] = useState();
  const [newReferenceValue, setReferenceValue] = useState('');

  const [updateJson, setupdateJson] = useState({});
  const [last4DigitsValue, setlast4DigitsValue] = useState();
  const [PaymentDropDown, setPaymentDropDown] = useState([]);
  const [PaymentTypeDropDown, setPaymentTypeDropDown] = useState([]);

  const [paymentMethodNameValue, setpaymentMethodNameValue] = useState([]);
  const [paymentTypeNameValue, setpaymentTypeNameValue] = useState([]);
  const [formValidation, setFormValidation] = useState([]);




  const showModal = async () => {
    // setIsModalVisible(true);
    setIsField(true);
    setReferenceValue(referId);
    await setNewAccount({ value: selectedAccountId, label: selectedAccount });
    updateJson.reference = referId;
    setlast4DigitsValue(last4Digits);
    setpaymentMethodNameValue({ value: paymentMethod, label: paymentMethodName });
    setpaymentTypeNameValue({ value: paymentType, label: paymentTypeName });
    setNewSelectTicket(selectedTicket);
    formValidationCustom.isValid = true
    setFormValidation(formValidationCustom);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const closeInvoice = () => {
    setIsField(false);
  };
  const formValidationCustom = {
    isSubmitted: false,
    isValid: false,
    accountInput: false,
    accountInputMsg: '',
    account4digits: '',

  }

  const saveInvoice = async () => {
    formValidationCustom.isSubmitted = true;
    formValidationCustom.isValid = true;
    if (!selectedNewAccount.value) {
      formValidationCustom.accountInput = true;
      formValidationCustom.isValid = false
      formValidationCustom.accountInputMsg = 'Account Field is required';
    }
    if (!selectedNewTicket.value) {
      formValidationCustom.ticketSysyemInput = true;
      formValidationCustom.isValid = false
      formValidationCustom.ticketInputMsg = 'Ticketing system is required';
    }
    if (!last4DigitsValue || isNaN(last4DigitsValue)) {
      formValidationCustom.account4digits = true;
      formValidationCustom.isValid = false
      formValidationCustom.digitsInputMsg = 'Digits Field is required';
      if (isNaN(last4DigitsValue)) {
        formValidationCustom.digitsInputMsg = 'Numbers only';
      }

    }
    else if (last4DigitsValue.length != 4) {
      formValidationCustom.account4digits = true;
      formValidationCustom.isValid = false
      formValidationCustom.digitsInputMsg = '4 Digits are required';
    }

    if (true == formValidationCustom.isValid) {
      formSubmit();
    }
    setFormValidation(formValidationCustom);
  }
  const formSubmit = async () => {
    setIsField(false);
    await setNewSelectTicket(selectedNewTicket);
    setSelectTicket(selectedNewTicket)
    if (selectedNewTicket && selectedNewTicket.label) {
      setSelectTicketName(selectedNewTicket.label);
    }
    if (updateJson.account && updateJson.account.label) {
      setAccount(updateJson.account.label);
    }
    await setNewAccount(updateJson.account);
    await setReferenceValue(updateJson.reference);
    await setRefId(newReferenceValue);
    await set4Digit(last4DigitsValue);
    await setPayMethodName(paymentMethodNameValue.label);
    await setPayTypeName(paymentTypeNameValue.label);

    let formData = { ticketing_system_id: selectedNewTicket.value, account_id: selectedNewAccount.value, external_reference: newReferenceValue, payment_method_id: paymentMethodNameValue.value, payment_type_id: paymentTypeNameValue.value, payment_last4_digits: last4DigitsValue, id: orderId }
    await updatePurchaseOrderDetails(formData);
    props.onChildUpdate();
  };



  const getAccountId = async (id) => {
    if (id) {
      let tic_sys_accounts = await allTicketingSystemAccountsByid(id);
      let account_options = [];
      if (tic_sys_accounts && tic_sys_accounts.length) {
        for (var ticketSystemAccountLen = 0; ticketSystemAccountLen < tic_sys_accounts.length; ticketSystemAccountLen++) {
          account_options.push({ key: 'tsa_' + ticketSystemAccountLen, value: tic_sys_accounts[ticketSystemAccountLen].id, label: tic_sys_accounts[ticketSystemAccountLen].email })
        }
      }
      setNewAccount()
      await setTicketAccountOptions(account_options);
    }
  }
  const setPurchaseStates = async () => {
    if (purchaseDetails) {

      await setPurchaseDetails(purchaseDetails);
      await setSelectTicketName(purchaseDetails.ticketing_system_name)
      await setAccount(purchaseDetails.ticket_system_acc_name);
      await setAccountId(purchaseDetails.ticket_system_acc_id);
      await setRefId(purchaseDetails.external_reference);
      await setPayMethod(purchaseDetails.payment_method_id);
      await setPayMethodName(purchaseDetails.payment_method_name);
      await setPayType(purchaseDetails.payment_type_id);
      await setPayTypeName(purchaseDetails.payment_type_name);
      await set4Digit(purchaseDetails.payment_last4_digits);
      await getAccountId(purchaseDetails.ticket_system_acc_id);
      await setorderId(purchaseDetails.id);


    }
  }

  const setTicketSystemList = async () => {
    if (ticketSystemList) {
      let options = [];
      let selectedValue = purchaseDetailsState ? { value: purchaseDetailsState.ticket_system_id, label: purchaseDetailsState.ticketing_system_name } : {};

      for (var ticketSystemCount = 0; ticketSystemCount < ticketSystemList.length; ticketSystemCount++) {
        options.push({ value: ticketSystemList[ticketSystemCount].id, label: ticketSystemList[ticketSystemCount].ticketing_system_name })
      }
      setTicketOptions(options);
      setSelectTicket(selectedValue);
      setNewSelectTicket(selectedValue);

    }
  }

  const setpaymentOptionsDirect = async () => {
    let paymentOptions = []
    if (paymentMethodList) {
      for (var paymentMethodCount = 0; paymentMethodCount < paymentMethodList.length; paymentMethodCount++) {
        paymentOptions.push({ value: paymentMethodList[paymentMethodCount].id, label: paymentMethodList[paymentMethodCount].name })
      }
    }
    await setPaymentDropDown(paymentOptions)
  }
  const setpaymentTypeOptionsDirect = async () => {
    let paymentTypeOptions = []
    if (paymentTypeList) {
      for (var payType = 0; payType < paymentTypeList.length; payType++) {
        paymentTypeOptions.push({ value: paymentTypeList[payType].id, label: paymentTypeList[payType].name })
      }
    }
    await setPaymentTypeDropDown(paymentTypeOptions)
  }

  let purchaseDetails = props.purchaseInfo && props.purchaseInfo.length ? props.purchaseInfo[0] : '';
  let ticketSystemList = props.ticketSystem && props.ticketSystem.length ? props.ticketSystem : '';
  let paymentMethodList = props.paymentMethodList && props.paymentMethodList.length ? props.paymentMethodList : '';
  let paymentTypeList = props.paymentTypeList && props.paymentTypeList.length ? props.paymentTypeList : '';
  useEffect(() => {
    setPurchaseStates();
  }, [purchaseDetails]);
  useEffect(() => {
    setTicketSystemList();
  }, [ticketSystemList]);
  useEffect(() => {
    setpaymentOptionsDirect();
  }, [paymentMethodList]);
  useEffect(() => {
    setpaymentTypeOptionsDirect();
  }, [paymentTypeList]);



  const changeTSystem = async (e) => {
    await setNewSelectTicket(e);
    await setNewAccount({})
    let tic_sys_accounts = await allTicketingSystemAccountsByid(e.value);
    let options = [];
    if (tic_sys_accounts && tic_sys_accounts.length) {
      for (var ticketSystemAccountLen = 0; ticketSystemAccountLen < tic_sys_accounts.length; ticketSystemAccountLen++) {
        options.push({ value: tic_sys_accounts[ticketSystemAccountLen].id, label: tic_sys_accounts[ticketSystemAccountLen].email })
      }
    }
    await setTicketAccountOptions(options);

    updateJson.ticketSystem = e;


  }
  const changePayMethod = async (e) => {
    setpaymentMethodNameValue(e)
  }
  const changePayType = async (e) => {
    setpaymentTypeNameValue(e)
  }

  const changeAccSystem = async (e) => {
    setNewAccount(e);
    setAccount(e.label);
    updateJson.account = e;
    if (e) {
      formValidationCustom.accountInput = false;
      setFormValidation(formValidationCustom);
    }

  }
  const changeRefValue = async (e) => {
    setReferenceValue(e.target.value);
  }

  const changeDigitsValue = async (e) => {
    setlast4DigitsValue(e.target.value);
    if (e.target.value != '' && last4DigitsValue.length != 4) {// && formValidation.isSubmitted
      formValidationCustom.account4digits = false;
      setFormValidation(formValidationCustom);
    }
    else {
      formValidationCustom.account4digits = true;
      setFormValidation(formValidationCustom);
    }
  }
  return (
    <>
      <Card
        className={lableInput == true ?"order-detail-channel-card purchase-edit-card":"order-detail-channel-card"}
      >
        <div className="purchase-pay-info">
          <div>
            <h5
              className="purchase-order-form-headings secondary-font-family primary-color p-0"
              style={{ display: "inline-block" }}
            >
              Purchase Details
            </h5>
          </div>
          <div className="attached-invoice-item">
            {!lableInput ?
              <Button className="btn-edit btn" onClick={showModal}><EditIcon /></Button> :
              <span>
                <Button className="btn-save-tn btn" onClick={saveInvoice}>Save</Button>
                <Button className="btn-edit-tn btn" onClick={closeInvoice}>Cancel</Button>
              </span>
            }
          </div>
        </div>
        <div className="purchaser-order-gris purchase-order-detail-section">
          <div className="order-grid-section p-0">
            {lableInput == false ?
              <div className="delivery-status-grid">
                <div className="Left-delivery-grid stylefont-weight-medium"><h6>Ticketing System:</h6></div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>{selectedTicketName}</h6></div>
                <div className="Left-delivery-grid stylefont-weight-medium"><h6>Account:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>{selectedAccount}</h6></div>
                <div className="Left-delivery-grid stylefont-weight-medium"><h6>Reference ID:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>{referId}</h6></div>
                <div className="Left-delivery-grid stylefont-weight-medium"><h6>Payment Method:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>{paymentMethodName}</h6></div>
                <div className="Left-delivery-grid stylefont-weight-medium"><h6>Payment Type:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>{paymentTypeName}</h6></div>
                <div className="Left-delivery-grid stylefont-weight-medium"><h6>Last 4 Digits:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium"><h6>{last4Digits}</h6></div>
              </div> :
              <div className="delivery-status-grid dynamic_edit-detail">
                <div className="Left-delivery-grid stylefont-weight-medium"><h6>Ticketing System:</h6></div>
                <div className="right-delivery-grid stylefont-weight-medium">
                  <Select options={ticketOptions} value={selectedNewTicket} onChange={(e) => changeTSystem(e)} />
                  {formValidation && formValidation.isSubmitted && formValidation.ticketSysyemInput &&
                    <span className="validate-error">{formValidation.ticketInputMsg} </span>
                  }
                </div>
                <div className="Left-delivery-grid stylefont-weight-medium"><h6>Account:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium">
                  <Select options={ticketAccountOptions} value={selectedNewAccount} onChange={(e) => changeAccSystem(e)} />
                  {formValidation && formValidation.isSubmitted && formValidation.accountInput &&
                    <span className="validate-error">{formValidation.accountInputMsg} </span>
                  }
                </div>
                <div className="Left-delivery-grid stylefont-weight-medium"><h6>Reference ID:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium">
                  <div className="edit-input-box">
                    <input type="text" className="form-control" value={newReferenceValue} onChange={(e) => changeRefValue(e)} placeholder="Reference ID" />
                  </div>

                </div>
                <div className="Left-delivery-grid stylefont-weight-medium"><h6>Payment Method:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium">
                  <div className="right-delivery-grid stylefont-weight-medium">
                    <Select options={PaymentDropDown} value={paymentMethodNameValue} onChange={(e) => changePayMethod(e)} />
                  </div>
                </div>
                <div className="Left-delivery-grid stylefont-weight-medium"><h6>Payment Type:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium">
                  <div className="right-delivery-grid stylefont-weight-medium">
                    <Select options={PaymentTypeDropDown} value={paymentTypeNameValue} onChange={(e) => changePayType(e)} />
                  </div>
                </div>
                <div className="Left-delivery-grid stylefont-weight-medium"><h6>Last 4 Digits:</h6> </div>
                <div className="right-delivery-grid stylefont-weight-medium">
                  <div className={formValidation.account4digits ? "edit-input-box is-in-valid" : "edit-input-box"}>
                    <input type="text" className="form-control" value={last4DigitsValue} onChange={(e) => changeDigitsValue(e)} maxLength='4' placeholder="Last 4 digits" />
                  </div>
                  {formValidation && formValidation.isSubmitted && formValidation.account4digits &&
                    <span className="validate-error">{formValidation.digitsInputMsg} </span>
                  }
                </div>
              </div>
            }
          </div>
        </div>
      </Card>
      {/* modal popup */}
    </>
  );
};

export default PurchaseDetails;
