import React, { useEffect, useContext, useState } from "react";
import UserContext from "../../contexts/user.jsx";
import { useTranslation } from "react-i18next";
import { createStripeSettingsURL } from "../../services/stripeService";
import Button from 'antd/es/button';

function ConnectBankButton() {
    const { t } = useTranslation();
    const user = useContext(UserContext);
    // initial state for the stripe URL is a net-new account
    const [stripeURL, setStripeURL] = useState("https://connect.stripe.com/express/oauth/authorize?response_type=code&amp;client_id=ca_GKWbVzboeoDqvXrt0y9kiTZI5PsMeSzK&amp;scope=read_write");

    useEffect(() => {
        /* if the user has not onboarded, don't let them
         * change their settings. instead, they'll get the default
         * value of the stripeURL variable, which is creating a new
         * account and being returned to the dashboard. */
        if (user && user.user && user.user.stripeId && user.user.stripeId !== "skip") {
            async function getStripeSettingsURL(accountID) {
                let urlObject = await createStripeSettingsURL(
                    accountID,
                    // use the base URL as the redirect URL
                    `${window.location.protocol}//${window.location.host}`
                );
            }
            getStripeSettingsURL(user.user.stripeId);
        }
    }, []);

    return (
        <Button
            target="_blank"
            rel="noopener noreferrer"
            href={stripeURL}
            style={{
                display: "block"
            }}
             type="primary" className="btn btn-primary custom-sync-btn continue-btn stylefont-weight-bold primary-bg-color">Connect your Bank
        </Button>
    )
}

export default ConnectBankButton;
