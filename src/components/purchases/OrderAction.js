import React, { useState, useEffect, useContext } from "react";
import Input from 'antd/es/input';
import { useTranslation } from "react-i18next";
import Card from "antd/es/card";
import { Button, Modal } from 'react-bootstrap';
// style icon
import styled from 'styled-components'
import { Envelope } from '@styled-icons/bootstrap/Envelope'
import { Printer } from '@styled-icons/bootstrap/Printer'
import { Attachment } from '@styled-icons/entypo/Attachment'
import { viewInvoiceAttachment } from '../../services/ordersService'
import { invoiceAttachment, updatePoAttachment } from '../../services/purchaseOrderService';
import { fileTypes } from "../../constants/constants";

const EnvelopeIcon = styled(Envelope)`
  color: #fff;
  width:25px;
  height:25px;
`
const PrinterIcon = styled(Printer)`
  color: #fff;
  width:25px;
  height:25px;
  padding-left:10px;
`
const AttachmentIcon = styled(Attachment)`
  color: #2C91EE;
  width:25px;
  height:25px;
  padding-left:10px;
`

const OrderAction = (props) => {

  const [attachmentFileName, setFileName] = useState();
  const [attachmentFile, setFile] = useState();

  const { t } = useTranslation();
  // purchase modal
  const [purchase, setIsOpen] = React.useState(false);

  const showModal = () => {
    setIsOpen(true);
  };

  const hideModal = () => {
    setIsOpen(false);
  };

  // invoice modal
  const [invoice, invoiceIsOpen] = React.useState(false);

  const invoiceshowModal = () => {
    invoiceIsOpen(true);
  };

  const invoicehideModal = () => {
    invoiceIsOpen(false);
  };
  // invoice modal
  const [editInvoice, editInvoiceIsOpen] = React.useState(false);
  const [showConfModal, showConfModalOpen] = React.useState(false);

  const editInvoiceIsShowModal = () => {
    editInvoiceIsOpen(true);
  };

  const editInvoicehideModal = () => {
    editInvoiceIsOpen(false);
    // setFile();
  };

  const editConfShowModal = () => {
    editInvoicehideModal()
    showConfModalOpen(true);
  };

  const editConfhideModal = () => {
    showConfModalOpen(false);
  };

  useEffect(() => {
  }, []);
  async function viewAttachment(orderAttachmentid, fileurl) {
    let attachments = await viewInvoiceAttachment(orderAttachmentid);
    let fileSplit = fileurl.split(".");
    let fileExtension = fileSplit && fileSplit.length ? fileSplit[fileSplit.length - 1] : '';
    if (attachments && attachments.data) {
      let pdfWindow = window.open("");
      pdfWindow.document.write(
        "<iframe width='100%' height='100%' src='data:" + fileTypes[fileExtension] + ";base64, " +
        attachments.data + "'></iframe>"
      )
    }
  }

  async function changeAttachmentForm(e) {
    setFile(e.target.files[0]);
    setFileName(e.target.files[0].name);
  }

  async function updateInvoice(po_id, attachment_id) {

    var invoiceUploadAttachmentData = {
      'purchaseOrderId': po_id,
      'invoiceAttachName': attachmentFileName,
      'invoiceAttachFile': attachmentFile,
    }

    await invoiceAttachment(invoiceUploadAttachmentData).then((res) => {
      updatePoAttachment(attachment_id, po_id, attachmentFileName, attachmentFileName);
      props.onAttachmentUpdate();
      editConfhideModal();
      setFileName(); setFile();
    });

  }

  return (
    <>
      <Card
        className="order-detail-channel-card order-action-items"
      >
        <div className="purchase-pay-info">
          <h5
            className="purchase-order-form-headings secondary-font-family p-0 order-action-title"
            style={{ display: "inline-block" }}
          >
            PO Actions:
          </h5>
        </div>
        <div className="order-action-sec">
          <div className="top-button-item">
            <button className="view-order-btn primary-bg-color stylefont-weight-medium" onClick={showModal}>View Purchase Order</button>
            <button className="view-order-btn view-invoice-btn primary-bg-color stylefont-weight-medium" onClick={invoiceshowModal}>View invoice</button>
            <button className="view-order-btn edit-invoice-btn primary-bg-color stylefont-weight-medium" onClick={editInvoiceIsShowModal}>Edit invoice</button>
          </div>
        </div>
      </Card>



      {/* purchase order modal popup */}
      <Modal show={purchase} onHide={hideModal} className="purchase-order-modal" aria-labelledby="contained-modal-title-vcenter" centered backdrop="static">
        <Modal.Header closeButton>
          <h5 className="substitute-txt">Purchase Order</h5>
        </Modal.Header>
        <Modal.Body>
          <div className="receipt-item">
            <h5 className="receipt-title">Email Invoice</h5>
            <p className="receipt-txt">Generate an email invoice of this order.</p>
            <Button className="receipt-btn"><EnvelopeIcon /> Generate Email</Button>
          </div>
          <div className="receipt-item">
            <h5 className="receipt-title">Print Invoice</h5>
            <p className="receipt-txt">Download a PDF version of this invoice.</p>
            <Button className="receipt-btn"><PrinterIcon /> Print Invoice</Button>
          </div>
        </Modal.Body>
      </Modal>
      {/* invoice order modal popup */}
      <Modal show={invoice} onHide={invoicehideModal} className="purchase-order-modal download-modal-popup" aria-labelledby="contained-modal-title-vcenter" centered backdrop="static">
        <Modal.Header closeButton>
          <h5 className="substitute-txt">Invoice</h5>
        </Modal.Header>
        <Modal.Body>
          <div className="receipt-item">
            <h5 className="receipt-title">Email Invoice</h5>
            <p className="receipt-txt">Generate an email invoice of this order.</p>
            <Button className="receipt-btn"><EnvelopeIcon /> Generate Email</Button>
          </div>
          <div className="receipt-item">
            <h5 className="receipt-title">Print Invoice</h5>
            <p className="receipt-txt">Download a PDF version of this invoice.</p>
            {/* <Button className="receipt-btn" ><PrinterIcon /> Print Receipt</Button> */}
            {(props.orderInfo && props.orderInfo[0] && props.orderInfo[0].attachment_id) ?
              <Button onClick={(e) => viewAttachment(props.orderInfo[0].attachment_id, props.orderInfo[0].attachment_url)} className="receipt-btn"><PrinterIcon />Print Invoice</Button> : <Button className="receipt-btn" disabled={true} title={'No attachments'}><PrinterIcon /> Print Invoice</Button>
            }
          </div>
        </Modal.Body>
      </Modal>
      {/* edit invoice order modal popup */}
      <Modal show={editInvoice} onHide={editInvoicehideModal} className="invoice-edit-modal" aria-labelledby="contained-modal-title-vcenter" centered backdrop="static">
        <Modal.Header closeButton>
          <h5 className="substitute-txt">Edit Invoice</h5>
        </Modal.Header>
        <Modal.Body>
          <div className="edit-invoice-sec">
            <h5 className="edit-gr-txt">Attached Invoice:</h5>
            <div className="edit-invoice-grid-sec">
              <div className="attached-sec-gr">
                <div className="attach-btn"><AttachmentIcon /> {(attachmentFileName == null && props.orderInfo && props.orderInfo[0] && props.orderInfo[0].attachment_name) ? props.orderInfo[0].attachment_name : attachmentFileName}</div>
              </div>
              <div className="view--replace-sec">
                {(props.orderInfo && props.orderInfo[0] && props.orderInfo[0].attachment_id) ?
                  <Button onClick={(e) => viewAttachment(props.orderInfo[0].attachment_id, props.orderInfo[0].attachment_url)} className="invoice-view--btn">View</Button> : ''
                }
              </div>
              <div className="replace-sec_gr ">
                <div className="replace--item-kjk">
                  <Input type="file" onChange={(e) => changeAttachmentForm(e)} />
                </div>
                <Button className="invoice-replace--btn replace-order-btn">Replace</Button>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer className="invoice-modal-footer">
          <Button className="edit_update-btn" disabled={attachmentFile ? false : true} onClick={editConfShowModal}>Update Invoice</Button>
        </Modal.Footer>
      </Modal>

      {/* Last confirmation-start*/}
      <Modal className="price-update-modal price-confirmation--modal" show={showConfModal} onHide={editConfhideModal} backdrop="static">
        <Modal.Header closeButton>
          <Modal.Title>Are you sure you want to update this invoice</Modal.Title>
        </Modal.Header>
        <Modal.Footer className="modal-btn-align">
          <Button variant="secondary" onClick={editConfhideModal}>
            Cancel
          </Button>
          <Button variant="primary" onClick={(e) => updateInvoice(props.orderInfo[0].id, props.orderInfo[0].attachment_id)}>
            Submit
          </Button>
        </Modal.Footer>
      </Modal>

    </>
  );
};

export default OrderAction;
