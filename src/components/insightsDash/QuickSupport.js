import React, { useState, useContext } from "react";
import PropTypes from "prop-types";
import UserContext from "../../contexts/user.jsx";
import {
  Col,
  Form,
  FormGroup,
  FormInput,
  FormTextarea,
  Button
} from "shards-react";
import { Translation, useTranslation } from "react-i18next";
import Card from "antd/es/card";
import { sendSupportMail } from "../../services/usersService";
import { Modal } from 'react-bootstrap';
import { responseStatus } from "../../constants/constants";

const QuickSupport = () => {
  const { user } = useContext(UserContext);
  const { t } = useTranslation();
  const [subject, setSubject] = useState("");
  const [issueDescription, setIssueDescription] = useState("");
  const [isSubmit, setMailSubmit] = useState(false);
  const [successModal, setSuccessModal] = useState(false);
  const [successModalMessage, setSuccessModalMessage] = useState('');

  const submitSupportTicket = async (e) => {
    e.preventDefault();
    if (subject === "" || issueDescription === "") {
      setMailSubmit(true);
      return;
    }
    let response = await sendSupportMail(subject, issueDescription, user.userRole);
    if (response && response.status == responseStatus.success) {
      setSuccessModalMessage("Mail has been sent successfully");
    }
    else {
      setSuccessModalMessage("Mail sending failed! Try again later");
    }
    setMailSubmit(false)
    setSuccessModal(true)
    setSubject("");
    setIssueDescription("");
  };
  return (
    <>
      <Col lg="12" md="12" sm="12" className="mb-4" style={{ height: "100%" }}>
        <h3 className="card-heading">
          <Translation>{t => t("support.submitTicket")}</Translation>
        </h3>
        <Card
          style={{
            borderRadius: "10px",
            height: "90%"
          }}
          bodyStyle={{ padding: "30px 10px 10px 10px" }}
          bordered={false}
        >
          <Form className="quick-post-form">
            {/* Title */}
            <div className="input-form">
              <label className="input-form-label">Subject Line</label>
              <FormInput
                style={{ backgroundColor: "#f5f6f8", border: "none" }}
                value={subject}
                onChange={e => setSubject(e.target.value)}
              />
              {!subject && isSubmit &&
                <span style={{ color: "#dc3545" }}>Subject is required </span>
              }
            </div>

            {/* Body */}
            <div className="input-form">
              <label className="input-form-label">Describe your issue.</label>
              <FormTextarea
                style={{
                  minHeight: "125px",
                  backgroundColor: "#f5f6f8",
                  border: "none"
                }}
                value={issueDescription}
                onChange={e => setIssueDescription(e.target.value)}
              />
              {!issueDescription && isSubmit &&
                <span style={{ color: "#dc3545" }}> Description is required </span>
              }
            </div>

            {/* Create Draft */}
            <FormGroup style={{ padding: "15px 10px" }} className="mb-0">
              <Button
                style={{ width: "100%" }}
                theme="primary"
                type="submit"
                //   disabled={issueDescription && subject ? false : true}
                onClick={submitSupportTicket}
              >
                <Translation>{t => t("support.submitTicket")}</Translation>
              </Button>
            </FormGroup>
          </Form>
        </Card>
      </Col >
      <Modal className="price-confirmation--modal" show={successModal} onHide={(e) => setSuccessModal(false)} backdrop="static">
        <Modal.Header closeButton>
        </Modal.Header>
        <Modal.Body  >
          <div className="update-price-item">
            <h3>{successModalMessage}</h3>
          </div>
        </Modal.Body>

        <Modal.Footer className="modal-btn-align">
          <Button variant="secondary" onClick={(e) => setSuccessModal(false)}>
            Close
          </Button>

        </Modal.Footer>
      </Modal>
    </>
  );
};

QuickSupport.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

QuickSupport.defaultProps = {
  title: `${(<Translation>{t => t("support.submitTicket")}</Translation>)}`
};

export default QuickSupport;
