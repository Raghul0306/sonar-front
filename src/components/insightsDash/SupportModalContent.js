import React, { useState, useEffect } from "react";
import Button from 'antd/es/button';
import Input from 'antd/es/input';
import Form from 'antd/es/form';
import Zendesk from "react-zendesk";
import { useTranslation } from "react-i18next";
import { sendEmailToUser } from '../../services/userService'


const { TextArea } = Input;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

const SupportModalContent = props => {
  const ZENDESK_KEY = "91a24802-b58b-494a-b92e-44825702ca9f";
  const setting = {
    color: {
      theme: "#000"
    },
    launcher: {
      chatLabel: {
        "en-US": "Need Help"
      }
    },
    webWidget: { chat: { connectOnPageLoad: true } },
    contactForm: {
      fields: [
        { id: "description", prefill: { "*": "My pre-filled description" } }
      ]
    }
  };
  /* Similar to ComponentWillMount */
  useEffect(() => {
    const formLoadValidation = async () => {
      // To disable submit button at the beginning.
      props.form.validateFields();
    };
    formLoadValidation();
  }, []);
  // handle form submission
  const submitForm = async (e) => {
    // step 1: get the values from the VALID form
    e.preventDefault();
    const fieldValues = props.form.getFieldsValue();
    console.info(fieldValues);
    const emailHtml = `<h1>Contact support</h1><h1>User email: ${fieldValues.email}</h1>`
    sendEmailToUser("support@myticketspos.com", emailHtml, comments)
    // alert(t('support.emailSent'))
    await props.handleCancel()
    props.handleSuccess()
  };
  const { t } = useTranslation();
  // variables for managing submission/error state for the form
  const {
    getFieldDecorator,
    getFieldsError,
    getFieldError,
    isFieldTouched
  } = props.form;
  const emailError = isFieldTouched("email") && getFieldError("email");
  const commentsError = isFieldTouched("comments") && getFieldError("comments");
  // eslint-disable-next-line
  const [email, setEmail] = useState(props.userEmail);
  // eslint-disable-next-line
  const [comments, setComments] = useState(0);

  return (
    <>
      {/* <div className="modal-floating-cancel-button">
        <Button shape="circle" size="large" onClick={props.handleCancel}>
          <CloseRoundedIcon
            style={{
              fontSize: "23px"
            }}
          />
        </Button>
      </div> */}
      {/* <p>{t('support.helpdeskIntro')} <a href="https://phunnel.zendesk.com" target="__blank">{t('support.helpdeskLink')}</a>.</p> */}
      <div className="popup-body m-0">
        <Zendesk
          zendeskKey={ZENDESK_KEY}
          {...setting}
          onLoaded={() => console.log("is loaded")}
        />
        {props && props.orderId && (<div style={{ marginTop: '20px' }}>
          <div style={{ margin: "0 !important", fontSize: '20px' }}>
            {`${t("orders.id")}: ${props.orderId}`}
          </div>
        </div>)}
        <Form onSubmit={submitForm} name="support">
          <Form.Item
            validateStatus={emailError ? "error" : ""}
            help={emailError || ""}
          >
            <span className="purchase-order-form-label">
              {t("support.email")}
            </span>
            {getFieldDecorator("email", {
              initialValue: email,
              rules: [{ required: true, type: "email", message: t("support.errors.email") }]
            })(
              <Input
                onChange={e => {
                  setEmail(e.target.value);
                }}
              />
            )}
          </Form.Item>
          <Form.Item
            validateStatus={commentsError ? "error" : ""}
            help={commentsError || ""}
          >
            <span className="purchase-order-form-label">
              {t("support.message")}
            </span>
            {getFieldDecorator("comments", {
              rules: [{ required: true, message: t("support.errors.comments") }]
            })(
              <TextArea
                onChange={e => {
                  setComments(e.target.value);
                }}
              />
            )}
          </Form.Item>

          <Form.Item validateStatus="success">
            <div
              style={{
                textAlign: "center",
                marginTop: "10px"
              }}
            >
              <Button
                style={{ marginTop: "15px" }}
                type="primary"
                htmlType="submit"
                className="contact-support-btn primary-bg-color stylefont-weight-bold"
                disabled={hasErrors(getFieldsError())}
              >
                {t("support.contact")}
              </Button>
            </div>
          </Form.Item>
        </Form>
      </div>
    </>
  );
};

export const SupportModalForm = Form.create({ name: "support" })(
  SupportModalContent
);
