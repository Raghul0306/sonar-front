import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row } from "shards-react";
//import UserContext from "../contexts/user.jsx";
//import { Redirect } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";

const PageTitle = loadable(() => import("../components/common/PageTitle"));


function SellerAgreement() {
  const { t } = useTranslation();
  //const user = useContext(UserContext);
  const [loading, setLoading] = useState(true);


  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      <Row noGutters className="page-header">
        <PageTitle
          sm="4"
          title={"Seller Agreement"}
          subtitle={"Test"}
          className="text-sm-left col-sm-12"
        />
      </Row>
      <div className="privacy-policy-section">
        <div class="common-doc-page section document-container">
          <h3 className="page-title-item stylefont-weight-bold">Seller Agreement</h3>
          <h5 className="page-subtitle-item stylefont-weight-bold">Revised: October 1, 2021</h5>
          <p className="doc-page-para stylefont-weight-regular">By completing the registration process either online or via email (the &ldquo; Registration Process &rdquo;) or by selling any Tickets on the MyTicketsPOS platform (defined below), you, on behalf of yourself and your company (&ldquo; Seller &rdquo;), agree to be bound by the terms of this Seller Agreement (together with all online terms described herein including the Handbook, the (&ldquo; Agreement &rdquo;) with MyTicketsPOS, (&ldquo; MyTicketsPOS &rdquo;) (each a &ldquo; Party &rdquo; and collectively &ldquo; the Parties &rdquo;).  MyTicketsPOS will provide to Seller the services under the terms of this Agreement.</p>
          <p className="doc-page-para stylefont-weight-regular">Seller acknowledges and agrees that this Agreement replaces any prior agreements or understandings between the Parties as of October 1, 2021.</p>
          <p className="doc-page-para stylefont-weight-regular"><strong>Definitions.</strong></p>
          <ul className="page-list p-0 pl-3">
            <li><p className="doc-page-para stylefont-weight-regular">&ldquo; Effective Date &rdquo; shall mean the date that Seller commences use of the MyTicketsPOS platforms. </p></li>
            <li><p className="doc-page-para stylefont-weight-regular">&ldquo; Handbook &rdquo; shall mean the additional terms and conditions and best practices set forth at <a href="www.MyTicketsPOS.com/terms-and-conditions" target="_blank">www.MyTicketsPOS.com/terms-and-conditions</a>, which may be updated by MyTicketsPOS from time to time.</p></li>
            <li><p className="doc-page-para stylefont-weight-regular">&ldquo; Sell Program &rdquo; shall mean the MyTicketsPOS program that allows Sellers to post listings of Tickets to sell on the Phunnel Digital Properties and its partner&rsquo;s properties.</p></li>
            <li><p className="doc-page-para stylefont-weight-regular">&ldquo; Phunnel Digital Properties &rdquo; shall mean the websites and mobile applications that contain Phunnel&rsquo;s marketplace of tickets for purchase and sale such as <a href="www.yadara.com" target="_blank">www.yadara.com.</a></p></li>
            <li><p className="doc-page-para stylefont-weight-regular">Setup Requirements: Prior to listing any Tickets on MyTicketsPOS or otherwise participating in the MyTicketsPOS Program, Seller must provide the following, to the satisfaction of MyTicketsPOS, in its sole discretion:</p></li>
            <div className="inner-listing-item">                        
              <ul className="page-list">
                  <li><p className="doc-page-para stylefont-weight-regular">Credit Card to charge penalties and authorize charges</p></li>
                  <li><p className="doc-page-para stylefont-weight-regular">An executed MyTicketsPOS ACH Payment Authorization form</p></li>
                  <li><p className="doc-page-para stylefont-weight-regular">Tax information and W-9</p></li>
                  <li><p className="doc-page-para stylefont-weight-regular">Phone number</p></li>
                  <li><p className="doc-page-para stylefont-weight-regular">Adequate general liability and other insurance with such coverage and in the amounts generally carried by ticket sellers</p></li>
              </ul>
            </div>
            <li><p className="doc-page-para stylefont-weight-regular">Handbook: Prior to listing any tickets on MyTicketsPOS Digital Properties or otherwise participating in the MyTicketsPOS Program, Seller shall read the Handbook in its entirety, and by listing tickets on a MyTicketsPOS Digital Property, Seller represents and warrants that it has read the Handbook and shall, at all times during its participation in the MyTicketsPOS Program, be in full compliance with the Handbook.</p></li>
            <li><p className="doc-page-para stylefont-weight-regular">Pricing:  Seller shall comply with all pricing requirements set forth in the Handbook, including, but not limited to, canceling an order at one price and relisting the same tickets for a higher price (whether on a Phunnel Digital Property or elsewhere) due to the increased popularity of an event (i.e., &ldquo; seller remorse &rdquo;). Such action will result in one or more penalties (as further described in the Handbook), up to and including termination from the MyTicketsPOS Program and termination of this Agreement.</p></li>
            <li><p className="doc-page-para stylefont-weight-regular">Seller Fees:  MyTicketsPOS may charge service fees for selling tickets utilizing the MyTicketsPOS platform, as well as delivery or fulfillment fees. Given the variability of event types, service fees may be adjusted based on geography, the volume of Tickets listed, or other factors. MyTicketsPOS may in its sole discretion change its Service Fees at any time.</p></li>
            <li><p className="doc-page-para stylefont-weight-regular">Penalties:  MyTicketsPOS reserves the right to charge the Seller fees for and penalties for any of Seller&rsquo;s acts or omissions in violation of this Agreement. Seller agrees to pay all such fees and penalties that are incurred. The fees and penalties may be offset from future payments owed to Seller, be charged to Seller&rsquo;s credit card on file, or via an ACH transfer. Seller is required to keep a valid credit/debit Ticket on file and an executed MyTicketsPOS ACH Payment Authorization form with MyTicketsPOS at all times.</p></li>
            <li><p className="doc-page-para stylefont-weight-regular">Buyer&rsquo;s Personal Information:  In order to satisfy a particular fulfillment method, Seller may be provided with a Buyer&rsquo;s personally identifiable information, such as phone number, email address, or address.  Under no circumstances may Seller use such information for any purpose other than fulfilling the ticket that the Buyer has purchased.  Violation of this policy may result in immediate termination of this Agreement and imposition of penalty fees.</p></li>
            <li><p className="doc-page-para stylefont-weight-regular">Binding Contract: Seller should be aware that making an offer of a certain ticket and a buyer accepting that ticket (which includes the price) in exchange for payment constitutes a binding contract on Seller. Not honoring a binding contract has legal effect and Seller should take all steps necessary to ensure that it can honor every binding contract it makes through the MyTicketsPOS platform. Breach of such contract can subject Seller to legal remedies outside of those available to MyTicketsPOS under this Agreement.</p></li>
            <li><p className="doc-page-para stylefont-weight-regular">Compliance With Laws:  Seller shall at all times comply with all applicable laws, including, but not limited to all local and state laws of each applicable state in which the seller does business, which may include laws prohibiting the use of bots, and the obligation to obtain licenses. Seller is solely responsible for knowledge of all laws related to its selling activities. Under no circumstances may Seller sell any Ticket on any Phunnel Digital Property that the Seller does not have in-hand or otherwise proof of purchase at the time of listing.</p></li>
            <li><p className="doc-page-para stylefont-weight-regular">License to Use Listings:  To the extent that Seller lists on any MyTicketsPOS Digital Property, by participating in the MyTicketsPOS Program, Seller hereby grants for all listings all necessary rights and licenses to the listings to MyTicketsPOS.</p></li>
            <li><p className="doc-page-para stylefont-weight-regular">Payment: An order becomes eligible for payment upon the conclusion of the event for which the ticket was sold. Payments occur frequently as per the Seller Agreement via ACH transfer (domestic) or International ACH Transfer (IAT - International) for sales completed between the previous Sunday and six days trailing. For example, if an item sells on Friday, payment for such an order will be made the following Thursday. MyTicketsPOS reserves the right to modify the payment day and schedule.</p></li>
            <li><p className="doc-page-para stylefont-weight-regular">Term/Termination:  This Agreement will be effective as of the Effective Date and shall continue until terminated in writing for any or no reason by either MyTicketsPOS or Seller.  Upon termination, Seller shall fulfill all outstanding orders that were received prior to termination.</p></li>
            <li><p className="doc-page-para stylefont-weight-regular">Entire Agreement: This Agreement represents the complete agreement between the Parties regarding the subject matter herein, and supersedes all prior oral and/or written communications, negotiations, representations, or agreements in relation to that subject matter made or entered into before the date of this Agreement. In the event of any conflict between the terms of the Handbook and the terms of this Agreement, the terms of the Handbook shall apply. This Agreement may be changed by MyTicketsPOS at any time and by continuing to participate in the MyTicketsPOS Program, Seller agrees to be bound by any modifications to the Agreement. This Agreement is governed by the laws of the state of Texas. Any disputes, actions, claims or causes of action arising out of or in connection with this Agreement (&ldquo; Dispute &rdquo;) shall be submitted to and finally settled by arbitration in Travis County, Texas for any arbitration, using the English language in accordance with the Arbitration Rules and Procedures of the Judicial Arbitration and Mediation Services, Inc. (JAMS) then in effect, by one or more commercial arbitrator(s) with substantial experience in resolving complex commercial contract disputes. Judgment upon the award so rendered may be entered in a court having MyTicketsPOS or application may be made to such court for judicial acceptance of any award and an order of enforcement, as the case may be. Notwithstanding the foregoing, each party shall have the right to institute an action in any court of proper jurisdiction for injunctive relief, and any such action for injunctive relief shall be subject to the exclusive judication of the state and federal courts located in Travis County, Texas (and the parties hereby consent to judication and venue in the U.S. federal courts located in the Travis County, Texas).</p></li>
            </ul>
          </div>
        </div>
      {/* Default Light Table */}
    </Container>
  ) : (
    <Loader />
  );
}

export default SellerAgreement;
