import axios from "axios";

export async function getInventoryReport(data) {
    return axios.post(process.env.REACT_APP_GRAPHQL_URL, {
        query: `query ($inventoryReport: inventoryReportInput) {
            getInventoryReport(input: $inventoryReport) {
                inventoryReportData,
                headers
            }
        }`,
        variables: {
            inventoryReport: data
        }
            
    });
}

export async function getPurchaseReport(data) {
    return axios.post(process.env.REACT_APP_GRAPHQL_URL, {
        query: `query ($purchaseReport: purchaseReportInput) {
            getPurchaseReport(input: $purchaseReport) {
                purchaseReportData,
                headers
            }
        }`,
        variables: {
            purchaseReport: data
        }
            
    });
}

export async function getSalesReport(data) {
    return axios.post(process.env.REACT_APP_GRAPHQL_URL, {
        query: `query ($salesReport: salesReportInput) {
            getSalesReport(input: $salesReport) {
                salesReportData,
                headers
            }
        }`,
        variables: {
            salesReport: data
        }
            
    });
}
