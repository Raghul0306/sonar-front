import axios from "axios";

export async function getCardsReportData() {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          {
            allCards{
              nodes{
                name
                id
                setBySet {
                  year
                  name
                  brandByBrandId {
                    name
                  }
                }
                performerByPerformer{
                  name
                  id
                }
                categoryByCategory{
                  name
                  id
                }
                listingsByCardId{
                  nodes{
                    userBySellerId{
                      fullName
                    }
                    cost
                    sold
                    price
                  }
                    totalCount
                  }
              }
            }
          }
               `,
      variables: {}
    });
    return tokenResponse.data.data.allCards.nodes;
  } catch (error) {
    console.log("Could not find cards");
  }
}

/**
 * Originally written to populate a dropdown list on
 * in the Add Listings modal for the Collectibles
 * Admin Panel. Will return all card categories.
 */
export async function getAllCategories() {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        allCategories{
          nodes{
            name
            id
          }
        }
      }
      `
    });
    // return tokenResponse.data.data.allCategories.nodes;
    let noSneakerCategories = tokenResponse.data.data.allCategories.nodes.filter(
      category => category.id !== 3
    );
    return noSneakerCategories;
  } catch (error) {
    console.log("Could not find categories");
  }
}

/**
 * Used to populate front end inputs which the user interacts
 * with while searching for cards when adding a listing.
 */
export async function getBrandsByCategory(categoryID) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        allBrands(condition:{categoryId:${categoryID}}){
          nodes{
            name
            id
          }
        }
      }
      `
    });
    return tokenResponse.data.data.allBrands.nodes;
  } catch (error) {
    console.log("Could not find brands");
  }
}

export async function getAllBrands() {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        allBrands {
          nodes{
            name
            id
          }
        }
      }
      `
    });
    return tokenResponse.data.data.allBrands.nodes;
  } catch (error) {
    console.log("Could not find all brands");
  }
}

export async function getSetsByBrand(brandId) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        allSets(condition:{brandId:${brandId}}){
          nodes{
            id
            name
            year
          }
        }
      }
      `
    });
    return tokenResponse.data.data.allSets.nodes;
  } catch (error) {
    console.log("Could not find sets");
  }
}

export async function getBrandById(brandId) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        brandById(id:${brandId}) {
          name
          id
        }
      }
      `
    });
    return tokenResponse.data.data.brandById;
  } catch (error) {
    console.log("Could not find brand by ID");
  }
}

export async function getSetById(setId) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        setById(id:${setId}) {
          name
          id
          year
        }
      }
      `
    });
    return tokenResponse.data.data.setById;
  } catch (error) {
    console.log("Could not find set by ID");
  }
}

/**
 * Used to populate front end inputs which the user interacts
 * with while searching for cards when adding a listing.
 */
export async function getSetsByBrandAndCategory(brandId, categoryId) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        allSets(condition:{brandId:${brandId}, categoryId:${categoryId}}){
          nodes{
            id
            name
            year
          }
        }
      }
      `
    });
    return tokenResponse.data.data.allSets.nodes;
  } catch (error) {
    console.log("Could not find sets");
  }
}

/**
 * Used to populate front end inputs which the user interacts
 * with while searching for cards when adding a listing.
 */
export async function getCardsBySet(setId) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        setById(id:${setId}){
          cardsBySet{
            nodes{
              id
              cardNumber
              categoryByCategory {
                name
              }
              organization
              performer
              brandByBrand{
                name
                id
              }
              setBySet{
                name
                id
                year
                description
              }
            }
          }
        }
      }`
    });
    return tokenResponse.data.data.setById.cardsBySet.nodes;
  } catch (error) {
    console.log("Could not find cards");
  }
}

export async function getCardsBySetAndNumber(setId, cardNumber) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        setById(id:${setId}){
          cardsBySet(condition:{cardNumber:"${cardNumber}"}){
            nodes{
              id
              cardNumber
              categoryByCategory {
                name
              }
              organization
              performer
              brandByBrand{
                name
                id
              }
              setBySet{
                name
                id
                year
                description
              }
            }
          }
        }
      }`
    });
    return tokenResponse.data.data.setById.cardsBySet.nodes;
  } catch (error) {
    console.log("Could not find cards by set and card number");
  }
}

/**
 * Used to populate front end inputs which the user interacts
 * with while searching for cards when adding a listing.
 */
export async function getCardsBySetsAndPerformer(setIds, performerId) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        searchCardsBySetsAndPerformer(_setIds:[${setIds}],_performerId:${performerId}){
          nodes{
            id
            cardNumber
            categoryByCategory {
              name
            }
            organization
            performer
            brandByBrand{
              name
              id
            }
            setBySet{
              name
              id
              year
              description
            }
          }
        }
      }`
    });
    return tokenResponse.data.data.searchCardsBySetsAndPerformer.nodes;
  } catch (error) {
    console.log("Could not find cards by performer and sets");
  }
}

/**
 * Used for the Collectibles Admin Panel front end,
 * when a user is searching for a card. Can optionally
 * only retrieve a certain number of cards.
 * @param {Number} performerId
 */
export async function getCardsByPerformer(performerId) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
          searchCardsByPerformer(_performerId:${performerId}) {
            nodes {
              id
              cardNumber
              categoryByCategory {
                name
                id
              }
              performer
              brandByBrand {
                id
                name
              }
              setBySet {
                name
                id
                year
                description
              }
            }
          }
        }`
    });
    return tokenResponse.data.data.searchCardsByPerformer.nodes;
  } catch (error) {
    console.log("Could not find cards");
  }
}

/**
 * Used for populating search result in the
 * Admin Panel front end. Will be a refined search
 * so should fire relatively quickly.
 * @param {Number} performerId
 */
export async function getCardsByPerformerAndSet(performerId, setId) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
            allCards(condition: {performer: ${performerId}, set: ${setId}}) {
              nodes {
                id
                cardNumber
                categoryByCategory{
                  name
                  id
                }
                performerByPerformer{
                  name
                  id
                }
                organizationByOrganization{
                  name
                  id
                }
                brandByBrand {
                  id
                  name
                }
                setBySet {
                  name
                  id
                  year
                  description
                }
              }
            }
          }
        `
    });
    return tokenResponse.data.data.allCards.nodes;
  } catch (error) {
    console.log("Could not find cards by performer and set.");
  }
}

/**
 * Used for populating search result in the
 * Admin Panel front end. Will be a refined search
 * so should fire relatively quickly.
 */
export async function getSetsByBrandCategoryAndYear(brandId, category, year) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
            allSets(condition: {brandId: ${brandId}, categoryId: ${category}, year: ${year}}) {
              nodes {
                name
                id
                year
              }
            }
          }
        `
    });
    return tokenResponse.data.data.allSets.nodes;
  } catch (error) {
    console.log("Could not find cards by performer and set.");
  }
}

/**
 * Used for populating search result in the
 * Admin Panel front end. Will be a refined search
 * so should fire relatively quickly.
 * @param {Number} performerId
 */
export async function getYearsByBrandAndCategory(brandId, category) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
            returnYearsByBrandAndCategory(_category: ${category}, _brand: ${brandId}){
              nodes{
                year
                id
                name
              }
            }
          }
        `
    });
    return tokenResponse.data.data.returnYearsByBrandAndCategory.nodes;
  } catch (error) {
    console.log("Could not find sets by brand and category.");
  }
}

/**
 * Will return a list of performers when given a performer ID.
 * @param {Number} performerId
 */
export async function getPerformerById(performerId) {
  let response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
    query: `{
      performerById(id:${performerId}){
        id
        name
      }
    }`
  });
  if (!response.hasError) {
    let performer = await response.data.data.performerById;
    return performer;
  }
}

/**
 * Will return an organization when given an organization ID.
 * @param {Number} organizationId
 */
export async function getOrganizationById(organizationId) {
  let response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
    query: `{
      organizationById(id:${organizationId}){
        id
        name
      }
    }`
  });
  if (!response.hasError) {
    let organization = await response.data.data.organizationById;
    return organization;
  }
}
