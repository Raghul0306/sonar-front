import axios from "axios";

export async function getAuthToken(code) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
            createStripeOAuthToken(code: "${code}") {
              stripe_user_id
            }
        }
          
        `
    });
    return response.data.data.createStripeOAuthToken;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function createHobbyShopStripeCheckoutSession(listing){
  let response = await axios.post(process.env.REACT_APP_GRAPHQL_URL,
    {
      query: `{
        CreateHobbyShopStripeCheckoutSession(listing: ${listing}) {
          id
          payment_intent
        }
      }`
    }
  )
  if (!response.hasError){
    let data = response.data.data.CreateHobbyShopStripeCheckoutSession;
    return data
  }
}

export async function getStripeCheckoutSessionById(checkoutSessionId) {
  let response = await axios.post(process.env.REACT_APP_GRAPHQL_URL,
  {
    query: `{
      GetStripeCheckoutSessionById(checkoutSessionId:"${checkoutSessionId}"){
        id
        amount_total
        metadata {
          listingID
        }
      }
    }`
  })
  if (!response.hasError){
    let data = response.data.data.GetStripeCheckoutSessionById;
    return data
  }
}

export async function createStripeSettingsURL(accountID, redirect_url) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          CreateStripeLoginLink(account_id: "${accountID}", redirect_url:"${redirect_url}") {
            url
          }
        }     
        `
    });
    return response.data.data.CreateStripeLoginLink;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function listStripePayouts(arrival_date, limit) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          listStripePayoutByArrivalDate(arrivalDate:"${arrival_date}" ){
            amount
            arrivalDate
            balanceTransaction
            created
            id
          }
        }
        
        `
    });
    return response.data.data.listStripePayouts;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function listStripeBrokerPayouts(arrival_date, limit, account_id) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          listStripeBrokerPayouts(arrival_date:"${arrival_date}", limit: ${limit}, account_id:"${account_id}"){
            amount
            arrival_date
            balance_transaction
            created
            id
          }
        }
        
        `
    });
    return response.data.data.listStripeBrokerPayouts;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function listFutureStripePayouts(arrival_date, limit) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          listFutureStripePayouts(arrival_date:${arrival_date}, limit: ${limit}){
            amount
            arrival_date
            balance_transaction
          }
        }
        
        `
    });
    return response.data.data.listFutureStripePayouts;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function listFutureStripeBrokerPayouts(
  arrival_date,
  limit,
  account_id
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          listFutureStripeBrokerPayouts(arrival_date:${arrival_date}, limit: ${limit}, account_id:${account_id}){
            amount
            arrival_date
            balance_transaction
          }
        }
        
        `
    });
    return response.data.data.listFutureStripeBrokerPayouts;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getStripeChargesForBroker(
  starting_after,
  ending_before,
  stripe_broker_id
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          GetStripeCharges(starting_after:${starting_after},ending_before:${ending_before}, stripe_broker_id:${stripe_broker_id}){
            id
            amount
          }
        }
        
        `
    });
    return response.data.data.GetStripeCharges;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getStripeConnectedChargesForAdmin(
  starting_after,
  ending_before
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          GetStripeConnectedCharges( starting_after:${starting_after},ending_before:${ending_before}){
            id
            amount
          }
        }
        
        `
    });
    return response.data.data.GetStripeConnectedCharges;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getStripeNetVolume(
  starting_after,
  ending_before,
  stripe_broker_id = null
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          GetStripeNetVolume(starting_after:${starting_after},ending_before:${ending_before}, stripe_broker_id:${stripe_broker_id})
        }
        `
    });
    return response.data.data.GetStripeNetVolume;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getStripeConnectedFeesForAdmin(
  starting_after,
  ending_before
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          GetStripeConnectedFees(starting_after:${starting_after},ending_before:${ending_before}){
            id
            amount
          }
        }
        
        `
    });
    return response.data.data.GetStripeConnectedFees;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function numberOfNewCustomersForAdmin(
  starting_after,
  ending_before
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          NumberOfNewCustomers(starting_after:${starting_after},ending_before:${ending_before})
        }
        
        `
    });
    return response.data.data.NumberOfNewCustomers;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function numberOfBrokerAccountsOpened(
  starting_after,
  ending_before
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          NumberOfConnectedAccounts(starting_after:${starting_after},ending_before:${ending_before})
        }
        `
    });
    return response.data.data.NumberOfConnectedAccounts;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function numberOfStripePayments(
  starting_after,
  ending_before,
  stripe_broker_id = null
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          NumberOfStripePayments(starting_after:${starting_after},ending_before:${ending_before}, stripe_broker_id:${stripe_broker_id})
        }
        
        `
    });
    return response.data.data.NumberOfStripePayments;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function averageRevenuePerCustomer(starting_after, ending_before) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          AverageRevenuePerCustomer(starting_after:${starting_after},ending_before:${ending_before})
        }
        
        `
    });
    return response.data.data.AverageRevenuePerCustomer;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getDiputes(
  starting_after,
  ending_before,
  stripe_broker_id = null
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          GetDisputes(starting_after:${starting_after},ending_before:${ending_before}, stripe_broker_id:${stripe_broker_id}){
            amount
            id
          }
        }
        
        `
    });
    return response.data.data.GetDisputes;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function disputeActivtyRate(
  starting_after,
  ending_before,
  stripe_broker_id = null
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          DisputeActivityRate(starting_after:${starting_after},ending_before:${ending_before}, stripe_broker_id:${stripe_broker_id})
        }
        
        `
    });
    return response.data.data.DisputeActivityRate;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getHighRiskPayments(
  starting_after,
  ending_before,
  stripe_broker_id = null
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        GetHighRiskPayments(starting_after:${starting_after},ending_before:${ending_before}, stripe_broker_id:${stripe_broker_id}){
          id
          amount
        }
        
        `
    });
    return response.data.data.GetHighRiskPayments;
  } catch (error) {
    console.log("Could not authenticate");
  }
}
