
import axios from "axios";
import { uploadURLs, downloadURLs } from "../constants/constants";

/* This list holds all the properties we desire from a listing.
 * This was done so we only have to update 1 place when we want
 * more or less information about a listing. */
const defaultListingPropertiesQuery = `
  id  
  buyer_id
  seller_id
  cost
  sold
  inactive
  price
  quantity
  tags
  createdAt   
`;
const defaultFilterListingPropertiesQuery = `
  event_id
  eventname
  eventdate   
  venuename
  timezone
  city
  state
  country
  totalseats
  totalsoldtkts
  totalsoldtktsinlast7days
  totalavailabletkts
  daystoevent 
  awaitingdelivery
  inactive
  listingDtls
`;
const defaultAddListingsFilterQuery = `
  event_id
  eventname
  eventdate
  venuename
  city
  state
  country
  timezone  
`;
const headers = { 'Content-Type': 'multipart/form-data' };
let axiosconfig = {
  headers: headers
};

export async function getNumberOfUserListings(userId) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query allListings($userID: Int!) {
            allListings(condition: { sellerId: $userID }) {
            totalCount
            }
          }
               `,
      variables: {
        userID: userId
      }
    });
    return tokenResponse.data.data.allListings.totalCount;
  } catch (error) {
    console.log("Could not find listings");
    return false;
  }
}

/**
 * Will get a list of all performers by a certain name.
 * Can optionally limit the number of results.
 */
export async function getPerformersByName(performerName, topLimit) {
  // build the query based on a limit being passed in or not
  let performerQuery = `searchPerformersByName(_pattern: "${performerName}"`;
  if (topLimit) {
    performerQuery += ` first:${topLimit}`;
  }
  performerQuery += `) {`;
  // use that query we just built
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        ${performerQuery}
          nodes {
            name
            id
            category
            sets
          }
        }
      }`
    });
    return tokenResponse.data.data.searchPerformersByName.nodes;
  } catch (error) {
    console.log("Could not find performers");
  }
}

//search performer by first or last name
export async function getPerformersByFirstOrLastName(performerName, topLimit) {
  // build the query based on a limit being passed in or not
  let performerQuery = `searchPerformersByFirstOrLastName(_pattern: "${performerName}"`;
  if (topLimit) {
    performerQuery += ` first:${topLimit}`;
  }
  performerQuery += `) {`;
  // use that query we just built
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        ${performerQuery}
          nodes {
            name
            id
            category
            sets
          }
        }
      }`
    });
    return tokenResponse.data.data.searchPerformersByFirstOrLastName.nodes;
  } catch (error) {
    console.log("Could not find performers", error);
  }
}
export async function getAllEventsArrayList() {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query  
            {
              getAllEventIdPerformerVenueCityCountryTags
            }`

    });
    return tokenResponse.data.data.getAllEventIdPerformerVenueCityCountryTags;
  }
  catch (error) {
    console.log("Could not find performers", error);
  }
}

export async function writeListingToDB(listing) {
  try {
    // TODO: Confirm w/ Aaron we don't need this line,
    // because we don't even pass over barcode.
    //delete listing.barcode
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation createListing($listing: ListingInput!){
            createListing(input:{
              listing: $listing
            }) {
              clientMutationId
              listing {
                id
              }
            }
          }
             `,
      variables: {
        listing: listing
      }
    });
    return response.data.data.createListing.listing;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Getting all ticket types from ticket types table
 */

export async function getAllTicketTypes() {
  try {
    let eventsResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allTicketTypes {            
            id
            name
            created_at
            updated_at
          }
        }`
      }
    );
    return eventsResponse.data.data.allTicketTypes;
  } catch (error) {
    console.log("Error:No records");
  }
}


/**
 * Getting all seat types from seat types table
 */
export async function getAllSeatTypesList() {
  try {
    let seatResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allSeattypes {            
            id
            name
            created_at
            updated_at
          }
        }`
      }
    );
    return seatResponse.data.data.allSeattypes;
  } catch (error) {
    console.log("Error:No records");
  }
}

/**
 * Getting all Credit card types from genre table
 */

 export async function getAllCreditCardTypes() {
  try {
    let eventsResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allCreditCardTypes   {            
            id
            type_name
          }
        }`
      }
    );
    return eventsResponse.data.data.allCreditCardTypes;
  } catch (error) {
    console.log("Error:No records");
  }
}

/**
 * Getting all Genre types from genre table
 */

export async function allGenreDetails() {
  try {
    let eventsResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allGenreDetails   {            
            id
            name
            parent_category
            child_category
            tevo_id
            mercury_id
            created_at
            updated_at
          }
        }`
      }
    );
    return eventsResponse.data.data.allGenreDetails;
  } catch (error) {
    console.log("Error:No records");
  }
}
/**
 * Getting all Category types from category table
 */

 export async function allCategoryDetails() {
  try {
    let categoryResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allCategoryDetails   {            
            id
            name
          }
        }`
      }
    );
    return categoryResponse.data.data.allCategoryDetails;
  } catch (error) {
    console.log("Error:No records");
  }
}
/**
 * Getting all Tags  from tags table
 */
export async function allSubCategoryDetails() {
  try {
    let eventsResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allSubCategoryDetails   {            
            id
            name
            parent_category
            child_category
            tevo_id
            mercury_id
            created_at
            updated_at
          }
        }`
      }// zip_code last_login 
    );
    return eventsResponse.data.data.allSubCategoryDetails;
  } catch (error) {
    console.log("Error:No records");
  }
}
/**
 * Getting all Tags  from tags table
 */

/**
 * Getting TicketingSystem Tags  from Ticketing System table
 */
export async function getAllTagList(userId) {
  try {

    const tagsResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
            query ($GetAllTagsInput: GetAllTagsInput!) {
              allTags(input: $GetAllTagsInput){
                
                id
                tag_name
                
              }
            }`,
      variables: {
        "GetAllTagsInput": {
          "userId": userId
        }
      }
    });
    return tagsResponse.data.data.allTags;
  } catch (error) {
    console.log("Error:No records");
  }
}

/**
 * Getting all Channel Markups  from channel markups table
 */
export async function getAllChannels() {
  try {
    let channelmarkupsResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allChannels   {            
            id
            channel_name
          }
        }`
      }
    );
    return channelmarkupsResponse.data.data.allChannels;
  } catch (error) {
    console.log("Error:No records");
  }
}

/**
 * Getting TicketingSystem Tags  from Ticketing System table
 */
export async function getAllTicketingSystemList() {
  try {
    let ticketingSystemsResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allTicketingSystems   {            
            id
            ticketing_system_name

          }
        }`
      }
    );
    return ticketingSystemsResponse.data.data.allTicketingSystems;
  } catch (error) {
    console.log("Error:No records");
  }
}

/**
 * Getting Disclosures Tags  from disclosures table
 */
export async function getAllDisclosureList(userId) {
  try {
    let disclosuresResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
              query ($GetAllDisclosuresInput: GetAllDisclosuresInput!) {
                allDisclosures(input: $GetAllDisclosuresInput){
                  
                  id
                  disclosure_name
                  
                }
              }`,
        variables: {
          "GetAllDisclosuresInput": {
            "userId": userId
          }
        }
      }
    );
    return disclosuresResponse.data.data.allDisclosures;
  } catch (error) {
    console.log("Error:No records");
  }
}

/**
 * Getting all Attributes  from attribute table
 */
export async function getAllAttributeList(userId) {
  try {
    let attributesResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
              query ($GetAllAttributesInput: GetAllAttributesInput!) {
                allAttributes(input: $GetAllAttributesInput){
                  id
                  attribute_name
                }
              }`,
        variables: {
          "GetAllAttributesInput": {
            "userId": userId
          }
        }
      }
    );
    return attributesResponse.data.data.allAttributes;
  } catch (error) {
    console.log("Error:No records");
  }
}

/**
 * Getting all Ticketing System Accounts  from TicketingSystemAccounts table
 */
export async function allTicketingSystemAccounts(ticketSystemId,userId, isOperationFilter=false) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query ($getAllTicketingSystemAccountsInput: GetAllTicketingSystemAccountsInput!){
            allTicketingSystemAccounts(input: $getAllTicketingSystemAccountsInput){            
              id
              email              
            }
          }
             `,
      variables: {
        "getAllTicketingSystemAccountsInput": {
          "id": ticketSystemId,
          "userId":userId,
          "isOperationFilter":isOperationFilter
        }
      }
    });
    return response.data.data.allTicketingSystemAccounts;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Getting selected Ticketing System Accounts  from TicketingSystemAccounts table
 */
export async function selectedTicketingSystemAccounts(list_id) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query ($getAllTicketingSystemAccountsInput: GetAllTicketingSystemAccountsInput!){
            selectedTicketingSystemAccounts(input: $getAllTicketingSystemAccountsInput){            
              id
              email              
            }
          }
             `,
      variables: {
        "getAllTicketingSystemAccountsInput": {
          "id": list_id
        }
      }
    });
    return response.data.data.selectedTicketingSystemAccounts;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Create Disclosures
 */
export async function Createdisclosure(params, userId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation($addDisclosureInput: addDisclosureInput!){
            addDisclosure(input: $addDisclosureInput){
              message
              data
              status
            }
            } 
             `,
      variables: {
        "addDisclosureInput": {
          "values": params,
          "userID": userId
        }
      }
    });
    return response.data.data.addDisclosure;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Create Attributes
 */
export async function Createattribute(params, userId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation($addAttributeInput: addAttributeInput!){
              addAttribute(input: $addAttributeInput){
              message
              data
              status
              }
          } 
             `,
      variables: {
        "addAttributeInput": {
          "values": params,
          "userID": userId
        }
      }
    });
    return response.data.data.addAttribute;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Page : Add Listing
 * Function For :  Create tags based on user
 * Ticket No : TIC-113
 */
export async function Createtag(params, userId) {

  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation($addTagInput: addTagInput!){
            addTag(input: $addTagInput){
              message
              data
              status
              }
          } 
             `,
      variables: {
        "addTagInput": {
          "values": params,
          "userID": userId
        }
      }
    });
    return response.data.data.addTag;
  } catch (error) {
    console.error(error);
  }

}

/**
 * Page : Add Listing
 * Function For :  Create Ticketing system api
 * Ticket No : TIC-113
 */
export async function Createticketingsystem(params) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation($addTicketingSystemInput: addTicketingSystemInput!){
            addTicketingSystem(input: $addTicketingSystemInput){
              message
              data
              status
            }
            } 
             `,
      variables: {
        "addTicketingSystemInput": {
          "values": params
        }
      }
    });
    return response.data.data.addTicketingSystem;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Page : Listings
 * Function For :  Change listing group status
 * Ticket No : TIC-250
 */

export async function updateListingGroupStatusById(listingGroupId){
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `query ($getStatusChangeInput: getStatusChangeInput!) {
        updateListingGroupStatus(input: $getStatusChangeInput) {
          status_data
        }
      }`,
      variables: {
        "getStatusChangeInput": {
          "listing_group_id": listingGroupId
        }
      }
    });
    return response.data.data.updateListingGroupStatus;
  } catch (error) {
    console.log("Could not get portfolio data");
  }
}

 /**
 * Page : Add Listing
 * Function For :  Call Create Attachment api
 * Ticket No : TIC-243
 */
export async function listingattachment(attachment_data, listingattachment) {
  try {
    //Backend S3 Start
    {
      attachment_data && attachment_data.length > 0
        && attachment_data.map((item, i) => {

          let attachcount = 0;
          {
            item.attachments && item.attachments.length > 0
              && item.attachments.map((attachment_item, j) => {
                var attachment_lenth = item.attachments.length;
                var attachment_custom_lenth = listingattachment.length;
                const formData = new FormData();
                formData.append('attachment_file_data', attachment_item.attachment_file_data);
                formData.append('attachment_file', attachment_item.attachment_file);
                formData.append('eventId', listingattachment[attachcount].eventId);
                formData.append('ListingId', listingattachment[attachcount].result.listing_group_id);
                if (attachment_lenth !== attachment_custom_lenth) {
                  attachcount = j + 1;
                } else {
                  attachcount = j;
                }
                if (attachment_item.attachment_file) {
                  axios.post(process.env.REACT_APP_FILE_DOWNLOAD+uploadURLs.listingsAttachmentURL, formData, {
                    headers: {
                      'Content-Type': 'multipart/form-data',
                    },
                  })
                    .then(function (response) {
                      return response.data
                    })
                    .catch(function (response) {
                      return false
                    });
                }
              })
          }
        }
        )
    }
    //Backend S3 End
    return true;
  } catch (error) {
    console.log("Could not create user", error);
  }

}

/**
 * Adding listing api to Listing table
 */
export async function createListing(listingObject) {
  try {
    let listResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation($addListinginput: addListinginput!){
          addListing(input: $addListinginput){            
            isError  
            listingGroupIds  
            msg    
            attachmentData      
          }
        }
        `, variables: {
        "addListinginput": {
          "seller_id": listingObject.seller_id,
          "buyer_id": listingObject.buyer_id,
          "eventIds": listingObject.eventIds,
          "ticket_type_id": listingObject.ticket_type_id,
          "in_hand": listingObject.inhand_date,
          "splitId": parseInt(listingObject.splitId),
          "seat_type_id": listingObject.seat_type_id,
          "quantity": listingObject.quantity,
          "quantity_ga": listingObject.quantity_ga,
          "ticket_system_id": listingObject.ticketing_system,
          "ticket_system_account": listingObject.ticketing_system_accounts,
          "hide_seat_number": listingObject.hide_seat_number,
          "seatInformationsJSON": listingObject.seatInformationsJSON,
          "channelmarkupsJSON": listingObject.channelmarkupsJSON
        }
      }
    });
    return listResponse.data.data.addListing;
  } catch (error) {
    console.log("Could not create user", error);
  }


}


/**
 * Adding listing api to Listing table
 */
export async function updateListing(listingObject, channelData) {
  try {
    let listResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation($editListing: editListing!){
          editListing(input: $editListing){            
            isError    
            msg       
            attachmentData   
          }
        }
        `, variables: {
        "editListing": {
          "isPriceChanged": listingObject.isPriceChanged,
          "userId": listingObject.seller_id, 
          "eventId": listingObject.eventId,
          "listingGroupId": listingObject.listingGroupId,
          "face_value": String(listingObject.face_value),
          "ticket_type_id": listingObject.ticket_type_id,
          "in_hand": listingObject.inhand_date,
          "splits": listingObject.splitId,
          "seat_type_id": listingObject.seat_type_id,
          "quantity": listingObject.quantity,
          "quantity_ga": String(listingObject.quantity_ga),
          "ticket_system_id": listingObject.ticketing_system,
          "hide_seat_number": String(listingObject.hide_seat_number),
          "po_id": listingObject.po_id,
          "price": String(listingObject.price),
          "row": listingObject.row,
          "internal_notes": listingObject.internal_notes,
          "external_notes": listingObject.external_notes,
          "seat_end": parseInt(listingObject.seat_end),
          "seat_start": parseInt(listingObject.seat_start),
          "section": String(listingObject.section),
          "unit_cost": String(listingObject.unit_cost),
          "group_cost": String(listingObject.group_cost),
          "disclosureids": listingObject.disclosureids,
          "selectedTags": listingObject.selectedTags,
          "attrbtids": listingObject.attrbtids,
          "ticketing_system_accounts": String(listingObject.ticketing_system_accounts),
          "channelData": channelData,
          "seatInformationsJSON": listingObject.seatInformationsJSON,
          "channelmarkupsJSON": listingObject.channelmarkupsJSON
        }
      }
    });
    return listResponse.data.data.editListing;
  } catch (error) {
    console.log("Could not create user", error);
  }


}

/**
 * Fetch Attachment by Attachment Group ID
 * @param {*} input 
 * @returns 
 */
export async function fetchAttachmentsById(listingGroupId) {
  try {

    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: ` 
        query ($eventListId: fetchListing!) {
          fetchAttachmentDetails(input: $eventListId){
            id
            attachment_name
            attachment_url
          }
        }
         `,
      variables: {
        "eventListId": {
          "listingGroupId": listingGroupId
        }
      }
    });
    // filter out all listings without an ebay listing ID
    let allListings = response.data.data.fetchAttachmentDetails;
    return allListings;

  } catch (error) {

  }
}

/**
 * 
 * @param {*} input 
 * @returns matched strings for events wild card search 
 */
export async function eventSearches(input, isOperationFilter=false) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `query ($GetEventPerformerVenueTagsInput: GetEventPerformerVenueTagsInput!) {
        getAllEventIdPerformerVenueCityCountryTags(
          input: $GetEventPerformerVenueTagsInput
        )
      }      
        `, variables: {
        "GetEventPerformerVenueTagsInput": {
          "searchStr": input.searches,
          "isOperationFilter":isOperationFilter
        }
      }
    });
    return response.data.data.getAllEventIdPerformerVenueCityCountryTags;
  } catch (error) {
    console.log("Could not get lists");
  }
}
/*
*Listing and Filtering the events with related tables 
*
*/

export async function getUserFilterListings(userId, input) {
  var getListings = {};
  if (userId > 0) {
    getListings.userId = userId
  }
  if (input && input.tagIds) {
    getListings.tagIds = input.tagIds;
  }
  if (input && input.searches) {
    getListings.searches = input.searches;
  }
  if (input && input.from_date) {
    getListings.from_date = input.from_date;
  }
  if (input && input.to_date) {
    getListings.to_date = input.to_date;
  }
  if (input && input.day_of_week) {
    getListings.day_of_week = input.day_of_week;
  }
  if (input && input.ticket_type) {
    getListings.ticket_type = input.ticket_type;
  }
  if (input && input.category) {//for genre
    getListings.genre = input.category;
  }
  if (input && input.tags) {
    getListings.tags = input.tags;
  }
  if (input && input.in_hand) {
    getListings.in_hand = input.in_hand;
  }
  if (input && input.section) {
    getListings.section = input.section;
  }
  if (input && input.row) {
    getListings.row = input.row;
  }
  if (input && input.quantity) {
    getListings.quantity = input.quantity;
  }
  if (input && input.hold) {
    getListings.hold = input.hold;
  }
  if (input && input.inventory_attached) {
    getListings.inventory_attached = input.inventory_attached;
  }
  if (input && input.completedEvents) {
    getListings.completedEvents = input.completedEvents;
  }
  if (input && input.currentPage) {
    getListings.page = input.currentPage;
  }
  if (input && input.pageLimit) {
    getListings.limit = input.pageLimit;
  }

  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($getListings: GetFilterListings!) {
        filterListings(input: $getListings) {
          
          ${defaultFilterListingPropertiesQuery} 
        }
      }
        `, variables: {
        getListings
      }
    });

    return response.data.data.filterListings;
  } catch (error) {
    console.log("Could not get lists");
  }
}

/**
 * Add Listings Filter Page - To Get Add Filter Listings Data - TIC 109,110,112
 */
export async function getAddListingsFilter(input) {
  var getListings = {};
  if (input && input.searches) {
    getListings.searches = input.searches;
  }
  if (input && input.from_date) {
    getListings.from_date = input.from_date;
  }
  if (input && input.to_date) {
    getListings.to_date = input.to_date;
  }
  if (input && input.day_of_week) {
    getListings.day_of_week = input.day_of_week;
  }
  if (input && input.ticket_type) {
    getListings.ticket_type = input.ticket_type;
  }
  if (input && input.category) {//for genre
    getListings.genre = input.category;
  }
  if (input && input.tags) {
    getListings.tags = input.tags;
  }
  if (input && input.in_hand) {
    getListings.in_hand = input.in_hand;
  }
  if (input && input.section) {
    getListings.section = input.section;
  }
  if (input && input.row) {
    getListings.row = input.row;
  }
  if (input && input.quantity) {
    getListings.quantity = input.quantity;
  }
  if (input && input.inventory_attached) {
    getListings.inventory_attached = input.inventory_attached;
  }
  if (input && input.currentPage) {
    getListings.page = input.currentPage;
    if(input.currentPage==0){
      getListings.page = 1;
    }  
  }
  if (input && input.pageLimit) {
    getListings.limit = input.pageLimit;
  }


  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($getListings: GetAddFilterListings!) {
        addListingsFilter(input: $getListings) {
          ${defaultAddListingsFilterQuery} 
        }
      }
        `, variables: {
        getListings
      }
    });
    return response.data.data.addListingsFilter;
  } catch (error) {
    console.log("Could not get lists");
  }
}

export async function getUserListings(userID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($getListings: GetListings!) {
        allListings(input: $getListings) {
          ${defaultListingPropertiesQuery} 
        }
      }

        `, variables: {
        "getListings": {
          "seller_id": userID
        }
      }
    });
    return response.data.data.allListings;
  } catch (error) {
    console.log("Could not get lists");
  }
}

//Update Listing Price - Listing Level
export async function updateListingPrice(listingGroupId, price, oldPrice, eventId, userId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($UpdateListingInput:updateListingPriceInput!){
          updateListingPriceById(input:$UpdateListingInput) {
            response
          }
        }`,
      variables: {
        "UpdateListingInput": {
          "listingGroupId": listingGroupId,
          "price": parseFloat(price),
          "lastPrice": parseFloat(oldPrice), 
          "eventId": eventId, 
          "userId": userId
        }
      }
    });
    return response.data.data.updateListingPriceById;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

//Update Listing Price - Event Level
export async function updateEventLevelListingPrice(eventId, price, userId,updateType,listingIds)
{
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($input:updateEventLevelListingPriceInput!){
          updateEventLevelListingPrice(input:$input) {
            isError
            msg
          }
        }`,
      variables: {
        "input": {
          "eventId": eventId,
          "price": parseFloat(price), 
          "userId": userId,
          "updateSymbol": updateType,
          "listingIds": listingIds
        }
      }
    });
    return response.data.data.updateEventLevelListingPrice;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

//Update Listing Price - Event Level
export async function updateMultiListLevelListingPrice(listIds,eventId, price, userId,updateType)
{
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($input:updateMultiListingPriceInput!){
          updateMultiListingPriceById(input:$input) {
            isError
            msg
          }
        }`,
      variables: {
        "input": {
          "listIds": listIds,
          "eventId": eventId,
          "price": parseFloat(price), 
          "userId": userId,
          "updateSymbol":updateType
        }
      }
    });
    return response.data.data.updateMultiListingPriceById;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateListingLastPriceChange(listingID, priceChangeTime) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation {
          updateListingById(input:{
            id: ${listingID},
            listingPatch:{
              lastPriceChange: "${priceChangeTime}"
            }
          }) {
            clientMutationId
            listing{
              price
            }
          }
        }`,
    });
    console.log("Attempted to update price last changed time");
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing's price change time");
  }
}

export async function updateListingSold(listingID, sold) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingSold($listingID: Int!, $sold: Boolean){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              sold: $sold
            }
          }) {
            clientMutationId
            listing{
              sold
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        sold: sold
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateListingInactive(listingID, inactive) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingSold($listingID: Int!, $inactive: Boolean){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              inactive: $inactive
            }
          }) {
            clientMutationId
            listing{
              inactive
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        inactive: inactive
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateListingTags(listingID, tags) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingTags($listingID: Int!, $tags: String){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              tags: $tags
            }
          }) {
            clientMutationId
            listing{
              tags
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        tags: tags
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update tags on listing");
  }
}

export async function updateListingNotes(listingID, notes) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingSold($listingID: Int!, $notes: String){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              misc: $notes
            }
          }) {
            clientMutationId
            listing{
              misc
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        notes: notes
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing notes");
  }
}

export async function updateListingQuantity(listingID, quantity) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingQuantity($listingID: Int!, $quantity: Int){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              quantity: $quantity
            }
          }) {
            clientMutationId
            listing{
              quantity
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        quantity: quantity
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing quantity");
  }
}

export async function updateListingPurchaseOrderId(listingID, purchaseOrderId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingQuantity($listingID: Int!, $purchaseOrderId: Int){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              purchaseOrderId: $purchaseOrderId
            }
          }) {
            clientMutationId
            listing{
              purchaseOrderId
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        purchaseOrderId: purchaseOrderId
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing purchase order ID");
  }
}

export async function updateListingReservePrice(listingID, reserve) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingReservePrice($listingID: Int!, $reserve: Float){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              reserve: $reserve
            }
          }) {
            clientMutationId
            listing{
              reserve
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        reserve: reserve
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing reserve");
  }
}

export async function updateListingCardAttribute(listingID, cardAttribute) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateEbayActive($listingID: Int!, $cardAttribute: String){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              cardAttribute: $cardAttribute
            }
          }) {
            clientMutationId
            listing{
              cardAttribute
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        cardAttribute: cardAttribute
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing cardAttribute");
  }
}

export async function updateEbayActive(listingID, ebayMarkupActive) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateEbayActive($listingID: Int!, $ebayMarkupActive: Boolean){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              ebayMarkupActive: $ebayMarkupActive
            }
          }) {
            clientMutationId
            listing{
              ebayMarkupActive
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        ebayMarkupActive: ebayMarkupActive
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing stubhubMarkupActive");
  }
}

export async function updateEbayMarkupType(listingID, ebayMarkupType) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingEbayType($listingID: Int!, $ebayMarkupType: String){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              ebayMarkupType: $ebayMarkupType
            }
          }) {
            clientMutationId
            listing{
              ebayMarkupActive
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        ebayMarkupType: ebayMarkupType
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing ebayMarkupType");
  }
}

export async function updateEbayMarkup(listingID, ebayMarkup) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateEbayMarkup($listingID: Int!, $ebayMarkup: Int){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              ebayMarkup: $ebayMarkup
            }
          }) {
            clientMutationId
            listing{
              ebayMarkup
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        ebayMarkup: ebayMarkup
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing ebayMarkup");
  }
}

export async function updateStockxActive(listingID, stockxMarkupActive) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateEbayActive($listingID: Int!, $stockxMarkupActive: Boolean){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              stockxMarkupActive: $stockxMarkupActive
            }
          }) {
            clientMutationId
            listing{
              stockxMarkupActive
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        stockxMarkupActive: stockxMarkupActive
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing stockxMarkupActive");
  }
}

export async function updateStockxMarkupType(listingID, stockxMarkupType) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingEbayType($listingID: Int!, $stockxMarkupType: String){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              stockxMarkupType: $stockxMarkupType
            }
          }) {
            clientMutationId
            listing{
              stockxMarkupType
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        stockxMarkupType: stockxMarkupType
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing stockxMarkupType");
  }
}

export async function updateStockxMarkup(listingID, stockxMarkup) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateEbayMarkup($listingID: Int!, $stockxMarkup: Int){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              stockxMarkup: $stockxMarkup
            }
          }) {
            clientMutationId
            listing{
              stockxMarkup
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        stockxMarkup: stockxMarkup
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing stockxMarkup");
  }
}

export async function updatePhunnelActive(listingID, phunnelMarkupActive) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateEbayActive($listingID: Int!, $phunnelMarkupActive: Boolean){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              phunnelMarkupActive: $phunnelMarkupActive
            }
          }) {
            clientMutationId
            listing{
              phunnelMarkupActive
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        phunnelMarkupActive: phunnelMarkupActive
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing phunnelMarkupActive");
  }
}

export async function updatePhunnelMarkupType(listingID, phunnelMarkupType) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingEbayType($listingID: Int!, $phunnelMarkupType: String){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              phunnelMarkupType: $phunnelMarkupType
            }
          }) {
            clientMutationId
            listing{
              phunnelMarkupType
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        phunnelMarkupType: phunnelMarkupType
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing phunnelMarkupType");
  }
}

export async function updatePhunnelMarkup(listingID, phunnelMarkup) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateEbayMarkup($listingID: Int!, $phunnelMarkup: Int){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              phunnelMarkup: $phunnelMarkup
            }
          }) {
            clientMutationId
            listing{
              phunnelMarkup
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        phunnelMarkup: phunnelMarkup
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing phunnelMarkup");
  }
}

export async function getCollectionValue(userID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
          getCollectionValue(_sellerid:${userID})
        }`
    });
    return response.data.data.getCollectionValue;
  } catch (error) {
    console.log("Could not update listing notes");
  }
}

/**
    * Page : home
    * Function For : To get the portfolio insights data
    * Ticket No : TIC-34
*/

export async function protFoloioData(userID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `query ($getCollectionValue: getSellerId!){
        getCollectionValue(input: $getCollectionValue){            
          current_inventory_value,
          margin_value,
          life_time_sales_value,
          live_listing_value,
          hold_time_data_value,
          busted_order_date,
          average_lisitng_age            
        }
      }`,
      variables: {
        "getCollectionValue": {
          "seller_id": userID
        }
      }
    });
    return response.data.data.getCollectionValue;
  } catch (error) {
    console.log("Could not get portfolio data");
  }
}

export async function orderCardInfo(userID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `query ($orderCardInfo: getSellerId!){
        orderCardInfo(input: $orderCardInfo){            
          pastDue,
          openOrders,
          completedOrders,
          delivery           
        }
      }`,
      variables: {
        "orderCardInfo": {
          "seller_id": userID
        }
      }
    });
    return response.data.data.orderCardInfo;
  } catch (error) {
    console.log("Could not fetch order card info");
  }
}

export async function getAllSoldListings(userID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
          allListings(condition: { sold:true, sellerId: ${userID} }) {
            nodes {
              id
              name
              cardByCardId {
                cardNumber
                setBySet{
                  name
                  year
                  brandByBrandId{
                    name
                  }
                }
              }
              cost
              grade
              reserve
              isAuction
              sold
              inactive
              price
              quantity
              misc
            }
          }
        }`
    });
    return response.data.data.allListings.nodes;
  } catch (error) {
    console.log("Could not get all sold user listings.");
  }
}

export async function getLiveUserListings(userID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
          allListings(condition: { sold: false, inactive: false, sellerId: ${userID} }) {
            nodes {
              createdAt
            }
          }
        }`
    });
    return response.data.data.allListings.nodes;
  } catch (error) {
    console.log("Could not get all live user listings.");
  }
}

/**
  * Page : home
  * Function For : To display the donut chart for Performers & Category
  * Ticket No : TIC-38 & 39
  * TO DO : Need to fetch those data dynamically
*/

export async function getUnsoldUserListings(userID, chartType) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `query ($getChartDetails: getPieChartType!) {
        getChartDetails(input: $getChartDetails) {
          chartData
        }
      }`,
      variables: {
        "getChartDetails": {
          "seller_id": userID,
          "pieChartType": chartType
        }
      }
    });
    return response.data.data.getChartDetails;
  } catch (error) {
    console.log("Could not get all live user listings.");
  }
}

export async function getNumberOfSoldWaitingUserListings(userID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
          allListings(condition: { sold: true, delivered:false, sellerId: ${userID} }) {
            totalCount
          }
        }`
    });
    return response.data.data.allListings.totalCount;
  } catch (error) {
    console.log("Could not get number for all sold user listings.");
  }
}

export async function getNumberOfDeliveredUserListings(userID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
          allListings(condition: { sold:true, delivered:true, sellerId: ${userID} }) {
            totalCount
          }
        }`
    });
    return response.data.data.allListings.totalCount;
  } catch (error) {
    console.log("Could not get number for all delivered user listings.");
    return false;
  }
}

export async function filterAllLiveListingsBySeller(
  sellerId,
  startDate,
  endDate
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          returnLiveListingsByDateRangeAndSellerid(_sellerid: ${sellerId}, _startdate:  "${startDate}", _enddate: "${endDate}"){
            nodes {
              ${defaultListingPropertiesQuery}
            }
          }
        }
        `
    }
    );
    return response.data.data.returnLiveListingsByDateRangeAndSellerid.nodes;
  } catch (error) {
    console.info(error);
  }
}

export async function getUserListingsInAuction(userID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
          allListings(condition: { isAuction:true, sold: false, inactive: false, sellerId: ${userID} }) {
            nodes {
              id
              name
              isAuction
              auctionEventByAuctionEventId{
                id
                startDate
                endDate
              }
            }
          }
        }`
    });
    return response.data.data.allListings.nodes;
  } catch (error) {
    console.log("Could not get all sold user listings.");
  }
}

export async function getPotentialCollectionValue(userID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
          getPotentialCollectionValue(_sellerid:${userID})
        }`
    });
    return response.data.data.getPotentialCollectionValue;
  } catch (error) {
    console.log("Could not get potential collection value");
  }
}

export async function getTotalUnsoldUserListingValue(userID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
          getUnsoldCollectionValue(_sellerid:${userID})
        }`
    });
    return response.data.data.getUnsoldCollectionValue;
  } catch (error) {
    console.log("Could not get potential collection value");
  }
}

/**
    * Page : home
    * Function For : To get the top events listings
    * Ticket No : TIC-37
*/

export async function getTopFiveSoldListings(sellerID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `query ($getTopFiveSoldListings: getSellerId!){
        getTopFiveSoldListings(input: $getTopFiveSoldListings){            
          revenue_list,
          sale_list
        }
      }`,
      variables: {
        "getTopFiveSoldListings": {
          "seller_id": sellerID
        }
      }
    });
    return response.data.data.getTopFiveSoldListings;
  } catch (error) {
    console.log("Could not get top sold listings");
  }
}

/**
    * Page : home
    * Function For : To get the top events listings
    * Ticket No : TIC-38 & 39
    * TO DO : Need to fetch those data dynamically
*/

export async function getSoldListingsByPerformer(sellerID, performerName) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
          allListings(condition: { name: "${performerName}", sold: true, sellerId: ${sellerID} }) {
            totalCount
          }
        }`
    });
    return response.data.data.allListings.totalCount;
  } catch (error) {
    console.log("Could not get number of sold listings by performer");
  }
}

/**
      * Page : home
      * Function For : Added API services to get the hot sellers
      * Ticket No : TIC-136 & 137
*/

export async function getHotSellersList() {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        getHotSellersList{            
          seller_list
        }
      }`
    });
    return response.data.data.getHotSellersList;
  } catch (error) {
    console.log("Could not get hot sellers listings");
  }
}

/**
      * Page : home
      * Function For : Added API services to get the hot buyers
      * Ticket No : TIC-136 & 137
*/

export async function getHotBuyersList() {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        getHotBuyersList{            
          buyer_list
        }
      }`
    });
    return response.data.data.getHotBuyersList;
  } catch (error) {
    console.log("Could not get hot buyers listings");
  }
}

export async function updateAllUserListingsEbayMarkup(sellerID, ebayMarkup) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `mutation {
        updateUserListingBySellerid(
          input: {sellerid:${sellerID}, userListingPatch: {stubhubmarkup:  "${ebayMarkup}"}}
        ) {
          userListing {
            sellerid
            stubhubmarkup
          }
          
        }
      }`
    });
    return response.data.data.updateUserListingsEbayMarkup;
  } catch (error) {
    console.log("Could not update Stubhub markup values for all user listings");
  }
}

export async function updateAllUserListingsEbayMarkupType(
  sellerID,
  ebayMarkupType
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `mutation {
        updateUserListingBySellerid(
          input: {sellerid:${sellerID}, userListingPatch: {stubhubmarkuptype:  "${ebayMarkupType}"}}
        ) {
          userListing {
            sellerid
            stubhubmarkuptype
          }
          
        }
      }`
    });
    return response.data.data.updateUserListingsEbayMarkupType;
  } catch (error) {
    console.log("Could not update ebay markup type for all user listings");
  }
}

export async function updateAllUserListingsPhunnelMarkup(
  sellerID,
  phunnelMarkup
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: ` mutation {
        updateUserListingBySellerid(
          input: {sellerid:${sellerID}, userListingPatch: {phunnelmarkup:  "${phunnelMarkup}"}}
        ) {
          userListing {
            sellerid
            phunnelmarkup
          }          
        }
      } `
    });
    return response.data.data.updateUserListingsPhunnelMarkup;
  } catch (error) {
    console.log("Could not update phunnel markup values for all user listings");
  }
}

export async function updateAllUserListingsPhunnelMarkupType(
  sellerID,
  phunnelMarkupType
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `mutation {
        updateUserListingBySellerid(
          input: {sellerid:${sellerID}, userListingPatch: {phunnelmarkuptype:  "${phunnelMarkupType}"}}
        ) {
          userListing {
            sellerid
            phunnelmarkuptype
          }          
        }
      } `
    });
    return response.data.data.updateUserListingsPhunnelMarkupType;
  } catch (error) {
    console.log("Could not update phunnel markup type for all user listings");
  }
}

export async function updateAllUserListingsAmazonMarkup(
  sellerID,
  amazonMarkup
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `mutation {
        updateUserListingBySellerid(
          input: {sellerid:${sellerID}, userListingPatch: {viagogomarkup:  "${amazonMarkup}"}}
        ) {
          userListing {
            sellerid
            viagogomarkup
          }          
        }
      }`
    });
    return response.data.data.updateUserListingsAmazonMarkup;
  } catch (error) {
    console.log("Could not update Viagogo markup values for all user listings");
  }
}

export async function updateAllUserListingsAmazonMarkupType(
  sellerID,
  amazonMarkupType
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `mutation {
        updateUserListingBySellerid(
          input: {sellerid:${sellerID}, userListingPatch: {viagogomarkuptype:  "${amazonMarkupType}"}}
        ) {
          userListing {
            sellerid
            viagogomarkuptype
          }          
        }
      }`
    });
    return response.data.data.updateUserListingsAmazonMarkupType;
  } catch (error) {
    console.log("Could not update Viagogo markup type for all user listings");
  }
}
export async function updateAllUserListingsStockxMarkup(
  sellerID,
  stockxMarkup
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `mutation {
        updateUserListingBySellerid(
          input: {sellerid:${sellerID}, userListingPatch: {seatgeekmarkup:  "${stockxMarkup}"}}
        ) {
          userListing {
            sellerid
            seatgeekmarkup
          }          
        }
      }`
    });
    return response.data.data.updateUserListingsStockxMarkup;
  } catch (error) {
    console.log("Could not update StockX markup values for all user listings");
  }
}

export async function updateAllUserListingsStockxMarkupType(
  sellerID,
  stockxMarkupType
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `mutation {
        updateUserListingBySellerid(
          input: {sellerid:${sellerID}, userListingPatch: {seatgeekmarkuptype:  "${stockxMarkupType}"}}
        ) {
          userListing {
            sellerid
            seatgeekmarkuptype
          }          
        }
      }`
    });
    return response.data.data.updateUserListingsStockxMarkupType;
  } catch (error) {
    console.log("Could not update stockx markup type for all user listings");
  }
}

export async function filterAllDeliveredListingsBySeller(sellerId, startDate, endDate) {
  try {
    const response = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          returnDeliveredListingsByDateRangeAndSellerid(_sellerid: ${sellerId}, _startdate:  "${startDate}", _enddate: "${endDate}"){
            nodes {
              ${defaultListingPropertiesQuery}
            }
          }
        }
        `
      }
    );
    return response.data.data.returnDeliveredListingsByDateRangeAndSellerid.nodes;
  } catch (error) {
    console.info(error);
  }
}

export async function filterAllOpenListingsBySeller(sellerId, startDate, endDate) {
  try {
    const response = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          returnOpenListingsByDateRangeAndSellerid(_sellerid: ${sellerId}, _startdate:  "${startDate}", _enddate: "${endDate}"){
            nodes {
              ${defaultListingPropertiesQuery}
            }
          }
        }
        `
      }
    );
    return response.data.data.returnOpenListingsByDateRangeAndSellerid.nodes;
  } catch (error) {
    console.info(error);
  }
}

export async function filterAllListingsLinkedToPurchaseOrderBySeller(sellerId, startDate, endDate) {
  try {
    const response = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          returnListingsWithLinkedPurchaseOrderByDateAndSeller(_sellerid: ${sellerId}, _startdate:  "${startDate}", _enddate: "${endDate}"){
            nodes {
              ${defaultListingPropertiesQuery}
            }
          }
        }
        `
      }
    );
    return response.data.data.returnListingsWithLinkedPurchaseOrderByDateAndSeller.nodes;
  } catch (error) {
    console.info(error);
  }
}

export async function filterAllLiveListingsByDate(startDate, endDate) {
  try {
    const response = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          returnLiveListingsByDateRange(_startdate:  "${startDate}", _enddate: "${endDate}"){
            nodes {
              ${defaultListingPropertiesQuery}
            }
          }
        }
        `
      }
    );
    return response.data.data.returnLiveListingsByDateRange.nodes;
  } catch (error) {
    console.info(error);
  }
}

export async function filterAllDeliveredListingsByDate(startDate, endDate) {
  try {
    const response = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          returnDeliveredListingsByDateRange(_startdate:  "${startDate}", _enddate: "${endDate}"){
            nodes {
              ${defaultListingPropertiesQuery}
            }
          }
        }
        `
      }
    );
    return response.data.data.returnDeliveredListingsByDateRange.nodes;
  } catch (error) {
    console.info(error);
  }
}

export async function filterAllOpenListingsByDate(startDate, endDate) {
  try {
    const response = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          returnOpenListingsByDateRange(_startdate:  "${startDate}", _enddate: "${endDate}"){
            nodes {
              ${defaultListingPropertiesQuery}
            }
          }
        }
        `
      }
    );
    return response.data.data.returnOpenListingsByDateRange.nodes;
  } catch (error) {
    console.info(error);
  }
}

export async function filterAllListingsLinkedToPurchaseOrderByDate(startDate, endDate) {
  try {
    const response = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          returnListingsWithLinkedPurchaseOrderByDate(_startdate:  "${startDate}", _enddate: "${endDate}"){
            nodes {
              ${defaultListingPropertiesQuery}
            }
          }
        }
        `
      }
    );
    return response.data.data.returnListingsWithLinkedPurchaseOrderByDate.nodes;
  } catch (error) {
    console.info(error);
  }
}

export async function updateAllUserListingsAmazonMarkupActive(
  sellerID,
  amazonMarkupActive
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `mutation {
        updateUserListingBySellerid(
          input: {sellerid:${sellerID}, userListingPatch: {viagogomarkupActive:  ${amazonMarkupActive}}}
        ) {
          userListing {
            sellerid
            
          }          
        }
      }`
    });
    return response.data.data.updateUserAmazonChannel;
  } catch (error) {
    console.log("Could not update Viagogo markup active for user listings");
  }
}

export async function updateAllUserListingsEbayMarkupActive(
  sellerID,
  ebayMarkupActive
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `mutation {
        updateUserListingBySellerid(
          input: {sellerid:${sellerID}, userListingPatch: {stubhubmarkupActive:   ${ebayMarkupActive} }}
        ) {
          userListing {
            sellerid
            
          }          
        }
      }`
    });
    return response.data.data.updateUserEbayChannel;
  } catch (error) {
    console.log("Could not update Stubhub markup active for user listings");
  }
}

export async function updateAllUserListingsPhunnelMarkupActive(
  sellerID,
  phunnelMarkupActive
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `mutation {
        updateUserListingBySellerid(
          input: {sellerid:${sellerID}, userListingPatch: {phunnelmarkupActive:   ${phunnelMarkupActive} }}
        ) {
          userListing {
            sellerid
            
          }          
        }
      }`
    });
    return response.data.data.updateUserPhunnelChannel;
  } catch (error) {
    console.log("Could not update phunnel markup active for user listings");
  }
}

export async function updateAllUserListingsStockxMarkupActive(
  sellerID,
  stockxMarkupActive
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `mutation {
        updateUserListingBySellerid(
          input: {sellerid:${sellerID}, userListingPatch: {seatgeekmarkupActive:   ${stockxMarkupActive} }}
        ) {
          userListing {
            sellerid
            
          }          
        }
      }`
    });
    return response.data.data.updateUserStockxChannel;
  } catch (error) {
    console.log("Could not update stockX markup active for user listings");
  }
}

export async function SearchSetByNameAndYear(setName, year) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
          searchSetByNameAndYear(_pattern:"${setName}", _year:${year}){
            nodes {
              name
              year
              id
            }
          }
        }`
    });
    return response.data.data.searchSetByNameAndYear.nodes;
  } catch (error) {
    console.log("Could not search sets by name & year");
  }
}

export async function updateListingImage(listingID, imageUrlArray) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingImage($listingID: Int!, $imageUrlArray: String){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              image: $imageUrlArray
            }
          }) {
            clientMutationId
            listing{
              image
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        imageUrlArray: imageUrlArray
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing images");
  }
}

export async function getListingById(listingID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        listingById(id:${listingID}) {
          ${defaultListingPropertiesQuery}
        }
      }`
    });
    return response.data.data.listingById;
  } catch (error) {
    console.log("Could not get listing by ID");
  }
}

export async function updateListingEbayOrderId(listingID, ebayOrderId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingEbayOrderId($listingID: Int!, $ebayOrderId: String){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              ebayOrderId: $ebayOrderId
            }
          }) {
            clientMutationId
            listing{
              id
              ebayOrderId
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        ebayOrderId: ebayOrderId
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing Stubhub order ID");
  }
}

export async function updateListingEbayListingId(listingID, ebayListingId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateListingEbayListingId($listingID: Int!, $ebayListingId: String){
          updateListingById(input:{
            id: $listingID,
            listingPatch:{
              ebayListingId: $ebayListingId
            }
          }) {
            clientMutationId
            listing{
              id
              ebayListingId
            }
          }
        }
        `,
      variables: {
        listingID: listingID,
        ebayListingId: ebayListingId
      }
    });
    return response.data.data.updateListingById.listing;
  } catch (error) {
    console.log("Could not update listing Stubhub listing ID");
  }
}

export async function removeListingById(listingID) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation($listingID: Int!){
          deleteListingById(input:{
            id: $listingID,
          }) {
            listingEdge{
              node {
                id
              }
            }
          }
        }
        `,
      variables: {
        listingID: listingID
      }
    });
    return response.data.data.deleteListingById;
  } catch (error) {
    console.log("Could not delete listing by listing ID");
  }
}

export async function getAllPublishableUserListings(sellerId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        allListings(condition:{sellerId:${sellerId}, sold: false, inactive: false}) {
          nodes {
            ${defaultListingPropertiesQuery}
          }
        }
      }`
    });
    // filter out all listings without an ebay listing ID
    let allListings = response.data.data.allListings.nodes;
    allListings = allListings.filter(listing => listing.ebayOrderId);
    return allListings;
  } catch (error) {
    console.log("Could not get listing by ID");
  }
}

export async function fetchListById(listingGroupId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: ` 
        query ($eventListId: fetchListing!) {
          fetchListingDetails(input: $eventListId){
            event_id
            in_hand
            tagids
            attributeids
            disclosureids
            channel_ids
            ticket_type_id
            seat_type_id
            splits_id
            ticket_system_id
            hide_seat_numbers
            ticket_system_account
            quantity
            row
            seat_start
            seat_end
            price
            face_value
            group_cost
            unit_cost
            internal_notes
            external_notes
            section
            PO_ID
            event_name
            venue_name
            city
            state
            country
            eventdate
            dateDiff
            chm_ids
            sold
            days_old
            timezone
          }
        }
         `,
      variables: {
        "eventListId": {
          "listingGroupId": listingGroupId
        }
      }
    });
    // filter out all listings without an ebay listing ID
    let allListings = response.data.data.fetchListingDetails[0];
    return allListings;
  } catch (error) {
    console.log("Could not get listing by ID");
  }
}
export async function fetchChannelValues(listingGroupId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: ` 
        query ($eventListId: fetchListing!) {
          fetchChannelMarkups(input: $eventListId){
            values            
          }
        }`,
      variables: {
        "eventListId": {
          "listingGroupId": listingGroupId
        }
      }
    });
    // filter out all listings without an ebay listing ID
    let allListings = response.data.data.fetchChannelMarkups.values;
    return allListings;
  } catch (error) {
    console.log("Could not get listing by ID");
  }
}

export async function updateShareListings(listingGroupId, shareType, levelName, eventId, inActive) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($inputData:updateShareListingsInput!){
          updateShareListings(input:$inputData) {
            isError    
            msg
          }
        }`,
      variables: {
        "inputData": {
          "listingGroupId": listingGroupId,
          "shareType": shareType,
          "levelName": levelName,
          "eventId": eventId,
          "inActive": inActive
        }
      }
    });
    return response.data.data.updateShareListings;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateInternalNotesListings(listingGroupId, internalNotes, levelName, eventId, tags) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($inputData:updateInternalNotesListingsInput!){
          updateInternalNotesListings(input:$inputData) {
            isError    
            msg
          }
        }`,
      variables: {
        "inputData": {
          "listingGroupId": listingGroupId,
          "internalNotes": internalNotes,
          "levelName": levelName,
          "eventId": eventId,
          "selectedTags":tags
        }
      }
    });
    return response.data.data.updateInternalNotesListings;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

//Update Seat type
export async function updateSeatType(selectedTicketTypes, selectedListingId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation($inputData: updateSeatTypeListingsInput!){
            updateSeatType(input: $inputData){
                  isError
                  msg
            }
          }`,
      variables: {
        "inputData": {
          "selectedTicketTypes": selectedTicketTypes,
          "selectedListingId": selectedListingId
        }
      }
    });
    return response.data.data.updateSeatType;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

//Update Stock type
export async function updateStockType(selectedStockTypes, selectedListingId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation($inputData: updateStockTypeListingsInput!){
            updateStockType(input: $inputData){
                  isError
                  msg
            }
          }`,
      variables: {
        "inputData": {
          "selectedStockTypes": selectedStockTypes,
          "selectedListingId": selectedListingId
        }
      }
    });
    return response.data.data.updateStockType;

  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateDisclosuresInListings(listingGroupId, disclosures, externalNotes, levelName, eventId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($inputData:updateDisclosuresInListingsInput!){
          updateDisclosuresInListings(input:$inputData) {
            isError    
            msg
          }
        }`,
      variables: {
        "inputData": {
          "listingGroupId": listingGroupId,
          "disclosures": disclosures,
          "externalNotes": externalNotes,
          "levelName": levelName,
          "eventId": eventId
        }
      }
    });
    return response.data.data.updateDisclosuresInListings;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getAllSplits() {
  try {
    let eventsResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          getAllSplits {            
            id
            name
            created_at
            updated_at
          }
        }`
      }
    );
    return eventsResponse.data.data.getAllSplits;
  } catch (error) {
    console.log("Error:No records");
  }
}


export async function updateSplitsInListings(listingGroupId, splitId, levelName, eventId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($inputData:updateSplitsInListingsInput!){
          updateSplitsInListings(input:$inputData) {
            isError    
            msg
          }
        }`,
      variables: {
        "inputData": {
          "listingGroupId": listingGroupId,
          "splitId": parseInt(splitId),
          "levelName": levelName,
          "eventId": eventId
        }
      }
    });
    return response.data.data.updateSplitsInListings;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function removeListing(listingGroupId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: ` 
      mutation($removeListing: removeListing!){
        removeListing(input: $removeListing){            
          isError    
          msg          
        }
      }`,
      variables: {
        "removeListing": {
          "listingGroupId": listingGroupId
        }
      }
    });
    // filter out all listings without an ebay listing ID
    let allListings = response.data.data.removeListing;
    return allListings;
  } catch (error) {
    console.log("Could not get listing by ID");
  }
}

export async function updateinhandDateInListings(inHand, eventId, level, listingGroupIdArray) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($inputData:updateInhandDateInListingsInput!){
          updateInhandDateInListings(input:$inputData) {
            isError    
            msg
          }
        }`,
      variables: {
        "inputData": {
          "inHand": inHand,
          "eventId": eventId,
          "levelName":level,
          "listingGroupId":listingGroupIdArray
        }
      }
    });
    return response.data.data.updateInhandDateInListings;

  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getInHandDateListing(eventId, type) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($inputData:getInHandDateListingListing!){
          getInHandDateListing(input:$inputData) {
            isError    
            msg
            listingData
          }
        }`,
      variables: {
        "inputData": {
          "eventId": eventId,
          "type": type
        }
      }
    });

    return response.data.data.getInHandDateListing;

  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getListingData(listingGroupId, level, eventId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($inputData:getListingDataInput!){
          getListingData(input:$inputData) {
            isError    
            msg
            listingData
          }
        }`,
      variables: {
        "inputData": {
          "listingGroupId": listingGroupId,
          "eventId": eventId,
          "level": level,
        }
      }
    });

    return response.data.data.getListingData;

  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getEventDetailsBasedOnEventId(eventIds) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        query ($inputData:getEventDetailsInput!){
          getEventDetailsBasedOnEventId(input:$inputData) {
            event_id
            eventname
            eventdate
            venuename
            city
            state
            country
            timezone
          }
        }`,
      variables: {
        "inputData": {
          "eventIds": eventIds
        }
      }
    });
    return response.data.data.getEventDetailsBasedOnEventId;

  } catch (error) {
    console.log("Could not authenticate");
  }
}

/**
  * Download file from s3 bucket
  */
export async function getFileFromAWS(eventId, listId, attachment) {
  try {
    const formData = new FormData();

    formData.append('eventId', eventId);
    formData.append('ListingId', listId);
    formData.append('attachmentname', attachment);
    let response = await axios.post(process.env.REACT_APP_FILE_DOWNLOAD+downloadURLs.listingsAttachmentURL, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
    return response.data;

  }
  catch (error) {
    console.log(error)
  }
}

/**
 * Get listing level value based on list id
 */
export async function getListingIconId(listingGroupId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: ` 
      query ($eventListId: fetchListing!) {
        fetchListingIconsId(input: $eventListId){
          splits_id
          internal_notes
          external_notes
          in_hand
          disclosure_id
        }
      }`,
      variables: {
        "eventListId": {
          "listingGroupId": listingGroupId
        }
      }
    });
    // filter out all listings without an ebay listing ID
    let allListings = response.data.data.fetchListingIconsId[0];
    return allListings;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateListingGroupMarkAsSold(listingGroupId)
{
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: ` 
      mutation ($input: updateListingGroupMarkAsSoldInput!) {
        updateListingGroupMarkAsSold(input: $input){
          isError
          msg
        }
      }`,
      variables: {
        "input": {
          "listingGroupId": listingGroupId
        }
      }
    });
    // filter out all listings without an ebay listing ID
    return response.data.data.updateListingGroupMarkAsSold;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function checkInhandDateIsEqual(eventId)
{
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: ` 
      query ($checkInhandDateInput: checkInhandDateInput!) {
        checkInhandDateIsEqual(input: $checkInhandDateInput) {
          response
          inHandDate
        }
      }`,
      variables: {
        "checkInhandDateInput": {
          "eventId": eventId
        }
      }
    });
    // filter out all listings without an ebay listing ID
    return response.data.data.checkInhandDateIsEqual;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function createTicketingSystemAccount(data, userId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation($addTicketingSystemAccountInput: addTicketingSystemAccountInput!){
            addTicketingSystemAccount(input: $addTicketingSystemAccountInput){
              message
              data
              status
            }
            } 
             `,
      variables: {
        "addTicketingSystemAccountInput": {
          "values": data,
          "userID": userId
        }
      }
    });
    return response.data.data.addTicketingSystemAccount;
  } catch (error) {
    console.error(error);
  }
}

export async function getRecentListingDetailsBasedOnEventId(eventIds) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        query ($inputData:getEventDetailsInput!){
          getRecentListingDetailsBasedOnEventId(input:$inputData) {
            event_id
            eventname
            eventdate
            tickettype_id
            ticket_system_id
            seat_type_id
            splits_id
            in_hand
            hide_seat_numbers
          }
        }`,
      variables: {
        "inputData": {
          "eventIds": eventIds
        }
      }
    });
    return response.data.data.getRecentListingDetailsBasedOnEventId;

  } catch (error) {
    console.log("Could not authenticate");
  }
}
export async function getEventCount(userId, input) {
  var getListings = {};
  if (userId > 0) {
    getListings.userId = userId
  }
  if (input && input.searches) {
    getListings.searches = input.searches;
  }
  if (input && input.from_date) {
    getListings.from_date = input.from_date;
  }
  if (input && input.to_date) {
    getListings.to_date = input.to_date;
  }
  if (input && input.day_of_week) {
    getListings.day_of_week = input.day_of_week;
  }
  if (input && input.ticket_type) {
    getListings.ticket_type = input.ticket_type;
  }
  if (input && input.day_of_week) {
    getListings.day_of_week = input.day_of_week;
  }
  if (input && input.category) {//for genre
    getListings.genre = input.category;
  }
  if (input && input.tags) {
    getListings.tags = input.tags;
  }
  if (input && input.in_hand) {
    getListings.in_hand = input.in_hand;
  }
  if (input && input.section) {
    getListings.section = input.section;
  }
  if (input && input.row) {
    getListings.row = input.row;
  }
  if (input && input.quantity) {
    getListings.quantity = input.quantity;
  }
  if (input && input.hold) {
    getListings.hold = input.hold;
  }
  if (input && input.completedEvents) {
    getListings.completedEvents = input.completedEvents;
  }
  if (input && input.inventory_attached) {
    getListings.inventory_attached = input.inventory_attached;
  }

  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($getEventCountInput: GetFilterListings!) {
        getEventCount(input: $getEventCountInput) {
          isError
          countInfo
        }
      }
        `, variables: {
          getEventCountInput:getListings
      }
    });

    return response.data.data.getEventCount;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Getting all listing status  from listing status table
 */
 export async function getAllListingStatus() {
  try {
    let listingStatusResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allListingStatus {
            id
            name
            status
          }
        }
        `
      }
    );
    return listingStatusResponse.data.data.allListingStatus;
  } catch (error) {
    console.log("Error:No records");
  }
}


/**
 * Update Viagogo status
 */
 export async function updateFlagStatus(listGroupId,status) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($updateFlagInput:updateFlagInput!){
          updateFlagStatus(input:$updateFlagInput) {
            isError
            msg
          }
        }`,
      variables: {
        "updateFlagInput": {
          "listingGroupId": listGroupId,
          "status": status,
          
        }
      }
    });
    return response.data.data.updateFlagStatus;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

/**
 * Update Viagogo status
 */
export async function updateViagogoStatus(listingGroupId,listStatus) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($UpdateViagogoInput:updateViagogoInput!){
          updateViagogoStatus(input:$UpdateViagogoInput) {
            isError
            msg
          }
        }`,
      variables: {
        "UpdateViagogoInput": {
          "listingGroupId": listingGroupId,
          "listing_status_id": parseFloat(listStatus),
          
        }
      }
    });
    return response.data.data.updateViagogoStatus;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function createIssueFlag(listingGroupId,issueValues,event_id) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($CreateIssueFlagInput:createIssueFlagInput!){
          createIssueFlag(input:$CreateIssueFlagInput) {
            isError
            msg
          }
        }`,
      variables: {
        "CreateIssueFlagInput": {
          "listingGroupId": listingGroupId,
          "issueValues": issueValues,
          "event_id":event_id
          
        }
      }
    });
    return response.data.data.createIssueFlag;
  } catch (error) {
    console.log("Could not authenticate");
  }
}
/**
 * Getting all issue types from Issue type table
 */
 export async function getAllIssueTypes() {
  try {
    let listingStatusResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allIssueTypes {
            value
            label
            
          }
        }
        `
      }
    );
    return listingStatusResponse.data.data.allIssueTypes;
  } catch (error) {
    console.log("Error:No records");
  }
}

/**
 * To Get Listing Group Details Based On Listing Group Id - TIC-816 Viagogo Hold
 */
export async function getListingGroupDataBasedOnListingGroupId(listingGroupId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($input: getListingGroupDataInput!) {
        getListingGroupDataBasedOnListingGroupId(input: $input) {
          isError
          message
          listingDetails
        }
      }

        `, variables: {
        "input": {
          "listingGroupId": listingGroupId
        }
      }
    });
    return response.data.data.getListingGroupDataBasedOnListingGroupId;
  } catch (error) {
    console.log("Could not get lists");
  }
}


/**
 * Adding listing group data to broker order table - TIC-816
 */
 export async function createViagogoBrokerOrder(listingData) {
  try {
    let response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation($input: viagogoBrokerOrderInput!){
          createViagogoBrokerOrder(input: $input){            
            isError    
            message   
          }
        }
        `, variables: {
        "input": {
          "eventId": listingData.event_id,
          "listingGroupId": listingData.id,
          "channelReferenceId": 1,
          "channelId": 1,
          "loggedInUserId": parseInt(listingData.loggedInUserId), 
          "orderStatusId": 1,
          "deliveryStatusId": parseInt(listingData.delivery_status_id.value),
          "inHandDate": listingData.inhand_date/* ,
          "quantity": listingObject.quantity,
          "price": String(listingObject.price),
          "row": listingObject.row,
          "internal_notes": listingObject.internal_notes,
          "seat_end": parseInt(listingObject.seat_end),
          "seat_start": parseInt(listingObject.seat_start),
          "section": String(listingObject.section),
          "unit_cost": String(listingObject.unit_cost) */
        }
      }
    });
    return response.data.data.createViagogoBrokerOrder;
  } catch (error) {
    console.log("Could not create user", error);
  }
}

/**
 * Getting all delivery status from delivery status table
 */
 export async function getAllDeliveryStatusDetails() {
  try {
    let response = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          getAllDeliveryStatusDetails {
            value
            label 
          }
        }
        `
      }
    );
    return response.data.data.getAllDeliveryStatusDetails;
  } catch (error) {
    console.log("Error:No records");
  }
}