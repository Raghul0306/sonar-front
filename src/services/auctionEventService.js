import axios from "axios";

export async function writeAuctionEventToDB(auctionEvent){
    try {
      const response = await  axios
          .post(process.env.REACT_APP_GRAPHQL_URL, {
            query: `
            mutation CreateAuctionEvent($auctionEvent: AuctionEventInput!){
              createAuctionEvent(input:{
                auctionEvent: $auctionEvent
              }) {
                clientMutationId
                auctionEvent {
                  id
                }
              }
            }
               `,
            variables: {
              auctionEvent: auctionEvent
            }
          })
      return response.data.data.createAuctionEvent.auctionEvent
    } catch (error) {
      console.error(error);
    }
}

export async function getAllAuctionEvents(){
    try {
        let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
          query: `
          {
            allAuctionEvents {
              nodes {
                id
                name
                categoryByCategory{
                  name
                  id
                }
                performerByPerformer{
                  name
                  id
                }
                setBySet{
                  name
                  id
                  year
                  brandByBrandId{
                    name
                    id
                  }
                }
                startDate
                endDate
                minGrade
              }
            }
          }
        `});
        return tokenResponse.data.data.allAuctionEvents.nodes;
      } catch (error) {
        console.log("Could not find Auction Events");
      }
}