import axios from "axios";

/**
 * Will get delivery status using the label ID provided
 * by ShipEngine API upon label creation.
 */
export async function getDeliveryStatusByLabelId(labelId) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        getDeliveryStatusByLabelId(label_id:"${labelId}") {
          tracking_number
          status_code
          status_description
          carrier_status_code
          carrier_status_description
          shipped_date
          estimated_delivery_date
          actual_delivery_date
        }
      }`
    });
    return tokenResponse.data.data.getDeliveryStatusByLabelId;
  }
  catch(error) {
    console.log("Could not find delivery status.");
  }
}
export async function createShippingLabel(seller, buyer, deliveryType, deliverySignatureNeeded) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        GenerateShippingLabel(
          seller: ${seller}
          buyer: ${buyer}
          deliveryType: "${deliveryType}"
          deliverySignature: ${deliverySignatureNeeded === true  ? true : false}
        ) {
          label_id
          label_download {
              png
              pdf
          }
          shipment_cost {
            currency
            amount
          }
        }
      }`
    });
    return tokenResponse.data.data.GenerateShippingLabel;
  }
  catch(error) {
    console.log("Could not generate a shipping label.");
  }
}