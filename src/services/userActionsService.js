import axios from "axios";
import { uploadURLs, folderNames } from "../constants/constants";
/* This list holds all the properties we desire from a listing.
 * This was done so we only have to update 1 place when we want
 * more or less information about a listing. */

const headers = { 'Content-Type': 'multipart/form-data' };


export async function saveUploadListingsFile(userId, attachment_data)
{
  try {
    //Local Storage Starts
    const data = new FormData()
    data.append('file', attachment_data)

    await axios.post(process.env.REACT_APP_FILE_DOWNLOAD+uploadURLs.tempLocalStorageUploadListingsURL, data, {
      //Code Here
    }).then(res => {
      if(res.status == 200)
      {
        const response = axios.post(process.env.REACT_APP_GRAPHQL_URL, {
          query: `
            query($input: csvFileReadingInput!){
              readUploadListingsFile(input: $input){            
                isError    
                msg       
              }
            }
            `, variables: {
            "input": {
              "userId": userId, 
              "filePath": res.data
            }
          }
        });
       
        //return response.data.data;
      }
    })

    //Local Storage Ends

    //S3 Storage Starts

    let curDate = new Date();
    let myDate = curDate.getDate();
    let myMonth = curDate.getMonth() + 1;
    let myYear = curDate.getFullYear();
    let myTime = curDate.getTime();

    let myCurDateTime = myDate + "" + myMonth + "" + myYear + "" + myTime;

    let attachmentfile_name = attachment_data.name ? (myCurDateTime + "_" + attachment_data.name) : '';

    const myS3Data = new FormData();
    myS3Data.append('attachment_file_data', attachment_data);
    myS3Data.append('attachment_file', attachmentfile_name);
    myS3Data.append('folderName1', folderNames.uploadListingsFolder);
    myS3Data.append('folderName2', userId);

    if (attachment_data.name) {
      await axios.post(process.env.REACT_APP_FILE_DOWNLOAD+uploadURLs.imageUploadURL, myS3Data, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
        .then(function (response) {
          return response.data
        })
        .catch(function (response) {
          return false
        });
    }
    //S3 Storage Ends
  } catch (error) {
    console.log("Could not authenticate", error);
  }
}
