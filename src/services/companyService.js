
import axios from "axios";
const headers = { 'Content-Type': 'multipart/form-data' };
let axiosconfig = {
  headers: headers
};

export async function getAllCompanies(userId) {
  try {
    if(userId){
      const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
        query: `
          query($input: allCompaniesInput!){
              getAssignedCompanies(input: $input){
                id
                company_name
                created_at
                updated_at
              }
              } 
               `,
        variables: {
          "input": {
            "userId": userId
          }
        }
      });
      return response.data.data.getAssignedCompanies;
    }else{
      let response = await axios.post(
        process.env.REACT_APP_GRAPHQL_URL,
        {
          query: `
          {
            getAllCompanies {            
              id
              company_name
              parent_company_id
              parentcompanyname
              email
              phone
              address1
              address2
              address1
              zip_code
              country
              state
              city
              created_at
              updated_at
              usercompany
            }
          }`
        }
      );
      return response.data.data.getAllCompanies;
    }
  } catch (error) {
    console.log("Error:No records");
  }
}

export async function saveCompanyNameDetails(inputData, userId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation($input: companyNameInput!){
            saveCompanyDetails(input: $input){
              isError
              message
              companyId
            }
            } 
             `,
      variables: {
        "input": {
          "companyId": inputData.companyId, 
          "parentCompanyId": inputData.parentCompanyId, 
          "companyName": inputData.companyName,
          "phoneNumber": inputData.phoneNumber,
          "email": inputData.email,
          "address1": inputData.address,
          "address2": inputData.address2,
          "zip_code": inputData.zipcode,
          "country": inputData.country,
          "state": inputData.state,
          "city": inputData.city,
          "userId": userId
        }
      }
    });
    return response.data.data.saveCompanyDetails;
  } catch (error) {
    console.error(error);
  }
}

export async function getAllSuperAdminDetails(userId) {
  try {
    let response = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          getAllSuperAdminDetails {            
            id
            first_name
            last_name
            email
            company_id
            created_at
            updated_at
          }
        }`
      }
    );
    return response.data.data.getAllSuperAdminDetails;
  } catch (error) {
    console.log("Error:No records");
  }
}

export async function saveAssignedSuperAdmin(inputData, userId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation($input: assignSuperAdminInput!){
            saveCompanyAssignedSuperAdmin(input: $input){
              isError
              message
              companyId
            }
            } 
             `,
      variables: {
        "input": {
          "userId": userId, 
          "companyId": inputData.companyId, 
          "superAdminId": inputData.superAdminId,
          
        }
      }
    });
    return response.data.data.saveCompanyAssignedSuperAdmin;
  } catch (error) {
    console.error(error);
  }
}

export async function findCompanyEmailExists(companyId, email) {
  try {
    let response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        query ($input: checkCompanyEmailInput!) {
          checkCompanyEmailExists(input: $input) {
            id
            email
          }
        }`, variables: {
        "input": {
          "companyId": parseInt(companyId), 
          "email": email
        }
      }
    });

    return response.data.data.checkCompanyEmailExists;
  } catch (error) {
    console.log("Could not check email exists", error);
  }
}