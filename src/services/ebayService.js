import axios from "axios";

// NOTE: looking at the ebay warehouse for completed items
export async function getEbaySales(brandName, setName, setYear, cardNumber, performerId){
    try {
        let tokenResponse = await axios.post(process.env.REACT_APP_EBAY_WAREHOUSE_GRAPHQL_URL, {
          query: `{
            allEbayCompleteditems {
              nodes {
                itemid
                title
                globalid
                galleryurl
                viewitemurl
                paymentmethod
                autopay
                postalcode
                location
                country
                sellingstatus
                returnsaccepted
                ismultivariationlisting
                topratedlisting
                endtime2
                viewitemurlfornaturalsearch
                listingtype2
                location2
                galleryurl2
                pictureurl
                primarycategoryid
                primarycategoryname
                bidcount
                listingstatus
                timeleft
                title2
                hitcount
                country2
                autopay2
                convertedcurrentpriceCurrencyid
                convertedcurrentpriceValue
                itemspecificsNamevaluelist
                categoryid
                categoryname
                shippingservicecost
                shippingtype
                shiptolocations
                expeditedshipping
                onedayshippingavailable
                handlingtime
                bestofferenabled
                buyitnowavailable2
                starttime
                endtime
                listingtype
                gift
                dbPerformerName
                performerId
                condition
                subtitle
                conditionid
                conditiondisplayname
                globalshipping
                watchcount
                conditiondescription
                buyitnowavailable
                convertedbuyitnowpriceCurrencyid
                convertedbuyitnowpriceValue
                quantityavailablehint
                quantitythreshold
                buyitnowprice
                convertedbuyitnowprice
                charityid
                productid
                gallerypluspictureurl
                discountpriceinfo
                discountpriceinfoOriginalretailpriceCurrencyid
                discountpriceinfoOriginalretailpriceValue
                discountpriceinfoPricingtreatment
                discountpriceinfoSoldonebay
                discountpriceinfoSoldoffebay
                secondarycategory
                quantity
              }
            }
          }`
        });
        return tokenResponse.data.data.allEbayCompleteditems.nodes.filter(ebayItem => {
            if(ebayItem) {
                let itemSpecifics = ebayItem.itemspecificsNamevaluelist;
                // convert the string representing an array of objects to an actual array of objects using regex
                // source: https://stackoverflow.com/a/16214042
                
                // replace single quotes with double quotes (except where they are used as apostrophes)
                var transformedEbayItem = itemSpecifics.replace(/'([:}{,A-Z0-9#\][.])/g, '"$1');
                // catch any uses of single quotes that aren't apostrophes, but also encase lower case letters
                transformedEbayItem = transformedEbayItem.replace(/ '/g, ' "');
                // catch any uses of single quotes being used in arrays
                transformedEbayItem = transformedEbayItem.replace(/\['/g, '["');
                transformedEbayItem = transformedEbayItem.replace(/([^"]+)|("[^"]+")/g, function($0, $1, $2) {
                    if ($1) {
                        return $1.replace(/([a-zA-Z0-9]+?):/g, '"$1":');
                    } else {
                        return $2;
                    }
                });
                if(transformedEbayItem) {
                    transformedEbayItem = JSON.parse(transformedEbayItem);
                    // we will use brand set year and card number to match, so we have to satisfy them all
                    let cardMatch = {
                        brand: false,
                        set: false,
                        year: false,
                        number: false,
                        performerId: ebayItem.performerId === performerId.toString(),
                    }
                    // create the filter for brand, set, year, card number
                    for(let ebayItemCount = 0; ebayItemCount < transformedEbayItem.length; ebayItemCount++) {
                        if (transformedEbayItem[ebayItemCount].Name === "Card Manufacturer") {
                            if(transformedEbayItem[ebayItemCount].Value === brandName) {
                                cardMatch.brand = true;
                            }
                        }
                        // "Brand" might have set or brand
                        else if (transformedEbayItem[ebayItemCount].Name === "Brand") {
                            if(transformedEbayItem[ebayItemCount].Value === brandName) {
                                cardMatch.brand = true;
                            }
                        }
                        if (transformedEbayItem[ebayItemCount].Name === "Series") {
                            if(transformedEbayItem[ebayItemCount].Value === setName) {
                                cardMatch.set = true;
                            }
                        }
                        // "Brand" might have set or brand
                        else if (transformedEbayItem[ebayItemCount].Name === "Brand") {
                            if(transformedEbayItem[ebayItemCount].Value === setName) {
                                cardMatch.set = true;
                            }
                        }
                        if (transformedEbayItem[ebayItemCount].Name === "Year") {
                            if(transformedEbayItem[ebayItemCount].Value === setYear.toString()) {
                                cardMatch.year = true;
                            }
                        }
                        if (transformedEbayItem[ebayItemCount].Name === "Card Number") {
                            if(transformedEbayItem[ebayItemCount].Value === `#${cardNumber.toString()}`) {
                                cardMatch.number = true;
                            }
                        }
                    }
                    // report how many matches we have
                    if(cardMatch.brand && cardMatch.number && cardMatch.performerId) {
                        if(cardMatch.set && cardMatch.year && cardMatch.number && cardMatch.performerId) {
                            //console.info("Found a matching card via 5 factors: brand, set, year, card number, and performer id");
                            return true;
                        }
                        //console.info("Found a matching card via 3 factors: brand, card number, and performer id");
                        return true;
                    }
                }
            }
            return false;
        });
        // now that we have all ebay completed items, let's do some filtering to find out which ones are relevant to us
      } catch (error) {
            console.log(error)
            console.log("Could not find ebay sales data");
      }
}
export async function getEbayItemsByCategoryAndKeyword(categoryId, keyword){
    try {
        console.info(`going to query getEbayItemsByCatAndKey with ${categoryId} and ${keyword}`);
        let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
            query: `{
                GetEbayItemsByCategoryAndKeyword(categoryId:${categoryId}, keyword:"${keyword}") {
                    autoPay
                    charityId
                    compatibility
                    condition
                    country
                    discountPriceInfo
                    distance
                    eBayPlusEnabled
                    galleryInfoContainer
                    galleryPlusPictureURL
                    galleryURL
                    globalId
                    isMultiVariationListing
                    itemId
                    listingInfo
                    location
                    paymentMethod
                    pictureURLLarge
                    pictureURLSuperSize
                    postalCode
                    primaryCategory
                    productId
                    returnsAccepted
                    secondaryCategory
                    sellerInfo
                    sellingStatus
                    shippingInfo
                    storeInfo
                    subtitle
                    title
                    topRatedListing
                    unitPrice
                    viewItemURL
                }
            }`
        });
        console.info(`Query successful! check out the data:`);
        console.info(tokenResponse.data.data.GetEbayItemsByCategoryAndKeyword);
        return tokenResponse.data.data.GetEbayItemsByCategoryAndKeyword;
        // now that we have all ebay completed items, let's do some filtering to find out which ones are relevant to us
      } catch (error) {
            console.log(error)
            console.log("Could not find ebay sales data");
      }
}

/// =======================
/// AUTHORIZATION FUNCTIONS
/// =======================

/**
 * STEP ONE
 * --------
 * Will initialize a connection with the Ebay API by beginning a handshake
 * process. You must first declare WHO you are. This will happen on the
 * backed, as InitEbayConnection will be called before any auth functions. */

/**
 * STEP TWO
 * --------
 * After initEbayConnection() is run, this will return a URL that can be used
 * to ensure the user authorizes MyHobby to post on Ebay on their behalf.
 */
export async function getEbayAuthorizationURL(clientId, clientSecret, redirectUri) {
    try {
        let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
          query: `{
            GetEbayAuthURL(
                clientId: "${clientId}",
                clientSecret: "${clientSecret}",
                redirectUri: "${redirectUri}",
                environment: "${process.env.REACT_APP_EBAY_ENVIRONMENT}"
            ) 
          }`
        });
        return tokenResponse.data.data.GetEbayAuthURL;
    } catch (error) {
        console.log(error)
        console.log("Could not get ebay authorization URL");
    }
}

/**
 * STEP THREE
 * ----------
 * After terms are accepted on the page that getEbayAuthorizationURL
 * links to, ebay returns an "auth code" (which is passed in here).
 * This function returns an "ebay auth token object" that is given back from
 * Ebay in exchange for that "auth code".
 */
export async function getEbayTokenFromAuthCode(clientId, clientSecret, redirectUri, authCode) {
    try {
        let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
          query: `{
            GetEbayTokenFromAuthCode(
                clientId: "${clientId}"
                clientSecret: "${clientSecret}"
                redirectUri: "${redirectUri}"
                authCode: "${authCode}"
                environment: "${process.env.REACT_APP_EBAY_ENVIRONMENT}"
            ) {
                access_token
                expires_in
                refresh_token
                refresh_token_expires_in
                token_type
            }
          }`
        });
        return tokenResponse.data.data.GetEbayTokenFromAuthCode;
    } catch (error) {
        console.log(error)
        console.log("Could not get ebay auth token from auth code");
    }
}

/**
 * Will update an ebay user authorization token object, given an
 * access token. This should only be used when the access token
 * Ebay has returned is expired.
 * @param {*} accessToken 
 */
export async function updateExpiredEbayAccessToken(clientId, clientSecret, redirectUri, refreshToken) {
    try {
        let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
            query: `{
            UpdateExpiredEbayAccessToken(
                clientId: "${clientId}"
                clientSecret: "${clientSecret}"
                redirectUri: "${redirectUri}"
                refresh_token: "${refreshToken}"
                environment: "${process.env.REACT_APP_EBAY_ENVIRONMENT}"
            ) {
                access_token
                expires_in
                token_type
            }
        }`
        });
        return tokenResponse.data.data.UpdateExpiredEbayAccessToken;
    } catch (error) {
        console.log(error)
        console.log("Could not get ebay auth token from refresh token");
    }
}

/** Will generate an ebay order from a listing */
export async function generateEbayOrderFromListing(accessToken, listing, merchantLocationKey, fulfillmentPolicyId, returnPolicyId, paymentPolicyId) {
    try {
        let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
          query: `{
            GenerateEbayOrder(
                accessToken: "${accessToken}"
                listing: ${JSON.stringify(listing).replace(/"([^"]+)":/g, '$1:')}
                merchantLocationKey: "${merchantLocationKey}"
                fulfillmentPolicyId: "${fulfillmentPolicyId}"
                returnPolicyId: "${returnPolicyId}"
                paymentPolicyId: "${paymentPolicyId}"
            ) {
                listingId
                offerId
            }
          }`
        });
        return tokenResponse.data.data.GenerateEbayOrder;
    } catch (error) {
        console.log(error)
        console.log("Could not generate the ebay order");
    }
}

/** Will withdraw an ebay order by order ID */
export async function withdrawEbayOffer(accessToken, offerId) {
    try {
        let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
          query: `{
            WithdrawEbayOffer(
                accessToken: "${accessToken}"
                offerId: "${offerId}"
            ) {
                listingId
            }
          }`
        });
        return tokenResponse.data.data.WithdrawEbayOffer;
    } catch (error) {
        console.log(error)
        console.log("Could not withdraw the ebay order");
    }
}

/** Will publish an ebay order by order ID */
export async function publishEbayOffer(accessToken, offerId) {
    try {
        let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
          query: `{
            PublishEbayOffer(
                accessToken: "${accessToken}"
                offerId: "${offerId}"
            ) {
                listingId
            }
          }`
        });
        return tokenResponse.data.data.PublishEbayOffer;
    } catch (error) {
        console.log(error)
        console.log("Could not publish the ebay order");
    }
}

/* Will generate an inventory location, necessary for publishing an ebay offer */
export async function createInventoryLocation(accessToken, user) {
    try {
        let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
          query: `{
            CreateInventoryLocation(
                accessToken: "${accessToken}"
                firstName: "${user.firstName}"
                lastName: "${user.lastName}"
                city: "${user.city}"
                state: "${user.state}"
                postalCode: "${user.postalCode}"
                country: "${user.country}"
            )
          }`
        });
        return tokenResponse.data.data.CreateInventoryLocation;
    } catch (error) {
        console.log(error)
        console.log("Could not generate warehouse location");
    }
}

export async function setDefaultReturnPolicy(accessToken) {
    try {
        let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
          query: `{
            SetDefaultReturnPolicy(
                accessToken: "${accessToken}"
            )
          }`
        });
        return tokenResponse.data.data.SetDefaultReturnPolicy;
    } catch (error) {
        console.log(error)
        console.log("Could not set default return policy");
    }
}
export async function setDefaultFulfillmentPolicy(accessToken) {
    try {
        let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
          query: `{
            SetDefaultFulfillmentPolicy(
                accessToken: "${accessToken}"
            )
          }`
        });
        return tokenResponse.data.data.SetDefaultFulfillmentPolicy;
    } catch (error) {
        console.log(error)
        console.log("Could not set default fulfillment policy");
    }
}
export async function setDefaultPaymentPolicy(accessToken) {
    try {
        let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
          query: `{
            SetDefaultPaymentPolicy(
                accessToken: "${accessToken}"
            )
          }`
        });
        return tokenResponse.data.data.SetDefaultPaymentPolicy;
    } catch (error) {
        console.log(error)
        console.log("Could not set default payment policy");
    }
}