import axios from "axios";

/*
* Operation Listing and Filtering the events with related tables 
*
*/

export async function getOperationFilterListings(userId, input) {

  var getListings = {};
  if (userId > 0) {
    getListings.userId = userId
  }
  if (input && input.tagIds) {
    getListings.tagIds = input.tagIds;
  }
  if (input && input.searches) {
    getListings.searches = input.searches;
  }
  if (input && input.from_date) {
    getListings.from_date = input.from_date;
  }
  if (input && input.to_date) {
    getListings.to_date = input.to_date;
  }
  if (input && input.day_of_week) {
    // getListings.day_of_week = input.day_of_week;
  }
  if (input && input.ticket_type) {
    getListings.ticket_type = input.ticket_type;
  }
  if (input && input.category) {//for genre
    getListings.genre = input.category;
  }
  if (input && input.tags) {
    getListings.tags = input.tags;
  }
  if (input && input.in_hand) {
    getListings.in_hand = input.in_hand;
  }
  if (input && input.section) {
    getListings.section = input.section;
  }
  if (input && input.row) {
    getListings.row = input.row;
  }
  if (input && input.quantity) {
    getListings.quantity = input.quantity;
  }
  if (input && input.hold) {
    getListings.hold = input.hold;
  }
  if (input && input.inventory_attached) {
    getListings.inventory_attached = input.inventory_attached;
  }
  if (input && input.currentPage) {
    getListings.page = input.currentPage;
  }
  if (input && input.pageLimit) {
    getListings.limit = input.pageLimit;
  }
  if (input && input.purchaseDate) {
    getListings.purchaseDate = input.purchaseDate;
  }
  if (input && input.split) {
    getListings.split = input.split;
  }
  if (input && input.company) {
    getListings.company = input.company;
  }
  if (input && input.userAssignedTo) {
    getListings.userAssignedTo = input.userAssignedTo;
  }
  if (input && input.ticketingSystemAccount) {
    getListings.ticketingSystemAccount = input.ticketingSystemAccount;
  }

  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($getListings: GetFilterListings!) {
        operationFilterListings(input: $getListings) {
          isError
          msg
          data
          totalEventCount
        }
      }
        `, variables: {
        getListings
      }
    });

    return response.data.data.operationFilterListings;
  } catch (error) {
    console.log("Could not get lists");
  }

}

export async function removeQCHoldStatus(listingGroupId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: ` 
      query($removeListing: removeListing!){
        changeQCStatus(input: $removeListing){            
          isError    
          msg          
        }
      }`,
      variables: {
        "removeListing": {
          "listingGroupId": [listingGroupId]
        }
      }
    });
    // change qc status of given listing 
    let allListings = response.data.data.changeQCStatus;
    return allListings;
  } catch (error) {
    console.log("Could not get listing by ID");
  }
}

export async function updateCostForListings(listingGroupId, unitCost) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($inputData:updateUnitCostInput!){
          updateUnitCost(input:$inputData) {
            isError    
            msg
          }
        }`,
      variables: {
        "inputData": {
          "listingGroupId": listingGroupId,
          "unitCost": unitCost
        }
      }
    });
    return response.data.data.updateUnitCost;

  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function bulkUpdateNotesAndTags(listingGroupId, internalNotes, tags) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($inputData:bulkUpdateNotesAndTagsInput!){
          bulkUpdateNotesAndTags(input:$inputData) {
            isError    
            msg
          }
        }`,
      variables: {
        "inputData": {
          "listingGroupId": listingGroupId,
          "internalNotes": internalNotes,
          "selectedTags":tags
        }
      }
    });
    return response.data.data.bulkUpdateNotesAndTags;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateDisclosureInBulk( listingGroupId, disclosures, externalNotes ) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($inputData:updateDisclosureInBulkInput!){
          updateDisclosureInBulk(input:$inputData) {
            isError    
            msg
          }
        }`,
      variables: {
        "inputData": {
          "listingGroupId": listingGroupId,
          "disclosures": disclosures,
          "externalNotes": externalNotes
        }
      }
    });
    return response.data.data.updateDisclosureInBulk;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateSplitsInBulk( listingGroupId, splitId ) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation ($inputData:updateSplitsInBulkInput!){
          updateSplitsInBulk(input:$inputData) {
            isError    
            msg
          }
        }`,
      variables: {
        "inputData": {
          "listingGroupId": listingGroupId,
          "splitId": parseInt(splitId)
        }
      }
    });
    return response.data.data.updateSplitsInBulk;
  } catch (error) {
    console.log("Could not authenticate");
  }
}
export async function updateOperationListingFromGridCell( listingGroupId, columnName, input ) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        query ($inputData:updateListingFromGridInput!){
          updateListingFromGrid(input:$inputData) {
            isError    
            msg
          }
        }`,
      variables: {
        "inputData": {
          "listingGroupId": listingGroupId,
          "columnName": columnName,
          "section": input.section || "",
          "row": input.row || "",
          "owner": input.owner || "",
          "ticketingSystemAccount": input.ticketingSystemAccount || "",
          "externalReference": input.externalReference || "",
          "inHandDate": input.inHandDate || "",
          "purchaseOrderDate": input.purchaseOrderDate || "",
          "last4Digits": input.last4Digits || "",
          "seatStart": input.seatStart || 0,
          "seatEnd": input.seatEnd || 0,
          "quantity": input.quantity || 0
        }
      }
    });
    return response.data.data.updateListingFromGrid;
  } catch (error) {
    console.log("Could not authenticate");
  }
}
