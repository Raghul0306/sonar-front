import { folderNames, uploadURLs, downloadURLs } from "../constants/constants";
import axios from "axios";
import uniqBy from 'lodash/uniqBy';

export async function getEventsReportData() {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          {
            allEvents{
              nodes{
                posName
                id
                date
                performerByPerformer{
                  name
                  abv
                }
                performerByOpponent{
                  name
                  abv
                }
                categoryByCategory{
                  name
                }
                eventtypeByEventType{
                  name
                }
                listingsByEventId{
                    nodes{
                      userBySellerId{
                        fullName
                      }
                      section
                      row
                      cost
                      sold
                      price
                    }
                    totalCount
                  }
              }
            }
          }
               `,
      variables: {

      }
    });
    return tokenResponse.data.data.allEvents.nodes;
  } catch (error) {
    console.log("Could not find listings");
  }
}

export async function getEventsByPerformer() {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          allPerformers{
            nodes{
              name
              eventsByPerformer{
                nodes{
                  posName
                  listingsByEventId{
                    nodes{
                      userBySellerId{
                        fullName
                      }
                      section
                      row
                      cost
                      price
                      eventByEventId{
                        posName
                      }
                    }
                  }
                }
              }
            }
          }
        }
             `,
      variables: {

      }
    });
    return tokenResponse.data.data.allPerformers.nodes;
  } catch (error) {
    console.log("Could not find listings");
  }
}

export function sortIntoBroker(events) {
  let brokerArray = [];
  events.forEach(performer => {
    performer.eventsByPerformer.nodes.forEach(event => {
      event.listingsByEventId.nodes.forEach(listing => {
        brokerArray.push(listing)
      })
    })
  })

  let topBrokers = uniqBy(brokerArray, function (x) { return x.userBySellerId.fullName });
  let brokers = [];
  topBrokers.forEach(broker => {
    brokers.push({
      name: broker.userBySellerId.fullName,
      listings: brokerArray.filter(listing => listing.userBySellerId.fullName === broker.userBySellerId.fullName).length
    })
  })
  brokers.sort((a, b) => (a.listings < b.listings) ? 1 : -1)
  return brokers.slice(0, 5)

  //Group this array into a row group based on the Broker, then the event.
}

export function calcPotentialRev(listing) {
  return ((listing.price / listing.cost) * 100).toFixed() + "%"
}

export async function addEvents(eventsObject, userDetails) {
  try {
    let listResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation($addEventInput: addEventInput!){
          addEvent(input: $addEventInput){            
            isError    
            msg    
            attachmentData      
          }
        }
        `, variables: {
        "addEventInput": {
          "eventName": eventsObject.eventName,
          "eventStatus": eventsObject.eventStatus,
          "eventDate": eventsObject.eventDate,
          "performer": eventsObject.performer,
          "eventTime": eventsObject.eventTime,
          "opponent": eventsObject.opponent,
          "state": eventsObject.state,
          "city": eventsObject.city,
          "country": parseInt(eventsObject.country),
          "ticketingSystem": eventsObject.ticketingSystem,
          "category": eventsObject.category,
          "genre": eventsObject.genre,
          "venueAddress": eventsObject.venueAddress,
          "eventLink": eventsObject.eventLink,
          "venueAddress2": eventsObject.venueAddress2,
          "venueName": eventsObject.venueName,
          "zip_code": eventsObject.zip_code,
          "userDetails": userDetails,
          "stateName": eventsObject.stateName,
          "cityName": eventsObject.cityName,
        }
      }
    });
    return listResponse.data.data.addEvent;
  } catch (error) {
    console.log("Could not create user", error);
  }

}

export async function updateEvents(eventsObject, userDetails) {
  try {
    let listResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation($updateEventInput: updateEventInput!){
          updateEvent(input: $updateEventInput){            
            isError    
            msg    
            attachmentData      
          }
        }
        `, variables: {
        "updateEventInput": {
          "eventId": eventsObject.eventId,
          "eventName": eventsObject.eventName,
          "eventDate": eventsObject.eventDate,
          "eventTime": eventsObject.eventTime,
          "eventStatus": eventsObject.eventStatus,
          "venueId": eventsObject.venueId,
          "venueName": eventsObject.venueName,
          "venueAddress": eventsObject.venueAddress,
          "venueAddress2": eventsObject.venueAddress2,
          "venueCity": eventsObject.venueCity,
          "venueZipcode": eventsObject.venueZipcode,
          "venueState": eventsObject.venueState,
          "venueCountry": eventsObject.venueCountry,
          "ticketSystemId": eventsObject.ticketSystemId,
          "category": eventsObject.category,
          "genre": eventsObject.genre,
          "eventLink": eventsObject.eventLink,
          "eventImagelink": eventsObject.eventImagelink,
          "userDetails": userDetails,
          "stateName": eventsObject.stateName,
          "cityName": eventsObject.cityName,
        }
      }
    });
    return listResponse.data.data.updateEvent;
  } catch (error) {
    console.log("Could not update event", error);
  }

}

export async function getAllEventStatus() {
  try {
    let response = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `{
          allEventStatus {
              id
              event_status
          }
        }`
      }
    );

    return response.data.data.allEventStatus;
  } catch (error) {
    console.log("Could not retrieve Event Status from DB", error);
  }
}

export async function venueSearchFilter(input) {
  var getListings = {};
  if (input && input.searches) {
    getListings.searches = input.searches;
  }

  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($GetVenueSearchFilterInput: GetVenueSearchFilterInput!) {
        venueSearchFilter(input: $GetVenueSearchFilterInput) 
      }
        `, variables: {
        "GetVenueSearchFilterInput": {
          "searches": input.searches,
          "type": input.type
        }
      }
    });

    return response.data.data.venueSearchFilter;
  } catch (error) {
    console.log("Could not get lists");
  }
}

export async function getEventFilter(searchText) {

  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($filterEventDetailsInput: filterEventDetailsInput!) {
        getEventFilter(input: $filterEventDetailsInput) {
          data
        }
      }
        `, variables: {
        "filterEventDetailsInput": {
          "searchText": searchText
        }
      }
    });

    return response.data.data.getEventFilter;
  } catch (error) {
    console.log("Could not get lists");
  }
}

export async function getEventdetail(eventId) {

  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($getEventDetailsInput: getEventDetailsInput!) {
        getEventDetail(input: $getEventDetailsInput) {
        data
        }
      }
        `, variables: {
        "getEventDetailsInput": {
          "eventIds": eventId
        }
      }
    });

    return response.data.data.getEventDetail;
  } catch (error) {
    console.log("Could not get lists");
  }

}

export async function getVenueFilter(input) {
  var getListings = {};
  if (input && input.searches) {
    getListings.searches = input.searches;
  }

  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($GetVenueFilterInput: GetVenueFilterInput!) {
        getVenueFilter(input: $GetVenueFilterInput) {
          id
          name
          address
          address2
          city
          state
          country
          zip_code
        }
      }
        `, variables: {
        "GetVenueFilterInput": {
          "searches": input.searches
        }
      }
    });

    return response.data.data.getVenueFilter[0];
  } catch (error) {
    console.log("Could not get lists");
  }
}

export async function imageUpload(eventId, image, imageName) {
  try {
    const formData = new FormData();
    formData.append('attachment_file_data', image);
    formData.append('attachment_file', imageName);
    formData.append('folderName1', folderNames.eventsFolder);
    formData.append('folderName2', eventId);

    if (imageName) {
      axios.post(process.env.REACT_APP_FILE_DOWNLOAD + uploadURLs.imageUploadURL, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
        .then(function (response) {
          return response.data
        })
        .catch(function (response) {
          return false
        });
    }
    return true;
  } catch (error) {
    console.log("Could not create user", error);
  }

}

export async function viewEventAttachment(eventId, fileName) {
  try {
    const response = await axios({
      method: 'post',
      url: process.env.REACT_APP_FILE_DOWNLOAD + downloadURLs.viewEventAttachment,
      data: { eventId, fileName }
    })
    return response;
  } catch (error) {
    console.log("Could not get lists");
  }
}

export async function updateEventStatus(eventId, eventStatusId) {
  try {
    let listResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation($updateEventStatusInput: updateEventStatusInput!){
          updateEventStatus(input: $updateEventStatusInput){            
            isError    
            msg    
            data      
          }
        }
        `, variables: {
        "updateEventStatusInput": {
          "eventId": eventId,
          "eventStatusId": eventStatusId.value
        }
      }
    });
    return listResponse.data.data.updateEventStatus;
  } catch (error) {
    console.log("Could not update event", error);
  }

}