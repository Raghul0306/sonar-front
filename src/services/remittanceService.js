import axios from "axios";

export async function getRemittanceData(stripe_user_id) {
  try {
	const date = Date.now(); //Placeholder. this will be a passed in argument. 
    const response = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
			listStripePayouts(arrival_date:"${date}", limit:100){
			  id
			  amount
			  arrival_date
			  created
			}
		}
          
        `
      }
    );
    return response.data.data.listStripePayouts;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getFutureRemittanceData(stripe_user_id) {
	try {
	  const date = Date.now(); //Placeholder. this will be a passed in argument. 
	  const response = await axios.post(
		process.env.REACT_APP_GRAPHQL_URL,
		{
		  query: `
		  {
			  listFutureStripePayouts(arrival_date:"${date}", limit:1){
				id
				amount
				arrival_date
				created
			  }
		  }
			
		  `
		}
	  );
	  return response.data.data.listStripePayouts;
	} catch (error) {
	  console.log("Could not authenticate");
	}
  }
  
