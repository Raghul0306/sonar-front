import axios from "axios";
export async function getToken(user) {
  
  let loginput = {};
  if(user.email){
    loginput.login = user.email;
  }
  if(user.password){
    loginput.password = user.password;
  }
  if(user.verificationToken){
    loginput.verificationToken = user.verificationToken;
  }

  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      mutation($loginput: Loginput!){
        userLogin(input: $loginput){
              id
              token
        }
      } `,
      variables: {
          "loginput": loginput
      }
    });
    return tokenResponse.data.data.userLogin;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getUser(user) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query($getuserinput: Getuserinput!) {
            getuser(input: $getuserinput){
            firstName
            id
            lastName
            fullName
            companyName
            email
            phone
            userRole
            userRoleName
            stripeId
            streetAddress
            streetAddress2
            postalCode
            city
            state
            country
            needsToAcceptTerms
            needsToAcceptPrivacy
            needsToChangePassword
            needsToAcceptSeller
            }
          }
               `,
        variables: {
            "getuserinput": {
              "id": user
            }
      }
    });
    return tokenResponse.data.data.getuser;
  } catch (error) {
    console.log("Could not find user");
  }
}

export async function getCount(user) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query allListings($userID: Int!) {
        allListings(condition: { sellerId: $userID }) {
         totalCount
        }
      }
        `,
      variables: {
        userID: user.id
      }
    });
    return userResponse.data.data.allListings.totalCount;
  } catch (error) {
    console.log("Could not authenticate");
  }
}
