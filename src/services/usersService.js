import axios from "axios";

/**
      * Page : home
      * Function For : Function to fetch the broker list
      * Ticket No : TIC-171
      * TO DO : Need to implement the dsahboard data changes based on the broker selected
  */

export async function getBrokerUsers() {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        getBrokerUsers{            
          broker_list    
        }
      }`
    });
    return response.data.data.getBrokerUsers;
  } catch (error) {
    console.log("Could not update listing notes");
  }
}

export async function sendSupportMail(subject, body) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `query ($sendSupportMail: getMailData!){
        sendSupportMail(input: $sendSupportMail){            
          status
          userInfo
        }
      }`,
      variables: {
        "sendSupportMail": {
          "subject": subject,
          "message": body
        }
      }
    });
    return response.data.data.sendSupportMail;
  } catch (error) {
    console.log("Could not update listing notes");
  }
}


export async function brokerSupportMail(subject, body, from) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `query ($brokerSupportMail: getMailDataBroker!){
        brokerSupportMail(input: $brokerSupportMail){            
          status
        }
      }`,
      variables: {
        "brokerSupportMail": {
          "subject": subject,
          "message": body,
          "from": from
        }
      }
    });
    return response.data.data.brokerSupportMail;
  } catch (error) {
    console.log("Could not send the mail");
  }
}


export async function getAllUsers(user) {
  try {
    let userResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allUsers {            
              id
              first_name
              last_name
              company_name
              email
              phone
              user_role
              last_login
                        
          }
        }`
      }// zip_code last_login 
    );
    return userResponse.data.data.allUsers;
  } catch (error) {
    console.log("Could not authenticate");
  }
}
export async function getAllUsersByRole(user) {
  try {
    let userResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        
          query($getuserrole: Getuserroleinput!) {
            allRoleUsers(input: $getuserrole) {            
            id
            first_name
            last_name
            company_name
            email
            phone
            user_role
            notes
            last_login
          }
        }
      
        `, variables: {
          "getuserrole": {
            "id": user
          }
        }
      }
    );
    return userResponse.data.data.allRoleUsers;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function getBuyerUsers() {
  try {
    let userResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `{
          allBuyers {
              id
              first_name
              last_name
              email
              phone_number
              address1
              address2
              zip_code
              country
              state
              notes
              city
              transactionsByBuyerId
          }
        }`
      }
    );
    return userResponse.data.data.allBuyers;
  } catch (error) {
    console.log("Could not retrieve buyers from DB");
  }
}
