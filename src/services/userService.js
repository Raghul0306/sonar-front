import axios from "axios";

export async function updateUser(user) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {

      query: `
      mutation($updateUserByIdinput: updateUserByIdinput!){
        updateUserById(input: $updateUserByIdinput){
          id, 
          firstName, 
          lastName, 
          email,
          city,
          companyName,
          fullName,
          country,
          phone,
          postalCode
          state,
          streetAddress,
          streetAddress2,
          needsToAcceptTerms
          needsToAcceptPrivacy
          needsToChangePassword
          needsToSyncBank
          message
        }
      } `,
      variables: {

        "updateUserByIdinput": {
          "id": user.id,
          "firstName": user.firstName,
          "lastName": user.lastName,
          "email": user.email,
          "city": user.city,
          "companyName": user.companyName,
          "country": user.country,
          "phone": user.phone,
          "postalCode": user.postalCode,
          "state": user.state,
          "streetAddress": user.streetAddress,
          "streetAddress2": user.streetAddress2
        }

      }
    });
    return userResponse.data.data.updateUserById;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateUserFirstName(id, name) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateUserByUser($id: Int!, $name: String!){
            updateUserById(input:{
              id: $id,
              userPatch: {
                firstName: $name
              }
            }) {
              clientMutationId
              user{
                firstName
                id
                lastName
                fullName
                phone
                email
                about
                userRole
              }
            }
          }
             `,
      variables: {
        id: id,
        name: name
      }
    });
    return userResponse.data.data.updateUserById.user;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateUserLastName(id, name) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateUserByUser($id: Int!, $name: String!){
            updateUserById(input:{
              id: $id,
              userPatch: {
                lastName: $name
              }
            }) {
              clientMutationId
              user{
                firstName
                id
                lastName
                fullName
                phone
                email
                about
                userRole
              }
            }
          }
             `,
      variables: {
        id: id,
        name: name
      }
    });
    return userResponse.data.data.updateUserById.user;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateUserStripeID(id, stripeId) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
        query: `query ($updateStripId: getBankInfo!){
          updateStripId(input: $updateStripId){
            status        
            userInfo
          }
        }`,
        variables: {
          "updateStripId": {
            "id": id,
            "stripeId": stripeId
          }
        }
    });
    return userResponse.data.data.updateStripId;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateUserEbayAccessAndRefreshToken(
  id,
  accessToken,
  refreshToken
) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateUserByUser($id: Int!, $accessToken: String!, $refreshToken: String!){
            updateUserById(input:{
              id: $id,
              userPatch: {
                ebayAccessToken: $accessToken
                ebayRefreshToken: $refreshToken
              }
            }) {
              clientMutationId
              user{
                firstName
                id
                lastName
                companyName
                fullName
                phone
                email
                about
                userRole
                stripeId
                streetAddress
                streetAddress2
                city
                state
                country
                zipCode
                
              }
            }
          }
             `,
      /*postalCode
         ebayClientId
         ebayClientSecret
         ebayRedirectUri
         ebayAccessToken
         ebayRefreshToken*/
      variables: {
        id: id,
        accessToken: accessToken,
        refreshToken: refreshToken
      }
    });
    return userResponse.data.data.updateUserById.user;
  } catch (error) {
    console.log(
      "Could not record user ebay access & refresh token to database"
    );
  }
}

export async function updateUserEbayAccessToken(id, accessToken) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateUserByUser($id: Int!, $accessToken: String!){
            updateUserById(input:{
              id: $id,
              userPatch: {
                ebayAccessToken: $accessToken
              }
            }) {
              clientMutationId
              user{
                firstName
                id
                lastName
                companyName
                fullName
                phone
                email
                about
                userRole
                stripeId
                streetAddress
                streetAddress2
                city
                state
                country
                zipCode
               
              }
            }
          }
             `,
      /* postalCode
         ebayClientId
         ebayClientSecret
         ebayRedirectUri
         ebayAccessToken
         ebayRefreshToken
         ebayFulfillmentPolicyId
         ebayReturnPolicyId
         ebayPaymentPolicyId
         ebayActive*/
      variables: {
        id: id,
        accessToken: accessToken
      }
    });
    return userResponse.data.data.updateUserById.user;
  } catch (error) {
    console.log("Could not record user ebay access token to database");
  }
}

export async function updateUserNotes(id, notes) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateUserByUser($id: Int!, $notes: String!){
            updateUserById(input:{
              id: $id,
              userPatch: {
                about: $notes
              }
            }) {
              clientMutationId
              user{
                firstName
                id
                lastName
                fullName
                phone
                email
                about
                userRole
                stripeId
              }
            }
          }
             `,
      variables: {
        id: id,
        notes: notes
      }
    });
    return userResponse.data.data.updateUserById.user;
  } catch (error) {
    console.log("Could not update user's notes");
  }
}

export async function updateBuyerNotes(id, notes) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateBuyerByBuyer($id: Int!, $notes: String!){
            updateBuyerById(input:{
              id: $id,
              buyerPatch: {
                notes: $notes
              }
            }) {
              clientMutationId
              buyer {
                firstName
                id
                lastName
                phone
                email
                notes
                userRole
              }
            }
          }
             `,
      variables: {
        id: id,
        notes: notes
      }
    });
    return userResponse.data.data.updateBuyerById.buyer;
  } catch (error) {
    console.log("Could not update buyer's notes");
  }
}

export async function updateUserNeedsToChangePassword(id) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation updateUserByUser($id: Int!){
            updateUserById(input:{
              id: $id,
              userPatch: {
                needsToChangePassword: false
              }
            }) {
              clientMutationId
              user{
                firstName
                id
                companyName
                lastName
                fullName
                phone
                email
                about
                userRole
                needsToChangePassword
                needsToAcceptTerms
                needsToAcceptPrivacy
                needsToAcceptSeller
                stripeId
              }
            }
          }
             `,
      variables: {
        id: id
      }
    });
    return userResponse.data.data.updateUserById.user;
  } catch (error) {
    console.log("Could not update user's notes");
  }
}

export async function getTokenUser(verifyToken) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `query ($getUserDataInput: getUserData!) {
        verifyUserToken(input: $getUserDataInput) {
          status
          token_user
        }
      }`,
      variables: {
        "getUserDataInput": {
          "token": verifyToken
        }
      }
    });
    return response.data.data.verifyUserToken;
    
  } catch (error) {
    console.log("Could not verify token");
  }
}

export async function updateUserNeedsToAcceptTerms(id) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      mutation($Getuserinput: Getuserinput!){
        updateUserNeedsToAcceptTerms(input: $Getuserinput){
        id
        firstName
        lastName
        fullName
        email
        phone
        userRole
        userRoleName
        stripeId
        last_login
        companyName
        streetAddress
        streetAddress2
        city
        state
        postalCode
        country
        needsToChangePassword
        needsToAcceptTerms
        needsToAcceptPrivacy
        needsToAcceptSeller
        needsToSyncBank
        message
          
        }
      }
             `,
      variables: {
        "Getuserinput": {
          "id": id
        }
      }
    });
    return userResponse.data.data.updateUserNeedsToAcceptTerms;
  } catch (error) {
    console.log("Could not update user's termAgreement");
  }
}

export async function updateUserNeedsToAcceptSeller(id) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      mutation($Getuserinput: Getuserinput!){
        updateUserNeedsToAcceptSeller(input: $Getuserinput){
        id
        firstName
        lastName
        fullName
        email
        phone
        userRole
        userRoleName
        stripeId
        last_login
        companyName
        streetAddress
        streetAddress2
        city
        state
        postalCode
        country
        needsToChangePassword
        needsToAcceptTerms
        needsToAcceptPrivacy
        needsToAcceptSeller
        needsToSyncBank
        message
          
        }
      }
             `,
      variables: {
        "Getuserinput": {
          "id": id
        }
      }
    });
    return userResponse.data.data.updateUserNeedsToAcceptSeller;
  } catch (error) {
    console.log("Could not update user's Seller Agreement");
  }
}



export async function updateUserNeedsToAcceptPrivacy(id) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      mutation($Getuserinput: Getuserinput!){
        updateUserNeedsToAcceptPrivacy(input: $Getuserinput){
        id
        firstName
        lastName
        fullName
        email
        phone
        userRole
        userRoleName
        stripeId
        last_login
        companyName
        streetAddress
        streetAddress2
        city
        state
        postalCode
        country
        needsToChangePassword
        needsToAcceptTerms
        needsToAcceptPrivacy
        needsToAcceptSeller
        needsToSyncBank
        message
          
        }
      }
             `,
      variables: {
        "Getuserinput": {
          "id": id
        }
      }
    });
    return userResponse.data.data.updateUserNeedsToAcceptPrivacy;
  } catch (error) {
    console.log("Could not update user's termAgreement");
  }
}
export async function getUserById(userID) {
  try {


    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query($getuserinput: Getuserinput!) {
            getuser(input: $getuserinput){
              firstName
              id
              lastName
              fullName
              companyName
              email
              phone
              userRole
              stripeId
              streetAddress
              streetAddress2
              city
              state
              postalCode
              country
              needsToAcceptTerms
              needsToAcceptPrivacy
              needsToAcceptSeller
              needsToChangePassword
            }
          }
               `,
      variables: {
        "getuserinput": {
          "id": userID
        }
      }
    });

    return userResponse.data.data.getuser;
  } catch (error) {
    console.log("Could not authenticate");
  }
}
export async function getCountries() {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      {
        allCountries {            
         id
         name
        }
      }
        `
    });

    return userResponse.data.data.allCountries;
  } catch (error) {
    console.log("Could not find country", error);
  }
}
export async function getCities(country_id, state_id) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($cityInput:cityInput!){
        allCities(input:$cityInput) {
          id    
          name
        }
      }
        `,
      variables: {
        "cityInput": {
        "country_id": country_id,
        "state_id": state_id
        }
      }
    });

    return userResponse.data.data.allCities;
  } catch (error) {
    console.log("Could not find country", error);
  }
}

export async function getStates(country_id) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($stateInput:stateInput!){
        allStates(input:$stateInput) {
        id    
        name
        abbreviation
        }
      }
      `,
      variables: {
        "stateInput": {
        "country_id": country_id
        }
      }
    });

    return userResponse.data.data.allStates;
  } catch (error) {
    console.log("Could not find country", error);
  }
}


export async function getCountry(country_id) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($getCountryInput:getCountryInput!){
        getCountry(input:$getCountryInput) {
        id
        name
        }
      }
      `,
      variables: {
        "getCountryInput": {
        "id": country_id
        }
      }
    });

    return userResponse.data.data.getCountry;
  } catch (error) {
    console.log("Could not find country", error);
  }
}

export async function getCity(city_id) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($getCityInput:getCityInput!){
        getCity(input:$getCityInput) {
        id
        name
        }
      }
      `,
      variables: {
        "getCityInput": {
        "id": city_id
        }
      }
    });

    return userResponse.data.data.getCity;
  } catch (error) {
    console.log("Could not find city", error);
  }
}

//Channel Fee
export async function addChannelFees(userId, createdBy, channelFeesJSON) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      mutation ($channelFeeInput:channelFeeInput!){
        addChannelFees(input:$channelFeeInput) {
          isError    
          message
        }
      }
        `,
      variables: {
        "channelFeeInput": {
        "userId": userId,
        "createdBy": createdBy,
        "channelFeesJSON": channelFeesJSON
        }
      }
    });

    return userResponse.data.data.addChannelFees;
  } catch (error) {
    console.log("Could not add channel fees", error);
  }
}

//Get channel fee
export async function getUserChannelFee(userId) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($getChannelFeeInput:getChannelFeeInput!){
        getChannelFee(input:$getChannelFeeInput) {
          channelFeesJSON
        }
      }
        `,
      variables: {
        "getChannelFeeInput": {
        "userId": userId
        }
      }
    });

    return userResponse.data.data.getChannelFee;
  } catch (error) {
    console.log("Could not get channel fees", error);
  }
}

export async function getState(state_id) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($getStateInput:getStateInput!){
        getState(input:$getStateInput) {
        id
        name
        }
      }
      `,
      variables: {
        "getStateInput": {
        "id": state_id
        }
      }
    });

    return userResponse.data.data.getState;
  } catch (error) {
    console.log("Could not find state", error);
  }
}
export async function getBuyerById(buyerId) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          buyerById(id: ${buyerId}) {
            firstName
            id
            lastName
            phone
            email
            notes
            cognitoId
            dob
            streetAddress
            streetAddress2
            city
            state
            country
            zipCode
          }
        }
        
        `
    });
    return userResponse.data.data.buyerById;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateUserPassword(user, password) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      mutation($userPasswordinput: UserPasswordinput!){
        updatePassword(input: $userPasswordinput){
              firstName
              id
              companyName
              lastName
              fullName
              phone
              email
              userRole
              userRoleName
              needsToChangePassword
              needsToAcceptTerms
              needsToAcceptPrivacy
              stripeId
              message
        }
      } `,
      variables: {
        "userPasswordinput": {
          "user": user,
          "password": password
        }
      }
    });
    return tokenResponse.data.data.updatePassword;

  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function checkUserPassword(email, password) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
  mutation($loginput: Loginput!){
    userLogin(input: $loginput){
  id
  }
  } `,
      variables: {
        "loginput": {
          "login": email,
          "password": password,
        }
      }
    });

    return userResponse.data.data.userLogin;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function registerUser(userObject) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation($userRegistration: UserRegistration!){
          registerUser(input: $userRegistration){            
            response  
            userId            
          }
        }
        `, variables: {
        "userRegistration": {
          "first_name": userObject.firstName,
          "last_name": userObject.lastName,
          "email": userObject.email,
          "password": userObject.password,
          "user_role": parseInt(userObject.userRole),
          "company_id": parseInt(userObject.company),
          "phone": userObject.phonenumber,
          "created_by":parseInt(userObject.createdBy)
        }
      }
    });
    return userResponse.data.data.registerUser;
  } catch (error) {
    console.log("Could not create user", error);
  }
}

export async function childUserRoles(role) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($getUserRole: GetUserRole!) {
        userRoles(input: $getUserRole) {
          id 
          user_role_name 
          keycode       
        }
      }
      
        `, variables: {
        "getUserRole": {
          "user_role_name": role
        }
      }
    });

    return userResponse.data.data.userRoles;
  } catch (error) {
    console.log("Could not fetch user roles", error);
  }
}
export async function findEmailExists(email) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        query ($getUserEmail: checkEMail!) {
          emailExists(input: $getUserEmail) {
            id
            email
          }
        }`, variables: {
        "getUserEmail": {
          "email": email
        }
      }
    });

    return userResponse.data.data.emailExists;
  } catch (error) {
    console.log("Could not check email exists", error);
  }
}
export async function userRoleExist(params) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        query ($getcheckUserRoleInput: checkUserRoleInput!) {
          checkUserRoleExist(input: $getcheckUserRoleInput) {
            status
          }
        }`, variables: {
        "getcheckUserRoleInput": {
          "userRole": params.userRole,
          "createdBy": params.createdBy
        }
      }
    });
    return userResponse.data.data.checkUserRoleExist;
  } catch (error) {
    console.log("Could not check email exists", error);
  }
}
export async function findUserByEmail(email) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          allUsers(condition:{email:"${email}"}){
            nodes{
              id
              email
            }
          }
        }
        `
    });

    return userResponse.data.data.allUsers.nodes[0];
  } catch (error) {
    console.log("Could not find user", error);
  }
}

export async function changePassword(passwordDetails) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
            query ($changePassword: changePassword!) {
              sendChangePassword(input: $changePassword){
                change_password_status
              }
            }`,
      variables: {
        "changePassword": {
          "passwordDetails": passwordDetails
        }
      }
    });
    return userResponse.data.data.sendChangePassword;
  } catch (error) {
    console.log("Could not change password", error);
  }
}

export async function sendMagicLink(email) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
            query ($sendMagicLink: sendMagicLink!) {
              sendResetMail(input: $sendMagicLink){
                 reset_status
              }
            }`,
      variables: {
        "sendMagicLink": {
          "email": email
        }
      }
    });
    return userResponse.data.data.sendResetMail;
  } catch (error) {
    console.log("Could not send user a reset password email", error);
  }
}

export async function sendAccountCreationEmail(email, password) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{ sendAccountCreationEmail(email:"${email}",tempPassword:"${password}") }`
    });
    return userResponse.data.data.sendAccountCreationEmail;
  } catch (error) {
    console.log("Could not send user an account creation email", error);
  }
}

export async function sendEmailToUser(address, html, subject) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{ sendEmailToUser(address:"${address}", html:"${html}", subject:"${subject}") }`
    });
    return userResponse.data.data.sendEmailToUser;
  } catch (error) {
    console.log("Could not send user an email", error);
  }
}

export async function updateUserLastLoggedIn(userId, lastLoginTime) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation {
            updateUserById(input:{
              id: ${userId},
              userPatch: {
                lastLogin: "${lastLoginTime}"
              }
            }) {
              clientMutationId
              user{
                firstName
                id
                lastName
                companyName
                fullName
                phone
                email
                about
                userRole
                stripeId
                streetAddress
                streetAddress2
                city
                state
                country
                zipCode                
                lastLogin
              }
            }
          }`
    });
    return userResponse.data.data.updateUserById.user;
  } catch (error) {
    console.log("Could not update user last logged in time");
  }
}

export async function updateUserEbayActive(userId, ebayActive) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        mutation {
            updateUserById(input:{
              id: ${userId},
              userPatch: {
                stubhubActive: ${ebayActive}
              }
            }) {
              clientMutationId
              user{
                firstName
                id
                lastName
                companyName
                fullName
                phone
                email
                about
                userRole
                stripeId
                streetAddress
                streetAddress2
                city
                state
                country
                zipCode
                
              }
            }
          }`
    });/*postalCode
    ebayClientId
    ebayClientSecret
    ebayRedirectUri
    ebayAccessToken
    ebayRefreshToken
    ebayFulfillmentPolicyId
    ebayReturnPolicyId
    ebayPaymentPolicyId
    ebayActive*/
    return userResponse.data.data.updateUserById.user;
  } catch (error) {
    console.log("Could not update user ebay active setting");
  }
}
export async function checkValidResetToken(token) {
  try {
    let userResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
            query ($getResetToken: getResetToken!) {
              checkResetToken(input: $getResetToken){
                id
              }
            }`,
      variables: {
        "getResetToken": {
          "token": token
        }
      }
    });
    return userResponse.data.data.checkResetToken;
  } catch (error) {
    console.log("Could not change the password", error);
  }
}