import axios from "axios";
import moment from "moment";

const defaultPurchaseOrderProperties = `
id
createdAt
sellerId
purchaseDate
purchaseAmount
purchaseCurrency
paymentMethod
paymentCardLast4
listingId
paymentGroup
tags
externalReference
vendorId
lineItems
fees
shippingAndHandling
salesTax
`;

export async function getUserPurchaseOrders(sellerId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
            allPurchaseOrders(condition:{sellerId:${sellerId}}) {
                nodes {
                  ${defaultPurchaseOrderProperties}
                }
            }
        }`
    });
    return response.data.data.allPurchaseOrders.nodes;
  } catch (error) {
    console.log("Could not retrieve user purchase orders");
  }
}

export async function getUserPurchaseOrdersInRange(
  sellerId,
  startingDate,
  endingDate
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
            returnListOfPurchaseOrdersByDateRangeAndSellerId(_sellerid:${sellerId}, _startdate:"${startingDate}", _enddate:"${endingDate}") {
                nodes {
                  ${defaultPurchaseOrderProperties}
                }
            }
        }`
    });
    return response.data.data.returnListOfPurchaseOrdersByDateRangeAndSellerId
      .nodes;
  } catch (error) {
    console.log("Could not retrieve user purchase orders by date range");
  }
}

/**
 * Creates a vendor, for use in purchase orders. For now, the shipping
 * address is also the billing address.
 * @param {String} name
 * @param {String} phone
 * @param {String} email
 * @param {String} address
 * @param {String} address2
 * @param {String} zip
 * @param {String} city
 * @param {String} state
 * @param {String} country
 */
export async function createVendor(
  name,
  phone,
  email,
  address,
  address2,
  zip,
  city,
  state,
  country
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation{
            createVendor(input:{
              vendor:{
                name:"${name}"
                shippingAddress:"${address}"
                shippingAddress2:"${address2}"
                shippingAddressZip:"${zip}"
                shippingAddressCity:"${city}"
                shippingAddressState:"${state}"
                shippingAddressCountry:"${country}"
                billingAddress:"${address}"
                billingAddress2:"${address2}"
                billingAddressZip:"${zip}"
                billingAddressCity:"${city}"
                billingAddressState:"${state}"
                billingAddressCountry:"${country}"
                phone:"${phone}"
                email:"${email}"
              }
            }){
              vendor{
                id
              }
            }
          }
        `
    });
    return response.data.data.createVendor.vendor;
  } catch (error) {
    console.log("Could not create new vendor");
  }
}

/**
 * Adding purchase order data to purchase order table
 */
export async function createPurchaseOrder(purchaseOrderdataObject) {
  try {
    let purchaseResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation($addPurchaseOrderInput: addPurchaseOrderInput!){
            addPurchaseOrder(input: $addPurchaseOrderInput){            
              isError    
              msg
              data
            }
          }
        `, variables: {
        "addPurchaseOrderInput": {
          "user_id": purchaseOrderdataObject.userId,
          "vendor_id": purchaseOrderdataObject.vendorId,
          "purchase_date": purchaseOrderdataObject.purchaseDate,
          "payment_method_id": purchaseOrderdataObject.paymentMethod,
          "sales_tax": purchaseOrderdataObject.salesTax,
          "purchase_amount": purchaseOrderdataObject.purchaseAmount,
          "payment_last4_digits": purchaseOrderdataObject.cardLastFourDigits,
          "shipping_and_handling_fee": purchaseOrderdataObject.shipAndHandle,
          "payment_type_id": purchaseOrderdataObject.paymentType,
          "external_reference": purchaseOrderdataObject.referenceId,
          "fees": purchaseOrderdataObject.otherFees,
          "notes": purchaseOrderdataObject.notes,
          "currency_id": purchaseOrderdataObject.currencyId,
          "created_by": purchaseOrderdataObject.createdBy,
          "tagsInformationsJSON": purchaseOrderdataObject.tags,
          "vendorInformationsJSON": purchaseOrderdataObject.vendorInformationsJSON,
          "listingInformationsJSON": purchaseOrderdataObject.listingInformationsJSON,
          "InvoiceAttachmentInformationsJSON": purchaseOrderdataObject.InvoiceAttachmentInformationsJSON,
        }
      }
    });
    return purchaseResponse.data.data.addPurchaseOrder;
  } catch (error) {
    console.log("Could not create user", error);
  }
}



export async function vendorSearchFilter(searchtext, type) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($GetVendorSearchFilterInput: GetVendorSearchFilterInput!) {
        vendorSearchFilter(input: $GetVendorSearchFilterInput) 
      }
        `, variables: {
        "GetVendorSearchFilterInput": {
          "searches": searchtext,
          "type": type
        }
      }
    });

    return response.data.data.vendorSearchFilter;
  } catch (error) {
    console.log("Could not get lists");
  }
}

export async function listingSearchFilter(searchtext,userId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($GetListingSearchFilterInput: GetListingSearchFilterInput!) {
        listingSearchFilter(input: $GetListingSearchFilterInput) 
      }
        `, variables: {
        "GetListingSearchFilterInput": {
          "searches": searchtext,
          "userId": userId
        }
      }
    });
    return response.data.data.listingSearchFilter;
  } catch (error) {
    console.log("Could not get lists");
  }
}

export async function getVendorFilter(input) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($GetVendorSearchFilterInput: GetVendorSearchFilterInput!) {
        getvendorFilter(input: $GetVendorSearchFilterInput) {
          id
          name
          email
          phone
          address
          address2
          city
          state
          country
          zip
        }
      }
        `, variables: {
        "GetVendorSearchFilterInput": {
          "searches": input.searchtext
        }
      }
    });
    return response.data.data.getvendorFilter[0];
  } catch (error) {
    console.log("Could not get lists");
  }
}

export async function getListingFilter(value) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($GetListingSearchFilterInput: GetListingSearchFilterInput!) {
        getListingFilter(input: $GetListingSearchFilterInput) {
          id
          eventName
          eventDate
          section
          row
          quantity
          seat_start
          seat_end
          group_cost
          unit_cost
          seat_type_id
        }
      }
        `, variables: {
        "GetListingSearchFilterInput": {
          "searches": value
        }
      }
    });
    return response.data.data.getListingFilter[0];
  } catch (error) {
    console.log("Could not get lists");
  }
}

export async function getAllPaymentTypes() {
  try {
    let paymenttypeResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allPaymentTypes   {            
            id
            name
          }
        }`
      }
    );
    return paymenttypeResponse.data.data.allPaymentTypes;
  } catch (error) {
    console.log("Error:No records");
  }
}

export async function getAllPaymentGroups() {
  try {
    let paymentgroupResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          allPaymentGroups {            
            id
            name
          }
        }`
      }
    );
    return paymentgroupResponse.data.data.allPaymentGroups;
  } catch (error) {
    console.log("Error:No records");
  }
}

/**
 * Used to augment a purchase order after it's created, and create a link
 * to a listing.
 * @param {*} purchaseOrderId
 * @param {*} listingId
 */
export async function addListingIdToPurchaseOrderById(
  purchaseOrderId,
  listingId
) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation{
            updatePurchaseOrderById(input:{id:${purchaseOrderId}, purchaseOrderPatch:{
              listingId:${listingId}
            }}){
              purchaseOrder{
                id
              }
            }
          }
        `
    });
    return response.data.data.updatePurchaseOrderById.purchaseOrder;
  } catch (error) {
    console.log("Could not add vendor ID to purchase order");
  }
}

export async function updatePurchaseOrderTags(purchaseOrderId, tags) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation{
            updatePurchaseOrderById(input:{id:${purchaseOrderId}, purchaseOrderPatch:{
              tags:"${tags}"
            }}){
              purchaseOrder{
                id
              }
            }
          }
        `
    });
    return response.data.data.updatePurchaseOrderById.purchaseOrder;
  } catch (error) {
    console.log("Could not add tags to purchase order");
  }
}

export async function updatePurchaseOrderNotes(purchaseOrderId, notes) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          mutation{
            updatePurchaseOrderById(input:{id:${purchaseOrderId}, purchaseOrderPatch:{
              externalReference:"${notes}"
            }}){
              purchaseOrder{
                id
              }
            }
          }
        `
    });
    return response.data.data.updatePurchaseOrderById.purchaseOrder;
  } catch (error) {
    console.log("Could not add notes to purchase order");
  }
}

/**
 * Will get a vendor by an ID.
 */
export async function getVendorById(vendorId) {
  // use that query we just built
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        allVendors(condition:{id:${vendorId}}){
          nodes {
            id
            name
            shippingAddress
            shippingAddress2
            shippingAddressZip
            shippingAddressCity
            shippingAddressState
            shippingAddressCountry
            billingAddress
            billingAddress2
            billingAddressZip
            billingAddressCity
            billingAddressState
            billingAddressCountry
            phone
            email
          }
        }
      }`
    });
    // we know there is only 1 match by ID because ID is unique
    return tokenResponse.data.data.allVendors.nodes[0];
  } catch (error) {
    console.log("Could not find vendor by ID");
  }
}
export async function getAllPaymentTags() {
  try {
    let paymentTagsResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        {
          getTagsPurchaseOrder {            
            value
            label
          }
        }`
      }
    );
    return paymentTagsResponse.data.data.getTagsPurchaseOrder;
  } catch (error) {
    console.log("Error:No records");
  }
}

/**
 * Will get a list of all vendors by a certain name.
 * Can optionally limit the number of results.
 */
export async function getVendorsByName(vendorName, topLimit) {
  // build the query based on a limit being passed in or not
  let vendorQuery = `searchVendorsByName(_pattern: "${vendorName}"`;
  if (topLimit) {
    vendorQuery += ` first:${topLimit}`;
  }
  vendorQuery += `) {`;
  // use that query we just built
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        ${vendorQuery}
          nodes {
            id
            name
            shippingAddress
            shippingAddress2
            shippingAddressZip
            shippingAddressCity
            shippingAddressState
            shippingAddressCountry
            billingAddress
            billingAddress2
            billingAddressZip
            billingAddressCity
            billingAddressState
            billingAddressCountry
            phone
            email
          }
        }
      }`
    });
    return tokenResponse.data.data.searchVendorsByName.nodes;
  } catch (error) {
    console.log("Could not find vendors");
  }
}

export async function getPurchaseOrderById(purchaseOrderId) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
          purchaseOrderById(id:${purchaseOrderId}) {
            ${defaultPurchaseOrderProperties}
          }
        }`
    });
    return response.data.data.purchaseOrderById;
  } catch (error) {
    console.log("Could not retrieve purchase order by id");
  }
}

export async function updatePurchaseOrder(purchaseOrder) {
  try {
    let purchaseOrderResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        mutation updatePurchaseOrderByPurchaseOrder($id: Int!, $purchaseOrder: PurchaseOrderPatch!){
            updatePurchaseOrderById(input:{
              id: $id,
              purchaseOrderPatch: $purchaseOrder
            }) {
              clientMutationId
              purchaseOrder {
                ${defaultPurchaseOrderProperties}
              }
            }
          }
             `,
        variables: {
          id: purchaseOrder.id,
          purchaseOrder: purchaseOrder
        }
      }
    );
    return purchaseOrderResponse.data.data.updatePurchaseOrderById
      .purchaseOrder;
  } catch (error) {
    console.log(
      "Could not update purchase order by id using generic purchase order patch."
    );
  }
}

export async function getPurchaseOrderDetails(id) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query ($getOrderDetails: purchaseDetailsInput!) {
            getPurchaseOrderDetails(input: $getOrderDetails) {
              purchaseOrderDetails
            }
          }`,
      variables: {
        "getOrderDetails": {
          "id": id
        }
      }
    });
    return tokenResponse.data.data.getPurchaseOrderDetails.purchaseOrderDetails;
  }
  catch (error) {
    console.log(
      "Could not get purchase order by id ."
    );
  }

}

export async function updatePurchaseOrderDetails(purchaseOrder) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query ($updateOrderDetails: purchaseDetailsUpdate!) {
            purchaseOrderDetailsUpdate(input: $updateOrderDetails) {
              status_data
            }
          }`,
      variables: {
        updateOrderDetails: {
          id: purchaseOrder.id,
          purchaseOrderDetails: purchaseOrder
        }
      }
    });
    return tokenResponse.data.data.purchaseOrderDetailsUpdate.purchaseOrderDetails;
  }
  catch (error) {
    console.log(
      "Could not get purchase order by id ."
    );
  }

}
export async function updatePurchaseOrderDetailsVendor(purchaseOrder) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query ($updateOrderDetails: purchaseDetailsUpdate!) {
            purchaseOrderDetailsVendorUpdate(input: $updateOrderDetails) {
              status_data
            }
          }`,
      variables: {
        updateOrderDetails: {
          id: purchaseOrder.id,
          purchaseOrderDetails: purchaseOrder
        }
      }
    });
    return tokenResponse.data.data.purchaseOrderDetailsVendorUpdate.purchaseOrderDetails;
  }
  catch (error) {
    console.log(
      "Could not get purchase order by id ."
    );
  }

}
export async function updatePurchaseOrderTagsById(purchaseOrder) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query ($updateOrderDetails: purchaseDetailsPOTUpdate!) {
            purchaseOrderDetailsTagsUpdate(input: $updateOrderDetails) {
              status_data
            }
          }`,
      variables: {
        updateOrderDetails: {
          id: purchaseOrder.id,
          purchaseOrderDetails: purchaseOrder.tags
        }
      }
    });
    return tokenResponse.data.data.purchaseOrderDetailsVendorUpdate.purchaseOrderDetails;
  }
  catch (error) {
    console.log(
      "Could not get purchase order by id ."
    );
  }

}
export async function getPurchaseOrderLists(id) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query ($getOrderDetails: purchaseDetailsInput!) {
            purchaseorderlistings(input: $getOrderDetails) {
              purchaseOrderGrouplist
            }
          }`,
      variables: {
        "getOrderDetails": {
          "id": id
        }
      }
    });
    return tokenResponse.data.data.purchaseorderlistings.purchaseOrderGrouplist;
  }
  catch (error) {
    console.log(
      "Could not get purchase order by id ."
    );
  }

}

export async function getAllPurchaseOrdersLists(searchText, startDate, endDate, userId) {
  try {
    let tokenResponse = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query ($purchaseOrderSearchInput: purchaseOrderSearchInput!) {
            allPurchaseOrders(input: $purchaseOrderSearchInput) {
              id,    
              listingId,
              eventId,
              purchaseDate,
              amount,
              purchaseMethod,
              lastFourDigits,
              invoice,
              tags,
              attachment_url
            }
          }`,
      variables: {
        "purchaseOrderSearchInput": {
          "searchText": searchText,
          "startDate": startDate,
          "endDate": endDate,
          "userId": userId
        }
      }
    });
    return tokenResponse.data.data.allPurchaseOrders;
  }
  catch (error) {
    console.log(
      "Could not get purchase order by id ."
    );
  }

}

export async function getPODetails(listingGroupIds) {
  try {
    let response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        query ($fetchListingGroupInput: fetchListingGroupInput!){
          fetchListingGroupDetails(input: $fetchListingGroupInput){            
            isError
            msg
            value    
          }
        }
        `, variables: {
        "fetchListingGroupInput": {
          "listingGroupIds": listingGroupIds
        }
      }
    });
    return response.data.data.fetchListingGroupDetails;
  } catch (error) {
    console.log("Could not create user", error);
  }


}

/**
 * Getting all Ticketing System Accounts by ticket system id from TicketingSystemAccounts table
 */
export async function allTicketingSystemAccountsByid(id) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
          query ($getAllTicketingSystemAccountsInput: GetAllTicketingSystemAccountsInput!){
            allTicketingSystemAccountsById(input: $getAllTicketingSystemAccountsInput){            
              id
              email              
            }
          }
             `,
      variables: {
        "getAllTicketingSystemAccountsInput": {
          "id": id
        }
      }
    });
    return response.data.data.allTicketingSystemAccountsById;
  } catch (error) {
    console.error(error);
  }
}

export async function invoiceAttachment(invoiceUploadAttachmentData) {
  const formData = new FormData();
  await formData.append('attachment_file_data', invoiceUploadAttachmentData.invoiceAttachFile);
  await formData.append('attachment_file_name', invoiceUploadAttachmentData.invoiceAttachName);
  await formData.append('attachment_po_id', invoiceUploadAttachmentData.purchaseOrderId);
  if (invoiceUploadAttachmentData.invoiceAttachFile) {
    await axios.post(process.env.REACT_APP_FILE_DOWNLOAD + '/invoiceUpload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
      .then(function (response) {
        return response.data
      })
      .catch(function (response) {
        return false
      });
  }

}

export async function updatePoAttachment(id, po_id, attachment_url, attachmentName) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
            query ($updatePoAttachment: updatePoAttachment) {
              updateAttachmentDetails(input: $updatePoAttachment) {
                status
              }
            }
          `,
      variables: {
        "updatePoAttachment": {
          "id": id,
          "po_id": po_id,
          "attachment_url": attachment_url,
          "attachment_name": attachmentName
        }
      }
    });
    return response.data.data.updateAttachmentDetails;
  } catch (error) {
    console.error(error);
  }

}