import axios from "axios";

const defaultOrderProperties = `
  id
  stripeId
 
  createdAt
   
  listingId
  listingByListingId {
    id
    delivered
    
    price
  }
  buyerId
   
  sellerId
  userBySellerId {
    id
    email
    phone
    fullName
  }`;

export async function getAllOrders() {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          allTransactions {
            nodes {
              ${defaultOrderProperties}
            }
          }
        }
        `
    });
    return response.data.data.allTransactions.nodes;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function filterAllOrders(startdate, enddate) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          returnListOfOrdersByDateRange(_startdate:  "${startdate}", _enddate: "${enddate}"){
            nodes {
              ${defaultOrderProperties}
            }
          }
        }
        `
    });
    return response.data.data.returnListOfOrdersByDateRange.nodes;
  } catch (error) {
    console.log("Could not authenticate");
  }
}
export async function getAllOrdersBySeller(id) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          allTransactions(condition: {sellerId: ${id}}) {
            nodes {
              ${defaultOrderProperties}
            }
          }
        }
        `
    });
    return response.data.data.allTransactions.nodes;
  } catch (error) {
    console.log("Could not authenticate");
  }
}


/**
     * Page : home
     * Function For : Added API service to get the sales and revenue data
     * Ticket No : TIC-134
*/

export async function getSalesByMonth() {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        getSalesByMonth{            
          sales_list
        }
      }`
    });
    return response.data.data.getSalesByMonth;
  } catch (error) {
    console.log("Could not get Sales By Month");
  }
}

/**
    * Page : home
    * Function For : To get the revenue, sales and listing data on the insights page
    * Ticket No : TIC-35
*/

export async function getRevenueData(userID, dayDiff) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `query ($getRevenueValue: getFilterData!){
        getRevenueValue(input: $getRevenueValue){            
          local_today_rev,
          local_week_rev,
          local_month_rev,
          local_year_to_date_rev       
        }
      }`,
      variables: {
        "getRevenueValue": {
          "seller_id": userID,
          "day_diff": dayDiff
        }
      }
    });
    return response.data.data.getRevenueValue;
  } catch (error) {
    console.info(error);
  }
}

export async function getSalesData(userID, dayDiff) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `query ($getSalesValue: getFilterData!){
        getSalesValue(input: $getSalesValue){            
          local_today_rev,
          local_week_rev,
          local_month_rev,
          local_year_to_date_rev       
        }
      }`,
      variables: {
        "getSalesValue": {
          "seller_id": userID,
          "day_diff": dayDiff
        }
      }
    });
    return response.data.data.getSalesValue;
  } catch (error) {
    console.info(error);
  }
}

export async function getListingData(userID, dayDiff) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `query ($getListingValue: getFilterData!){
        getListingValue(input: $getListingValue){            
          local_today_rev,
          local_week_rev,
          local_month_rev,
          local_year_to_date_rev       
        }
      }`,
      variables: {
        "getListingValue": {
          "seller_id": userID,
          "day_diff": dayDiff
        }
      }
    });
    return response.data.data.getListingValue;
  } catch (error) {
    console.info(error);
  }
}

/**
    * Page : home
    * Function For : Added serve to get the barchart data
    * Ticket No : TIC-131
*/

export async function getAllOrdersBarChart() {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `{
        getAllOrdersBarChart{            
          lineChart,
          barChart,
          salesMax,
          revenueMax,
          calendarPeriod   
        }
      }`
    });
    return response.data.data.getAllOrdersBarChart;
  } catch (error) {
    console.info(error);
  }
}

export async function filterAllOrdersBySeller(id, startdate, enddate) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          returnListOfOrdersByDateRangeAndSellerid(_sellerid: ${id},_startdate:  "${startdate}", _enddate: "${enddate}"){
            nodes {
              ${defaultOrderProperties}
            }
          }
        }
        `
    });
    return response.data.data.returnListOfOrdersByDateRangeAndSellerid.nodes;
  } catch (error) {
    console.info(error);
  }
}


export async function brokerOrders(id, startdate, enddate, orderStatus) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: ` 
        query ($listBrokerOrders: brokerOrdersInput!) {
          allBrokerOrders(input: $listBrokerOrders) {
            id
            channel_id
            created_at
            delivery_at
            cancelled_order_type_id
            channel_name
            delivery_status_id
            listing_group_id
            buyer_name
            buyer_email
            phone_number
            external_channel_reference_id
          }
        }`,
      variables: {
        "listBrokerOrders": {
          id: id,
          start_date: startdate,
          end_date: enddate,
          orderStatus: orderStatus
        }
      }
    });
    return response.data.data.allBrokerOrders;
  } catch (error) {
    console.info(error);
  }
}
export async function getCognitoUserInfo(id) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          listSingleCognitoUser(sub:"${id}"){
            name
            phone_number
            email
            sub
          }
        }
        `
    });
    let user = response.data.data.listSingleCognitoUser;
    return user;
  } catch (error) {
    console.log("Could not authenticate");
  }
}
export async function getAllOrdersByBuyer(id) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          allTransactions(condition: {buyerId: ${id}}) {
            nodes {
              ${defaultOrderProperties}
            }
          }
        }
        `
    });
    let transactions = response.data.data.allTransactions.nodes;
    transactions.forEach(async transaction => {
      if (transactions.cognitoUserId) {
        let user = await getCognitoUserInfo(transaction.cognitoUserId);
        transaction.userInfo = user;
      }
    });
    return transactions;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function filterAllOrdersByBuyer(id, startdate, enddate) {
  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
        {
          returnListOfOrdersByDateRangeAndBuyerid(_buyerid: ${id},_startdate:  "${startdate}", _enddate: "${enddate}"){
            nodes {
              ${defaultOrderProperties}
            }
          }
        }
        `
    });
    return response.data.data.returnListOfOrdersByDateRangeAndBuyerid.nodes;
  } catch (error) {
    console.log("Could not authenticate");
  }
}

export async function updateOrder(transaction) {
  try {
    let transactionResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `
        mutation updateTransactionByTransaction($id: Int!, $transaction: TransactionPatch!){
            updateTransactionById(input:{
              id: $id,
              transactionPatch: $transaction
            }) {
              clientMutationId
              transaction {
                ${defaultOrderProperties}
              }
            }
          }
             `,
        variables: {
          id: transaction.id,
          transaction: transaction
        }
      }
    );
    return transactionResponse.data.data.updateTransactionById.transaction;
  } catch (error) {
    console.log("Could not update order by id using generic order patch.");
  }
}

export async function getOrderById(orderId) {
  try {
    let transactionResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `{
            transactionById(id: ${orderId}) {
              ${defaultOrderProperties}
            }
        }`
      }
    );
    return transactionResponse.data.data.transactionById;
  } catch (error) {
    console.log("Could not get order by id.");
  }
}

export async function createOrder(transaction) {
  try {
    let transactionResponse = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `mutation {
          createTransaction(input: {
            transaction: {
              sellerId: ${transaction.sellerId},
              stripeId: "${transaction.stripeId}",
              listingId: ${transaction.listingId},
              buyerId: ${transaction.buyerId},
            }}) {
            clientMutationId
            transaction {
              id
            }
          }
        }`
      }
    );
    return transactionResponse.data.data.createTransaction;
  } catch (error) {
    console.log("Could not create order.");
  }
}

export async function getBrokerOrderDetailsById(detailID) {
  try {
    let orderDetails = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `query ($brokerOrderId: brokerOrderId!) {
          viewOrderDetails(input: $brokerOrderId) {
            broker_order_detail
          }
        }`,
        variables: {
          "brokerOrderId": {
            "orderId": detailID
          }
        }
      }
    );
    return orderDetails.data.data.viewOrderDetails;
  } catch (error) {
    console.log("Could not create order.");
  }
}
export async function getMarketPlaceOrderDetailsById(detailID) {
  try {
    let orderDetails = await axios.post(
      process.env.REACT_APP_GRAPHQL_URL,
      {
        query: `query ($getSingleMarketPlaceOrderDetailsInput: getSingleMarketPlaceOrderDetailsInput!) {
          getSingleMarketPlaceOrderDetails(input: $getSingleMarketPlaceOrderDetailsInput) {
            isError
            msg
            data
          }
        }`,
        variables: {
          "getSingleMarketPlaceOrderDetailsInput": {
            "marketPlaceOrderId": parseInt(detailID)
          }
        }
      }
    );
    return orderDetails.data.data.getSingleMarketPlaceOrderDetails;
  } catch (error) {
    console.log("Could not create order.");
  }
}
export async function getAllMarketPlaceOrders(searches, dateFrom, dateTo, orderStatus) {

  try {
    const response = await axios.post(process.env.REACT_APP_GRAPHQL_URL, {
      query: `
      query ($GetAllMarketPlaceOrdersInput: GetAllMarketPlaceOrdersInput!) {
        allMarketPlaceOrders(input: $GetAllMarketPlaceOrdersInput) {
          isError
          msg
          data
        }
      }
        `, variables: {
        "GetAllMarketPlaceOrdersInput": {
          "searches": searches,
          "orderStartDate": dateFrom,
          "orderEndDate": dateTo,
          "deliveryStatusId": orderStatus
        }
      }
    });

    return response.data.data.allMarketPlaceOrders;
  } catch (error) {
    console.log("Could not get lists");
  }
}


export async function downloadAttachmentZipFile(attachmentData) {
  try {
    const response = await axios({
      method: 'post',
      url: process.env.REACT_APP_FILE_DOWNLOAD + '/downloadAttachmentsZipfile',
      responseType: 'arraybuffer',
      data: attachmentData
    })

    return response;
  } catch (error) {
    console.log("Could not get lists");
  }
}


export async function viewTicket(attachmentId, eventId, listingId) {
  try {
    const response = await axios({
      method: 'post',
      url: process.env.REACT_APP_FILE_DOWNLOAD + '/viewTicketFile',
      data: {
        attachmentId, eventId, listingId // This is the body part
      }
    })

    return response;
  } catch (error) {
    console.log("Could not get lists");
  }
}

export async function viewInvoiceAttachment(purchase_order_attachment_id) {
  try {
    const response = await axios({
      method: 'post',
      url: process.env.REACT_APP_FILE_DOWNLOAD + '/purchase-order-invoice-view',
      data: {
        purchase_order_attachment_id // This is the body part
      }
    })

    return response;
  } catch (error) {
    console.log("Could not get lists");
  }
}

export async function editAttachmentTicket(attachment_item, listingData) {

  const formData = new FormData();
  formData.append('attachment_file_data', attachment_item.attachment_file_data);
  formData.append('attachment_file', attachment_item.attachment_file);
  formData.append('eventId', listingData.eventId);
  formData.append('ListingId', listingData.listingGroupId);
  formData.append('attachmentId', listingData.attachmentId);


  if (attachment_item.attachment_file) {
    await axios.post(process.env.REACT_APP_FILE_DOWNLOAD + '/editTicketAttachment', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
      .then(function (response) {
        return response.data
      })
      .catch(function (response) {
        return false
      });
  }

}

export async function getChannelLogo(channelId) {
  try {
    const response = await axios({
      method: 'post',
      url: process.env.REACT_APP_FILE_DOWNLOAD + '/getChannelLogo',
      data: {
        channelId 
      }
    })

    return response;
  } catch (error) {
    console.log("Could not get lists");
  }
}
