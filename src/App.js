import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import UserContext from "./contexts/user.jsx";
import routes from "./routes";
import withTracker from "./withTracker";
import "./assets/ui-stylesheet.css";
import "./assets/stylesheet-font.css";
import { userRoles, userRolesLower } from "./constants/constants";

export default () => (
  <div>
    <UserContext.Consumer>
      {context => (
        <Router basename={process.env.REACT_APP_BASENAME || ""}>
          <div className={context && context.user && context.user.userRole === userRoles.marketplace_admin ? userRolesLower.marketplace_admin : null}>
            {routes.map((route, index) => {
              return (
                <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  component={withTracker(props => {
                    return (
                      <route.layout access={route.access} {...props}>
                        <route.component {...props} />
                      </route.layout>
                    );
                  })}
                />
              );
            })}
          </div>
        </Router>
      )}
    </UserContext.Consumer>
  </div>
);
