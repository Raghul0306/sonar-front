import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "shards-react";
import { Redirect } from "react-router-dom";

import MainNavbar from "../components/layout/MainNavbar/MainNavbar";
import MainSidebar from "../components/layout/MainSidebar/MainSidebar";
import MainFooter from "../components/layout/MainFooter";
import useAccess from "../hooks/useAccess";

const DefaultLayout = ({ children, noNavbar, noFooter, access }) => {
  const isAllow = useAccess(access);

  if (isAllow) {
    return (
      <Container fluid>
        <Row>
          <MainSidebar />
          <Col
            className="main-content p-0"
            xl={{ size: 10, offset: 2 }}
            lg={{ size: 9, offset: 3 }}
            md={{ size: 9, offset: 3 }}
            sm="12"
            tag="main"
          >
            {!noNavbar && <MainNavbar />}
            {children}
            {!noFooter && <MainFooter />}
          </Col>
        </Row>
      </Container>
    );
  }

  return <Redirect to="/login" />;
};

DefaultLayout.propTypes = {
  /**
   * Whether to display the navbar, or not.
   */
  noNavbar: PropTypes.bool,
  /**
   * Whether to display the footer, or not.
   */
  noFooter: PropTypes.bool,
  /**
   * Whether to access route or not.
   */
  access: PropTypes.array
};

DefaultLayout.defaultProps = {
  noNavbar: false,
  noFooter: false,
  access: []
};

export default DefaultLayout;
