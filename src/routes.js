import React from "react";
import loadable from "@loadable/component";
import { Redirect } from "react-router-dom";

// Layout Types
const DefaultLayout = loadable(() => import("./layouts/Default"));
const OpenLayout = loadable(() => import("./layouts/OpenLayout"));

// Route Views
const Dashboard = loadable(() => import("./views/Dashboard"));
const EditAccount = loadable(() => import("./components/userInfo/editAccount"));
const EditAccountAsAdmin = loadable(() =>
  import("./components/userInfo/editAccountAsAdmin")
);
const Errors = loadable(() => import("./views/Errors"));
const Upload = loadable(() => import("./views/Upload"));
const Reports = loadable(() => import("./views/Reports"));
const MarketplaceReports = loadable(() => import("./views/MarketplaceReports"));
const Remittances = loadable(() => import("./views/Remittances"));
const Remittance = loadable(() => import("./views/Remittance"));
const Listings = loadable(() => import("./views/Listings"));
const AddListings = loadable(() => import("./views/AddListings"));
const ViewListings = loadable(() => import("./views/ViewListings"));
const SupportDashboard = loadable(() => import("./views/SupportDashboard"));
const AllUsers = loadable(() => import("./views/AllUsers"));
const AllMarketPlaceUsers = loadable(() => import("./views/AllMarketPlaceUsers"));
const AllBuyerUsers = loadable(() => import("./views/AllBuyerUsers"));
const SpecificOrders = loadable(() => import("./views/SpecificOrders"));
const BrokerDetailsPage = loadable(() => import("./views/BrokerOrderDetails"));
const Orders = loadable(() => import("./views/Orders"));
const MarketplaceOrders = loadable(() => import("./views/MarketplaceOrders"));
const PayoutConnection = loadable(() => import("./views/PayoutConnection"));
const Operation = loadable(() => import("./views/Operation"));
const Accounting = loadable(() => import("./views/Accounting"));
const SellerProfile = loadable(() => import("./views/SellerProfile"));
const BuyerProfile = loadable(() => import("./views/BuyerProfile"));
const PayoutsReceipt = loadable(() => import("./views/PayoutsReceipt"));
const PayoutConnectionSuccess = loadable(() =>
  import("./views/PayoutConnectionSuccess")
);
const PayoutConnectionSettings = loadable(() =>
  import("./views/PayoutConnectionSettings")
);
const AuctionEvents = loadable(() => import("./views/AuctionEvents"));
const Purchases = loadable(() => import("./views/Purchases"));
const PurchaseSuccess = loadable(() => import("./views/PurchaseSuccess"));
const PurchaseCancelled = loadable(() => import("./views/PurchaseCancelled"));
const PurchaseCreateOrder = loadable(() => import("./views/PurchaseCreateOrder"));
const PurchaseOrderDetail = loadable(() => import("./views/PurchaseOrderDetail"));
const EbayAuthAccepted = loadable(() => import("./views/EbayAuthAccepted"));
const EbayAuthRejected = loadable(() => import("./views/EbayAuthRejected"));
const Marketing = loadable(() => import("./views/Marketing"));
const Company = loadable(() => import("./views/Companies"));

//Detailed Components
const ListingInsights = loadable(() => import("./views/ListingInsights"));
const InsightDetail = loadable(() => import("./views/InsightDetail"));
const Payouts = loadable(() => import("./views/Payouts"));
const AddEvents = loadable(() => import("./components/events/addEvent"));
const OrdersSubstitute = loadable(() => import("./views/OrderSubstitute"));
const PrivacyPolicy = loadable(() => import("./services/PrivacyPolicy"));
const TermsAndConditions = loadable(() => import("./services/TermsAndConditions"));
const SellerAgreement = loadable(() => import("./services/SellerAgreement"));
const OrderDetails = loadable(() => import("./views/MarketPlaceOrderDetails"));
const LoginForm = loadable(() => import("./components/login"));
const VerifyToken = loadable(() => import("./components/VerifyToken"));

export default [
  {
    path: "/",
    exact: true,
    layout: OpenLayout,
    component: () => <LoginForm />
  },
  {
    path: "/login",
    exact: true,
    layout: OpenLayout,
    component: () => <LoginForm />
  },
  {
    path: "/change-password/:token",
    exact: true,
    layout: OpenLayout,
    component: () => <LoginForm />
  },
  {
    path: "/verify-email/:token",
    exact: true,
    layout: OpenLayout,
    component: () => <VerifyToken />
  },
  {
    path: "/home",
    layout: DefaultLayout,
    component: Dashboard
  },
  {
    path: "/user-profile",
    layout: DefaultLayout,
    component: EditAccount
  },
  {
    path: "/edit-user-profile",
    layout: DefaultLayout,
    component: EditAccount
  },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  },
  // {
  //   path: "/components-overview",
  //   layout: DefaultLayout,
  //   component: ComponentsOverview
  // },
  // {
  //   path: "/support",
  //   layout: DefaultLayout,
  //   component: Support
  // },
  {
    path: "/reports",
    layout: DefaultLayout,
    component: Reports
  },
  {
    path: "/marketplace-reports",
    layout: DefaultLayout,
    exact: true,
    component: Reports
  },//MarketplaceReports
  {
    path: "/remittance",
    exact: true,
    layout: DefaultLayout,
    component: Remittances,
    access: ["support", "admin", "super_admin", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin"]
  },
  {
    path: "/payouts",
    exact: true,
    layout: DefaultLayout,
    component: Payouts,
    access: ["support", "admin", "super_admin", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin"]
  },
  {
    path: "/remittance/:id",
    exact: true,
    layout: DefaultLayout,
    component: Remittance,
    access: ["support", "broker", "seller", "admin", "super_admin", "marketplace_admin", "marketplace_user", "broker_user","super_super_admin"]
  },
  {
    path: "/upload",
    layout: DefaultLayout,
    component: Upload
  },
  {
    path: "/listings",
    layout: DefaultLayout,
    component: Listings
  },
  {
    path: "/add-listings",
    layout: DefaultLayout,
    component: AddListings
  },
  {
    path: "/add-listing/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/add-listings" />
  },
  {
    path: "/add-listing/:id",
    exact: true,
    layout: DefaultLayout,
    component: AddListings
  },
  {
    path: "/view-listing/:id",
    exact: true,
    layout: DefaultLayout,
    component: ViewListings
  },
  {
    path: "/support",
    exact: true,
    layout: DefaultLayout,
    component: SupportDashboard,
    access: ["support", "admin", "super_admin", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin"]
  },
  {
    path: "/support/listings",
    exact: true,
    layout: DefaultLayout,
    component: ListingInsights,
    access: ["support", "admin","super_super_admin", "super_admin"]
  },
  {
    path: "/support/events",
    exact: true,
    layout: DefaultLayout,
    component: ListingInsights,
    access: ["support", "admin","super_super_admin", "super_admin"]
  },
  {
    path: "/support/detail",
    exact: true,
    layout: DefaultLayout,
    component: InsightDetail,
    access: ["support", "admin","super_super_admin", "super_admin"]
  },
  {
    path: "/all-users",
    exact: true,
    layout: DefaultLayout,
    component: AllUsers,
    access: ["super_super_admin","super_admin"]
  },
  {
    path: "/all-marketplace-users",
    exact: true,
    layout: DefaultLayout,
    component: AllMarketPlaceUsers,
    access: ["super_super_admin","super_admin","admin","broker"]
  },
  {
    path: "/all-buyer-users",
    exact: true,
    layout: DefaultLayout,
    component: AllBuyerUsers,
    access: ["support", "admin", "marketplace_admin", "marketplace_user","super_super_admin", "super_admin"]
  },
  {
    path: "/broker-orders",
    exact: true,
    layout: DefaultLayout,
    component: SpecificOrders,
    access: ["admin", "buyer", "seller", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/broker-orders/:id",
    exact: true,
    layout: DefaultLayout,
    component: BrokerDetailsPage,
    access: ["admin", "buyer", "seller", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/marketplace-orders/:id",
    exact: true,
    layout: DefaultLayout,
    component: OrderDetails,
    access: ["admin", "buyer", "seller", "marketplace_admin", "marketplace_user","super_super_admin", "super_admin"]
  },
  {
    path: "/specific-orders",
    exact: true,
    layout: DefaultLayout,
    component: SpecificOrders,
    access: ["admin", "buyer", "seller", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/orders",
    exact: true,
    layout: DefaultLayout,
    component: Orders,
    access: ["support", "admin", "marketplace_admin", "marketplace_user", "seller", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/orders-substitute",
    exact: true,
    layout: DefaultLayout,
    component: OrdersSubstitute,
    access: ["support", "admin", "marketplace_admin", "marketplace_user", "seller", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/marketplace-orders",
    exact: true,
    layout: DefaultLayout,
    component: MarketplaceOrders,
    access: ["support", "admin", "marketplace_admin", "marketplace_user", "seller", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/payout-connection",
    exact: true,
    layout: DefaultLayout,
    component: PayoutConnection,
    access: ["buyer", "seller","super_super_admin"]
  },
  {
    path: "/payout-connection/success",
    exact: true,
    layout: DefaultLayout,
    component: PayoutConnectionSuccess,
    access: ["buyer", "seller", "admin", "marketplace_admin","super_super_admin", "super_admin"]
  },
  {
    path: "/payout-connection/settings",
    exact: true,
    layout: DefaultLayout,
    component: PayoutConnectionSettings,
    access: ["buyer", "seller","super_super_admin"]
  },
  {
    path: "/auction-events",
    exact: true,
    layout: DefaultLayout,
    component: AuctionEvents,
    access: ["admin", "support", "marketplace_admin","super_super_admin", "super_admin"]
  },
  {
    path: "/user/:id",
    exact: true,
    layout: DefaultLayout,
    component: EditAccountAsAdmin,
    access: ["support", "admin", "marketplace_admin", "broker","super_super_admin","super_admin"]
  },
  {
    path: "/purchases",
    exact: true,
    layout: DefaultLayout,
    component: Purchases
  },
  {
    path: "/purchase/success",
    exact: true,
    layout: DefaultLayout,
    component: PurchaseSuccess
  },
  {
    path: "/purchase/cancelled",
    exact: true,
    layout: DefaultLayout,
    component: PurchaseCancelled
  },
  {
    path: "/purchase/create-order",
    exact: true,
    layout: DefaultLayout,
    component: PurchaseCreateOrder
  },
  {
    path: "/purchase/create-order/:id",
    exact: true,
    layout: DefaultLayout,
    component: PurchaseCreateOrder
  },
  {
    path: "/purchase/purchase-detail/:id",
    exact: true,
    layout: DefaultLayout,
    component: PurchaseOrderDetail
  },
  {
    path: "/ebayAuthAccepted",
    exact: true,
    layout: DefaultLayout,
    component: EbayAuthAccepted
  },
  {
    path: "/ebayAuthRejected",
    exact: true,
    layout: DefaultLayout,
    component: EbayAuthRejected
  },
  {
    path: "/marketing",
    exact: true,
    layout: DefaultLayout,
    component: Marketing,
    access: ["marketplace_admin", "marketplace_user","super_super_admin"]
  },
  {
    path: "/events",
    layout: DefaultLayout,
    exact: true,
    component: AddEvents,
    access: ["admin", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/events/:find",
    layout: DefaultLayout,
    exact: true,
    component: AddEvents,
    access: ["admin", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/privacy-policy",
    exact: true,
    layout: DefaultLayout,
    component: PrivacyPolicy,
    access: ["support", "admin", "marketplace_admin", "marketplace_user", "seller", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/terms-conditon",
    exact: true,
    layout: DefaultLayout,
    component: TermsAndConditions,
    access: ["support", "admin", "marketplace_admin", "marketplace_user", "seller", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/seller-agreement",
    exact: true,
    layout: DefaultLayout,
    component: SellerAgreement,
    access: ["support", "admin", "marketplace_admin", "marketplace_user", "seller", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/operation",
    layout: DefaultLayout,
    component: Operation,
    access: ["admin", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/accounting",
    layout: DefaultLayout,
    component: Accounting,
    access: ["admin", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/seller-profile",
    layout: DefaultLayout,
    component: SellerProfile,
    access: ["admin", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/buyer-profile",
    layout: DefaultLayout,
    component: BuyerProfile,
    access: ["admin", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin", "super_admin"]
  },
  {
    path: "/payouts-receipt",
    layout: DefaultLayout,
    component: PayoutsReceipt,
    access: ["admin", "marketplace_admin", "marketplace_user", "broker", "broker_user","super_super_admin", "super_admin"]
  }, 
  {
    path: "/company",
    exact: true,
    layout: DefaultLayout,
    component: Company,
    access: ["super_super_admin"]
  }/* ,
  {
    path: "/broker-orders/:id",
    exact: true,
    layout: DefaultLayout,
    component: BrokerDetailsPage,
    access: ["admin", "buyer", "seller", "marketplace_admin", "marketplace_user", "broker", "broker_user"]
  }, */
];
