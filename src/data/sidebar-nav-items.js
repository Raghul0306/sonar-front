import React from 'react';
import myticket from '../assets/img/myticket-blue.png';
import myticketWhite from '../assets/img/myticket-white.png';
import operation from '../assets/img/operation-blue.png';
import operationWhite from '../assets/img/operation-white.png';
import insights from '../assets/img/insights-blue.png';
import insightsWhite from '../assets/img/insights-white.png';
import myorder from '../assets/img/myorder-blue.png';
import myorderWhite from '../assets/img/myorder-white.png';
import purchase from '../assets/img/purchase-blue.png';
import purchaseWhite from '../assets/img/purchase-white.png'; 
import remittance from '../assets/img/remittance-blue.png';
import remittanceWhite from '../assets/img/remittance-white.png'; 
import events from '../assets/img/events-blue.png';
import eventsWhite from '../assets/img/events-white.png';   
import users from '../assets/img/users-blue.png';
import usersWhite from '../assets/img/users-white.png';   
import accounting from '../assets/img/accounting-blue.png';
import accountingWhite from '../assets/img/accounting-white.png'; 
import reports from '../assets/img/reports-blue.png';
import reportsWhite from '../assets/img/reports-white.png'; 
// market place
import marketInsight from '../assets/market-place-icon/market-place-insights.png'; 
import marketInsightWhite from '../assets/market-place-icon/market-place-insights-active.png'; 
import marketOrder from '../assets/market-place-icon/market-order.png'; 
import marketOrderWhite from '../assets/market-place-icon/market-order-active.png'; 
import marketSeller from '../assets/market-place-icon/market-seller.png'; 
import marketSellerWhite from '../assets/market-place-icon/market-seller-active.png'; 
import marketBuyer from '../assets/market-place-icon/market-buyer.png'; 
import marketBuyerWhite from '../assets/market-place-icon/market-buyer-active.png'; 
import marketPayout from '../assets/market-place-icon/market-payout.png'; 
import marketPayoutWhite from '../assets/market-place-icon/market-payout-active.png'; 
import marketEvent from '../assets/market-place-icon/market-event.png'; 
import marketEventWhite from '../assets/market-place-icon/market-event-active.png'; 
import marketMarketing from '../assets/market-place-icon/market-marketing.png'; 
import marketMarketingWhite from '../assets/market-place-icon/market-marketing-active.png'; 
import marketMarketplace from '../assets/market-place-icon/market-marketplace.png'; 
import marketMarketplaceWhite from '../assets/market-place-icon/market-marketplace-active.png'; 
import marketUser from '../assets/market-place-icon/market-user.png'; 
import marketUserWhite from '../assets/market-place-icon/market-user-active.png'; 
import marketMyticket from '../assets/market-place-icon/market-myticket.png'; 
import marketMyticketWhite from '../assets/market-place-icon/market-myticket-active.png'; 
import marketOperation from '../assets/market-place-icon/market-operation.png'; 
import marketOperationWhite from '../assets/market-place-icon/market-operation-active.png'; 
import marketMyorder from '../assets/market-place-icon/market-myorder.png'; 
import marketMyorderWhite from '../assets/market-place-icon/market-myorder-active.png'; 
import marketPurchase from '../assets/market-place-icon/market-purchase.png'; 
import marketPurchaseWhite from '../assets/market-place-icon/market-purchase-active.png'; 
import marketRemittance from '../assets/market-place-icon/market-remittances.png'; 
import marketRemittanceWhite from '../assets/market-place-icon/market-remittances-active.png'; 
import marketAccounting from '../assets/market-place-icon/market-accounting.png'; 
import marketAccountingWhite from '../assets/market-place-icon/market-accounting-active.png'; 
import marketSellerReport from '../assets/market-place-icon/market-seller-report.png'; 
import marketSellerReportWhite from '../assets/market-place-icon/market-seller-report-active.png'; 
import { userRolesLower } from '../constants/constants';

export default function ({ userRole, id }) {
  if(userRole === userRolesLower.marketplace_user){
  return [
      {
        title: "Marketplace Insights",
        to: "/home",
        htmlBefore: `<img class="inactive-icon" src= ${marketInsight} alt="icon"/>
        <img class="active-icon" src= ${marketInsightWhite} alt="icon"/>`,
        htmlAfter: ""
      },
      {
        title: "Orders",
        htmlBefore: `<img class="inactive-icon" src= ${marketOrder} alt="icon"/>
        <img class="active-icon" src= ${marketOrderWhite} alt="icon"/>`,
        to: "/marketplace-orders",
        access: ["marketplace_user", "admin", "marketplace_admin"]
      },
      {
        title: "Payouts",
        htmlBefore:  `<img class="inactive-icon" src= ${marketPayout} alt="icon"/>
        <img class="active-icon" src= ${marketPayoutWhite} alt="icon"/>`,
        to: "/payouts"
      },
      {
        title: "All Buyers",
        htmlBefore: `<img class="inactive-icon" src= ${marketBuyer} alt="icon"/>
        <img class="active-icon" src= ${marketBuyerWhite} alt="icon"/>`,
        to: "/all-buyer-users",
        access: ["marketplace_user", "admin", "marketplace_admin"]
      },
      {
        title: "Events",
        htmlBefore: `<img class="inactive-icon" src= ${marketEvent} alt="icon"/>
        <img class="active-icon" src= ${marketEventWhite} alt="icon"/>`,
        to: "/events",
        access: ["marketplace_user"]
      },
      {
        title: "Marketing",
        htmlBefore: `<img class="inactive-icon" src= ${marketMarketing} alt="icon"/>
        <img class="active-icon" src= ${marketMarketingWhite} alt="icon"/>`,
        to: "/marketing",
        access: ["marketplace_user"]
      },
      {
        title: "Marketplace Reports",
        htmlBefore: `<img class="inactive-icon" src= ${marketMarketplace} alt="icon"/>
        <img class="active-icon" src= ${marketMarketplaceWhite} alt="icon"/>`,
        to: `/marketplace-reports`,
        
      },
      {
        title: "MyTickets",
        htmlBefore: `<img class="inactive-icon" src= ${marketMyticket} alt="icon"/>
        <img class="active-icon" src= ${marketMyticketWhite} alt="icon"/>`,
        to: "/listings"
      },
      {
        title: "MyOrders",
        htmlBefore: `<img class="inactive-icon" src= ${marketMyorder} alt="icon"/>
        <img class="active-icon" src= ${marketMyorderWhite} alt="icon"/>`,
        to: "/broker-orders",
        access: ["buyer", "marketplace_user", "marketplace_admin"]
      },      
      {
        title: "Purchase Orders",
        htmlBefore: `<img class="inactive-icon" src= ${marketPurchase} alt="icon"/>
        <img class="active-icon" src= ${marketPurchaseWhite} alt="icon"/>`,
        to: "/purchases"
      },
      {
        title: "Reports",
        htmlBefore: `<img class="inactive-icon" src= ${marketSellerReport} alt="icon"/>
        <img class="active-icon" src= ${marketSellerReportWhite} alt="icon"/>`,
        to: "/reports"
      }
    ]
  }else if(userRole === userRolesLower.marketplace_admin){
    return  [
      {
        title: "Marketplace Insights",
        to: "/home",
        htmlBefore: `<img class="inactive-icon" src= ${marketInsight} alt="icon"/>
        <img class="active-icon" src= ${marketInsightWhite} alt="icon"/>`,
        htmlAfter: ""
      },
      {
        title: "Orders",
        htmlBefore: `<img class="inactive-icon" src= ${marketOrder} alt="icon"/>
        <img class="active-icon" src= ${marketOrderWhite} alt="icon"/>`,
        to: "/marketplace-orders",
        access: ["support", "admin", "marketplace_admin"]
      },
      {
        title: "Payouts",
        htmlBefore:  `<img class="inactive-icon" src= ${marketPayout} alt="icon"/>
        <img class="active-icon" src= ${marketPayoutWhite} alt="icon"/>`,
        to: "/payouts"
      },
      {
        title: "All Buyers",
        htmlBefore: `<img class="inactive-icon" src= ${marketBuyer} alt="icon"/>
        <img class="active-icon" src= ${marketBuyerWhite} alt="icon"/>`,
        to: "/all-buyer-users",
        access: ["support", "admin", "marketplace_admin"]
      },
      {
        title: "Events",
        htmlBefore: `<img class="inactive-icon" src= ${marketEvent} alt="icon"/>
        <img class="active-icon" src= ${marketEventWhite} alt="icon"/>`,
        to: "/events",
        access: ["marketplace_admin"]
      },
      {
        title: "Marketing",
        htmlBefore: `<img class="inactive-icon" src= ${marketMarketing} alt="icon"/>
        <img class="active-icon" src= ${marketMarketingWhite} alt="icon"/>`,
        to: "/marketing",
        access: ["marketplace_admin"]
      },
      {
        title: "Marketplace Reports",
        htmlBefore: `<img class="inactive-icon" src= ${marketMarketplace} alt="icon"/>
        <img class="active-icon" src= ${marketMarketplaceWhite} alt="icon"/>`,
        to: `/marketplace-reports`,
      },
      {
        title: "MyTickets",
        htmlBefore: `<img class="inactive-icon" src= ${marketMyticket} alt="icon"/>
        <img class="active-icon" src= ${marketMyticketWhite} alt="icon"/>`,
        to: "/listings"
      },
      {
        title: "MyOrders",
        htmlBefore: `<img class="inactive-icon" src= ${marketMyorder} alt="icon"/>
        <img class="active-icon" src= ${marketMyorderWhite} alt="icon"/>`,
        to: "/broker-orders",
        access: ["buyer", "seller", "marketplace_admin"]
      },      
      {
        title: "Purchase Orders",
        htmlBefore: `<img class="inactive-icon" src= ${marketPurchase} alt="icon"/>
        <img class="active-icon" src= ${marketPurchaseWhite} alt="icon"/>`,
        to: "/purchases"
      },
      {
        title: "Remittances",
        htmlBefore: `<img class="inactive-icon" src= ${marketRemittance} alt="icon"/>
        <img class="active-icon" src= ${marketRemittanceWhite} alt="icon"/>`,
        to: "/remittance",
        access: ["support", "admin", "marketplace_admin"]
      },
      {
        title: "Hobby Shop",
        htmlBefore: '<i class="material-icons">storefront</i>',
        to: "/hobbyshop",
        access: ["buyer", "seller", "admin"]
      },
      {
        title: "Remittance",
        htmlBefore: `<img class="inactive-icon" src= ${marketRemittance} alt="icon"/>
        <img class="active-icon" src= ${marketRemittanceWhite} alt="icon"/>`,
        to: `/remittance/${id}`,
        access: ["buyer", "seller"]
      },
      {
        title: "Seller Reports",
        htmlBefore: `<img class="inactive-icon" src= ${marketSellerReport} alt="icon"/>
        <img class="active-icon" src= ${marketSellerReportWhite} alt="icon"/>`,
        to: "/reports"
      }
    ]
  }else {
    return [
      {
        title: "MyTickets",
        htmlBefore: `<img class="inactive-icon" src= ${myticket} alt="icon"/>
        <img class="active-icon" src= ${myticketWhite} alt="icon"/>`,
        to: "/listings",
        access: ["broker","broker_user","admin","marketplace_user","super_super_admin","super_admin","processing_admin","processing_user","accounting_admin","accounting_user","admin","buyer","support_admin","support_user"]
      },
      {
        title: "Operations",
        htmlBefore: `<img class="inactive-icon" src= ${operation} alt="icon"/>
        <img class="active-icon" src= ${operationWhite} alt="icon"/>`,
        to: "/operation",
        access: ["buyer", "seller","admin", "marketplace_admin", "breaks_admin", "broker","broker_user","marketplace_user","super_super_admin","super_admin"]
      },
      {
        title: "Insights",
        to: "/home",
        htmlBefore: `<img class="inactive-icon" src= ${insights} alt="icon"/>
        <img class="active-icon" src= ${insightsWhite} alt="icon"/>`,
        htmlAfter: "",
        access: ["buyer","seller","admin","marketplace_admin","marketplace_user","breaks_admin","broker","broker_user","super_super_admin","super_admin"]
      },
      {
        title: "MyOrders",
        htmlBefore: `<img class="inactive-icon" src= ${myorder} alt="icon"/>
        <img class="active-icon" src= ${myorderWhite} alt="icon"/>`,
        to: "/broker-orders",
        access: ["buyer", "seller","admin", "marketplace_admin", "breaks_admin", "broker","broker_user","marketplace_user","super_super_admin","super_admin"]
      },
      {
        title: "Purchase Orders",
        htmlBefore: `<img class="inactive-icon" src= ${purchase} alt="icon"/>
        <img class="active-icon" src= ${purchaseWhite} alt="icon"/>`,
        to: "/purchases",
        access: ["buyer","seller","admin","marketplace_admin","marketplace_user","breaks_admin","broker","broker_user","super_super_admin","super_admin"
        ]
      },
      {
        title: "Payouts",
        htmlBefore: `<img class="inactive-icon" src= ${remittance} alt="icon"/>
        <img class="active-icon" src= ${remittanceWhite} alt="icon"/>`,
        to: "/payouts",
        access: ["buyer", "seller", "breaks_admin", "marketplace_user","super_super_admin","super_admin"]
      },
      {
        title: "Remittance",
        htmlBefore: `<img class="inactive-icon" src= ${remittance} alt="icon"/>
        <img class="active-icon" src= ${remittanceWhite} alt="icon"/>`,
        to: `/remittance/${id}`,
        access: ["buyer", "seller", "breaks_admin", "broker","broker_user","marketplace_user","super_super_admin","super_admin"]
      },
      {
        title: "Marketplace Reports",
        htmlBefore: `<img class="inactive-icon" src= ${reports} alt="icon"/>
        <img class="active-icon" src= ${reportsWhite} alt="icon"/>`,
        to: `/marketplace-reports`,
        access: ["marketplace_user","super_super_admin","super_admin"]
      },
      {
        title: "Remittances",
        htmlBefore: `<img class="inactive-icon" src= ${remittance} alt="icon"/>
        <img class="active-icon" src= ${remittanceWhite} alt="icon"/>`,
        to: "/remittance",
        access: ["support", "admin", "marketplace_admin","super_super_admin","super_admin"]
      },     
      {
        title: "Accounting",
        htmlBefore: `<img class="inactive-icon" src= ${accounting} alt="icon"/>
        <img class="active-icon" src= ${accountingWhite} alt="icon"/>`,
        to: "/accounting",
        access: ["support", "admin", "marketplace_admin","super_super_admin","super_admin"]
      },
      {
        title: "Events",
        htmlBefore: `<img class="inactive-icon" src= ${events} alt="icon"/>
        <img class="active-icon" src= ${eventsWhite} alt="icon"/>`,
        to: "/events",
        access: ["marketplace_user", "admin", "marketplace_admin","broker","broker_user","super_super_admin","super_admin"]
      },
      {
        title: "Company",
        htmlBefore: `<img class="inactive-icon" src= ${events} alt="icon"/>
        <img class="active-icon" src= ${eventsWhite} alt="icon"/>`,
        to: "/company",
        access: ["super_super_admin"]
      },
      {
        title: "All Sellers",
        htmlBefore: `<img class="inactive-icon" src= ${users} alt="icon"/>
        <img class="active-icon" src= ${usersWhite} alt="icon"/>`,
        to: "/all-users",
        access: ["super_super_admin","super_admin"]
      },
      {
         title: "All Buyers",
        htmlBefore: `<img class="inactive-icon" src= ${users} alt="icon"/>
        <img class="active-icon" src= ${usersWhite} alt="icon"/>`,
        to: "/all-buyer-users",
        access: ["support", "marketplace_admin","marketplace_user","super_super_admin","super_admin"]
      },
      {
        title: "Users",
        htmlBefore: `<img class="inactive-icon" src= ${users} alt="icon"/>
        <img class="active-icon" src= ${usersWhite} alt="icon"/>`,
        to: "/all-marketplace-users",
        access: ["super_super_admin","super_admin","admin","broker"]
      },
      {
        title: "Orders",
         htmlBefore: `<img class="inactive-icon" src= ${myorder} alt="icon"/>
        <img class="active-icon" src= ${myorderWhite} alt="icon"/>`,
        to: "/marketplace-orders",
        access: ["support", "marketplace_admin","marketplace_user","super_super_admin","super_admin"]
      },
      {
        title: "Reports",
        htmlBefore: `<img class="inactive-icon" src= ${reports} alt="icon"/>
        <img class="active-icon" src= ${reportsWhite} alt="icon"/>`,
        to: "/reports",
        access: [
          "buyer",
          "seller",
          "admin",
          "marketplace_admin",
          "marketplace_user",
          "breaks_admin",
          "broker","broker_user","super_super_admin",
          "super_admin"
        ]
      },
      {
        title: "Marketing",
        htmlBefore: `<img class="inactive-icon" src= ${myticket} alt="icon"/>
        <img class="active-icon" src= ${myticketWhite} alt="icon"/>`,
        to: "/marketing",
        access: ["marketplace_user","super_super_admin"]
      },
    ]
  }
}
