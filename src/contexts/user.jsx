import React from "react";

// set the defaults
export const UserContext = React.createContext({
  user: null,
  setLanguage: () => {},
  setUser: () => {},
  changeRole: ()=> {},
  switchUser:() => {},
  logout:() => {},
  userStats: null
});

export default UserContext;
