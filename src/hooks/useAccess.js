import { useContext, useState, useLayoutEffect } from "react";
import UserContext from "../contexts/user.jsx";

/**
 * Check if user have permission
 * @param {array} access - array of access role
 * @returns {boolean}
 */
const useAccess = access => {
  const [isAllow, setIsAllow] = useState();
  const {
    user
  } = useContext(UserContext);

  let userRole;
  if(user) userRole = user.userRole;
  else return false;

  useLayoutEffect(() => {
    if (access && access.length) {
      if(userRole && userRole.length){
       const currentIsAllow = access.lastIndexOf(userRole.toLowerCase());
       setIsAllow(currentIsAllow >= 0);
      }
    }
  }, []);

  if (!access || !access.length) return true;

  return isAllow === undefined ? true : isAllow;
};

export default useAccess;
