
import axios from 'axios';
import Cookies from "universal-cookie";
import history from '../helpers/history';

export function setHeaderToken(){
    axios.interceptors.request.use(function (config) {
        let cookies = new Cookies;
        let token = cookies.get("accessToken");
        if(token) config.headers.Authorization = `Bearer ${token}`;
        return config;
    });
    axios.interceptors.response.use(
    (response) => {
        return response;
    },
    function (error) {
        if(error && error.response && error.response.status === 401){
            let cookies = new Cookies;
            
            cookies.set("userObject", '', { path: "/", maxAge: 0 });
            cookies.set("accessToken", '', { path: "/", maxAge: 0 });

            history.push('/');
            window.open('/','_self');
        }            
    })
}