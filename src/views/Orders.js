import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
const OrdersGrid = loadable(() => import("../components/datagrid/OrdersGrid"));
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const Orders = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      <Row noGutters className="page-header">
        <PageTitle
          title={t("orders.admin.title")}
          subtitle={t("title")}
          className="text-sm-left mb-3"
        />
      </Row>
      <Row>
        <Col className="col-12">
          <OrdersGrid />
        </Col>
      </Row>
    </Container>
  ) : (
    <Loader />
  );
};

export default Orders;
