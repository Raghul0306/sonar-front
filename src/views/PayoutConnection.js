import React, { useContext, useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row } from "shards-react";
import UserContext from "../contexts/user.jsx";
import { Redirect } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
const EbayAuth = loadable(() => import("../components/ebayAuth/ebayAuth"));
const PageTitle = loadable(() => import("../components/common/PageTitle"));

function PayoutConnection() {
  const { t } = useTranslation();
  const user = useContext(UserContext);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(false);
  }, []);
  if (user.user.stripeId && user.user.ebayAccessToken) {
    return <Redirect to="/payout-connection/settings" />;
  } else {
    return !loading ? (
      <Container fluid className="main-content-container pl-db-50">
        {/* Page Header */}
        <Row noGutters className="page-header">
          <PageTitle
            sm="4"
            title={t("payout.title")}
            subtitle={t("title")}
            className="text-sm-left"
          />
        </Row>
        {/* Default Light Table */}
        <Row>Payout Connection view Goes Here</Row>
        <Row>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://connect.stripe.com/express/oauth/authorize?response_type=code&amp;client_id=ca_GKWbVzboeoDqvXrt0y9kiTZI5PsMeSzK&amp;scope=read_write"
            style={{
              display: "inline-block",
              marginBottom: "1px",
              backgroundImage: "linear-gradient(#28a0e5,#015e94)",
              border: 0,
              padding: "1px",
              height: "32px",
              textDecoration: "none",
              borderRadius: "4px",
              boxShadow: "0 1px 0 rgba(0,0,0,.2)",
              cursor: "pointer",
              userSelect: "none"
            }}
          >
            <span
              style={{
                display: "block",
                position: "relative",
                padding: "0 12px",
                height: "30px",
                background: "#1275ff",
                backgroundImage: "linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4)",
                fontSize: "15px",
                lineHeight: "30px",
                color: "#fff",
                fontWeight: "700",
                fontFamily: "Helvetica Neue,Helvetica,Arial,sans-serif",
                textShadow: "0 -1px 0 rgba(0,0,0,.2)",
                webkitBoxShadow: "inset 0 1px 0 hsla(0,0%,100%,.25)",
                boxShadow: "inset 0 1px 0 hsla(0,0%,100%,.25)",
                borderRadius: "3px",
                paddingLeft: "44px"
              }}
            >
              {t("payout.connect")}
            </span>
          </a>
        </Row>
        <Row>
          <EbayAuth />
        </Row>
      </Container>
    ) : (
      <Loader />
    );
  }
}

export default PayoutConnection;
