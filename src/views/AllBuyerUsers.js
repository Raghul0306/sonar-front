import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
const AllBuyerUsersGrid = loadable(() =>
  import("../components/datagrid/AllBuyerUsersGrid")
);
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const AllBuyerUsers = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      <Row noGutters className="page-header">
        <PageTitle
          title={t("buyers.title")}
          subtitle={t("title")}
          className="text-sm-left mb-3 col-sm-12"
        />
      </Row>
      <Row>
        <Col className="col-12">
          <AllBuyerUsersGrid />
        </Col>
      </Row>
    </Container>
  ) : (
    <Loader />
  );
};

export default AllBuyerUsers;
