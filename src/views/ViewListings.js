import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
/* const MyAddListings = loadable(() =>
  import("../components/listings/addListing")
); */
const MyViewListings = loadable(() =>
  import("../components/listings/viewListing")
);


const ViewListings = ({
  match: {
    params: { id }
  }
}) => {

  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      
      {id && id.length > 0 ? 
        <MyViewListings
        listId={id} 
        />
       : 
         <ViewListings />
      }

    </Container>
  ) : (
    <Loader />
  );
};

export default ViewListings;
