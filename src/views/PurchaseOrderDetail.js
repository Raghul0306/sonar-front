import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
import { useHistory } from "react-router-dom";

import styled from 'styled-components'
import { IosArrowLeft } from '@styled-icons/fluentui-system-filled/IosArrowLeft';
import { getPurchaseOrderDetails, getPurchaseOrderLists, getAllPaymentGroups, getAllPaymentTags, getAllPaymentTypes } from "../services/purchaseOrderService";
import { getAllTicketingSystemList } from "../services/listingsService";
import { getCountries } from "../services/userService";
const Leftarrow = styled(IosArrowLeft)`
  color: #2C91EE;
  width:18px;
  height:18px;
  `;

const PageTitle = loadable(() => import("../components/common/PageTitle"));

const Vendor = loadable(() =>
  import("../components/purchases/Vendor")
);
const PurchaseInfo = loadable(() =>
  import("../components/purchases/PurchaseInfo")
);
const PurchaseDetails = loadable(() =>
  import("../components/purchases/PurchaseDetails")
);
const OrderAction = loadable(() =>
  import("../components/purchases/OrderAction")
);

const PurchaseDetailGrid = loadable(() =>
  import("../components/datagrid/PurchaseDetailGrid")
);
const OrderNotes = loadable(() =>
  import("../components/purchases/OrderNotes")
);


function PurchaseOrderDetail(props) {

  const { t } = useTranslation();
  //const user = useContext(UserContext);
  const [loading, setLoading] = useState(true);
  const [orderDetails, setDetails] = useState({});
  const [ticket_systems, setTicket_systems] = useState({});
  const [ticket_systems_table, setTicket_systems_table] = useState({});
  const [allCountries_list, setCountriesList] = useState({});
  const [getPurchaseOrderID, setPurchaseOrderID] = useState(props.match.params.id);
  const [getPaymentMethods, setPaymentMethods] = useState([]);
  const [getPaymentTypes, setPaymentTypes] = useState([]);
  const [getPaymentTags, setPaymentTags] = useState([]);
  const history = useHistory();
  useEffect(() => {
    setLoading(false);
    purchaseOrderDetails();
    history.push({ pageTitle: t("purchases.orderDetails"), pageSubTitle: t("title"), redirectPath: 'purchases' });
  }, []);
  const purchaseOrderDetails = async () => {
    let details = await getPurchaseOrderDetails(getPurchaseOrderID);
    let ticket_systems_list = await getAllTicketingSystemList();
    let ticket_systems_table = await getPurchaseOrderLists(getPurchaseOrderID);
    let paymentMethods = await getAllPaymentGroups();
    let paymentTypes = await getAllPaymentTypes();
    let allPaymentTags = await getAllPaymentTags();
    let countries_list = await getCountries();
    setDetails(details);
    setTicket_systems(ticket_systems_list);
    setTicket_systems_table(ticket_systems_table);
    setCountriesList(countries_list);
    setPaymentMethods(paymentMethods);
    setPaymentTypes(paymentTypes);
    setPaymentTags(allPaymentTags);
  }

  const purchaseOrderDetailsUpdate = async () => {
    purchaseOrderDetails();
  }

  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      <div
        style={{
          display: 'grid',
          alignItems: 'center'
        }}>
        {/* <Row noGutters className="page-header pb-2 pt-4">
          <div style={{ display: "flex", alignItems: "center", width: "100%" }}>
            <PageTitle
              title={t("purchases.orderDetails")}
              subtitle={t("title")}
              className="text-sm-left col-sm-12"
            />
          </div>
        </Row> */}
      </div>
      <div className="purchase-details-grid-3 grid-divi-item">
        <PurchaseInfo onChildUpdate={(e) => purchaseOrderDetailsUpdate()} purchaseInfo={orderDetails} orderId={getPurchaseOrderID} />
        <PurchaseDetails onChildUpdate={(e) => purchaseOrderDetailsUpdate()} purchaseInfo={orderDetails} ticketSystem={ticket_systems} paymentMethodList={getPaymentMethods} paymentTypeList={getPaymentTypes} />
        <OrderAction onAttachmentUpdate={(e) => purchaseOrderDetailsUpdate()} orderInfo={orderDetails} />
      </div>
      <div className="purchase-order-notes grid-note-item">
        <Vendor onChildUpdate={(e) => purchaseOrderDetailsUpdate()} vendorInfo={orderDetails} countries={allCountries_list} />
        <OrderNotes onChildUpdate={(e) => purchaseOrderDetailsUpdate()} orderInfo={orderDetails} payTags={getPaymentTags} />
      </div>
      <div className="purchase-order-table">
        <PurchaseDetailGrid onChildUpdate={(e) => purchaseOrderDetailsUpdate()} orderList={ticket_systems_table} />
      </div>
      {/* Default Light Table */}

    </Container>
  ) : (
    <Loader />
  );
}

export default PurchaseOrderDetail;