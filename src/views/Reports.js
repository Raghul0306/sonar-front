import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col,CardHeader } from "shards-react";
import Card from 'antd/es/card';
import Button from 'antd/es/button';
import Icon from 'antd/es/icon';
import DatePicker from 'antd/es/date-picker';
import TimePicker from 'antd/es/time-picker';
import Select from 'react-select'
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
import iconInhand from "../assets/myTicketsIconCalendar.png";
import iconTime from "../assets/MyTickets-icon_clock.png";
import { useHistory } from "react-router-dom";
import moment from "moment";
import { CSVLink } from "react-csv"; 
import { reports } from "../constants/constants";
import {getInventoryReport,getPurchaseReport,getSalesReport} from "../services/reportingService.js";
import lodashMap from 'lodash/map';
import lodashGet from 'lodash/get';
const ReportGrid = loadable(() =>
  import("../components/datagrid/ReportGrid")
);

const ScheduleReport = loadable(() =>
  import("../components/reports/ScheduleReport")
);


function disabledEndDate(current,currentStartDate) { 
  return current && (current < moment(currentStartDate).endOf() || current > moment().endOf());
}

function disableStartDate(current,currentEndDate) { 
  return current && current > moment().endOf();
}


const Reports = (props) => {
  const { t } = useTranslation();
  const [startDate, setStartDate] = useState(0);
  const [downloadRepType,setDownloadRepType] = useState('');
  const [endDate, setEndDate] = useState(0);
  const [loading, setLoading] = useState(true);
  //const [openModal, setOpenModal] = useState(false);
  const [reportLoading, setReportLoading] = useState(false);
  const [reportData, setReportData] = useState([]);
  const [reportDataHeader, setReportDataHeader] = useState([]);
  
  const [frequency, setFrequency] = useState();
  const [scheduleDay, setScheduleDay] = useState();
  const [scheduleTime, setScheduleTime] = useState();

  let downloadDisable = !downloadRepType || !startDate || !endDate;
  let scheduleBtnDisable = downloadDisable || !scheduleDay || !scheduleTime;
  
  useEffect(() => {
    setLoading(false);
  }, []);

  useEffect(() => {
    if(!downloadDisable){
      downloadReport();
    }
  }, [downloadDisable,downloadRepType,startDate,endDate]);

 
  const downloadReport = async () => {

    if(!downloadDisable){
      setReportLoading(true)
      
      if(downloadRepType === "inventory_report") {
        let inventoryReportResult = await getInventoryReport({startDate,endDate});
        let {headers = [],inventoryReportData = []} = lodashGet(inventoryReportResult,'data.data.getInventoryReport');
        setExcelData(headers,inventoryReportData[0]);
      }    

      if(downloadRepType == "purchase_report") {
        let purchaseReportResult = await getPurchaseReport({startDate,endDate});
        let {headers = [],purchaseReportData = []} = lodashGet(purchaseReportResult,'data.data.getPurchaseReport')
        setExcelData(headers,purchaseReportData[0]);
      }

      if(downloadRepType == "sales_report") {
        let salesReportResult = await getSalesReport({startDate,endDate});
        let {headers = [],salesReportData = []} = lodashGet(salesReportResult,'data.data.getSalesReport')
        setExcelData(headers,salesReportData[0]);
      }

      setReportLoading(false)
    }     
    
  };

  const setExcelData = (headers,data) => {
    setReportData(data);
    setReportDataHeader(headers);
  }
  /*
    Note: We need this functions for schedule modal
    const handleCancel = () => {
      setOpenModal(!openModal);
    };

    const handleOk = () => {
      setOpenModal(!openModal);
    };
  */


  function dropdownFilterOption ({ label }, string) {
    if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  }



  const reportList = lodashMap(reports.reportType,report=>report);

  const frequencyList = reports.reportFrequency;

  const history = useHistory();
  const setHistoryPageTitle = () => {
    if (!history.location.pageTitle) {       
        history.push({ pageTitle: t("reports.title"), pageSubTitle: t("title") });
    }
  }
  if (!history.location.pageTitle) {
    setHistoryPageTitle();
  }

  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      
      <Col sm={12}>
        <Row>
          <Card className="down-schedule-report" style={{ width: "100%" }}>
            <CardHeader>
              <h5 className="card-title secondary-font-family">Download and Schedule Reports</h5>
            </CardHeader>
            <div
              style={{
                display: "grid",
                gridTemplateColumns: "repeat(4, 1fr)",
                width: "100%",
                marginTop: "10px",
                marginBottom: "10px",
                alignItems: "flex-end"
              }}
            >
            <div className="report-custom-field">
              <label className="form-label-userprofile">Report Type <span>*</span></label>
              <Select              
                placeholder={t("reports.scheduleReport.reportType")}
                style={{ width: "100%" }}
                name="reportType"
                value={reportList.find(report=>report.value === downloadRepType)}
                filterOption={dropdownFilterOption}
                onChange={(e)=>{
                  setDownloadRepType(e?e.value:null);
                }}
                options={reportList}
                allowClear
                showSearch
                isClearable
                />
            </div>
              <div className="report-custom-field">
                <label className="form-label-userprofile">
                  {t("form.startDate")}<span>*</span>
                </label>
                <div className="calendar-icon-field">
                  <span className="calendar-icon">                    
                    <img src={iconInhand}/>
                  </span>
                  <DatePicker size="sm" placeholder="DD/MM/YYYY" disabledDate={(current)=>disableStartDate(current,endDate)} onChange={e =>{
                     setStartDate(e?e._d:null);
                    }} />
                </div>
              </div>
              <div className="report-custom-field">
                <label className="form-label-userprofile">
                  {t("form.endDate")}<span>*</span>
                </label>
                <div className="calendar-icon-field">
                  <span className="calendar-icon">                   
                    <img src={iconInhand}/>
                  </span>
                  
                  <DatePicker size="sm" placeholder="DD/MM/YYYY" disabledDate={(current)=>disabledEndDate(current,startDate)} onChange={e =>{
                     setEndDate(e?e._d:null);
                    }} />
                </div>
              </div>
              
              <div className="report-custom-field">
                  <CSVLink
                    filename={`${downloadRepType} ${moment(startDate).format("MM.DD.YYYY")}-${moment(
                      endDate
                    ).format("MM.DD.YYYY")}.csv`}
                    data={reportData}
                    headers={reportDataHeader}
                  > {
                    reportLoading ?  
                    <Icon type="loading" style={{ fontSize: 32 }} spin /> : 
                    <Button type="primary" disabled={downloadDisable}
                      className={downloadDisable?"inactive":"active"} >
                       {t("reports.download")}
                    </Button>
                    }
                  </CSVLink>
              </div>
              <div className="report-custom-field text-danger mt-2">
                {reportData.length == 0 && !downloadDisable? "There is no data for selected date range.":"" }
              </div>
              
            </div>
            <div
              style={{
                display: "grid",
                gridTemplateColumns: "repeat(4, 1fr)",
                width: "100%",
                marginTop: "2rem",
                marginBottom: "10px",
                alignItems: "flex-end"
              }}
            >
            <div className="report-custom-field">
              <label className="form-label-userprofile">Frequency <span>*</span></label>
              <Select
                placeholder={"Frequency"}
                style={{ width: "100%" }}
                name="frequency"
                value={frequencyList.find(currentFrequency=>currentFrequency.value === frequency)}
                filterOption={dropdownFilterOption}
                onChange={(e)=>setFrequency(e?e.value:null)}
                options={frequencyList}
                allowClear
                showSearch
                isClearable
              />
            </div>
              <div className="report-custom-field">
                <label className="form-label-userprofile">
                {t("reports.scheduleReport.Schedule")} <span>*</span>
                </label>
                <div className="calendar-icon-field">
                  <span className="calendar-icon">                   
                    <img src={iconInhand}/>
                  </span>
                  
                  <DatePicker size="sm" placeholder="DD/MM/YYYY" onChange={e => setScheduleDay(e?e._d:null)} />
                </div>
              </div>
              <div className="report-custom-field">
                <label className="form-label-userprofile">
                {t("reports.scheduleReport.ScheduleTime")} <span>*</span>
                </label>
                <div className="calendar-icon-field">
                  <span className="calendar-icon">
                    <img src={iconTime}/>
                  </span>
                  <TimePicker size="sm" onChange={e => setScheduleTime(e?e._d:null)} />
                </div>
              </div>
              <div className="report-custom-field">
          
              <Button
                className={!scheduleBtnDisable?"active":"inactive"}
                disabled={scheduleBtnDisable}
                type="primary"
              >
                {t("reports.schedule")}
              </Button>

              {/* 
              Note : Schedule modal
              <Modal className="edit-report-modal"
                title={t("reports.scheduleReport.title")}
                visible={openModal}
                onOk={handleOk}
                footer={null}
                onCancel={handleCancel}
                closable={true}
                width="800px"
                maskClosable={false}
                >
                <ScheduleReport
                  onSubmit={handleOk}
                  handleCancel={handleCancel}
                />
              </Modal> */}
              </div>
            </div>
          </Card>
        </Row>
        <Row>
        </Row>
      </Col>
      <Row>
        <Col>
          <div className="scheduled-report--sec">
            <ReportGrid />
          </div>
        </Col>
      </Row>
    </Container>
  ) : (
    <Loader />
  );
};

export default Reports;
