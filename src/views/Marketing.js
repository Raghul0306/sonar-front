import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const Marketing = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      <Row noGutters className="page-header">
        <PageTitle
          sm="4"
          title={t("marketing.title")}
          subtitle={t("title")}
          className="text-sm-left"
        />
      </Row>
      {/* Default Light Table */}
      <div style={{ display: "inline-flex" }}>Marketing page coming soon</div>
    </Container>
  ) : (
    <Loader />
  );
};

export default Marketing;
