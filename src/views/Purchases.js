import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
import { useHistory } from "react-router-dom";
import { buttonProps } from "../constants/constants";
const UserPurchases = loadable(() =>
  import("../components/purchases/userPurchases")
);
const Purchases = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  const setHistoryPageTitle = () => {
    if (!history.location.pageTitle) {
      history.push({ pageTitle: t("purchases.title"), pageSubTitle: t("title"), buttonProps: buttonProps.purchase });
    }
  }
  if (!history.location.pageTitle) {
    setHistoryPageTitle();
  }
  useEffect(() => {
    setLoading(false);
  }, []);

  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      <UserPurchases />
    </Container>
  ) : (
    <Loader />
  );
};

export default Purchases;
