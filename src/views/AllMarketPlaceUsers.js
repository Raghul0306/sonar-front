import React, { useState, useEffect, useContext, useRef } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import Modal from 'antd/es/modal';
import UserContext from "../contexts/user.jsx";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers";
import * as yup from "yup";
import { registerUser, childUserRoles, findEmailExists } from "../services/userService";
import { getAllCompanies } from "../services/companyService";
import { sendAccountCreationEmail, updateUser } from "../services/userService";
import Loader from "../components/Loader/Loader";
import { userRolesLower, userRolesNames } from '../constants/constants';
import Select from 'react-select'
import { Confirm, Info } from "../components/modal/common-modal";
import { useHistory, withRouter, Link } from "react-router-dom";

const AllMarketPlaceUsersGrid = loadable(() =>
  import("../components/datagrid/AllMarketPlaceUsersGrid")
);
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const AllUsers = () => {

  const { t } = useTranslation();
  const [addUserVisible, setAddUserVisible] = useState(false);
  const [loading, setLoading] = useState(true);
  let [userValue, setState] = useState('text');
  let [modalHeaderTitle, setModalHeaderTitle] = useState("Add User");


  const { user } = useContext(UserContext);
  const history = useHistory();
  const setHistoryPageTitle = () => {
    if (!history.location.pageTitle) {
      history.push({ pageTitle: t("users.title"), pageSubTitle: t("title") })
    }
  }
  if (!history.location.pageTitle) {
    setHistoryPageTitle();
  }
  function closeModal() {
    setAddUserVisible(false);
  }

  const [functionToCall, createFunctionToCall] = useState(() => () => { });

  const userAdded = () => {
    functionToCall();
    setAddUserVisible(false);
  }
  const userConfirmation = () => {
    setModalHeaderTitle('Confirm');
  }
  const userAddSuccess = () => {
    setModalHeaderTitle('New User Confirmation');
  }
  const changeTitle = () => {
    setModalHeaderTitle('Add User')
  }


  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <>
      <Container fluid className="main-content-container pl-db-50">
        {/* Page Header */}
        <Row noGutters className="page-header">
          {/* <PageTitle
            title={t("users.title")}
            subtitle={t("title")}
            className="text-sm-left mb-3 col-sm-12"
          /> */}
          {/* Show User Creation ONLY FOR ADMINS */}

          {/* {(user.userRole == "marketplace_admin" || user.userRole == "admin" || user.userRole == "broker") && (
            <div
              onClick={() => {
                setAddUserVisible(true);
              }}
              style={{
                marginLeft: "auto",
                alignSelf: "center",
                fontSize: "36px",
                cursor: "pointer"
              }}
            >
              +
            </div>
            
          />
          )} */}
        </Row>
        <Row>
          <Col className="col-12">
            <AllMarketPlaceUsersGrid
              createFunctionToCall={createFunctionToCall}
              click={(e) => setAddUserVisible(true)} />
          </Col>
        </Row>
      </Container>
      <Modal className="add-user-modal"
        title={modalHeaderTitle}
        visible={addUserVisible}
        footer={null}
        onCancel={closeModal}
        maskClosable={false}
        destroyOnClose={true}
        width="30%"
      >
        <ModalContents onConfirm={userAdded} onVerify={userConfirmation} onCancel={changeTitle} onSuccess={userAddSuccess} />
      </Modal>
    </>
  ) : (
    <Loader />
  );
};

const SignupSchema = yup.object().shape({
  firstName: yup
    .string().required("First Name is required")
    .min(3, 'First Name should be a minimum of 3 characters'),
  lastName: yup
    .string().required("Last Name is required"),
  email: yup
    .string()
    .required("Email is required")
    .email("Invalid Email Format"),
  password: yup
    .string()
    .required("Password is required")
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
      "Must Contain 8 Characters, at least 1 Uppercase, 1 Lowercase, 1 Number and 1 Special Character"
    ),
  phonenumber: yup.string()
    .required("Phone number is required")
    .matches(/^[0-9]+$/, "Phone number must be only digits")
    .min(10, 'Phone number should be a minimum of 10 digits')
    .max(15, 'Phone number should be a maximum of 15 digits')
});

function ModalContents(props) {
  var [role, setRole] = useState();// useState("ADMIN");
  const [userChildRole, setUserRole] = useState('');
  var [company, setCompany] = useState();
  const [userCompanyList, setuserCompanyList] = useState('');
  const [inputValue, setInputValue] = useState("");
  const [emailDuplicate, setEmailDuplicate] = useState(false);
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  //Declare inputs
  let [userFirstName, setUserFirstName] = useState('');
  let [userLastName, setUserLastName] = useState('');
  let [UserEmail, setUserEmail] = useState('');
  let [UserPassword, setUserPassword] = useState('');
  let [UserPhonenumber, setUserPhonenumber] = useState('');
  let [formdata, setformdata] = useState('');
  let [isDisabled, setIsDisabled] = useState(true);
  let [isDisabledOnSubmit, setIsDisabledOnSubmit] = useState(false);

  //Modal popup start
  const [closeButtonModal, setcloseButtonModal] = useState(false);
  const [submitButtonModal, setsubmitButtonModal] = useState(false);

  const [infocloseButtonModal, setinfocloseButtonModal] = useState(false);
  const [infoButtonModal, setinfoButtonModal] = useState(false);

  const [isSubmitted, setIsSubmitted] = useState(false);
  const [newUserId, viewProfileLink] = useState(0);

  useEffect(() => {
    setLoading(false);
    if (!userChildRole) {
      getUserRoles(user.userRole);
    }
    var userId = '';
    if (user.userRole !== userRolesLower.super_super_admin) {
      userId = user.id;
    }
    getUserCompanies(userId);
  }, []);
  const getUserRoles = async (roleName) => {

    let childUserRole = await childUserRoles(roleName);
    const userRoleList = [];
    childUserRole && childUserRole.length && childUserRole.map(items => {
      if (items.keycode != userRolesLower.super_super_admin) {
        let userRoleName = items.user_role_name;
        if (items.keycode == userRolesLower.super_super_admin) {
          userRoleName = userRolesNames.super_super_admin;
        }
        else if (items.keycode == userRolesLower.super_admin) {
          userRoleName = userRolesNames.super_admin;
        }
        else if (items.keycode == userRolesLower.admin) {
          userRoleName = userRolesNames.admin;
        }
        else if (items.keycode == userRolesLower.broker) {
          userRoleName = userRolesNames.broker;
        }
        else if (items.keycode == userRolesLower.broker_user) {
          userRoleName = userRolesNames.broker_user;
        }
        else if (items.keycode == userRolesLower.accounting_admin) {
          userRoleName = userRolesNames.accounting_admin;
        }
        else if (items.keycode == userRolesLower.accounting_user) {
          userRoleName = userRolesNames.accounting_user;
        }
        else if (items.keycode == userRolesLower.processing_admin) {
          userRoleName = userRolesNames.processing_admin;
        }
        else if (items.keycode == userRolesLower.processing_user) {
          userRoleName = userRolesNames.processing_user;
        }
        else if (items.keycode == userRolesLower.support_admin) {
          userRoleName = userRolesNames.support_admin;
        }
        else if (items.keycode == userRolesLower.support_user) {
          userRoleName = userRolesNames.support_user;
        }
        else if (items.keycode == userRolesLower.marketplace_admin) {
          userRoleName = userRolesNames.marketplace_admin;
        }
        else if (items.keycode == userRolesLower.marketplace_user) {
          userRoleName = userRolesNames.marketplace_user;
        }
        userRoleList.push({ label: userRoleName, value: items.id })
      }
    });
    setUserRole(userRoleList);
  }

  const getUserCompanies = async (userId) => {
    let getAllCompanyData = await getAllCompanies(userId);
    const getAllCompanyList = [];
    getAllCompanyData && getAllCompanyData.length && getAllCompanyData.map(items => {
      getAllCompanyList.push({ label: items.company_name, value: items.id })
    });
    setuserCompanyList(getAllCompanyList);
  }



  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(SignupSchema)
  });
  const { user } = useContext(UserContext);

  function handleInputChange(event) {
    setEmailDuplicate(false)
    setUserEmail(event.target.value);
  }
  //Input Filed onchange
  const handleUserFirstName = (e) => {
    setUserFirstName(e.target.value);
  };
  const handleUserLastName = (e) => {
    setUserLastName(e.target.value);
  };
  const handleUserPassword = (e) => {
    setUserPassword(e.target.value);
  };
  const handleUserPhonenumber = (e) => {
    setUserPhonenumber(e.target.value);
  };
  const handleUserRole = (e) => {
    setRole(e)
  }
  const handleUserCompany = (e) => {
    setCompany(e)
  }

  const onSubmit = async data => {
    if (role && company) {
      setcloseButtonModal(true); props.onVerify();
      var loginUserDetails = { createdBy: user.id }
      var addUserData = await { ...data, ...loginUserDetails };
      setformdata(addUserData);
    }
  }


  const handleinfoButtonModal = async (params) => {
    setinfocloseButtonModal(false);
  }

  function dropdownFilterOption({ label }, string) {
    if (string) {
      return label.toLowerCase().includes(string.toLowerCase());
    } else {
      return true;
    }
  };

  const handlesubmitButtonModal = async (params) => {

    setsubmitButtonModal(params);
    // setcloseButtonModal(false);

    if (params) {
      setIsDisabled(true)

      if (!role || !company) {
        return false;
      }
      else {
        role = role.value;
        company = company.value;
      }
      if (role && company) {
        const payload = { ...formdata, userRole: role, company: company };
        let emailExists = await findEmailExists(formdata.email);
        if (emailExists.length) {
          setEmailDuplicate(true);
          setIsDisabled(false);
          setcloseButtonModal(false);
          setIsDisabledOnSubmit(false);
          props.onCancel();
        }
        else {
          if (params === true) {
            setIsDisabledOnSubmit(true);
            let newUser = await registerUser(payload);
            let newUser2 = await updateUser({ ...newUser, userRole: role });
            // send the user an email to let them know they have an account
            await sendAccountCreationEmail(formdata.email, formdata.password);
            setUserFirstName('');
            setUserLastName('');
            setUserPassword('');
            setUserEmail('');
            setUserPhonenumber('');
            setCompany('');
            if (newUser && newUser.userId) {
              viewProfileLink(newUser.userId);
              props.onSuccess();
            }
            setIsDisabled(false);
            return newUser2
              ? ''
              : null;
          }
        }
      }
    }
    else {
      setcloseButtonModal(false);
      props.onCancel();
    }
  }

  const userRoleValidation = async () => {
    setIsDisabled(false);
    setIsSubmitted(true);
  }
  return (
    <div>
      {!closeButtonModal &&
        <form onSubmit={handleSubmit(onSubmit)} autoComplete="nofill" >
          <div className="row">
            <div className="form-group col-md-6">
              <label htmlFor="firstName">First Name</label>
              <input type="text" className="form-control" id="firstName" placeholder="First Name" name="firstName" ref={register} value={userFirstName} onChange={handleUserFirstName}  autoComplete="nofill" />

              <div className="invalid-feedback" style={{ display: errors.firstName && errors.firstName.message ? "block" : 'none' }} >
                <p>{errors.firstName && errors.firstName.message ? errors.firstName.message : ''}</p>
              </div>

            </div>
            <div className="form-group col-md-6">
              <label htmlFor="lastName">Last Name</label>
              <input type="text" className="form-control" id="lastName" placeholder="Last Name" name="lastName" ref={register} value={userLastName} onChange={handleUserLastName}  autoComplete="nofill"/>
              <div className="invalid-feedback" style={{ display: errors.lastName && errors.lastName.message ? "block" : 'none' }} >
                <p>{errors.lastName && errors.lastName.message ? errors.lastName.message : ''}</p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="form-group col-md-6">
              <label htmlFor="email">Email</label>
              <input type="email" className="form-control" id="email" placeholder="Email" name="email" ref={register} value={UserEmail} onChange={handleInputChange}  autoComplete="nofill" />
              <div className="invalid-feedback" style={{ display: errors.email && errors.email.message ? "block" : 'none' }} >
                <p>{errors.email && errors.email.message ? errors.email.message : ''}</p>
              </div>
              <span className="invalid-feedback" style={{ display: emailDuplicate && !errors.email ? "block" : 'none' }} >
                <p> Email is already Exists</p>
              </span>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="password">Password</label>
              <input type="password" className="form-control" id="password" placeholder="Password" name="password" ref={register} value={UserPassword} onChange={handleUserPassword} autoComplete="nofill" />
              <div className="invalid-feedback" style={{ display: errors.password && errors.password.message ? "block" : 'none' }} >
                <p>{errors.password && errors.password.message ? errors.password.message : ''}</p>
              </div>
            </div>
          </div>

          <div className="row justify-content-between">
            <div className="form-group col-md-6">
              <label htmlFor="phonenumber">Phone Number</label>
              <input type="text" className="form-control" id="phonenumber" placeholder="XXX-XXX-XXXX" name="phonenumber" ref={register} value={UserPhonenumber} onChange={handleUserPhonenumber} autoComplete="nofill" />
              <div className="invalid-feedback" style={{ display: errors.phonenumber && errors.phonenumber.message ? "block" : 'none' }} >
                <p>{errors.phonenumber && errors.phonenumber.message ? errors.phonenumber.message : ''}</p>
              </div>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="inputState">Role</label>
              <Select
                {...props}
                placeholder={"Select Role"}
                style={{ width: "100%" }}
                name="role"
                value={role}
                filterOption={dropdownFilterOption}
                options={userChildRole}
                onChange={handleUserRole}
                allowClear
                showSearch
                isClearable
                autoComplete="nofill"
              />
              <div className="invalid-feedback" style={{ display: !role && isSubmitted ? "block" : 'none' }} >
                <p>Role is required</p>
              </div>
            </div>

            <div className="form-group col-md-6">
              <label htmlFor="inputState">Company</label>
              <Select
                {...props}
                placeholder={"Select Company"}
                style={{ width: "100%" }}
                name="company"
                value={company}
                filterOption={dropdownFilterOption}
                options={userCompanyList}
                onChange={handleUserCompany}
                allowClear
                showSearch
                autoComplete="nofill"
                isClearable
              />
              <div className="invalid-feedback" style={{ display: !company && isSubmitted ? "block" : 'none' }} >
                <p>Company is required</p>
              </div>
            </div>

          </div>
          <div className="add--user-item">
            <button type="submit" onClick={(e) => userRoleValidation()} className="btn addnew-btn">Add New User</button>
          </div>
        </form>
      }
      {closeButtonModal && newUserId == 0 &&
        <div className="user--confirmation add-user-popup">
          <span className="are-sure-content">Create a new User</span>
          <button type="button" onClick={(e) => handlesubmitButtonModal(false)} disabled={isDisabledOnSubmit} className="btn addnew-btn user-confirm">Cancel</button>
          <button type="button" onClick={(e) => handlesubmitButtonModal(true)} disabled={isDisabled} className="btn addnew-btn">Submit</button>
        </div>
      }
      {
        newUserId != 0 &&
        <div>
          <span className="are-success-content">A new user has been successfully added.</span>
          <div className="add--user-item">
            <Link className="text-info" to={`/user/${newUserId}`}>
              <button type="submit" onClick={(e) => viewProfileLink(0)} className="btn addnew-btn">{t("userInfo.gotoProfile")}</button>
            </Link>
          </div>

        </div>
      }

    </div>
  );
}

export default AllUsers;
