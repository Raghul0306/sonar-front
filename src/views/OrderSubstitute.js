import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import Card from "antd/es/card";

import { Modal} from 'react-bootstrap';  
import Loader from "../components/Loader/Loader";
import { useHistory } from "react-router-dom";
const TicketSubstitute = loadable(() =>
  import("../components/orders/TicketSubstitute")
);

const OrdersSubstitute = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);

  // modal popup
  const [substitute, setIsOpen] = React.useState(false);

  const showModal = () => {
    setIsOpen(true);
  };

  const hideModal = () => {
    setIsOpen(false);
  };
  const history = useHistory();
  useEffect(() => {
    setLoading(false);
    history.push({ pageTitle: t("substitute.title"), pageSubTitle: t("title"), redirectPath:'broker-orders' });
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      {/* <Row noGutters className="page-header pl-db-50">
        <PageTitle
          title={t("substitute.title")}
          subtitle={t("title")}
          className="text-sm-left mb-3"
        />
      </Row> */}
      <Row>
        <Col className="w-100">
            <Card
          className="order-detail-channel-card substitue-card-sec"
          >
            <div className="event-detail-section">
              <div className="d-flex">
                <h3 className="order-detail-event-title stylefont-weight-bold">Philadelphia Phillies at San Francisco Giants</h3>
                <h5 className="order-detail-event-id stylefont-weight-medium">657483</h5>
              </div>
              <div className="d-flex">
                <h4 className="event-time-date stylefont-weight-bold primary-color">Saturday Jul 24, 2021 @ 7:30 PM PST</h4>
                <h4 className="event-location stylefont-weight-bold pl-3 secondary-color">Oracle Park, San Francisco, CA, USA</h4>
              </div>
              
            </div>
          </Card>
          <TicketSubstitute />
        </Col>
      </Row>
      <Row>
        <Col>
        <div>
          <div className="substitute-grid-button">
            <div className="sub-button-item">
              <button className="btn sub-tick-btn btn-disable" onClick={showModal}>Substitute Tickets</button>
            </div>
            <div className="grid-txt-item">
              <h5>Subsititute tickets within 5 rows of your current order without a penalty charge.</h5>
              <p>Select any ticket from your account that fit the substiution criteria.</p>
            </div>
            <div className="sub-button-item">
              <button className="btn add-list-tick-btn">Add Listing</button>
            </div>
            <div className="grid-txt-item">
              <h5>Can’t find a ticket to substitute?</h5>
              <p>You can always try to buy and add a ticket listing to substitute.</p>
            </div>
          </div>
        </div>
        </Col>
      </Row>
      <div>
         {/* modal popup */}
         <Modal show={substitute} onHide={hideModal} className="substitute-modal" aria-labelledby="contained-modal-title-vcenter" centered backdrop="static">
          <Modal.Header>
            <h5 className="substitute-txt">Your substitution request has been sent!</h5>
          </Modal.Header>
          <Modal.Body>
            <p className="body-txt">We will notify you when your substitution has been confirmed.</p>
          </Modal.Body>
          <Modal.Footer className="myorder-modal-footer">
            <button className="btn btn-primary substitute-myorder-btn" >Go to MyOrders</button>
          </Modal.Footer>
        </Modal>
      </div>
    </Container>
  ) : (
    <Loader />
  );
};

export default OrdersSubstitute;
