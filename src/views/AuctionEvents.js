import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
const AllAuctionEvents = loadable(() =>
  import("../components/auction-events/AllAuctionEvents")
);
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const AuctionEvents = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      <Row noGutters className="page-header">
        <PageTitle
          title={t("auctionEvents.title")}
          subtitle={t("title")}
          className="text-sm-left mb-3"
        />
      </Row>
      <Col sm={12}>
        <AllAuctionEvents />
      </Col>
    </Container>
  ) : (
    <Loader />
  );
};

export default AuctionEvents;
