import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
import { useHistory, withRouter } from "react-router-dom";
const MarketplaceOrdersGrid = loadable(() => import("../components/datagrid/MarketplaceOrdersGrid"));


const MarketplaceOrders = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  useEffect(() => {
    setLoading(false);
    history.push({ pageTitle:t("orders.admin.title"), pageSubTitle: t("title") });
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      <Row noGutters className="page-header">
        {/* <PageTitle
          title={t("orders.admin.title")}
          subtitle={t("title")}
          className="text-sm-left mb-3"
        /> */}
      </Row>
      <Row>
        <Col className="col-12">
          <MarketplaceOrdersGrid />
        </Col>
      </Row>
    </Container>
  ) : (
    <Loader />
  );
};

export default withRouter(MarketplaceOrders);
