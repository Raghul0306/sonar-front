import React, { useEffect, useState, useContext } from "react";
import { Container, Row } from "shards-react";
import UserContext from "../contexts/user.jsx";
import { useTranslation } from "react-i18next";
import loadable from "@loadable/component";
import Cookies from "universal-cookie";
import { Redirect } from "react-router-dom";
import { updateUserEbayAccessAndRefreshToken } from "../services/userService";
import {
  getEbayTokenFromAuthCode,
  createInventoryLocation
} from "../services/ebayService";
const cookies = new Cookies();
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const EbayAuthAccepted = () => {
  const { t } = useTranslation();
  const user = useContext(UserContext);
  const [loading, setLoading] = useState(true);

  /* effectively componentDidMount */
  useEffect(() => {
    async function ebayInit() {
      let search = window.location.search;
      let params = new URLSearchParams(search);
      // get ebay token from auth code and record access & refresh token
      let ebayAuthToken = await getEbayTokenFromAuthCode(
        user.user.ebayClientId,
        user.user.ebayClientSecret,
        user.user.ebayRedirectUri,
        params.get("code")
      );
      console.log("Getting eBay auth token from auth code");
      // record access and refresh token in our database so users don't have to re-auth with ebay everytime
      let newUserObject = await updateUserEbayAccessAndRefreshToken(
        user.user.id,
        ebayAuthToken.access_token,
        ebayAuthToken.refresh_token
      );
      // set the user merchant location and record that as well
      // will always be "firstName-lastName-Warehouse"
      await createInventoryLocation(ebayAuthToken.access_token, user.user);
      // await setDefaultReturnPolicy(ebayAuthToken.access_token);
      // await setDefaultFulfillmentPolicy(ebayAuthToken.access_token);
      // await setDefaultPaymentPolicy(ebayAuthToken.access_token);
      //await updateUserMerchantLocation(user.user.id, merchantLocationKey);
      //user.setUser(newUserObject);
      cookies.set("userObject", newUserObject, { path: "/" });
      setLoading(false);
    }
    ebayInit();
  }, [false]);

  return (
    <Container fluid className="main-content-container pl-db-50">
      <Row
        noGutters
        style={{ padding: "0.5rem 0" }}
        className="page-header"
      >
        <PageTitle
          title={t("userInfo.accountSync.ebay.sync")}
          className="text-sm-left mb-3"
        />
      </Row>
      {!loading ? (
        <Redirect to="/user-profile" />
      ) : (
        <div>{t("userInfo.accountSync.ebay.noCode")}</div>
      )}
    </Container>
  );
};

export default EbayAuthAccepted;
