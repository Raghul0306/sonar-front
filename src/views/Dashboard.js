import React, { useContext, useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import UserContext from "../contexts/user.jsx";
import Loader from "../components/Loader/Loader";
import {  userRolesLower } from '../constants/constants';
import { useHistory } from "react-router-dom";
const QuickSupport = loadable(() =>
  import("../components/insightsDash/QuickSupport")
);
const RevenueCard = loadable(() =>
  import("../components/insightsСards/RevenueCard")
);
const SalesCard = loadable(() =>
  import("../components/insightsСards/SalesCard")
);
const ListingVolumeCard = loadable(() =>
  import("../components/insightsСards/ListingVolumeCard")
);
const MarketplaceSupport = loadable(() =>
  import("../components/insightsСards/MarketplaceSupport")
);
const MarketingCard = loadable(() =>
  import("../components/insightsСards/MarketingCard")
);
const HotSellers = loadable(() =>
  import("../components/insightsСards/HotSellers")
);
const HotBuyers = loadable(() =>
  import("../components/insightsСards/HotBuyers")
);
const MarketplaceGrid = loadable(() =>
  import("../components/insightsСards/MarketplaceGrid")
);
const TopPerformersCard = loadable(() =>
  import("../components/insightsСards/TopPerformersCard")
);
const PortfolioCard = loadable(() =>
  import("../components/insightsСards/PortfolioCard")
);

const MarketplaceChart = loadable(() =>
  import("../components/insightsСards/MarketplaceChart")
);
const LisitingAmountCard = loadable(() =>
  import("../components/insightsСards/ListingAmountCard")
);
const InventoryPie = loadable(() =>
  import("../components/insightsСards/InventoryPie.js")
);
const AllocationPie = loadable(() =>
  import("../components/insightsСards/AllocationPie.js")
);
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const Dashboard = ({ smallStats }) => {
  const [loading, setLoading] = useState(true);
  const { t } = useTranslation();
  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    setLoading(false);
  }, []);
  const history = useHistory();
  const setHistoryPageTitle = () => {
    if (!history.location.pageTitle) {
      history.push({ pageTitle: t("dashboard.title"), pageSubTitle: t("title") });
    }
  }
  if (!history.location.pageTitle) {
    setHistoryPageTitle();
  }
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
     

      {/* This is where the Seller and Admin board will be created */}
      {(user.userRole === userRolesLower.super_super_admin || user.userRole === userRolesLower.super_admin || user.userRole === userRolesLower.admin || user.userRole === userRolesLower.broker || user.userRole === userRolesLower.broker_user) && (
          <div>
            <Row className="dashboard-row">
              <Col sm="12" md="12" lg="12" style={{ padding: "1rem 0 2rem 0" }}>
                <PortfolioCard />
              </Col>
              <Col sm="12" md="12" lg="12" style={{ padding: "0" }}>
                <div className="insights-row">
                  <div className="revenue-sales-listings">
                    <RevenueCard />
                    <SalesCard />
                    <LisitingAmountCard />
                  </div>
                  <Col style={{ padding: "0" }}>
                    <ListingVolumeCard />
                  </Col>
                  <Col style={{ padding: "0" }}>
                    <TopPerformersCard />
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12" md="12" lg="12" style={{ padding: "0" }}>
                <div className="insights-row">
                  <Col style={{ padding: "0", height: "100%" }}>
                    <InventoryPie />
                  </Col>
                  <Col style={{ padding: "0", height: "100%" }}>
                    <AllocationPie />
                  </Col>
                  <Col style={{ padding: "0", height: "100%" }}>
                    <QuickSupport />
                  </Col>
                </div>
              </Col>
            </Row>
          </div>
        )}
      {/* This is where the Marketplace Admin board will be created */}
      {(user.userRole === userRolesLower.marketplace_admin || user.userRole === userRolesLower.marketplace_user) && (
          <div>
            <Row
              noGutters
              style={{ padding: "1rem 0" }}
              className="page-header"
            >
              <PageTitle
                title={""}
                subtitle={t("title")}
                className="text-sm-left mb-3"
              />
            </Row>
            <Row>
              <Col sm="12" md="12" lg="12" style={{ padding: "0" }}>
                <div className="insights-row">
                  <div className="revenue-sales-listings">
                    <h3 className="card-heading">{t("remittance.insights")}</h3>
                    <RevenueCard />
                    <SalesCard />
                    <LisitingAmountCard />
                  </div>
                  <Col style={{ padding: "0" }}>
                    <ListingVolumeCard />
                  </Col>
                  <Col style={{ padding: "0" }}>
                    <MarketplaceSupport />
                  </Col>
                </div>
              </Col>
            </Row>
            <Row>
              <Col
                sm="12"
                md="12"
                lg="12"
                style={{ padding: "0", marginBottom: "10px" }}
              >
                <MarketplaceChart />
              </Col>
            </Row>
            <Row>
              <Col
                sm="12"
                md="12"
                lg="12"
                style={{ padding: "0", marginBottom: "20px" }}
              >
                <MarketplaceGrid />
              </Col>
            </Row>
            <Row style={{ paddingTop: "20px" }}>
              <Col sm="12" md="12" lg="12" style={{ padding: "0" }}>
                <div className="insights-row">
                  <Col style={{ padding: "0" }}>
                    <MarketingCard />
                  </Col>
                  <Col style={{ padding: "0" }}>
                    <HotSellers />
                  </Col>
                  <Col style={{ padding: "0" }}>
                    <HotBuyers />
                  </Col>
                </div>
              </Col>
            </Row>

            <Row>
              <Col sm="12" md="12" lg="12" style={{ padding: "0" }}>
                <div className="insights-row">
                  <Col style={{ padding: "0" }}>
                    <InventoryPie />
                  </Col>
                  <Col style={{ padding: "0" }}>
                    <AllocationPie />
                  </Col>
                  <Col style={{ padding: "0" }}>
                    <QuickSupport />
                  </Col>
                </div>
              </Col>
            </Row>
          </div>
        )}
    </Container>
  ) : (
    <Loader />
  );
};

export default Dashboard;
