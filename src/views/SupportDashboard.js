import React from "react";
import { Container} from "shards-react";
import Card from 'antd/es/card';
import Col from 'antd/es/col';
import Row from 'antd/es/row';
import { Link } from 'react-router-dom';
import {useTranslation } from 'react-i18next';

const SupportDashboard = () => {
  const { t } = useTranslation();
  return (
  <Container fluid className="main-content-container pl-db-50">
   {t('support.title')}
   <div style={{ background: '#ECECEC', padding: '30px' }}>
    <Row gutter={16}>
      <Col lg={8} xs={24}>
        <Card title={t('support.listingsCard.title')} bordered={false}>
            <Link to="/support/listings">{t('support.listingsCard.description')}</Link>
        </Card>
      </Col>
      <Col lg={8} xs={24}>
        <Card title={t('support.venuesCard.title')} bordered={false}>
          {t('support.venuesCard.description')}
        </Card>
      </Col>
      <Col lg={8} xs={24}>
        <Card title={t('support.salesCard.title')} bordered={false}>
          {t('support.salesCard.description')}
        </Card>
      </Col>
    </Row>
    <Row gutter={16} style={{marginTop: "40px"}}>
      <Col lg={8} xs={24}>
        <Card title={t('support.eventsCard.title')} bordered={false}>
        <Link to="/support/listings">{t('support.eventsCard.description')}</Link>
        </Card>
      </Col>
      <Col lg={8} xs={24}>
        <Card title={t('support.brokersCard.title')} bordered={false}>
          {t('support.brokersCard.description')}
        </Card>
      </Col>
      <Col lg={8} xs={24}>
        <Card title={t('support.performersCard.title')} bordered={false}>
          {t('support.performersCard.description')}
        </Card>
      </Col>
    </Row>
  </div>,
  </Container>
)};

export default SupportDashboard;