import React, { useContext, useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row } from "shards-react";
import UserContext from "../contexts/user.jsx";
//import { Redirect } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
import { getStripeCheckoutSessionById } from "../services/stripeService";
import { getListingById, updateListingSold } from "../services/listingsService";
import { createOrder } from "../services/ordersService";
const PageTitle = loadable(() => import("../components/common/PageTitle"));

function PurchaseSuccess() {
  const { t } = useTranslation();
  const user = useContext(UserContext);
  const [loading, setLoading] = useState(true);

  function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  function grabSessionID() {
    return getParameterByName("session_id");
  }

  async function getStripeCheckoutSession(sessionId) {
    return await getStripeCheckoutSessionById(sessionId);
  }

  async function getListing(listingID) {
    return await getListingById(listingID);
  }

  async function writeNewOrderToDB(sessionId, listingId, sellerId) {
    // create transaction we expect
    let transactionConstructor = {
      stripeId: sessionId,
      listingId: listingId,
      sellerId: sellerId,
      buyerId: user.user.id,
    };
    // give back that transaction
    return await createOrder(transactionConstructor);
  }

  useEffect(() => {
    const newSuccessfulOrder = async () => {
      // grab the URL parameter "session_id"
      const sessionId = grabSessionID();
      const stripeCheckoutSession = await getStripeCheckoutSession(sessionId);
      const listing = await getListing(stripeCheckoutSession.metadata.listingID);
      if(!listing.sold) {
        const newOrder = await writeNewOrderToDB(stripeCheckoutSession.id, listing.id, listing.sellerId);
        await updateListingSold(listing.id, true);
      }
      else {
        console.warn("Seems you are hitting this purchase success URL for an already successful order.");
      }
      setLoading(false);
    };
    newSuccessfulOrder();
  }, []);
    return !loading ? (
      <Container fluid className="main-content-container pl-db-50">
        {/* Page Header */}
        <Row noGutters className="page-header">
          <PageTitle
            sm="4"
            title={t("payments.title")}
            subtitle={t("title")}
            className="text-sm-left"
          />
        </Row>
        {/* Default Light Table */}
        <Row>{t("payments.success")}</Row>
      </Container>
    ) : (
      <Loader />
    );
}

export default PurchaseSuccess;