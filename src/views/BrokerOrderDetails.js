import React, { useContext, useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container } from "shards-react";

import { useTranslation } from "react-i18next";
import UserContext from "../contexts/user.jsx";
import { useParams } from 'react-router-dom';
import Loader from "../components/Loader/Loader";
import { getBrokerOrderDetailsById, getChannelLogo } from "../services/ordersService";
import { ticketTypes, responseStatus } from "../constants/constants";
import styled from 'styled-components'
import { IosArrowLeft } from '@styled-icons/fluentui-system-filled/IosArrowLeft';
import { Link } from "react-router-dom";
import { brokerSupportMail } from "../services/usersService";

import Input from 'antd/es/input';
import Form from 'antd/es/form';
import Button from 'antd/es/button';
import Modal from 'antd/es/modal';

import { useHistory, withRouter } from "react-router-dom";

const Leftarrow = styled(IosArrowLeft)`
  color: #2C91EE;
  width:18px;
  height:18px;
`;
const ChannelCards = loadable(() =>
  import("../components/brokerOrders/ChannelCards.js")
);
const EventsListCard = loadable(() =>
  import("../components/brokerOrders/EventsListCard.js")
);
const OrderActionsCard = loadable(() =>
  import("../components/brokerOrders/OrderActionCard.js")
);
const DeliveryStatusCard = loadable(() =>
  import("../components/brokerOrders/DeliveryStatusCard.js")
);
const BuyerinfoCard = loadable(() =>
  import("../components/brokerOrders/BuyerinfoCard.js")
);
const SellerDetailsCard = loadable(() =>
  import("../components/brokerOrders/SellerDetailsCard.js")
);
const PickupInformation = loadable(() =>
  import("../components/brokerOrders/PickupInformation.js")
);

const OrderDetails = ({ smallStats,
  match: {
    params: { id }
  }
}) => {
  const [loading, setLoading] = useState(true);
  const [orderDetails, setDetails] = useState({});
  const [channelLogoData, setChannelLogoData] = useState("");
  const { t } = useTranslation();
  const { user, setUser } = useContext(UserContext);
  const parameters = useParams();

  const pageReload = (data) => {
    setLoading(data);
    GenerateShippingLabel()
    setLoading(false);
  }
  const history = useHistory(); 
  useEffect(() => {
    setLoading(false);
    GenerateShippingLabel();   
  }, []);

  // invoice modal
  const [supportModal, supportIsOpen] = React.useState(false);

  const supportshowModal = () => {
    supportIsOpen(true);
  };
  const supporthideModal = () => {
    supportIsOpen(false);
  };

  const GenerateShippingLabel = async () => {
    let details = await getBrokerOrderDetailsById(id);
    setDetails(details);
    if(details.broker_order_detail && details.broker_order_detail.channel_id){
      await getChannelLogo(details.broker_order_detail.channel_id).then((res) => {
        if(res && res.data)
        {
          setChannelLogoData(res.data)
        }else{
          setChannelLogoData(false)
        }    
      });
    }
    else{
      setChannelLogoData(false)
    }

  }

  const [issueDescription, setIssueDescription] = useState("");
  const submitSupportTicket = async (e) => {
    e.preventDefault();
    if (issueDescription === "") {
      alert("Issue description is required");
      return;
    }
    let response = await brokerSupportMail('Broker Order Details Id: ' + id, issueDescription, user.email);
    if (response && response.status == responseStatus.success) {
      alert("mail send successfully");
      supportIsOpen(false);
    }
    else {
      alert("Please send again later")
    }


    setIssueDescription("");
  };
  return orderDetails && !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      <div
        noGutters
        style={{ padding: "1rem 0" }}
        className="page-header"
      >

        <div style={{ display: "flex", alignItems: "center", width: "100%" }}>
          <Link to={`/broker-orders`}>
            {/* <button className="back-btn btn"><Leftarrow /></button> */}
          </Link>
          {/* <PageTitle
            title={"Order Details"}
            className="text-sm-left mb-3"
          /> */}
        </div>
        <div className="order-grid-view">
          <div className="order-grid-section">
            <ChannelCards orderDetails={orderDetails} logoImageData={channelLogoData} />
            <OrderActionsCard />
          </div>

          {orderDetails.broker_order_detail && orderDetails.broker_order_detail.ticket_type && orderDetails.broker_order_detail.ticket_type[0] && (
            <div className="order-grid-section">
              <EventsListCard eventDetails={orderDetails} />
              <DeliveryStatusCard deliveryDetails={orderDetails} />
              <BuyerinfoCard buyerDetails={orderDetails} />
              {ticketTypes.eTransfer.id !== orderDetails.broker_order_detail.ticket_type[0].id && (
                // .ticket_type_id
                <SellerDetailsCard pageReload={pageReload} attachmentDetails={orderDetails} />
              )}
            </div>
          )}
        </div>
        {orderDetails.ticket_type && (
          <div className="information-detail-section">
            <PickupInformation ticketType={orderDetails.broker_order_detail.ticket_type} />
          </div>
        )}
      </div>
      <div>
        {/* invoice order modal popup */}

        <Modal
          title='Contact Support'
          visible={supportModal}
          onCancel={(e) => supporthideModal()}
          closable={true}
          footer={false}
          destroyOnClose={true}
          className="contact-support"
          maskClosable={false}
        >
          <Form name="support">
            <Form.Item  >
              <span className="purchase-order-form-label">
                Order ID
              </span>
              <Input value={id} name="orderId"
              />
            </Form.Item>
            <Form.Item>
              <span className="purchase-order-form-label">
                {t("support.email")}
              </span>
              <Input name="email" value={user.email} />
            </Form.Item>
            <Form.Item>
              <span className="purchase-order-form-label">
                {t("support.message")}
              </span>
            </Form.Item>
            <Form.Item validateStatus="success">
              <div
                style={{
                  textAlign: "center",
                  marginTop: "10px"
                }}
              >
                < Form.Item>
                  <Input.TextArea onChange={e => setIssueDescription(e.target.value)} value={issueDescription} />
                </Form.Item>
                <Button
                  style={{ marginTop: "15px" }}
                  type="primary"
                  htmlType="submit"
                  className="contact-support-btn primary-bg-color stylefont-weight-bold"
                  onClick={submitSupportTicket}
                >
                  {t("support.contact")}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>

    </Container>
  ) : (
    <Loader />
  );
};

export default withRouter(OrderDetails);
