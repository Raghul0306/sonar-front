import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
import Button from 'antd/es/button';
import Modal from 'antd/es/modal';
import styled from 'styled-components'
import { Envelope } from '@styled-icons/bootstrap/Envelope'
import { Printer } from '@styled-icons/bootstrap/Printer'
import { IosArrowLeft } from '@styled-icons/fluentui-system-filled/IosArrowLeft';
import { useHistory } from "react-router-dom";
const Leftarrow = styled(IosArrowLeft)`
color: #2C91EE;
width:18px;
height:18px;
  `;

const EnvelopeIcon = styled(Envelope)`
  color: #fff;
  width:25px;
  height:25px;
`
const PrinterIcon = styled(Printer)`
  color: #fff;
  width:25px;
  height:25px;
`
const SpecificOrdersGrid = loadable(() =>
  import("../components/datagrid/CompaniesGrid")
);
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const Companies = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  const setHistoryPageTitle = () => {
    if (!history.location.pageTitle) {
      history.push({ pageTitle: t("company.title"), pageSubTitle: t("title") });
    }
  }
  if (!history.location.pageTitle) {
    setHistoryPageTitle();
  }
  useEffect(() => {
    setLoading(false);
  }, []);

  // invoice modal
  const [invoice, invoiceIsOpen] = React.useState(false);

  const invoiceshowModal = () => {
    invoiceIsOpen(true);
  };

  const invoicehideModal = () => {
    invoiceIsOpen(false);
  };

  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      <div noGutters className="page-header">

        <div className="purchase-order-gridgap"
          style={{
            display: 'grid',
            gridTemplateColumns: 'auto',
            alignItems: 'center'
          }}>
          <div style={{ display: "flex", alignItems: "center", width: "100%" }}>
            {/* <button className="back-btn btn"><Leftarrow/></button> */}
            {/* <PageTitle
                title={t("specificOrders.title")}
                subtitle={t("title")}
                className="text-sm-left mb-3"
              /> */}
          </div>

          {/* <div className="top-button-item">
            <button className="view-order-btn" onClick={invoiceshowModal}>Invoice</button>
            <button className="view-order-btn">Retransfer</button>
            <button className="view-order-btn">Need Help?</button>
          </div> */}
        </div>
      </div>
      <Row>
        <Col className="w-100">
          <SpecificOrdersGrid />
        </Col>
      </Row>
      {/* invoice order modal popup */}
      <Modal show={invoice} onHide={invoicehideModal} className="purchase-order-modal" aria-labelledby="contained-modal-title-vcenter" centered backdrop="static">
        <Modal.Header closeButton>
          <h5 className="substitute-txt">Invoice</h5>
        </Modal.Header>
        <modalBody>
          <div className="receipt-item">
            <h5 className="receipt-title">Email Receipt</h5>
            <p className="receipt-txt">Generate an email invoice of this order.</p>
            <Button className="receipt-btn"><EnvelopeIcon /> Generate Email</Button>
          </div>
          <div className="receipt-item">
            <h5 className="receipt-title">Print Receipt</h5>
            <p className="receipt-txt">Download a PDF version of this invoice.</p>
            <Button className="receipt-btn"><PrinterIcon /> Print Receipt</Button>
          </div>
        </modalBody>
      </Modal>
    </Container>
  ) : (
    <Loader />
  );
};

export default Companies;
