import React, { useState, useEffect, useContext } from "react";
import loadable from "@loadable/component";
import { userRolesLower } from "../constants/constants";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import UserContext from "../contexts/user.jsx";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers";
import * as yup from "yup";
import { registerUser, childUserRoles, findEmailExists } from "../services/userService";
import { sendAccountCreationEmail, updateUser } from "../services/userService";
import Loader from "../components/Loader/Loader";
import styled from 'styled-components'
import { IosArrowLeft } from '@styled-icons/fluentui-system-filled/IosArrowLeft';

const Leftarrow = styled(IosArrowLeft)`
  color: #2C91EE;
  width:18px;
  height:18px;
  `;
const BuyerProfileView = loadable(() =>
  import("../components/buyer/BuyerProfileView")
);
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const BuyerProfile = () => {

  const { t } = useTranslation();
  const [addUserVisible, setAddUserVisible] = useState(false);
  const [loading, setLoading] = useState(true);
  let [userValue, setState] = useState('text');
  const { user } = useContext(UserContext);

  function closeModal() {
    setAddUserVisible(false);
  }

  const [functionToCall, createFunctionToCall] = useState(() => () => { });

  const userAdded = () => {
    functionToCall();
    setAddUserVisible(false);
  }
  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <>
      <Container fluid className="main-content-container pl-db-50">
        {/* Page Header */}
        <Row noGutters className="page-header">
          <div style={{display:"flex",alignItems:"center",width:"100%"}}>
            {/* <button className="back-btn btn"><Leftarrow/></button> */}
            <PageTitle
              title={t("buyers.profile")}
              subtitle={t("title")}
              className="text-sm-left mb-3 col-sm-12"
            />
          </div>
          {/* Show User Creation ONLY FOR ADMINS */}
         
        </Row>
        <Row>
          <Col className="col-12">
            <BuyerProfileView />
          </Col>
        </Row>
      </Container>
     
    </>
  ) : (
    <Loader />
  );
};

const SignupSchema = yup.object().shape({
  firstName: yup
    .string().required("First Name is required")
    .min(3, 'First Name should be a minimum of 3 characters'),
  lastName: yup
    .string().required("Last Name is required"),
  email: yup
    .string()
    .required("Email is required")
    .email("Invalid Email Format"),
  password: yup
    .string()
    .required("Password is required")
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
      "Must Contain 8 Characters, at least 1 Uppercase, 1 Lowercase, 1 Number and 1 Special Character"
    )
});

function ModalContents(props) {
  var [role, setRole] = useState();// useState("ADMIN");
  const [userChildRole, setUserRole] = useState('');
  const [inputValue, setInputValue] = useState("");
  const [emailDuplicate, setemailDuplicate] = useState(false);
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  //Declare inputs
  let [userFirstName, setUserFirstName] = useState('');
  let [userLastName, setUserLastName] = useState('');
  let [UserEmail, setUserEmail] = useState('');
  let [UserPassword, setUserPassword] = useState('');
  useEffect(() => {
    setLoading(false);
    if (!userChildRole) {

      getUserRoles(user.userRole);
    }

  }, []);
  const getUserRoles = async (roleName) => {

    let childUserRole = await childUserRoles(roleName);
    setUserRole(childUserRole);
  }

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(SignupSchema)
  });
  const { user } = useContext(UserContext);

  function handleInputChange(event) {
    setemailDuplicate(false)
    setUserEmail(event.target.value);
  }
  //Input Filed onchange
  const handleUserFirstName = (e) => {
    setUserFirstName(e.target.value);
  };
  const handleUserLastName = (e) => {
    setUserLastName(e.target.value);
  };
  const handleUserPassword = (e) => {
    setUserPassword(e.target.value);
  };

  const onSubmit = async data => {

    if (!role) {
      if (user.userRole == userRolesLower.admin) {
        role = 1;
      }
      else if (user.userRole == userRolesLower.broker) {
        role = 3;
      }
      else if (user.userRole == userRolesLower.marketplace_admin) {
        role = 5
      }
    }
    if (role) {
      const payload = { ...data, userRole: role };
      let emailExists = await findEmailExists(data.email);
      if (!emailExists.length) {
        let newUser = await registerUser(payload);
        let newUser2 = await updateUser({ ...newUser, userRole: role });
        // send the user an email to let them know they have an account
        await sendAccountCreationEmail(data.email, data.password);
        props.onConfirm()
        alert("New user added successfully");
        setUserFirstName('');
        setUserLastName('');
        setUserPassword('');
        setUserEmail('');
        // setRole(1);

        return newUser2
          ? ''
          : null;
      }
      else {
        setemailDuplicate(true)
      }

    };
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="row justify-content-between">
        <div className="form-group col-md-6">
          <label for="firstName">First Name</label>
          <input type="text" className="form-control" id="firstName" placeholder="First Name" name="firstName" ref={register} value={userFirstName} onChange={handleUserFirstName} />

          <div className="invalid-feedback" style={{ display: errors.firstName && errors.firstName.message ? "block" : 'none' }} >
            <p>{errors.firstName && errors.firstName.message ? errors.firstName.message : ''}</p>
          </div>

        </div>
        <div className="form-group col-md-6">
          <label for="lastName">Last Name</label>
          <input type="text" className="form-control" id="lastName" placeholder="Last Name" name="lastName" ref={register} value={userLastName} onChange={handleUserLastName} />
          <div className="invalid-feedback" style={{ display: errors.lastName && errors.lastName.message ? "block" : 'none' }} >
            <p>{errors.lastName && errors.lastName.message ? errors.lastName.message : ''}</p>
          </div>
        </div>
      </div>
      <div className="row justify-content-between">
        <div className="form-group col-md-6">
          <label for="email">Email</label>
          <input type="email" className="form-control" id="email" placeholder="Email" name="email" ref={register} value={UserEmail} onChange={handleInputChange} />
          <div className="invalid-feedback" style={{ display: errors.email && errors.email.message ? "block" : 'none' }} >
            <p>{errors.email && errors.email.message ? errors.email.message : ''}</p>
          </div>
          <span className="invalid-feedback" style={{ display: emailDuplicate && !errors.email ? "block" : 'none' }} >
            <p> Email is already Exists</p>
          </span>
        </div>
        <div className="form-group col-md-6">
          <label for="password">Password</label>
          <input type="password" className="form-control" id="password" placeholder="Password" name="password" ref={register} value={UserPassword} onChange={handleUserPassword} />
          <div className="invalid-feedback" style={{ display: errors.password && errors.password.message ? "block" : 'none' }} >
            <p>{errors.password && errors.password.message ? errors.password.message : ''}</p>
          </div>
        </div>
      </div>

      <div className="row justify-content-between">
        <div className="form-group col-md-6">
          <label for="phonenumber">Phone Number</label>
          <input type="text" className="form-control" id="phone" placeholder="Phone Number" name="phone" ref={register} />
          <div class="invalid-feedback" style={{ display: errors.password && errors.password.message ? "block" : 'none' }} >
            <p>{  errors.password && errors.password.message ? errors.password.message:''}</p>
          </div>
        </div>
        <div className="form-group col-md-6">
          <label for="inputState">Role</label>
          <select id="role" name='role' className="form-control" onChange={e => setRole(e.target.value)}
            value={role}
            ref={register}>
            {userChildRole.length > 0
              && userChildRole.map((item, i) => {
                return (
                  <option key={i} value={item.id}>{t("userInfo.roles." + item.user_role_name)}</option>
                )
              })}

          </select>

        </div>

      </div>
      <div className="add--user-item">
        <button type="submit" className="btn addnew-btn">Add New User</button>
      </div>
    </form>
  );
}

export default BuyerProfile;
