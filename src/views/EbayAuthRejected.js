import React from "react";
import { Container, Row } from "shards-react";
import { useTranslation } from "react-i18next";
import loadable from "@loadable/component";
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const EbayAuthRejected = () => {
  const { t } = useTranslation();

  return (
    <Container fluid className="main-content-container pl-db-50">
      <Row
        noGutters
        style={{ padding: "0.5rem 0" }}
        className="page-header"
      >
        <PageTitle
          title={t("userInfo.accountSync.ebay.sync")}
          className="text-sm-left mb-3"
        />
      </Row>
      <div>{t("userInfo.accountSync.ebay.rejected")}</div>
    </Container>
  );
};

export default EbayAuthRejected;
