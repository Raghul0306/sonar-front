import React from "react";
import { Container, Button } from "shards-react";
import {useTranslation } from 'react-i18next';
const Errors = () => {
  const { t } = useTranslation();
  return(
  <Container fluid className="main-content-container">
    <div className="error">
      <div className="error__content">
        <h2>500</h2>
        <h3>{t('error.title')}</h3>
        <p>{t('error.subtitle')}</p>
        <Button pill>{t('error.back')}</Button>
      </div>
    </div>
  </Container>
);}

export default Errors;
