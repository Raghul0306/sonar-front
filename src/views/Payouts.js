import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
const PayoutsGrid = loadable(() =>
  import("../components/datagrid/PayoutsGrid")
);
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const Payouts = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      <Row noGutters className="page-header">
        <PageTitle
          title={t("payouts.title")}
          subtitle={t("title")}
          className="text-sm-left mb-3"
        />
      </Row>
      <Row>
        <Col className="col-12">
          <PayoutsGrid />
        </Col>
      </Row>
    </Container>
  ) : (
    <Loader />
  );
};

export default Payouts;
