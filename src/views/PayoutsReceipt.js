import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
import styled from 'styled-components'
import { IosArrowLeft } from '@styled-icons/fluentui-system-filled/IosArrowLeft';
import { useHistory } from "react-router-dom";

const Leftarrow = styled(IosArrowLeft)`
  color: #2C91EE;
  width:18px;
  height:18px;
  `;

const PayoutReceiptView = loadable(() =>
  import("../components/payouts/PayoutReceiptView")
);
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const PayoutsReceipt = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  useEffect(() => {
    setLoading(false);
    history.push({ pageTitle: t("payouts.payout_receipt"), pageSubTitle: t("title"), redirectPath: 'home' });
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      <Row noGutters className="page-header">
        {/* <div style={{display:"flex",alignItems:"center",width:"100%"}}>
            <button className="back-btn btn"><Leftarrow/></button>
              <PageTitle
              title={t("payouts.payout_receipt")}
              subtitle={t("title")}
              className="text-sm-left mb-3"
            />
          </div> */}

      </Row>
      <Row>
        <Col className="col-12">
          <PayoutReceiptView />
        </Col>
      </Row>
    </Container>
  ) : (
    <Loader />
  );
};

export default PayoutsReceipt;
