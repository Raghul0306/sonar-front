import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
//import UserContext from "../contexts/user.jsx";
//import { Redirect } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";

import styled from 'styled-components'
import { IosArrowLeft } from '@styled-icons/fluentui-system-filled/IosArrowLeft';
import { useHistory, withRouter } from "react-router-dom";
const Leftarrow = styled(IosArrowLeft)`
color: #2C91EE;
width:18px;
height:18px;
  `;
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const CreatePurchaseOrder = loadable(() =>
  import("../components/purchases/createPurchaseOrder")
);


function PurchaseCreateOrder() {
  const { t } = useTranslation();
  //const user = useContext(UserContext);
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  useEffect(() => {
    setLoading(false);
    history.push({ pageTitle: t("purchases.add"), pageSubTitle:t("title"), redirectPath: 'purchases' });
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      <Row noGutters className="page-header">

        <div style={{ display: "flex", alignItems: "center", width: "100%" }}>
          {/* <Link to="/purchases"> <button className="back-btn btn"><Leftarrow /></button></Link>
          <PageTitle
            title={t("purchases.add")}
            subtitle={t("title")}
            className="text-sm-left col-sm-12"
          /> */}
        </div>
      </Row>
      <Row>
        <Col md="12">
          <CreatePurchaseOrder />
        </Col>
      </Row>
      {/* Default Light Table */}
    </Container>
  ) : (
    <Loader />
  );
}

export default withRouter(PurchaseCreateOrder);