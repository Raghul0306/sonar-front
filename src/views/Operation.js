import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
import { useHistory } from "react-router-dom";
const OperationFilter = loadable(() =>
  import("../components/operation/operationFilter")
);

const Operation = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  const setHistoryPageTitle = () => {
    if (!history.location.pageTitle) {
      history.push({ pageTitle: t("Operation.title"), pageSubTitle: t("title") });
    }
  }
  if (!history.location.pageTitle) {
    setHistoryPageTitle();
  }
  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* <Row noGutters className="page-header">
        <PageTitle
          title={t("Operation.title")}
          subtitle={t("title")}
          className="text-sm-left mb-3"
        />
      </Row> */}

      <OperationFilter />
    </Container>
  ) : (
    <Loader />
  );
};

export default Operation;
