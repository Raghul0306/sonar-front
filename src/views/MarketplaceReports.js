import React, { useState, useEffect, useContext, useRef } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import Card from 'antd/es/card';
import Select from 'antd/es/select';
import Button from 'antd/es/button';
import Modal from 'antd/es/modal';
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
import UserContext from "../contexts/user.jsx";
import getFormattedDate from "../helpers/getFormattedDate";
import {
  filterAllLiveListingsByDate,
  filterAllDeliveredListingsByDate,
  filterAllOpenListingsByDate,
  filterAllListingsLinkedToPurchaseOrderByDate,
  getNumberOfUserListings
} from "../services/listingsService";
import { getPurchaseOrderById } from "../services/purchaseOrderService";
import { getAllUsers, getBuyerUsers } from "../services/usersService";
import DatePicker from "antd/es/date-picker";
import moment from "moment";
import { CSVLink } from "react-csv";
import Icon from "antd/es/icon";
import { marketPlaceReportTypes } from "../constants/constants";

const PageTitle = loadable(() => import("../components/common/PageTitle"));
const ScheduleReport = loadable(() =>
  import("../components/reports/ScheduleReport")
);
const { Option } = Select;

const MarketplaceReports = () => {
  const { t } = useTranslation();
  const [startDate, setStartDate] = useState(0);
  const [endDate, setEndDate] = useState(0);
  const [loading, setLoading] = useState(true);
  const [openModal, setOpenModal] = useState(false);
  const [type, setType] = useState("sales");
  const [plType, setPlType] = useState("item");
  const [reportLoading, setReportLoading] = useState(false);
  const [reportData, setReportData] = useState([]);
  const { user } = useContext(UserContext);
  const csvDownload = useRef();

  useEffect(() => {
    setLoading(false);
  }, []);
  // formats the data for csv file for sales reports
  // function setCsvData(data) {
  //   console.log(data);
  //   const csvData =
  //     data.length > 0
  //       ? data.map(d => {
  //           return {
  //             "Order ID": d.id,
  //             "Transition Date": moment(d.createdAt).format("MM-DD-YYYY"),
  //             cancelled: d.cancelled ? "True" : "False",
  //             StripeId: d.stripeId,
  //             shippingLabelId: d.shippingLabelId,
  //             cognitoUserId: d.cognitoUserId,
  //             buyerName:
  //               d.buyerByBuyerId.firstName + " " + d.buyerByBuyerId.lastName,
  //             buyerPhone: d.buyerByBuyerId.phone,
  //             buyerEmail: d.buyerByBuyerId.email,
  //             buyerStreetAddress:
  //               d.buyerByBuyerId.streetAddress +
  //               " " +
  //               d.buyerByBuyerId.streetAddress2 +
  //               " ",
  //             buyerState: d.buyerByBuyerId.state,
  //             buyerCity: d.buyerByBuyerId.city,
  //             buyerCountry: d.buyerByBuyerId.country,
  //             buyerPostalCode: d.buyerByBuyerId.postalCode,
  //             listingDelivered: d.listingByListingId.delivered
  //               ? "true"
  //               : "false",
  //             listingId: d.listingByListingId.id,
  //             listingName: d.listingByListingId.name,
  //             listingPrice: d.listingByListingId.price,
  //             listingPurchaseOrderId: d.listingByListingId.purchaseOrderId
  //           };
  //         })
  //       : [];
  //   setReportData(csvData);
  // }
  // formats the data for csv file for listings report
  function setListingCsvData(data) {
    const csvData =
      data.length > 0
        ? data.map(d => {
            const data = {
              "Card Name": d.name + "",
              "Card Number": d.cardByCardId.cardNumber + "",
              Brand: d.cardByCardId.setBySet.brandByBrandId.name + "",
              "Set Description": d.cardByCardId.setBySet.description + "",
              Set: d.cardByCardId.setBySet.name + "",
              Year: d.cardByCardId.setBySet.year + "",
              "Card Image": d.image + "",
              "Grade Type": d.gradeType + "",
              Grade: d.grade + "",
              "Grade Attribute": d.gradeAttribute + "",
              "Reserve Price": d.reserve + "",
              Cost: d.cost + "",
              Auction: d.isAuction + "",
              Sold: d.sold + "",
              Inactive: d.inactive + "",
              "Purchase Order ID": d.purchaseOrderId + "",
              "Ebay Listing ID": d.ebayOrderId + "",
              "Ebay Markup": d.ebayMarkup + ""
            };
            delete data.cardByCardId;
            return data;
          })
        : [];
    setReportData(csvData);
  }
  // formats the data for csv file for purchase order report
  // function setPurchaseOrderCsvData(data, vendorNames) {
  //   console.log(data);
  //   console.log(vendorNames);
  //   const csvData =
  //     data.length > 0
  //       ? data.map((d, i) => {
  //           const data = {
  //             "Purchase Order ID": d.id,
  //             Amount: d.purchaseAmount,
  //             Currency: d.purchaseCurrency,
  //             "Payment Method": d.paymentMethod,
  //             "Last 4": d.paymentCardLast4,
  //             Vendor: vendorNames[i] ? vendorNames[i].name : "",
  //             createdAt: moment(d.createdAt).format("MM-DD-YYYY"),
  //             "Purchase Date": moment(d.purchaseDate).format("MM-DD-YYYY")
  //           };
  //           return data;
  //         })
  //       : [];
  //   setReportData(csvData);
  // }

  // function setDeliveryReports(data) {
  //   const csvData =
  //     data.length > 0
  //       ? data.map(d => {
  //           const data = {
  //             ...d
  //           };
  //           return data;
  //         })
  //       : [];
  //   setReportData(csvData);
  // }

  function setDeliveryOpenReports(data) {
    const csvData =
      data.length > 0
        ? data.map(d => {
            const data = {
              ...d,
              cardNumber: d.cardByCardId.cardNumber,
              cardSetBrandName: d.cardByCardId.setBySet.brandByBrandId.name,
              cardSetDescription: d.cardByCardId.setBySet.description,
              cardSetName: d.cardByCardId.setBySet.name,
              cardSetYear: d.cardByCardId.setBySet.year
            };
            delete data.cardByCardId;
            return data;
          })
        : [];
    setReportData(csvData);
  }
  function setPLCsvData(data) {
    if (plType === marketPlaceReportTypes.plCSVType) {
      const csvData =
        data.length > 0
          ? data.map(d => {
              const data = {
                ...d,
                cardNumber: d.cardByCardId.cardNumber,
                cardSetBrandName: d.cardByCardId.setBySet.brandByBrandId.name,
                cardSetDescription: d.cardByCardId.setBySet.description,
                cardSetName: d.cardByCardId.setBySet.name,
                cardSetYear: d.cardByCardId.setBySet.year,
                profit: d.price - d.cost
              };
              delete data.cardByCardId;
              return data;
            })
          : [];
      setReportData(csvData);
    }
  }

  // gets sales report form backend
  async function getMarketSalesReport() {
    if (startDate === 0 || endDate === 0) {
      alert("Please enter start date and end date");
      return;
    }
    let data;
    setReportLoading(true);
    data = await filterAllLiveListingsByDate(
      getFormattedDate(startDate),
      getFormattedDate(endDate)
    );
    setReportLoading(false);
    setListingCsvData(data);
    if (data && data.length > 0) {
      csvDownload.current.link.click();
    } else {
      window.alert("No reports found with the given start and end date.");
    }
    return data;
  }
  async function getMarketDeliveryReport() {
    if (startDate === 0 || endDate === 0) {
      alert("Please enter start date and end date");
      return;
    }
    let data;
    setReportLoading(true);
    data = await filterAllDeliveredListingsByDate(
      user.id,
      getFormattedDate(startDate),
      getFormattedDate(endDate)
    );
    setReportLoading(false);
    // setDeliveryReports(data);
    // if (data && data.length > 0) {
    //   csvDownload.current.link.click();
    // } else {
    //   window.alert("No reports found with the given start and end date.");
    // }
    // return data;
  }
  async function getMarketDeliveryOpenReport() {
    if (startDate === 0 || endDate === 0) {
      alert("Please enter start date and end date");
      return;
    }
    let data;
    setReportLoading(true);
    data = await filterAllOpenListingsByDate(
      getFormattedDate(startDate),
      getFormattedDate(endDate)
    );
    setReportLoading(false);
    setDeliveryOpenReports(data);
    if (data && data.length > 0) {
      csvDownload.current.link.click();
    } else {
      window.alert("No reports found with the given start and end date.");
    }
    return data;
  }
  async function getMarketProfitLossReport() {
    if (startDate === 0 || endDate === 0) {
      alert("Please enter start date and end date");
      return;
    }
    let data;
    setReportLoading(true);
    data = await filterAllListingsLinkedToPurchaseOrderByDate(
      getFormattedDate(startDate),
      getFormattedDate(endDate)
    );

    const purchaseOrderPromises = data.map(async d => {
      const op = await getPurchaseOrderById(d.purchaseOrderId);
      return op;
    });
    const purchaseOrders = await Promise.all(purchaseOrderPromises);
    setReportLoading(false);
    setPLCsvData(data);
    if (plType === marketPlaceReportTypes.plCSVType) {
      if (data && data.length > 0) {
        csvDownload.current.link.click();
      } else {
        window.alert("No reports found with the given start and end date.");
      }
    }
    return data;
  }

  async function getMarketListingsReport() {
    if (startDate === 0 || endDate === 0) {
      alert("Please enter start date and end date");
      return;
    }
    let data;
    setReportLoading(true);
    data = await filterAllLiveListingsByDate(
      getFormattedDate(startDate),
      getFormattedDate(endDate)
    );
    setReportLoading(false);
    setListingCsvData(data);
    if (data && data.length > 0) {
      csvDownload.current.link.click();
    } else {
      window.alert("No reports found with the given start and end date.");
    }
    return data;
  }

  function setMarketUserReports(users, userListingCounts, buyers) {
    const csvBuyersData =
      buyers.length > 0
        ? buyers.map(d => {
            const purchases = d.transactionsByBuyerId.nodes.length;
            const purchaseRevenue = d.transactionsByBuyerId.nodes
              .map(t => t.listingByListingId.price)
              .reduce((a, b) => a + b, 0);
              
            const aov = purchaseRevenue / purchases;
            const ltv = aov * purchases * 3;
            const data = {
              ...d,
              purchases,
              purchases_revenue: purchaseRevenue,
              average_order_value: aov,
              life_time_value: ltv
            };
            delete data.transactionsByBuyerId;
            return data;
          })
        : [];
    const csvUsersData =
      users.length > 0
        ? users.map((d, i) => {
            const data = {
              user_id: d.id,
              user_firstname: d.firstName,
              user_lastname: d.lastName,
              user_fullname: d.fullName,
              user_email: d.email,
              user_about: d.about,
              user_phone: d.phone,
              user_stripeId: d.stripeId,
              user_role: d.userRole,
              user_no_of_listings: userListingCounts[i]
            };
            return data;
          })
        : [];

    setReportData([...csvUsersData, ...csvBuyersData]);
  }

  async function getMarketUserReports() {
    if (startDate === 0 || endDate === 0) {
      alert("Please enter start date and end date");
      return;
    }
    let data;
    setReportLoading(true);
    const allUsers = await getAllUsers();
    const countOfUserListingsPromise = allUsers.map(
      async v => await getNumberOfUserListings(v.id)
    );
    const countOfUserListings = await Promise.all(countOfUserListingsPromise);
    const allBuyers = await getBuyerUsers();
    data = [...allUsers, ...allBuyers];
    setMarketUserReports(allUsers, countOfUserListings, allBuyers);
    setReportLoading(false);
    if (data && data.length > 0) {
      csvDownload.current.link.click();
    } else {
      window.alert("No reports found with the given start and end date.");
    }
    return data;
  }
  // download report button
  const downloadReport = () => {
    if (type && type.length === 0) {
      alert("Please select report type");
      return;
    }

    if (type === marketPlaceReportTypes.sales) {
      getMarketSalesReport();
    }
    if (type === marketPlaceReportTypes.listings) {
      getMarketListingsReport();
    }
    if (type === marketPlaceReportTypes.delivery) {
      getMarketDeliveryReport();
    }
    if (type === marketPlaceReportTypes.deliveryOpen) {
      getMarketDeliveryOpenReport();
    }
    if (type === marketPlaceReportTypes.plType) {
      getMarketProfitLossReport();
    }
    if (type === marketPlaceReportTypes.user) {
      getMarketUserReports();
    }
  };

  const handleCancel = () => {
    setOpenModal(!openModal);
  };
  const handleOk = () => {
    setOpenModal(!openModal);
  };

  function handleChange(value) {
    setType(value);
  }
  function handlePlChange(value) {
    setPlType(value);
  }
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      <Row noGutters className="page-header">
        <PageTitle
          title={t("reports.title")}
          subtitle={t("title")}
          className="text-sm-left mb-3"
        />
      </Row>
      <Col sm={12}>
        <Row>
          {/* If you need to change your stripe payout settings (Bank account,
          contact information, etc)
          <a href={stripeURL}>click here</a> */}
          <Card bordered={false} style={{ width: "100%" }}>
            <div>
              <Select
                bordered={false}
                style={{ width: 250 }}
                values={type}
                defaultValue="sales"
                onChange={handleChange}
              >
                <Option value="sales">Sales Report</Option>
                <Option value="listings">Listings Report</Option>
                <Option value="pl">Profit & Loss Report</Option>
                <Option value="delivery-open">Open Orders</Option>
                <Option value="delivery">Delivery Report</Option>
                <Option value="users-reports">User Reports</Option>
              </Select>
              {type === marketPlaceReportTypes.plType && (
                <Select
                  bordered={false}
                  style={{ width: 150, marginLeft: "20px" }}
                  values={plType}
                  defaultValue="item"
                  onChange={handlePlChange}
                >
                  <Option value="item">Item</Option>
                  <Option value="monthly">Monthly</Option>
                  <Option value="annualy">Annualy</Option>
                </Select>
              )}
            </div>
            <div
              style={{
                display: "grid",
                gridTemplateColumns: "1fr 1fr",
                width: "400px",
                marginTop: "10px",
                marginBottom: "10px"
              }}
            >
              <div>
                <label className="form-label-userprofile">
                  {t("form.startDate")}
                </label>
                <DatePicker size="sm" onChange={e => setStartDate(e._d)} />
              </div>
              <div>
                <label className="form-label-userprofile">
                  {t("form.endDate")}
                </label>
                <DatePicker size="sm" onChange={e => setEndDate(e._d)} />
              </div>
            </div>
            <div>
              {reportLoading ? (
                <Icon type="loading" style={{ fontSize: 32 }} spin />
              ) : (
                <>
                  <Button type="primary" onClick={downloadReport}>
                    {t("reports.download")}
                  </Button>
                  <CSVLink
                    filename={`${
                      type === marketPlaceReportTypes.plType
                        ? "Profit & Loss"
                        : type === marketPlaceReportTypes.purchase
                        ? "Purchase Order"
                        : type.replace(/(^\w{1})|(\s+\w{1})/g, letter =>
                            letter.toUpperCase()
                          )
                    } Report ${moment(startDate).format("MM.DD.YYYY")}-${moment(
                      endDate
                    ).format("MM.DD.YYYY")}.csv`}
                    ref={csvDownload}
                    data={reportData}
                    style={{ display: "none" }}
                  />
                  <Button
                    onClick={() => setOpenModal(!openModal)}
                    style={{ marginLeft: "10px" }}
                    type="primary"
                  >
                    {t("reports.schedule")}
                  </Button>
                </>
              )}

              <Modal
                title={t("reports.scheduleReport.title")}
                visible={openModal}
                onOk={handleOk}
                footer={null}
                onCancel={handleCancel}
                closable={false}
                width="50%"
                maskClosable={false}
              >
                <ScheduleReport
                  onSubmit={handleOk}
                  handleCancel={handleCancel}
                />
              </Modal>
            </div>
          </Card>
        </Row>
        <Row>
          {/* reports scheduled code goes here */}
          {/* <div className="reports-schedule"></div> */}
        </Row>
      </Col>
    </Container>
  ) : (
    <Loader />
  );
};

export default MarketplaceReports;
