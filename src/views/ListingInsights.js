import React from "react";
import loadable from "@loadable/component";
const ListingInsightsGrid = loadable(() =>
  import("../components/datagrid/ListingsGrid")
);

const ListingInsights = () => <ListingInsightsGrid />;

export default ListingInsights;
