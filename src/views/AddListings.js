import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
import { useHistory, withRouter } from "react-router-dom";
/* const MyAddListings = loadable(() =>
  import("../components/listings/addListing")
); */
import styled from 'styled-components'
import { IosArrowLeft } from '@styled-icons/fluentui-system-filled/IosArrowLeft';
const Leftarrow = styled(IosArrowLeft)`
color: #2C91EE;
width:18px;
height:18px;
  `;
const MyAddListings = loadable(() =>
  import("../components/listings/findListing")
);
const MyAddListingForms = loadable(() =>
  import("../components/listings/addListing")
);
const PageTitle = loadable(() => import("../components/common/PageTitle"));

const AddListings = ({
  match: {
    params: { id }
  }
}) => {

  const { t } = useTranslation();
  const history = useHistory();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(false);
    history.push({ pageTitle: t("addlisting.title"), pageSubTitle: t("title") });
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {id && id.length > 0 ?
        <MyAddListingForms
          eventId={id}
        />
        :
        <MyAddListings />
      }

    </Container>
  ) : (
    <Loader />
  );
};

export default withRouter(AddListings);
