import React, { useEffect, useContext, useState } from "react";
import loadable from "@loadable/component";
import { Container, Row } from "shards-react";
import { getAuthToken } from "../services/stripeService";
import UserContext from "../contexts/user.jsx";
import { updateUserStripeID } from "../services/userService";
import { Redirect } from "react-router-dom";
import Cookies from "universal-cookie";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
const cookies = new Cookies();
const PageTitle = loadable(() => import("../components/common/PageTitle"));

function PayoutConnectionSuccess() {
  const user = useContext(UserContext);
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    // Update the document title using the browser API
    async function getUrlParameter(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]"); //eslint-disable-line
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
      var results = regex.exec(window.location.search);
      return results === null
        ? ""
        : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    //let cookies = new Cookies();
    async function associateStripeIdWithBroker() {
      let urlCodeParam = await getUrlParameter("code");
      /* if we have no Stripe ID on the user and a url Code,
       * we know we have to associate the Stripe ID with the broker
       * for the first time. */
      if (!user.user.stripeId && urlCodeParam) {
        const userID = user.user.id;
        let response = await getAuthToken(urlCodeParam);
        const stripeId = response.stripe_user_id;
        let newUserObject = await updateUserStripeID(userID, stripeId);
        user.setUser(newUserObject);
        cookies.set("userObject", newUserObject, { path: "/" });
      }
    }
    associateStripeIdWithBroker().then(() => {
      setLoading(false);
    });
  }, []);

  if (!user.user.stripeId) {
    return !loading ? (
      <Container fluid className="main-content-container pl-db-50">
        <Row noGutters className="page-header">
          <PageTitle
            title={t("payout.success.title")}
            subtitle={t("title")}
            className="text-sm-left mb-3"
          />
        </Row>
        <Row>{t("payout.success.line1")}</Row>
        <Row>
          {t("payout.success.line2")}
          {user.user.stripeId}.
        </Row>
      </Container>
    ) : (
      <Loader />
    );
  } else {
    return !loading ? <Redirect to="/" /> : <Loader />;
  }
}

export default PayoutConnectionSuccess;
