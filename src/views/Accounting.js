import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
import { useHistory } from "react-router-dom";
const AccountReport = loadable(() =>
  import("../components/accounting/AccountReport")
);

const Accounting = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  const setHistoryPageTitle = () => {
    if (!history.location.pageTitle) {
      history.push({ pageTitle: t("accounting.title"), pageSubTitle: t("title") });
    }
  }
  if (!history.location.pageTitle) {
    setHistoryPageTitle();
  }

  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      {/* <Row noGutters className="page-header">
        <PageTitle
          title = "Accounting"
          subtitle={t("title")}
          className="text-sm-left mb-3"
        />
      </Row> */}
      <Row>
        <Col className="col-12">
          <AccountReport />
        </Col>
      </Row>
    </Container>
  ) : (
    <Loader />
  );
};

export default Accounting;
