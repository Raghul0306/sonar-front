import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row, Col } from "shards-react";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
import { useHistory } from "react-router-dom";
const RemittancesGrid = loadable(() =>
  import("../components/datagrid/RemittancesGrid")
);

const Remittances = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  const setHistoryPageTitle = () => {
    if (!history.location.pageTitle) {
      history.push({ pageTitle: t("remittances.title"), pageSubTitle: t("title") });
    }
  }
  if (!history.location.pageTitle) {
    setHistoryPageTitle();
  }
  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      {/* Page Header */}
      {/* <Row noGutters className="page-header">
        <PageTitle
          title={t("remittances.title")}
          subtitle={t("title")}
          className="text-sm-left mb-3"
        />
      </Row> */}
      <Row>
        <Col className="col-12">          
      <div className="revenue-section-grid"
          style={{
            display: "grid",
            gridTemplateColumns: "repeat(3, 1fr)",
            width: "100%",
            marginBottom: "20px"
          }}
        >
          <div className="revenue-sec">
            <h3 className="gross-revenue-amount">$ 50,604.32</h3>
            <h3 className="gross-revenue-title">{t("remittances.GrossRevenue")}</h3>
          </div>
          <div className="revenue-sec">
            <h3 className="net-revenue-amount">$ 12,034.22</h3>
            <h3 className="net-revenue-title">{t("remittances.NetRevenue")}</h3>
          </div>
          <div className="revenue-sec">
            <h3 className="upcoming-payment-amount">$ 3,408.45</h3>
            <h3 className="upcoming-payment-title">{t("remittances.UpcomingPayments")}</h3>
          </div>
      </div>
          <RemittancesGrid />
        </Col>
      </Row>
    </Container>
  ) : (
    <Loader />
  );
};

export default Remittances;
