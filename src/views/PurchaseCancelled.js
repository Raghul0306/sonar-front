import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container, Row } from "shards-react";
//import UserContext from "../contexts/user.jsx";
//import { Redirect } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
const PageTitle = loadable(() => import("../components/common/PageTitle"));

function PurchaseCancelled() {
  const { t } = useTranslation();
  //const user = useContext(UserContext);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(false);
  }, []);
    return !loading ? (
      <Container fluid className="main-content-container pl-db-50">
        {/* Page Header */}
        <Row noGutters className="page-header">
          <PageTitle
            sm="4"
            title={t("payments.title")}
            subtitle={t("title")}
            className="text-sm-left"
          />
        </Row>
        {/* Default Light Table */}
        <Row>{t("payments.cancelled")}</Row>
      </Container>
    ) : (
      <Loader />
    );
}

export default PurchaseCancelled;