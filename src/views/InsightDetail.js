import React from "react";
import { Container} from "shards-react";
import Col from 'antd/es/col';
import Row from 'antd/es/row';
import Table from 'antd/es/table';
import Badge from 'antd/es/badge';



function NestedTable() {
  const expandedRowRender = () => {
    const columns = [
      { title: 'Event Date', dataIndex: 'date', key: 'date' },
      { title: 'Event Name', dataIndex: 'name', key: 'name' },
      { title: 'Listing ID', dataIndex: 'upgradeNum', key: 'upgradeNum' },
      {
        title: 'Status',
        key: 'state',
        render: () => (
          <span>
            <Badge status="success" />
            Fufilled
          </span>
        ),
      },
      // {
      //   title: 'Action',
      //   dataIndex: 'operation',
      //   key: 'operation',
      //   render: () => (
      //     <span className="table-operation">
      //       <a href="javascript:;">Pause</a>
      //       <a href="javascript:;">Stop</a>
      //       <Dropdown overlay={menu}>
      //         <a href="javascript:;">
      //           More <Icon type="down" />
      //         </a>
      //       </Dropdown>
      //     </span>
      //   ),
      // },
    ];

    const data = [];
    for (let listCount = 0; listCount < 3; ++listCount) {
      data.push({
        key: listCount,
        date: 'This is the event date',
        name: 'This is Event Name name',
        upgradeNum: 'this is the listing ID',
      });
    }
    return <Table columns={columns} dataSource={data} pagination={false} />;
  };

  const columns = [
    { title: 'League', dataIndex: 'name', key: 'name' },
    { title: 'Total Events', dataIndex: 'version', key: 'version' },
    { title: 'Total Listings', dataIndex: 'upgradeNum', key: 'upgradeNum' },
  ];

  const data = [{
    name: "MLB",
    version: "148",
    upgradeNum:12334
  },
  {
    name: "NFL",
    version: "91",
    upgradeNum:1094
  },
  {
    name: "NBA",
    version: "88",
    upgradeNum:886
  }
];

  return (
    <Table
      className="components-table-demo-nested"
      columns={columns}
      expandedRowRender={expandedRowRender}
      dataSource={data}
    />
  );
}
const InsightDetail = () => (
  <Container fluid className="main-content-container pl-db-50">
   Insight Details
   <Row gutter={16}>
      <Col lg={8} xs={24}>
      

      </Col>
      <Col lg={8} xs={24}>

     
      </Col>
    </Row>
    <NestedTable />
  </Container>
);

export default InsightDetail;