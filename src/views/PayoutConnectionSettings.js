import React, { useContext, useEffect, useState } from "react";
import loadable from "@loadable/component";
import { Container, Row, Card, CardBody, Col } from "shards-react";
import UserContext from "../contexts/user.jsx";
import { Redirect } from "react-router-dom";
import { createStripeSettingsURL } from "../services/stripeService";
import { useTranslation } from "react-i18next";
import Loader from "../components/Loader/Loader";
const PageTitle = loadable(() => import("../components/common/PageTitle"));

function PayoutConnectionSettings() {
  const user = useContext(UserContext);
  const { t } = useTranslation();
  const [stripeURL, setStripeURL] = useState(0);
  const [dummyVar] = useState(0);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    if (user.user.stripeId) {
      async function getStripeSettingsURL(accountID) {
        let urlObject = await createStripeSettingsURL(
          accountID,
          window.location.href
        );
        setStripeURL(urlObject.url);
      }
      getStripeSettingsURL(user.user.stripeId);
      setLoading(false);
    }
  }, [dummyVar]);
  if (user.user.stripeId) {
    return !loading ? (
      <Container fluid className="main-content-container pl-db-50">
        <Row noGutters className="page-header">
          <PageTitle
            title={t("payout.settings.title")}
            subtitle={t("title")}
            className="text-sm-left mb-3"
          />
        </Row>
        <Col sm={12}>
          <Row
            style={{
              alignSelf: "center",
              justifyContent: "center",
              width: "70%",
              height: "400px",
              marginTop: "15px"
            }}
          >
            {/* If you need to change your stripe payout settings (Bank account,
          contact information, etc)
          <a href={stripeURL}>click here</a> */}
            <Card style={{ width: "100%" }}>
              <CardBody>
                <p>{t("payout.settings.edit1")}</p>
                <p>
                  {t("payout.settings.please")}
                  <a href={stripeURL}>{t("payout.settings.click")}</a>{" "}
                  {t("payout.settings.redirect")}
                </p>
              </CardBody>
            </Card>
          </Row>
        </Col>
      </Container>
    ) : (
      <Loader />
    );
  } else {
    return <Redirect to="/payout-connection" />;
  }
}

export default PayoutConnectionSettings;
