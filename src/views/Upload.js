import React, { useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container } from "shards-react";
import Loader from "../components/Loader/Loader";
const Upload = loadable(() => import("../components/uploader/upload"));

const TicketUpload = () => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(false);
  }, []);
  return !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      <Upload />
    </Container>
  ) : (
    <Loader />
  );
};

export default TicketUpload;
