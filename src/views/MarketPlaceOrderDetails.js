import React, { useContext, useState, useEffect } from "react";
import loadable from "@loadable/component";
import { Container } from "shards-react";

import { useTranslation } from "react-i18next";
import UserContext from "../contexts/user.jsx";
import { useParams } from 'react-router-dom';
import Loader from "../components/Loader/Loader";
import { ticketTypes, userRolesLower } from "../constants/constants";
import { getMarketPlaceOrderDetailsById, getChannelLogo } from "../services/ordersService";
import styled from 'styled-components'
import { IosArrowLeft } from '@styled-icons/fluentui-system-filled/IosArrowLeft';
import { useHistory } from "react-router-dom";
const Leftarrow = styled(IosArrowLeft)`
  color: #2C91EE;
  width:18px;
  height:18px;
`;
const ChannelCards = loadable(() =>
  import("../components/orders/ChannelCards.js")
);
const EventsListCard = loadable(() =>
  import("../components/orders/EventsListCard.js")
);
const OrderActionsCard = loadable(() =>
  import("../components/orders/OrderActionCard.js")
);
const DeliveryStatusCard = loadable(() =>
  import("../components/orders/DeliveryStatusCard.js")
);
const SellerDetailsCard = loadable(() =>
  import("../components/orders/SellerDetailsCard.js")
);
const PickupInformation = loadable(() =>
  import("../components/orders/PickupInformation.js")
);
const SellerInfoCard = loadable(() =>
  import("../components/orders/SellerInfoCard.js")
);

const PageTitle = loadable(() => import("../components/common/PageTitle"));


const OrderDetails = ({ smallStats,
  match: {
    params: { id }
  }
}) => {
  const [loading, setLoading] = useState(true);
  const [orderDetails, setDetails] = useState({});
  const history = useHistory();
  const [channelLogoData, setChannelLogoData] = useState("");
  //const [eventDetails, setEventDetails] = useState({});
  //const [listingDetails, setListingDetails] = useState({});
  const { t } = useTranslation();
  const { user, setUser } = useContext(UserContext);
  const parameters = useParams();


  useEffect(() => {
    setLoading(false);
    GenerateShippingLabel();
    history.push({ pageTitle: "Order Details", pageSubTitle: "title", redirectPath: 'marketplace-orders' });
  }, []);

  const pageReload = (data) => {
    setLoading(data);
    GenerateShippingLabel()
    setLoading(false);
  }


  const GenerateShippingLabel = async () => {

    if (user.userRole == userRolesLower.admin || userRolesLower.marketplace_admin || userRolesLower.marketplace_user) {
      let marketPlaceOrderDetails = await getMarketPlaceOrderDetailsById(id);
      setDetails(marketPlaceOrderDetails.data);
      
      if(marketPlaceOrderDetails.data && marketPlaceOrderDetails.data.broker_order && marketPlaceOrderDetails.data.broker_order.channel_id){
        await getChannelLogo(marketPlaceOrderDetails.data.broker_order.channel_id).then((res) => {
          if(res && res.data)
          {
            setChannelLogoData(res.data)
          }else{
            setChannelLogoData(false)
          }
        });
      }
      else{
        setChannelLogoData(false)
      }
    }
  }

  return orderDetails && !loading ? (
    <Container fluid className="main-content-container pl-db-50">
      <div
        noGutters
        style={{ padding: "1rem 0" }}
        className="page-header"
      >
         
        <div className="order-grid-view">
          <div className="order-grid-section">
            <ChannelCards orderDetails={orderDetails} logoImageData={channelLogoData} />
            <OrderActionsCard />
          </div>

          {orderDetails.event && orderDetails.listing_group && (
            <div className="order-grid-section grid2-seller-info">
              <EventsListCard eventDetails={orderDetails.event} eventId={orderDetails.event_id} listingDetails={orderDetails.listing_group} listingGroupId={orderDetails.listing_group_id} />
              <DeliveryStatusCard deliveryStatus={orderDetails} />

              <SellerInfoCard SellerDetails={orderDetails.user} />

              {ticketTypes.eTransfer.id !== orderDetails.ticket_type.ticket_type_id && (
                <SellerDetailsCard pageReload={pageReload} attatchmentDetails={orderDetails.listing_group} eventId={orderDetails.event_id} listingGroupId={orderDetails.listing_group_id} ticketType={orderDetails.ticket_type} seatStart={orderDetails.listing_group.seats.substring(0, orderDetails.listing_group.seats.indexOf("-"))} />
              )}

            </div>
          )}

        </div>
        {orderDetails.ticket_type && (
          <div className="information-detail-section grid3-seller-info">
            <PickupInformation ticketType={orderDetails.ticket_type} />
            {/* <SellerInfoCard SellerDetails={orderDetails.user} /> */}
          </div>
        )}

      </div>
    </Container>
  ) : (
    <Loader />
  );
};

export default OrderDetails;
