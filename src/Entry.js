import React, { Component } from "react";
import loadable from "@loadable/component";
import history from './helpers/history';
import "antd/dist/antd.css";
import "./assets/overrides.css";
import UserContext from "./contexts/user.jsx";
import Cookies from "universal-cookie";
import "bootstrap/dist/css/bootstrap.min.css";
import "./shards-dashboard/styles/shards-dashboards.1.1.0.min.css";
import { getUser } from "./services/checkAuthService";
import { updateUserLastLoggedIn } from "./services/userService";
import * as moment from "moment";
import "./assets/custom-style.css";
// import "./assets/ui-stylesheet.css";
// import "./assets/stylesheet-font.css";
//Translations
import i18next from "i18next";
import { initReactI18next } from "react-i18next";
import common_en from "./locales/en/translation.json";
import {setHeaderToken} from "./interceptors/interceptor.js"
const App = loadable(() => import("./App"));

const cookies = new Cookies();

const languageDetector = {
  type: "languageDetector",
  async: true,
  detect: cb => cb("en"),
  init: () => {},
  cacheUserLanguage: () => {}
};
i18next
  .use(languageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: "en",
    debug: false,
    resources: {
      en: {
        translation: common_en
      }
    }
  });
class Entry extends Component {
  setUser = user => {
    this.setState({ user }, function() {});
  };

  changeRole = role => {
    this.setState({ tempRole: role }, function() {});
  };

  switchUser = async userId => {
    let user = await getUser(userId);
    this.setState({ tempUser: user }, function() {});
  };

  logout = user => {
    cookies.set("userObject", user, { path: "/", maxAge: 0 }); //Remove was not working, so issue an immediate expire on the cookie.
    this.setState({ user: null });
    cookies.set("accessToken", '', { path: "/", maxAge: 0 });
    history.push('/');
  };

  state = {
    user: null,
    setUser: this.setUser,
    changeRole: this.changeRole,
    switchUser: this.switchUser,
    tempUser: null,
    logout: this.logout,
    loading: true,
    userStats: null,
    requestPassForm: false,
    resetToken: false
  };

  componentDidMount() {
    setHeaderToken();
    let user = cookies.get("userObject");
    if (user) {
      this.setState(
        {
          user: user,
          userStats: { unsoldListings: 0 },
          loading: false
        },
        function() {
          // update the last login time here
          updateUserLastLoggedIn(user.id, moment(Date.now()).format("YYYY-MM-DD HH:MM:SS"));
        }
      );
    } else {
      this.setState({ loading: false });
    }
  }

  render() {
    return <div className="App App-header">
        <UserContext.Provider value={this.state}>
              {!this.state.loading && <App /> }
        </UserContext.Provider>
      </div>
    
  }
}

export default Entry;
