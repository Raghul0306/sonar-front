module.exports = {

    onboardingStage: { zero: 0, one: 1, two: 2, three: 3, four: 4, five: 5, six: 6 },
    listingConst: {
        in_hand: "in_hand",
        quantity: "quantity",
        inventory_attached: "inventory_attached",
        inv_attach: "inv_attach",
        hold: "hold",
        row: "row",
        section: "section",
        tags: "tags",
        category: "category",
        genre: "genre",
        ticket_type: "ticket_type",
        ticketType: "ticketType",
        yesLabel: "Yes",
        noLabel: "No",
        yesValue: "yes",
        noValue: "no",
        inhand_date: "inhand_date",
        split: "split",
        seat_type_id: "seat_type_id",
        hundred: 99,
        max_nine: 99999,
        max_quantity: 100,
        maxLength_percent: 2,//max percent 99
        maxLength_amount: 5,
        channel_markup_type_one: 1,
        channel_markup_type_two: 2,
        etickets: "eTicket",
        seat_start: 'seat_start',
        seat_end: 'seat_end',
        group_cost: 'group_cost',
        face_value: 'face_value',
        ticket_type_id: 'ticket_type_id',
        pagination_length: 2,
        attachment_error: 'List updated successfully.  Attachment(s) are failed to upload! Try again later',
        add_attachment_error: 'List created successfully.  Attachment(s) are failed to upload! Try again later',
        price: 'price',
        localpickup_id: 5,
        ticketingSystem: "ticketing_system",
        ticketing_system_accounts: "ticketing_system_accounts",
        yadara:"Yadara",
        tagIds:"tagIds",
        disclosure:"disclosure",
        viewListing:"viewListing",
        viewPO:"viewPO",
        completedEvents:"completedEvents",
        purchaseDate:"purchaseDate",
        company:"company",
        userAssignedTo:"userAssignedTo",
        ticketingSystemAccount:"ticketingSystemAccount", 
        soldprice: "soldprice"
    },

    monthNamesShort: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ],
    daysFull: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],

    weekDays: [
        { label: "Sunday", value: "sunday" },
        { label: "Monday", value: "monday" },
        { label: "Tuesday", value: "tuesday" },
        { label: "Wednesday", value: "wednesday" },
        { label: "Thursday", value: "thursday" },
        { label: "Friday", value: "friday" },
        { label: "Saturday", value: "saturday" },
    ],
    userRoles: {
        admin: "ADMIN",
        broker: "BROKER",
        broker_user: "BROKER_USER",
        marketplace_admin: "MARKETPLACE_ADMIN",
        marketplace_user: "MARKETPLACE_USER",
        seller: "SELLER", 
        breaks_seller: "BREAKS_SELLER", 
        support: "SUPPORT"
    },
    userRolesLower: {
        admin: "admin",
        broker: "broker",
        broker_user: "broker_user",
        marketplace_admin: "marketplace_admin",
        marketplace_user: "marketplace_user",
        buyer: "buyer", 
        support: "support",
        super_super_admin: "super_super_admin", 
        super_admin: "super_admin", 
        accounting_admin: "accounting_admin", 
        accounting_user: "accounting_user", 
        processing_admin: "processing_admin", 
        processing_user: "processing_user", 
        support_admin: "support_admin", 
        support_user: "support_user" 
    },
    userRolesNames: {
        admin: "Admin",
        broker: "Broker Admin",
        broker_user: "Broker User",
        marketplace_admin: "Marketplace Admin",
        marketplace_user: "Marketplace User",
        buyer: "Buyer", 
        support: "Support",
        super_super_admin: "Super Super Admin", 
        super_admin: "Super Admin", 
        accounting_admin: "Accounting Admin", 
        accounting_user: "Accounting User", 
        processing_admin: "Processing Admin", 
        processing_user: "Processing User", 
        support_admin: "Support Admin", 
        support_user: "Support User" 
    },
    userRoleIds: {
        admin: 1,
        broker: 2,
        broker_user: 3,
        marketplace_admin: 4,
        marketplace_user: 5,
        buyer: 6,
        support: 7
    },
    userRolesArray: [
        "Super Super Admin",
        "Super Admin",
        "Admin",
        "Broker",
        "Broker User",
        "Accounting Admin",
        "Accounting User",
        "Processing Admin",
        "Processing User",
        "Marketplace Admin",
        "Marketplace User",
        "Buyer",
        "Support",
        "Support Admin",
        "Support User"
    ],
    boardings: {
        doneOnboarding: "doneOnboarding",
    },
    steps: {
        stepone: "stepOne",
        steptwo: "stepTwo",
        stepthree: "stepThree",
        stepfour: "stepFour",
        stepfive: "stepFive",
        stepsix: "stepSix",
    },
    preselectedSeat: { "label": "Consecutive", "value": "1" },
    preselectedHideSeatNumber: [false],
    preselectedSplit: { "label": "Never Leave One", "value": "3" },
    fileTypes: {
        png: 'image/jpeg',
        gif: 'image/jpeg',
        jpg: 'image/jpeg',
        jpeg: 'image/jpeg',
        jfif: 'image/jpeg',
        pdf: 'application/pdf',
        csv: 'text/csv',
        zip: 'application/zip',
        rar: 'application/octet-stream',
        xlsx_listing: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        csv_listing: "application/vnd.ms-excel",
    },
    delivery_status: {
        completed: "1",
        cancelled: "2",
        pending: "3",
        incomplete: "4",
        all: "all"
    },
    delivery_status_name: {
        completed: "Completed",
        cancelled: "Cancelled",
        pending: "Pending",
        incomplete: "Incomplete",
        delivered: "Delivered",
    },
    order_status: [
        'NA', 'Completed', 'Active', 'Cancelled Refund', 'Cancelled No Refund', 'Hold'
    ],
    ticketTypes: {
        eTicket: {
            id: 1,
            name: "eTicket"
        },
        eTransfer: {
            id: 2,
            name: "eTransfer"
        },
        hardStock: {
            id: 3,
            name: "Hard Stock"
        },
        willCall: {
            id: 4,
            name: "Will Call"
        },
        localPickup: {
            id: 5,
            name: "Local Pick Up"
        },
        qrCode: {
            id: 6,
            name: "QR Code"
        }
    },
    seatTypes: {
        generalAdmission: {
            id: '2',
            name: "General Admission"
        }
    },
    seatTypeNames:[
        'Sequential',
        'General Admission',
        'Odd/Even',
        'Piggyback'
    ],
    userRolesTemplate: {
        admin: "sidebar-default",
        broker: "secondary-sidebar",
        broker_user: "secondary-sidebar",
        marketplace_admin: "primary-sidebar",
        marketplace_user: "primary-sidebar",
        super_super_admin : "sidebar-default",
        super_admin : "sidebar-default",
        accounting_admin : "sidebar-default",
        accounting_user : "sidebar-default",
        processing_admin : "sidebar-default",
        processing_user : "sidebar-default",
        buyer : "sidebar-default",
        support : "sidebar-default",
        support_admin : "sidebar-default",
        support_user : "sidebar-default"
    },
    folderNames: {
        eventsFolder: "events",
        channelsFolder: "channels",
        venuesFolder: "venues",
        caegoryFolder: "categories",
        genreFolder: "genre",
        uploadListingsFolder: "uploadlistings",
        uploadsFolder: "uploads"
    }, 
    uploadURLs: {
        tempLocalStorageUploadListingsURL: "/uploadlistingfile", 
        imageUploadURL: "/imageupload",
        listingsAttachmentURL: "/upload"
    },
    downloadURLs: {
        listingsAttachmentURL: "/downloads", 
        viewEventAttachment: "/viewEventAttachment"
    }, 
    eventClassName:{
        listOfStatus:"list-of-status",
        searchResultActive:"search-result-active"
    }, 
    currentPathNames: {
        addListing: "add-listing", 
        addListings: "add-listings", 
        listings: "listings",
        operation: "operation",

    }, 
    sidebarTabNames: {
        listings: "LISTINGS", 
        orders: "ORDERS"
    }, 
    responseStatus: {
        ok: "ok", 
        error: "error", 
        success: "success"
    }, 
    listingGroupInfo: {
        section: "section", 
        row: "row", 
        quantity: "quantity", 
        quantityGA: "quantity_ga", 
        seatStart: "seat_start", 
        seatEnd: "seat_end", 
        cost: "cost", 
        price: "price", 
        attribute: "attribute", 
        disclosure: "disclosure", 
        ticketingSystem: "ticketing_system", 
        tag: "tag", 
        ticketingSystemAccount: "ticketingSystemAccount"
    }, 
    shareTypes: {
        share: "share", 
        pauseShare: "pauseShare"
    }, 
    editEventStateVariables: {
        eventName: "eventName", 
        eventDate: "eventDate", 
        eventTime: "eventTime", 
        venueAddress: "venueAddress", 
        venueAddress2: "venueAddress2", 
        venueCity: "venueCity", 
        venueZipCode: "venueZipcode", 
        venueState: "venueState", 
        venueCountry: "venueCountry", 
        ticketingSystemId: "ticketSystemId", 
        eventLink: "eventLink",
        category: "category",
        genre: "genre"
    }, 
    levelNames: {
        listing: "listing", 
        event: "event"
    }, 
    purchaseOrderVendorStateVariables: {
        type: "vendor", 
        vendorAddress: "vendorAddress1", 
        vendorAddress2: "vendorAddress2", 
        vendorName: "vendorName", 
        vendorEmail: "vendorEmail", 
        vendorPhone: "vendorPhone", 
        vendorCity: "vendorCity", 
        vendorState: "vendorState", 
        vendorZipCode: "vendorZipcode", 
        vendorCountry: "vendorCountry"
    }, 
    marketPlaceReportTypes: {
        name: "Marketplace Reports", 
        plCSVType: "item", 
        plType: "pl", 
        sales: "sales", 
        listings: "listings", 
        delivery: "delivery", 
        deliveryOpen: "delivery-open", 
        user: "users-reports", 
        purchase: "purchase"
    }, 
    modalPopupEventNames: {
        addEvent: "addEvent", 
        editEvent: "editEvent"
    }, 
    searchFiltersFor: {
        venue: "venue", 
        venueName: "venueName"
    },
    tabsPane:{
        paramKey:"find",
        tabOne:"1",
        tabTwo:"2",
        tabThree:"3"
    },
    buttonProps:{
        purchase:'purchase'
    },
    fieldTypes: {
        text: 'text', 
        password: 'password'
    },
    channelManagementConst: {
        zero: 0,
        one: 1,
        two: 2
    },
    channels: {
        Phunnel:"Phunnel"
    },
    filterListingData:{
        section:"section",
        sold:"Sold",
        available:"Available",
        urgent:"Urgent",
        inactive:"Inactive",
        row:"row",
        price:"price",
        flag:"flag"         
    },
    sortListingData:{
        sectionInAscending:"sortListingDataBySectionInAscending",
        sectionInDescending:"sortListingDataBySectionInDescending",
        rowInAscending:"sortListingDataByRowInAscending",
        rowInDescending:"sortListingDataByRowInDescending",
        priceInAscending:"sortListingDataByPriceInAscending",
        priceInDescending:"sortListingDataByPriceInDescending"
    },
    currencySymbol:{
        usd:'$'
    },
    allListingStatus:{
        qc_hold:"qc_hold",
        active:"active",
        inactive:"inactive",
        sold:"sold",
        viagogo_hold:"viagogo_hold"
    },
    operationConstantData: {
        tagsAndNotes:"Tags and Notes",
        listingDetails:"Listing Details",
        purchaseOrderDetails:"Purchase Order Details",
        disclosures:"Disclosures",
        inhandDate:"In-Hand Date",
        splits:"Splits",
        seatType:"Seat Type",
        stockType:"Stock Type",
        cost:"Cost",
        generalAdmission:"General Admission"
    },
    reports:{
        reportType:{
            purchaseReport:{value:"purchase_report",label:"Purchase Report"},
            inventoryReport:{value:"inventory_report",label:"Inventory Report"},
            salesReport:{value:"sales_report",label:"Sales Report"}
        },
        reportFrequency: [
            {value:'daily',label:"Daily"},
            {value:'weekly',label:"Weekly"},
            {value:'monthly',label:"Monthly"},
            {value:'yearly',label:"Yearly"}
        ]
    },
    operationGridConstant:{
        section:"section",
        row:"row",
        owner:"owner",
        externalReference:"externalReference",
        ticketingSystemAccount:"ticketingSystemAccount",
        seatEnd:"seatEnd",
        seatStart:"seatStart",
        purchaseOrderDate:"purchaseOrderDate",
        last4Digits:"last4Digits",
        inHandDate:"inHandDate",
        seatNumberInfo:"seatNumberInfo",
        quantity:"quantity"
    },
    dateValidationString: /^((0[1-9]|[12][0-9]|3[01])(\/)(0[13578]|1[02]))|((0[1-9]|[12][0-9])(\/)(02))|((0[1-9]|[12][0-9]|3[0])(\/)(0[469]|11))(\/)\d{4}$/,
    backButtonEvent:"deleteContentBackward"

    
};
