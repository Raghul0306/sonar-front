import moment from 'moment';
const getFormattedUnixDate = (date) => {
    return moment.unix(date).format('MM/DD/YYYY')
}

export default getFormattedUnixDate