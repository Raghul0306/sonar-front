/**
 * Return formatted date
 * @param {string} value unformatted date
 * @param {string} divider date divider
 * @param {boolean} time show time
 */
const getFormattedDate = (value, divider = "-", time = false) => {
  if (value) {
    const date = new Date(value);
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const year = date.getFullYear();
    const hours = date.getHours();
    const minutes = date.getMinutes();

    const formattedMonth = month < 10 ? "0" + month : month;
    const formattedDay = day < 10 ? "0" + day : day;
    const formattedMinutes = minutes < 10 ? "0" + minutes : minutes;
    const formattedTime =
      hours > 12
        ? hours - 12 + `:${formattedMinutes}pm`
        : hours < 12
        ? (hours === 0 ? 12 : hours) + `:${formattedMinutes}am`
        : hours + `:${formattedMinutes}pm`;

    return `${formattedDay}${divider}${formattedMonth}${divider}${year}${
      time ? " " + formattedTime : ""
    }`;
  }
};

export default getFormattedDate;
