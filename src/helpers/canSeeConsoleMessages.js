export default function canSeeConsoleMessages(user) {
    return user && user.userRole === "ADMIN";
}