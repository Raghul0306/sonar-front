import { getListingById } from "../services/listingsService";

/**
 * Will return an array of image URLs most relevant to the
 * listing in question. By default will return an empty array.
 * @param {Integer} listingId 
 */
export async function returnListingImages(listingId) {
    let returnImages = [];
    // grab the listing
    let listing = await getListingById(listingId);
    if(listing.image) {
        // use listing images
        return listing.image;
    }
    else if(listing.cardByCardId.imgUrlFront) {
        if(listing.cardByCardId.imgUrlBack) {
            return [listing.cardByCardId.imgUrlFront, listing.cardByCardId.imgUrlBack];
        }
        return [listing.cardByCardId.imgUrlFront];
    }
    return returnImages;
}