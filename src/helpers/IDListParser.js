import { getPerformerById, getOrganizationById } from "../services/cardService";

export async function getPerformerNamesFromPerformerIDList(performerIDList) {
    let performers = [];
    if(performerIDList.length > 1) {
        let thePerformer;
        for(let performerIdCount = 0; performerIdCount < performerIDList.length; performerIdCount++) {
            thePerformer = await getPerformerById(performerIDList[performerIdCount]);
            performers.push(thePerformer.name);
        }
    }
    else {
        let thePerformer = await getPerformerById(performerIDList[0]);
        performers.push(thePerformer.name);
    }
    return performers;
}

export async function getOrgNamesFromOrgIDList(orgIDList) {
    let organizations = [];
    if(orgIDList.length > 1) {
        let theOrganization;
        for(let orgIdCount = 0; orgIdCount < orgIDList.length; orgIdCount++) {
            theOrganization = await getOrganizationById(orgIDList[orgIdCount]);
            organizations.push(theOrganization.name);
        }
    }
    else {
        let theOrganization = await getOrganizationById(orgIDList[0]);
        organizations.push(theOrganization.name);
    }
    return organizations;
}