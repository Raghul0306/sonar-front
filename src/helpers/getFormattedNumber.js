/**
 * Return formatted number
 * @param {number} num number to be formatted
 * @param {string} type type of formatting
 */
const getFormattedNumber = (num, type) => {
  const tempNumb = Math.abs(num)
    .toFixed(2)
    .split(".");
  let int = tempNumb[0];
  let dig = tempNumb[1];

  if (int.length > 3) {
    int = int.substr(0, int.length - 3) + "," + int.substr(int.length - 3, 3);
  }
  switch (type) {
    case "digital":
      return (num >= 0 ? "$" : "- $") + `${int}.${dig}`;
    case "count":
      return int;
    default:
      return (num >= 0 ? "$" : "- $") + `${int}`;
  }
};

export default getFormattedNumber;
