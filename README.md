# MyTickets Point of Sale
The MyTickets Point of Sale is an application written in React, driven by a NodeJS/GraphQL backend. The application sits on an Amazon EC2 instance, as well as the GraphQL necessary for communicating with the PostgreSQL databases.

The application will use "services" to communicate with the `phunnel-api` over GraphQL, using queries and mutations. If you need additional functionality in the API, you'll need to modify both `phunnel-api` and write a service for that new function inside of this repository.

Understand that if there is a third-party integration you are adding to the application, the typical approach would be to write a microservice in an adjacent repository, import that microservice as a new function in the `phunnel-api`, and then write a service inside of this repository to access that function.


## Getting Started:
Before you run the application locally, ensure you have all the correct packages by running these two commands:

```
npm install
npm start
```

or

```
yarn install
yarn start
```

## Using the right GraphQL server for application data
When you download the source code, you will see a `.env` file, with a few variables referencing "GraphQL". Be sure these are set to the correct web address to see data.
