import React from 'react';
import loadable from "@loadable/component";
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
// Analytics
import "./index.css";
// you can broadcast custom events like this:
//amplitude.getInstance().logEvent('open app');
const Entry = loadable(() => import("./Entry"));


ReactDOM.render(<Entry/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
